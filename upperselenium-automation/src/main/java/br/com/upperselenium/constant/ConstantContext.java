package br.com.upperselenium.constant;

public enum ConstantContext {

	// PIM
	USUARIO("usuario"),
	USUARIO_NOME_REAL("usuarioNomeReal"),
	
	// Utilizados nos Testes de Exemplo
	EXISTING_USER("existingUser"),
	EXISTING_NOTE("existingNote"),
	NEW_USER("newUser"),
	RESULT_NAME("resultName");
	
	private String value;

	private ConstantContext(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
	
}