package br.com.upperselenium.constant;


/**
 * Interface de Constantes: As constantes não devem ser abreviadas e/ou devem possuir o mesmo nome correspondente ao seu valor,
 * ignorando-se apenas caracteres especiais (com possibilidades de nomenclatura por extenso para a constante).
 *
 * Ex: String SOBRE_NOS = "Sobre Nós";
 * Ex: String _200_GRAUS_CELSIUS = "200°C";
 * 
 * @author Hudson
 *
 */
public interface DPConstant {
	
	/**
	 * Definir o caminho do arquivo .json a partir de "src/test/resources/."
	 * Ou seja, considerar as constantes de módulos a partir de "dataprovider/...".
	 * 
	 * @author HudsonRosa
	 */
	interface Path {
		
		String PROJECT_PACKAGE = "br.com.upperselenium.";
		String DATAPROVIDER_FOLDER = "dataprovider/";
		
		// Configurações de diretórios dos arquivos DP dos testes de Exemplo
		String SAMPLE_DP_FOLDER = DATAPROVIDER_FOLDER + "sample/";
		String LOGIN_SAMPLE_DP_FOLDER = DATAPROVIDER_FOLDER + "sample/";
		
		// Configurações de diretórios dos arquivos DP da PIM App
		String APP_DP_FOLDER = DATAPROVIDER_FOLDER + "pim/";
		String APP_LOGIN_DP_FOLDER = DATAPROVIDER_FOLDER + "pim/wrapper";
		
	}

}
