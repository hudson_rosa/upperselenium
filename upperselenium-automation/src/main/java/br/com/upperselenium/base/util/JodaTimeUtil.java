package br.com.upperselenium.base.util;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.joda.time.Interval;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import br.com.upperselenium.base.parameter.PatternPRM;

/**
 * Classe utilitária para manipulação de datas. (utiliza-se da biblioteca JodaTime) 
 * 
 * @author Hudson
 */
public class JodaTimeUtil {

	private static final String EMPTY = "";
	private static final int ONE = 1;
	private static String CURRENT_YEAR = "CURRENT_YEAR";
	private static String NEXT_YEAR = "NEXT_YEAR";
	private static String LAST_YEAR = "LAST_YEAR";
	private static final int _MILIS = 1000;

	private JodaTimeUtil() {
	}
	
	/**
	 * Método que retorna a diferença entre intervalo inicial e final de tempo, em milissegundos.
	 * 
	 * @param initialInterval
	 * @param finalInterval
	 * @return
	 */
	public static long diffDurationIntervalInMillis(DateTime initialInterval, DateTime finalInterval) {
		Interval interval = new Interval(initialInterval, finalInterval);
		Duration duration = interval.toDuration();
		return duration.getMillis(); 
	}
	
	/**
	 * Método que retorna a diferença entre intervalo inicial e final de tempo, em dias.
	 * 
	 * @param initialInterval
	 * @param finalInterval
	 * @return
	 */
	public static long diffDurationIntervalInDays(DateTime initialInterval, DateTime finalInterval) {
		Interval interval = new Interval(initialInterval, finalInterval);
		Duration duration = interval.toDuration();
		return duration.getStandardDays();  
	}

	/**
	 * Método que retorna a diferença entre intervalo inicial e final de tempo, em horas.
	 * 
	 * @param initialInterval
	 * @param finalInterval
	 * @return
	 */
	public static long diffDurationIntervalInHours(DateTime initialInterval, DateTime finalInterval) {
		Interval interval = new Interval(initialInterval, finalInterval);
		Duration duration = interval.toDuration();
		return duration.getStandardHours();  
	}	
	
	/**
	 * Método que retorna a diferença entre intervalo inicial e final de tempo, em segundos.
	 * 
	 * @param initialInterval
	 * @param finalInterval
	 * @return
	 */
	public static long diffDurationIntervalInMinutes(DateTime initialInterval, DateTime finalInterval) {
		Interval interval = new Interval(initialInterval, finalInterval);
		Duration duration = interval.toDuration();
		return duration.getStandardMinutes();  
	}	
	
	/**
	 * Método que retorna a diferença entre intervalo inicial e final de tempo, em segundos.
	 * 
	 * @param initialInterval
	 * @param finalInterval
	 * @return
	 */
	public static long diffDurationIntervalInSeconds(DateTime initialInterval, DateTime finalInterval) {
		Interval interval = new Interval(initialInterval, finalInterval);
		Duration duration = interval.toDuration();
		return duration.getStandardSeconds();  
	}	
	
	
	/**
	 * Método para conversão de Milissegundos em Segundos.
	 * 
	 * @param result
	 * @return
	 */
	@SuppressWarnings("unused")
	private static int convertMilisInSecs(int number) {
		int seconds = (number / _MILIS) % 60 ;
		return seconds;
	}

	/**
	 * Método para retornar o tempo de duração entre dois intervalos DateTime. O retorno será em dias, horas, minutos e segundos.
	 * 
	 * @param startTimeContext
	 * @param endTimeContext
	 * @return
	 */
	public static String getDurationTimeDDHHMMSS(String startTimeContext, String endTimeContext) {
		DateTime start = JodaTimeUtil.convertStringInDateTime(startTimeContext);
		DateTime end = JodaTimeUtil.convertStringInDateTime(endTimeContext);
		
		Duration duration = new Duration(start, end);
		PeriodFormatter formatter = new PeriodFormatterBuilder()
		     .appendDays().appendSuffix(" dia(s) ")
		     .appendHours().appendSuffix(" hora(s) ")
		     .appendMinutes().appendSuffix(" minuto(s) ")
		     .appendSeconds().appendSuffix(" segundo(s)")
		     .toFormatter();
		String timeDuration = formatter.print(duration.toPeriod());
		return timeDuration;
	}
	
	/**
	 * Método para conversão de DateTime em uma String.
	 * 
	 * @param dateTime
	 * @return
	 */
	public static String convertDateTimeInString(DateTime dateTime) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern(PatternPRM.Period.FORMAT_TIME_DDMMYYYYHHMMSS);
		String dateFormatted = dateTime.toString(formatter);
		return dateFormatted;
	}
	
	/**
	 * Método para conversão de uma String em um DateTime.
	 * 
	 * @param dateTime
	 * @return
	 */
	public static DateTime convertStringInDateTime(String dateTime) {
		DateTime date = DateTime.parse(dateTime,DateTimeFormat.forPattern(PatternPRM.Period.FORMAT_TIME_DDMMYYYYHHMMSS));
		return date;
	}
	
	/**
	 * Método que incrementa o mês de acordo com a data atual e retorna "mes_por_extenso/yyyy".
	 * Ex: Data Atual: 18/12/2015, com acréscimo de 5 meses, retorno será 05/2016
	 * 
	 * @param increase
	 * @return String
	 */
	public static String plusNumericMonthPatternMMYYYY(Integer increase) {
		DateTimeFormatter monthAndYearFormat = DateTimeFormat.forPattern(PatternPRM.Date.FORMAT_DATE_MMYYYY).withLocale(new Locale("pt", "BR"));
		String monthAndYear = EMPTY + DateTime.now().plusMonths(increase).toString(monthAndYearFormat);
		return monthAndYear;
	}

	/**
	 * Método que incrementa o mês de acordo com a data atual e retorna "mes_por_extenso/yyyy".
	 * Ex: Data Atual: 18/12/2015, com acréscimo de 5 meses, retorno será Maio/2016
	 * 
	 * @param increase
	 * @return String
	 */
	public static String plusMonthNamePatternMMMMMYYYY(Integer increase) {
		DateTimeFormatter monthAndYearFormat = DateTimeFormat.forPattern(PatternPRM.Date.FORMAT_DATE_MMMMMYYYY).withLocale(new Locale("pt", "BR"));
		String monthAndYear = EMPTY + DateTime.now().plusMonths(increase).toString(monthAndYearFormat);
		return monthAndYear;
	}
	
	/**
	 * Converte Dias Corridos em uma data específica de acordo com o parâmetro passado. 
	 * 
	 * @param currentDaysInNumber
	 * @return
	 */
	public static String parseNumericCurrentDaysToDDMMYYYY(String currentDaysInNumber) {
		if(StringUtils.isBlank(currentDaysInNumber)){
			return null;
		}
		Integer parsedDaysToInteger = Integer.parseInt(currentDaysInNumber);		
		DateTime dateNow = DateTime.now();
		dateNow = dateNow.plusDays(parsedDaysToInteger);		
		DateTimeFormatter dataFormat = formatDatePatternDDMMYYYY();
		String dateInCurrentDays = EMPTY + dateNow.toString(dataFormat);
		return dateInCurrentDays;
	}	
	
	/**
	 * Método responsável pela definição do ano a ser substituído na massa de dados, 
	 * através das constantes:
	 * ANO_ATUAL, ANO_SEGUINTE, ANO_ANTERIOR
	 * 
	 * @param date
	 * @return
	 */	
	public static String replaceYear(String date) {
		String year = date;
		if (date.toUpperCase().contains(CURRENT_YEAR)) {
			year = date.replace(CURRENT_YEAR, EMPTY + getCurrentYear());
		} else if (date.toUpperCase().contains(NEXT_YEAR)) {
			year = date.replace(NEXT_YEAR, EMPTY + getFormatedNextYear(formatYearPatternYYYY()));
		} else if (date.toUpperCase().contains(LAST_YEAR)){
			year = date.replace(LAST_YEAR, getFormatedLastYear(formatYearPatternYYYY()));
		}
		return year;
	}
	
	
	// RECUPERA DATAS FORMATADAS
	
	/**
	 * Retorna o Ano Atual como DateTime.
	 * 
	 * @return DateTime;
	 */
	public static int getCurrentYear() {
		return DateTime.now().getYear();
	}
	
	/**
	 * Retorna o Ano Seguinte formatado.
	 * 
	 * @param formatoAno
	 * @param now
	 * @return DateTime
	 */
	public static String getFormatedNextYear(DateTimeFormatter formatoAno) {
		return DateTime.now().plusYears(ONE).toString(formatoAno);
	}

	/**
	 * Retorna o Ano Anterior formatado.
	 * 
	 * @param formatoAno
	 * @param now
	 * @return DateTime, DateTimeFormatter
	 */
	public static String getFormatedLastYear(DateTimeFormatter formatoAno) {
		return DateTime.now().minusYears(ONE).toString(formatoAno);
	}
	
	/**
	 * Retorna o Mês Atual formatado.
	 * 
	 * @param formatoAno
	 * @return DateTime, DateTimeFormatter
	 */
	public static String getFormatedCurrentMonthNumber(DateTimeFormatter formatoMes) {
		return DateTime.now().toString(formatMonthPatternMM());
	}
	
	/**
	 * Retorna o Mês Atual formatado.
	 * 
	 * @param formatoAno
	 * @return DateTime, DateTimeFormatter
	 */
	public static String getFormatedCurrentMonthName() {
		return DateTime.now().toString(formatMonthPatternMMMMM());
	}
	
	/**
	 * Obtém a Data Atual com formatação padrão
	 * 
	 * @return DateTime, DateTimeFormatter
	 */
	public static String getCurrentDatePatternDDMMYYYY() {
		return DateTime.now().toString(formatDatePatternDDMMYYYY());
	}

	/**
	 * Obtém a Dia Atual com formatação padrão
	 * @return DateTime, DateTimeFormatter
	 */
	public static String getCurrentDayPatternDD() {
		return DateTime.now().toString(formatDayPatternDD());
	}
	
	/**
	 * Obtém a Data Atual com formatação padrão
	 * 
	 * @return DateTime, DateTimeFormatter
	 */
	public static String getCurrentDateWithCustomPattern(String patternOfDate) {
		return DateTime.now().toString(formatDatePtBrWithCustomPattern(patternOfDate));
	}
	
	// RECUPERA DATAS EM FORMATO STRING 
	
	/**
	 * Método que retorna o Ano Atual no formato "yyyy".
	 * 
	 * @return String
	 */
	public static String getStringCurrentYear() {
		String year = EMPTY;
		year = year + getCurrentYear();
		return year;
	}	
	
	/**
	 * Método que retorna o Mês Atual no formato "MM".
	 * 
	 * @return String
	 */
	public static String getStringCurrentMonth() {
		String month = EMPTY;
		month = month + DateTime.now().getMonthOfYear();
		return month;
	}

	/**
	 * Método que retorna o Dia Atual no formato "dd".
	 * 
	 * @return String
	 */
	public static String getStringCurrentDay() {
		String day = EMPTY;
		day = day + DateTime.now().getDayOfMonth();
		return day;
	}
	
	
	// FORMATADORES DE DATA
	
	/**
	 * Define a formatação do ano com o padrão "yyyy".
	 * 
	 * @return DateTimeFormatter
	 */
	public static DateTimeFormatter formatYearPatternYYYY() {
		DateTimeFormatter yearFormat = DateTimeFormat.forPattern(PatternPRM.Date.FORMAT_DATE_YYYY).withLocale(new Locale("pt", "BR"));
		return yearFormat;
	}
	
	/**
	 * Define a formatação do mês com o padrão "MM".
	 * 
	 * @return DateTimeFormatter
	 */
	public static DateTimeFormatter formatMonthPatternMM() {
		DateTimeFormatter monthFormat = DateTimeFormat.forPattern(PatternPRM.Date.FORMAT_DATE_MM).withLocale(new Locale("pt", "BR"));
		return monthFormat;
	}
	
	/**
	 * Define a formatação por extenso do mês com o padrão "MMMMM".
	 * 
	 * @return DateTimeFormatter
	 */
	public static DateTimeFormatter formatMonthPatternMMMMM() {
		DateTimeFormatter monthFormat = DateTimeFormat.forPattern(PatternPRM.Date.FORMAT_DATE_MMMMM).withLocale(new Locale("pt", "BR"));
		return monthFormat;
	}
	
	/**
	 * Retorna a data atual formatada como dd/MM/yyyy.
	 * 
	 * @return DateTimeFormatter
	 */
	public static DateTimeFormatter formatDatePatternDDMMYYYY() {
		DateTimeFormatter dateFormat = DateTimeFormat.forPattern(PatternPRM.Date.FORMAT_DATE_DDMMYYYY).withLocale(new Locale("pt", "BR"));
		return dateFormat;
	}	

	/**
	 * Retorna a dia atual formatada como dd.
	 * 
	 * @return DateTimeFormatter
	 */
	public static DateTimeFormatter formatDayPatternDD() {
		DateTimeFormatter dateFormat = DateTimeFormat.forPattern(PatternPRM.Date.FORMAT_DATE_DD).withLocale(new Locale("pt", "BR"));
		return dateFormat;
	}	
	
	/**
	 * Define o locale para pt-BR em relação ao padrão de formatação de data passado pelo parâmetro.
	 * 
	 * @param patternOfDate
	 * @return DateTimeFormatter
	 */
	public static DateTimeFormatter formatDatePtBrWithCustomPattern(String patternOfDate) {
		return DateTimeFormat.forPattern(patternOfDate).withLocale(new Locale("pt", "BR"));
	}

	// RECUPERA PERÍODOS DE TEMPO FORMATADOS
	public static String getCurrentPeriodPatternHHMMSS() {
		return DateTime.now().toString(formatPeriodPatternHHMMSS());
	}

	// RECUPERA PERÍODOS DE TEMPO FORMATADOS
	public static String getCurrentPeriodPatternDDMMYYYYHHMMSS() {
		return DateTime.now().toString(formatPeriodPatternDDMMYYYYHHMMSS());
	}
	
	// FORMATADORES DE TEMPO	
	public static DateTimeFormatter formatPeriodPatternDDMMYYYYHHMMSS() {
		DateTimeFormatter timeFormat = DateTimeFormat.forPattern(PatternPRM.Period.FORMAT_TIME_DDMMYYYYHHMMSS).withLocale(new Locale("pt", "BR"));
		return timeFormat;
	}
	
	public static DateTimeFormatter formatPeriodPatternHHMMSS() {
		DateTimeFormatter timeFormat = DateTimeFormat.forPattern(PatternPRM.Period.FORMAT_TIME_HHMMSS).withLocale(new Locale("pt", "BR"));
		return timeFormat;
	}	
		
}
