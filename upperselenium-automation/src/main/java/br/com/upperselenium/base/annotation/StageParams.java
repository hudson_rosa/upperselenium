package br.com.upperselenium.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface StageParams {
	
	/**
	 * Define o objetivo do Fluxo de teste.
	 * @return
	 */
	String goal() default "";
	
	/**
	 * Define a classe da Suite de Testes deste Fluxo.
	 * @return
	 */
	Class<?> suiteClass() default Object.class;
} 
