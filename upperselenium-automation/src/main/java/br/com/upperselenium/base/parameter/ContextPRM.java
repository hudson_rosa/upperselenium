package br.com.upperselenium.base.parameter;

/**
 * Interface de Constantes: As constantes não devem ser abreviadas e/ou devem possuir o mesmo nome correspondente ao seu valor,
 * ignorando-se apenas caracteres especiais (com possibilidades de nomenclatura por extenso para a constante).
 *
 * Ex: String SOBRE_NOS = "Sobre Nós";
 * Ex: String _200_GRAUS_CELSIUS = "200°C";
 * 
 * @author Hudson
 *
 */
public interface ContextPRM {

	public static final String FLAG_DELETE_REPORTS_LIST = "flagDeleteReportsList";
	public static final String TEST_RESULT_LIST = "testResultList";
	public static final String PAGE_PERFORMANCE_LIST = "pagePerformanceList";
	public static final String PAGE_PERFORMANCE_AUX_LIST = "pagePerformanceAuxList";
	public static final String PAGE_PERFORMANCE_RANKING_LIST = "pagePerformanceRankingList";

}
