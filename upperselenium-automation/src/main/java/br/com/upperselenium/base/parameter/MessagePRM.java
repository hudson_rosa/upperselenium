package br.com.upperselenium.base.parameter;

/**
 * Interface de Constantes: As constantes não devem ser abreviadas e/ou devem possuir o mesmo nome correspondente ao seu valor,
 * ignorando-se apenas caracteres especiais (com possibilidades de nomenclatura por extenso para a constante).
 *
 * Ex: String SOBRE_NOS = "Sobre Nós";
 * Ex: String _200_GRAUS_CELSIUS = "200°C";
 * 
 * @author Hudson
 *
 */
public interface MessagePRM {

	String SCREENSHOT_OBTAINED = "Screenshot da tela foi obtido!";
	
	interface AsException {
		String ALERT_IS_NOT_PRESENT = "Alerta não está presente para ser clicado!";
		String ALERT_NOT_EXPECTED = "Um alerta não esperado foi exibido no navegador por não identificar um campo aguardado pelo teste!\n"
				+ "Isto impediu que os testes subsequentes fossem executados.";
		String ELEMENT_NOT_DISPLAYABLE = "Elemento não encontrado ou não está visível!";
		String ELEMENT_NOT_ENABLED = "Elemento não está disponível!";
		String ELEMENT_NOT_INTERABLE = "Elemento não encontrado ou não interável durante a interpretação de seu Xpath.";
		String ELEMENT_NOT_PRESENT = "Elemento não está presente!";
		String ELEMENT_NOT_SELECTABLE = "Elemento não está selecionável!";
		String ENCODING_NOT_SUPPORTED = "Encoding não suportado ao gerar a criptografia!";
		String EXECUTION_SUITE_ERROR = "Erro na execução das Suites! Possível falha nas configurações de execução ou há incompatibilidade com o Webdriver!";
		String DATAPROVIDER_EXCEPTION = "Não foi possível obter o arquivo DataProvider!";
		String DEFAULT_WAIT = "Impossível obter o tempo padrão de espera do Webdriver através do valor definido!";
		String IMAGE_FILE_NOT_CREATED = "Impossível criar o arquivo da imagem da tela do browser.";
		String INSTANTIATION_PAGE_ERROR = "O objeto da Page Object especificado não pode ser instanciado!";
		String IO_CAPABILITIES_ERROR = "Erro ao definir as Capabilities ao WebDriver!";
		String IO_ERROR = "Erro de I/O sobre o arquivo ";
		String IO_FATAL_ERROR = "Erro fatal de I/O sobre o arquivo ";
		String IO_REPORT_NOT_FOUND = "Erro ao criar o Html Report, pois não foi possível encontrar o caminho especificado!";
		String IO_WEBDRIVER_ERROR = "Erro ao obter informações para instanciar o WebDriver!";
		String ISOLATED_SUITE_EXECUTION_ERROR = "Erro ao tentar executar a Suite isoladamente!";
		String FILE_EXCLUSION_ERROR = "Não foi possível excluir o(s) arquivo(s) em: ";
		String FILE_NOT_UPDATED = "Arquivo não foi alterado!";
		String GET_JSON_EXCEPTION = "Não foi possível obter o arquivo Json!";
		String MONTH_NOT_FOUND = "O mês não foi definido corretamente e/ou não foi encontrado para a seleção da data.";
		String OBSERVER_EXCEPTION = "Impossível executar os resultados finais das Suites da classe 'ObserverFinalResultsListener.java'!";
		String PERFORMANCE_RESULTS_NOT_CALCULATED = "Não foi possível calcular a divisão por zero ao carregar a página!";
		String PROPERTIES_CKECKING_EXCEPTION = "Ocorreu um erro ao verificar os dados do arquivo de properties!";
		String PROBLEM_WHEN_ENCRYPTING = "Ocorreu uma erro ao encryptar a mensagem fornecida no gerador de dados randômicos!";
		String RANDOM_DATA_PROBLEM = "Problema ao processar o algorítmo de criptografia!";
		String REFLECTION_PAGE_ERROR = "Não foi possível acessar reflexivamente as definições da Page Object (campos, métodos, construtor, etc.)!";
		String RESULTS_NOT_CALCULATED = "Os resultados dos testes não puderam ser obtidos!";
		String SESSION_NOT_CLOSED = "A sessão do WebDriver não foi encerrada corretamente!";
		String STAGE_EXCEPTION = "EXCEÇÃO LANÇADA NESTA STAGE!";
		String TIMEOUT_EXCEPTION = "Timeout na execução do teste. Parâmetros 'timeWaitingDefault' teve seu tempo excedido";
		String VALUE_NOT_FOUND = "O valor não foi encontrado.";
		String WAIT_IMPLICIT = "Impossível aguardar pelo elemento de forma implícita!";
		String WAIT_FOR_A_TIME = "Impossível aguardar pelo elemento!";
		String WAIT_FOR_PRESENT_ALERT = "Impossível aguardar pelo Alert. Ele parece não estar presente!";
		String WAIT_FOR_PRESENT_ELEMENT = "Elemento parece não estar presente para ser utilizado!";
		String WAIT_FOR_CLICKABLE_ELEMENT = "Elemento parece não estar presente ou visível para ser clicado!";
		String WAIT_FOR_PRESENT_PAGE = "Impossível aguardar pela página referida. Ela parece não estar sendo carregada!";
		String WRITE_JSON_EXCEPTION = "Não foi possível escrever no arquivo Json!";
		String XPATH_ERROR = "O XPath definido não pode ser interpretdo corretamente!";
	}

}
