package br.com.upperselenium.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface SuiteParams {
	
	/**
	 * Descreve as características comuns entre os Flow tests da Suite.
	 * 
	 * @return
	 */
	String description() default "";
} 
