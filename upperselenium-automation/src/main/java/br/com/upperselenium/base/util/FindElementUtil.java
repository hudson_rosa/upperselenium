package br.com.upperselenium.base.util;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriverException;

import br.com.upperselenium.base.WebDriverMaster;
import br.com.upperselenium.base.parameter.TimePRM;

/**
 * Classe utilitária para validação de elementos manipuláveis.
 * 
 * @author Hudson
 */
public class FindElementUtil {
		
	private FindElementUtil() {}
	
	/**
	 * Define se o elemento passado por parâmetro está sendo exibido ou não.
	 * 
	 * @param byElement
	 * @return
	 */
	public static Boolean isDisplayedElement(By byElement) {
		try {
			WebDriverMaster.getWebDriver().findElement(byElement).isDisplayed();
			return Boolean.TRUE;
		} catch (WebDriverException e) {
			return Boolean.FALSE;
		}
	}
	
	/**
	 * Define se o elemento está habilitado para interação ou não.
	 * 
	 * @param byElement
	 * @return
	 */
	public static Boolean isEnabledElement(By byElement) {
		try {
			WebDriverMaster.getWebDriver().findElement(byElement).isEnabled();
			return Boolean.TRUE;
		} catch (WebDriverException e) {
			return Boolean.FALSE;
		}
	}

	/**
	 * Define se o elemento está selecionável ou não.
	 * 
	 * @param byElement
	 * @return
	 */
	public static Boolean isSelectedElement(By byElement) {
		try {
			WebDriverMaster.getWebDriver().findElement(byElement).isSelected();
			return Boolean.TRUE;
		} catch (WebDriverException e) {
			return Boolean.FALSE;
		}
	}

	public static void acceptAlert() {
		long waitForAlert = System.currentTimeMillis() + TimePRM._3_SECS;
		boolean boolFound = false;
		do {
			try {
				Alert alert = WebDriverMaster.getWebDriver().switchTo().alert();
				if (alert != null) {
					alert.accept();
					boolFound = true;
				}
			} catch (NoAlertPresentException enap) {}
		} while ((System.currentTimeMillis() < waitForAlert) && (!boolFound));
	}

	public static void cancelAlert() {
		long waitForAlert = System.currentTimeMillis() + TimePRM._3_SECS;
		boolean boolFound = false;
		do {
			try {
				Alert alert = WebDriverMaster.getWebDriver().switchTo().alert();
				if (alert != null) {
					alert.dismiss();
					boolFound = true;
				}
			} catch (NoAlertPresentException enap) {}
		} while ((System.currentTimeMillis() < waitForAlert) && (!boolFound));
	}
		
}
		
