package br.com.upperselenium.base.exception;

public class RunningException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	/**
	 * Sobre as declarações do bloco catch, utilizar preferencialmente:
	 *
	 * Exemplo:
	 *      [...]
	 * 		} catch (Throwable t) {
	 *			throw new RunningException(BaseLogger.logException(MessagePRM.AsException.[NOME_DA_CONSTANTE]), t);
	 *		}
	 * 
	 * @param message
	 * @param exceptionCause
	 */
	public RunningException(String message, Throwable exceptionCause) {
		super(message, exceptionCause);
	}
}
