package br.com.upperselenium.base.parameter;

/**
 * Interface de Constantes: As constantes não devem ser abreviadas e/ou devem possuir o mesmo nome correspondente ao seu valor,
 * ignorando-se apenas caracteres especiais (com possibilidades de nomenclatura por extenso para a constante).
 *
 * Ex: String SOBRE_NOS = "Sobre Nós";
 * Ex: String _200_GRAUS_CELSIUS = "200°C";
 * 
 * @author Hudson
 *
 */
public interface KeyPRM {

	interface PropertyKey {
		
		String BROWSER_TYPE = "browserType";
		String BROWSER_NAME = "browserName";
		String DELETE_ALL_REPORTS = "deleteAllReports";
		String GRID_HUB = "grid.url";
		String GRID2_HUB = "grid2.hub";
		String REPORT_DIRECTORY = "reportDirectory";
		String SHOW_CONTEXT_VARIABLES = "showContextVariables";
		String SITE_URL = "site.url";
		String TIME_WAITING_DEFAULT = "timeWaitingDefault";
		
	}

	interface PropertyValue {
		
		String CAPABILITIES = "capabilities";
		
	}
}
