package br.com.upperselenium.base.sample.test.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseSuite;
import br.com.upperselenium.base.annotation.SuiteParams;
import br.com.upperselenium.base.sample.test.flow.SampleT0000SignInFlow;

@RunWith(Suite.class)

@SuiteClasses({
	SampleT0000SignInFlow.class
	})

@SuiteParams(description="Test Cases de exemplo para UI, Performance e Rest API")
public class S000SampleSuiteBdd extends BaseSuite {}
