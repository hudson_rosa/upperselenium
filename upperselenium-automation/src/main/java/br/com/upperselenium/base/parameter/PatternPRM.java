package br.com.upperselenium.base.parameter;

/**
 * Interface de Constantes: As constantes não devem ser abreviadas e/ou devem possuir o mesmo nome correspondente ao seu valor,
 * ignorando-se apenas caracteres especiais (com possibilidades de nomenclatura por extenso para a constante).
 *
 * Ex: String SOBRE_NOS = "Sobre Nós";
 * Ex: String _200_GRAUS_CELSIUS = "200°C";
 * 
 * @author Hudson
 *
 */
public interface PatternPRM {

	String FORMAT_DATETIME_DDMMYYYY_HHMMSS = "dd/MM/yyyy - HH:mm:ss";
	String FORMAT_DATETIME_DDMMYYYYHHMMSS = "dd/MM/yyyy HH:mm:ss";
	String FORMAT_DATETIME_DDMMYYYY_AS_HHMMSS = "dd/MM/yyyy às HH:mm:ss";
	String FORMAT_DATETIME_DDMMYYYY_AT_HHMMSS = "dd/MM/yyyy at HH:mm:ss";
	String FORMAT_DATETIME_DD_MM_YYYY_HH_MM_SS = "dd-MM-yyyy_at_HH-mm-ss";
	String FORMAT_DATETIME_DDMMYYYYHHMMSSMILLIS = "ddMMyyyyHHmmssSSS";
	
	interface Date {
		
		String FORMAT_DATE_DD = "dd";		
		String FORMAT_DATE_DDMMYYYY = "dd/MM/yyyy";
		String FORMAT_DATE_MM = "MM";
		String FORMAT_DATE_MMMMM = "MMMMM";
		String FORMAT_DATE_MMYYYY = "MM/yyyy";
		String FORMAT_DATE_MMMMMYYYY = "MMMMM/yyyy";
		String FORMAT_DATE_YYYY = "yyyy";
				
	}
	
	interface Period {
		
		String FORMAT_TIME_DDMMYYYYHHMMSS = "dd/MM/yyyy HH:mm:ss";
		String FORMAT_TIME_HHMMSS = "HH:mm:ss";
		String FORMAT_TIME_HH = "HH";
		String FORMAT_TIME_MM = "mm";
		String FORMAT_TIME_SS = "ss";
		
	}
	
	interface Month {
		
		String JANEIRO = "janeiro";
		String FEVEREIRO = "fevereiro";
		String MARCO = "março";
		String ABRIL = "abril";
		String MAIO = "maio";
		String JUNHO = "junho";
		String JULHO = "julho";
		String AGOSTO = "agosto";
		String SETEMBRO = "setembro";
		String OUTUBRO = "outubro";
		String NOVEMBRO = "novembro";
		String DEZEMBRO = "dezembro";		
		
		String JAN = "jan";
		String FEV = "fev";
		String MAR = "mar";
		String ABR = "abr";
		String MAI = "mai";
		String JUN = "jun";
		String JUL = "jul";
		String AGO = "ago";
		String SET = "set";
		String OUT = "out";
		String NOV = "nov";
		String DEZ = "dez";		
		
	}

}
