package br.com.upperselenium.base.util;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;

import br.com.upperselenium.base.asset.WaitConfig;
import br.com.upperselenium.base.exception.WaitException;
import br.com.upperselenium.base.logger.BaseLogger;
import br.com.upperselenium.base.parameter.MessagePRM;

/**
 * Classe utilitária para definição de espera (wait) sobre o elemento a ser interagido pelo WebDriver.
 * 
 * @author Hudson
 */
public class WaitElementUtil extends BaseLogger{
		
	private WaitElementUtil() {}
	
	/**
	 * Define o tempo de espera implícito do WebDriver na execução do teste.
	 * O tempo pode ser especificado o parâmetro.
	 * 
	 * @param webDriver
	 * @param timeWaitInSeconds
	 */
	public static final void setImplicitlyWait(WebDriver webDriver, long timeWaitInSeconds) {
		try {
			webDriver.manage().timeouts().implicitlyWait(timeWaitInSeconds, TimeUnit.SECONDS);
		} catch (Exception e) {
			throw new WaitException(logException(MessagePRM.AsException.WAIT_IMPLICIT), e);
		}
	}

	/**
	 * Threads aguardam de acordo com o tempo especificado no parâmetro.
	 * 
	 * @param timeWaitInSeconds
	 */
	public static final void waitForATime(long timeWaitInSeconds) {
		try {
			long timeInMillis = timeWaitInSeconds * 1000;
			Thread.sleep(timeInMillis);
		} catch (InterruptedException ie) {
			throw new WaitException(logException(MessagePRM.AsException.WAIT_FOR_A_TIME), ie);
		}
	}

	/**
	 * De acordo com o tempo limite de timeout especificado por parâmetro, o
	 * WebDriver aguarda a presença de algum alert JavaScript na tela.
	 * 
	 * @param webDriver
	 * @param timeWaitInSeconds
	 */
	public static final void waitForAPresentAlert(WebDriver webDriver, long timeWaitInSeconds) {
		try {
			new WebDriverWait(webDriver, timeWaitInSeconds).until(ExpectedConditions.alertIsPresent());
		} catch (Exception e) {
			throw new WaitException(logException(MessagePRM.AsException.WAIT_FOR_PRESENT_ALERT), e);
		}			
	}	

	/**
	 * Caso o elemento (mapeado com By.by) ainda não esteja com seu estado clicável, o WebDriver o aguarda
	 * de acordo com o tempo especificado por parâmetro.
	 * 
	 * @param webDriver
	 * @param by
	 * @param timeWaitInSeconds
	 * @return
	 */
	public static final WebElement waitForElementToBeClickable(WebDriver webDriver, By by, long timeWaitInSeconds) {
		try {
			return new WebDriverWait(webDriver, timeWaitInSeconds).until(ExpectedConditions.elementToBeClickable(by));
		} catch (Exception e) {
			throw new WaitException(logException(MessagePRM.AsException.WAIT_FOR_CLICKABLE_ELEMENT), e);
		}	
	}

	/**
	 * Caso o elemento (mapeado com WebElement) ainda não esteja com seu estado clicável, o WebDriver o aguarda
	 * de acordo com o tempo especificado por parâmetro.
	 * 
	 * @param webDriver
	 * @param webElement
	 * @param timeWaitInSeconds
	 * @return
	 */
	public static final WebElement waitForElementToBeClickable(WebDriver webDriver, WebElement webElement, long timeWaitInSeconds) {
		try {
			return new WebDriverWait(webDriver, timeWaitInSeconds).until(ExpectedConditions.elementToBeClickable(webElement));
		} catch (Exception e) {
			throw new WaitException(logException(MessagePRM.AsException.WAIT_FOR_CLICKABLE_ELEMENT), e);
		}	
	}

	/**
	 * Caso o elemento (mapeado com By.by) ainda não esteja presente na tela, o WebDriver o aguarda
	 * de acordo com o tempo especificado por parâmetro.
	 * 
	 * @param webDriver
	 * @param by
	 * @param timeWaitInSeconds
	 * @return
	 */
	public static final WebElement waitForPresenceOfElementLocated(WebDriver webDriver, By by, long timeWaitInSeconds) {
		try {
			return new WebDriverWait(webDriver, timeWaitInSeconds).until(ExpectedConditions.presenceOfElementLocated(by));
		} catch (Exception e) {
			throw new WaitException(logException(MessagePRM.AsException.WAIT_FOR_PRESENT_ELEMENT), e);
		}	
	}
	
	/**
	 * Aguarda o carregamento de uma página por um tempo determinado, de acordo com o especificado por parâmetro.
	 * 
	 * @param webDriver
	 * @param timeWaitInSeconds
	 */
	public static final void waitForPageToLoad(WebDriver webDriver, long timeWaitInSeconds) {
		try {
			long timeOutInMillis = timeWaitInSeconds * 1000;
			new WaitForPageToLoad().apply(webDriver, new String[] { String.valueOf(timeOutInMillis) });
		} catch (Exception e) {
			throw new WaitException(logException(MessagePRM.AsException.WAIT_FOR_PRESENT_PAGE), e);
		}	
	}
	
	/**
	 * Aguarda o carregamento de uma página a partir do tempo definido no testconfig.properties.
	 * 
	 * @param webDriver
	 */
	public  static final void getTimeoutWhenPageIsLoading(WebDriver webDriver) {
		try {
			webDriver.manage().timeouts().pageLoadTimeout(WaitConfig.setTimeWaitingConfigDefault(), TimeUnit.SECONDS);
		} catch (Exception e) {
			throw new WaitException(logException(MessagePRM.AsException.WAIT_FOR_PRESENT_PAGE), e);
		}	
	}

	/**
	 * Retorna TRUE ou FALSE sobre a verificação de presença do elemento (mapeado com By.by),
	 * de acordo com o tempo especificado por parâmetro.
	 * 
	 * @param webDriver
	 * @param by
	 * @param timeWaitInSeconds
	 * @return
	 */
	public static Boolean isElementPresent(WebDriver webDriver, By by, long timeWaitInSeconds) {
		try {
			waitForPresenceOfElementLocated(webDriver, by, timeWaitInSeconds);
			return Boolean.TRUE;
		} catch (WebDriverException e) {
			BaseLogger.logThrowables(MessagePRM.AsException.ELEMENT_NOT_PRESENT, e);
			return Boolean.FALSE;
		}
	}
		
}
		
