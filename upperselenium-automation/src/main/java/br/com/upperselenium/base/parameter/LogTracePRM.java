package br.com.upperselenium.base.parameter;

/**
 * Interface de Constantes: As constantes não devem ser abreviadas e/ou devem possuir o mesmo nome correspondente ao seu valor,
 * ignorando-se apenas caracteres especiais (com possibilidades de nomenclatura por extenso para a constante).
 *
 * Ex: String SOBRE_NOS = "Sobre Nós";
 * Ex: String _200_GRAUS_CELSIUS = "200°C";
 * 
 * @author Hudson
 *
 */
public interface LogTracePRM {
		
		String BLANK_LINE = "| ";
		String BREAK_LINE = "|__________________________________|";
		String CONTINUOUS_LINE = "---------------------------------------------------------------------------------------------->";
		String EVENTS_TEXT = "|---> ";
		String HIGHLIGHT_HEADER_START = "|--------------------";
		String HIGHLIGHT_HEADER_END = "--------------------|";
		String HIGHLIGHT_TEXT = "|---- ";
}
