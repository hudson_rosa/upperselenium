package br.com.upperselenium.base.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import br.com.upperselenium.base.BaseSuite;
import br.com.upperselenium.base.WebDriverMaster;
import br.com.upperselenium.base.asset.ReportConfig;
import br.com.upperselenium.base.logger.BaseLogger;
import br.com.upperselenium.base.parameter.MessagePRM;
/**
 * Classe utilitária para realização de PrintScreen para um arquivo de imagem ".PNG". 
 * 
 * @author Hudson
 */
public class ScreenshotEvidencesUtil extends BaseLogger{
	
	private static File fileHTML = BaseSuite.getFileHTML();
	private static String pathFile = BaseSuite.getPathAndNameForIndexHTML();

	/**
	 * Método que realiza o print da tela do navegador no momento do teste.
	 * Como retorno, obtém-se o arquivo de saída a ser criado e armazenado no diretório
	 * especificado por parâmetro, bem como o WebDriver.
	 * 
	 * @param webDriver
	 * @param outputDir
	 * @return
	 */
	public static File takeScreenShot(WebDriver webDriver, File outputDir, String fileName) {
		File screenshot = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
		File outputFile = new File(outputDir, fileName + "_Scr" + screenshot.getName().substring(10));
		try {
			FileUtils.copyFile(screenshot, outputFile);
			logInfoTextHighlight(MessagePRM.SCREENSHOT_OBTAINED);
		} catch (IOException e) {
			BaseLogger.logThrowables(MessagePRM.AsException.IMAGE_FILE_NOT_CREATED, e);
			return null;
		}
		return outputFile;
	}
	
	public static void takeDetachedScreenshot() {
		File outputEvidenceDirectory = new File(ReportConfig.getEvidenceDirectory());
		String fileEvidenceName = "DetachedPage";		
		File takeScreenShot = ScreenshotEvidencesUtil.takeScreenShot(WebDriverMaster.getWebDriver(), outputEvidenceDirectory, fileEvidenceName);
		ReportConfig.appendTag(fileHTML, ReportConfig.writeImgTag(ReportConfig.getAncestorDirectory() 
				+ takeScreenShot.getPath(), null, null, "screenshotEventFiring", "Página da aplicação"), pathFile);
	}
	
}