package br.com.upperselenium.base.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface FlowParams {
	
	/**
	 * Define o ID para o Fluxo de Teste.
	 * @return
	 */
	String idTest() default "";

	/**
	 * Define o path do dataprovider utilizado para Login da aplicação a partir de "src/test/resources/.".
	 * @return
	 */
	String loginDirPath() default "";
	
	/**
	 * Define o path do dataprovider até o nível do(s) arquivo(s) "**DP.json", a partir de "src/test/resources/.".
	 * @return
	 */
	String testDirPath() default "";
	
	/**
	 * Define o objetivo do Fluxo de teste.
	 * @return
	 */
	String goal() default "";
	
	/**
	 * Define a classe da Suite de Testes deste Fluxo.
	 * @return
	 */
	Class<?> suiteClass() default Object.class;
} 
