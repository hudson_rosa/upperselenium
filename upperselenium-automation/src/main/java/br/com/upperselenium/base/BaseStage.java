package br.com.upperselenium.base;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.joda.time.DateTime;
import org.openqa.selenium.JavascriptExecutor;

import br.com.upperselenium.base.asset.ReportConfig;
import br.com.upperselenium.base.entity.PageLoaded;
import br.com.upperselenium.base.entity.PerformanceData;
import br.com.upperselenium.base.exception.RunningException;
import br.com.upperselenium.base.logger.BaseLogger;
import br.com.upperselenium.base.logger.TestLogger;
import br.com.upperselenium.base.parameter.EnumContextPRM;
import br.com.upperselenium.base.parameter.MessagePRM;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.base.util.DPOUtil;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.base.util.JSONUtil;
import br.com.upperselenium.base.util.WaitElementUtil;
import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

/**
 * Classe Base utilizada a cada chamada de uma Stage do Flow,
 * ocorrendo o carregamento dos DPOs e JSon (massa de dados), bem como a
 * execução dos métodos correspondentes às Pages envolvidas.
 * 
 * @author Hudson
 */
public abstract class BaseStage extends DPOUtil implements IBaseStage, TestLogger {
	
	private static List<PageLoaded> pages = new ArrayList<PageLoaded>();
	private static List<PageLoaded> newList = new ArrayList<PageLoaded>();
	private static String fileSufix; 	
	private static PerformanceData pagesTimeLoadingList = new PerformanceData();
	
	private static int counterItemStage = 1;

	public static int getCounterItemStage() {
		return counterItemStage++;
	}

	public static int getCounterItemStageRestartedOnList() {
		return counterItemStage = 1;
	}

	// DEFAULT WAITINGS
    public static void waitForAPresentAlert(int timeWaitInSeconds) {
    	WaitElementUtil.waitForAPresentAlert(WebDriverMaster.getWebDriver(), TimePRM._10_SECS);
    }

    public static void waitForPageToLoad(int timeWaitInSeconds) {
    	WaitElementUtil.waitForPageToLoad(WebDriverMaster.getWebDriver(), timeWaitInSeconds);
    }

    public static void waitForPageToLoadUntil10s() {
    	WaitElementUtil.waitForPageToLoad(WebDriverMaster.getWebDriver(), TimePRM._10_SECS);
    }

    // RANDOM STRINGS AND NUMBERS
    public static String getRandomNumber() {
    	String random = BaseContext.getParameter(EnumContextPRM.RANDOM_NUMBER.getValue()).toString();
		return random;    	
    }
    
    public static String getRandomNumberSpaced() {
    	String random = " " + BaseContext.getParameter(EnumContextPRM.RANDOM_NUMBER.getValue()).toString();
    	return random;    	
    }
    
    public static String getRandomStringSpaced() {
    	String random = " " + BaseContext.getParameter(EnumContextPRM.RANDOM_CRYPTED_STRING.getValue()).toString();    	
		return random;
    }
    
    public static String getRandomStringNonSpaced() {
    	String random = BaseContext.getParameter(EnumContextPRM.RANDOM_CRYPTED_STRING.getValue()).toString();    	
    	return random;
    }
    
    public static String getRandomSHA1NonSpaced() {
    	String random = BaseContext.getParameter(EnumContextPRM.RANDOM_PURE_SHA1_STRING.getValue()).toString();    	
    	return random;
    }

    public static String getRandomStringBySize(int sizeString) {
    	String random = BaseContext.getParameter(EnumContextPRM.RANDOM_CRYPTED_STRING.getValue()).toString();    	
    	return random.substring(0, sizeString);
    }

    public static String getRandomSHA1Spaced() {
    	String random = " " + BaseContext.getParameter(EnumContextPRM.RANDOM_PURE_SHA1_STRING.getValue()).toString();    	
    	return random;
    }

    /**
     * Método utilizado para reconhecer as issues definidas no DP.
     * @param issueValue
     */
    public static void getIssue(String issueValue) {
    	if(StringUtils.isNotBlank(issueValue) || StringUtils.isNotEmpty(issueValue)){
	    	BaseReport.reportContentOfIssueLink(issueValue);
    	}
    }

	/**
	 * Método utilizado para gerar logs da Stage atual em execução.
	 */
	public void getCurrentStage() {
		Class<? extends BaseStage> currentStage = this.getClass();
		String flowNumberNow = BaseContext.getParameter(EnumContextPRM.FLOW_NUMBER.getValue()).toString();
		BaseContext.setParameter(EnumContextPRM.PAGE_PERFORMANCE_STAGE.getValue(), currentStage.getSimpleName());
		int stageItemNow = getCounterItemStage();
		String itemFlowAndStage = flowNumberNow + "." + stageItemNow;
		BaseReport.reportContentFromStageDescriptions(currentStage, flowNumberNow, stageItemNow, itemFlowAndStage);		
	}

	/**
	 * Carrega dados do Data Provider (Path e nome do arquivo JSon). <br>
	 * É necessário definir os paths adequadamente nas constantes no arquivo StagePRM e, <br>
	 * em seguida, inserir a constante como parâmetro String da nova instância adicionada no Flow. <br><br>
	 * 
	 * <b>CLASSE "ExemploTesteFlow.java":</b><br><br>
	 * Ex.:<br>
	 * [...]<br>
	 * addStage(new ExemploStage(<b>StagePRM.DataProviderFile.EXEMPLO_DP_JSON</b>)); <br>
	 * [...]<br><br>
	 * 
	 * <b>CLASSE "Exemplo2Flow.java":</b><br><br>
	 * Ex.:<br>
	 * [...]<br>
	 * addStage(new ExemploStage(<b>&lt;primeiraStringPathJsonDP&gt;,&lt;segundaStringPathJsonDP&gt;</b>)); <br>
	 * [...]<br><br>
	 *  
	 * O construtor de 'ExemploStage.java' deve estar implementado conforme segue:
	 * 
	 * <b>CLASSE "Exemplo2Stage.java":</b><br><br>
	 * <i>
	 * 		public LoginStage(String <u>dataProviderPathFile1</u>, String <u>dataProviderPathFile2</u>) { <br>
	 *		&nbsp;&nbsp;&nbsp;	<b>primeiroDPO</b> = loadDataProviderFile(<b>ExemploDPO.class</b>, <u>dataProviderPathFile1</u>); <br>
	 *		&nbsp;&nbsp;&nbsp;	<b>segundoDPO</b> = loadDataProviderFile(<b>OutroExemploDPO.class</b>, <u>dataProviderPathFile2</u>); <br>
	 *		&nbsp;&nbsp;&nbsp;	initElementsFromPage(); <br>
	 *      }<br>
	 *  </i>
	 */
	public <D> D loadDataProviderFile(Class<D> dpoClass, String dataProviderPathFile) {
		D dataProvider = getDataProvider(dataProviderPathFile, dpoClass);
		return dataProvider;
	}
	
	/**
	 * Carrega uma ou mais Pages referentes à Stage para inicializar os fields <br>
	 * com ou sem a marcação @FindBy. Para isto é necessário apenas invocar o método que
	 * inicializa a instancia da classe Page passada por parâmetro, de acordo <br> 
	 * com o exemplo abaixo:<br>
	 * <br>
	 * 
	 * <b>CLASSE "ExemploStage.java":</b><br>
	 * <br>
	 * <i> ExemploPage exPage;<br>
	 * <br>
	 * 
	 * public void initElementsFromPage() {<br>
	 * &nbsp;&nbsp;&nbsp; exPage = initElementsFromPage(ExemploPage.class);<br>
	 * }<br>
	 * </i>
	 */
	public <E extends BasePage> E initElementsFromPage(Class<E> pageClass) {
		try {
			E newPageClass = pageClass.newInstance();
			newPageClass.initPageElements();
			return newPageClass;
		} catch (InstantiationException e) {
			throw new RunningException(BaseLogger.logException(MessagePRM.AsException.INSTANTIATION_PAGE_ERROR), e);
		} catch (IllegalAccessException e) {
			throw new RunningException(BaseLogger.logException(MessagePRM.AsException.REFLECTION_PAGE_ERROR), e);
		}		
	}
	
	/**
	 * Thead para realizar ação simultânea de validação de mensagem enquanto
	 * ocorrem outras interações no navegador.
	 * 
	 * @param pageClass
	 * @param operationMessage
	 */
	public void paralelizeOperationMessage(final BaseHelperPage page, final String messageDPO){
		new Thread() {
			public void run() {
				try {
					waitForPageToLoad(TimePRM._10_SECS);
					page.validateMessageDefault(messageDPO);
				} catch (Exception e) {}
			}
		}.start();
	}
	
	/**
	 * Thead para realizar ação simultânea de Alert enquanto
	 * ocorrem outras interações no navegador.
	 * 
	 * @param pageClass
	 * @param operationMessage
	 */
	public void paralelizeOperation(){
		new Thread() {
			public void run() {
				try {
					FindElementUtil.acceptAlert();
				} catch (Exception e) {}
			}
		}.start();
	}
	
	/**
	 * Este método é implementado pelo BaseFlow.
	 */
	public void runCompleteStage() {
		initMappedPages();
		getCurrentStage();
		runStage();
		runValidations();
	}
	
	/**
	 * Este método realiza a verificação e tria dados das URLs considerando-se a maior degradação de tempo no carregamento da página.
	 */
	public static void calcPageLoadPerformanceDegradation() {
		StopWatch pageLoad = new StopWatch();
		JavascriptExecutor js = (JavascriptExecutor) WebDriverMaster.getWebDriver();
		try {
			pageLoad.start();
			double loadTimeSecs = (double) js.executeScript(
					"return (window.performance.timing.loadEventEnd - window.performance.timing.navigationStart) / 1000");
			pageLoad.stop();
			PageLoaded page = new PageLoaded();
			if (pageLoad.isStopped()) {
				page.setCurrentStage(
						BaseContext.getParameter(EnumContextPRM.PAGE_PERFORMANCE_STAGE.getValue()).toString());
				page.setCurrentUrl(BasePage.getCurrentPage());
				page.setTimePageLoadSec(loadTimeSecs);
				BaseReport.reportContentOfUrl("<a href=" + page.getCurrentUrl() + " target=\"_blank\">"
						+ page.getCurrentUrl() + "</a>: " + page.getTimePageLoadSec() + " segundos");
				newList = new ArrayList<>();
				if (pages.size() == 0) {
					pages.add(page);
				}
				for (int i = 0; i < pages.size(); i++) {
					if ((page.getCurrentUrl()).equals(pages.get(i).getCurrentUrl())) {
						if ((Double) page.getTimePageLoadSec() >= (Double) pages.get(i).getTimePageLoadSec()) {
							pages.set(i, page);
						}
						break;
					} else if ((i + 1) > pages.size() - 1) {
						newList.add(page);
					}
				}
				if (newList.size() > 0) {
					pages.addAll(newList);
				}
			}
		} catch (Throwable failStage) {
			pageLoad.stop();
			throw new RunningException(
					BaseLogger.logException(MessagePRM.AsException.PERFORMANCE_RESULTS_NOT_CALCULATED), failStage);
		}
	}

	public static void createJSONPagesTimeLoadingFile() {
		String jsonFile = ReportConfig.getReportDirectory()+"PagePerformance_"+getSufixDay()+"_"+getRandomStringNonSpaced()+".json";
		BaseContext.setParameter(EnumContextPRM.PAGE_PERFORMANCE_FILE.getValue(),jsonFile);		
		pagesTimeLoadingList.setDegradationList(pages);				
		JSONUtil.createJSON(jsonFile, pagesTimeLoadingList);
		BaseLogger.logInfoTextHighlight("Os dados de performance das páginas foi salvo com sucesso!");
		BaseLogger.logInfoTextHighlight("Verifique o arquivo: '" + jsonFile + "'");
	}
	
	/**
	 * Método para montar a estrutura de data para o nome do arquivo JSON.
	 * @return
	 */
	public static String getSufixDay() {
		String hif = "_";						
		String day = normalizeTimeStrings(DateTime.now().dayOfMonth().getAsString());		
		String month = normalizeTimeStrings(DateTime.now().monthOfYear().getAsString());		
		String year = normalizeTimeStrings(DateTime.now().year().getAsString());				
		fileSufix = day + hif + month + hif + year; 
		return fileSufix;
	}
	
	/**
	 * Método para montar a estrutura de hora para o nome do arquivo JSON.
	 * @return
	 */
	public static String getSufixTime() {
		String hif = "_";						
		String hour = normalizeTimeStrings(DateTime.now().hourOfDay().getAsString());		
		String minute = normalizeTimeStrings(DateTime.now().minuteOfHour().getAsString());		
		String seconds = normalizeTimeStrings(DateTime.now().secondOfMinute().getAsString());		
		fileSufix = hif + hour + hif + minute + hif + seconds; 
		return fileSufix;
	}

	private static String normalizeTimeStrings(String timeType){
		if (timeType.length() <= 1){
			return "0" + timeType;
		}
		return timeType;
	}	
	
}
