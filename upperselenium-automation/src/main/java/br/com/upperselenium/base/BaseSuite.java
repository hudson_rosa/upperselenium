package br.com.upperselenium.base;

import java.io.File;
import java.io.IOException;

import org.joda.time.DateTime;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import br.com.upperselenium.base.asset.ReportConfig;
import br.com.upperselenium.base.exception.RunningException;
import br.com.upperselenium.base.logger.BaseLogger;
import br.com.upperselenium.base.logger.TestLogger;
import br.com.upperselenium.base.parameter.EnumContextPRM;
import br.com.upperselenium.base.parameter.MessagePRM;
import br.com.upperselenium.base.util.JodaTimeUtil;

/**
 * Classe Base que prepara a execução dos Fluxos associados à Suite.
 * 
 * @author HudsonRosa
 *
 */
public abstract class BaseSuite extends BaseLogger implements TestLogger {
	private static String ALL_SUITES = "1";
	private static String runningAllSuites;
	private static String dateForHTMLFileName;
	private static String dateForExecutedTest;
	private static String pathHtmlFile = ReportConfig.getReportDirectory() + "SuiteReport_" + getNameHTMLFile() + "_" + BaseStage.getRandomStringNonSpaced();
	private static File htmlFile = new File(pathHtmlFile);
		
		
	/**
	 * Método para montar a estrutura de data e hora para o nome do arquivo de
	 * Report HTML.
	 * @return
	 */
	public static String getNameHTMLFile() {
		String hif = "-";						
		String day = normalizeTimeStrings(DateTime.now().dayOfMonth().getAsString());		
		String month = normalizeTimeStrings(DateTime.now().monthOfYear().getAsString());		
		String year = normalizeTimeStrings(DateTime.now().year().getAsString());		
		String hour = normalizeTimeStrings(DateTime.now().hourOfDay().getAsString());		
		String minute = normalizeTimeStrings(DateTime.now().minuteOfHour().getAsString());		
		String seconds = normalizeTimeStrings(DateTime.now().secondOfMinute().getAsString());		
		
		dateForHTMLFileName = day + hif + month + hif + year + "_at_" + hour + hif + minute + hif + seconds; 
		dateForExecutedTest = JodaTimeUtil.getCurrentDatePatternDDMMYYYY() + " às " + JodaTimeUtil.getCurrentPeriodPatternHHMMSS(); 
		return dateForHTMLFileName;
	}

	private static String normalizeTimeStrings(String timeType){
		if (timeType.length() <= 1){
			return "0" + timeType;
		}
		return timeType;
	}
	
	public static void setNameHTMLFile(String nameHTMLFile) {
		BaseSuite.dateForHTMLFileName = nameHTMLFile;
	}	
	
	public static String getDateForExecutedTest() {
		return dateForExecutedTest;
	}

	public static String getPathAndNameForIndexHTML() {
		return pathHtmlFile;
	}

	public static File getFileHTML() {
		return htmlFile;
	}

	/**
	 * ---------------------------------------------
	 * Executa as pré-condições da suite, tais como:
	 * <br> 
	 * <ul>
	 * 		<li>Deletar arquivos de report antigos</li>
	 * 		<li>Preparar o Webdriver com as capabilities</li>
	 * 		<li>Gerar logs de inicialização do teste</li>
	 * </ul>
	 * 
	 * @throws IOException
	 */
	@BeforeClass
	public static void setUpClass() throws IOException {
		flagStartWebdriver();
	}

	private static void flagStartWebdriver() {
		try {
			String isNullFlagRunAll =(String) BaseContext.getParameter(EnumContextPRM.FLAG_RUN_ALL.getValue());
			if (isNullFlagRunAll == null){
				BaseContext.setParameter(EnumContextPRM.START_TIME_TO_SUITE_EXECUTION.getValue(), JodaTimeUtil.getCurrentPeriodPatternDDMMYYYYHHMMSS());
				BaseContext.setParameter(EnumContextPRM.FLAG_RUN_ALL.getValue(),"0");
				runningAllSuites = BaseContext.getParameter(EnumContextPRM.FLAG_RUN_ALL.getValue()).toString();
				initWebDriver();
			}
		} catch (Exception e) {
			throw new RunningException(BaseLogger.logException(MessagePRM.AsException.ISOLATED_SUITE_EXECUTION_ERROR), e);
		}
	}

	private static void initWebDriver() {
		if (!ALL_SUITES.equals(runningAllSuites)){
			ReportConfig.deleteOldReportFiles();
			WebDriverMaster.initWebDriverSession();
		}
	}
	
	/**
	 * ---------------------------------------------
	 * Executa as pós-condições da suite, tais como: 
	 * <br>
	 * <ul>
	 * 		<li>Finalizar a instância do WebDriver</li>
	 * 		<li>Gerar reports no arquivo "suiteReport.html"</li>
	 * </ul>
	 * 
	 * @throws IOException
	 */	
	@AfterClass
	public static void tearDownClass() {		
		logInfoWithContinuousLine("----------------");
		logInfoFirstOrLastHeaderTitle("SUITE FINALIZADA");
		logInfoWithContinuousLine("----------------");
		diffForRuntimeExecution();
		closeWebDriver();
		logInfoSimpleText("Confira os resultados das Evidências da Suite no REPORT:");
		logInfoSimpleText(pathHtmlFile+".html");
	}

	private static void closeWebDriver() {
		runningAllSuites = BaseContext.getParameter(EnumContextPRM.FLAG_RUN_ALL.getValue()).toString();
		if (!ALL_SUITES.equals(runningAllSuites)){
			WebDriverMaster.destroyWebDriverSession();
		}
	}

	/**
	 * Método para calcular o tempo total da execução da Suite e ajustar as
	 * variáveis de contexto.
	 */
	private static void diffForRuntimeExecution() {
		BaseContext.setParameter(EnumContextPRM.FINISHED_TIME_FOR_SUITE.getValue(), JodaTimeUtil.getCurrentPeriodPatternDDMMYYYYHHMMSS());
		String startSuite = BaseContext.getParameter(EnumContextPRM.START_TIME_TO_SUITE_EXECUTION.getValue()).toString(); 
		String endSuite = BaseContext.getParameter(EnumContextPRM.FINISHED_TIME_FOR_SUITE.getValue()).toString();
		BaseContext.setParameter(EnumContextPRM.TOTAL_TIME_EXECUTION.getValue(), JodaTimeUtil.getDurationTimeDDHHMMSS(startSuite, endSuite));
		
		BaseStage.createJSONPagesTimeLoadingFile();
	}
		
}