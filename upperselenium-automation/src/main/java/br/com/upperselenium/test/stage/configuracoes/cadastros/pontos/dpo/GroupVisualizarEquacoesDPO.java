package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import java.util.List;

public class GroupVisualizarEquacoesDPO {

	private String labelVigenciaEquacao;
	private String labelSequenciaDeCalculo;
	private List<GridVisualizarEquacoesDPO> gridVisualizarEquacoes;
	private String copiarEmNovaEquacao;
	private String editar;
	private String remover;

	public String getLabelVigenciaEquacao() {
		return labelVigenciaEquacao;
	}

	public void setLabelVigenciaEquacao(String labelVigenciaEquacao) {
		this.labelVigenciaEquacao = labelVigenciaEquacao;
	}

	public String getLabelSequenciaDeCalculo() {
		return labelSequenciaDeCalculo;
	}

	public void setLabelSequenciaDeCalculo(String labelSequenciaDeCalculo) {
		this.labelSequenciaDeCalculo = labelSequenciaDeCalculo;
	}

	public List<GridVisualizarEquacoesDPO> getGridVisualizarEquacoes() {
		return gridVisualizarEquacoes;
	}

	public void setGridVisualizarEquacoes(List<GridVisualizarEquacoesDPO> gridVisualizarEquacoes) {
		this.gridVisualizarEquacoes = gridVisualizarEquacoes;
	}

	public String getCopiarEmNovaEquacao() {
		return copiarEmNovaEquacao;
	}

	public void setCopiarEmNovaEquacao(String copiarEmNovaEquacao) {
		this.copiarEmNovaEquacao = copiarEmNovaEquacao;
	}

	public String getEditar() {
		return editar;
	}

	public void setEditar(String editar) {
		this.editar = editar;
	}

	public String getRemover() {
		return remover;
	}

	public void setRemover(String remover) {
		this.remover = remover;
	}

}