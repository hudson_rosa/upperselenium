package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaConfiguracoesPontosDPO extends BaseHelperDPO {

	private String quadrante;
	private String energiaDelEnergiaRec;
	private String situacaoComercial;
	private String medicaoA;
	private String tensaoDeMedicao;
	private String limiteDePotenciaDeConsumo;
	private String limiteDePotenciaDeGeracao;
	private String conexao;
	private String capacidadeDeConexao;
	private String demandaDimensionada;
	private String compensacaoDePerdas;

	public String getQuadrante() {
		return quadrante;
	}

	public void setQuadrante(String quadrante) {
		this.quadrante = quadrante;
	}

	public String getEnergiaDelEnergiaRec() {
		return energiaDelEnergiaRec;
	}

	public void setEnergiaDelEnergiaRec(String energiaDelEnergiaRec) {
		this.energiaDelEnergiaRec = energiaDelEnergiaRec;
	}

	public String getSituacaoComercial() {
		return situacaoComercial;
	}

	public void setSituacaoComercial(String situacaoComercial) {
		this.situacaoComercial = situacaoComercial;
	}

	public String getMedicaoA() {
		return medicaoA;
	}

	public void setMedicaoA(String medicaoA) {
		this.medicaoA = medicaoA;
	}

	public String getTensaoDeMedicao() {
		return tensaoDeMedicao;
	}

	public void setTensaoDeMedicao(String tensaoDeMedicao) {
		this.tensaoDeMedicao = tensaoDeMedicao;
	}

	public String getLimiteDePotenciaDeConsumo() {
		return limiteDePotenciaDeConsumo;
	}

	public void setLimiteDePotenciaDeConsumo(String limiteDePotenciaDeConsumo) {
		this.limiteDePotenciaDeConsumo = limiteDePotenciaDeConsumo;
	}

	public String getLimiteDePotenciaDeGeracao() {
		return limiteDePotenciaDeGeracao;
	}

	public void setLimiteDePotenciaDeGeracao(String limiteDePotenciaDeGeracao) {
		this.limiteDePotenciaDeGeracao = limiteDePotenciaDeGeracao;
	}

	public String getConexao() {
		return conexao;
	}

	public void setConexao(String conexao) {
		this.conexao = conexao;
	}

	public String getCapacidadeDeConexao() {
		return capacidadeDeConexao;
	}

	public void setCapacidadeDeConexao(String capacidadeDeConexao) {
		this.capacidadeDeConexao = capacidadeDeConexao;
	}

	public String getDemandaDimensionada() {
		return demandaDimensionada;
	}

	public void setDemandaDimensionada(String demandaDimensionada) {
		this.demandaDimensionada = demandaDimensionada;
	}

	public String getCompensacaoDePerdas() {
		return compensacaoDePerdas;
	}

	public void setCompensacaoDePerdas(String compensacaoDePerdas) {
		this.compensacaoDePerdas = compensacaoDePerdas;
	}

}