package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo;

public class GridAlterarColetasDPO {

	private String flagItem;
	private String grandeza;
	private String dataDataUltimaColeta;

	public String getFlagItem() {
		return flagItem;
	}

	public void setFlagItem(String flagItem) {
		this.flagItem = flagItem;
	}

	public String getGrandeza() {
		return grandeza;
	}

	public void setGrandeza(String grandeza) {
		this.grandeza = grandeza;
	}

	public String getDataDataUltimaColeta() {
		return dataDataUltimaColeta;
	}

	public void setDataDataUltimaColeta(String dataDataUltimaColeta) {
		this.dataDataUltimaColeta = dataDataUltimaColeta;
	}

}
