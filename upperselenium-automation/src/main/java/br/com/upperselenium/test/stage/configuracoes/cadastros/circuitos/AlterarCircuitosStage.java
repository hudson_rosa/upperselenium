package br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos.dpo.AlterarCircuitosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos.page.AlteracaoCircuitosPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos.page.CircuitosPage;

public class AlterarCircuitosStage extends BaseStage {
	private AlterarCircuitosDPO alterarCircuitosDPO;
	private CircuitosPage circuitosPage;
	private AlteracaoCircuitosPage alteracaoCircuitosPage;
	
	public AlterarCircuitosStage(String dp) {
		alterarCircuitosDPO = loadDataProviderFile(AlterarCircuitosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		circuitosPage = initElementsFromPage(CircuitosPage.class);
		alteracaoCircuitosPage = initElementsFromPage(AlteracaoCircuitosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarCircuitosDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();		
		alteracaoCircuitosPage.typeTextNome(alterarCircuitosDPO.getNome());
		alteracaoCircuitosPage.typeTextAlias(alterarCircuitosDPO.getAlias());
		alteracaoCircuitosPage.typeTextMedidor(alterarCircuitosDPO.getMedidor());
		alteracaoCircuitosPage.typeRadioEntradaDigital(alterarCircuitosDPO.getEntradaDigitalRDO());
		alteracaoCircuitosPage.typeRadioArquivo(alterarCircuitosDPO.getArquivoRDO());
		alteracaoCircuitosPage.typeSelectEntradaDigital(alterarCircuitosDPO.getEntradaDigitalCBX());
		alteracaoCircuitosPage.typeCheckBoxCreg(alterarCircuitosDPO.getCreg());
		alteracaoCircuitosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoCircuitosPage.validateMessageDefault(alterarCircuitosDPO.getOperationMessage());
	}

}
