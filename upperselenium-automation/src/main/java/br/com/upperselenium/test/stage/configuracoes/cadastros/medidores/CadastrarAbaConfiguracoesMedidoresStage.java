package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo.CadastrarAbaConfiguracoesMedidoresDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page.CadastroAbaConfiguracoesMedidoresPage;

public class CadastrarAbaConfiguracoesMedidoresStage extends BaseStage {
	private CadastrarAbaConfiguracoesMedidoresDPO cadastrarAbaConfiguracoesMedidoresDPO;
	private CadastroAbaConfiguracoesMedidoresPage cadastroAbaConfiguracoesMedidoresPage;
	
	public CadastrarAbaConfiguracoesMedidoresStage(String dp) {
		cadastrarAbaConfiguracoesMedidoresDPO = loadDataProviderFile(CadastrarAbaConfiguracoesMedidoresDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaConfiguracoesMedidoresPage = initElementsFromPage(CadastroAbaConfiguracoesMedidoresPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaConfiguracoesMedidoresDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaConfiguracoesMedidoresPage.clickAbaConfiguracoes();
		FindElementUtil.acceptAlert();
		cadastroAbaConfiguracoesMedidoresPage.typeCheckBoxInformarNumeradorEDenominadorRtpRtc(cadastrarAbaConfiguracoesMedidoresDPO.getInformarNumeradorEDenominadorRtpRtc());
		cadastroAbaConfiguracoesMedidoresPage.typeTextNumeradorRTP(cadastrarAbaConfiguracoesMedidoresDPO.getNumeradorRTP());
		cadastroAbaConfiguracoesMedidoresPage.typeTextNumeradorRTC(cadastrarAbaConfiguracoesMedidoresDPO.getNumeradorRTC());
		cadastroAbaConfiguracoesMedidoresPage.typeTextDenominadorRTP(cadastrarAbaConfiguracoesMedidoresDPO.getDenominadorRTP());
		cadastroAbaConfiguracoesMedidoresPage.typeTextDenominadorRTC(cadastrarAbaConfiguracoesMedidoresDPO.getDenominadorRTC());
		cadastroAbaConfiguracoesMedidoresPage.typeTextRelacaoRTP(cadastrarAbaConfiguracoesMedidoresDPO.getRelacaoRTP());
		cadastroAbaConfiguracoesMedidoresPage.typeTextRelacaoRTC(cadastrarAbaConfiguracoesMedidoresDPO.getRelacaoRTC());
		cadastroAbaConfiguracoesMedidoresPage.typeTextClasse(cadastrarAbaConfiguracoesMedidoresDPO.getClasse());
		cadastroAbaConfiguracoesMedidoresPage.typeTextClasseDeExatidao(cadastrarAbaConfiguracoesMedidoresDPO.getClasseDeExatidao());
		cadastroAbaConfiguracoesMedidoresPage.typeSelectQuantidadeDeElementos(cadastrarAbaConfiguracoesMedidoresDPO.getQuantidadeDeElementos());
		cadastroAbaConfiguracoesMedidoresPage.typeTextQuantidadeDeFios(cadastrarAbaConfiguracoesMedidoresDPO.getQuantidadeDeFios());
		cadastroAbaConfiguracoesMedidoresPage.keyPageDown1();
		cadastroAbaConfiguracoesMedidoresPage.typeTextConstanteDeIntegracao(cadastrarAbaConfiguracoesMedidoresDPO.getConstanteDeIntegracao());
		cadastroAbaConfiguracoesMedidoresPage.typeTextConstanteDeEnergiaAtiva(cadastrarAbaConfiguracoesMedidoresDPO.getConstanteDeEnergiaAtiva());
		cadastroAbaConfiguracoesMedidoresPage.typeTextConstanteDeEnergiaReativa(cadastrarAbaConfiguracoesMedidoresDPO.getConstanteDeEnergiaReativa());
		cadastroAbaConfiguracoesMedidoresPage.typeTextCorrenteMaxima(cadastrarAbaConfiguracoesMedidoresDPO.getCorrenteMaxima());
		cadastroAbaConfiguracoesMedidoresPage.typeTextCorrenteNominal(cadastrarAbaConfiguracoesMedidoresDPO.getCorrenteNominal());
		cadastroAbaConfiguracoesMedidoresPage.typeTextTensaoNominal(cadastrarAbaConfiguracoesMedidoresDPO.getTensaoNominal());
		cadastroAbaConfiguracoesMedidoresPage.typeTextTensaoMaxima(cadastrarAbaConfiguracoesMedidoresDPO.getTensaoMaxima());
		cadastroAbaConfiguracoesMedidoresPage.keyPageDown2();
		cadastroAbaConfiguracoesMedidoresPage.typeTextFrequencia(cadastrarAbaConfiguracoesMedidoresDPO.getFrequencia());
		cadastroAbaConfiguracoesMedidoresPage.typeTextFrequenciaMaxima(cadastrarAbaConfiguracoesMedidoresDPO.getFrequenciaMaxima());
		cadastroAbaConfiguracoesMedidoresPage.typeTextConstantePRN(cadastrarAbaConfiguracoesMedidoresDPO.getConstantePRN());
		cadastroAbaConfiguracoesMedidoresPage.typeTextCodigoEletrobras(cadastrarAbaConfiguracoesMedidoresDPO.getCodigoEletrobras());
		cadastroAbaConfiguracoesMedidoresPage.typeTextConstanteDeCombustivel(cadastrarAbaConfiguracoesMedidoresDPO.getConstanteDeCombustivel());
		cadastroAbaConfiguracoesMedidoresPage.keyPageDown3();
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaConfiguracoesMedidoresDPO.getNumeroDoEquipamentoSAP())){
			cadastroAbaConfiguracoesMedidoresPage.typeTextNumeroDoEquipamentoSAP(cadastrarAbaConfiguracoesMedidoresDPO.getNumeroDoEquipamentoSAP()+getRandomStringNonSpaced());
		}
		cadastroAbaConfiguracoesMedidoresPage.typeSelectTipoDeLigacao(cadastrarAbaConfiguracoesMedidoresDPO.getTipoDeLigacao());
		cadastroAbaConfiguracoesMedidoresPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaConfiguracoesMedidoresPage.validateMessageDefault(cadastrarAbaConfiguracoesMedidoresDPO.getOperationMessage());		
		validateCalculoRelacaoRTP();
		validateCalculoRelacaoRTC();
	}
	
	private void validateCalculoRelacaoRTP() {
		cadastroAbaConfiguracoesMedidoresPage.validateEqualsToValues(
				cadastrarAbaConfiguracoesMedidoresDPO.getCalculoRelacaoRTP(), 
				cadastroAbaConfiguracoesMedidoresPage.getRelacaoRTP());
	}
	
	private void validateCalculoRelacaoRTC() {
		cadastroAbaConfiguracoesMedidoresPage.validateEqualsToValues(
				cadastrarAbaConfiguracoesMedidoresDPO.getCalculoRelacaoRTC(), 
				cadastroAbaConfiguracoesMedidoresPage.getRelacaoRTC());		
	}

}
