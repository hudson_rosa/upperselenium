package br.com.upperselenium.test.stage.configuracoes.implantacao.auto_discovery.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAutoDiscoveryPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 
	private static final String SELECT_MODELO = "//*[@id='Id']";
	private static final String SELECT_TIPO_DE_MEDIDOR = "//*[@id='TipoDeMedidor']";
	private static final String SELECT_COMUNICACAO = "//*[@id='Comunicacao']";
	private static final String SELECT_TIPO_DE_PONTO = "//*[@id='TipoDePonto']";
	private static final String SELECT_PASTA = "//*[@id='Pasta']";
	private static final String SELECT_QUADRANTE = "//*[@id='Quadrante']";
	
	public void typeSelectModelo(String value){
		typeSelectComboOption(SELECT_MODELO, value);
	}
	
	public void typeSelectTipoDeMedidor(String value){
		typeSelectComboOption(SELECT_TIPO_DE_MEDIDOR, value);
	}
	
	public void typeSelectComunicacao(String value){
		typeSelectComboOption(SELECT_COMUNICACAO, value);
	}
	
	public void typeSelectTipoDePonto(String value){
		typeSelectComboOption(SELECT_TIPO_DE_PONTO, value);
	}
	
	public void typeSelectPasta(String value){
		typeSelectComboOption(SELECT_PASTA, value);
	}
	
	public void typeSelectQuadrante(String value){
		typeSelectComboOption(SELECT_QUADRANTE, value);
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}