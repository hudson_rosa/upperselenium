package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.dpo.NavegarCadastroTiposDePontoDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.page.CadastroTiposDePontoPage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.page.TiposDePontoPage;

public class NavegarCadastroTiposDePontoStage extends BaseStage {
	private NavegarCadastroTiposDePontoDPO navegarCadastroTiposDePontoDPO;
	private TiposDePontoPage tiposDePontoPage;
	private CadastroTiposDePontoPage cadastroTiposDePontoPage;
	
	public NavegarCadastroTiposDePontoStage(String dp) {
		navegarCadastroTiposDePontoDPO = loadDataProviderFile(NavegarCadastroTiposDePontoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDePontoPage = initElementsFromPage(TiposDePontoPage.class);
		cadastroTiposDePontoPage = initElementsFromPage(CadastroTiposDePontoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarCadastroTiposDePontoDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		tiposDePontoPage.clickNovo();
		cadastroTiposDePontoPage.getLabelButtonSalvar(navegarCadastroTiposDePontoDPO.getLabelButtonSalvar());
		cadastroTiposDePontoPage.getBreadcrumbsText(navegarCadastroTiposDePontoDPO.getBreadcrumbs());
		cadastroTiposDePontoPage.clickUpToBreadcrumbs(navegarCadastroTiposDePontoDPO.getUpToBreadcrumb());
		cadastroTiposDePontoPage.getWelcomeTitle(navegarCadastroTiposDePontoDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
