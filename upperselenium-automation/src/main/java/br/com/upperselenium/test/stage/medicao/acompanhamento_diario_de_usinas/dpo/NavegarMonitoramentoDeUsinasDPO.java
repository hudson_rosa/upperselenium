package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarMonitoramentoDeUsinasDPO extends BaseHelperDPO {

	private String labelNiveis;
	private String labelVertimento;
	private String labelEficiencia;
	private String labelUnidadesGeradoras;
	private String labelVazao;

	public String getLabelNiveis() {
		return labelNiveis;
	}

	public void setLabelNiveis(String labelNiveis) {
		this.labelNiveis = labelNiveis;
	}

	public String getLabelVertimento() {
		return labelVertimento;
	}

	public void setLabelVertimento(String labelVertimento) {
		this.labelVertimento = labelVertimento;
	}

	public String getLabelEficiencia() {
		return labelEficiencia;
	}

	public void setLabelEficiencia(String labelEficiencia) {
		this.labelEficiencia = labelEficiencia;
	}

	public String getLabelUnidadesGeradoras() {
		return labelUnidadesGeradoras;
	}

	public void setLabelUnidadesGeradoras(String labelUnidadesGeradoras) {
		this.labelUnidadesGeradoras = labelUnidadesGeradoras;
	}

	public String getLabelVazao() {
		return labelVazao;
	}

	public void setLabelVazao(String labelVazao) {
		this.labelVazao = labelVazao;
	}

	@Override
	public String toString() {
		return "NavegarMonitoramentoDeUsinasDPO [labelNiveis=" + labelNiveis + ", labelVertimento=" + labelVertimento
				+ ", labelEficiencia=" + labelEficiencia + ", labelUnidadesGeradoras=" + labelUnidadesGeradoras
				+ ", labelVazao=" + labelVazao + "]";
	}

}