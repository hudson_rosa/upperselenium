package br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AgendarComandosDoMedidorDPO extends BaseHelperDPO {

	private String filtrarComando;
	private String filteredComando;
	private List<String> digiteUmOuMaisMedidores;

	public String getFiltrarComando() {
		return filtrarComando;
	}

	public String getFilteredComando() {
		return filteredComando;
	}

	public void setFilteredComando(String filteredComando) {
		this.filteredComando = filteredComando;
	}

	public void setFiltrarComando(String filtrarComando) {
		this.filtrarComando = filtrarComando;
	}

	public List<String> getDigiteUmOuMaisMedidores() {
		return digiteUmOuMaisMedidores;
	}

	public void setDigiteUmOuMaisMedidores(List<String> digiteUmOuMaisMedidores) {
		this.digiteUmOuMaisMedidores = digiteUmOuMaisMedidores;
	}

}