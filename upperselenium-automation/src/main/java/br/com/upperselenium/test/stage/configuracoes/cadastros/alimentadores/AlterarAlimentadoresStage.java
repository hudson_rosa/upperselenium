package br.com.upperselenium.test.stage.configuracoes.cadastros.alimentadores;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.configuracoes.cadastros.alimentadores.dpo.AlterarAlimentadoresDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.alimentadores.page.AlimentadoresPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.alimentadores.page.AlteracaoAlimentadoresPage;

public class AlterarAlimentadoresStage extends BaseStage {
	private AlterarAlimentadoresDPO alterarAlimentadoresDPO;
	private AlimentadoresPage alimentadoresPage;
	private AlteracaoAlimentadoresPage alteracaoAlimentadoresPage;
	
	public AlterarAlimentadoresStage(String dp) {
		alterarAlimentadoresDPO = loadDataProviderFile(AlterarAlimentadoresDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alimentadoresPage = initElementsFromPage(AlimentadoresPage.class);
		alteracaoAlimentadoresPage = initElementsFromPage(AlteracaoAlimentadoresPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarAlimentadoresDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();		
		alteracaoAlimentadoresPage.typeTextNome(alterarAlimentadoresDPO.getNome());
		alteracaoAlimentadoresPage.typeTextPontoDeMedicao(alterarAlimentadoresDPO.getPontoDeMedicao());
		alteracaoAlimentadoresPage.typeTextPseudonimoPerdasTecnicas(alterarAlimentadoresDPO.getPseudonimoPerdasTecnicas());
		alteracaoAlimentadoresPage.typeTextPseudonimoConsumo(alterarAlimentadoresDPO.getPseudonimoConsumo());
		alteracaoAlimentadoresPage.typeTextPontoDeConsumoAdicional(alterarAlimentadoresDPO.getPontoDeConsumoAdicional());
		alteracaoAlimentadoresPage.typeSelectRegiao(alterarAlimentadoresDPO.getRegiao());
		alteracaoAlimentadoresPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoAlimentadoresPage.validateMessageDefault(alterarAlimentadoresDPO.getOperationMessage());

	}

}
