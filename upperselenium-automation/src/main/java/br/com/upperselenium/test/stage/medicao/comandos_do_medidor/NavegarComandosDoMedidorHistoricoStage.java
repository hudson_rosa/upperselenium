package br.com.upperselenium.test.stage.medicao.comandos_do_medidor;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo.NavegarComandosDeMedidorHistoricoDPO;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.page.ComandosDoMedidorHistoricoPage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.page.ComandosDoMedidorPage;

public class NavegarComandosDoMedidorHistoricoStage extends BaseStage {
	private NavegarComandosDeMedidorHistoricoDPO navegarComandosDoMedidorAgendarParamDPO;
	private ComandosDoMedidorPage comandosDoMedidorPage;	
	private ComandosDoMedidorHistoricoPage comandosDoMedidorAgendarParamPage;	
	
	public NavegarComandosDoMedidorHistoricoStage(String dp) {
		navegarComandosDoMedidorAgendarParamDPO = loadDataProviderFile(NavegarComandosDeMedidorHistoricoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		comandosDoMedidorPage = initElementsFromPage(ComandosDoMedidorPage.class);
		comandosDoMedidorAgendarParamPage = initElementsFromPage(ComandosDoMedidorHistoricoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarComandosDoMedidorAgendarParamDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		comandosDoMedidorPage.clickLinkHistorico();
		comandosDoMedidorAgendarParamPage.getLabelHeaderMedidor(navegarComandosDoMedidorAgendarParamDPO.getLabelHeaderMedidor());
		comandosDoMedidorAgendarParamPage.getLabelHeaderInstante(navegarComandosDoMedidorAgendarParamDPO.getLabelHeaderInstante());
		comandosDoMedidorAgendarParamPage.getLabelHeaderDescricao(navegarComandosDoMedidorAgendarParamDPO.getLabelHeaderDescricao());
		comandosDoMedidorAgendarParamPage.getLabelHeaderStatus(navegarComandosDoMedidorAgendarParamDPO.getLabelHeaderStatus());
		comandosDoMedidorAgendarParamPage.getLabelHeaderErro(navegarComandosDoMedidorAgendarParamDPO.getLabelHeaderErro());
		comandosDoMedidorAgendarParamPage.getBreadcrumbsText(navegarComandosDoMedidorAgendarParamDPO.getBreadcrumbs());
		comandosDoMedidorAgendarParamPage.clickUpToBreadcrumbs(navegarComandosDoMedidorAgendarParamDPO.getUpToBreadcrumb());
		comandosDoMedidorAgendarParamPage.getWelcomeTitle(navegarComandosDoMedidorAgendarParamDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
