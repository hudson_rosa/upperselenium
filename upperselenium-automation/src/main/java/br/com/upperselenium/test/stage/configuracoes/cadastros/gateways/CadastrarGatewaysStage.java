package br.com.upperselenium.test.stage.configuracoes.cadastros.gateways;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.dpo.CadastrarGatewaysDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.page.CadastroGatewaysPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.page.GatewaysPage;

public class CadastrarGatewaysStage extends BaseStage {
	private CadastrarGatewaysDPO cadastrarGatewaysDPO;
	private GatewaysPage gatewaysPage;
	private CadastroGatewaysPage cadastroGatewaysPage;
	
	public CadastrarGatewaysStage(String dp) {
		cadastrarGatewaysDPO = loadDataProviderFile(CadastrarGatewaysDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		gatewaysPage = initElementsFromPage(GatewaysPage.class);
		cadastroGatewaysPage = initElementsFromPage(CadastroGatewaysPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarGatewaysDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		gatewaysPage.clickNovo();
		cadastroGatewaysPage.typeTextID(cadastrarGatewaysDPO.getId()+getRandomStringNonSpaced());				
		cadastroGatewaysPage.typeSelectModelo(cadastrarGatewaysDPO.getModelo());				
		cadastroGatewaysPage.typeTextNome(cadastrarGatewaysDPO.getNome()+getRandomStringSpaced());				
		cadastroGatewaysPage.typeTextDescricao(cadastrarGatewaysDPO.getDescricao()+getRandomStringSpaced());				
		cadastroGatewaysPage.typeTextEntradaDigital1(cadastrarGatewaysDPO.getEntradaDigital1());				
		cadastroGatewaysPage.typeTextEntradaDigital2(cadastrarGatewaysDPO.getEntradaDigital2());				
		cadastroGatewaysPage.typeTextEntradaDigital3(cadastrarGatewaysDPO.getEntradaDigital3());				
		cadastroGatewaysPage.typeTextEntradaDigital4(cadastrarGatewaysDPO.getEntradaDigital4());				
		cadastroGatewaysPage.typeSelectTipoDeComunicacao(cadastrarGatewaysDPO.getTipoDeComunicacao());				
		cadastroGatewaysPage.typeTextEnderecoIPPorta(cadastrarGatewaysDPO.getEnderecoIPPorta());				
		cadastroGatewaysPage.typeTextEnderecoSecundarioIPPorta(cadastrarGatewaysDPO.getEnderecoSecundarioIPPorta());
		cadastroGatewaysPage.keyPageDown();
		cadastroGatewaysPage.typeTextIdDeComunicacao(cadastrarGatewaysDPO.getIdDeComunicacao());				
		cadastroGatewaysPage.typeTextUsuario(cadastrarGatewaysDPO.getUsuario());				
		cadastroGatewaysPage.typeTextSenha(cadastrarGatewaysDPO.getSenha());				
		cadastroGatewaysPage.typeCheckComunicacaoCriptografada(cadastrarGatewaysDPO.getComunicacaoCriptografada());				
		cadastroGatewaysPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroGatewaysPage.validateMessageDefault(cadastrarGatewaysDPO.getOperationMessage());
	}

}
