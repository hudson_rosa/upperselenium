package br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarSubestacaoDPO extends BaseHelperDPO {

	private String nome;
	private String descricao;
	private String quantidadeDePontos;
	private String alimentacaoAuxiliar;
	private String observacoes;
	private String propriedade;
	private String fronteira;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getQuantidadeDePontos() {
		return quantidadeDePontos;
	}

	public void setQuantidadeDePontos(String quantidadeDePontos) {
		this.quantidadeDePontos = quantidadeDePontos;
	}

	public String getAlimentacaoAuxiliar() {
		return alimentacaoAuxiliar;
	}

	public void setAlimentacaoAuxiliar(String alimentacaoAuxiliar) {
		this.alimentacaoAuxiliar = alimentacaoAuxiliar;
	}

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public String getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}

	public String getFronteira() {
		return fronteira;
	}

	public void setFronteira(String fronteira) {
		this.fronteira = fronteira;
	}

}