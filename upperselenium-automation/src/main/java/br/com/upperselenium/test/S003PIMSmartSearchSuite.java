package br.com.upperselenium.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseSuite;
import br.com.upperselenium.base.annotation.SuiteParams;
import br.com.upperselenium.test.flow.valida_busca.T0001SmartSearchNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_busca.T0002SmartSearchComandosMedidorFlow;
import br.com.upperselenium.test.flow.valida_busca.T0003SmartSearchExtratorDadosFlow;
import br.com.upperselenium.test.flow.valida_busca.T0004SmartSearchImpressaoDadosFlow;
import br.com.upperselenium.test.flow.valida_busca.T0005SmartSearchMaximaDemAparenteFlow;


@RunWith(Suite.class)

@SuiteClasses({
	T0002SmartSearchComandosMedidorFlow.class,
	T0003SmartSearchExtratorDadosFlow.class,
	T0004SmartSearchImpressaoDadosFlow.class,
	T0005SmartSearchMaximaDemAparenteFlow.class,
	T0001SmartSearchNavegacoesFlow.class
	})

@SuiteParams(description="PIM - Componente Smart Search")
public class S003PIMSmartSearchSuite extends BaseSuite {}
