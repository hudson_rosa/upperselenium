package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regras.CadastrarRegrasStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regras.GoToRegrasStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0020RegrasCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0020-Regras", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Regras realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0020RegrasCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToRegrasStage());
		addStage(new CadastrarRegrasStage(getDP("CadastrarRegrasDP.json")));
	}	
}
