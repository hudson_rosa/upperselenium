package br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.dpo.CadastrarUnidadesConsumidorasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.page.CadastroUnidadesConsumidorasPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.page.UnidadesConsumidorasPage;

public class CadastrarUnidadesConsumidorasStage extends BaseStage {
	private CadastrarUnidadesConsumidorasDPO cadastrarUnidadesConsumidorasDPO;
	private UnidadesConsumidorasPage unidadesConsumidorasPage;
	private CadastroUnidadesConsumidorasPage cadastroUnidadesConsumidorasPage;
	
	public CadastrarUnidadesConsumidorasStage(String dp) {
		cadastrarUnidadesConsumidorasDPO = loadDataProviderFile(CadastrarUnidadesConsumidorasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		unidadesConsumidorasPage = initElementsFromPage(UnidadesConsumidorasPage.class);
		cadastroUnidadesConsumidorasPage = initElementsFromPage(CadastroUnidadesConsumidorasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarUnidadesConsumidorasDPO.getIssue());
		execute();
	}
	
	private void execute() {
		unidadesConsumidorasPage.clickNova();
		cadastroUnidadesConsumidorasPage.typeTextNumero(cadastrarUnidadesConsumidorasDPO.getNumero()+getRandomNumber());		
		cadastroUnidadesConsumidorasPage.typeTextNome(cadastrarUnidadesConsumidorasDPO.getNome()+getRandomNumberSpaced());		
		cadastroUnidadesConsumidorasPage.typeSelectRamoDeAtividade(cadastrarUnidadesConsumidorasDPO.getRamoDeAtividade());		
		cadastroUnidadesConsumidorasPage.typeSelectEtapa(cadastrarUnidadesConsumidorasDPO.getEtapa());		
		cadastroUnidadesConsumidorasPage.typeTextPostoTarifario(cadastrarUnidadesConsumidorasDPO.getPostoTarifario());		
		cadastroUnidadesConsumidorasPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroUnidadesConsumidorasPage.validateMessageDefault(cadastrarUnidadesConsumidorasDPO.getOperationMessage());
	}

}
