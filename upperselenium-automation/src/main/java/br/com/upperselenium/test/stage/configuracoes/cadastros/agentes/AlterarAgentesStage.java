package br.com.upperselenium.test.stage.configuracoes.cadastros.agentes;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.configuracoes.cadastros.agentes.dpo.AlterarAgentesDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.agentes.page.AgentesPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.agentes.page.AlteracaoAgentesPage;

public class AlterarAgentesStage extends BaseStage {
	private AlterarAgentesDPO alterarAgentesDPO;
	private AgentesPage agentesPage;
	private AlteracaoAgentesPage alteracaoAgentesPage;
	
	public AlterarAgentesStage(String dp) {
		alterarAgentesDPO = loadDataProviderFile(AlterarAgentesDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		agentesPage = initElementsFromPage(AgentesPage.class);
		alteracaoAgentesPage = initElementsFromPage(AlteracaoAgentesPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarAgentesDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();	
		alteracaoAgentesPage.typeTextNome(alterarAgentesDPO.getNome());
		alteracaoAgentesPage.typeTextDescricao(alterarAgentesDPO.getDescricao()+getRandomStringSpaced());
		alteracaoAgentesPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoAgentesPage.validateMessageDefault(alterarAgentesDPO.getOperationMessage());
	}

}
