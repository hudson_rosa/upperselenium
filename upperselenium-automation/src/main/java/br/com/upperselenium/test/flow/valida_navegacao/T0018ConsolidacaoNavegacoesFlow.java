package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.medicao.consolidacao.GoToConsolidacaoStage;
import br.com.upperselenium.test.stage.medicao.consolidacao.NavegarConsolidacaoStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0018ConsolidacaoNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0018-Consolidacao", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Consolidação validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0018ConsolidacaoNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToConsolidacaoStage());
		addStage(new NavegarConsolidacaoStage(getDP("NavegarConsolidacaoDP.json")));
	}	
}
