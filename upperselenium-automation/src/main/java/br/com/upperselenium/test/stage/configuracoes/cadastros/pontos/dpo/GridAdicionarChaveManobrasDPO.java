package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

public class GridAdicionarChaveManobrasDPO {

	private String chave;
	private String filterChave;
	private String gridValueChave;
	private String buttonRemove;

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getFilterChave() {
		return filterChave;
	}

	public void setFilterChave(String filterChave) {
		this.filterChave = filterChave;
	}

	public String getGridValueChave() {
		return gridValueChave;
	}

	public void setGridValueChave(String gridValueChave) {
		this.gridValueChave = gridValueChave;
	}

	public String getButtonRemove() {
		return buttonRemove;
	}

	public void setButtonRemove(String buttonRemove) {
		this.buttonRemove = buttonRemove;
	}

}
