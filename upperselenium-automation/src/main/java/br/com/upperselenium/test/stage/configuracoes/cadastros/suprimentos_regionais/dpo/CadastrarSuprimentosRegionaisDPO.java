package br.com.upperselenium.test.stage.configuracoes.cadastros.suprimentos_regionais.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarSuprimentosRegionaisDPO extends BaseHelperDPO {

	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}