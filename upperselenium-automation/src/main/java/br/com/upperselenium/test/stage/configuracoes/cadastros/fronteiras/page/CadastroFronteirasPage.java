package br.com.upperselenium.test.stage.configuracoes.cadastros.fronteiras.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroFronteirasPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div/div[2]/form/div[6]/input";   
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String TEXT_PONTO_DE_ENERGIA_REQUERIDA = "//*[@id='Ponto']";
	private static final String TEXT_PONTO_DE_PERDA_TECNICA_AT_MEDIDA = "//*[@id='PontoDePerTecAtMedida']"; 
	private static final String TEXT_PSEUDONIMO_DE_SISTEMAS_DE_PERDAS_TECNICAS = "//*[@id='AliasSistemaDePerdasTecnicas']";
	private static final String TEXT_SUPRIMENTO_REGIONAL = "//*[@id='SuprimentoRegional']";
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	
	
	public void typeTextPontoDeEnergiaRequerida(String value){
		typeText(TEXT_PONTO_DE_ENERGIA_REQUERIDA, value);
	}
	
	public void typeTextPontoDePerdaTecnicaATMedida(String value){
		typeTextAutoCompleteSelect(TEXT_PONTO_DE_PERDA_TECNICA_AT_MEDIDA, value);
	}
	
	public void typeTextPseudonimoDeSistemasDePerdasTecnicas(String value){
		typeText(TEXT_PSEUDONIMO_DE_SISTEMAS_DE_PERDAS_TECNICAS, value);
	}
	
	public void typeTextSuprimentoRegional(String value){
		typeText(TEXT_SUPRIMENTO_REGIONAL, value);
	}
		
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}