package br.com.upperselenium.test.stage.wrapper.menu_area.page;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AreasMenuPage extends BaseHelperPage{
	
	private static final String LINK_CONFIGURACOES = ".//div[1]/div/div/div/ul/li[1]/a[contains(text(), 'Configurações')]";
	private static final String LINK_MEDICAO = ".//div[1]/div/div/div/ul/li[2]/a[contains(text(), 'Medição')]";	
	private static final String LINK_ANALISES = ".//div[1]/div/div/div/ul/li[3]/a[contains(text(), 'Análises')]";
	private static final String LINK_CCEE = ".//div[1]/div/div/div/ul/li[4]/a[contains(text(), 'CCEE')]";
	private static final String LINK_CREG = ".//div[1]/div/div/div/ul/li[5][contains(text(), 'CREG')]";	
	private static final String LINK_FATURAMENTO = ".//div[1]/div/div/div/ul/li[6]/a[contains(text(), 'Faturamento')]";
	private static final String LINK_MONITORAMENTO = ".//div[1]/div/div/div/ul/li[7]/a[contains(text(), 'Monitoramento')]";
	private static final String LABEL_USUARIO_CONECTADO = "/html/body/div[1]/div/div/p";
	private static final String LINK_NOTIFICADOR = "notification-icon";
	private static final String LINK_SAIR = ".//div[1]/div/div/p[contains(@class, 'navbar-text pull-right')]/a[contains(text(), 'Sair')]";
	
	@FindBy(xpath=LABEL_USUARIO_CONECTADO)
	private WebElement lblUsuarioConectado;	
	
	public boolean isClickableLinkConfiguracoes() {
		return isDisplayedElement(LINK_CONFIGURACOES);
	}
	
	public boolean isClickableLinkMedicao() {
		return isDisplayedElement(LINK_MEDICAO);
	}	

	public boolean isClicableLnkAnalises() {
		return isDisplayedElement(LINK_ANALISES);
	}

	public boolean isClickableLinkCCEE() {		
		return isDisplayedElement(LINK_CCEE);
	}
	
	public boolean isClickableLinkCREG() {		
		return isDisplayedElement(LINK_CREG);
	}	
	
	public boolean isClickableLinkFaturamento() {		
		return isDisplayedElement(LINK_FATURAMENTO);
	}	
	
	public boolean isClicableLnkMonitoramento() {
		return isDisplayedElement(LINK_MONITORAMENTO);
	}

	public boolean isClickableLinkUsuario() {
		return isDisplayedElement(LABEL_USUARIO_CONECTADO);
	}

	public boolean isClickableLinkNotificador() {
		return isDisplayedElement(LINK_NOTIFICADOR);
	}
	
	public boolean isClickableLinkSair() {
		return isDisplayedElement(LINK_SAIR);
	}
	
	public void clickConfiguracoes(){
		clickOnElement(LINK_CONFIGURACOES);
	}
	
	public void clickMedicao(){
		clickOnElement(LINK_MEDICAO);
	}	
		
	public void clickAnalises(){
		clickOnElement(LINK_ANALISES);
	}
	
	public void clickOnCCEE(){
		clickOnElement(LINK_CCEE);
	}
	
	public void clickCREG(){
		clickOnElement(LINK_CREG);
	}
	
	public void clickFaturamento(){
		clickOnElement(LINK_FATURAMENTO);
	}	
	
	public void clickMonitoramento(){
		clickOnElement(LINK_MONITORAMENTO);
	}

	public void clickNotificador(){
		clickOnElement(LINK_NOTIFICADOR);
	}
	
	public void clickSair(){
		clickOnElement(LINK_SAIR);
	}
	
	public void getLabelUsuarioConectado(String value) {
		getLabel(LABEL_USUARIO_CONECTADO, value);
	}
	
}