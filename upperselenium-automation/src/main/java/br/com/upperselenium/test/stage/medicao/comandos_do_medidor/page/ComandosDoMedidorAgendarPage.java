package br.com.upperselenium.test.stage.medicao.comandos_do_medidor.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class ComandosDoMedidorAgendarPage extends BaseHelperPage {

	private static final String LABEL_COMANDOS = "//*[@id='multiSelectBlock']/multi-select/label";
	private static final String LABEL_INSTANTE = "//*[@id='formAgendamento']/div[2]/label";
	private static final String LABEL_EXPIRACAO = "//*[@id='formAgendamento']/div[2]/div[2]/label";
	private static final String LABEL_DIGITE_UM_OU_MAIS_MEDIDORES = "//*[@id='formAgendamento']/div[3]/label";
	private static final String LABEL_REPETICAO = "//*[@id='controlesRecorrencia']/legend";
	private static final String BUTTON_COMANDOS = "//*[@id='multiSelectBlock']/multi-select/div/button";
	private static final String TEXT_COMANDOS_FILTRAR = "//*[@id='multi-select-pesquisa']";
	private static final String CHECK_FILTERED_COMANDO = "//*[@class='multi-select-options']//ul/li/input";
	private static final String BUTTON_CONFIGURAR_PARAMETROS = "//*[@id='carregueComandos']";
	private static final String TEXT_AUTOCOMPLETE_MEDIDORES = "//*[@id='formAgendamento']/div[3]/div/ul/li/input";
	private static final String BUTTON_AGENDAR = "//*[@id='Agendar']";

	public void getLabelComandos(String value) {
		getLabel(LABEL_COMANDOS, value);
	}

	public void getLabelInstante(String value) {
		getLabel(LABEL_INSTANTE, value);
	}

	public void getLabelExpiracao(String value) {
		getLabel(LABEL_EXPIRACAO, value);
	}

	public void getLabelDigiteUmOuMaisMedidores(String value) {
		getLabel(LABEL_DIGITE_UM_OU_MAIS_MEDIDORES, value);
	}

	public void getLabelRepeticao(String value) {
		getLabel(LABEL_REPETICAO, value);
	}

	public void clickButtonComandos() {
		clickOnElement(BUTTON_COMANDOS);
		waitForPageToLoadUntil10s();
	}

	public void typeTextFiltrarComando(String value) {
		typeTextAutoCompleteFilter(TEXT_COMANDOS_FILTRAR, value);
		waitForATime(TimePRM._3_SECS);
	}

	public void typeCheckBoxFilteredComando(String value) {
		waitForPresenceOfElementLocated(CHECK_FILTERED_COMANDO);
		typeCheckOption(CHECK_FILTERED_COMANDO, value);		
	}

	public void typeTextAutocompleteMedidores(String value) {
		typeTextAutoCompleteWithPaste(TEXT_AUTOCOMPLETE_MEDIDORES, value);
	}
	
	public void clickButtonConfigurarParametros() {
		clickOnElement(BUTTON_CONFIGURAR_PARAMETROS);
		waitForPageToLoadUntil10s();
	}

	public void clickButtonAgendar() {
		clickOnElement(BUTTON_AGENDAR);
		waitForPageToLoadUntil10s();
	}

	public void keyTab() {
		useKey(TEXT_AUTOCOMPLETE_MEDIDORES, Keys.TAB);
		waitForPageToLoadUntil10s();
	}

	public void keySpace() {
		useKey(TEXT_AUTOCOMPLETE_MEDIDORES, Keys.TAB);
		waitForPageToLoadUntil10s();
	}

}