package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class TiposDeMedidorPage extends BaseHelperPage{
	
	private static final String LINK_NOVO = ".//div[2]/div/div[2]/p/a";
	private static final String GRID_BUTTON_EDITAR = "//*[@id='DataTables_Table_0']/tbody/tr%%/td[5]/div/a";
	private static final String GRID_LINK_MARCA = "//*[@id='DataTables_Table_0']/tbody/tr[1]/td[1]/a";
	private static final String LABEL_HEADER_MARCA = "//*[@id='DataTables_Table_0']/thead/tr[2]/th[contains(text(),'Marca')]";	
	private static final String LABEL_HEADER_MODELO = "//*[@id='DataTables_Table_0']/thead/tr[2]/th[contains(text(),'Modelo')]";	
	private static final String LABEL_HEADER_DESCRICAO = "//*[@id='DataTables_Table_0']/thead/tr[2]/th[contains(text(),'Descrição')]";	
	private static final String LABEL_HEADER_ABAS = "//*[@id='DataTables_Table_0']/thead/tr[2]/th[contains(text(),'Abas')]";	
		
	public void clickNovo(){
		clickOnElement(LINK_NOVO);
		waitForPageToLoadUntil10s();
	}
	
	public void getLabelHeaderMarca(String value){
		getLabel(LABEL_HEADER_MARCA, value);
	}
	
	public void getLabelHeaderModelo(String value){
		getLabel(LABEL_HEADER_MODELO, value);
	}
	
	public void getLabelHeaderDescricao(String value){
		getLabel(LABEL_HEADER_DESCRICAO, value);
	}
	
	public void getLabelHeaderAbas(String value){
		getLabel(LABEL_HEADER_ABAS, value);
	}
	
	public void clickGridButtonFirstLineEditar(String value){
		waitForPageToLoadUntil10s();
		clickGridButtonOnElement(GRID_BUTTON_EDITAR, value, 1);
	}
	
	public void clickGridFirstLineLinkMarca(String value){
		waitForPageToLoadUntil10s();
		clickGridButtonOnElement(GRID_LINK_MARCA, value, 1);
	}
	
}