package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.base.util.WaitElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo.AlterarAbaConstantesMedidoresDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page.AlteracaoAbaConstantesMedidoresPage;

public class AlterarAbaConstantesMedidoresStage extends BaseStage {
	private AlterarAbaConstantesMedidoresDPO alterarAbaConstantesMedidoresDPO;
	private AlteracaoAbaConstantesMedidoresPage alterarAbaConstantesMedidoresPage;
	
	public AlterarAbaConstantesMedidoresStage(String dp) {
		alterarAbaConstantesMedidoresDPO = loadDataProviderFile(AlterarAbaConstantesMedidoresDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alterarAbaConstantesMedidoresPage = initElementsFromPage(AlteracaoAbaConstantesMedidoresPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarAbaConstantesMedidoresDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alterarAbaConstantesMedidoresPage.clickAbaConstantes();
		FindElementUtil.acceptAlert();
		alterarAbaConstantesMedidoresPage.typeCheckAll(alterarAbaConstantesMedidoresDPO.getCheckAll());
		if(StringUtil.isNotBlankOrNotNull(alterarAbaConstantesMedidoresDPO.getCheckAll())){
			editModalValorKeConstante();
		} else {
			editModalValorKeConstanteForFilteredRegister();
		}
		alterarAbaConstantesMedidoresPage.typeTextFilterGrandeza(alterarAbaConstantesMedidoresDPO.getFilterGrandeza());
		alterarAbaConstantesMedidoresPage.typeTextFilterValorKeContante(alterarAbaConstantesMedidoresDPO.getFilterValorKeConstante());
	}

	private void editModalValorKeConstanteForFilteredRegister() {
		alterarAbaConstantesMedidoresPage.typeTextFilterGrandeza(alterarAbaConstantesMedidoresDPO.getFilterGrandeza());
		alterarAbaConstantesMedidoresPage.typeCheckBoxGridItemGrandeza(alterarAbaConstantesMedidoresDPO.getFilterGrandeza());
		for (int index=0; index < alterarAbaConstantesMedidoresDPO.getGridConstantes().size(); index++){
			if(StringUtil.isNotBlankOrNotNull(alterarAbaConstantesMedidoresDPO.getGridConstantes().get(index).getFlagItem())) {
				alterarAbaConstantesMedidoresPage.typeCheckBoxGridItems(alterarAbaConstantesMedidoresDPO.getGridConstantes().get(index).getFlagItem(), index+1);
				alterarAbaConstantesMedidoresPage.getGridLabelValueGrandeza(alterarAbaConstantesMedidoresDPO.getGridConstantes().get(index).getGrandeza(), index+1);
				alterarAbaConstantesMedidoresPage.getGridLabelValueValorKeConstante(alterarAbaConstantesMedidoresDPO.getGridConstantes().get(index).getValorKeConstante(), index+1);
				WaitElementUtil.waitForATime(TimePRM._3_SECS);
				editModalValorKeConstante();
			}
		}
	}

	private void editModalValorKeConstante() {
		alterarAbaConstantesMedidoresPage.isClickableButtonEditar();
		alterarAbaConstantesMedidoresPage.clickEditar();
		alterarAbaConstantesMedidoresPage.typeModalTextValorKeConstante(alterarAbaConstantesMedidoresDPO.getModalValorKeConstante());
		alterarAbaConstantesMedidoresPage.clickModalSalvar();
	}

	@Override
	public void runValidations() {
		alterarAbaConstantesMedidoresPage.validateMessageDefault(alterarAbaConstantesMedidoresDPO.getOperationMessage());
		validateGridFiltered();
	}

	private void validateGridFiltered() {
		alterarAbaConstantesMedidoresPage.getGridLabelValueGrandeza(alterarAbaConstantesMedidoresDPO.getGridConstantes().get(0).getGrandeza(), 1);
		alterarAbaConstantesMedidoresPage.getGridLabelValueValorKeConstante(alterarAbaConstantesMedidoresDPO.getGridConstantes().get(0).getValorKeConstante(), 1);
	}
}
