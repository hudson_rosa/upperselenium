package br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarUnidadesConsumidorasDPO extends BaseHelperDPO {

	private String numero;
	private String nome;
	private String ramoDeAtividade;
	private String etapa;
	private String postoTarifario;

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getRamoDeAtividade() {
		return ramoDeAtividade;
	}

	public void setRamoDeAtividade(String ramoDeAtividade) {
		this.ramoDeAtividade = ramoDeAtividade;
	}

	public String getEtapa() {
		return etapa;
	}

	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}

	public String getPostoTarifario() {
		return postoTarifario;
	}

	public void setPostoTarifario(String postoTarifario) {
		this.postoTarifario = postoTarifario;
	}

}