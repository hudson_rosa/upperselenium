package br.com.upperselenium.test.stage.configuracoes.cadastros.curvas_tipicas;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.base.util.WaitElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.curvas_tipicas.dpo.SalvarCurvasTipicasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.curvas_tipicas.page.CadastroCurvasTipicasPage;

public class SalvarCurvasTipicasStage extends BaseStage {
	private SalvarCurvasTipicasDPO cadastrarCurvasTipicasDPO;
	private CadastroCurvasTipicasPage cadastroCurvasTipicasPage;
	
	public SalvarCurvasTipicasStage(String dp) {
		cadastrarCurvasTipicasDPO = loadDataProviderFile(SalvarCurvasTipicasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroCurvasTipicasPage = initElementsFromPage(CadastroCurvasTipicasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarCurvasTipicasDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();
		cadastroCurvasTipicasPage.typeTextFilterPontoDeMedicao(cadastrarCurvasTipicasDPO.getFilterPontoDeMedicao());
		cadastroCurvasTipicasPage.typeTextFilterTipoDePonto(cadastrarCurvasTipicasDPO.getFilterTipoDePonto());
		cadastroCurvasTipicasPage.typeTextFilterPasta(cadastrarCurvasTipicasDPO.getFilterPasta());
		cadastroCurvasTipicasPage.typeTextFilterSituacaoComercial(cadastrarCurvasTipicasDPO.getFilterSituacaoComercial());
		cadastroCurvasTipicasPage.typeTextFilterCriterioDeDados(cadastrarCurvasTipicasDPO.getFilterCriterioDeDados());
		cadastroCurvasTipicasPage.typeCheckBoxGridItemPonto(cadastrarCurvasTipicasDPO.getFilterPontoDeMedicao());
		for (int index=0; index < cadastrarCurvasTipicasDPO.getGridCurvasTipicas().size(); index++){
			cadastroCurvasTipicasPage.typeCheckBoxGridItems(cadastrarCurvasTipicasDPO.getGridCurvasTipicas().get(index).getFlagItem(), index+1);
			cadastroCurvasTipicasPage.typeGridSelectCriterioDeDados(cadastrarCurvasTipicasDPO.getGridCurvasTipicas().get(index).getCriterioDeDados(), index+1);
			cadastroCurvasTipicasPage.typeGridTextF1(cadastrarCurvasTipicasDPO.getGridCurvasTipicas().get(index).getF1(), index+1);
			WaitElementUtil.waitForATime(TimePRM._3_SECS);
		}
		cadastroCurvasTipicasPage.isClickableButtonSalvar();
		cadastroCurvasTipicasPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroCurvasTipicasPage.validateMessageDefault(cadastrarCurvasTipicasDPO.getOperationMessage());
	}

}
