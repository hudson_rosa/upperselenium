package br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.dpo.CadastrarAbaPontosClientsSCDEDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.page.CadastroAbaPontosClientsSCDEPage;

public class CadastrarAbaPontosClientsSCDEStage extends BaseStage {
	private CadastrarAbaPontosClientsSCDEDPO cadastrarAbaPontosClientsSCDEDPO;
	private CadastroAbaPontosClientsSCDEPage cadastroAbaPontosClientsSCDEPage;
	
	public CadastrarAbaPontosClientsSCDEStage(String dp) {
		cadastrarAbaPontosClientsSCDEDPO = loadDataProviderFile(CadastrarAbaPontosClientsSCDEDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaPontosClientsSCDEPage = initElementsFromPage(CadastroAbaPontosClientsSCDEPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaPontosClientsSCDEDPO.getIssue());
		goToAbaPontos();
		execute();		
	}
	
	private void goToAbaPontos() {
		waitForPageToLoadUntil10s();	
		cadastroAbaPontosClientsSCDEPage.clickAbaPontos();
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();		
		cadastroAbaPontosClientsSCDEPage.typeTextFilterNome(cadastrarAbaPontosClientsSCDEDPO.getFilterNome());
		cadastroAbaPontosClientsSCDEPage.typeTextFilterPasta(cadastrarAbaPontosClientsSCDEDPO.getFilterPasta());
		cadastroAbaPontosClientsSCDEPage.typeTextFilterTipoDePonto(cadastrarAbaPontosClientsSCDEDPO.getFilterTipoDePonto());
		cadastroAbaPontosClientsSCDEPage.typeCheckBoxGridItemPonto(cadastrarAbaPontosClientsSCDEDPO.getFilterNome());
		FindElementUtil.acceptAlert();
		cadastroAbaPontosClientsSCDEPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaPontosClientsSCDEPage.validateMessageDefault(cadastrarAbaPontosClientsSCDEDPO.getOperationMessage());
	}
	
}
