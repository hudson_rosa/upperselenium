package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.GoToTiposDePontoStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.NavegarAlteracaoTiposDePontoStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.NavegarCadastroTiposDePontoStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.NavegarRegistroLinkTiposDePontoStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.NavegarTiposDePontoStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0012TiposDePontoNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0012-TiposDePonto", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Tipos de Ponto validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0012TiposDePontoNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToTiposDePontoStage());
		addStage(new NavegarTiposDePontoStage(getDP("NavegarTiposDePontoDP.json")));
		addStage(new GoToTiposDePontoStage());
		addStage(new NavegarCadastroTiposDePontoStage(getDP("NavegarCadastroTiposDePontoDP.json")));
		addStage(new NavegarRegistroLinkTiposDePontoStage(getDP("NavegarRegistroLinkTiposDePontoDP.json")));
		addStage(new NavegarAlteracaoTiposDePontoStage(getDP("NavegarAlteracaoTiposDePontoDP.json")));
	}	
}
