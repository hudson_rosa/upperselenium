package br.com.upperselenium.test.stage.configuracoes.cadastros.tensoes;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.tensoes.dpo.CadastrarTensoesDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.tensoes.page.CadastroTensoesPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.tensoes.page.TensoesPage;

public class CadastrarTensoesStage extends BaseStage {
	private CadastrarTensoesDPO cadastrarTensoesDPO;
	private TensoesPage tensoesPage;
	private CadastroTensoesPage cadastroTensoesPage;
	
	public CadastrarTensoesStage(String dp) {
		cadastrarTensoesDPO = loadDataProviderFile(CadastrarTensoesDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tensoesPage = initElementsFromPage(TensoesPage.class);
		cadastroTensoesPage = initElementsFromPage(CadastroTensoesPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarTensoesDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		tensoesPage.clickNovo();
		cadastroTensoesPage.typeTextValor(cadastrarTensoesDPO.getValor()+getRandomNumber());		
		cadastroTensoesPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroTensoesPage.validateMessageDefault(cadastrarTensoesDPO.getOperationMessage());
	}

}
