package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoTiposDeMedidorPage extends BaseHelperPage{
	
	private static final String LINK_ABA_GERAL = "//*[@id='abastemplates']/li[1]/a";	
	private static final String LINK_ABA_ABAS = "//*[@id='abastemplates']/li[2]/a";	
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 
	
	// ABA GERAL
	private static final String TEXT_MARCA = "//*[@id='Marca']";
	private static final String TEXT_MODELO = "//*[@id='Modelo']";
	private static final String SELECT_ESPECIE = "//*[@id='Especie']";
	private static final String TEXT_DESCRICAO = "//*[@id='Descricao']";
	
	// ABA ABAS
	private static final String LABEL_HEADER_ABAS = "//*[@id='dataTableList']/thead/tr/th[contains(text(),'Abas')]";
	private static final String LABEL_HEADER_VISUALIZAR = "//*[@id='dataTableList']/thead/tr/th[contains(text(),'Visualizar')]";
	
	public void clickAbaGeral(){
		waitForPageToLoadUntil10s();
		clickOnElement(LINK_ABA_GERAL);
	}
	
	public void clickAbaAbaS(){
		waitForPageToLoadUntil10s();
		clickOnElement(LINK_ABA_ABAS);
	}	
	
	public void typeTextMarca(String value){
		typeText(TEXT_MARCA, value);
	}	
	
	public void typeTextModelo(String value){
		typeText(TEXT_MODELO, value);
	}	

	public void typeSelectEspecie(String value){
		typeSelectComboOption(SELECT_ESPECIE, value);
	}
	
	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}	
	
	public void keyPageDown(){
		useKey(TEXT_MARCA, Keys.PAGE_DOWN);
	}	

	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
	public void getLabelHeaderAbas(String value){
		getLabel(LABEL_HEADER_ABAS, value);
	}
	
	public void getLabelHeaderVisualizar(String value){
		getLabel(LABEL_HEADER_VISUALIZAR, value);
	}

	public void getLabelButtonSalvar(String value){
		getLabel(BUTTON_SALVAR, value);
	}
	
}