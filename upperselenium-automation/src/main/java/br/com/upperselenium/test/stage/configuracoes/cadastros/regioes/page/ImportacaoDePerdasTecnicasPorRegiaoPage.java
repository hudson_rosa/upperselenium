package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class ImportacaoDePerdasTecnicasPorRegiaoPage extends BaseHelperPage{
	
	private static final String BUTTON_CHOOSE_FILE = "//*[@id='Arquivo']";
	private static final String BUTTON_IMPORTAR = "//*[@id='importar']";
	private static final String LABEL_ARQUIVO = ".//div[2]/div/div[2]/form/fieldset/div/div[1]/label";
	private static final String MESSAGE_ARQUIVO_ERROR = "//*[@id='Arquivo-error']";
				
	public boolean isClickableChooseFile() {
		return isDisplayedElement(BUTTON_CHOOSE_FILE);
	}
	
	public void clickChooseFile(){
		clickOnElement(BUTTON_CHOOSE_FILE);
		waitForPageToLoadUntil10s();
	}
	
	public boolean isClickableImportar() {
		return isDisplayedElement(BUTTON_IMPORTAR);
	}
	
	public void clickImportar(){
		clickOnElement(BUTTON_IMPORTAR);
		waitForPageToLoadUntil10s();
	}
	
	public String getLabelArquivo(){
		return getValueFromElement(LABEL_ARQUIVO);
	}		

	public String getMessageErrorArquivo(){
		return getValueFromElement(MESSAGE_ARQUIVO_ERROR);
	}		
	
}