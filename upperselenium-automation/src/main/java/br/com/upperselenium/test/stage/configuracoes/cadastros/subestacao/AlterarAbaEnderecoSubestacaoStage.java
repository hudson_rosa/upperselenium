package br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.dpo.AlterarAbaEnderecoSubestacaoDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.page.AlteracaoAbaEnderecoSubestacaoPage;

public class AlterarAbaEnderecoSubestacaoStage extends BaseStage {
	private AlterarAbaEnderecoSubestacaoDPO alterarAbaEnderecoSubestacaoDPO;
	private AlteracaoAbaEnderecoSubestacaoPage alteracaoAbaEnderecoSubestacaoPage;
	
	public AlterarAbaEnderecoSubestacaoStage(String dp) {
		alterarAbaEnderecoSubestacaoDPO = loadDataProviderFile(AlterarAbaEnderecoSubestacaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alteracaoAbaEnderecoSubestacaoPage = initElementsFromPage(AlteracaoAbaEnderecoSubestacaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarAbaEnderecoSubestacaoDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoAbaEnderecoSubestacaoPage.clickAbaEndereco();
		alteracaoAbaEnderecoSubestacaoPage.typeTextLogradouro(alterarAbaEnderecoSubestacaoDPO.getLogradouro());			
		alteracaoAbaEnderecoSubestacaoPage.typeTextNumero(alterarAbaEnderecoSubestacaoDPO.getNumero());			
		alteracaoAbaEnderecoSubestacaoPage.typeTextBairro(alterarAbaEnderecoSubestacaoDPO.getBairro());			
		alteracaoAbaEnderecoSubestacaoPage.typeTextCEP(alterarAbaEnderecoSubestacaoDPO.getCep());			
		alteracaoAbaEnderecoSubestacaoPage.typeSelectCidade(alterarAbaEnderecoSubestacaoDPO.getCidade());			
		alteracaoAbaEnderecoSubestacaoPage.typeSelectRegiao(alterarAbaEnderecoSubestacaoDPO.getRegiao());
		alteracaoAbaEnderecoSubestacaoPage.typeTextLatitude(alterarAbaEnderecoSubestacaoDPO.getLatitude());			
		alteracaoAbaEnderecoSubestacaoPage.typeTextLongitude(alterarAbaEnderecoSubestacaoDPO.getLongitude());			
		alteracaoAbaEnderecoSubestacaoPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoAbaEnderecoSubestacaoPage.validateMessageDefault(alterarAbaEnderecoSubestacaoDPO.getOperationMessage());
	}

}
