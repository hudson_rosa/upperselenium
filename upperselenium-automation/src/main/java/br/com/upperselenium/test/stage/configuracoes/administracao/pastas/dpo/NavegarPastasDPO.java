package br.com.upperselenium.test.stage.configuracoes.administracao.pastas.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarPastasDPO extends BaseHelperDPO {

	private String exportacaoExcel;

	public String getExportacaoExcel() {
		return exportacaoExcel;
	}

	public void setExportacaoExcel(String exportacaoExcel) {
		this.exportacaoExcel = exportacaoExcel;
	}

}