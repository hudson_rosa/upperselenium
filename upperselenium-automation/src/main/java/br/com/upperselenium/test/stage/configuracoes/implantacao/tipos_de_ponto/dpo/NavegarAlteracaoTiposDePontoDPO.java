package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarAlteracaoTiposDePontoDPO extends BaseHelperDPO {

	private String clickItem;
	private String labelAbaGeralNome;
	private String labelHeaderAbas;
	private String labelHeaderVisualizar;
	private String labelHeaderHabilitado;
	private String labelHeaderGrandeza;
	private String labelHeaderTipoDeGrandeza;
	private String labelHeaderIntegralizacao;
	private String labelHeaderEhMedida;
	private String labelMonitoraDadosDuplicados;
	private String labelButtonSalvar;

	public String getClickItem() {
		return clickItem;
	}

	public void setClickItem(String clickItem) {
		this.clickItem = clickItem;
	}

	public String getLabelAbaGeralNome() {
		return labelAbaGeralNome;
	}

	public void setLabelAbaGeralNome(String labelAbaGeralNome) {
		this.labelAbaGeralNome = labelAbaGeralNome;
	}

	public String getLabelHeaderAbas() {
		return labelHeaderAbas;
	}

	public void setLabelHeaderAbas(String labelHeaderAbas) {
		this.labelHeaderAbas = labelHeaderAbas;
	}

	public String getLabelHeaderVisualizar() {
		return labelHeaderVisualizar;
	}

	public void setLabelHeaderVisualizar(String labelHeaderVisualizar) {
		this.labelHeaderVisualizar = labelHeaderVisualizar;
	}

	public String getLabelHeaderHabilitado() {
		return labelHeaderHabilitado;
	}

	public void setLabelHeaderHabilitado(String labelHeaderHabilitado) {
		this.labelHeaderHabilitado = labelHeaderHabilitado;
	}

	public String getLabelHeaderGrandeza() {
		return labelHeaderGrandeza;
	}

	public void setLabelHeaderGrandeza(String labelHeaderGrandeza) {
		this.labelHeaderGrandeza = labelHeaderGrandeza;
	}

	public String getLabelHeaderTipoDeGrandeza() {
		return labelHeaderTipoDeGrandeza;
	}

	public void setLabelHeaderTipoDeGrandeza(String labelHeaderTipoDeGrandeza) {
		this.labelHeaderTipoDeGrandeza = labelHeaderTipoDeGrandeza;
	}

	public String getLabelHeaderIntegralizacao() {
		return labelHeaderIntegralizacao;
	}

	public void setLabelHeaderIntegralizacao(String labelHeaderIntegralizacao) {
		this.labelHeaderIntegralizacao = labelHeaderIntegralizacao;
	}

	public String getLabelHeaderEhMedida() {
		return labelHeaderEhMedida;
	}

	public void setLabelHeaderEhMedida(String labelHeaderEhMedida) {
		this.labelHeaderEhMedida = labelHeaderEhMedida;
	}

	public String getLabelMonitoraDadosDuplicados() {
		return labelMonitoraDadosDuplicados;
	}

	public void setLabelMonitoraDadosDuplicados(String labelMonitoraDadosDuplicados) {
		this.labelMonitoraDadosDuplicados = labelMonitoraDadosDuplicados;
	}

	public String getLabelButtonSalvar() {
		return labelButtonSalvar;
	}

	public void setLabelButtonSalvar(String labelButtonSalvar) {
		this.labelButtonSalvar = labelButtonSalvar;
	}

	@Override
	public String toString() {
		return "NavegarAlteracaoTiposDePontoDPO [clickItem=" + clickItem + ", labelAbaGeralNome=" + labelAbaGeralNome
				+ ", labelHeaderAbas=" + labelHeaderAbas + ", labelHeaderVisualizar=" + labelHeaderVisualizar
				+ ", labelHeaderHabilitado=" + labelHeaderHabilitado + ", labelHeaderGrandeza=" + labelHeaderGrandeza
				+ ", labelHeaderTipoDeGrandeza=" + labelHeaderTipoDeGrandeza + ", labelHeaderIntegralizacao="
				+ labelHeaderIntegralizacao + ", labelHeaderEhMedida=" + labelHeaderEhMedida
				+ ", labelMonitoraDadosDuplicados=" + labelMonitoraDadosDuplicados + ", labelButtonSalvar="
				+ labelButtonSalvar + "]";
	}

}