package br.com.upperselenium.test.stage.configuracoes.cadastros.curvas_tipicas.page;

import org.openqa.selenium.By;

import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroCurvasTipicasPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='btnSalvar']"; 
	private static final String TEXT_FILTER_PONTO_DE_MEDICAO = "//*[@id='curvasTipicasDT']/thead/tr[1]/th[2]/span/input";
	private static final String TEXT_FILTER_TIPO_DE_PONTO = "//*[@id='curvasTipicasDT']/thead/tr[1]/th[3]/span/input";
	private static final String TEXT_FILTER_PASTA = "//*[@id='curvasTipicasDT']/thead/tr[1]/th[4]/span/input";
	private static final String TEXT_FILTER_SITUACAO_COMERCIAL = "//*[@id='curvasTipicasDT']/thead/tr[1]/th[5]/span/input";
	private static final String TEXT_FILTER_CRITERIO_DE_DADOS = "//*[@id='curvasTipicasDT']/thead/tr[1]/th[6]/span/input";
	private static final String GRID_CHECK_PONTO = "//*[@id='curvasTipicasDT']/tbody/tr/td[1]/input";
	private static final String GRID_CHECK_ITEM = "//*[@id='curvasTipicasDT']/tbody/tr[%i%]/td[1]/input";
	private static final String GRID_SELECT_CRITERIO_DE_DADOS = "//*[@id='curvasTipicasDT']/tbody/tr[%i%]/td[6]/select";
	private static final String GRID_TEXT_F1 = "//*[@id='curvasTipicasDT']/tbody/tr[%i%]/td[7]/input";
		
	public void typeTextFilterPontoDeMedicao(String value){
		typeText(TEXT_FILTER_PONTO_DE_MEDICAO, value);
	}	
	
	public void typeTextFilterTipoDePonto(String value){
		typeText(TEXT_FILTER_TIPO_DE_PONTO, value);
	}	
	
	public void typeTextFilterPasta(String value){
		typeText(TEXT_FILTER_PASTA, value);
	}	

	public void typeTextFilterSituacaoComercial(String value){
		getBlockOverlay();
		typeText(TEXT_FILTER_SITUACAO_COMERCIAL, value);
		getBlockOverlay();
	}	
	
	public void typeTextFilterCriterioDeDados(String value){
		typeText(TEXT_FILTER_CRITERIO_DE_DADOS, value);
		getBlockOverlay();
	}	
	
	public void typeCheckBoxGridItemPonto(String value){
		typeGridCheckFirstFilteredItem(GRID_CHECK_PONTO, value);
	}
	
	public void typeGridSelectCriterioDeDados(String value, int index){		
		typeGridSelectComboOption(GRID_SELECT_CRITERIO_DE_DADOS, value, index);
	}	
	
	public void typeGridTextF1(String value, int index){
		typeGridText(GRID_TEXT_F1, value, index);
	}	
	
	public void typeCheckBoxGridItems(String value,int index){
		typeGridCheckOption(GRID_CHECK_ITEM, value, index);
	}
			
	public boolean isClickableButtonSalvar() {
		waitForATime(TimePRM._2_SECS);
		return isDisplayedElement(By.xpath(BUTTON_SALVAR));
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}