package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarAbaConstantesMedidoresDPO extends BaseHelperDPO {

	private String filterGrandeza;
	private String filterValorKeConstante;
	private String modalValorKeConstante;
	private String checkAll;
	private List<GridAlterarConstantesDPO> gridConstantes;

	public String getFilterGrandeza() {
		return filterGrandeza;
	}

	public void setFilterGrandeza(String filterGrandeza) {
		this.filterGrandeza = filterGrandeza;
	}

	public String getFilterValorKeConstante() {
		return filterValorKeConstante;
	}

	public void setFilterValorKeConstante(String filterValorKeConstante) {
		this.filterValorKeConstante = filterValorKeConstante;
	}

	public String getModalValorKeConstante() {
		return modalValorKeConstante;
	}

	public void setModalValorKeConstante(String modalValorKeConstante) {
		this.modalValorKeConstante = modalValorKeConstante;
	}

	public String getCheckAll() {
		return checkAll;
	}

	public void setCheckAll(String checkAll) {
		this.checkAll = checkAll;
	}

	public List<GridAlterarConstantesDPO> getGridConstantes() {
		return gridConstantes;
	}

	public void setGridConstantes(List<GridAlterarConstantesDPO> gridConstantes) {
		this.gridConstantes = gridConstantes;
	}

}