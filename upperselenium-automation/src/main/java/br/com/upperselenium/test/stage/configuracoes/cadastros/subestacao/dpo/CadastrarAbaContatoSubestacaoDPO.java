package br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaContatoSubestacaoDPO extends BaseHelperDPO {

	private String telefoneSE1;
	private String telefoneSE2;
	private String contato1;
	private String email1;
	private String telContato1;
	private String contato2;
	private String email2;
	private String telContato2;
	private String telContato3;

	public String getTelefoneSE1() {
		return telefoneSE1;
	}

	public void setTelefoneSE1(String telefoneSE1) {
		this.telefoneSE1 = telefoneSE1;
	}

	public String getTelefoneSE2() {
		return telefoneSE2;
	}

	public void setTelefoneSE2(String telefoneSE2) {
		this.telefoneSE2 = telefoneSE2;
	}

	public String getContato1() {
		return contato1;
	}

	public void setContato1(String contato1) {
		this.contato1 = contato1;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getTelContato1() {
		return telContato1;
	}

	public void setTelContato1(String telContato1) {
		this.telContato1 = telContato1;
	}

	public String getContato2() {
		return contato2;
	}

	public void setContato2(String contato2) {
		this.contato2 = contato2;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public String getTelContato2() {
		return telContato2;
	}

	public void setTelContato2(String telContato2) {
		this.telContato2 = telContato2;
	}

	public String getTelContato3() {
		return telContato3;
	}

	public void setTelContato3(String telContato3) {
		this.telContato3 = telContato3;
	}

}