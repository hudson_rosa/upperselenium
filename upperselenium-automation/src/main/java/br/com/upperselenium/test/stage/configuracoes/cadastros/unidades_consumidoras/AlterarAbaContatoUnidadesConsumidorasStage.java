package br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.dpo.AlterarAbaContatoUnidadesConsumidorasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.page.AlteracaoAbaContatoUnidadesConsumidorasPage;

public class AlterarAbaContatoUnidadesConsumidorasStage extends BaseStage {
	private AlterarAbaContatoUnidadesConsumidorasDPO alterarAbaContatoUnidadesConsumidorasDPO;
	private AlteracaoAbaContatoUnidadesConsumidorasPage alteracaoAbaContatoUnidadesConsumidorasPage;
	
	public AlterarAbaContatoUnidadesConsumidorasStage(String dp) {
		alterarAbaContatoUnidadesConsumidorasDPO = loadDataProviderFile(AlterarAbaContatoUnidadesConsumidorasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alteracaoAbaContatoUnidadesConsumidorasPage = initElementsFromPage(AlteracaoAbaContatoUnidadesConsumidorasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarAbaContatoUnidadesConsumidorasDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoAbaContatoUnidadesConsumidorasPage.clickAbaContato();
		if(StringUtil.isNotBlankOrNotNull(alterarAbaContatoUnidadesConsumidorasDPO.getButtonAdicionar())){
			alteracaoAbaContatoUnidadesConsumidorasPage.clickAdicionar();
			alteracaoAbaContatoUnidadesConsumidorasPage.typeModalTextNome(alterarAbaContatoUnidadesConsumidorasDPO.getModalNome());
			alteracaoAbaContatoUnidadesConsumidorasPage.typeModalTextEmail(alterarAbaContatoUnidadesConsumidorasDPO.getModalEmail());
			alteracaoAbaContatoUnidadesConsumidorasPage.typeModalTextTelefone(alterarAbaContatoUnidadesConsumidorasDPO.getModalTelefone());
			alteracaoAbaContatoUnidadesConsumidorasPage.clickModalSalvar();
			alteracaoAbaContatoUnidadesConsumidorasPage.validateMessageDefault(alterarAbaContatoUnidadesConsumidorasDPO.getOperationMessage());
		}
		alteracaoAbaContatoUnidadesConsumidorasPage.typeTextFilterNome(alterarAbaContatoUnidadesConsumidorasDPO.getFilterNome());
		alteracaoAbaContatoUnidadesConsumidorasPage.typeTextFilterEmail(alterarAbaContatoUnidadesConsumidorasDPO.getFilterEmail());
	}

	@Override
	public void runValidations() {
		validateGridValueNome();
		validateGridValueEmail();
		validateGridValueTelefone();
	}

	private void validateGridValueNome() {
		alteracaoAbaContatoUnidadesConsumidorasPage.validateEqualsToValues(
				alterarAbaContatoUnidadesConsumidorasDPO.getColNome(), 
				alteracaoAbaContatoUnidadesConsumidorasPage.getGridTextNome());		
	}
	
	private void validateGridValueEmail() {
		alteracaoAbaContatoUnidadesConsumidorasPage.validateEqualsToValues(
				alterarAbaContatoUnidadesConsumidorasDPO.getColEmail(), 
				alteracaoAbaContatoUnidadesConsumidorasPage.getGridTextEmail());			
	}
	
	private void validateGridValueTelefone() {
		alteracaoAbaContatoUnidadesConsumidorasPage.validateEqualsToValues(
				alterarAbaContatoUnidadesConsumidorasDPO.getColTelefone(), 
				alteracaoAbaContatoUnidadesConsumidorasPage.getGridTextTelefone());
	}

}
