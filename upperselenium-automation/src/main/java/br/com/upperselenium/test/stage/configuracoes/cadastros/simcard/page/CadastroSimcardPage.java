package br.com.upperselenium.test.stage.configuracoes.cadastros.simcard.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroSimcardPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/form/fieldset/div[5]/input";   
	private static final String TEXT_SID = "//*[@id='Sid']";
	private static final String TEXT_NUMERO = "//*[@id='Numero']";
	private static final String TEXT_OPERADORA = "//*[@id='Operadora']";
	private static final String TEXT_GATEWAY = "//*[@id='Gateway_Nome']";
	
	public void typeTextSID(String value){
		typeText(TEXT_SID, value);
	}
	
	public void typeTextNumero(String value){
		typeText(TEXT_NUMERO, value);
	}
	
	public void typeTextOperadora(String value){
		typeText(TEXT_OPERADORA, value);
	}
	
	public void typeTextGateway(String value){
		typeText(TEXT_GATEWAY, value);
	}
		
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}