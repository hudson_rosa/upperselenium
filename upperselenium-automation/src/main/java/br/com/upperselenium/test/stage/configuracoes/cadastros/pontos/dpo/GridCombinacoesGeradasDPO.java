package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

public class GridCombinacoesGeradasDPO {

	private String flagItem;
	private String flagEdit;
	private String filterChaveCombinacao;
	private String filterPontoAplicado;
	private String gridValueChaveCombinacao;
	private String gridValuePontoAplicado;
	private String modalPonto;

	public String getFlagItem() {
		return flagItem;
	}

	public void setFlagItem(String flagItem) {
		this.flagItem = flagItem;
	}

	public String getFlagEdit() {
		return flagEdit;
	}

	public void setFlagEdit(String flagEdit) {
		this.flagEdit = flagEdit;
	}

	public String getFilterChaveCombinacao() {
		return filterChaveCombinacao;
	}

	public void setFilterChaveCombinacao(String filterChaveCombinacao) {
		this.filterChaveCombinacao = filterChaveCombinacao;
	}

	public String getFilterPontoAplicado() {
		return filterPontoAplicado;
	}

	public void setFilterPontoAplicado(String filterPontoAplicado) {
		this.filterPontoAplicado = filterPontoAplicado;
	}

	public String getGridValueChaveCombinacao() {
		return gridValueChaveCombinacao;
	}

	public void setGridValueChaveCombinacao(String gridValueChaveCombinacao) {
		this.gridValueChaveCombinacao = gridValueChaveCombinacao;
	}

	public String getGridValuePontoAplicado() {
		return gridValuePontoAplicado;
	}

	public void setGridValuePontoAplicado(String gridValuePontoAplicado) {
		this.gridValuePontoAplicado = gridValuePontoAplicado;
	}

	public String getModalPonto() {
		return modalPonto;
	}

	public void setModalPonto(String modalPonto) {
		this.modalPonto = modalPonto;
	}

}
