package br.com.upperselenium.test.stage.configuracoes.implantacao.configuracao_do_sistema.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoConfDoSistemaPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/form/p/input";
	private static final String LINK_ABA_ADMINISTRACAO = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Administração')]";
	private static final String LINK_ABA_ALARMES = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Alarmes')]";
	private static final String LINK_ABA_API = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'API')]";
	private static final String LINK_ABA_CONNECTION_MANAGER = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Connection Manager')]";
	private static final String LINK_ABA_CREG = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'CREG')]";
	private static final String LINK_ABA_DASHBOARD = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Dashboard')]";
	private static final String LINK_ABA_DEMO = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Demo')]";
	private static final String LINK_ABA_DIAGRAMA_FASORIAL = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Diagrama Fasorial')]";
	private static final String LINK_ABA_EMAIL = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Email')]";
	private static final String LINK_ABA_EXPORTACAO_PARA_UE = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Exportação para UE')]";
	private static final String LINK_ABA_EXPORTACOES = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Exportações')]";
	private static final String LINK_ABA_FATURAMENTO = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Faturamento')]";
	private static final String LINK_ABA_GRAFICOS = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Gráficos')]";
	private static final String LINK_ABA_INTEGRACAO_SCDE = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Integração SCDE')]";
	private static final String LINK_ABA_INTEGRACAO_UE = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Integração UE')]";
	private static final String LINK_ABA_LDAP = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'LDAP')]";
	private static final String LINK_ABA_LOAD_PROFILE = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Load Profile')]";
	private static final String LINK_ABA_MAPA_POR_TRAFO_BT = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Mapa por Trafo BT')]";
	private static final String LINK_ABA_PONTOS = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Pontos')]";
	private static final String LINK_ABA_RELATORIOS = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Relatórios')]";
	private static final String LINK_ABA_SAP_PM = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'SAP PM')]";
	private static final String LINK_ABA_SATELITE_NOAA = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Satélite NOAA')]";
	private static final String LINK_ABA_SOA = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'SOA')]";
	private static final String LINK_ABA_TASKS = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Tasks')]";
	private static final String LINK_ABA_TEMPO_REAL = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'Tempo Real')]";
	private static final String LINK_ABA_WEBSERVICES = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), 'WebServices')]";
	private static final String LINK_ABA_OOO = ".//div[2]/div/div[2]/form/div/ul/li/a[contains(text(), '...')]";
	private static final String LABEL_HEADER_CHAVE = "//*[@class='tab-pane']/table/thead/tr/th[contains(text(),'Chave')]";
	private static final String LABEL_HEADER_VALOR = "//*[@class='tab-pane']/table/thead/tr/th[contains(text(),'Valor')]";
	
	
	public void clickAbaAdministracao(){
		waitForPresenceOfElementLocated(LINK_ABA_ADMINISTRACAO);
		clickOnElement(LINK_ABA_ADMINISTRACAO);
	}
	
	public void clickAbaAlarmes(){
		waitForPresenceOfElementLocated(LINK_ABA_ALARMES);
		clickOnElement(LINK_ABA_ALARMES);
	}
	
	public void clickAbaApi(){
		waitForPresenceOfElementLocated(LINK_ABA_API);
		clickOnElement(LINK_ABA_API);
	}
	
	public void clickAbaConnectionManager(){
		waitForPresenceOfElementLocated(LINK_ABA_CONNECTION_MANAGER);
		clickOnElement(LINK_ABA_CONNECTION_MANAGER);
	}
	
	public void clickAbaCreg(){
		waitForPresenceOfElementLocated(LINK_ABA_CREG);
		clickOnElement(LINK_ABA_CREG);
	}
	
	public void clickAbaDashboard(){
		waitForPresenceOfElementLocated(LINK_ABA_DASHBOARD);
		clickOnElement(LINK_ABA_DASHBOARD);
	}
	
	public void clickAbaDemo(){
		waitForPresenceOfElementLocated(LINK_ABA_DEMO);
		clickOnElement(LINK_ABA_DEMO);
	}
	
	public void clickAbaDiagramaFasorial(){
		waitForPresenceOfElementLocated(LINK_ABA_DIAGRAMA_FASORIAL);
		clickOnElement(LINK_ABA_DIAGRAMA_FASORIAL);
	}
	
	public void clickAbaEmail(){
		waitForPresenceOfElementLocated(LINK_ABA_EMAIL);
		clickOnElement(LINK_ABA_EMAIL);
	}
	
	public void clickAbaExportacaoParaUE(){
		waitForPresenceOfElementLocated(LINK_ABA_EXPORTACAO_PARA_UE);
		clickOnElement(LINK_ABA_EXPORTACAO_PARA_UE);
	}
	
	public void clickAbaExportacoes(){
		waitForPresenceOfElementLocated(LINK_ABA_EXPORTACOES);
		clickOnElement(LINK_ABA_EXPORTACOES);
	}
	
	public void clickAbaFaturamento(){
		waitForPresenceOfElementLocated(LINK_ABA_FATURAMENTO);
		clickOnElement(LINK_ABA_FATURAMENTO);
	}
	
	public void clickAbaGraficos(){
		waitForPresenceOfElementLocated(LINK_ABA_GRAFICOS);
		clickOnElement(LINK_ABA_GRAFICOS);
	}
	
	public void clickAbaIntegracaoScde(){
		waitForPresenceOfElementLocated(LINK_ABA_INTEGRACAO_SCDE);
		clickOnElement(LINK_ABA_INTEGRACAO_SCDE);
	}
	
	public void clickAbaIntegracaoUE(){
		waitForPresenceOfElementLocated(LINK_ABA_INTEGRACAO_UE);
		clickOnElement(LINK_ABA_INTEGRACAO_UE);
	}
	
	public void clickAbaLdap(){
		waitForPresenceOfElementLocated(LINK_ABA_LDAP);
		clickOnElement(LINK_ABA_LDAP);
	}
	
	public void clickAbaLoadProfile(){
		waitForPresenceOfElementLocated(LINK_ABA_LOAD_PROFILE);
		clickOnElement(LINK_ABA_LOAD_PROFILE);
	}
	
	public void clickAbaMapaPorTrafoBt(){
		waitForPresenceOfElementLocated(LINK_ABA_MAPA_POR_TRAFO_BT);
		clickOnElement(LINK_ABA_MAPA_POR_TRAFO_BT);
	}
	
	public void clickAbaPontos(){
		waitForPresenceOfElementLocated(LINK_ABA_PONTOS);
		clickOnElement(LINK_ABA_PONTOS);
	}
	
	public void clickAbaRelatorios(){
		waitForPresenceOfElementLocated(LINK_ABA_RELATORIOS);
		clickOnElement(LINK_ABA_RELATORIOS);
	}
	
	public void clickAbaSAPPM(){
		waitForPresenceOfElementLocated(LINK_ABA_SAP_PM);
		clickOnElement(LINK_ABA_SAP_PM);
	}
	
	public void clickAbaSateliteNOAA(){
		waitForPresenceOfElementLocated(LINK_ABA_SATELITE_NOAA);
		clickOnElement(LINK_ABA_SATELITE_NOAA);
	}
	
	public void clickAbaSOA(){
		waitForPresenceOfElementLocated(LINK_ABA_SOA);
		clickOnElement(LINK_ABA_SOA);
	}
	
	public void clickAbaTasks(){
		waitForPresenceOfElementLocated(LINK_ABA_TASKS);
		clickOnElement(LINK_ABA_TASKS);
	}
	
	public void clickAbaTempoReal(){
		waitForPresenceOfElementLocated(LINK_ABA_TEMPO_REAL);
		clickOnElement(LINK_ABA_TEMPO_REAL);
	}
	
	public void clickAbaWebServices(){
		waitForPresenceOfElementLocated(LINK_ABA_WEBSERVICES);
		clickOnElement(LINK_ABA_WEBSERVICES);
	}
	
	public void clickAbaOOO(){
		waitForPresenceOfElementLocated(LINK_ABA_OOO);
		clickOnElement(LINK_ABA_OOO);
	}

	public void getLabelHeaderChave(String value){
		getLabel(LABEL_HEADER_CHAVE, value);
	}
	
	public void getLabelHeaderValor(String value){
		getLabel(LABEL_HEADER_VALOR, value);
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}