package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.page.PerdasTecnicasPage;

public class LinkToImportacaoDePerdasTecnicasPorRegiaoStage extends BaseStage {
	private PerdasTecnicasPage perdasTecnicasPage;
	
	public LinkToImportacaoDePerdasTecnicasPorRegiaoStage() {}

	@Override
	public void initMappedPages() {
		perdasTecnicasPage = initElementsFromPage(PerdasTecnicasPage.class);
	}

	@Override
	public void runStage() {
		navigateToPage();	
	}
	
	private void navigateToPage() {
		waitForPageToLoadUntil10s();
		perdasTecnicasPage.clickImportacaoDeDadosDePerdasTecnicasPorRegiao();
	}

	@Override
	public void runValidations() {}

}
