package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.page.RegioesPage;

public class LinkToPerdasTecnicasStage extends BaseStage {
	private RegioesPage regioesPage;
	
	public LinkToPerdasTecnicasStage() {}

	@Override
	public void initMappedPages() {
		regioesPage = initElementsFromPage(RegioesPage.class);
	}

	@Override
	public void runStage() {
		navigateToPage();	
	}
	
	private void navigateToPage() {
		waitForPageToLoadUntil10s();
		regioesPage.clickPerdasTecnicas();
	}

	@Override
	public void runValidations() {}

}
