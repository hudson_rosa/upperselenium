package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.GoToTiposDePontoCCEEStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.NavegarAlteracaoTiposDePontoCCEEStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.NavegarCadastroTiposDePontoCCEEStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.NavegarRegistroLinkTiposDePontoCCEEStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.NavegarTiposDePontoCCEEStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0013TiposDePontoCCEENavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0013-TiposDePontoCCEE", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Tipos de Ponto CCEE validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0013TiposDePontoCCEENavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToTiposDePontoCCEEStage());
		addStage(new NavegarTiposDePontoCCEEStage(getDP("NavegarTiposDePontoCCEEDP.json")));
		addStage(new GoToTiposDePontoCCEEStage());
		addStage(new NavegarCadastroTiposDePontoCCEEStage(getDP("NavegarCadastroTiposDePontoCCEEDP.json")));
		addStage(new NavegarRegistroLinkTiposDePontoCCEEStage(getDP("NavegarRegistroLinkTiposDePontoCCEEDP.json")));
		addStage(new NavegarAlteracaoTiposDePontoCCEEStage(getDP("NavegarAlteracaoTiposDePontoCCEEDP.json")));
	}	
}
