package br.com.upperselenium.test.stage.wrapper.menu_area.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class SideBarMedicaoPage extends BaseHelperPage {

	private static final String LINK_ACOMPANHAMENTO_DIARIO_DA_COLETA = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Acompanhamento Diário da Coleta')]";
	private static final String LINK_ACOMPANHAMENTO_DIARIO_DE_USINAS = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Acompanhamento Diário das Usinas')]";
	private static final String LINK_COMANDOS_DO_GATEWAY = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Comandos do Gateway')]";
	private static final String LINK_COMANDOS_DO_MEDIDOR = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Comandos do Medidor')]";
	private static final String LINK_CONSOLIDACAO = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Consolidação')]";
	private static final String LINK_CONSOLIDACAO_DE_DADOS_ANEMOMETRICOS = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Consolidação de Dados Anemométricos')]";
	private static final String LINK_COLETA_DE_SEDIMENTOS_NA_AGUA = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Coleta de Sedimentos na Água')]";
	private static final String LINK_CURVAS_CHAVE = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Curvas-Chave')]";
	private static final String LINK_EVENTOS_DO_GATEWAY = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Eventos do Gateway')]";
	private static final String LINK_EVENTOS_DO_MEDIDOR = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Eventos do Medidor')]";
	private static final String LINK_EXPORTACAO_PRN = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Exportação PRN')]";
	private static final String LINK_EXPORTACOES = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Exportações')]";
	private static final String LINK_IMPORTACAO_DE_ARQUIVO_DE_CIRCUITOS = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Importação de Arquivo de Circuitos')]";
	private static final String LINK_IMPORTACAO_DE_DADOS = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Importação de Dados')]";
	private static final String LINK_IMPORTACAO_DE_FATURAS = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Importação de Faturas')]";
	private static final String LINK_INTEGRACAO_COM_SME_30_EM_30_MIN = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Integração com SME - 30 em 30 min')]";
	private static final String LINK_INTEGRACAO_COM_SME = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Integração com SME')]";
	private static final String LINK_INTEGRACAO_RETROATIVA_COM_A_ANA = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Integração Retroativa com a ANA')]";
	private static final String LINK_INTEGRACAO_RETROATIVA_COM_SME = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Integração Retroativa com SME')]";
	private static final String LINK_INTEGRACAO_RETROATIVA_COM_SME_30_EM_30_MIN = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Integração Retroativa com SME - 30 em 30 min')]";
	private static final String LINK_ORIGEM_DE_DADOS_ANEMOMETRICOS = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Origem de Dados Anemométricos')]";
	private static final String LINK_PROGRAMACOES = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Programações')]";
	private static final String LINK_RELATORIO_DE_DATA_DA_ULTIMA_COLETA_NEXUS = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Relatório de Data da Última Coleta - NEXUS')]";
	private static final String LINK_RELATORIO_DE_STATUS_DE_RELOGIO = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Relatório de Status De Relógio')]";
	private static final String LINK_SINCRONIZACAO_MANUAL = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Sincronização Manual')]";
	private static final String LINK_STATUS_DE_IMPORTACAO_DE_DADOS = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Status de Importações de Dados')]";
	private static final String LINK_ULTIMO_ENVIO_DOS_XMLS_PARA_ANA = "//*[@id='tree-content']/div[6]/div/div/div/ul/li/a[contains(text(), 'Último Envio dos XMLs para ANA')]";

	public boolean isClickableLinkAcompanhamentoDiarioDaColeta() {
		return isDisplayedElement(LINK_ACOMPANHAMENTO_DIARIO_DA_COLETA);
	}

	public boolean isClickableLinkAcompanhamentoDiarioDeUsinas() {
		return isDisplayedElement(LINK_ACOMPANHAMENTO_DIARIO_DE_USINAS);
	}

	public boolean isClickableLinkComandosDoGateway() {
		return isDisplayedElement(LINK_COMANDOS_DO_GATEWAY);
	}

	public boolean isClickableLinkComandosDoMedidor() {
		return isDisplayedElement(LINK_COMANDOS_DO_MEDIDOR);
	}

	public boolean isClickableLinkConsolidacao() {
		return isDisplayedElement(LINK_CONSOLIDACAO);
	}

	public boolean isClickableLinkConsolidacaoDeDadosAnemometricos() {
		return isDisplayedElement(LINK_CONSOLIDACAO_DE_DADOS_ANEMOMETRICOS);
	}

	public boolean isClickableLinkColetaDeSedimentosNaAgua() {
		return isDisplayedElement(LINK_COLETA_DE_SEDIMENTOS_NA_AGUA);
	}

	public boolean isClickableLinkCurvasChave() {
		return isDisplayedElement(LINK_CURVAS_CHAVE);
	}

	public boolean isClickableLinkEventosDoGateway() {
		return isDisplayedElement(LINK_EVENTOS_DO_GATEWAY);
	}

	public boolean isClickableLinkEventosDoMedidor() {
		return isDisplayedElement(LINK_EVENTOS_DO_MEDIDOR);
	}

	public boolean isClickableLinkExportacaoPRN() {
		return isDisplayedElement(LINK_EXPORTACAO_PRN);
	}

	public boolean isClickableLinkExportacoes() {
		return isDisplayedElement(LINK_EXPORTACOES);
	}

	public boolean isClickableLinkImportacaoDeArquivoDeCircuitos() {
		return isDisplayedElement(LINK_IMPORTACAO_DE_ARQUIVO_DE_CIRCUITOS);
	}

	public boolean isClickableLinkImportacaoDeDados() {
		return isDisplayedElement(LINK_IMPORTACAO_DE_DADOS);
	}

	public boolean isClickableLinkImportacaoDeFaturas() {
		return isDisplayedElement(LINK_IMPORTACAO_DE_FATURAS);
	}

	public boolean isClickableLinkIntegracaoComSme30Em30Min() {
		return isDisplayedElement(LINK_INTEGRACAO_COM_SME_30_EM_30_MIN);
	}

	public boolean isClickableLinkIntegracaoComSme() {
		return isDisplayedElement(LINK_INTEGRACAO_COM_SME);
	}

	public boolean isClickableLinkIntegracaoRetroativaComAAna() {
		return isDisplayedElement(LINK_INTEGRACAO_RETROATIVA_COM_A_ANA);
	}

	public boolean isClickableLinkIntegracaoRetroativaComSme() {
		return isDisplayedElement(LINK_INTEGRACAO_RETROATIVA_COM_SME);
	}

	public boolean isClickableLinkntegracaoRetroativaComSme30Em30Min() {
		return isDisplayedElement(LINK_INTEGRACAO_RETROATIVA_COM_SME_30_EM_30_MIN);
	}

	public boolean isClickableLinkOrigemDeDadosAnemometricos() {
		return isDisplayedElement(LINK_ORIGEM_DE_DADOS_ANEMOMETRICOS);
	}

	public boolean isClickableLinkProgramacoes() {
		return isDisplayedElement(LINK_PROGRAMACOES);
	}

	public boolean isClickableLinkRelatorioDeDataDaUltimaColetaNexus() {
		return isDisplayedElement(LINK_RELATORIO_DE_DATA_DA_ULTIMA_COLETA_NEXUS);
	}

	public boolean isClickableLinkRelatorioDeStatusDeRelogio() {
		return isDisplayedElement(LINK_RELATORIO_DE_STATUS_DE_RELOGIO);
	}

	public boolean isClickableLinkSincronizacaoManual() {
		return isDisplayedElement(LINK_SINCRONIZACAO_MANUAL);
	}

	public boolean isClickableLinkStatusDeImportacaoDeDados() {
		return isDisplayedElement(LINK_STATUS_DE_IMPORTACAO_DE_DADOS);
	}

	public boolean isClickableLinkUltimoEnvioDosXmlsParaAna() {
		return isDisplayedElement(LINK_ULTIMO_ENVIO_DOS_XMLS_PARA_ANA);
	}

	public void clickAcompanhamentoDiarioDaColeta() {
		waitForElementToBeClickable(LINK_ACOMPANHAMENTO_DIARIO_DA_COLETA);
		clickOnElement(LINK_ACOMPANHAMENTO_DIARIO_DA_COLETA);
	}

	public void clickAcompanhamentoDiarioDeUsinas() {
		waitForElementToBeClickable(LINK_ACOMPANHAMENTO_DIARIO_DE_USINAS);
		clickOnElement(LINK_ACOMPANHAMENTO_DIARIO_DE_USINAS);
	}

	public void clickComandosDoGateway() {
		waitForElementToBeClickable(LINK_COMANDOS_DO_GATEWAY);
		clickOnElement(LINK_COMANDOS_DO_GATEWAY);
	}

	public void clickComandosDoMedidor() {
		waitForElementToBeClickable(LINK_COMANDOS_DO_MEDIDOR);
		clickOnElement(LINK_COMANDOS_DO_MEDIDOR);
	}

	public void clickConsolidacao() {
		waitForElementToBeClickable(LINK_CONSOLIDACAO);
		clickOnElement(LINK_CONSOLIDACAO);
	}

	public void clickConsolidacaoDeDadosAnemometricos() {
		waitForElementToBeClickable(LINK_CONSOLIDACAO_DE_DADOS_ANEMOMETRICOS);
		clickOnElement(LINK_CONSOLIDACAO_DE_DADOS_ANEMOMETRICOS);
	}

	public void clickColetaDeSedimentosNaAgua() {
		waitForElementToBeClickable(LINK_COLETA_DE_SEDIMENTOS_NA_AGUA);
		clickOnElement(LINK_COLETA_DE_SEDIMENTOS_NA_AGUA);
	}

	public void clickCurvasChave() {
		waitForElementToBeClickable(LINK_CURVAS_CHAVE);
		clickOnElement(LINK_CURVAS_CHAVE);
	}

	public void clickEventosDoGateway() {
		waitForElementToBeClickable(LINK_EVENTOS_DO_GATEWAY);
		clickOnElement(LINK_EVENTOS_DO_GATEWAY);
	}

	public void clickEventosDoMedidor() {
		waitForElementToBeClickable(LINK_EVENTOS_DO_MEDIDOR);
		clickOnElement(LINK_EVENTOS_DO_MEDIDOR);
	}

	public void clickExportacaoPRN() {
		waitForElementToBeClickable(LINK_EXPORTACAO_PRN);
		clickOnElement(LINK_EXPORTACAO_PRN);
	}

	public void clickExportacoes() {
		waitForElementToBeClickable(LINK_EXPORTACOES);
		clickOnElement(LINK_EXPORTACOES);
	}

	public void clickImportacaoDeArquivoDeCircuitos() {
		waitForElementToBeClickable(LINK_IMPORTACAO_DE_ARQUIVO_DE_CIRCUITOS);
		clickOnElement(LINK_IMPORTACAO_DE_ARQUIVO_DE_CIRCUITOS);
	}

	public void clickImportacaoDeDados() {
		waitForElementToBeClickable(LINK_IMPORTACAO_DE_DADOS);
		clickOnElement(LINK_IMPORTACAO_DE_DADOS);
	}

	public void clickImportacaoDeFaturas() {
		waitForElementToBeClickable(LINK_IMPORTACAO_DE_FATURAS);
		clickOnElement(LINK_IMPORTACAO_DE_FATURAS);
	}

	public void clickIntegracaoComSme30Em30Min() {
		waitForElementToBeClickable(LINK_INTEGRACAO_COM_SME_30_EM_30_MIN);
		clickOnElement(LINK_INTEGRACAO_COM_SME_30_EM_30_MIN);
	}

	public void clickIntegracaoComSme() {
		waitForElementToBeClickable(LINK_INTEGRACAO_COM_SME);
		clickOnElement(LINK_INTEGRACAO_COM_SME);
	}

	public void clickIntegracaoRetroativaComAAna() {
		waitForElementToBeClickable(LINK_INTEGRACAO_RETROATIVA_COM_A_ANA);
		clickOnElement(LINK_INTEGRACAO_RETROATIVA_COM_A_ANA);
	}

	public void clickIntegracaoRetroativaComSme() {
		waitForElementToBeClickable(LINK_INTEGRACAO_RETROATIVA_COM_SME);
		clickOnElement(LINK_INTEGRACAO_RETROATIVA_COM_SME);
	}

	public void clickntegracaoRetroativaComSme30Em30Min() {
		waitForElementToBeClickable(LINK_INTEGRACAO_RETROATIVA_COM_SME_30_EM_30_MIN);
		clickOnElement(LINK_INTEGRACAO_RETROATIVA_COM_SME_30_EM_30_MIN);
	}

	public void clickOrigemDeDadosAnemometricos() {
		waitForElementToBeClickable(LINK_ORIGEM_DE_DADOS_ANEMOMETRICOS);
		clickOnElement(LINK_ORIGEM_DE_DADOS_ANEMOMETRICOS);
	}

	public void clickProgramacoes() {
		waitForElementToBeClickable(LINK_PROGRAMACOES);
		clickOnElement(LINK_PROGRAMACOES);
	}

	public void clickRelatorioDeDataDaUltimaColetaNexus() {
		waitForElementToBeClickable(LINK_RELATORIO_DE_DATA_DA_ULTIMA_COLETA_NEXUS);
		clickOnElement(LINK_RELATORIO_DE_DATA_DA_ULTIMA_COLETA_NEXUS);
	}

	public void clickRelatorioDeStatusDeRelogio() {
		waitForElementToBeClickable(LINK_RELATORIO_DE_STATUS_DE_RELOGIO);
		clickOnElement(LINK_RELATORIO_DE_STATUS_DE_RELOGIO);
	}

	public void clickSincronizacaoManual() {
		waitForElementToBeClickable(LINK_SINCRONIZACAO_MANUAL);
		clickOnElement(LINK_SINCRONIZACAO_MANUAL);
	}

	public void clickStatusDeImportacaoDeDados() {
		waitForElementToBeClickable(LINK_STATUS_DE_IMPORTACAO_DE_DADOS);
		clickOnElement(LINK_STATUS_DE_IMPORTACAO_DE_DADOS);
	}

	public void clickUltimoEnvioDosXmlsParaAna() {
		waitForElementToBeClickable(LINK_ULTIMO_ENVIO_DOS_XMLS_PARA_ANA);
		clickOnElement(LINK_ULTIMO_ENVIO_DOS_XMLS_PARA_ANA);
	}

	public void keyPageDown() {
		useKey(LINK_ACOMPANHAMENTO_DIARIO_DA_COLETA, Keys.PAGE_DOWN);
	}

}