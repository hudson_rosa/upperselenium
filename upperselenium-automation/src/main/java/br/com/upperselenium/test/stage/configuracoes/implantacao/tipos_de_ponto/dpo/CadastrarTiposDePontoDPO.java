package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarTiposDePontoDPO extends BaseHelperDPO {

	private String nomeDefault;
	private String nomeRandom;
	private String descricao;
	private String grupoDeMedicao;
	private String monitoraDadosDuplicados;

	public String getNomeDefault() {
		return nomeDefault;
	}

	public void setNomeDefault(String nomeDefault) {
		this.nomeDefault = nomeDefault;
	}

	public String getNomeRandom() {
		return nomeRandom;
	}

	public void setNomeRandom(String nomeRandom) {
		this.nomeRandom = nomeRandom;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getGrupoDeMedicao() {
		return grupoDeMedicao;
	}

	public void setGrupoDeMedicao(String grupoDeMedicao) {
		this.grupoDeMedicao = grupoDeMedicao;
	}

	public String getMonitoraDadosDuplicados() {
		return monitoraDadosDuplicados;
	}

	public void setMonitoraDadosDuplicados(String monitoraDadosDuplicados) {
		this.monitoraDadosDuplicados = monitoraDadosDuplicados;
	}

}