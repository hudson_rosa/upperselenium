package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaCCEEPontosPage extends BaseHelperPage{
	
	private static final String LINK_ABA_CCEE = "//*[@id='tabs']/ul/li[4]/a";
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 	
	private static final String TEXT_CODIGO_SCDE = "//*[@id='CodigoDoScde']"; 
	private static final String TEXT_CODIGO_DO_ATIVO = "//*[@id='CodigoDaCcee']";
	private static final String TEXT_NOME_DO_ATIVO = "//*[@id='NomeDoAtivo']";
	private static final String SELECT_AGENTE_CONECTANTE = "//*[@id='AgenteConectante']";
	private static final String SELECT_AGENTE_CONECTADO = "//*[@id='AgenteConectado']";
	private static final String SELECT_AGENTE_DE_MEDICAO = "//*[@id='AgenteDeMedicao']";
	private static final String SELECT_TIPO = "//*[@id='TipoCcee']";
	private static final String TEXT_CODIGO_DO_AGENTE = "//*[@id='CodigoDoAgente']";
	private static final String TEXT_NOME_DO_AGENTE = "//*[@id='NomeDoAgente']";
	private static final String SELECT_CLIENT_SCDE = "//*[@id='ClientScde']";
	
	public void typeTextCodigoCCEE(String value){
		typeText(TEXT_CODIGO_SCDE, value);
	}	
	
	public void typeTextCodigoDoAtivo(String value){
		typeText(TEXT_CODIGO_DO_ATIVO, value);
	}	
	
	public void typeTextNomeDoAtivo(String value){
		typeText(TEXT_NOME_DO_ATIVO, value);
	}	

	public void typeSelectAgenteConectante(String value){		
		typeSelectComboOption(SELECT_AGENTE_CONECTANTE, value);
	}
	
	public void typeSelectAgenteConectado(String value){		
		typeSelectComboOption(SELECT_AGENTE_CONECTADO, value);
	}
	
	public void typeSelectAgenteDeMedicao(String value){		
		typeSelectComboOption(SELECT_AGENTE_DE_MEDICAO, value);
	}
	
	public void typeSelectTipo(String value){		
		typeSelectComboOption(SELECT_TIPO, value);
	}
	
	public void typeTextCodigoDoAgente(String value){
		typeText(TEXT_CODIGO_DO_AGENTE, value);
	}	
	
	public void typeTextNomeDoAgente(String value){
		typeText(TEXT_NOME_DO_AGENTE, value);
	}	
	
	public void typeSelectClientScde(String value){		
		typeSelectComboOption(SELECT_CLIENT_SCDE, value);
	}
		
	public void keyPageDown(){
		useKey(TEXT_CODIGO_SCDE, Keys.PAGE_DOWN);
	}	
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
				
	public void clickAbaCCEE(){
		clickOnElement(LINK_ABA_CCEE);
	}
	
}