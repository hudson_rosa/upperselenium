package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.dpo.NavegarAlteracaoTiposDePontoCCEEDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.page.AlteracaoTiposDePontoCCEEPage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.page.TiposDePontoCCEEPage;

public class NavegarAlteracaoTiposDePontoCCEEStage extends BaseStage {
	private NavegarAlteracaoTiposDePontoCCEEDPO navegarAlteracaoTiposDePontoDPO;
	private TiposDePontoCCEEPage tiposDePontoPage;
	private AlteracaoTiposDePontoCCEEPage alteracaoTiposDePontoPage;
	
	public NavegarAlteracaoTiposDePontoCCEEStage(String dp) {
		navegarAlteracaoTiposDePontoDPO = loadDataProviderFile(NavegarAlteracaoTiposDePontoCCEEDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDePontoPage = initElementsFromPage(TiposDePontoCCEEPage.class);
		alteracaoTiposDePontoPage = initElementsFromPage(AlteracaoTiposDePontoCCEEPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAlteracaoTiposDePontoDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		tiposDePontoPage.clickGridButtonFirstLineEditar(navegarAlteracaoTiposDePontoDPO.getClickItem());
		alteracaoTiposDePontoPage.getLabelButtonSalvar(navegarAlteracaoTiposDePontoDPO.getLabelButtonSalvar());
		alteracaoTiposDePontoPage.getBreadcrumbsText(navegarAlteracaoTiposDePontoDPO.getBreadcrumbs());
		alteracaoTiposDePontoPage.clickUpToBreadcrumbs(navegarAlteracaoTiposDePontoDPO.getUpToBreadcrumb());
		alteracaoTiposDePontoPage.getWelcomeTitle(navegarAlteracaoTiposDePontoDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
