package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.dpo.NavegarRegistroLinkTiposDePontoDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.page.AlteracaoTiposDePontoPage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.page.TiposDePontoPage;

public class NavegarRegistroLinkTiposDePontoStage extends BaseStage {
	private NavegarRegistroLinkTiposDePontoDPO navegarRegistroLinkTiposDePontoDPO;
	private TiposDePontoPage tiposDePontoPage;
	private AlteracaoTiposDePontoPage alteracaoTiposDePontoPage;
	
	public NavegarRegistroLinkTiposDePontoStage(String dp) {
		navegarRegistroLinkTiposDePontoDPO = loadDataProviderFile(NavegarRegistroLinkTiposDePontoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDePontoPage = initElementsFromPage(TiposDePontoPage.class);
		alteracaoTiposDePontoPage = initElementsFromPage(AlteracaoTiposDePontoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarRegistroLinkTiposDePontoDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		tiposDePontoPage.clickGridFirstLineLinkNome(navegarRegistroLinkTiposDePontoDPO.getClickItem());
		alteracaoTiposDePontoPage.getLabelMonitoraDadosDuplicados(navegarRegistroLinkTiposDePontoDPO.getLabelMonitoraDadosDuplicados());
		alteracaoTiposDePontoPage.clickAbaAbas();
		alteracaoTiposDePontoPage.getLabelHeaderAbas(navegarRegistroLinkTiposDePontoDPO.getLabelHeaderAbas());
		alteracaoTiposDePontoPage.getLabelHeaderVisualizar(navegarRegistroLinkTiposDePontoDPO.getLabelHeaderVisualizar());
		alteracaoTiposDePontoPage.clickAbaTemplates();
		alteracaoTiposDePontoPage.getLabelHeaderHabilitado(navegarRegistroLinkTiposDePontoDPO.getLabelHeaderHabilitado());
		alteracaoTiposDePontoPage.getLabelHeaderGrandeza(navegarRegistroLinkTiposDePontoDPO.getLabelHeaderGrandeza());
		alteracaoTiposDePontoPage.getLabelHeaderTipoDeGrandeza(navegarRegistroLinkTiposDePontoDPO.getLabelHeaderTipoDeGrandeza());
		alteracaoTiposDePontoPage.getLabelHeaderIntegralizacao(navegarRegistroLinkTiposDePontoDPO.getLabelHeaderIntegralizacao());
		alteracaoTiposDePontoPage.getLabelHeaderEhMedida(navegarRegistroLinkTiposDePontoDPO.getLabelHeaderEhMedida());
		alteracaoTiposDePontoPage.clickAbaIcones();
		alteracaoTiposDePontoPage.getLabelButtonSalvar(navegarRegistroLinkTiposDePontoDPO.getLabelButtonSalvar());
		alteracaoTiposDePontoPage.getBreadcrumbsText(navegarRegistroLinkTiposDePontoDPO.getBreadcrumbs());
		alteracaoTiposDePontoPage.clickUpToBreadcrumbs(navegarRegistroLinkTiposDePontoDPO.getUpToBreadcrumb());
		alteracaoTiposDePontoPage.getWelcomeTitle(navegarRegistroLinkTiposDePontoDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
