package br.com.upperselenium.test.stage.configuracoes.cadastros.fronteiras;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.fronteiras.dpo.CadastrarFronteirasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.fronteiras.page.CadastroFronteirasPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.fronteiras.page.FronteirasPage;

public class CadastrarFronteirasStage extends BaseStage {
	private CadastrarFronteirasDPO cadastrarFronteirasDPO;
	private FronteirasPage fronteirasPage;
	private CadastroFronteirasPage cadastroFronteirasPage;
	
	public CadastrarFronteirasStage(String dp) {
		cadastrarFronteirasDPO = loadDataProviderFile(CadastrarFronteirasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		fronteirasPage = initElementsFromPage(FronteirasPage.class);
		cadastroFronteirasPage = initElementsFromPage(CadastroFronteirasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarFronteirasDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		fronteirasPage.clickNova();
		cadastroFronteirasPage.typeTextNome(cadastrarFronteirasDPO.getNome()+getRandomStringSpaced());				
		cadastroFronteirasPage.typeTextPontoDeEnergiaRequerida(cadastrarFronteirasDPO.getPontoDeEnergiaRequerida());				
		cadastroFronteirasPage.typeTextPontoDePerdaTecnicaATMedida(cadastrarFronteirasDPO.getPontoDeperdaTecnicaATMedida());				
		cadastroFronteirasPage.typeTextPseudonimoDeSistemasDePerdasTecnicas(cadastrarFronteirasDPO.getPseudonimoDeSistemasDePerdasTecnicas()+getRandomStringSpaced());				
		cadastroFronteirasPage.typeTextSuprimentoRegional(cadastrarFronteirasDPO.getSuprimentoRegional());				
		cadastroFronteirasPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroFronteirasPage.validateMessageDefault(cadastrarFronteirasDPO.getOperationMessage());
	}

}
