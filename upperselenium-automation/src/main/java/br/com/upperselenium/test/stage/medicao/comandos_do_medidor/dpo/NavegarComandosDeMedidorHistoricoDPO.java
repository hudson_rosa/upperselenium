package br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarComandosDeMedidorHistoricoDPO extends BaseHelperDPO {

	private String labelHeaderMedidor;
	private String labelHeaderInstante;
	private String labelHeaderDescricao;
	private String labelHeaderStatus;
	private String labelHeaderErro;

	public String getLabelHeaderMedidor() {
		return labelHeaderMedidor;
	}

	public void setLabelHeaderMedidor(String labelHeaderMedidor) {
		this.labelHeaderMedidor = labelHeaderMedidor;
	}

	public String getLabelHeaderInstante() {
		return labelHeaderInstante;
	}

	public void setLabelHeaderInstante(String labelHeaderInstante) {
		this.labelHeaderInstante = labelHeaderInstante;
	}

	public String getLabelHeaderDescricao() {
		return labelHeaderDescricao;
	}

	public void setLabelHeaderDescricao(String labelHeaderDescricao) {
		this.labelHeaderDescricao = labelHeaderDescricao;
	}

	public String getLabelHeaderStatus() {
		return labelHeaderStatus;
	}

	public void setLabelHeaderStatus(String labelHeaderStatus) {
		this.labelHeaderStatus = labelHeaderStatus;
	}

	public String getLabelHeaderErro() {
		return labelHeaderErro;
	}

	public void setLabelHeaderErro(String labelHeaderErro) {
		this.labelHeaderErro = labelHeaderErro;
	}

	@Override
	public String toString() {
		return "NavegarComandosDeMedidorHistoricoDPO [labelHeaderMedidor=" + labelHeaderMedidor
				+ ", labelHeaderInstante=" + labelHeaderInstante + ", labelHeaderDescricao=" + labelHeaderDescricao
				+ ", labelHeaderStatus=" + labelHeaderStatus + ", labelHeaderErro=" + labelHeaderErro + "]";
	}

}