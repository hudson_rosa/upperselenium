package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarAbaHidrologiaPontosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastroAbaHidrologiaPontosPage;

public class CadastrarAbaHidrologiaPontosStage extends BaseStage {
	private CadastrarAbaHidrologiaPontosDPO cadastrarAbaHidrologiaPontosDPO;
	private CadastroAbaHidrologiaPontosPage cadastroAbaHidrologiaPontosPage;
	
	public CadastrarAbaHidrologiaPontosStage(String dp) {
		cadastrarAbaHidrologiaPontosDPO = loadDataProviderFile(CadastrarAbaHidrologiaPontosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaHidrologiaPontosPage = initElementsFromPage(CadastroAbaHidrologiaPontosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaHidrologiaPontosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaHidrologiaPontosPage.clickAbaHidrologia();
		FindElementUtil.acceptAlert();
		checkForTypeCodigoDaEstacao();
		cadastroAbaHidrologiaPontosPage.typeTextSiglaDaEstacao(cadastrarAbaHidrologiaPontosDPO.getSiglaDaEstacao());
		cadastroAbaHidrologiaPontosPage.typeSelectSituacao(cadastrarAbaHidrologiaPontosDPO.getSituacao());
		cadastroAbaHidrologiaPontosPage.typeSelectTipoDeEstacao(cadastrarAbaHidrologiaPontosDPO.getTipoDeEstacao());
		cadastroAbaHidrologiaPontosPage.typeTextLogin(cadastrarAbaHidrologiaPontosDPO.getLogin());
		cadastroAbaHidrologiaPontosPage.typeTextSenha(cadastrarAbaHidrologiaPontosDPO.getSenha());
		cadastroAbaHidrologiaPontosPage.typeTextCodigoFluviometrico(cadastrarAbaHidrologiaPontosDPO.getCodigoFluviometrico());
		cadastroAbaHidrologiaPontosPage.keyPageDown();
		cadastroAbaHidrologiaPontosPage.typeTextCodigoPluviometrico(cadastrarAbaHidrologiaPontosDPO.getCodigoPluviometrico());
		cadastroAbaHidrologiaPontosPage.typeTextParametroA(cadastrarAbaHidrologiaPontosDPO.getParametroA());
		cadastroAbaHidrologiaPontosPage.typeTextParametroB(cadastrarAbaHidrologiaPontosDPO.getParametroB());
		cadastroAbaHidrologiaPontosPage.typeTextParametroC(cadastrarAbaHidrologiaPontosDPO.getParametroC());
		cadastroAbaHidrologiaPontosPage.typeCheckUtilizarLoginESenhaAna(cadastrarAbaHidrologiaPontosDPO.getUtilizarLoginESenhaEstacaoANA());
		cadastroAbaHidrologiaPontosPage.getTooltipUtilizarLoginESenhaAna(cadastrarAbaHidrologiaPontosDPO.getTooltipUtilizarLoginESenhaEstacaoANA());
		cadastroAbaHidrologiaPontosPage.clickSalvar();
	}

	private void checkForTypeCodigoDaEstacao() {
		if(!StringUtil.isNotBlankOrNotNull(cadastrarAbaHidrologiaPontosDPO.getCodigoDaEstacaoHidrometeorogica())){
			cadastroAbaHidrologiaPontosPage.typeTextCodigoDaEstacaoHidrometeorologica(cadastrarAbaHidrologiaPontosDPO.getCodigoDaEstacaoHidrometeorogica()+getRandomStringNonSpaced());
		} else {
			cadastroAbaHidrologiaPontosPage.typeTextCodigoDaEstacaoHidrometeorologica(cadastrarAbaHidrologiaPontosDPO.getCodigoDaEstacaoHidrometeorogica());
		}
	}

	@Override
	public void runValidations() {
		cadastroAbaHidrologiaPontosPage.validateMessageDefault(cadastrarAbaHidrologiaPontosDPO.getOperationMessage());
	}

}
