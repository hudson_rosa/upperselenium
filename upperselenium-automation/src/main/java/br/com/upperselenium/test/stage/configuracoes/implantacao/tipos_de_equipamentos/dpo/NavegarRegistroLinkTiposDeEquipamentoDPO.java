package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarRegistroLinkTiposDeEquipamentoDPO extends BaseHelperDPO {

	private String clickItem;

	public String getClickItem() {
		return clickItem;
	}

	public void setClickItem(String clickItem) {
		this.clickItem = clickItem;
	}

	@Override
	public String toString() {
		return "NavegarAlteracaoPerfisDPO [clickItem=" + clickItem + "]";
	}

}