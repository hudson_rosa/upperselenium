package br.com.upperselenium.test.stage.configuracoes.cadastros.suprimentos_regionais;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.suprimentos_regionais.dpo.AlterarSuprimentosRegionaisDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.suprimentos_regionais.page.AlteracaoSuprimentosRegionaisPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.suprimentos_regionais.page.SuprimentosRegionaisPage;

public class AlterarSuprimentosRegionaisStage extends BaseStage {
	private AlterarSuprimentosRegionaisDPO alterarSuprimentosRegionaisDPO;
	private SuprimentosRegionaisPage suprimentosRegionaisPage;
	private AlteracaoSuprimentosRegionaisPage alteracaoSuprimentosRegionaisPage;
	
	public AlterarSuprimentosRegionaisStage(String dp) {
		alterarSuprimentosRegionaisDPO = loadDataProviderFile(AlterarSuprimentosRegionaisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		suprimentosRegionaisPage = initElementsFromPage(SuprimentosRegionaisPage.class);
		alteracaoSuprimentosRegionaisPage = initElementsFromPage(AlteracaoSuprimentosRegionaisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarSuprimentosRegionaisDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoSuprimentosRegionaisPage.typeTextNome(alterarSuprimentosRegionaisDPO.getNome()+getRandomStringSpaced());		
		alteracaoSuprimentosRegionaisPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoSuprimentosRegionaisPage.validateMessageDefault(alterarSuprimentosRegionaisDPO.getOperationMessage());
	}

}
