package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.AlterarUsuariosStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.CadastrarUsuariosStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.GoToUsuariosStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0027UsuariosCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0027-Usuarios", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Usuários realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0027UsuariosCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToUsuariosStage());
		addStage(new CadastrarUsuariosStage(getDP("CadastrarUsuariosDP.json")));
		addStage(new GoToUsuariosStage());
		addStage(new AlterarUsuariosStage(getDP("AlterarUsuariosDP.json")));
	}	
}
