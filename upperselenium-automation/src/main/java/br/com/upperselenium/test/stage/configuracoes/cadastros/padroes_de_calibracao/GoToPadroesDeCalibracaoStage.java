package br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.AreasMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.DropDownMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.SideBarCadastrosPage;

public class GoToPadroesDeCalibracaoStage extends BaseStage {
	private AreasMenuPage areasMenuPage;
	private DropDownMenuPage dropDownMenuPage;
	private SideBarCadastrosPage sideBarCadastrosPage;
	
	public GoToPadroesDeCalibracaoStage() {}

	@Override
	public void initMappedPages() {
		areasMenuPage = initElementsFromPage(AreasMenuPage.class);
		dropDownMenuPage = initElementsFromPage(DropDownMenuPage.class);
		sideBarCadastrosPage = initElementsFromPage(SideBarCadastrosPage.class);
	}

	@Override
	public void runStage() {
		navigateToPage();	
	}
	
	private void navigateToPage() {
		waitForPageToLoad(TimePRM._20_SECS);
		areasMenuPage.clickConfiguracoes();
		waitForPageToLoadUntil10s();
		dropDownMenuPage.clickCadastros();
		waitForPageToLoadUntil10s();
		sideBarCadastrosPage.clickPadroesDeCalibracao();
		waitForPageToLoadUntil10s();		
	}

	@Override
	public void runValidations() {}

}
