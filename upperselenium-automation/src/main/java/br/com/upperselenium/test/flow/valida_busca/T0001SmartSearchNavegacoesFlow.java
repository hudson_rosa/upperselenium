package br.com.upperselenium.test.flow.valida_busca;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.home.smartsearch.SmartSearchStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0001SmartSearchNavegacoesFlow.class)
@FlowParams(idTest = "PIM-SmartSearch-T0001", testDirPath = "", loginDirPath = "",
	goal = "Realiza a pesquisa, acesso ao link obtido e validar se o breadcrumbs corresponde ao conteúdo da pesquisa.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0001SmartSearchNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new SmartSearchStage(getDP("LoginDP.json"), getDP("SmartSearchPageDP.json")));
	}	
}
