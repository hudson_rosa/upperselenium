package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarAbaConfiguracoesPontosVirtuaisDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastroAbaConfiguracoesPontosVirtuaisPage;

public class CadastrarAbaConfiguracoesPontosVirtuaisStage extends BaseStage {
	private CadastrarAbaConfiguracoesPontosVirtuaisDPO cadastrarAbaConfiguracoesPontosVirtuaisDPO;
	private CadastroAbaConfiguracoesPontosVirtuaisPage cadastroAbaConfiguracoesPontosVirtuaisPage;
	
	public CadastrarAbaConfiguracoesPontosVirtuaisStage(String dp) {
		cadastrarAbaConfiguracoesPontosVirtuaisDPO = loadDataProviderFile(CadastrarAbaConfiguracoesPontosVirtuaisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaConfiguracoesPontosVirtuaisPage = initElementsFromPage(CadastroAbaConfiguracoesPontosVirtuaisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaConfiguracoesPontosVirtuaisPage.clickAbaConfiguracoes();
		FindElementUtil.acceptAlert();
		cadastroAbaConfiguracoesPontosVirtuaisPage.typeSelectQuadrante(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getQuadrante());
		cadastroAbaConfiguracoesPontosVirtuaisPage.typeSelectEnergiaDelEnergiaRec(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getEnergiaDelEnergiaRec());
		cadastroAbaConfiguracoesPontosVirtuaisPage.typeSelectSituacaoComercial(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getSituacaoComercial());
		cadastroAbaConfiguracoesPontosVirtuaisPage.typeSelectMedicaoA(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getMedicaoA());
		cadastroAbaConfiguracoesPontosVirtuaisPage.typeSelectTensaoDeMedicao(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getTensaoDeMedicao());
		cadastroAbaConfiguracoesPontosVirtuaisPage.keyPageDown();
		cadastroAbaConfiguracoesPontosVirtuaisPage.typeTextLimiteDePotenciaDeConsumo(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getLimiteDePotenciaDeConsumo());
		cadastroAbaConfiguracoesPontosVirtuaisPage.typeTextLimiteDePotenciaDeGeracao(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getLimiteDePotenciaDeGeracao());
		cadastroAbaConfiguracoesPontosVirtuaisPage.typeTextConexao(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getConexao());
		cadastroAbaConfiguracoesPontosVirtuaisPage.typeTextCapacidadeDeConexao(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getCapacidadeDeConexao());
		cadastroAbaConfiguracoesPontosVirtuaisPage.typeTextDemandaDimensionada(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getDemandaDimensionada());
		cadastroAbaConfiguracoesPontosVirtuaisPage.typeCheckBoxCompensacaoDePerdas(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getCompensacaoDePerdas());
		cadastroAbaConfiguracoesPontosVirtuaisPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaConfiguracoesPontosVirtuaisPage.validateMessageDefault(cadastrarAbaConfiguracoesPontosVirtuaisDPO.getOperationMessage());
	}

}
