package br.com.upperselenium.test.stage.configuracoes.cadastros.simcard;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.simcard.dpo.CadastrarSimcardDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.simcard.page.CadastroSimcardPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.simcard.page.SimcardPage;

public class CadastrarSimcardStage extends BaseStage {
	private CadastrarSimcardDPO cadastrarSimcardDPO;
	private SimcardPage simcardPage;
	private CadastroSimcardPage cadastroSimcardPage;
	
	public CadastrarSimcardStage(String dp) {
		cadastrarSimcardDPO = loadDataProviderFile(CadastrarSimcardDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		simcardPage = initElementsFromPage(SimcardPage.class);
		cadastroSimcardPage = initElementsFromPage(CadastroSimcardPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarSimcardDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		simcardPage.clickNovo();
		cadastroSimcardPage.typeTextSID(cadastrarSimcardDPO.getSid()+getRandomStringSpaced());		
		cadastroSimcardPage.typeTextNumero(cadastrarSimcardDPO.getNumero()+getRandomNumber());		
		cadastroSimcardPage.typeTextOperadora(cadastrarSimcardDPO.getOperadora()+getRandomStringSpaced());		
		cadastroSimcardPage.typeTextGateway(cadastrarSimcardDPO.getGateway());		
		cadastroSimcardPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroSimcardPage.validateMessageDefault(cadastrarSimcardDPO.getOperationMessage());
	}

}
