package br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.dpo.AlterarAbaEnderecoUnidadesConsumidorasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.page.AlteracaoAbaEnderecoUnidadesConsumidorasPage;

public class AlterarAbaEnderecoUnidadesConsumidorasStage extends BaseStage {
	private AlterarAbaEnderecoUnidadesConsumidorasDPO alterarAbaEnderecoUnidadesConsumidorasDPO;
	private AlteracaoAbaEnderecoUnidadesConsumidorasPage alteracaoAbaEnderecoUnidadesConsumidorasPage;
	
	public AlterarAbaEnderecoUnidadesConsumidorasStage(String dp) {
		alterarAbaEnderecoUnidadesConsumidorasDPO = loadDataProviderFile(AlterarAbaEnderecoUnidadesConsumidorasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alteracaoAbaEnderecoUnidadesConsumidorasPage = initElementsFromPage(AlteracaoAbaEnderecoUnidadesConsumidorasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarAbaEnderecoUnidadesConsumidorasDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoAbaEnderecoUnidadesConsumidorasPage.clickAbaEndereco();
		alteracaoAbaEnderecoUnidadesConsumidorasPage.typeTextLogradouro(alterarAbaEnderecoUnidadesConsumidorasDPO.getLogradouro());			
		alteracaoAbaEnderecoUnidadesConsumidorasPage.typeTextNumero(alterarAbaEnderecoUnidadesConsumidorasDPO.getNumero());			
		alteracaoAbaEnderecoUnidadesConsumidorasPage.typeTextBairro(alterarAbaEnderecoUnidadesConsumidorasDPO.getBairro());			
		alteracaoAbaEnderecoUnidadesConsumidorasPage.typeTextCEP(alterarAbaEnderecoUnidadesConsumidorasDPO.getCep());			
		alteracaoAbaEnderecoUnidadesConsumidorasPage.typeSelectCidade(alterarAbaEnderecoUnidadesConsumidorasDPO.getCidade());			
		alteracaoAbaEnderecoUnidadesConsumidorasPage.typeSelectRegiao(alterarAbaEnderecoUnidadesConsumidorasDPO.getRegiao());
		alteracaoAbaEnderecoUnidadesConsumidorasPage.typeTextLatitude(alterarAbaEnderecoUnidadesConsumidorasDPO.getLatitude());			
		alteracaoAbaEnderecoUnidadesConsumidorasPage.typeTextLongitude(alterarAbaEnderecoUnidadesConsumidorasDPO.getLongitude());			
		alteracaoAbaEnderecoUnidadesConsumidorasPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoAbaEnderecoUnidadesConsumidorasPage.validateMessageDefault(alterarAbaEnderecoUnidadesConsumidorasDPO.getOperationMessage());
	}

}
