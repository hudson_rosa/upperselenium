package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo.CadastrarAbaCCEEMedidoresDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page.CadastroAbaCCEEMedidoresPage;

public class CadastrarAbaCCEEMedidoresStage extends BaseStage {
	private CadastrarAbaCCEEMedidoresDPO cadastrarAbaCCEEMedidoresDPO;
	private CadastroAbaCCEEMedidoresPage cadastroAbaCCEEMedidoresPage;
	
	public CadastrarAbaCCEEMedidoresStage(String dp) {
		cadastrarAbaCCEEMedidoresDPO = loadDataProviderFile(CadastrarAbaCCEEMedidoresDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaCCEEMedidoresPage = initElementsFromPage(CadastroAbaCCEEMedidoresPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaCCEEMedidoresDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaCCEEMedidoresPage.clickAbaCCEE();
		FindElementUtil.acceptAlert();
		cadastroAbaCCEEMedidoresPage.typeTextCodigoSCDE(cadastrarAbaCCEEMedidoresDPO.getCodigoDoSCDE()+getRandomStringNonSpaced());
		cadastroAbaCCEEMedidoresPage.typeSelectQuadranteDEL(cadastrarAbaCCEEMedidoresDPO.getQuadranteDEL());
		cadastroAbaCCEEMedidoresPage.typeSelectQuadranteREC(cadastrarAbaCCEEMedidoresDPO.getQuadranteREC());
		cadastroAbaCCEEMedidoresPage.typeTextAlgoritmoDeCompDePerdas(cadastrarAbaCCEEMedidoresDPO.getAlgoritmoDeCompDePerdas());
		cadastroAbaCCEEMedidoresPage.typeCheckGeracaoDeXML(cadastrarAbaCCEEMedidoresDPO.getGeracaoDeXML());
		cadastroAbaCCEEMedidoresPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaCCEEMedidoresPage.validateMessageDefault(cadastrarAbaCCEEMedidoresDPO.getOperationMessage());
	}

}
