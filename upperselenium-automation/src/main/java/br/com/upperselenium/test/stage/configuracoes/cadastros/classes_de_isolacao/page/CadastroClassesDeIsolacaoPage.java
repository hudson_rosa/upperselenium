package br.com.upperselenium.test.stage.configuracoes.cadastros.classes_de_isolacao.page;

import org.openqa.selenium.By;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroClassesDeIsolacaoPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//form/fieldset/div[3]/input";
	private static final String TEXT_VALOR_KV = "//*[@id='Valor']";
	private static final String TEXT_DESCRICAO = "//*[@id='Descricao']";
	
	public void typeTextValor(String value){
		typeText(TEXT_VALOR_KV, value);
	}	
	
	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}	
	
	public boolean isClickableButtonSalvar() {
		return isDisplayedElement(By.xpath(BUTTON_SALVAR));
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}