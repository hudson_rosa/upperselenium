package br.com.upperselenium.test.stage.configuracoes.cadastros.feriados;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.feriados.dpo.AlterarFeriadosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.feriados.page.AlteracaoFeriadosPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.feriados.page.FeriadosPage;

public class AlterarFeriadosStage extends BaseStage {
	private AlterarFeriadosDPO alterarFeriadosDPO;
	private FeriadosPage feriadosPage;
	private AlteracaoFeriadosPage alteracaoFeriadosPage;
	
	public AlterarFeriadosStage(String dp) {
		alterarFeriadosDPO = loadDataProviderFile(AlterarFeriadosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		feriadosPage = initElementsFromPage(FeriadosPage.class);
		alteracaoFeriadosPage = initElementsFromPage(AlteracaoFeriadosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarFeriadosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();
		alteracaoFeriadosPage.typeTextNome(alterarFeriadosDPO.getNome()+getRandomStringSpaced());				
		alteracaoFeriadosPage.typeTextData(alterarFeriadosDPO.getDataDia());				
		alteracaoFeriadosPage.typeTextDescricao(alterarFeriadosDPO.getDescricao()+getRandomStringSpaced());				
		alteracaoFeriadosPage.typeSelectEstado(alterarFeriadosDPO.getEstado());				
		alteracaoFeriadosPage.typeSelectCidade(alterarFeriadosDPO.getCidade());				
		alteracaoFeriadosPage.typeSelectRegiao(alterarFeriadosDPO.getRegiao());				
		alteracaoFeriadosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoFeriadosPage.validateMessageDefault(alterarFeriadosDPO.getOperationMessage());
	}

}
