package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.dpo.NavegarAlteracaoTiposDeEquipamentoDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.page.AlteracaoTiposDeEquipamentoPage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.page.TiposDeEquipamentoPage;

public class NavegarAlteracaoTiposDeEquipamentoStage extends BaseStage {
	private NavegarAlteracaoTiposDeEquipamentoDPO navegarAlteracaoTiposDeEquipamentoDPO;
	private TiposDeEquipamentoPage tiposDeEquipamentoPage;
	private AlteracaoTiposDeEquipamentoPage alteracaoTiposDeEquipamentoPage;
	
	public NavegarAlteracaoTiposDeEquipamentoStage(String dp) {
		navegarAlteracaoTiposDeEquipamentoDPO = loadDataProviderFile(NavegarAlteracaoTiposDeEquipamentoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDeEquipamentoPage = initElementsFromPage(TiposDeEquipamentoPage.class);
		alteracaoTiposDeEquipamentoPage = initElementsFromPage(AlteracaoTiposDeEquipamentoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAlteracaoTiposDeEquipamentoDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		tiposDeEquipamentoPage.clickGridButtonFirstLineEditar(navegarAlteracaoTiposDeEquipamentoDPO.getClickItem());
		alteracaoTiposDeEquipamentoPage.getBreadcrumbsText(navegarAlteracaoTiposDeEquipamentoDPO.getBreadcrumbs());
		alteracaoTiposDeEquipamentoPage.clickUpToBreadcrumbs(navegarAlteracaoTiposDeEquipamentoDPO.getUpToBreadcrumb());
		alteracaoTiposDeEquipamentoPage.getWelcomeTitle(navegarAlteracaoTiposDeEquipamentoDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
