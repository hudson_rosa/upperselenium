package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.cidades.CadastrarCidadesStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.cidades.GoToCidadesStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0005CidadesCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0005-Cidades", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Cidades realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0005CidadesCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToCidadesStage());
		addStage(new CadastrarCidadesStage(getDP("CadastrarCidadesDP.json")));
	}	
}
