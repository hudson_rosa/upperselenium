package br.com.upperselenium.test.stage.home.smartsearch.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class ListItensSmartSearchDPO extends BaseHelperDPO {

	private String titulo;
	private String link;
	private String tags;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

}