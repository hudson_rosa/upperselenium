package br.com.upperselenium.test.stage.home.smartsearch;

import br.com.upperselenium.base.BaseReport;
import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.WebDriverMaster;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.home.smartsearch.dpo.SmartSearchDPO;
import br.com.upperselenium.test.stage.home.smartsearch.page.SmartSearchPage;
import br.com.upperselenium.test.stage.wrapper.login.dpo.LoginDPO;

public class SmartSearchStage extends BaseStage {
	private LoginDPO loginDPO;
	private SmartSearchDPO smartSearchDPO;
	private SmartSearchPage smartSearchPage;

	public SmartSearchStage(String dp1, String dp2) {
		loginDPO = loadDataProviderFile(LoginDPO.class, dp1);
		smartSearchDPO = loadDataProviderFile(SmartSearchDPO.class, dp2);
	}

	@Override
	public void initMappedPages() {
		smartSearchPage = initElementsFromPage(SmartSearchPage.class);
	}

	@Override
	public void runStage() {
		String searchingFor = smartSearchDPO.getTextForSearch();
		if (!StringUtil.isNotBlankOrNotNull(searchingFor)) {
			int counter = 0;
			for (int index = 0; index < smartSearchDPO.getEntitiesList().size(); index++) {
				counter = index + 1;
				waitForPageToLoadUntil10s();
				smartSearchPage.typeTextSmartSearch(smartSearchDPO.getEntitiesList().get(index).getTitulo());
				smartSearchPage.clickFoundItemTitulo(smartSearchDPO.getEntitiesList().get(index).getTitulo());
				waitForPageToLoadUntil10s();
				smartSearchPage.getBreadcrumbsText(smartSearchDPO.getBreadcrumbs());
				smartSearchPage.getWelcomeTitle(smartSearchDPO.getWelcomeTitle());
				if (!smartSearchPage.isClickableLogo()) {
					BaseReport.reportContentOfElementTitleFailed(							
							"FALHA LINK " + counter + " - NÃO ENCONTROU BREADCRUMB EM: "
									+ smartSearchDPO.getEntitiesList().get(index).getTitulo());
					WebDriverMaster.getWebDriver().get(loginDPO.getUrl());
				} else {
					BaseReport.reportContentOfElementTitleSuccess(
							"SUCESSO LINK" + counter + ": "	
									+ smartSearchDPO.getEntitiesList().get(index).getTitulo());
					WebDriverMaster.getWebDriver().get(loginDPO.getUrl());
				}
			}
		} else {
			waitForPageToLoadUntil10s();
			smartSearchPage.typeTextSmartSearch(smartSearchDPO.getTextForSearch());
			smartSearchPage.clickFoundItemTitulo(smartSearchDPO.getTextForSearch());
			waitForPageToLoadUntil10s();
			smartSearchPage.getBreadcrumbsText(smartSearchDPO.getBreadcrumbs());
			smartSearchPage.getWelcomeTitle(smartSearchDPO.getWelcomeTitle());
		}
		smartSearchPage.clickLinkLogo();
	}

	@Override
	public void runValidations() {
	}

}
