package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.feriados.CadastrarFeriadosStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.feriados.GoToFeriadosStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0011FeriadosCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0011-Feriados", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Feriados realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0011FeriadosCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToFeriadosStage());
		addStage(new CadastrarFeriadosStage(getDP("CadastrarFeriadosDP.json")));
	}	
}
