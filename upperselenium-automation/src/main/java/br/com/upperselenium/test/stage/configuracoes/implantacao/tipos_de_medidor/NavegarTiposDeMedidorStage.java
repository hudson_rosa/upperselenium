package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.dpo.NavegarTiposDeMedidorDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.page.TiposDeMedidorPage;

public class NavegarTiposDeMedidorStage extends BaseStage {
	private NavegarTiposDeMedidorDPO navegarTiposDeMedidorDPO;
	private TiposDeMedidorPage tiposDeMedidorPage;
	
	public NavegarTiposDeMedidorStage(String dp) {
		navegarTiposDeMedidorDPO = loadDataProviderFile(NavegarTiposDeMedidorDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDeMedidorPage = initElementsFromPage(TiposDeMedidorPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarTiposDeMedidorDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		tiposDeMedidorPage.getLabelHeaderMarca(navegarTiposDeMedidorDPO.getLabelMarca());
		tiposDeMedidorPage.getLabelHeaderModelo(navegarTiposDeMedidorDPO.getLabelModelo());
		tiposDeMedidorPage.getLabelHeaderDescricao(navegarTiposDeMedidorDPO.getLabelDescricao());
		tiposDeMedidorPage.getLabelHeaderAbas(navegarTiposDeMedidorDPO.getLabelAbas());
		tiposDeMedidorPage.getBreadcrumbsText(navegarTiposDeMedidorDPO.getBreadcrumbs());
		tiposDeMedidorPage.clickUpToBreadcrumbs(navegarTiposDeMedidorDPO.getUpToBreadcrumb());
		tiposDeMedidorPage.getWelcomeTitle(navegarTiposDeMedidorDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
