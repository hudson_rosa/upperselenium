package br.com.upperselenium.test.stage.configuracoes.cadastros.postos_tarifarios.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarPostosTarifariosDPO extends BaseHelperDPO {

	private String nome;
	private String deslocarNoHorarioDeVerao;
	private String horarioDePontaInicio;
	private String horarioDePontaFim;
	private String segundaHPonta;
	private String tercaHPonta;
	private String quartaHPonta;
	private String quintaHPonta;
	private String sextaHPonta;
	private String sabadoHPonta;
	private String domingoHPonta;
	private String feriadoHPonta;
	private String segundaHCapacitivo;
	private String tercaHCapacitivo;
	private String quartaHCapacitivo;
	private String quintaHCapacitivo;
	private String sextaHCapacitivo;
	private String sabadoHCapacitivo;
	private String domingoHCapacitivo;
	private String feriadoHCapacitivo;
	private String segundaHReservado;
	private String tercaHReservado;
	private String quartaHReservado;
	private String quintaHReservado;
	private String sextaHReservado;
	private String sabadoHReservado;
	private String domingoHReservado;
	private String feriadoHReservado;
	private String horarioDeForaPontaInicio;
	private String horarioDeForaPontaFim;
	private String horarioCapacitivoInicio;
	private String horarioCapacitivoFim;
	private String horarioReservado;
	private String horarioReservadoInicio;
	private String horarioReservadoFim;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDeslocarNoHorarioDeVerao() {
		return deslocarNoHorarioDeVerao;
	}

	public void setDeslocarNoHorarioDeVerao(String deslocarNoHorarioDeVerao) {
		this.deslocarNoHorarioDeVerao = deslocarNoHorarioDeVerao;
	}

	public String getHorarioDePontaInicio() {
		return horarioDePontaInicio;
	}

	public void setHorarioDePontaInicio(String horarioDePontaInicio) {
		this.horarioDePontaInicio = horarioDePontaInicio;
	}

	public String getHorarioDePontaFim() {
		return horarioDePontaFim;
	}

	public void setHorarioDePontaFim(String horarioDePontaFim) {
		this.horarioDePontaFim = horarioDePontaFim;
	}

	public String getSegundaHPonta() {
		return segundaHPonta;
	}

	public void setSegundaHPonta(String segundaHPonta) {
		this.segundaHPonta = segundaHPonta;
	}

	public String getTercaHPonta() {
		return tercaHPonta;
	}

	public void setTercaHPonta(String tercaHPonta) {
		this.tercaHPonta = tercaHPonta;
	}

	public String getQuartaHPonta() {
		return quartaHPonta;
	}

	public void setQuartaHPonta(String quartaHPonta) {
		this.quartaHPonta = quartaHPonta;
	}

	public String getQuintaHPonta() {
		return quintaHPonta;
	}

	public void setQuintaHPonta(String quintaHPonta) {
		this.quintaHPonta = quintaHPonta;
	}

	public String getSextaHPonta() {
		return sextaHPonta;
	}

	public void setSextaHPonta(String sextaHPonta) {
		this.sextaHPonta = sextaHPonta;
	}

	public String getSabadoHPonta() {
		return sabadoHPonta;
	}

	public void setSabadoHPonta(String sabadoHPonta) {
		this.sabadoHPonta = sabadoHPonta;
	}

	public String getDomingoHPonta() {
		return domingoHPonta;
	}

	public void setDomingoHPonta(String domingoHPonta) {
		this.domingoHPonta = domingoHPonta;
	}

	public String getFeriadoHPonta() {
		return feriadoHPonta;
	}

	public void setFeriadoHPonta(String feriadoHPonta) {
		this.feriadoHPonta = feriadoHPonta;
	}

	public String getSegundaHCapacitivo() {
		return segundaHCapacitivo;
	}

	public void setSegundaHCapacitivo(String segundaHCapacitivo) {
		this.segundaHCapacitivo = segundaHCapacitivo;
	}

	public String getTercaHCapacitivo() {
		return tercaHCapacitivo;
	}

	public void setTercaHCapacitivo(String tercaHCapacitivo) {
		this.tercaHCapacitivo = tercaHCapacitivo;
	}

	public String getQuartaHCapacitivo() {
		return quartaHCapacitivo;
	}

	public void setQuartaHCapacitivo(String quartaHCapacitivo) {
		this.quartaHCapacitivo = quartaHCapacitivo;
	}

	public String getQuintaHCapacitivo() {
		return quintaHCapacitivo;
	}

	public void setQuintaHCapacitivo(String quintaHCapacitivo) {
		this.quintaHCapacitivo = quintaHCapacitivo;
	}

	public String getSextaHCapacitivo() {
		return sextaHCapacitivo;
	}

	public void setSextaHCapacitivo(String sextaHCapacitivo) {
		this.sextaHCapacitivo = sextaHCapacitivo;
	}

	public String getSabadoHCapacitivo() {
		return sabadoHCapacitivo;
	}

	public void setSabadoHCapacitivo(String sabadoHCapacitivo) {
		this.sabadoHCapacitivo = sabadoHCapacitivo;
	}

	public String getDomingoHCapacitivo() {
		return domingoHCapacitivo;
	}

	public void setDomingoHCapacitivo(String domingoHCapacitivo) {
		this.domingoHCapacitivo = domingoHCapacitivo;
	}

	public String getFeriadoHCapacitivo() {
		return feriadoHCapacitivo;
	}

	public void setFeriadoHCapacitivo(String feriadoHCapacitivo) {
		this.feriadoHCapacitivo = feriadoHCapacitivo;
	}

	public String getSegundaHReservado() {
		return segundaHReservado;
	}

	public void setSegundaHReservado(String segundaHReservado) {
		this.segundaHReservado = segundaHReservado;
	}

	public String getTercaHReservado() {
		return tercaHReservado;
	}

	public void setTercaHReservado(String tercaHReservado) {
		this.tercaHReservado = tercaHReservado;
	}

	public String getQuartaHReservado() {
		return quartaHReservado;
	}

	public void setQuartaHReservado(String quartaHReservado) {
		this.quartaHReservado = quartaHReservado;
	}

	public String getQuintaHReservado() {
		return quintaHReservado;
	}

	public void setQuintaHReservado(String quintaHReservado) {
		this.quintaHReservado = quintaHReservado;
	}

	public String getSextaHReservado() {
		return sextaHReservado;
	}

	public void setSextaHReservado(String sextaHReservado) {
		this.sextaHReservado = sextaHReservado;
	}

	public String getSabadoHReservado() {
		return sabadoHReservado;
	}

	public void setSabadoHReservado(String sabadoHReservado) {
		this.sabadoHReservado = sabadoHReservado;
	}

	public String getDomingoHReservado() {
		return domingoHReservado;
	}

	public void setDomingoHReservado(String domingoHReservado) {
		this.domingoHReservado = domingoHReservado;
	}

	public String getFeriadoHReservado() {
		return feriadoHReservado;
	}

	public void setFeriadoHReservado(String feriadoHReservado) {
		this.feriadoHReservado = feriadoHReservado;
	}

	public String getHorarioDeForaPontaInicio() {
		return horarioDeForaPontaInicio;
	}

	public void setHorarioDeForaPontaInicio(String horarioDeForaPontaInicio) {
		this.horarioDeForaPontaInicio = horarioDeForaPontaInicio;
	}

	public String getHorarioDeForaPontaFim() {
		return horarioDeForaPontaFim;
	}

	public void setHorarioDeForaPontaFim(String horarioDeForaPontaFim) {
		this.horarioDeForaPontaFim = horarioDeForaPontaFim;
	}

	public String getHorarioCapacitivoInicio() {
		return horarioCapacitivoInicio;
	}

	public void setHorarioCapacitivoInicio(String horarioCapacitivoInicio) {
		this.horarioCapacitivoInicio = horarioCapacitivoInicio;
	}

	public String getHorarioCapacitivoFim() {
		return horarioCapacitivoFim;
	}

	public void setHorarioCapacitivoFim(String horarioCapacitivoFim) {
		this.horarioCapacitivoFim = horarioCapacitivoFim;
	}

	public String getHorarioReservado() {
		return horarioReservado;
	}

	public void setHorarioReservado(String horarioReservado) {
		this.horarioReservado = horarioReservado;
	}

	public String getHorarioReservadoInicio() {
		return horarioReservadoInicio;
	}

	public void setHorarioReservadoInicio(String horarioReservadoInicio) {
		this.horarioReservadoInicio = horarioReservadoInicio;
	}

	public String getHorarioReservadoFim() {
		return horarioReservadoFim;
	}

	public void setHorarioReservadoFim(String horarioReservadoFim) {
		this.horarioReservadoFim = horarioReservadoFim;
	}

}