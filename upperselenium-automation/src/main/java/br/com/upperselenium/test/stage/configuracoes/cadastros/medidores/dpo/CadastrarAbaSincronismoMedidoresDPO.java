package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaSincronismoMedidoresDPO extends BaseHelperDPO {

	private String acertoDeRelogio;
	private String sincronismoHora;
	private String sincronismoMinuto;

	public String getAcertoDeRelogio() {
		return acertoDeRelogio;
	}

	public void setAcertoDeRelogio(String acertoDeRelogio) {
		this.acertoDeRelogio = acertoDeRelogio;
	}

	public String getSincronismoHora() {
		return sincronismoHora;
	}

	public void setSincronismoHora(String sincronismoHora) {
		this.sincronismoHora = sincronismoHora;
	}

	public String getSincronismoMinuto() {
		return sincronismoMinuto;
	}

	public void setSincronismoMinuto(String sincronismoMinuto) {
		this.sincronismoMinuto = sincronismoMinuto;
	}

}