package br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.dpo.AlterarAbaContatoSubestacaoDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.page.AlteracaoAbaContatoSubestacaoPage;

public class AlterarAbaContatoSubestacaoStage extends BaseStage {
	private AlterarAbaContatoSubestacaoDPO alterarAbaContatoSubestacaoDPO;
	private AlteracaoAbaContatoSubestacaoPage alteracaoAbaContatoSubestacaoPage;
	
	public AlterarAbaContatoSubestacaoStage(String dp) {
		alterarAbaContatoSubestacaoDPO = loadDataProviderFile(AlterarAbaContatoSubestacaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alteracaoAbaContatoSubestacaoPage = initElementsFromPage(AlteracaoAbaContatoSubestacaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarAbaContatoSubestacaoDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoAbaContatoSubestacaoPage.clickAbaContato();
		alteracaoAbaContatoSubestacaoPage.typeTextTelefoneSE1(alterarAbaContatoSubestacaoDPO.getTelefoneSE1());			
		alteracaoAbaContatoSubestacaoPage.typeTextTelefoneSE2(alterarAbaContatoSubestacaoDPO.getTelefoneSE2());			
		alteracaoAbaContatoSubestacaoPage.typeTextContato1(alterarAbaContatoSubestacaoDPO.getContato1());			
		alteracaoAbaContatoSubestacaoPage.typeTextEmail1(alterarAbaContatoSubestacaoDPO.getEmail1());			
		alteracaoAbaContatoSubestacaoPage.typeTextTelContato1(alterarAbaContatoSubestacaoDPO.getTelContato1());			
		alteracaoAbaContatoSubestacaoPage.typeTextContato2(alterarAbaContatoSubestacaoDPO.getContato2());			
		alteracaoAbaContatoSubestacaoPage.typeTextEmail2(alterarAbaContatoSubestacaoDPO.getEmail2());			
		alteracaoAbaContatoSubestacaoPage.typeTextTelContato2(alterarAbaContatoSubestacaoDPO.getTelContato2());			
		alteracaoAbaContatoSubestacaoPage.typeTextTelContato3(alterarAbaContatoSubestacaoDPO.getTelContato3());			
		alteracaoAbaContatoSubestacaoPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoAbaContatoSubestacaoPage.validateMessageDefault(alterarAbaContatoSubestacaoDPO.getOperationMessage());
	}

}
