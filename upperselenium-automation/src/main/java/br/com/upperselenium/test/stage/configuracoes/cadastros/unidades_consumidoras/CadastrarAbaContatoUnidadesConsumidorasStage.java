package br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.dpo.CadastrarAbaContatoUnidadesConsumidorasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.page.CadastroAbaContatoUnidadesConsumidorasPage;

public class CadastrarAbaContatoUnidadesConsumidorasStage extends BaseStage {
	private CadastrarAbaContatoUnidadesConsumidorasDPO cadastrarAbaContatoUnidadesConsumidorasDPO;
	private CadastroAbaContatoUnidadesConsumidorasPage cadastroAbaContatoUnidadesConsumidorasPage;
	
	public CadastrarAbaContatoUnidadesConsumidorasStage(String dp) {
		cadastrarAbaContatoUnidadesConsumidorasDPO = loadDataProviderFile(CadastrarAbaContatoUnidadesConsumidorasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaContatoUnidadesConsumidorasPage = initElementsFromPage(CadastroAbaContatoUnidadesConsumidorasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaContatoUnidadesConsumidorasDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaContatoUnidadesConsumidorasPage.clickAbaContato();
		if(StringUtil.isNotBlankOrNotNull(cadastrarAbaContatoUnidadesConsumidorasDPO.getButtonAdicionar())){
			cadastroAbaContatoUnidadesConsumidorasPage.clickAdicionar();
			cadastroAbaContatoUnidadesConsumidorasPage.typeModalTextNome(cadastrarAbaContatoUnidadesConsumidorasDPO.getModalNome());
			cadastroAbaContatoUnidadesConsumidorasPage.typeModalTextEmail(cadastrarAbaContatoUnidadesConsumidorasDPO.getModalEmail());
			cadastroAbaContatoUnidadesConsumidorasPage.typeModalTextTelefone(cadastrarAbaContatoUnidadesConsumidorasDPO.getModalTelefone());
			cadastroAbaContatoUnidadesConsumidorasPage.clickModalSalvar();
			cadastroAbaContatoUnidadesConsumidorasPage.validateMessageDefault(cadastrarAbaContatoUnidadesConsumidorasDPO.getOperationMessage());
		}
		cadastroAbaContatoUnidadesConsumidorasPage.typeTextFilterNome(cadastrarAbaContatoUnidadesConsumidorasDPO.getFilterNome());
		cadastroAbaContatoUnidadesConsumidorasPage.typeTextFilterEmail(cadastrarAbaContatoUnidadesConsumidorasDPO.getFilterEmail());
	}

	@Override
	public void runValidations() {
		validateGridValueNome();
		validateGridValueEmail();
		validateGridValueTelefone();
	}

	private void validateGridValueNome() {
		cadastroAbaContatoUnidadesConsumidorasPage.validateEqualsToValues(
				cadastrarAbaContatoUnidadesConsumidorasDPO.getColNome(), 
				cadastroAbaContatoUnidadesConsumidorasPage.getGridTextNome());
	}
	
	private void validateGridValueEmail() {
		cadastroAbaContatoUnidadesConsumidorasPage.validateEqualsToValues(
				cadastrarAbaContatoUnidadesConsumidorasDPO.getColEmail(), 
				cadastroAbaContatoUnidadesConsumidorasPage.getGridTextEmail());		
	}
	
	private void validateGridValueTelefone() {
		cadastroAbaContatoUnidadesConsumidorasPage.validateEqualsToValues(
				cadastrarAbaContatoUnidadesConsumidorasDPO.getColTelefone(), 
				cadastroAbaContatoUnidadesConsumidorasPage.getGridTextTelefone());			
	}
	
}
