package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.dpo.PerdasTecnicasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.page.PerdasTecnicasPage;

public class PerdasTecnicasStage extends BaseStage {
	private PerdasTecnicasDPO perdasTecnicasDPO;
	private PerdasTecnicasPage perdasTecnicasPage;
	
	public PerdasTecnicasStage(String dp) {
		perdasTecnicasDPO = loadDataProviderFile(PerdasTecnicasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		perdasTecnicasPage = initElementsFromPage(PerdasTecnicasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(perdasTecnicasDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		perdasTecnicasPage.typeTextMes(perdasTecnicasDPO.getMesAno());
		perdasTecnicasPage.typeTextFilterRegiao(perdasTecnicasDPO.getFilterRegiao());
		perdasTecnicasPage.typeTextFilterPerdaTecnica(perdasTecnicasDPO.getFilterPerdaTecnica());
		perdasTecnicasPage.clickFiltrar();
	}

	@Override
	public void runValidations() {
		validateGridRegiao();
		validateGridMesAno();
		validateGridPerdaTecnica();
	}
	
	private void validateGridRegiao() {
		perdasTecnicasPage.validateEqualsToValues(
				perdasTecnicasDPO.getRegiao(), 
				perdasTecnicasPage.getGridTextRegiao());
	}
	
	private void validateGridMesAno() {
		perdasTecnicasPage.validateEqualsToValues(
				perdasTecnicasDPO.getMesAnoCol(), 
				perdasTecnicasPage.getGridTextMesAno());		
	}
	
	private void validateGridPerdaTecnica() {
		perdasTecnicasPage.validateEqualsToValues(
				perdasTecnicasDPO.getPerdaTecnica(), 
				perdasTecnicasPage.getGridTextPerdaTecnica());				
	}

}
