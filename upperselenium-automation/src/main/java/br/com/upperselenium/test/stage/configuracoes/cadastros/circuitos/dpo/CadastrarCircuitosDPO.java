package br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarCircuitosDPO extends BaseHelperDPO {

	private String nome;
	private String alias;
	private String medidor;
	private String entradaDigitalRDO;
	private String arquivoRDO;
	private String entradaDigitalCBX;
	private String creg;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getMedidor() {
		return medidor;
	}

	public void setMedidor(String medidor) {
		this.medidor = medidor;
	}

	public String getEntradaDigitalRDO() {
		return entradaDigitalRDO;
	}

	public void setEntradaDigitalRDO(String entradaDigitalRDO) {
		this.entradaDigitalRDO = entradaDigitalRDO;
	}

	public String getArquivoRDO() {
		return arquivoRDO;
	}

	public void setArquivoRDO(String arquivoRDO) {
		this.arquivoRDO = arquivoRDO;
	}

	public String getEntradaDigitalCBX() {
		return entradaDigitalCBX;
	}

	public void setEntradaDigitalCBX(String entradaDigitalCBX) {
		this.entradaDigitalCBX = entradaDigitalCBX;
	}

	public String getCreg() {
		return creg;
	}

	public void setCreg(String creg) {
		this.creg = creg;
	}

}
