package br.com.upperselenium.test.stage.configuracoes.cadastros.gateways;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.dpo.CadastrarAbaEnderecoGatewaysDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.page.CadastroAbaEnderecoGatewaysPage;

public class CadastrarAbaEnderecoGatewaysStage extends BaseStage {
	private CadastrarAbaEnderecoGatewaysDPO cadastrarAbaEnderecoGatewaysDPO;
	private CadastroAbaEnderecoGatewaysPage cadastroAbaEnderecoGatewaysPage;
	
	public CadastrarAbaEnderecoGatewaysStage(String dp) {
		cadastrarAbaEnderecoGatewaysDPO = loadDataProviderFile(CadastrarAbaEnderecoGatewaysDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaEnderecoGatewaysPage = initElementsFromPage(CadastroAbaEnderecoGatewaysPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaEnderecoGatewaysDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaEnderecoGatewaysPage.clickAbaEndereco();
		cadastroAbaEnderecoGatewaysPage.typeTextLogradouro(cadastrarAbaEnderecoGatewaysDPO.getLogradouro());			
		cadastroAbaEnderecoGatewaysPage.typeTextNumero(cadastrarAbaEnderecoGatewaysDPO.getNumero());			
		cadastroAbaEnderecoGatewaysPage.typeTextBairro(cadastrarAbaEnderecoGatewaysDPO.getBairro());			
		cadastroAbaEnderecoGatewaysPage.typeTextCEP(cadastrarAbaEnderecoGatewaysDPO.getCep());			
		cadastroAbaEnderecoGatewaysPage.typeSelectCidade(cadastrarAbaEnderecoGatewaysDPO.getCidade());			
		cadastroAbaEnderecoGatewaysPage.typeSelectRegiao(cadastrarAbaEnderecoGatewaysDPO.getRegiao());
		cadastroAbaEnderecoGatewaysPage.typeTextLatitude(cadastrarAbaEnderecoGatewaysDPO.getLatitude());			
		cadastroAbaEnderecoGatewaysPage.typeTextLongitude(cadastrarAbaEnderecoGatewaysDPO.getLongitude());			
		cadastroAbaEnderecoGatewaysPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaEnderecoGatewaysPage.validateMessageDefault(cadastrarAbaEnderecoGatewaysDPO.getOperationMessage());
	}

}
