package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AcompDiarioDeUsinasPage extends BaseHelperPage{
	
	private static final String LINK_MONITORAMENTO_DE_USINAS = "//*[@id='tree-content']/div[6]/a[contains(text(),'Monitoramento de Usinas')]";
	private static final String LINK_IMPORTACAO_DE_DADOS_HIDROLOGICOS = "//*[@id='tree-content']/div[6]/a[contains(text(),'Importação de dados Hidrológicos')]";
	private static final String BUTTON_FILTRAR = ".//filtro-acompanhamento-diario-usinas/div/form-with-validation/form/button";
	private static final String LABEL_USINA = ".//filtro-acompanhamento-diario-usinas/div/form-with-validation/form/div[1]/label";
	private static final String LABEL_DATA = ".//filtro-acompanhamento-diario-usinas/div/form-with-validation/form/div[2]/date-picker/div/label";
	private static final String LABEL_HEADER_GERACAO = ".//tabela-acompanhamento-diario-usinas/div/table/thead/tr[1]/th[2]/span";
	private static final String LABEL_HEADER_NIVEL = ".//tabela-acompanhamento-diario-usinas/div/table/thead/tr[1]/th[3]/span";
	private static final String LABEL_HEADER_EFICIENCIA = ".//tabela-acompanhamento-diario-usinas/div/table/thead/tr[1]/th[4]/span";
	private static final String LABEL_HEADER_VAZAO = ".//tabela-acompanhamento-diario-usinas/div/table/thead/tr[1]/th[5]/span";
	
	public void clickLinkMonitoramentoDeUsinas(){
		clickOnElement(LINK_MONITORAMENTO_DE_USINAS);
		waitForPageToLoadUntil10s();
	}
	
	public void clickLinkImportacaoDeDadosHidrologicos(){
		clickOnElement(LINK_IMPORTACAO_DE_DADOS_HIDROLOGICOS);
		waitForPageToLoadUntil10s();
	}
	
	public void clickButtonFiltrar(String value){
		clickOnElementByCondition(BUTTON_FILTRAR, value);
		waitForPageToLoadUntil10s();
	}	
	
	public void getLabelUsina(String value){
		getLabel(LABEL_USINA, value);
	}
	
	public void getLabelData(String value){
		getLabel(LABEL_DATA, value);
	}
	
	public void getLabelHeaderGeracao(String value){
		waitForPresenceOfElementLocated(LABEL_HEADER_GERACAO);
		getLabel(LABEL_HEADER_GERACAO, value);
	}
	
	public void getLabelHeaderNivel(String value){
		getLabel(LABEL_HEADER_NIVEL, value);
	}
	
	public void getLabelHeaderEficiência(String value){
		getLabel(LABEL_HEADER_EFICIENCIA, value);
	}
	
	public void getLabelHeaderVazao(String value){
		getLabel(LABEL_HEADER_VAZAO, value);
	}

}