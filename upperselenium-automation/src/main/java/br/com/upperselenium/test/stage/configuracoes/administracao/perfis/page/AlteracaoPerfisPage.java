package br.com.upperselenium.test.stage.configuracoes.administracao.perfis.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoPerfisPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 
	private static final String LINK_ABA_GERAL = ".//div[2]/div/div[2]/ul[2]/li[1]/a";
	private static final String LINK_ABA_PERMISSOES = ".//div[2]/div/div[2]/ul[2]/li[2]/a";
	private static final String TEXT_NOME = "//*[@id='Perfil_Nome']";
	private static final String TEXT_DESCRICAO = "//*[@id='Perfil_Descricao']";
	private static final String SELECT_MODULO_PADRAO = "//*[@id='ModuloPadraoId']";
	private static final String SELECT_MODULOS = "//*[@id='ModulosEscolhidos']";
	private static final String CHECK_VISUALIZAR_ORFAOS = "//*[@id='Perfil_VisualizarOrfaos']";
	private static final String CHECK_RECEBER_EMAILS_ALARMES = "//*[@id='Perfil_RecebeEmailsDeAlarmsCriticos']";
	private static final String CHECK_ALTERAR_PROPRIA_SENHA = "//*[@id='Perfil_PermissaoParaAlterarAPropriaSenha']";
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	
	
	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}	

	public void typeSelectModuloPadrao(String value){
		typeSelectComboOption(SELECT_MODULO_PADRAO, value);
	}
	
	public void typeSelectModulos(String value){
		typeSelectComboOption(SELECT_MODULOS, value);
	}
	
	public void typeCheckVisualizarOrfaos(String value){
		typeCheckOption(CHECK_VISUALIZAR_ORFAOS, value);
	}
	
	public void typeCheckReceberEmailsAlarmes(String value){
		typeCheckOption(CHECK_RECEBER_EMAILS_ALARMES, value);
	}
	
	public void typeCheckAlterarPropriaSenha(String value){
		typeCheckOption(CHECK_ALTERAR_PROPRIA_SENHA, value);
	}

	public void keyPageDown(){
		useKey(TEXT_NOME, Keys.PAGE_DOWN);
	}	
	
	public void clickAbaGeral(){
		clickOnElement(LINK_ABA_GERAL);
		waitForPageToLoadUntil10s();
	}
	
	public void clickAbaPermissoes(){
		clickOnElement(LINK_ABA_PERMISSOES);
		waitForPageToLoadUntil10s();
	}	
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}