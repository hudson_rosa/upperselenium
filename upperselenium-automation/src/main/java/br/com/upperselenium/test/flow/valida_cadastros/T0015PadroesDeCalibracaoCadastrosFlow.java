package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao.CadastrarPadroesDeCalibracaoStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao.GoToPadroesDeCalibracaoStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0015PadroesDeCalibracaoCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0015-PadroesDeCalibracao", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Padrões de Calibração realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0015PadroesDeCalibracaoCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToPadroesDeCalibracaoStage());
		addStage(new CadastrarPadroesDeCalibracaoStage(getDP("CadastrarPadroesDeCalibracaoDP.json")));
	}	
}
