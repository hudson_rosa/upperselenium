package br.com.upperselenium.test.stage.configuracoes.cadastros.feriados;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.feriados.dpo.CadastrarFeriadosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.feriados.page.CadastroFeriadosPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.feriados.page.FeriadosPage;

public class CadastrarFeriadosStage extends BaseStage {
	private CadastrarFeriadosDPO cadastrarFeriadosDPO;
	private FeriadosPage feriadosPage;
	private CadastroFeriadosPage cadastroFeriadosPage;
	
	public CadastrarFeriadosStage(String dp) {
		cadastrarFeriadosDPO = loadDataProviderFile(CadastrarFeriadosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		feriadosPage = initElementsFromPage(FeriadosPage.class);
		cadastroFeriadosPage = initElementsFromPage(CadastroFeriadosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarFeriadosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		feriadosPage.clickNovo();
		cadastroFeriadosPage.typeTextNome(cadastrarFeriadosDPO.getNome()+getRandomStringSpaced());				
		cadastroFeriadosPage.typeTextData(cadastrarFeriadosDPO.getDataDia());				
		cadastroFeriadosPage.typeTextDescricao(cadastrarFeriadosDPO.getDescricao()+getRandomStringSpaced());				
		cadastroFeriadosPage.typeSelectEstado(cadastrarFeriadosDPO.getEstado());				
		cadastroFeriadosPage.typeSelectCidade(cadastrarFeriadosDPO.getCidade());				
		cadastroFeriadosPage.typeSelectRegiao(cadastrarFeriadosDPO.getRegiao());				
		cadastroFeriadosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroFeriadosPage.validateMessageDefault(cadastrarFeriadosDPO.getOperationMessage());
	}

}
