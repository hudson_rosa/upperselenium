package br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.dpo.CadastrarClientsSCDEDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.page.CadastroClientsSCDEPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.page.ClientsSCDEPage;

public class CadastrarClientsSCDEStage extends BaseStage {
	private CadastrarClientsSCDEDPO cadastrarClientsSCDEDPO;
	private ClientsSCDEPage clientsSCDEPage;
	private CadastroClientsSCDEPage cadastroClientsSCDEPage;
	
	public CadastrarClientsSCDEStage(String dp) {
		cadastrarClientsSCDEDPO = loadDataProviderFile(CadastrarClientsSCDEDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		clientsSCDEPage = initElementsFromPage(ClientsSCDEPage.class);
		cadastroClientsSCDEPage = initElementsFromPage(CadastroClientsSCDEPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarClientsSCDEDPO.getIssue());		
		execute();		
	}
	
	private void execute() {
		clientsSCDEPage.clickNovo();
		cadastroClientsSCDEPage.typeTextNome(cadastrarClientsSCDEDPO.getNome()+getRandomStringSpaced());
		cadastroClientsSCDEPage.typeSelectTipo(cadastrarClientsSCDEDPO.getTipo());
		cadastroClientsSCDEPage.typeTextPath(cadastrarClientsSCDEDPO.getPath());
		cadastroClientsSCDEPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroClientsSCDEPage.validateMessageDefault(cadastrarClientsSCDEDPO.getOperationMessage());
	}

}
