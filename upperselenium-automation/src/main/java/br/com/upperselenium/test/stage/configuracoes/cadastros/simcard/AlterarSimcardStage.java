package br.com.upperselenium.test.stage.configuracoes.cadastros.simcard;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.simcard.dpo.AlterarSimcardDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.simcard.page.AlteracaoSimcardPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.simcard.page.SimcardPage;

public class AlterarSimcardStage extends BaseStage {
	private AlterarSimcardDPO alterarSimcardDPO;
	private SimcardPage simcardPage;
	private AlteracaoSimcardPage alteracaoSimcardPage;
	
	public AlterarSimcardStage(String dp) {
		alterarSimcardDPO = loadDataProviderFile(AlterarSimcardDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		simcardPage = initElementsFromPage(SimcardPage.class);
		alteracaoSimcardPage = initElementsFromPage(AlteracaoSimcardPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarSimcardDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoSimcardPage.typeTextSID(alterarSimcardDPO.getSid()+getRandomStringSpaced());		
		alteracaoSimcardPage.typeTextNumero(alterarSimcardDPO.getNumero()+getRandomNumber());		
		alteracaoSimcardPage.typeTextOperadora(alterarSimcardDPO.getOperadora()+getRandomStringSpaced());		
		alteracaoSimcardPage.typeTextGateway(alterarSimcardDPO.getGateway());		
		alteracaoSimcardPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoSimcardPage.validateMessageDefault(alterarSimcardDPO.getOperationMessage());
	}
	
}
