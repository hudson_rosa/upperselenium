package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarAbaAnemometriaPontosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastroAbaAnemometriaPontosPage;

public class CadastrarAbaAnemometriaPontosStage extends BaseStage {
	private CadastrarAbaAnemometriaPontosDPO cadastrarAbaAnemometriaPontosDPO;
	private CadastroAbaAnemometriaPontosPage cadastroAbaAnemometriaPontosPage;
	
	public CadastrarAbaAnemometriaPontosStage(String dp) {
		cadastrarAbaAnemometriaPontosDPO = loadDataProviderFile(CadastrarAbaAnemometriaPontosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaAnemometriaPontosPage = initElementsFromPage(CadastroAbaAnemometriaPontosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaAnemometriaPontosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaAnemometriaPontosPage.clickAbaAnemometria();
		FindElementUtil.acceptAlert();
		checkForTypeCodigoDaEstacao();
		cadastroAbaAnemometriaPontosPage.clickSalvarCodigoDaEstacao();	
		cadastroAbaAnemometriaPontosPage.validateMessageDefault(cadastrarAbaAnemometriaPontosDPO.getOperationMessage());
		for (int index=0; index < cadastrarAbaAnemometriaPontosDPO.getGroupSalvarInstrumento().size(); index++){
			cadastroAbaAnemometriaPontosPage.typeSelectInstrumento(cadastrarAbaAnemometriaPontosDPO.getGroupSalvarInstrumento().get(index).getSelecioneInstrumento());
			cadastroAbaAnemometriaPontosPage.typeTextAltura(cadastrarAbaAnemometriaPontosDPO.getGroupSalvarInstrumento().get(index).getAltura());
			cadastroAbaAnemometriaPontosPage.clickSalvarInstrumento();
		}
		for (int index=0; index < cadastrarAbaAnemometriaPontosDPO.getGridVigenciaAnemometria().size(); index++){
			waitForPageToLoadUntil10s();
			addVigencia(index);
			cadastroAbaAnemometriaPontosPage.typeTextFilterInicioDaVigencia(cadastrarAbaAnemometriaPontosDPO.getGridVigenciaAnemometria().get(index).getFilterInicioDaVigencia());
			cadastroAbaAnemometriaPontosPage.typeTextFilterOffset(cadastrarAbaAnemometriaPontosDPO.getGridVigenciaAnemometria().get(index).getFilterOffset());
			cadastroAbaAnemometriaPontosPage.typeTextFilterSlope(cadastrarAbaAnemometriaPontosDPO.getGridVigenciaAnemometria().get(index).getFilterSlope());
			cadastroAbaAnemometriaPontosPage.getGridLabelInicioDaVigencia(cadastrarAbaAnemometriaPontosDPO.getGridVigenciaAnemometria().get(index).getGridInicioDaVigencia(), 1);
			cadastroAbaAnemometriaPontosPage.getGridLabelOffset(cadastrarAbaAnemometriaPontosDPO.getGridVigenciaAnemometria().get(index).getGridOffset(), 1);
			cadastroAbaAnemometriaPontosPage.getGridLabelSlope(cadastrarAbaAnemometriaPontosDPO.getGridVigenciaAnemometria().get(index).getGridSlope(), 1);
		}
		cadastroAbaAnemometriaPontosPage.keyPageUp();
	}

	private void addVigencia(int index) {
		if(StringUtil.isNotBlankOrNotNull(cadastrarAbaAnemometriaPontosDPO.getGridVigenciaAnemometria().get(index).getFlagItem())){
			cadastroAbaAnemometriaPontosPage.keyPageDown();
			cadastroAbaAnemometriaPontosPage.clickAdicionarVigencia();
			cadastroAbaAnemometriaPontosPage.typeModalTextInicioDaVigencia(cadastrarAbaAnemometriaPontosDPO.getGridVigenciaAnemometria().get(index).getModalInicioDaVigencia());			
			cadastroAbaAnemometriaPontosPage.typeModalTextSlope(cadastrarAbaAnemometriaPontosDPO.getGridVigenciaAnemometria().get(index).getModalSlope().trim());
			cadastroAbaAnemometriaPontosPage.typeModalTextOffset(cadastrarAbaAnemometriaPontosDPO.getGridVigenciaAnemometria().get(index).getModalOffset().trim());
			cadastroAbaAnemometriaPontosPage.clickModalAdicionarVigencia();
		}
	}

	private void checkForTypeCodigoDaEstacao() {
		if(!StringUtil.isNotBlankOrNotNull(cadastrarAbaAnemometriaPontosDPO.getCodigoDaEstacao())){
			cadastroAbaAnemometriaPontosPage.typeTextCodigoDaEstacao(cadastrarAbaAnemometriaPontosDPO.getCodigoDaEstacao()+getRandomStringNonSpaced());
		} else {
			cadastroAbaAnemometriaPontosPage.typeTextCodigoDaEstacao(cadastrarAbaAnemometriaPontosDPO.getCodigoDaEstacao());
		}
	}

	@Override
	public void runValidations() {}

}
