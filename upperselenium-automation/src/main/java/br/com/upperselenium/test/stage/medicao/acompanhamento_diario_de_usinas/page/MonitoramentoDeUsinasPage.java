package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class MonitoramentoDeUsinasPage extends BaseHelperPage{
	
	private static final String LABEL_NIVEIS = ".//form-with-validation/form/div[1]/div/niveis/div/div[1]/span";
	private static final String LABEL_VERTIMENTO = ".//form-with-validation/form/div[1]/div/vertimento/div/div[1]/span";
	private static final String LABEL_EFICIENCIA = ".//form-with-validation/form/div[1]/div/eficiencia/div/div[1]/span";
	private static final String LABEL_UNIDADES_GERADORAS = ".//form-with-validation/form/div[2]/unidades-geradoras/div/div[1]/span";
	private static final String LABEL_VAZAO = ".//form-with-validation/form/div[2]/vazao/div/div[1]/span";
	
	public void getLabelNiveis(String value){
		getLabel(LABEL_NIVEIS, value);
	}
	
	public void getLabelVertimento(String value){
		getLabel(LABEL_VERTIMENTO, value);
	}
	
	public void getLabelEficiencia(String value){
		getLabel(LABEL_EFICIENCIA, value);
	}
	
	public void getLabelUnidadesGeradoras(String value){
		getLabel(LABEL_UNIDADES_GERADORAS, value);
	}
	
	public void getLabelVazao(String value){
		getLabel(LABEL_VAZAO, value);
	}

}