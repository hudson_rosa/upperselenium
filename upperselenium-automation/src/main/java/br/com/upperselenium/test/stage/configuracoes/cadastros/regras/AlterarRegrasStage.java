package br.com.upperselenium.test.stage.configuracoes.cadastros.regras;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.base.util.WaitElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regras.dpo.AlterarRegrasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regras.page.AlteracaoRegrasPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regras.page.RegrasPage;

public class AlterarRegrasStage extends BaseStage {
	private AlterarRegrasDPO alterarRegrasDPO;
	private RegrasPage regrasPage;
	private AlteracaoRegrasPage alteracaoRegrasPage;
	
	public AlterarRegrasStage(String dp) {
		alterarRegrasDPO = loadDataProviderFile(AlterarRegrasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		regrasPage = initElementsFromPage(RegrasPage.class);
		alteracaoRegrasPage = initElementsFromPage(AlteracaoRegrasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarRegrasDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		regrasPage.clickNova();
		alteracaoRegrasPage.typeTextNome(alterarRegrasDPO.getNome()+getRandomStringSpaced());
		alteracaoRegrasPage.typeSelectPasta(alterarRegrasDPO.getPasta());
		for (int index=0; index < alterarRegrasDPO.getGridRegras().size(); index++){
			alteracaoRegrasPage.typeGridSelectSe(alterarRegrasDPO.getGridRegras().get(index).getSe(), index+1);
			alteracaoRegrasPage.typeGridSelectOperador(alterarRegrasDPO.getGridRegras().get(index).getOperador(), index+1);
			alteracaoRegrasPage.typeGridSelectValor(alterarRegrasDPO.getGridRegras().get(index).getValor(), index+1);
			alteracaoRegrasPage.clickGridAdd(alterarRegrasDPO.getGridRegras().get(index).getButtonAdd(), index+1);
			alteracaoRegrasPage.clickGridExclude(alterarRegrasDPO.getGridRegras().get(index).getButtonExclude(), index+1);
		}
		alteracaoRegrasPage.typeTextFormula(alterarRegrasDPO.getFormula());
		WaitElementUtil.waitForATime(TimePRM._3_SECS);
		alteracaoRegrasPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoRegrasPage.validateMessageDefault(alterarRegrasDPO.getOperationMessage());
	}

}
