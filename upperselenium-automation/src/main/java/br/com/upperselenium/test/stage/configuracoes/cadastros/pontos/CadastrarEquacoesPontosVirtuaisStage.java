package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarEquacoesPontosVirtuaisDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.NovaEquacaoPontosVirtuaisPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.VisualizacaoEquacoesPontosVirtuaisPage;

public class CadastrarEquacoesPontosVirtuaisStage extends BaseStage {
	private CadastrarEquacoesPontosVirtuaisDPO cadastrarEquacoesPontosVirtuaisDPO;
	private NovaEquacaoPontosVirtuaisPage novaEquacaoPontosVirtuaisPage;
	private VisualizacaoEquacoesPontosVirtuaisPage visualizarEquacoesPontosVirtuaisPage;
	
	public CadastrarEquacoesPontosVirtuaisStage(String dp) {
		cadastrarEquacoesPontosVirtuaisDPO = loadDataProviderFile(CadastrarEquacoesPontosVirtuaisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		novaEquacaoPontosVirtuaisPage = initElementsFromPage(NovaEquacaoPontosVirtuaisPage.class);
		visualizarEquacoesPontosVirtuaisPage = initElementsFromPage(VisualizacaoEquacoesPontosVirtuaisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarEquacoesPontosVirtuaisDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		visualizarEquacoesPontosVirtuaisPage.clickEquacoes();
		FindElementUtil.acceptAlert();
		createNewEquacao();
		waitForPageToLoadUntil10s();
		viewEquacoes();
	}

	private void createNewEquacao() {
		if (StringUtil.isNotBlankOrNotNull(cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(0).getVigencia())) {
			visualizarEquacoesPontosVirtuaisPage.clickNovaEquacao();
			createOrEditExpressao();
		}
	}
	
	private void createOrEditExpressao() {
		for (int index = 1; index <= cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().size(); index++) {
			novaEquacaoPontosVirtuaisPage.typeGroupTextVigencia(cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(index-1).getVigencia());
			novaEquacaoPontosVirtuaisPage.typeGroupSelectSequenciaDeCalculo(cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(index-1).getSequenciaDeCalculo());
			novaEquacaoPontosVirtuaisPage.getGroupTooltipSequenciaDeCalculo(cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(index-1).getTooltipSequencia());
			novaEquacaoPontosVirtuaisPage.typeGroupTextPontos(cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(index-1).getPontos());
			novaEquacaoPontosVirtuaisPage.typeGroupTextRegras(cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(index-1).getRegras());
			novaEquacaoPontosVirtuaisPage.keyPageDown1();
			configureNewIdentificador(index);
			novaEquacaoPontosVirtuaisPage.clickGroupSomarTodos();
			novaEquacaoPontosVirtuaisPage.typeGroupTextExpressao(cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(index-1).getEquacao());
			novaEquacaoPontosVirtuaisPage.getGroupTooltipExpressao(cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(index-1).getTootltipExpressao());
			novaEquacaoPontosVirtuaisPage.keyPageDown2();
			novaEquacaoPontosVirtuaisPage.clickSalvar();
		}
	}

	private void configureNewIdentificador(int index) {
		for (int anomTrGrid = 1; anomTrGrid <= cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(index-1).getGridCadastrarNovaEquacao().size(); anomTrGrid++) {
			novaEquacaoPontosVirtuaisPage.getGridValueIdentificador(cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(index-1).getGridCadastrarNovaEquacao().get(anomTrGrid-1).getIdentificador(), anomTrGrid);
			novaEquacaoPontosVirtuaisPage.getGridValueNome(cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(index-1).getGridCadastrarNovaEquacao().get(anomTrGrid-1).getNome(), anomTrGrid);
			novaEquacaoPontosVirtuaisPage.typeGridSelectQuadrante(cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(index-1).getGridCadastrarNovaEquacao().get(anomTrGrid-1).getQuadrante(), anomTrGrid);
			novaEquacaoPontosVirtuaisPage.typeGridCheckCP(cadastrarEquacoesPontosVirtuaisDPO.getGroupNovaEquacao().get(index-1).getGridCadastrarNovaEquacao().get(anomTrGrid-1).getCp(), anomTrGrid);
		}
	}

	private void viewEquacoes() {
		for (int anomFieldset = 1; anomFieldset <= cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().size(); anomFieldset++) {
			visualizarEquacoesPontosVirtuaisPage.getGroupLabelVigenciaExpressao(cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().get(anomFieldset-1).getLabelVigenciaEquacao(), anomFieldset);
			visualizarEquacoesPontosVirtuaisPage.getGroupLabelSequenciaDeCalculo(cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().get(anomFieldset-1).getLabelSequenciaDeCalculo(), anomFieldset);
			validateIdentificadores(anomFieldset);
			clickCopiarEmNovaEquacao(anomFieldset);
			clickEditEquacao(anomFieldset);
			clickRemoveEquacao(anomFieldset);
		}
	}

	private void clickRemoveEquacao(int anomFieldset) {
		if(StringUtil.isNotBlankOrNotNull(cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().get(anomFieldset-1).getRemover())){
			visualizarEquacoesPontosVirtuaisPage.clickGroupButtonRemover(cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().get(anomFieldset-1).getRemover(), anomFieldset);
		}
	}

	private void clickEditEquacao(int anomFieldset) {
		if(StringUtil.isNotBlankOrNotNull(cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().get(anomFieldset-1).getEditar())){
			visualizarEquacoesPontosVirtuaisPage.clickGroupButtonEditar(cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().get(anomFieldset-1).getEditar(), anomFieldset);
			createOrEditExpressao();
		}
	}

	private void clickCopiarEmNovaEquacao(int anomFieldset) {
		if(StringUtil.isNotBlankOrNotNull(cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().get(anomFieldset-1).getCopiarEmNovaEquacao())){
			visualizarEquacoesPontosVirtuaisPage.clickGroupButtonCopiarEmNovaEquacao(cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().get(anomFieldset-1).getCopiarEmNovaEquacao(), anomFieldset);
			createOrEditExpressao();
		}
	}
	
	private void validateIdentificadores(int anomFieldset) {
		for (int lineTrGrid = 1; lineTrGrid <= cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().size(); lineTrGrid++) {
			visualizarEquacoesPontosVirtuaisPage.getGridValueIdentificador(cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().get(anomFieldset-1).getGridVisualizarEquacoes().get(lineTrGrid-1).getValueIdentificador(), anomFieldset, lineTrGrid, 0);
			visualizarEquacoesPontosVirtuaisPage.getGridValuePonto(cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().get(anomFieldset-1).getGridVisualizarEquacoes().get(lineTrGrid-1).getValuePonto(), anomFieldset, lineTrGrid, 0);
			visualizarEquacoesPontosVirtuaisPage.getGridValueQuadrante(cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().get(anomFieldset-1).getGridVisualizarEquacoes().get(lineTrGrid-1).getValueQuadrante(), anomFieldset, lineTrGrid, 0);
			visualizarEquacoesPontosVirtuaisPage.getGridValueCompensacaoDePerdas(cadastrarEquacoesPontosVirtuaisDPO.getGroupVisualizarEquacoes().get(anomFieldset-1).getGridVisualizarEquacoes().get(lineTrGrid-1).getValueCompensacaoDePerdas(), anomFieldset, lineTrGrid, 0);
		}
	}

	@Override
	public void runValidations() {}

}
