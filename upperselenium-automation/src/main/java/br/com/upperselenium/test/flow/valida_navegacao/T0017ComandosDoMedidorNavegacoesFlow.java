package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.GoToComandosDoMedidorStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.NavegarComandosDoMedidorAgendarParametrosStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.NavegarComandosDoMedidorAgendarStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.NavegarComandosDoMedidorHistoricoStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.NavegarListagemComandosDoMedidorStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0017ComandosDoMedidorNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0017-ComandosDoMedidor", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Comandos do Medidor validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0017ComandosDoMedidorNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToComandosDoMedidorStage());
		addStage(new NavegarListagemComandosDoMedidorStage(getDP("NavegarListagemComandosDoMedidorDP.json")));
		addStage(new GoToComandosDoMedidorStage());
		addStage(new NavegarComandosDoMedidorAgendarStage(getDP("NavegarComandosDoMedidorAgendarDP.json")));
		addStage(new GoToComandosDoMedidorStage());
		addStage(new NavegarComandosDoMedidorAgendarParametrosStage(getDP("NavegarComandosDoMedidorAgendarParametrosDP.json")));
		addStage(new GoToComandosDoMedidorStage());
		addStage(new NavegarComandosDoMedidorHistoricoStage(getDP("NavegarComandosDoMedidorHistoricoDP.json")));
	}	
}
