package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.GoToTiposDeEquipamentoStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.NavegarAlteracaoTiposDeEquipamentoStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.NavegarCadastroTiposDeEquipamentoStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.NavegarRegistroLinkTiposDeEquipamentoStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.NavegarTiposDeEquipamentoStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0010TiposDeEquipamentoNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0010-TiposDeEquipamento", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Tipos de Equipamento validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0010TiposDeEquipamentoNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToTiposDeEquipamentoStage());
		addStage(new NavegarTiposDeEquipamentoStage(getDP("NavegarTiposDeEquipamentoDP.json")));
		addStage(new GoToTiposDeEquipamentoStage());
		addStage(new NavegarCadastroTiposDeEquipamentoStage(getDP("NavegarCadastroTiposDeEquipamentoDP.json")));
		addStage(new NavegarRegistroLinkTiposDeEquipamentoStage(getDP("NavegarRegistroLinkTiposDeEquipamentoDP.json")));
		addStage(new NavegarAlteracaoTiposDeEquipamentoStage(getDP("NavegarAlteracaoTiposDeEquipamentoDP.json")));
	}	
}
