package br.com.upperselenium.test.stage.configuracoes.administracao.pastas;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.dpo.NavegarCadastroPastasDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.page.CadastroPastasPage;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.page.PastasPage;

public class NavegarCadastroPastasStage extends BaseStage {
	private NavegarCadastroPastasDPO navegarCadastroPastasDPO;
	private PastasPage pastasPage;
	private CadastroPastasPage cadastroPastasPage;
	
	public NavegarCadastroPastasStage(String dp) {
		navegarCadastroPastasDPO = loadDataProviderFile(NavegarCadastroPastasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		pastasPage = initElementsFromPage(PastasPage.class);
		cadastroPastasPage = initElementsFromPage(CadastroPastasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarCadastroPastasDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		pastasPage.clickNova();
		pastasPage.getBreadcrumbsText(navegarCadastroPastasDPO.getBreadcrumbs());
		pastasPage.clickUpToBreadcrumbs(navegarCadastroPastasDPO.getUpToBreadcrumb());
		pastasPage.getWelcomeTitle(navegarCadastroPastasDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {
		cadastroPastasPage.validateMessageDefault(navegarCadastroPastasDPO.getOperationMessage());
	}
	
}
