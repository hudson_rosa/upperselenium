package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.dpo.NavegarRegistroLinkTiposDeMedidorDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.page.AlteracaoTiposDeMedidorPage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.page.TiposDeMedidorPage;

public class NavegarRegistroLinkTiposDeMedidorStage extends BaseStage {
	private NavegarRegistroLinkTiposDeMedidorDPO navegarRegistroLinkTiposDePontoDPO;
	private TiposDeMedidorPage tiposDeMedidorPage;
	private AlteracaoTiposDeMedidorPage alteracaoTiposDeMedidorPage;
	
	public NavegarRegistroLinkTiposDeMedidorStage(String dp) {
		navegarRegistroLinkTiposDePontoDPO = loadDataProviderFile(NavegarRegistroLinkTiposDeMedidorDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDeMedidorPage = initElementsFromPage(TiposDeMedidorPage.class);
		alteracaoTiposDeMedidorPage = initElementsFromPage(AlteracaoTiposDeMedidorPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarRegistroLinkTiposDePontoDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		tiposDeMedidorPage.clickGridFirstLineLinkMarca(navegarRegistroLinkTiposDePontoDPO.getClickItem());
		alteracaoTiposDeMedidorPage.clickAbaAbaS();
		alteracaoTiposDeMedidorPage.getLabelHeaderAbas(navegarRegistroLinkTiposDePontoDPO.getLabelAbas());
		alteracaoTiposDeMedidorPage.getLabelHeaderVisualizar(navegarRegistroLinkTiposDePontoDPO.getLabelVisualizar());
		alteracaoTiposDeMedidorPage.getLabelButtonSalvar(navegarRegistroLinkTiposDePontoDPO.getLabelButtonSalvar());
		alteracaoTiposDeMedidorPage.getBreadcrumbsText(navegarRegistroLinkTiposDePontoDPO.getBreadcrumbs());
		alteracaoTiposDeMedidorPage.clickUpToBreadcrumbs(navegarRegistroLinkTiposDePontoDPO.getUpToBreadcrumb());
		alteracaoTiposDeMedidorPage.getWelcomeTitle(navegarRegistroLinkTiposDePontoDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
