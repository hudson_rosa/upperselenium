package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.implantacao.configuracoes_de_tarefas.GoToConfiguracoesDeTarefasStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.configuracoes_de_tarefas.NavegarAlteracaoConfsDeTarefasStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0007ConfiguracoesDeTarefasNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0007-ConfiguracoesDeTarefas", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Configurações de Tarefas validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0007ConfiguracoesDeTarefasNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToConfiguracoesDeTarefasStage());
		addStage(new NavegarAlteracaoConfsDeTarefasStage(getDP("NavegarAlteracaoConfsDeTarefasDP.json")));
	}	
}
