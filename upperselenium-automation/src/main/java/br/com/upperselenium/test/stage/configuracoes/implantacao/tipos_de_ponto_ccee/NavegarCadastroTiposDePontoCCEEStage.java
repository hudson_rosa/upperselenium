package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.dpo.NavegarCadastroTiposDePontoCCEEDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.page.CadastroTiposDePontoCCEEPage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.page.TiposDePontoCCEEPage;

public class NavegarCadastroTiposDePontoCCEEStage extends BaseStage {
	private NavegarCadastroTiposDePontoCCEEDPO navegarCadastroTiposDePontoCCEEDPO;
	private TiposDePontoCCEEPage tiposDePontoCCEEPage;
	private CadastroTiposDePontoCCEEPage cadastroTiposDePontoCCEEPage;
	
	public NavegarCadastroTiposDePontoCCEEStage(String dp) {
		navegarCadastroTiposDePontoCCEEDPO = loadDataProviderFile(NavegarCadastroTiposDePontoCCEEDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDePontoCCEEPage = initElementsFromPage(TiposDePontoCCEEPage.class);
		cadastroTiposDePontoCCEEPage = initElementsFromPage(CadastroTiposDePontoCCEEPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarCadastroTiposDePontoCCEEDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		tiposDePontoCCEEPage.clickNovo();
		cadastroTiposDePontoCCEEPage.getLabelButtonSalvar(navegarCadastroTiposDePontoCCEEDPO.getLabelButtonSalvar());
		cadastroTiposDePontoCCEEPage.getBreadcrumbsText(navegarCadastroTiposDePontoCCEEDPO.getBreadcrumbs());
		cadastroTiposDePontoCCEEPage.clickUpToBreadcrumbs(navegarCadastroTiposDePontoCCEEDPO.getUpToBreadcrumb());
		cadastroTiposDePontoCCEEPage.getWelcomeTitle(navegarCadastroTiposDePontoCCEEDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
