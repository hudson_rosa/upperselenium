package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.dpo.NavegarMonitoramentoDeUsinasDPO;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.page.AcompDiarioDeUsinasPage;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.page.MonitoramentoDeUsinasPage;

public class NavegarMonitoramentoDeUsinasStage extends BaseStage {
	private NavegarMonitoramentoDeUsinasDPO navegarMonitoramentoDeUsinasDPO;
	private AcompDiarioDeUsinasPage acompDiarioDeUsinasPage;	
	private MonitoramentoDeUsinasPage monitoramentoDeUsinasPage;	
	
	public NavegarMonitoramentoDeUsinasStage(String dp) {
		navegarMonitoramentoDeUsinasDPO = loadDataProviderFile(NavegarMonitoramentoDeUsinasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		acompDiarioDeUsinasPage = initElementsFromPage(AcompDiarioDeUsinasPage.class);
		monitoramentoDeUsinasPage = initElementsFromPage(MonitoramentoDeUsinasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarMonitoramentoDeUsinasDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		acompDiarioDeUsinasPage.clickLinkMonitoramentoDeUsinas();
		monitoramentoDeUsinasPage.getLabelNiveis(navegarMonitoramentoDeUsinasDPO.getLabelNiveis());
		monitoramentoDeUsinasPage.getLabelVertimento(navegarMonitoramentoDeUsinasDPO.getLabelVertimento());
		monitoramentoDeUsinasPage.getLabelEficiencia(navegarMonitoramentoDeUsinasDPO.getLabelEficiencia());
		monitoramentoDeUsinasPage.getLabelUnidadesGeradoras(navegarMonitoramentoDeUsinasDPO.getLabelUnidadesGeradoras());
		monitoramentoDeUsinasPage.getLabelVazao(navegarMonitoramentoDeUsinasDPO.getLabelVazao());
		monitoramentoDeUsinasPage.getBreadcrumbsText(navegarMonitoramentoDeUsinasDPO.getBreadcrumbs());
		monitoramentoDeUsinasPage.clickUpToBreadcrumbs(navegarMonitoramentoDeUsinasDPO.getUpToBreadcrumb());
		monitoramentoDeUsinasPage.getWelcomeTitle(navegarMonitoramentoDeUsinasDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
