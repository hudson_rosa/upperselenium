package br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager.dpo;

public class GridAlterarConnectionManagerDPO {

	private String canal;
	private String ip;
	private String porta;
	private String timeout;
	private String tipo;
	private String alwaysOn;
	private String novaConexaoFechaAntiga;
	private String reiniciarHubAoFecharAntiga;
	private String prioridade;
	private String buttonAdd;
	private String buttonExclude;

	public String getCanal() {
		return canal;
	}

	public void setCanal(String canal) {
		this.canal = canal;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPorta() {
		return porta;
	}

	public void setPorta(String porta) {
		this.porta = porta;
	}

	public String getTimeout() {
		return timeout;
	}

	public void setTimeout(String timeout) {
		this.timeout = timeout;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getAlwaysOn() {
		return alwaysOn;
	}

	public void setAlwaysOn(String alwaysOn) {
		this.alwaysOn = alwaysOn;
	}

	public String getNovaConexaoFechaAntiga() {
		return novaConexaoFechaAntiga;
	}

	public void setNovaConexaoFechaAntiga(String novaConexaoFechaAntiga) {
		this.novaConexaoFechaAntiga = novaConexaoFechaAntiga;
	}

	public String getReiniciarHubAoFecharAntiga() {
		return reiniciarHubAoFecharAntiga;
	}

	public void setReiniciarHubAoFecharAntiga(String reiniciarHubAoFecharAntiga) {
		this.reiniciarHubAoFecharAntiga = reiniciarHubAoFecharAntiga;
	}

	public String getPrioridade() {
		return prioridade;
	}

	public void setPrioridade(String prioridade) {
		this.prioridade = prioridade;
	}

	public String getButtonAdd() {
		return buttonAdd;
	}

	public void setButtonAdd(String buttonAdd) {
		this.buttonAdd = buttonAdd;
	}

	public String getButtonExclude() {
		return buttonExclude;
	}

	public void setButtonExclude(String buttonExclude) {
		this.buttonExclude = buttonExclude;
	}

}
