package br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.page;

import org.openqa.selenium.By;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoAbaPontosClientsSCDEPage extends BaseHelperPage{
	
	private static final String LINK_ABA_PONTOS = ".//div[2]/div/div[2]/ul[2]/li[2]/a"; 
	private static final String BUTTON_SALVAR = "//*[@id='client-scde-pontos']/form/input"; 
	private static final String TEXT_FILTER_NOME = "//*[@id='clientscdeponto']/thead/tr[1]/th[2]/span/input";
	private static final String TEXT_FILTER_PASTA = "//*[@id='clientscdeponto']/thead/tr[1]/th[3]/span/input";
	private static final String TEXT_FILTER_TIPO_DE_PONTO = "//*[@id='clientscdeponto']/thead/tr[1]/th[4]/span/input";
	private static final String GRID_CHECK_PONTO = "//*[@id='clientscdeponto']/tbody/tr/td[1]/input"; 
	
	public void typeTextFilterNome(String value){
		typeText(TEXT_FILTER_NOME, value);
	}	
	
	public void typeTextFilterPasta(String value){
		typeText(TEXT_FILTER_PASTA, value);
	}	
	
	public void typeTextFilterTipoDePonto(String value){
		typeText(TEXT_FILTER_TIPO_DE_PONTO, value);
	}	
	
	public void typeCheckBoxGridItemPonto(String value){
		typeGridCheckFirstFilteredItem(GRID_CHECK_PONTO, value);
	}	
			
	public boolean isClickableButtonSalvar() {
		return isDisplayedElement(By.xpath(BUTTON_SALVAR));
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}

	public void clickAbaPontos(){
		clickOnElement(LINK_ABA_PONTOS);
	}
	
}