package br.com.upperselenium.test.stage.configuracoes.cadastros.postos_tarifarios;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.postos_tarifarios.dpo.CadastrarPostosTarifariosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.postos_tarifarios.page.CadastroPostosTarifariosPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.postos_tarifarios.page.PostosTarifariosPage;

public class CadastrarPostosTarifariosStage extends BaseStage {
	private CadastrarPostosTarifariosDPO cadastrarPostosTarifariosDPO;
	private PostosTarifariosPage postosTarifariosPage;
	private CadastroPostosTarifariosPage cadastroPostosTarifariosPage;
	
	public CadastrarPostosTarifariosStage(String dp) {
		cadastrarPostosTarifariosDPO = loadDataProviderFile(CadastrarPostosTarifariosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		postosTarifariosPage = initElementsFromPage(PostosTarifariosPage.class);
		cadastroPostosTarifariosPage = initElementsFromPage(CadastroPostosTarifariosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarPostosTarifariosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		postosTarifariosPage.clickNovo();
		cadastroPostosTarifariosPage.typeTextNome(cadastrarPostosTarifariosDPO.getNome()+getRandomStringSpaced());		
		cadastroPostosTarifariosPage.typeCheckBoxDeslocarNoHorarioDeVerao(cadastrarPostosTarifariosDPO.getDeslocarNoHorarioDeVerao());		
		cadastroPostosTarifariosPage.typeTextHorarioDePontaInicio(cadastrarPostosTarifariosDPO.getHorarioDePontaInicio());		
		cadastroPostosTarifariosPage.typeTextHorarioDePontaFim(cadastrarPostosTarifariosDPO.getHorarioDePontaFim());
		cadastroPostosTarifariosPage.typeCheckSegundaHPonta(cadastrarPostosTarifariosDPO.getSegundaHPonta());		
		cadastroPostosTarifariosPage.typeCheckTercaHPonta(cadastrarPostosTarifariosDPO.getTercaHPonta());		
		cadastroPostosTarifariosPage.typeCheckQuartaHPonta(cadastrarPostosTarifariosDPO.getQuartaHPonta());		
		cadastroPostosTarifariosPage.typeCheckQuintaHPonta(cadastrarPostosTarifariosDPO.getQuintaHPonta());		
		cadastroPostosTarifariosPage.typeCheckSextaHPonta(cadastrarPostosTarifariosDPO.getSextaHPonta());		
		cadastroPostosTarifariosPage.typeCheckSabadoHPonta(cadastrarPostosTarifariosDPO.getSabadoHPonta());		
		cadastroPostosTarifariosPage.typeCheckDomingoHPonta(cadastrarPostosTarifariosDPO.getDomingoHPonta());		
		cadastroPostosTarifariosPage.typeCheckFeriadoHPonta(cadastrarPostosTarifariosDPO.getFeriadoHPonta());		
		cadastroPostosTarifariosPage.typeTextHorarioCapacitivoInicio(cadastrarPostosTarifariosDPO.getHorarioCapacitivoInicio());		
		cadastroPostosTarifariosPage.typeTextHorarioCapacitivoFim(cadastrarPostosTarifariosDPO.getHorarioCapacitivoFim());
		cadastroPostosTarifariosPage.typeCheckSegundaHCapacitivo(cadastrarPostosTarifariosDPO.getSegundaHCapacitivo());		
		cadastroPostosTarifariosPage.typeCheckTercaHCapacitivo(cadastrarPostosTarifariosDPO.getTercaHCapacitivo());		
		cadastroPostosTarifariosPage.typeCheckQuartaHCapacitivo(cadastrarPostosTarifariosDPO.getQuartaHCapacitivo());		
		cadastroPostosTarifariosPage.typeCheckQuintaHCapacitivo(cadastrarPostosTarifariosDPO.getQuintaHCapacitivo());		
		cadastroPostosTarifariosPage.typeCheckSextaHCapacitivo(cadastrarPostosTarifariosDPO.getSextaHCapacitivo());		
		cadastroPostosTarifariosPage.typeCheckSabadoHCapacitivo(cadastrarPostosTarifariosDPO.getSabadoHCapacitivo());		
		cadastroPostosTarifariosPage.typeCheckDomingoHCapacitivo(cadastrarPostosTarifariosDPO.getDomingoHCapacitivo());		
		cadastroPostosTarifariosPage.typeCheckFeriadoHCapacitivo(cadastrarPostosTarifariosDPO.getFeriadoHCapacitivo());	
		cadastroPostosTarifariosPage.typeCheckBoxHorarioReservado(cadastrarPostosTarifariosDPO.getHorarioReservado());		
		cadastroPostosTarifariosPage.typeTextHorarioReservadoInicio(cadastrarPostosTarifariosDPO.getHorarioReservadoInicio());		
		cadastroPostosTarifariosPage.typeTextHorarioReservadoFim(cadastrarPostosTarifariosDPO.getHorarioReservadoFim());
		cadastroPostosTarifariosPage.typeCheckSegundaHReservado(cadastrarPostosTarifariosDPO.getSegundaHReservado());		
		cadastroPostosTarifariosPage.typeCheckTercaHReservado(cadastrarPostosTarifariosDPO.getTercaHReservado());		
		cadastroPostosTarifariosPage.typeCheckQuartaHReservado(cadastrarPostosTarifariosDPO.getQuartaHReservado());		
		cadastroPostosTarifariosPage.typeCheckQuintaHReservado(cadastrarPostosTarifariosDPO.getQuintaHReservado());		
		cadastroPostosTarifariosPage.typeCheckSextaHReservado(cadastrarPostosTarifariosDPO.getSextaHReservado());		
		cadastroPostosTarifariosPage.typeCheckSabadoHReservado(cadastrarPostosTarifariosDPO.getSabadoHReservado());		
		cadastroPostosTarifariosPage.typeCheckDomingoHReservado(cadastrarPostosTarifariosDPO.getDomingoHReservado());		
		cadastroPostosTarifariosPage.typeCheckFeriadoHReservado(cadastrarPostosTarifariosDPO.getFeriadoHReservado());	
		cadastroPostosTarifariosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		validateLabelHorarioDeForaPontaInicio();
		validateLabelHorarioDeForaPontaFim();
		cadastroPostosTarifariosPage.validateMessageDefault(cadastrarPostosTarifariosDPO.getOperationMessage());
	}

	private void validateLabelHorarioDeForaPontaInicio() {
		cadastroPostosTarifariosPage.validateEqualsToValues(
				cadastrarPostosTarifariosDPO.getHorarioDeForaPontaInicio(), 
				cadastroPostosTarifariosPage.getLabelHorarioDeForaPontaInicio());
	}
	
	private void validateLabelHorarioDeForaPontaFim() {
		cadastroPostosTarifariosPage.validateEqualsToValues(
				cadastrarPostosTarifariosDPO.getHorarioDeForaPontaFim(), 
				cadastroPostosTarifariosPage.getLabelHorarioDeForaPontaFim());
	}

}
