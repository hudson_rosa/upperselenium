package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastrarAbaEquipamentosTrafoPontosVirtuaisPage extends BaseHelperPage{
	
	private static final String LINK_EQUIPAMENTOS = "//*[@id='tabs']/ul/li[@class='dropdown']/a";
	private static final String LINK_SUBMENU_TRAFO = "//*[@id='tabs']/ul/li[contains(@class,'dropdown')]/ul[contains(@class,'dropdown-menu')]/li[1]/a[2]";
	private static final String BUTTON_SALVAR = "//*[@id='salvar']";   
	private static final String SELECT_TIPO = "//*[@id='FabricanteTrafo']";
	private static final String TEXT_ANO_DE_FABRICACAO = "//*[@id='AnoDeFabricacao']";
	private static final String TEXT_DESCRICAO = "//*[@id='Descricao']";
	private static final String TEXT_NUMERO_DE_SERIE = "//*[@id='NumeroDeSerieTrafo']";
	private static final String TEXT_CODIGO_INTERNO = "//*[@id='CodigoInterno']";
	private static final String TEXT_PROPRIEDADE = "//*[@id='PropriedadeTrafo']";
	private static final String TEXT_POTENCIA_ONAN = "//*[@id='PotenciaOnan']";
	private static final String TEXT_POTENCIA_ONAF = "//*[@id='PotenciaOnaf']";
	private static final String TEXT_POTENCIA_NOMINAL = "//*[@id='PotenciaNominal']";
	private static final String TEXT_UTILIZACAO_ONAF = "//*[@id='UtilizacaoOnaf']";
	private static final String TEXT_TENSAO_PRIMARIA = "//*[@id='TensaoPrimariaTrafo']";
	private static final String TEXT_TENSAO_SECUNDARIA = "//*[@id='TensaoSecundariaTrafo']";
	private static final String TEXT_PERDAS_NO_FERRO = "//*[@id='PerdasNoFerro']";
	private static final String TEXT_PERDAS_NO_COBRE = "//*[@id='PerdasNoCobre']";
	private static final String TEXT_IMPEDANCIA = "//*[@id='Impedancia']";
	private static final String TEXT_EXITACAO = "//*[@id='Exitacao']";
	private static final String TEXT_TAP_ACIMA = "//*[@id='TapAcima']";
	private static final String TEXT_TAP_ABAIXO = "//*[@id='TapAbaixo']";
	private static final String TEXT_MEDICAO_NA_ALTA = "//*[@id='MedicaoNaAlta']";

	public void clickEquipamentos(){
		clickOnElement(LINK_EQUIPAMENTOS);
	}

	public void clickSubmenuTrafo(){		
		clickOnElement(LINK_SUBMENU_TRAFO);
	}
	
	public void typeSelectTipo(String value){
		typeSelectComboOption(SELECT_TIPO, value);
	}
	
	public void typeTextAnoDeFabricacao(String value){
		typeText(TEXT_ANO_DE_FABRICACAO, value);
	}	
	
	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}	
	
	public void typeTextNumeroDeSerie(String value){
		typeText(TEXT_NUMERO_DE_SERIE, value);
	}	
	
	public void typeTextCodigoInterno(String value){
		typeText(TEXT_CODIGO_INTERNO, value);
	}	
	
	public void typeTextPropriedade(String value){
		typeText(TEXT_PROPRIEDADE, value);
	}	
	
	public void typeTextPotenciaONAN(String value){
		typeText(TEXT_POTENCIA_ONAN, value);
	}	
	
	public void typeTextPotenciaONAF(String value){
		typeText(TEXT_POTENCIA_ONAF, value);
	}	
	
	public void typeTextPotenciaNominal(String value){
		typeText(TEXT_POTENCIA_NOMINAL, value);
	}	
	
	public void typeTextUtilizacaoONAF(String value){
		typeText(TEXT_UTILIZACAO_ONAF, value);
	}	
	
	public void typeTextTensaoPrimaria(String value){
		typeText(TEXT_TENSAO_PRIMARIA, value);
	}	
	
	public void typeTextTensaoSecundaria(String value){
		typeText(TEXT_TENSAO_SECUNDARIA, value);
	}	
	
	public void typeTextPerdasNoFerro(String value){
		typeText(TEXT_PERDAS_NO_FERRO, value);
	}	
	
	public void typeTextPerdasNoCobre(String value){
		typeText(TEXT_PERDAS_NO_COBRE, value);
	}	
	
	public void typeTextImpedancia(String value){
		typeText(TEXT_IMPEDANCIA, value);
	}	
	
	public void typeTextExitacao(String value){
		typeText(TEXT_EXITACAO, value);
	}	
	
	public void typeTextTapAcima(String value){
		typeText(TEXT_TAP_ACIMA, value);
	}	
	
	public void typeTextTapAbaixo(String value){
		typeText(TEXT_TAP_ABAIXO, value);
	}	
	
	public void typeTextMedicaoNaAlta(String value){
		typeTextAutoCompleteSelect(TEXT_MEDICAO_NA_ALTA, value);
	}	
		
	public void keyPageDown1(){
		useKey(TEXT_ANO_DE_FABRICACAO, Keys.PAGE_DOWN);
	}	

	public void keyPageDown2(){
		useKey(TEXT_POTENCIA_NOMINAL, Keys.PAGE_DOWN);
	}	
	
	public void keyPageDown3(){
		useKey(TEXT_PERDAS_NO_COBRE, Keys.PAGE_DOWN);
	}	

	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}