package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.classes_de_isolacao.CadastrarClassesDeIsolacaoStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.classes_de_isolacao.GoToClassesDeIsolacaoStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0007ClassesDeIsolacaoCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0007-ClassesDeIsolacao", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Classes de Isolação realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0007ClassesDeIsolacaoCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToClassesDeIsolacaoStage());
		addStage(new CadastrarClassesDeIsolacaoStage(getDP("CadastrarClassesDeIsolacaoDP.json")));
	}	
}
