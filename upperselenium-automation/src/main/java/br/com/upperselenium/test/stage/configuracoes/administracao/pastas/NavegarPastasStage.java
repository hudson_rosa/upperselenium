package br.com.upperselenium.test.stage.configuracoes.administracao.pastas;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.dpo.NavegarPastasDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.page.PastasPage;

public class NavegarPastasStage extends BaseStage {
	private NavegarPastasDPO navegarPastasDPO;
	private PastasPage pastasPage;
	
	public NavegarPastasStage(String dp) {
		navegarPastasDPO = loadDataProviderFile(NavegarPastasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		pastasPage = initElementsFromPage(PastasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarPastasDPO.getIssue());
		execute();
	}
	
	private void execute() {
		pastasPage.getBreadcrumbsText(navegarPastasDPO.getBreadcrumbs());
		pastasPage.clickUpToBreadcrumbs(navegarPastasDPO.getUpToBreadcrumb());
		pastasPage.getWelcomeTitle(navegarPastasDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
