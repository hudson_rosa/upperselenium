package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaMedidoresPontosDPO extends BaseHelperDPO {

	private String principal;
	private String numeroDeSeriePrincipal;
	private String numeroInternoPrincipal;
	private String codigoSCDEPrincipal;
	private String retaguarda;
	private String numeroDeSerieRetaguarda;
	private String numeroInternoRetaguarda;
	private String codigoSCDERetaguarda;

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public String getNumeroDeSeriePrincipal() {
		return numeroDeSeriePrincipal;
	}

	public void setNumeroDeSeriePrincipal(String numeroDeSeriePrincipal) {
		this.numeroDeSeriePrincipal = numeroDeSeriePrincipal;
	}

	public String getNumeroInternoPrincipal() {
		return numeroInternoPrincipal;
	}

	public void setNumeroInternoPrincipal(String numeroInternoPrincipal) {
		this.numeroInternoPrincipal = numeroInternoPrincipal;
	}

	public String getCodigoSCDEPrincipal() {
		return codigoSCDEPrincipal;
	}

	public void setCodigoSCDEPrincipal(String codigoSCDEPrincipal) {
		this.codigoSCDEPrincipal = codigoSCDEPrincipal;
	}

	public String getRetaguarda() {
		return retaguarda;
	}

	public void setRetaguarda(String retaguarda) {
		this.retaguarda = retaguarda;
	}

	public String getNumeroDeSerieRetaguarda() {
		return numeroDeSerieRetaguarda;
	}

	public void setNumeroDeSerieRetaguarda(String numeroDeSerieRetaguarda) {
		this.numeroDeSerieRetaguarda = numeroDeSerieRetaguarda;
	}

	public String getNumeroInternoRetaguarda() {
		return numeroInternoRetaguarda;
	}

	public void setNumeroInternoRetaguarda(String numeroInternoRetaguarda) {
		this.numeroInternoRetaguarda = numeroInternoRetaguarda;
	}

	public String getCodigoSCDERetaguarda() {
		return codigoSCDERetaguarda;
	}

	public void setCodigoSCDERetaguarda(String codigoSCDERetaguarda) {
		this.codigoSCDERetaguarda = codigoSCDERetaguarda;
	}

}