package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaComunicacaoMedidoresAtoLDPO extends BaseHelperDPO {

	// ABS SSU Modbus - Ethernet
	private String comunicacaoAbsSsuModBus;
	private String absSsuEnderecoOuIp;
	private String absSsuPorta;
	private String absSsuId;

	// AMI Cylec *
	private String comunicacaoAmyCylec;

	// ANSI-C12
	private String comunicacaoAnsiC12;
	private String ansiC12EnderecoOuIp;
	private String ansiC12Porta;
	private String ansiC12ModeloDeMedidor;

	// ApiConstruserv
	private String comunicacaoApiConstruserv;
	private String construservIdentificadorDaEstacao;

	// ArquivosAmmonit
	private String comunicacaoArqAmmonit;
	private String ammonitVelVentoMediaAnemSuperior;
	private String ammonitVelVentoMaximaAnemSuperior;
	private String ammonitVelVentoMinimaAnemSuperior;
	private String ammonitVelVentoDesvioPadraoAnemSuperior;
	private String ammonitVelVentoMediaAnem2;
	private String ammonitVelVentoMaximaAnem2;
	private String ammonitVelVentoMinimaAnem2;
	private String ammonitVelVentoDesvioPadraoAnem2;
	private String ammonitVelVentoMediaAnem3;
	private String ammonitVelVentoMaximaAnem3;
	private String ammonitVelVentoMinimaAnem3;
	private String ammonitVelVentoDesvioPadraoAnem3;
	private String ammonitDirVentoMediaWindVaneSuperior;
	private String ammonitDirVentoDesvioPadraoWindVaneSuperior;
	private String ammonitDirVentoMediaWindVane2;
	private String ammonitDirVentoDesvioPadraoWindVane2;
	private String ammonitPressaoDoAr;
	private String ammonitTemperaturaDoAr;
	private String ammonitUmidadeRelativaDoAr;

	// ArquivosNOMAD
	private String comunicacaoArqNomad;
	private String nomadVelVentoMediaAnemSuperior;
	private String nomadVelVentoMaximaAnemSuperior;
	private String nomadVelVentoMinimaAnemSuperior;
	private String nomadVelVentoDesvioPadraoAnemSuperior;
	private String nomadVelVentoMediaAnem2;
	private String nomadVelVentoMaximaAnem2;
	private String nomadVelVentoMinimaAnem2;
	private String nomadVelVentoDesvioPadraoAnem2;
	private String nomadVelVentoMediaAnem3;
	private String nomadVelVentoMaximaAnem3;
	private String nomadVelVentoMinimaAnem3;
	private String nomadVelVentoDesvioPadraoAnem3;
	private String nomadDirVentoMediaWindVaneSuperior;
	private String nomadDirVentoDesvioPadraoWindVaneSuperior;
	private String nomadDirVentoMediaWindVane2;
	private String nomadDirVentoDesvioPadraoWindVane2;
	private String nomadPressaoDoAr;
	private String nomadTemperaturaDoAr;
	private String nomadUmidadeRelativaDoAr;

	// Bancada virtual Way2
	private String comunicacaoBancVirtualWay2;
	private String way2EnderecoOuIp;
	private String way2Porta;
	private String way2Id;

	// CCK
	private String comunicacaoCck;
	private String cckStrRemoto;

	// CCK 7550S - Ethernet
	private String comunicacaoCck7550SEth;
	private String cck7550EthEnderecoOuIp;
	private String cck7550EthPorta;
	private String cck7550EthId;

	// CD103
	private String comunicacaoCd103;
	private String cd103Gateway;

	// Coletor Titan
	private String comunicacaoColetorTitan;
	private String titanCaixa;

	// C750
	private String comunicacaoC750;
	private String c750Gateway;

	// ELO 2113 - ABNT Multiponto Serial Ethernet
	private String comunicacaoElo2113Eth;
	private String elo2113MpsEthEnderecoOuIp;
	private String elo2113MpsEthPorta;

	// ELO 2180 - Ethernet
	private String comunicacaoElo2180Eth;
	private String elo2180EthEnderecoOuIp;
	private String elo2180EthPorta;
	private String elo2180EthId;

	// ELO 2180 - V2.xx - Ethernet
	private String comunicacaoElo2180V2XXEth;
	private String elo2180V2xxEnderecoOuIp;
	private String elo2180V2xxPorta;
	private String elo2180V2xxId;

	// Elster A3RBR - Monoponto
	private String comunicacaoElsterA3RBRMp;
	private String elsterA3rbrEnderecoOuIp;
	private String elsterA3rbrPorta;

	// E650 - Monoponto
	private String comunicacaoE650Mp;
	private String e650MpEnderecoOuIp;
	private String e650MpPorta;

	// E750 - Ethernet
	private String comunicacaoE750Eth;
	private String e750EthEnderecoOuIp;
	private String e750EthPorta;
	private String e750EthId;

	// E750 - Monoponto
	private String comunicacaoE750Mp;
	private String e750MpEnderecoOuIp;
	private String e750MpPorta;

	// Garnet Elster *
	private String comunicacaoGarnetElster;

	// Gateway Reader
	private String comunicacaoGatewayReader;
	private String readerGateway;

	// Hidrometrologia Skywave *
	private String comunicacaoHidroSkywave;

	// Integração Banco de Dados Automação
	private String comunicacaoIntegBDAutomacao;
	private String bdAutomacaoIdentificadorRemoto;

	// ION Dial-Up
	private String comunicacaoIonDialUp;
	private String ionDialUpTelefone;
	private String ionDialUpId;

	// ION - Ethergate ou Terminal Server
	private String comunicacaoIonEtgTerm;
	private String ionEtgTermEnderecoOuIp;
	private String ionEtgTermPorta;
	private String ionEtgTermId;
	private String ionEtgTermColetarFormasOnda;

	// ION - Ethernet
	private String comunicacaoIonEth;
	private String ionEthEnderecoOuIp;
	private String ionEthPorta;
	private String ionEthId;
	private String ionEthColetarFormasOnda;

	// ION - GPRS
	private String comunicacaoIonGprs;
	private String ionGprsEnderecoOuIp;
	private String ionGprsPorta;
	private String ionGprsId;
	private String ionGprsColetarFormasOnda;

	// ION - Serial
	private String comunicacaoIonSer;
	private String ionSerialEnderecoOuIp;
	private String ionSerialId;

	// ION-Ethernet-E3-RT
	private String comunicacaoIonEthE3Rt;
	private String ionEthE3RtEnderecoOuIp;
	private String ionEthE3RtPorta;
	private String ionEthE3RtId;
	private String ionEthE3RtColetarFormasOnda;

	// ION7650 - Ethernet
	private String comunicacaoIon7650Eth;
	private String ion7650EthEnderecoOuIp;
	private String ion7650EthPorta;
	private String ion7650EthId;
	private String ion7650EthColetarFormasOnda;

	// iTech UTW *
	private String comunicacaoITechUtw;

	// Landis E34 - ABNT Multiponto Serial Ethernet
	private String comunicacaoLandisE34Eth;
	private String landisE34MpEthEnderecoOuIp;
	private String landisE34MpEthPorta;

	public String getComunicacaoAbsSsuModBus() {
		return comunicacaoAbsSsuModBus;
	}

	public String getAbsSsuEnderecoOuIp() {
		return absSsuEnderecoOuIp;
	}

	public String getAbsSsuPorta() {
		return absSsuPorta;
	}

	public String getAbsSsuId() {
		return absSsuId;
	}

	public String getComunicacaoAmyCylec() {
		return comunicacaoAmyCylec;
	}

	public String getComunicacaoAnsiC12() {
		return comunicacaoAnsiC12;
	}

	public String getAnsiC12EnderecoOuIp() {
		return ansiC12EnderecoOuIp;
	}

	public String getAnsiC12Porta() {
		return ansiC12Porta;
	}

	public String getAnsiC12ModeloDeMedidor() {
		return ansiC12ModeloDeMedidor;
	}

	public String getComunicacaoApiConstruserv() {
		return comunicacaoApiConstruserv;
	}

	public String getConstruservIdentificadorDaEstacao() {
		return construservIdentificadorDaEstacao;
	}

	public String getComunicacaoArqAmmonit() {
		return comunicacaoArqAmmonit;
	}

	public String getAmmonitVelVentoMediaAnemSuperior() {
		return ammonitVelVentoMediaAnemSuperior;
	}

	public String getAmmonitVelVentoMaximaAnemSuperior() {
		return ammonitVelVentoMaximaAnemSuperior;
	}

	public String getAmmonitVelVentoMinimaAnemSuperior() {
		return ammonitVelVentoMinimaAnemSuperior;
	}

	public String getAmmonitVelVentoDesvioPadraoAnemSuperior() {
		return ammonitVelVentoDesvioPadraoAnemSuperior;
	}

	public String getAmmonitVelVentoMediaAnem2() {
		return ammonitVelVentoMediaAnem2;
	}

	public String getAmmonitVelVentoMaximaAnem2() {
		return ammonitVelVentoMaximaAnem2;
	}

	public String getAmmonitVelVentoMinimaAnem2() {
		return ammonitVelVentoMinimaAnem2;
	}

	public String getAmmonitVelVentoDesvioPadraoAnem2() {
		return ammonitVelVentoDesvioPadraoAnem2;
	}

	public String getAmmonitVelVentoMediaAnem3() {
		return ammonitVelVentoMediaAnem3;
	}

	public String getAmmonitVelVentoMaximaAnem3() {
		return ammonitVelVentoMaximaAnem3;
	}

	public String getAmmonitVelVentoMinimaAnem3() {
		return ammonitVelVentoMinimaAnem3;
	}

	public String getAmmonitVelVentoDesvioPadraoAnem3() {
		return ammonitVelVentoDesvioPadraoAnem3;
	}

	public String getAmmonitDirVentoMediaWindVaneSuperior() {
		return ammonitDirVentoMediaWindVaneSuperior;
	}

	public String getAmmonitDirVentoDesvioPadraoWindVaneSuperior() {
		return ammonitDirVentoDesvioPadraoWindVaneSuperior;
	}

	public String getAmmonitDirVentoMediaWindVane2() {
		return ammonitDirVentoMediaWindVane2;
	}

	public String getAmmonitDirVentoDesvioPadraoWindVane2() {
		return ammonitDirVentoDesvioPadraoWindVane2;
	}

	public String getAmmonitPressaoDoAr() {
		return ammonitPressaoDoAr;
	}

	public String getAmmonitTemperaturaDoAr() {
		return ammonitTemperaturaDoAr;
	}

	public String getAmmonitUmidadeRelativaDoAr() {
		return ammonitUmidadeRelativaDoAr;
	}

	public String getComunicacaoArqNomad() {
		return comunicacaoArqNomad;
	}

	public String getNomadVelVentoMediaAnemSuperior() {
		return nomadVelVentoMediaAnemSuperior;
	}

	public String getNomadVelVentoMaximaAnemSuperior() {
		return nomadVelVentoMaximaAnemSuperior;
	}

	public String getNomadVelVentoMinimaAnemSuperior() {
		return nomadVelVentoMinimaAnemSuperior;
	}

	public String getNomadVelVentoDesvioPadraoAnemSuperior() {
		return nomadVelVentoDesvioPadraoAnemSuperior;
	}

	public String getNomadVelVentoMediaAnem2() {
		return nomadVelVentoMediaAnem2;
	}

	public String getNomadVelVentoMaximaAnem2() {
		return nomadVelVentoMaximaAnem2;
	}

	public String getNomadVelVentoMinimaAnem2() {
		return nomadVelVentoMinimaAnem2;
	}

	public String getNomadVelVentoDesvioPadraoAnem2() {
		return nomadVelVentoDesvioPadraoAnem2;
	}

	public String getNomadVelVentoMediaAnem3() {
		return nomadVelVentoMediaAnem3;
	}

	public String getNomadVelVentoMaximaAnem3() {
		return nomadVelVentoMaximaAnem3;
	}

	public String getNomadVelVentoMinimaAnem3() {
		return nomadVelVentoMinimaAnem3;
	}

	public String getNomadVelVentoDesvioPadraoAnem3() {
		return nomadVelVentoDesvioPadraoAnem3;
	}

	public String getNomadDirVentoMediaWindVaneSuperior() {
		return nomadDirVentoMediaWindVaneSuperior;
	}

	public String getNomadDirVentoDesvioPadraoWindVaneSuperior() {
		return nomadDirVentoDesvioPadraoWindVaneSuperior;
	}

	public String getNomadDirVentoMediaWindVane2() {
		return nomadDirVentoMediaWindVane2;
	}

	public String getNomadDirVentoDesvioPadraoWindVane2() {
		return nomadDirVentoDesvioPadraoWindVane2;
	}

	public String getNomadPressaoDoAr() {
		return nomadPressaoDoAr;
	}

	public String getNomadTemperaturaDoAr() {
		return nomadTemperaturaDoAr;
	}

	public String getNomadUmidadeRelativaDoAr() {
		return nomadUmidadeRelativaDoAr;
	}

	public String getComunicacaoBancVirtualWay2() {
		return comunicacaoBancVirtualWay2;
	}

	public String getWay2EnderecoOuIp() {
		return way2EnderecoOuIp;
	}

	public String getWay2Porta() {
		return way2Porta;
	}

	public String getWay2Id() {
		return way2Id;
	}

	public String getComunicacaoCck() {
		return comunicacaoCck;
	}

	public String getCckStrRemoto() {
		return cckStrRemoto;
	}

	public String getComunicacaoCck7550SEth() {
		return comunicacaoCck7550SEth;
	}

	public String getCck7550EthEnderecoOuIp() {
		return cck7550EthEnderecoOuIp;
	}

	public String getCck7550EthPorta() {
		return cck7550EthPorta;
	}

	public String getCck7550EthId() {
		return cck7550EthId;
	}

	public String getComunicacaoCd103() {
		return comunicacaoCd103;
	}

	public String getCd103Gateway() {
		return cd103Gateway;
	}

	public String getComunicacaoColetorTitan() {
		return comunicacaoColetorTitan;
	}

	public String getTitanCaixa() {
		return titanCaixa;
	}

	public String getComunicacaoC750() {
		return comunicacaoC750;
	}

	public String getC750Gateway() {
		return c750Gateway;
	}

	public String getComunicacaoElo2113Eth() {
		return comunicacaoElo2113Eth;
	}

	public String getElo2113MpsEthEnderecoOuIp() {
		return elo2113MpsEthEnderecoOuIp;
	}

	public String getElo2113MpsEthPorta() {
		return elo2113MpsEthPorta;
	}

	public String getComunicacaoElo2180Eth() {
		return comunicacaoElo2180Eth;
	}

	public String getElo2180EthEnderecoOuIp() {
		return elo2180EthEnderecoOuIp;
	}

	public String getElo2180EthPorta() {
		return elo2180EthPorta;
	}

	public String getElo2180EthId() {
		return elo2180EthId;
	}

	public String getComunicacaoElo2180V2XXEth() {
		return comunicacaoElo2180V2XXEth;
	}

	public String getElo2180V2xxEnderecoOuIp() {
		return elo2180V2xxEnderecoOuIp;
	}

	public String getElo2180V2xxPorta() {
		return elo2180V2xxPorta;
	}

	public String getElo2180V2xxId() {
		return elo2180V2xxId;
	}

	public String getComunicacaoElsterA3RBRMp() {
		return comunicacaoElsterA3RBRMp;
	}

	public String getElsterA3rbrEnderecoOuIp() {
		return elsterA3rbrEnderecoOuIp;
	}

	public String getElsterA3rbrPorta() {
		return elsterA3rbrPorta;
	}

	public String getComunicacaoE650Mp() {
		return comunicacaoE650Mp;
	}

	public String getE650MpEnderecoOuIp() {
		return e650MpEnderecoOuIp;
	}

	public String getE650MpPorta() {
		return e650MpPorta;
	}

	public String getComunicacaoE750Eth() {
		return comunicacaoE750Eth;
	}

	public String getE750EthEnderecoOuIp() {
		return e750EthEnderecoOuIp;
	}

	public String getE750EthPorta() {
		return e750EthPorta;
	}

	public String getE750EthId() {
		return e750EthId;
	}

	public String getComunicacaoE750Mp() {
		return comunicacaoE750Mp;
	}

	public String getE750MpEnderecoOuIp() {
		return e750MpEnderecoOuIp;
	}

	public String getE750MpPorta() {
		return e750MpPorta;
	}

	public String getComunicacaoGarnetElster() {
		return comunicacaoGarnetElster;
	}

	public String getComunicacaoGatewayReader() {
		return comunicacaoGatewayReader;
	}

	public String getReaderGateway() {
		return readerGateway;
	}

	public String getComunicacaoHidroSkywave() {
		return comunicacaoHidroSkywave;
	}

	public String getComunicacaoIntegBDAutomacao() {
		return comunicacaoIntegBDAutomacao;
	}

	public String getBdAutomacaoIdentificadorRemoto() {
		return bdAutomacaoIdentificadorRemoto;
	}

	public String getComunicacaoIonDialUp() {
		return comunicacaoIonDialUp;
	}

	public String getIonDialUpTelefone() {
		return ionDialUpTelefone;
	}

	public String getIonDialUpId() {
		return ionDialUpId;
	}

	public String getComunicacaoIonEtgTerm() {
		return comunicacaoIonEtgTerm;
	}

	public String getIonEtgTermEnderecoOuIp() {
		return ionEtgTermEnderecoOuIp;
	}

	public String getIonEtgTermPorta() {
		return ionEtgTermPorta;
	}

	public String getIonEtgTermId() {
		return ionEtgTermId;
	}

	public String getIonEtgTermColetarFormasOnda() {
		return ionEtgTermColetarFormasOnda;
	}

	public String getComunicacaoIonEth() {
		return comunicacaoIonEth;
	}

	public String getIonEthEnderecoOuIp() {
		return ionEthEnderecoOuIp;
	}

	public String getIonEthPorta() {
		return ionEthPorta;
	}

	public String getIonEthId() {
		return ionEthId;
	}

	public String getIonEthColetarFormasOnda() {
		return ionEthColetarFormasOnda;
	}

	public String getComunicacaoIonGprs() {
		return comunicacaoIonGprs;
	}

	public String getIonGprsEnderecoOuIp() {
		return ionGprsEnderecoOuIp;
	}

	public String getIonGprsPorta() {
		return ionGprsPorta;
	}

	public String getIonGprsId() {
		return ionGprsId;
	}

	public String getIonGprsColetarFormasOnda() {
		return ionGprsColetarFormasOnda;
	}

	public String getComunicacaoIonSer() {
		return comunicacaoIonSer;
	}

	public String getIonSerialEnderecoOuIp() {
		return ionSerialEnderecoOuIp;
	}

	public String getIonSerialId() {
		return ionSerialId;
	}

	public String getComunicacaoIonEthE3Rt() {
		return comunicacaoIonEthE3Rt;
	}

	public String getIonEthE3RtEnderecoOuIp() {
		return ionEthE3RtEnderecoOuIp;
	}

	public String getIonEthE3RtPorta() {
		return ionEthE3RtPorta;
	}

	public String getIonEthE3RtId() {
		return ionEthE3RtId;
	}

	public String getIonEthE3RtColetarFormasOnda() {
		return ionEthE3RtColetarFormasOnda;
	}

	public String getComunicacaoIon7650Eth() {
		return comunicacaoIon7650Eth;
	}

	public String getIon7650EthEnderecoOuIp() {
		return ion7650EthEnderecoOuIp;
	}

	public String getIon7650EthPorta() {
		return ion7650EthPorta;
	}

	public String getIon7650EthId() {
		return ion7650EthId;
	}

	public String getIon7650EthColetarFormasOnda() {
		return ion7650EthColetarFormasOnda;
	}

	public String getComunicacaoITechUtw() {
		return comunicacaoITechUtw;
	}

	public String getComunicacaoLandisE34Eth() {
		return comunicacaoLandisE34Eth;
	}

	public String getLandisE34MpEthEnderecoOuIp() {
		return landisE34MpEthEnderecoOuIp;
	}

	public String getLandisE34MpEthPorta() {
		return landisE34MpEthPorta;
	}

}