package br.com.upperselenium.test.stage.configuracoes.administracao.pastas;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.dpo.NavegarRegistroLinkPastasDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.page.AlteracaoPastasPage;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.page.PastasPage;

public class NavegarRegistroLinkPastasStage extends BaseStage {
	private NavegarRegistroLinkPastasDPO navegarRegistroLinkPastasDPO;
	private PastasPage pastasPage;
	private AlteracaoPastasPage alteracaoPastasPage;
	
	public NavegarRegistroLinkPastasStage(String dp) {
		navegarRegistroLinkPastasDPO = loadDataProviderFile(NavegarRegistroLinkPastasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		pastasPage = initElementsFromPage(PastasPage.class);
		alteracaoPastasPage = initElementsFromPage(AlteracaoPastasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarRegistroLinkPastasDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		pastasPage.clickGridFirstLineLinkNome(navegarRegistroLinkPastasDPO.getClickItem());
		alteracaoPastasPage.getBreadcrumbsText(navegarRegistroLinkPastasDPO.getBreadcrumbs());
		alteracaoPastasPage.clickUpToBreadcrumbs(navegarRegistroLinkPastasDPO.getUpToBreadcrumb());
		alteracaoPastasPage.getWelcomeTitle(navegarRegistroLinkPastasDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
