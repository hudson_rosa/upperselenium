package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.dpo.NavegarAlteracaoTiposDeMedidorDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.page.AlteracaoTiposDeMedidorPage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.page.TiposDeMedidorPage;

public class NavegarAlteracaoTiposDeMedidorStage extends BaseStage {
	private NavegarAlteracaoTiposDeMedidorDPO navegarAlteracaoTiposDeMedidorDPO;
	private TiposDeMedidorPage tiposDeMedidorPage;
	private AlteracaoTiposDeMedidorPage alteracaoTiposDeMedidorPage;
	
	public NavegarAlteracaoTiposDeMedidorStage(String dp) {
		navegarAlteracaoTiposDeMedidorDPO = loadDataProviderFile(NavegarAlteracaoTiposDeMedidorDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDeMedidorPage = initElementsFromPage(TiposDeMedidorPage.class);
		alteracaoTiposDeMedidorPage = initElementsFromPage(AlteracaoTiposDeMedidorPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAlteracaoTiposDeMedidorDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		tiposDeMedidorPage.clickGridButtonFirstLineEditar(navegarAlteracaoTiposDeMedidorDPO.getClickItem());
		alteracaoTiposDeMedidorPage.clickAbaAbaS();
		alteracaoTiposDeMedidorPage.getLabelHeaderAbas(navegarAlteracaoTiposDeMedidorDPO.getLabelAbas());
		alteracaoTiposDeMedidorPage.getLabelHeaderVisualizar(navegarAlteracaoTiposDeMedidorDPO.getLabelVisualizar());
		alteracaoTiposDeMedidorPage.getLabelButtonSalvar(navegarAlteracaoTiposDeMedidorDPO.getLabelButtonSalvar());
		alteracaoTiposDeMedidorPage.getBreadcrumbsText(navegarAlteracaoTiposDeMedidorDPO.getBreadcrumbs());
		alteracaoTiposDeMedidorPage.clickUpToBreadcrumbs(navegarAlteracaoTiposDeMedidorDPO.getUpToBreadcrumb());
		alteracaoTiposDeMedidorPage.getWelcomeTitle(navegarAlteracaoTiposDeMedidorDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
