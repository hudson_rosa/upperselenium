package br.com.upperselenium.test.stage.configuracoes.cadastros.alimentadores.page;

import org.openqa.selenium.By;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoAlimentadoresPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//form/fieldset/div[7]/div/input";
	private static final String SELECT_REGIAO = "//*[@id='IdRegiao']";
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String TEXT_PONTO_DE_MEDICAO = "//*[@id='Ponto']";
	private static final String TEXT_PSEUDONIMO_PERDAS_TECNICAS = "//*[@id='AliasSistemaDePerdasTecnicas']";
	private static final String TEXT_PSEUDONIMO_CONSUMO = "//*[@id='AliasSistemaDeConsumo']";
	private static final String TEXT_PSEUDONIMO_PONTO_DE_CONSUMO_ADICIONAL = "//*[@id='PontoDeConsumoAdicional']";

	public void typeTextNome(String value){
		typeText(getWebElement(TEXT_NOME), value);
	}

	public void typeTextPontoDeMedicao(String value){
		typeText(TEXT_PONTO_DE_MEDICAO, value);
	}
	
	public void typeTextPseudonimoPerdasTecnicas(String value){
		typeText(TEXT_PSEUDONIMO_PERDAS_TECNICAS, value);
	}
	
	public void typeTextPseudonimoConsumo(String value){
		typeText(TEXT_PSEUDONIMO_CONSUMO, value);
	}
	
	public void typeTextPontoDeConsumoAdicional(String value){
		typeText(TEXT_PSEUDONIMO_PONTO_DE_CONSUMO_ADICIONAL, value);
	}
	
	public void typeSelectRegiao(String value){		
		typeSelectComboOption(SELECT_REGIAO, value);
	}
	
	public boolean isClickableButtonSalvar() {
		return isDisplayedElement(By.xpath(BUTTON_SALVAR));
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
}