package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaMedidoresPontosPage extends BaseHelperPage{
	
	private static final String LINK_ABA_MEDIDORES = "//*[@id='tabs']/ul/li[3]/a";  
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 	
	private static final String TEXT_PRINCIPAL = "//*[@id='MedidorPrincipal']"; 
	private static final String TEXT_NUMERO_DE_SERIE_PRINCIPAL = "//*[@id='ponto-medidores']/form/fieldset/div[2]/div/span"; 
	private static final String TEXT_NUMERO_INTERNO_PRINCIPAL = "//*[@id='ponto-medidores']/form/fieldset/div[3]/div/span"; 
	private static final String TEXT_CODIGO_SCDE_PRINCIPAL = "//*[@id='ponto-medidores']/form/fieldset/div[4]/div/span"; 
	private static final String TEXT_RETAGUARDA = "//*[@id='MedidorRetaguarda']"; 
	private static final String TEXT_NUMERO_DE_SERIE_RETAGUARDA = "//*[@id='ponto-medidores']/form/fieldset/div[6]/div/span"; 
	private static final String TEXT_NUMERO_INTERNO_RETAGUARDA = "//*[@id='ponto-medidores']/form/fieldset/div[7]/div/span"; 
	private static final String TEXT_CODIGO_SCDE_RETAGUARDA = "//*[@id='ponto-medidores']/form/fieldset/div[8]/div/span"; 
	
	public void typeTextPrincipal(String value){
		typeTextAutoCompleteSelect(TEXT_PRINCIPAL, value);
	}
		
	public void getLabelNumeroDeSeriePrincipal(String value){
		getLabel(TEXT_NUMERO_DE_SERIE_PRINCIPAL, value);
	}
	
	public void getLabelNumeroInternoPrincipal(String value){
		getLabel(TEXT_NUMERO_INTERNO_PRINCIPAL, value);
	}
	
	public void getLabelCodigoScdePrincipal(String value){
		getLabel(TEXT_CODIGO_SCDE_PRINCIPAL, value);
	}
	
	public void typeTextRetaguarda(String value){
		typeTextAutoCompleteSelect(TEXT_RETAGUARDA, value);
	}
	
	public void getLabelNumeroDeSerieRetaguarda(String value){
		getLabel(TEXT_NUMERO_DE_SERIE_RETAGUARDA, value);
	}
	
	public void getLabelNumeroInternoRetaguarda(String value){
		getLabel(TEXT_NUMERO_INTERNO_RETAGUARDA, value);
	}
	
	public void getLabelCodigoScdeRetaguarda(String value){
		getLabel(TEXT_CODIGO_SCDE_RETAGUARDA, value);
	}

	public void keyPageDown(){
		useKey(TEXT_PRINCIPAL, Keys.PAGE_DOWN);
	}	
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
				
	public void clickAbaMedidores(){
		clickOnElement(LINK_ABA_MEDIDORES);
	}
	
}