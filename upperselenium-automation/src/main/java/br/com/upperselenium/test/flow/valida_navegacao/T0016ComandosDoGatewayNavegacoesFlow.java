package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.GoToComandosDoGatewayStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.NavegarComandosDoGatewayAgendarParametrosStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.NavegarComandosDoGatewayAgendarStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.NavegarComandosDoGatewayHistoricoStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.NavegarListagemComandosDoGatewayStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0016ComandosDoGatewayNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0016-ComandosDoGateway", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Comandos do Gateway validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0016ComandosDoGatewayNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToComandosDoGatewayStage());
		addStage(new NavegarListagemComandosDoGatewayStage(getDP("NavegarListagemComandosDoGatewayDP.json")));
		addStage(new GoToComandosDoGatewayStage());
		addStage(new NavegarComandosDoGatewayAgendarStage(getDP("NavegarComandosDoGatewayAgendarDP.json")));
		addStage(new GoToComandosDoGatewayStage());
		addStage(new NavegarComandosDoGatewayAgendarParametrosStage(getDP("NavegarComandosDoGatewayAgendarParametrosDP.json")));
		addStage(new GoToComandosDoGatewayStage());
		addStage(new NavegarComandosDoGatewayHistoricoStage(getDP("NavegarComandosDoGatewayHistoricoDP.json")));
	}	
}
