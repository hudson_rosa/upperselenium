package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarAbaDemandaContratadaPontosVirtuaisDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastroAbaDemandaContratadaPontosVirtuaisPage;

public class CadastrarAbaDemandaContratadaPontosVirtuaisStage extends BaseStage {
	private CadastrarAbaDemandaContratadaPontosVirtuaisDPO cadastrarAbaDemandaContratadaPontosVirtuaisDPO;
	private CadastroAbaDemandaContratadaPontosVirtuaisPage cadastroAbaDemandaContratadaPontosVirtuaisPage;
	
	public CadastrarAbaDemandaContratadaPontosVirtuaisStage(String dp) {
		cadastrarAbaDemandaContratadaPontosVirtuaisDPO = loadDataProviderFile(CadastrarAbaDemandaContratadaPontosVirtuaisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaDemandaContratadaPontosVirtuaisPage = initElementsFromPage(CadastroAbaDemandaContratadaPontosVirtuaisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		cadastroAbaDemandaContratadaPontosVirtuaisPage.clickAbaDemandaContratada();
		FindElementUtil.acceptAlert();
		
		for (int index=0; index < cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGroupNovaDemandaContratada().size(); index++){
			cadastroAbaDemandaContratadaPontosVirtuaisPage.typeTextInicio(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGroupNovaDemandaContratada().get(index).getInicio());
			cadastroAbaDemandaContratadaPontosVirtuaisPage.typeTextLimitePonta(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGroupNovaDemandaContratada().get(index).getLimitePonta());
			cadastroAbaDemandaContratadaPontosVirtuaisPage.typeTextValorPonta(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGroupNovaDemandaContratada().get(index).getValorPonta());
			cadastroAbaDemandaContratadaPontosVirtuaisPage.typeTextLimiteForaPonta(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGroupNovaDemandaContratada().get(index).getLimiteForaDePonta());
			cadastroAbaDemandaContratadaPontosVirtuaisPage.typeTextValorForaPonta(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGroupNovaDemandaContratada().get(index).getValorForaDePonta());
			cadastroAbaDemandaContratadaPontosVirtuaisPage.keyPageDown();
			cadastroAbaDemandaContratadaPontosVirtuaisPage.typeSelectTipo(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGroupNovaDemandaContratada().get(index).getTipo());
			cadastroAbaDemandaContratadaPontosVirtuaisPage.clickAdicionar();
			cadastroAbaDemandaContratadaPontosVirtuaisPage.keyPageUp();
		}
		for (int index=0; index < cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().size(); index++){
			if(StringUtil.isNotBlankOrNotNull(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getFlagItem())){
				cadastroAbaDemandaContratadaPontosVirtuaisPage.keyPageDown();
				cadastroAbaDemandaContratadaPontosVirtuaisPage.typeTextFilterInicioDaVigencia(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getFilterInicioDaVigencia());
				cadastroAbaDemandaContratadaPontosVirtuaisPage.typeTextFilterLimitePonta(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getFilterLimitePonta());
				cadastroAbaDemandaContratadaPontosVirtuaisPage.typeTextFilterLimiteForaPonta(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getFilterLimiteForaPonta());
				cadastroAbaDemandaContratadaPontosVirtuaisPage.typeTextFilterValorPonta(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getFilterValorPonta());
				cadastroAbaDemandaContratadaPontosVirtuaisPage.typeTextFilterValorForaPonta(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getFilterValorForaPonta());
				cadastroAbaDemandaContratadaPontosVirtuaisPage.typeTextFilterTipo(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getFilterTipo());
				cadastroAbaDemandaContratadaPontosVirtuaisPage.getGridLabelInicioDaVigencia(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getInicioDaVigencia(), 1);
				cadastroAbaDemandaContratadaPontosVirtuaisPage.getGridLabelLimitePonta(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getLimitePonta(), 1);
				cadastroAbaDemandaContratadaPontosVirtuaisPage.getGridLabelLimiteForaPonta(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getLimiteForaPonta(), 1);
				cadastroAbaDemandaContratadaPontosVirtuaisPage.getGridLabelValorPonta(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getValorPonta(), 1);
				cadastroAbaDemandaContratadaPontosVirtuaisPage.getGridLabelValorForaPonta(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getValorForaPonta(), 1);
				cadastroAbaDemandaContratadaPontosVirtuaisPage.getGridLabelTipo(cadastrarAbaDemandaContratadaPontosVirtuaisDPO.getGridDemandasContratadas().get(index).getTipo(), 1);
			}
		}
		cadastroAbaDemandaContratadaPontosVirtuaisPage.keyPageUp();
		cadastroAbaDemandaContratadaPontosVirtuaisPage.clickNova();
	}

	@Override
	public void runValidations() {}

}
