package br.com.upperselenium.test.flow.valida_rest_api;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.AgendarComandosDoMedidorStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.GoToComandosDoMedidorStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.RestApiValidateGETPedidoDeMedidoresStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0001PedidoDeMedidoresRequestApiFlow.class)
@FlowParams(idTest = "PIM-REST_API-T0001-Comandos", testDirPath = "", loginDirPath = "",
	goal = "Valida a API de Pedido de Medidores da PIM, a partir do verbo GET",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0001PedidoDeMedidoresRequestApiFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToComandosDoMedidorStage());
		addStage(new AgendarComandosDoMedidorStage(getDP("AgendarComandosDoMedidorDP.json")));
		addStage(new RestApiValidateGETPedidoDeMedidoresStage(getDP("RestApiValidateGETPedidoDeMedidoresDP.json")));
	}	
}
