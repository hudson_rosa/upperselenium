package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarAbaAnemometriaPontosStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarAbaCCEEPontosStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarAbaConfiguracoesPontosStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarAbaDemandaContratadaPontosStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarAbaHidrologiaPontosStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarAbaMedidoresPontosStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarPontosStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.GoToPontosStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0016PontosCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0016-Pontos", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Pontos realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0016PontosCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToPontosStage());
		addStage(new CadastrarPontosStage(getDP("CadastrarPontosDP.json")));
		addStage(new CadastrarAbaConfiguracoesPontosStage(getDP("CadastrarAbaConfiguracoesPontosDP.json")));
		addStage(new CadastrarAbaMedidoresPontosStage(getDP("CadastrarAbaMedidoresPontosDP.json")));
		addStage(new CadastrarAbaCCEEPontosStage(getDP("CadastrarAbaCCEEPontosDP.json")));
		addStage(new CadastrarAbaDemandaContratadaPontosStage(getDP("CadastrarAbaDemandaContratadaPontosDP.json")));
		addStage(new CadastrarAbaHidrologiaPontosStage(getDP("CadastrarAbaHidrologiaPontosDP.json")));		
		addStage(new CadastrarAbaAnemometriaPontosStage(getDP("CadastrarAbaAnemometriaPontosDP.json")));
	}	
}
