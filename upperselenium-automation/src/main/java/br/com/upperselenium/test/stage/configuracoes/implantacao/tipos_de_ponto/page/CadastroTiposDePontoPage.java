package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroTiposDePontoPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/form/fieldset/div[5]/input";   
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String TEXT_DESCRICAO = "//*[@id='Descricao']";
	private static final String SELECT_GRUPO_DE_MEDICAO = "//*[@id='IdListaGrupoMedicao']";
	private static final String CHECK_MONITORA_DADOS_DUPLICADOS = "//*[@id='MonitoraDuplicados']";
	
	public void typeTextNomeDefault(String value){
		typeText(TEXT_NOME, value);
	}	
	
	public void typeTextNomeRandom(String value){
		typeText(TEXT_NOME, value);
	}	

	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}	

	public void typeSelectGrupoDeMedicao(String value){
		typeSelectComboOption(SELECT_GRUPO_DE_MEDICAO, value);
	}
	
	public void typeCheckMonitoraDadosDuplicados(String value){
		typeCheckOptionByLabel(CHECK_MONITORA_DADOS_DUPLICADOS, value);
	}
		
	public void keyPageDown(){
		useKey(TEXT_NOME, Keys.PAGE_DOWN);
	}	

	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
	public void getLabelButtonSalvar(String value){
		getLabel(BUTTON_SALVAR, value);
	}
	
}