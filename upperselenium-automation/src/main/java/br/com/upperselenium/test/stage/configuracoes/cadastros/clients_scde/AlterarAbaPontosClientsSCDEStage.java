package br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.dpo.AlterarAbaPontosClientsSCDEDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.page.AlteracaoAbaPontosClientsSCDEPage;

public class AlterarAbaPontosClientsSCDEStage extends BaseStage {
	private AlterarAbaPontosClientsSCDEDPO alterarAbaPontosClientsSCDEDPO;
	private AlteracaoAbaPontosClientsSCDEPage alteracaoAbaPontosClientsSCDEPage;
	
	public AlterarAbaPontosClientsSCDEStage(String dp) {
		alterarAbaPontosClientsSCDEDPO = loadDataProviderFile(AlterarAbaPontosClientsSCDEDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alteracaoAbaPontosClientsSCDEPage = initElementsFromPage(AlteracaoAbaPontosClientsSCDEPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarAbaPontosClientsSCDEDPO.getIssue());
		goToAbaPontos();
		execute();		
	}
	
	private void goToAbaPontos() {
		waitForPageToLoadUntil10s();		
		alteracaoAbaPontosClientsSCDEPage.clickAbaPontos();
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();		
		alteracaoAbaPontosClientsSCDEPage.typeTextFilterNome(alterarAbaPontosClientsSCDEDPO.getFilterNome());
		alteracaoAbaPontosClientsSCDEPage.typeTextFilterPasta(alterarAbaPontosClientsSCDEDPO.getFilterPasta());
		alteracaoAbaPontosClientsSCDEPage.typeTextFilterTipoDePonto(alterarAbaPontosClientsSCDEDPO.getFilterTipoDePonto());
		alteracaoAbaPontosClientsSCDEPage.typeCheckBoxGridItemPonto(alterarAbaPontosClientsSCDEDPO.getFilterNome());
		FindElementUtil.acceptAlert();
		alteracaoAbaPontosClientsSCDEPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoAbaPontosClientsSCDEPage.validateMessageDefault(alterarAbaPontosClientsSCDEDPO.getOperationMessage());
	}

}
