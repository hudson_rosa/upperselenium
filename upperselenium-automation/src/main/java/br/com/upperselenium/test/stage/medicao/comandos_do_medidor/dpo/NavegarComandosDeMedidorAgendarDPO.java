package br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarComandosDeMedidorAgendarDPO extends BaseHelperDPO {

	private String labelComandos;
	private String labelInstante;
	private String labelExpiracao;
	private String labelDigiteUmOuMaisMedidores;
	private String labelRepeticao;

	public String getLabelComandos() {
		return labelComandos;
	}

	public void setLabelComandos(String labelComandos) {
		this.labelComandos = labelComandos;
	}

	public String getLabelInstante() {
		return labelInstante;
	}

	public void setLabelInstante(String labelInstante) {
		this.labelInstante = labelInstante;
	}

	public String getLabelExpiracao() {
		return labelExpiracao;
	}

	public void setLabelExpiracao(String labelExpiracao) {
		this.labelExpiracao = labelExpiracao;
	}

	public String getLabelDigiteUmOuMaisMedidores() {
		return labelDigiteUmOuMaisMedidores;
	}

	public void setLabelDigiteUmOuMaisMedidores(String labelDigiteUmOuMaisMedidores) {
		this.labelDigiteUmOuMaisMedidores = labelDigiteUmOuMaisMedidores;
	}

	public String getLabelRepeticao() {
		return labelRepeticao;
	}

	public void setLabelRepeticao(String labelRepeticao) {
		this.labelRepeticao = labelRepeticao;
	}

	@Override
	public String toString() {
		return "NavegarComandosDeMedidorAgendarDPO [labelComandos=" + labelComandos + ", labelInstante=" + labelInstante
				+ ", labelExpiracao=" + labelExpiracao + ", labelDigiteUmOuMaisMedidores="
				+ labelDigiteUmOuMaisMedidores + ", labelRepeticao=" + labelRepeticao + "]";
	}

}