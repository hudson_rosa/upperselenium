package br.com.upperselenium.test.stage.medicao.comandos_do_medidor.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class ComandosDoMedidorAgendarParametrosPage extends BaseHelperPage{
	
	private static final String LABEL_TIPO = "//*[@id='tree-content']/div[6]/form/fieldset/div[1]/label";
	private static final String LABEL_INSTANTE = "//*[@id='tree-content']/div[6]/form/fieldset/div[2]/label";
	private static final String LABEL_EXPIRACAO = "//*[@id='tree-content']/div[6]/form/fieldset/div[3]/label";
	private static final String LABEL_MEDIDORES_POR_PARAMETROS = "//*[@id='filtros']/b/label";
	private static final String LABEL_PASTA = "//*[@id='filtros']/div[2]/b/label";
	private static final String LABEL_TAGS = "//*[@id='filtros']/div[3]/div[1]/b/label";
	private static final String LABEL_ETAPA = "//*[@id='filtros']/div[3]/div[2]/b/label";
	private static final String LABEL_TIPO_DE_COMUNICACAO = "//*[@id='filtros']/div[3]/div[3]/b/label";
	private static final String LABEL_TIPO_DE_PONTO = "//*[@id='filtros']/div[3]/div[4]/b/label";
	private static final String LABEL_REPETICAO = "//*[@id='controlesRecorrencia']/legend";
	
	public void getLabelTipo(String value){
		getLabel(LABEL_TIPO, value);
	}
	
	public void getLabelInstante(String value){
		getLabel(LABEL_INSTANTE, value);
	}
	
	public void getLabelExpiracao(String value){
		getLabel(LABEL_EXPIRACAO, value);
	}
	
	public void getLabelMedidoresPorParametros(String value){
		getLabel(LABEL_MEDIDORES_POR_PARAMETROS, value);
	}
	
	public void getLabelPasta(String value){
		getLabel(LABEL_PASTA, value);
	}
	
	public void getLabelTags(String value){
		getLabel(LABEL_TAGS, value);
	}
	
	public void getLabelEtapa(String value){
		getLabel(LABEL_ETAPA, value);
	}
	
	public void getLabelTipoDeComunicacao(String value){
		getLabel(LABEL_TIPO_DE_COMUNICACAO, value);
	}
	
	public void getLabelTipoDePonto(String value){
		getLabel(LABEL_TIPO_DE_PONTO, value);
	}	
	
	public void getLabelRepeticao(String value){
		getLabel(LABEL_REPETICAO, value);
	}

}