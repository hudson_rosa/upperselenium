package br.com.upperselenium.test.stage.wrapper;

/**
 * Atributos comuns aplicados nos cenários de teste.
 * 
 * @param operationMessage
 * @param issue
 */
public class BaseHelperDPO {

	private String breadcrumbs;
	private String upToBreadcrumb;
	private String welcomeTitle;
	private String operationMessage;
	private String issue;

	public String getBreadcrumbs() {
		return breadcrumbs;
	}

	public void setBreadcrumbs(String breadcrumbs) {
		this.breadcrumbs = breadcrumbs;
	}

	public String getUpToBreadcrumb() {
		return upToBreadcrumb;
	}

	public void setUpToBreadcrumb(String upToBreadcrumb) {
		this.upToBreadcrumb = upToBreadcrumb;
	}

	public String getWelcomeTitle() {
		return welcomeTitle;
	}

	public void setWelcomeTitle(String welcomeTitle) {
		this.welcomeTitle = welcomeTitle;
	}

	public String getOperationMessage() {
		return operationMessage;
	}

	public void setOperationMessage(String operationMessage) {
		this.operationMessage = operationMessage;
	}

	public String getIssue() {
		return issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	@Override
	public String toString() {
		return "BaseHelperDPO [breadcrumbs=" + breadcrumbs + ", upToBreadcrumb=" + upToBreadcrumb + ", welcomeTitle="
				+ welcomeTitle + ", operationMessage=" + operationMessage + ", issue=" + issue + "]";
	}

}
