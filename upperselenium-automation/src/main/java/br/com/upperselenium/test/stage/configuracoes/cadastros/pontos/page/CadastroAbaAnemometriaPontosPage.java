package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaAnemometriaPontosPage extends BaseHelperPage{
	
	private static final String LINK_ABA_ANEMOMETRIA = "//*[@id='tabs']/ul/li[7]/a";
	private static final String BUTTON_SALVAR_ESTACAO = "//*[@id='painel-anemometria']/aba-anemometria/section/editor-codigo-estacao/form/div[2]/button";
	private static final String TEXT_CODIGO_DA_ESTACAO = "//*[@id='input-codigo-estacao']"; 
	
	private static final String BUTTON_SALVAR_INSTRUMENTO = "//*[@id='painel-anemometria']/aba-anemometria/section/configuracao-instrumentos/div/editor-altura/form/div[2]/button"; 	
	private static final String SELECT_INSTRUMENTO = "//*[@id='painel-anemometria']/aba-anemometria/section/configuracao-instrumentos/div/div/div/select"; 	
	private static final String TEXT_ALTURA = "//*[@id='input-altura']"; 

	private static final String BUTTON_ADICIONAR_VIGENCIA = "//*[@id='painel-anemometria']/aba-anemometria/section/configuracao-instrumentos/div/configuracao-offset-slope/div/div[1]/button"; 	
	private static final String MODAL_TEXT_INICIO_DA_VIGENCIA = "//*[@id='modal-offset-slope']/form/div[2]/div[1]/div/date-picker/div/div/input"; 
	private static final String MODAL_TEXT_SLOPE = "//*[@id='input-slope']"; 
	private static final String MODAL_TEXT_OFFSET = "//*[@id='input-offset']";
	private static final String MODAL_BUTTON_ADICIONAR_VIGENCIA = "//*[@id='modal-offset-slope']/form/div[@class='modal-footer']/button"; 	
	
	private static final String GRID_FILTER_INICIO_DA_VIGENCIA = "//*[@id='configuracoes-instrumento']/thead/tr[1]/th[1]/span/input";
	private static final String GRID_FILTER_OFFSET = "//*[@id='configuracoes-instrumento']/thead/tr[1]/th[2]/span/input";
	private static final String GRID_FILTER_SLOPE = "//*[@id='configuracoes-instrumento']/thead/tr[1]/th[3]/span/input";
	
	private static final String GRID_TEXT_INICIO_DA_VIGENCIA = "//*[@id='configuracoes-instrumento']/tbody/tr[%i%]/td[1]/span";
	private static final String GRID_TEXT_OFFSET = "//*[@id='configuracoes-instrumento']/tbody/tr[%i%]/td[2]/span";
	private static final String GRID_TEXT_SLOPE = "//*[@id='configuracoes-instrumento']/tbody/tr[%i%]/td[3]/span";
	
	public void clickSalvarCodigoDaEstacao(){
		clickOnElement(BUTTON_SALVAR_ESTACAO);
	}	
	
	public void clickSalvarInstrumento(){
		clickOnElement(BUTTON_SALVAR_INSTRUMENTO);
	}	
	
	public void clickAdicionarVigencia(){
		clickOnElement(BUTTON_ADICIONAR_VIGENCIA);
	}
	
	public void clickModalAdicionarVigencia(){
		clickActionOnElement(MODAL_BUTTON_ADICIONAR_VIGENCIA);
	}
	
	public void typeSelectInstrumento(String value){		
		typeSelectComboOption(SELECT_INSTRUMENTO, value);
	}
	
	public void typeTextCodigoDaEstacao(String value){
		typeText(TEXT_CODIGO_DA_ESTACAO, value);
	}

	public void typeTextAltura(String value){
		typeText(TEXT_ALTURA, value);
	}
	
	public void typeModalTextInicioDaVigencia(String value){
		waitForPageToLoad();
		waitForPresenceOfElementLocated(MODAL_TEXT_INICIO_DA_VIGENCIA);
		typeDatePickerDefault(MODAL_TEXT_INICIO_DA_VIGENCIA, value);
	}	
	
	public void typeModalTextSlope(String value){
		waitForATime(TimePRM._3_SECS);
		waitForPageToLoadUntil10s();
		typeText(MODAL_TEXT_SLOPE, value);
	}	
	
	public void typeModalTextOffset(String value){
		waitForPageToLoadUntil10s();
		typeText(MODAL_TEXT_OFFSET, value);
	}	
	
	public void typeTextFilterInicioDaVigencia(String value){
		typeText(GRID_FILTER_INICIO_DA_VIGENCIA, value);
	}	
	
	public void typeTextFilterOffset(String value){
		typeText(GRID_FILTER_OFFSET, value);
	}	

	public void typeTextFilterSlope(String value){
		typeText(GRID_FILTER_SLOPE, value);
	}	
	
	public void getGridLabelInicioDaVigencia(String value, int index){		
		waitForATime(TimePRM._2_SECS);
		getGridLabel(GRID_TEXT_INICIO_DA_VIGENCIA, value, index);
	}	
	
	public void getGridLabelOffset(String value, int index){		
		getGridLabel(GRID_TEXT_OFFSET, value, index);
	}
	
	public void getGridLabelSlope(String value, int index){		
		getGridLabel(GRID_TEXT_SLOPE, value, index);
	}
	
	public void keyPageUp(){
		useKey(BUTTON_ADICIONAR_VIGENCIA, Keys.PAGE_UP);
	}	
	
	public void keyPageDown(){
		waitForPageToLoadUntil10s();
		useKey(TEXT_ALTURA, Keys.PAGE_DOWN);
	}	
				
	public void clickAbaAnemometria(){
		clickOnElement(LINK_ABA_ANEMOMETRIA);
	}
	
}