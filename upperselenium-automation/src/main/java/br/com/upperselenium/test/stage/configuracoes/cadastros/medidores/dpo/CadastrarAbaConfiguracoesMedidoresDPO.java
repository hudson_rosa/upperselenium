package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaConfiguracoesMedidoresDPO extends BaseHelperDPO {

	private String informarNumeradorEDenominadorRtpRtc;
	private String numeradorRTP;
	private String numeradorRTC;
	private String denominadorRTP;
	private String denominadorRTC;
	private String relacaoRTP;
	private String relacaoRTC;
	private String classe;
	private String classeDeExatidao;
	private String quantidadeDeElementos;
	private String quantidadeDeFios;
	private String constanteDeIntegracao;
	private String constanteDeEnergiaAtiva;
	private String constanteDeEnergiaReativa;
	private String correnteMaxima;
	private String correnteNominal;
	private String tensaoNominal;
	private String tensaoMaxima;
	private String frequencia;
	private String frequenciaMaxima;
	private String constantePRN;
	private String codigoEletrobras;
	private String constanteDeCombustivel;
	private String calculoRelacaoRTP;
	private String calculoRelacaoRTC;
	private String numeroDoEquipamentoSAP;
	private String tipoDeLigacao;

	public String getInformarNumeradorEDenominadorRtpRtc() {
		return informarNumeradorEDenominadorRtpRtc;
	}

	public void setInformarNumeradorEDenominadorRtpRtc(String informarNumeradorEDenominadorRtpRtc) {
		this.informarNumeradorEDenominadorRtpRtc = informarNumeradorEDenominadorRtpRtc;
	}

	public String getNumeradorRTP() {
		return numeradorRTP;
	}

	public void setNumeradorRTP(String numeradorRTP) {
		this.numeradorRTP = numeradorRTP;
	}

	public String getNumeradorRTC() {
		return numeradorRTC;
	}

	public void setNumeradorRTC(String numeradorRTC) {
		this.numeradorRTC = numeradorRTC;
	}

	public String getDenominadorRTP() {
		return denominadorRTP;
	}

	public void setDenominadorRTP(String denominadorRTP) {
		this.denominadorRTP = denominadorRTP;
	}

	public String getDenominadorRTC() {
		return denominadorRTC;
	}

	public void setDenominadorRTC(String denominadorRTC) {
		this.denominadorRTC = denominadorRTC;
	}

	public String getRelacaoRTP() {
		return relacaoRTP;
	}

	public void setRelacaoRTP(String relacaoRTP) {
		this.relacaoRTP = relacaoRTP;
	}

	public String getRelacaoRTC() {
		return relacaoRTC;
	}

	public void setRelacaoRTC(String relacaoRTC) {
		this.relacaoRTC = relacaoRTC;
	}

	public String getClasse() {
		return classe;
	}

	public void setClasse(String classe) {
		this.classe = classe;
	}

	public String getClasseDeExatidao() {
		return classeDeExatidao;
	}

	public void setClasseDeExatidao(String classeDeExatidao) {
		this.classeDeExatidao = classeDeExatidao;
	}

	public String getQuantidadeDeElementos() {
		return quantidadeDeElementos;
	}

	public void setQuantidadeDeElementos(String quantidadeDeElementos) {
		this.quantidadeDeElementos = quantidadeDeElementos;
	}

	public String getQuantidadeDeFios() {
		return quantidadeDeFios;
	}

	public void setQuantidadeDeFios(String quantidadeDeFios) {
		this.quantidadeDeFios = quantidadeDeFios;
	}

	public String getConstanteDeIntegracao() {
		return constanteDeIntegracao;
	}

	public void setConstanteDeIntegracao(String constanteDeIntegracao) {
		this.constanteDeIntegracao = constanteDeIntegracao;
	}

	public String getConstanteDeEnergiaAtiva() {
		return constanteDeEnergiaAtiva;
	}

	public void setConstanteDeEnergiaAtiva(String constanteDeEnergiaAtiva) {
		this.constanteDeEnergiaAtiva = constanteDeEnergiaAtiva;
	}

	public String getConstanteDeEnergiaReativa() {
		return constanteDeEnergiaReativa;
	}

	public void setConstanteDeEnergiaReativa(String constanteDeEnergiaReativa) {
		this.constanteDeEnergiaReativa = constanteDeEnergiaReativa;
	}

	public String getCorrenteMaxima() {
		return correnteMaxima;
	}

	public void setCorrenteMaxima(String correnteMaxima) {
		this.correnteMaxima = correnteMaxima;
	}

	public String getCorrenteNominal() {
		return correnteNominal;
	}

	public void setCorrenteNominal(String correnteNominal) {
		this.correnteNominal = correnteNominal;
	}

	public String getTensaoNominal() {
		return tensaoNominal;
	}

	public void setTensaoNominal(String tensaoNominal) {
		this.tensaoNominal = tensaoNominal;
	}

	public String getTensaoMaxima() {
		return tensaoMaxima;
	}

	public void setTensaoMaxima(String tensaoMaxima) {
		this.tensaoMaxima = tensaoMaxima;
	}

	public String getFrequencia() {
		return frequencia;
	}

	public void setFrequencia(String frequencia) {
		this.frequencia = frequencia;
	}

	public String getFrequenciaMaxima() {
		return frequenciaMaxima;
	}

	public void setFrequenciaMaxima(String frequenciaMaxima) {
		this.frequenciaMaxima = frequenciaMaxima;
	}

	public String getConstantePRN() {
		return constantePRN;
	}

	public void setConstantePRN(String constantePRN) {
		this.constantePRN = constantePRN;
	}

	public String getCodigoEletrobras() {
		return codigoEletrobras;
	}

	public void setCodigoEletrobras(String codigoEletrobras) {
		this.codigoEletrobras = codigoEletrobras;
	}

	public String getConstanteDeCombustivel() {
		return constanteDeCombustivel;
	}

	public void setConstanteDeCombustivel(String constanteDeCombustivel) {
		this.constanteDeCombustivel = constanteDeCombustivel;
	}

	public String getCalculoRelacaoRTP() {
		return calculoRelacaoRTP;
	}

	public void setCalculoRelacaoRTP(String calculoRelacaoRTP) {
		this.calculoRelacaoRTP = calculoRelacaoRTP;
	}

	public String getCalculoRelacaoRTC() {
		return calculoRelacaoRTC;
	}

	public void setCalculoRelacaoRTC(String calculoRelacaoRTC) {
		this.calculoRelacaoRTC = calculoRelacaoRTC;
	}

	public String getNumeroDoEquipamentoSAP() {
		return numeroDoEquipamentoSAP;
	}

	public void setNumeroDoEquipamentoSAP(String numeroDoEquipamentoSAP) {
		this.numeroDoEquipamentoSAP = numeroDoEquipamentoSAP;
	}

	public String getTipoDeLigacao() {
		return tipoDeLigacao;
	}

	public void setTipoDeLigacao(String tipoDeLigacao) {
		this.tipoDeLigacao = tipoDeLigacao;
	}

	@Override
	public String toString() {
		return "CadastrarAbaConfiguracoesMedidoresDPO [informarNumeradorEDenominadorRtpRtc="
				+ informarNumeradorEDenominadorRtpRtc + ", numeradorRTP=" + numeradorRTP + ", numeradorRTC="
				+ numeradorRTC + ", denominadorRTP=" + denominadorRTP + ", denominadorRTC=" + denominadorRTC
				+ ", relacaoRTP=" + relacaoRTP + ", relacaoRTC=" + relacaoRTC + ", classe=" + classe
				+ ", classeDeExatidao=" + classeDeExatidao + ", quantidadeDeElementos=" + quantidadeDeElementos
				+ ", quantidadeDeFios=" + quantidadeDeFios + ", constanteDeIntegracao=" + constanteDeIntegracao
				+ ", constanteDeEnergiaAtiva=" + constanteDeEnergiaAtiva + ", constanteDeEnergiaReativa="
				+ constanteDeEnergiaReativa + ", correnteMaxima=" + correnteMaxima + ", correnteNominal="
				+ correnteNominal + ", tensaoNominal=" + tensaoNominal + ", tensaoMaxima=" + tensaoMaxima
				+ ", frequencia=" + frequencia + ", frequenciaMaxima=" + frequenciaMaxima + ", constantePRN="
				+ constantePRN + ", codigoEletrobras=" + codigoEletrobras + ", constanteDeCombustivel="
				+ constanteDeCombustivel + ", calculoRelacaoRTP=" + calculoRelacaoRTP + ", calculoRelacaoRTC="
				+ calculoRelacaoRTC + ", numeroDoEquipamentoSAP=" + numeroDoEquipamentoSAP + ", tipoDeLigacao="
				+ tipoDeLigacao + "]";
	}

}