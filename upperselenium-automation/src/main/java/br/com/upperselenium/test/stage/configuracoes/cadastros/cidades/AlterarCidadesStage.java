package br.com.upperselenium.test.stage.configuracoes.cadastros.cidades;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.configuracoes.cadastros.cidades.dpo.AlterarCidadesDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.cidades.page.AlteracaoCidadesPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.cidades.page.CidadesPage;

public class AlterarCidadesStage extends BaseStage {
	private AlterarCidadesDPO alterarCidadesDPO;
	private CidadesPage cidadesPage;
	private AlteracaoCidadesPage alteracaoCidadesPage;
	
	public AlterarCidadesStage(String dp) {
		alterarCidadesDPO = loadDataProviderFile(AlterarCidadesDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cidadesPage = initElementsFromPage(CidadesPage.class);
		alteracaoCidadesPage = initElementsFromPage(AlteracaoCidadesPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarCidadesDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();		
		alteracaoCidadesPage.typeTextNome(alterarCidadesDPO.getNome());
		alteracaoCidadesPage.typeSelectEstado(alterarCidadesDPO.getEstado());
		alteracaoCidadesPage.typeSelectRegiao(alterarCidadesDPO.getRegiao());
		alteracaoCidadesPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoCidadesPage.validateMessageDefault(alterarCidadesDPO.getOperationMessage());
	}

}
