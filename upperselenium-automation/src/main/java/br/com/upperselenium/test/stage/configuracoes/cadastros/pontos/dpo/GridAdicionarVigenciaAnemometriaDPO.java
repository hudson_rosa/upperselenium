package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

public class GridAdicionarVigenciaAnemometriaDPO {

	private String flagItem;
	private String modalInicioDaVigencia;
	private String modalSlope;
	private String modalOffset;
	private String filterInicioDaVigencia;
	private String filterSlope;
	private String filterOffset;
	private String gridInicioDaVigencia;
	private String gridOffset;
	private String gridSlope;

	public String getFlagItem() {
		return flagItem;
	}

	public void setFlagItem(String flagItem) {
		this.flagItem = flagItem;
	}

	public String getModalInicioDaVigencia() {
		return modalInicioDaVigencia;
	}

	public void setModalInicioDaVigencia(String modalInicioDaVigencia) {
		this.modalInicioDaVigencia = modalInicioDaVigencia;
	}

	public String getModalSlope() {
		return modalSlope;
	}

	public void setModalSlope(String modalSlope) {
		this.modalSlope = modalSlope;
	}

	public String getModalOffset() {
		return modalOffset;
	}

	public void setModalOffset(String modalOffset) {
		this.modalOffset = modalOffset;
	}

	public String getFilterInicioDaVigencia() {
		return filterInicioDaVigencia;
	}

	public void setFilterInicioDaVigencia(String filterInicioDaVigencia) {
		this.filterInicioDaVigencia = filterInicioDaVigencia;
	}

	public String getFilterSlope() {
		return filterSlope;
	}

	public void setFilterSlope(String filterSlope) {
		this.filterSlope = filterSlope;
	}

	public String getFilterOffset() {
		return filterOffset;
	}

	public void setFilterOffset(String filterOffset) {
		this.filterOffset = filterOffset;
	}

	public String getGridInicioDaVigencia() {
		return gridInicioDaVigencia;
	}

	public void setGridInicioDaVigencia(String gridInicioDaVigencia) {
		this.gridInicioDaVigencia = gridInicioDaVigencia;
	}

	public String getGridOffset() {
		return gridOffset;
	}

	public void setGridOffset(String gridOffset) {
		this.gridOffset = gridOffset;
	}

	public String getGridSlope() {
		return gridSlope;
	}

	public void setGridSlope(String gridSlope) {
		this.gridSlope = gridSlope;
	}
}
