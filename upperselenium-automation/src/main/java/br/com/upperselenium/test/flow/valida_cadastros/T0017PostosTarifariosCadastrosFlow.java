package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.postos_tarifarios.CadastrarPostosTarifariosStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.postos_tarifarios.GoToPostosTarifariosStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0017PostosTarifariosCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0017-PostosTarifarios", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Postos Tarifários realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0017PostosTarifariosCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToPostosTarifariosStage());
		addStage(new CadastrarPostosTarifariosStage(getDP("CadastrarPostosTarifariosDP.json")));
	}	
}
