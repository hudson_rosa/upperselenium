package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.dpo.NavegarCadastroTiposDeMedidorDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.page.CadastroTiposDeMedidorPage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.page.TiposDeMedidorPage;

public class NavegarCadastroTiposDeMedidorStage extends BaseStage {
	private NavegarCadastroTiposDeMedidorDPO navegarCadastroTiposDeMedidorDPO;
	private TiposDeMedidorPage tiposDeMedidorPage;
	private CadastroTiposDeMedidorPage cadastroTiposDeMedidorPage;
	
	public NavegarCadastroTiposDeMedidorStage(String dp) {
		navegarCadastroTiposDeMedidorDPO = loadDataProviderFile(NavegarCadastroTiposDeMedidorDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDeMedidorPage = initElementsFromPage(TiposDeMedidorPage.class);
		cadastroTiposDeMedidorPage = initElementsFromPage(CadastroTiposDeMedidorPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarCadastroTiposDeMedidorDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		tiposDeMedidorPage.clickNovo();
		cadastroTiposDeMedidorPage.getBreadcrumbsText(navegarCadastroTiposDeMedidorDPO.getBreadcrumbs());
		cadastroTiposDeMedidorPage.clickUpToBreadcrumbs(navegarCadastroTiposDeMedidorDPO.getUpToBreadcrumb());
		cadastroTiposDeMedidorPage.getWelcomeTitle(navegarCadastroTiposDeMedidorDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
