package br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarUsuariosDPO extends BaseHelperDPO {

	private String filterNome;
	private String filterLogin;
	private String filterPerfil;
	private String filterStatus;
	private List<GridAlterarUsuariosDPO> gridUsuarios;
	private String nome;
	private String login;
	private String senha;
	private String confirmacao;
	private String email;
	private String telefone;
	private String perfil;
	private String pasta;
	private String bloqueado;
	private String generateToken;

	public String getFilterNome() {
		return filterNome;
	}

	public void setFilterNome(String filterNome) {
		this.filterNome = filterNome;
	}

	public String getFilterLogin() {
		return filterLogin;
	}

	public void setFilterLogin(String filterLogin) {
		this.filterLogin = filterLogin;
	}

	public String getFilterPerfil() {
		return filterPerfil;
	}

	public void setFilterPerfil(String filterPerfil) {
		this.filterPerfil = filterPerfil;
	}

	public String getFilterStatus() {
		return filterStatus;
	}

	public void setFilterStatus(String filterStatus) {
		this.filterStatus = filterStatus;
	}

	public List<GridAlterarUsuariosDPO> getGridUsuarios() {
		return gridUsuarios;
	}

	public void setGridUsuarios(List<GridAlterarUsuariosDPO> gridUsuarios) {
		this.gridUsuarios = gridUsuarios;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getConfirmacao() {
		return confirmacao;
	}

	public void setConfirmacao(String confirmacao) {
		this.confirmacao = confirmacao;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getPasta() {
		return pasta;
	}

	public void setPasta(String pasta) {
		this.pasta = pasta;
	}

	public String getBloqueado() {
		return bloqueado;
	}

	public void setBloqueado(String bloqueado) {
		this.bloqueado = bloqueado;
	}

	public String getGenerateToken() {
		return generateToken;
	}

	public void setGenerateToken(String generateToken) {
		this.generateToken = generateToken;
	}

	@Override
	public String toString() {
		return "AlterarUsuariosDPO [filterNome=" + filterNome + ", filterLogin=" + filterLogin + ", filterPerfil="
				+ filterPerfil + ", filterStatus=" + filterStatus + ", gridUsuarios=" + gridUsuarios + ", nome=" + nome
				+ ", login=" + login + ", senha=" + senha + ", confirmacao=" + confirmacao + ", email=" + email
				+ ", telefone=" + telefone + ", perfil=" + perfil + ", pasta=" + pasta + ", bloqueado=" + bloqueado
				+ ", generateToken=" + generateToken + "]";
	}

}