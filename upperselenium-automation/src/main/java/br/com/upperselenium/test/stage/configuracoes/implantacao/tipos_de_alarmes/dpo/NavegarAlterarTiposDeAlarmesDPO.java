package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_alarmes.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarAlterarTiposDeAlarmesDPO extends BaseHelperDPO {

	private String labelNome;
	private String labelMotivo;
	private String labelCriticidade;
	private String labelStatus;

	public String getLabelNome() {
		return labelNome;
	}

	public void setLabelNome(String labelNome) {
		this.labelNome = labelNome;
	}

	public String getLabelMotivo() {
		return labelMotivo;
	}

	public void setLabelMotivo(String labelMotivo) {
		this.labelMotivo = labelMotivo;
	}

	public String getLabelCriticidade() {
		return labelCriticidade;
	}

	public void setLabelCriticidade(String labelCriticidade) {
		this.labelCriticidade = labelCriticidade;
	}

	public String getLabelStatus() {
		return labelStatus;
	}

	public void setLabelStatus(String labelStatus) {
		this.labelStatus = labelStatus;
	}

	@Override
	public String toString() {
		return "NavegarAlterarTiposDeAlarmesDPO [labelNome=" + labelNome + ", labelMotivo=" + labelMotivo
				+ ", labelCriticidade=" + labelCriticidade + ", labelStatus=" + labelStatus + "]";
	}

}