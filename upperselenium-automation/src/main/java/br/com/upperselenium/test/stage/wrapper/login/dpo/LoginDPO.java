package br.com.upperselenium.test.stage.wrapper.login.dpo;

public class LoginDPO {

	private String url;
	private String usuario;
	private String senha;
	private String nomeRealDoUsuario;

	public LoginDPO(String url, String usuario, String senha, String nomeRealDoUsuario) {
		this.url = url;
		this.usuario = usuario;
		this.senha = senha;
		this.nomeRealDoUsuario = nomeRealDoUsuario;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getNomeRealDoUsuario() {
		return nomeRealDoUsuario;
	}

	public void setNomeRealDoUsuario(String nomeRealDoUsuario) {
		this.nomeRealDoUsuario = nomeRealDoUsuario;
	}

	@Override
	public String toString() {
		return "LoginDataProvider [url=" + url 
				+ ", usuario=" + usuario + ", senha=" + senha 
				+ ", nomeRealDoUsuario=" + nomeRealDoUsuario + "]";
	}
}
