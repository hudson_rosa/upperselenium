package br.com.upperselenium.test.stage.configuracoes.administracao.perfis;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.dpo.NavegarAlteracaoPerfisDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.page.AlteracaoPerfisPage;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.page.PerfisPage;

public class NavegarAlteracaoPerfisStage extends BaseStage {
	private NavegarAlteracaoPerfisDPO navegarAlteracaoPerfisDPO;
	private PerfisPage perfisPage;
	private AlteracaoPerfisPage alteracaoPerfisPage;
	
	public NavegarAlteracaoPerfisStage(String dp) {
		navegarAlteracaoPerfisDPO = loadDataProviderFile(NavegarAlteracaoPerfisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		perfisPage = initElementsFromPage(PerfisPage.class);
		alteracaoPerfisPage = initElementsFromPage(AlteracaoPerfisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAlteracaoPerfisDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		perfisPage.clickGridButtonFirstLineEditar(navegarAlteracaoPerfisDPO.getClickItem());
		alteracaoPerfisPage.clickAbaPermissoes();
		alteracaoPerfisPage.getBreadcrumbsText(navegarAlteracaoPerfisDPO.getBreadcrumbs());
		alteracaoPerfisPage.clickUpToBreadcrumbs(navegarAlteracaoPerfisDPO.getUpToBreadcrumb());
		alteracaoPerfisPage.getWelcomeTitle(navegarAlteracaoPerfisDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
