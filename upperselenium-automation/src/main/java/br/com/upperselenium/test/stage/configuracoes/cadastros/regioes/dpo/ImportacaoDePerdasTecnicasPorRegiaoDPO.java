package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class ImportacaoDePerdasTecnicasPorRegiaoDPO extends BaseHelperDPO {

	private String labelArquivo;
	private String messageErrorArquivo;

	public String getLabelArquivo() {
		return labelArquivo;
	}

	public void setLabelArquivo(String labelArquivo) {
		this.labelArquivo = labelArquivo;
	}

	public String getMessageErrorArquivo() {
		return messageErrorArquivo;
	}

	public void setMessageErrorArquivo(String messageErrorArquivo) {
		this.messageErrorArquivo = messageErrorArquivo;
	}

}