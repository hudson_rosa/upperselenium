package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaOpcoesDeManobrasPontosVirtuaisPage extends BaseHelperPage{
	
	private static final String LINK_ABA_OPCOES_DE_MANOBRAS = "//*[@id='tabs']/ul/li[5]/a";
	private static final String BUTTON_ADICIONAR = "//*[@id='opcoes-de-manobras']/aba-opcoes-de-manobras/div/chaves-de-manobras/div/form-with-validation/form/button";
	
	private static final String TEXT_CHAVE = "//*[@id='input-chave']"; 
	private static final String GRID_FILTER_TEXT_CHAVE = "//*[@id='tabela-chaves-de-manobras']/table/thead/tr[1]/th[1]/list-filter/div/input";
	private static final String GRID_VALUE_CHAVE = "//*[@id='tabela-chaves-de-manobras']/table/tbody/tr/td[1]";
	private static final String GRID_BUTTON_REMOVE = "//*[@id='tabela-chaves-de-manobras']/table/tbody/tr/td[2]/button";
	
	private static final String BUTTON_GERAR_COMBINACOES = "//*[@id='opcoes-de-manobras']/aba-opcoes-de-manobras/div/combinacoes-de-manobras/div/button";	
	private static final String GRID_FILTER_TEXT_CHAVE_COMBINACAO = "//*[@id='tabela-combinacao-de-manobras']/table/thead/tr[1]/th[1]/list-filter/div/input";
	private static final String GRID_FILTER_TEXT_PONTO_APLICADO = "//*[@id='tabela-combinacao-de-manobras']/table/thead/tr[1]/th[2]/list-filter/div/input";	
	private static final String GRID_CHECK_ITEM = "//*[@id='tabela-combinacao-de-manobras']/table/tbody/tr[%i%]/td[1]/input";
	private static final String GRID_VALUE_CHAVE_COMBINACAO = "//*[@id='tabela-combinacao-de-manobras']/table/tbody/tr[%i%]/td[2]";
	private static final String GRID_VALUE_PONTO_APLICADO = "//*[@id='tabela-combinacao-de-manobras']/table/tbody/tr[%i%]/td[3]";
	
	private static final String BUTTON_EDITAR = "//*[@id='opcoes-de-manobras']/aba-opcoes-de-manobras/div/combinacoes-de-manobras/div/div/button";
	private static final String MODAL_TEXT_PONTO = "//*[@id='ponto-aplicado-mesmo']"; 
	private static final String MODAL_BUTTON_SALVAR = "//*[@id='modal-alteracao-de-ponto-aplicado']/form/div[3]/button"; 
	
	
	public void typeTextChave(String value){
		typeText(TEXT_CHAVE, value);
	}	

	public void clickAdicionar(){
		clickOnElement(BUTTON_ADICIONAR);
	}	
	
	public void typeTextFilterChave(String value, int index){
		typeGridText(GRID_FILTER_TEXT_CHAVE, value, index);
	}	
	
	public void getGridValueChave(String value, int index){
		waitForPageToLoadUntil10s();
		getGridLabel(GRID_VALUE_CHAVE, value, index);
	}

	public void clickGridButtonRemove(String value, int index){
		waitForPageToLoad();
		clickGridButtonOnElement(GRID_BUTTON_REMOVE, value, index);
	}
	
	public void clickGerarCombinacoes(){
		clickOnElement(BUTTON_GERAR_COMBINACOES);
		waitForATime(TimePRM._5_SECS);
	}	
	
	public void typeTextFilterChaveCombinacao(String value, int index){
		typeGridText(GRID_FILTER_TEXT_CHAVE_COMBINACAO, value, index);
	}	
	
	public void typeTextFilterPontoAplicado(String value, int index){
		typeGridText(GRID_FILTER_TEXT_PONTO_APLICADO, value, index);
	}	
	
	public void typeCheckBoxGridItem(String value, int index){
		typeGridCheckOption(GRID_CHECK_ITEM, value, index);
	}	
	
	public void getGridValueChaveCombinacao(String value, int index){		
		getGridLabel(GRID_VALUE_CHAVE_COMBINACAO, value, index);
	}
	
	public void getGridValuePontoAplicado(String value, int index){		
		getGridLabel(GRID_VALUE_PONTO_APLICADO, value, index);
	}

	public void clickEditar(){
		clickOnElement(BUTTON_EDITAR);
	}		
	
	public void typeModalTextPonto(String value){
		typeTextAutoCompleteSelect(MODAL_TEXT_PONTO, value);
	}		
	
	public void clickModalSalvar(){
		clickOnElement(MODAL_BUTTON_SALVAR);
	}	
	
	public void keyPageDown(){
		useKey(TEXT_CHAVE, Keys.PAGE_DOWN);
	}	

	public void keyPageUp(){
		useKey(TEXT_CHAVE, Keys.PAGE_UP);
	}	

	public void clickAbaOpcoesDeManobras(){
		clickOnElement(LINK_ABA_OPCOES_DE_MANOBRAS);
	}
	
}