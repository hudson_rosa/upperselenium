package br.com.upperselenium.test.stage.configuracoes.cadastros.regras.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarRegrasDPO extends BaseHelperDPO {

	private String nome;
	private String pasta;
	private String formula;
	private List<GridAlterarRegrasDPO> gridRegras;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPasta() {
		return pasta;
	}

	public void setPasta(String pasta) {
		this.pasta = pasta;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public List<GridAlterarRegrasDPO> getGridRegras() {
		return gridRegras;
	}

	public void setGridRegras(List<GridAlterarRegrasDPO> gridRegras) {
		this.gridRegras = gridRegras;
	}

}
