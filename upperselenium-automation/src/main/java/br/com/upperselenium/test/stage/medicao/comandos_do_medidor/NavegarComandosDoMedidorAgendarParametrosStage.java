package br.com.upperselenium.test.stage.medicao.comandos_do_medidor;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo.NavegarComandosDeMedidorAgendarParametrosDPO;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.page.ComandosDoMedidorAgendarParametrosPage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.page.ComandosDoMedidorPage;

public class NavegarComandosDoMedidorAgendarParametrosStage extends BaseStage {
	private NavegarComandosDeMedidorAgendarParametrosDPO navegarComandosDoMedidorAgendarParamDPO;
	private ComandosDoMedidorPage comandosDoMedidorPage;	
	private ComandosDoMedidorAgendarParametrosPage comandosDoMedidorAgendarParamPage;	
	
	public NavegarComandosDoMedidorAgendarParametrosStage(String dp) {
		navegarComandosDoMedidorAgendarParamDPO = loadDataProviderFile(NavegarComandosDeMedidorAgendarParametrosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		comandosDoMedidorPage = initElementsFromPage(ComandosDoMedidorPage.class);
		comandosDoMedidorAgendarParamPage = initElementsFromPage(ComandosDoMedidorAgendarParametrosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarComandosDoMedidorAgendarParamDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		comandosDoMedidorPage.clickLinkAgendarPorParametros();
		comandosDoMedidorAgendarParamPage.getLabelTipo(navegarComandosDoMedidorAgendarParamDPO.getLabelTipo());
		comandosDoMedidorAgendarParamPage.getLabelInstante(navegarComandosDoMedidorAgendarParamDPO.getLabelInstante());
		comandosDoMedidorAgendarParamPage.getLabelExpiracao(navegarComandosDoMedidorAgendarParamDPO.getLabelExpiracao());
		comandosDoMedidorAgendarParamPage.getLabelMedidoresPorParametros(navegarComandosDoMedidorAgendarParamDPO.getLabelMedidoresPorParametros());		
		comandosDoMedidorAgendarParamPage.getLabelPasta(navegarComandosDoMedidorAgendarParamDPO.getLabelPasta());		
		comandosDoMedidorAgendarParamPage.getLabelTags(navegarComandosDoMedidorAgendarParamDPO.getLabelTags());		
		comandosDoMedidorAgendarParamPage.getLabelEtapa(navegarComandosDoMedidorAgendarParamDPO.getLabelEtapa());		
		comandosDoMedidorAgendarParamPage.getLabelTipoDeComunicacao(navegarComandosDoMedidorAgendarParamDPO.getLabelTipoDeComunicacao());		
		comandosDoMedidorAgendarParamPage.getLabelTipoDePonto(navegarComandosDoMedidorAgendarParamDPO.getLabelTipoDePonto());		
		comandosDoMedidorAgendarParamPage.getLabelRepeticao(navegarComandosDoMedidorAgendarParamDPO.getLabelRepeticao());		
		comandosDoMedidorAgendarParamPage.getBreadcrumbsText(navegarComandosDoMedidorAgendarParamDPO.getBreadcrumbs());
		comandosDoMedidorAgendarParamPage.clickUpToBreadcrumbs(navegarComandosDoMedidorAgendarParamDPO.getUpToBreadcrumb());
		comandosDoMedidorAgendarParamPage.getWelcomeTitle(navegarComandosDoMedidorAgendarParamDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
