package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaComunicacaoMedidoresMtoZDPO extends BaseHelperDPO {

	// M2M
	private String comunicacaoM2m;
	private String m2mId;

	// Nansen K Art - ABNT Multiponto Serial Ethernet
	private String comunicacaoNansenKArtEth;
	private String nansenKArtEnderecoOuIp;
	private String nansenKArtPorta;

	// Nansen Spectrun SX-ABNT Multiponto Serial Ethernet
	private String comunicacaoNansenSpectrumSxEth;
	private String nansenSpectrunSxEnderecoOuIp;
	private String nansenSpectrunSxPorta;

	// NEXUS - Dial-Up
	private String comunicacaoNexusDialUp;
	private String nexusDialupTelefone;
	private String nexusDialupId;
	private String nexusDialupUsuarioParaComunicacao;
	private String nexusDialupSenhaParaComunicacao;
	private String nexusDialupColetarFormasOnda;

	// NEXUS - Ethernet
	private String comunicacaoNexusEth;
	private String nexusEthEnderecoOuIp;
	private String nexusEthPorta;
	private String nexusEthId;
	private String nexusEthColetarFormasOnda;
	private String nexusEthSeg;
	private String nexusEthTer;
	private String nexusEthQua;
	private String nexusEthQui;
	private String nexusEthSex;
	private String nexusEthSab;
	private String nexusEthDom;
	private String nexusEthHoraInicioDaColeta;
	private String nexusEthMinutoInicioDaColeta;
	private String nexusEthHoraFimDaColeta;
	private String nexusEthMinutoFimDaColeta;
	private String nexusEthUsuarioParaComunicacao;
	private String nexusEthSenhaParaComunicacao;

	// Nexus 1250
	private String comunicacaoNexus1250;
	private String nexus1250EnderecoOuIp;
	private String nexus1250DeviceAddress;
	private String nexus1250Porta;
	private String nexus1250Timeout;
	private String nexus1250Retentativas;
	private String nexus1250SenhaNivel1;
	private String nexus1250SenhaNivel2;
	private String nexus1250UsuarioEstendido;
	private String nexus1250SenhaUsuarioEstendido;

	// Nexus 1500
	private String comunicacaoNexus1500;
	private String nexus1500EnderecoOuIp;
	private String nexus1500DeviceAddress;
	private String nexus1500Porta;
	private String nexus1500Timeout;
	private String nexus1500Retentativas;
	private String nexus1500SenhaNivel1;
	private String nexus1500SenhaNivel2;
	private String nexus1500UsuarioEstendido;
	private String nexus1500SenhaUsuarioEstendido;

	// OneRF - Ethernet
	private String comunicacaoOneRfEth;
	private String onerfEthEnderecoOuIp;
	private String onerfEthPorta;

	// PQube3 - Ethernet e FTP
	private String comunicacaoPQubeEthFtp;
	private String pqube3EthFtpEnderecoOuIp;
	private String pqube3EthFtpPorta;
	private String pqube3EthFtpId;
	private String pqube3EthFtpPortaFt;
	private String pqube3EthFtpUsuario;
	private String pqube3EthFtpSenha;
	private String pqube3EthFtpOffsetDeMemoria;

	// Q1000 - Ethergate ou Terminal Server
	private String comunicacaoQ1000EtgTerm;
	private String q1000EtgTermEnderecoOuIp;
	private String q1000EtgTermPorta;
	private String q1000EtgTermId;
	private String q1000EtgTermUsuarioParaComunicacao;
	private String q1000EtgTermSenhaParaComunicacao;

	// Q1000 - Ethernet
	private String comunicacaoQ1000Eth;
	private String q1000EthEnderecoOuIp;
	private String q1000EthPorta;
	private String q1000EthId;
	private String q1000EthTimeout;
	private String q1000EthRetentativas;
	private String q1000EthSenha;

	// SAD - M2M
	private String comunicacaoSadM2m;
	private String sadM2mUrl;
	private String sadM2mRemota;

	// SAGA1000 - Dial-Up
	private String comunicacaoSaga1000DialUp;
	private String saga1000DialupEnderecoOuIp;
	private String saga1000DialupId;
	private String saga1000DialupSeg;
	private String saga1000DialupTer;
	private String saga1000DialupQua;
	private String saga1000DialupQui;
	private String saga1000DialupSex;
	private String saga1000DialupSab;
	private String saga1000DialupDom;
	private String saga1000DialupHoraInicioDaColeta;
	private String saga1000DialupMinutoInicioDaColeta;
	private String saga1000DialupHoraFimDaColeta;
	private String saga1000DialupMinutoFimDaColeta;

	// SAGA1000 - Ethergate ou Terminal Server
	private String comunicacaoSaga1000EtgTerm;
	private String saga1000EtgEnderecoOuIp;
	private String saga1000EtgPorta;
	private String saga1000EtgId;

	// SAGA1000 - Monoponto
	private String comunicacaoSaga1000Mp;
	private String saga1000MpEnderecoOuIp;
	private String saga1000MpPorta;

	// SAGA2000 - Monoponto
	private String comunicacaoSaga2000Mp;
	private String saga2000MpEnderecoOuIp;
	private String saga2000MpPorta;

	// SatLink2 - Satélite *
	private String comunicacaoSatLink2Satelite;

	// Shark - Modbus TCP/IP
	private String comunicacaoSharkModBusTcpIp;
	private String sharkModbusEnderecoOuIp;
	private String sharkModbusPorta;
	private String sharkModbusId;

	// Sistema Chip
	private String comunicacaoSistemaChip;
	private String sistemaChipNumeroDoTransponder;
	private String sistemaChipUrlDoWebService;
	private String sistemaChipUsuario;
	private String sistemaChipSenha;

	// SL7000 - Ethergate ou Terminal Server
	private String comunicacaoSl7000EtgTerm;
	private String sl7000EtgTermEnderecoOuIp;
	private String sl7000EtgTermDeviceAddress;
	private String sl7000EtgTermPorta;
	private String sl7000EtgTermUsuarioParaComunicacao;
	private String sl7000EtgTermSenhaParaComunicacao;

	// SL7000 - TCP
	private String comunicacaoSl7000Tcp;
	private String sl7000TcpEnderecoOuIp;
	private String sl7000TcpPorta;
	private String sl7000TcpId;
	private String sl7000TcpTimeout;
	private String sl7000TcpRetentativas;
	private String sl7000TcpTiposDeCliente;
	private String sl7000TcpSenha;

	// SL7000-Ethernet-E3-RT
	private String comunicacaoSl7000EthE3Rt;
	private String sl7000EthE3RtEnderecoOuIp;
	private String sl7000EthE3RtPorta;
	private String sl7000EthE3RtId;
	private String sl7000EthE3RtColetarFormasOnda;
	private String sl7000EthE3RtUsuarioParaComunicacao;
	private String sl7000EthE3RtSenhaParaComunicacao;

	// SmartGreen
	private String comunicacaoSmartGreen;
	private String smartgreenDeviceId;

	// Stevens DOT Logger - Satélite *
	private String comunicacaoStevensDotLoggerSatelite;

	// STM
	private String comunicacaoStm;
	private String stmStrRemoto;

	// Tekpea
	private String comunicacaoTekpea;
	private String tekpeaDeviceId;

	// TNS
	private String comunicacaoTns;
	private String tnsNumeroDoTransponder;

	// WebService
	private String comunicacaoWebservice;
	private String webserviceIdentificadorRemoto;

	// WebService - Hidrologia
	private String comunicacaoWebserviceHidrologia;
	private String wsHidrologiaCodigoSensorChuva;
	private String wsHidrologiaCodigoSensorNivel;

	// Weg ModBus - Ethernet
	private String comunicacaoWegModBusEth;
	private String wegModbusEnderecoOuIp;
	private String wegModbusPorta;

	// Wits *
	private String comunicacaoWits;

	// ZIV - Ethernet
	private String comunicacaoZivEth;
	private String zivEthEnderecoOuIp;
	private String zivEthPorta;
	private String zivEthId;
	private String zivEthUsuarioParaComunicacao;
	private String zivEthSenhaParaComunicacao;

	public String getComunicacaoM2m() {
		return comunicacaoM2m;
	}

	public String getM2mId() {
		return m2mId;
	}

	public String getComunicacaoNansenKArtEth() {
		return comunicacaoNansenKArtEth;
	}

	public String getNansenKArtEnderecoOuIp() {
		return nansenKArtEnderecoOuIp;
	}

	public String getNansenKArtPorta() {
		return nansenKArtPorta;
	}

	public String getComunicacaoNansenSpectrumSxEth() {
		return comunicacaoNansenSpectrumSxEth;
	}

	public String getNansenSpectrunSxEnderecoOuIp() {
		return nansenSpectrunSxEnderecoOuIp;
	}

	public String getNansenSpectrunSxPorta() {
		return nansenSpectrunSxPorta;
	}

	public String getComunicacaoNexusDialUp() {
		return comunicacaoNexusDialUp;
	}

	public String getNexusDialupTelefone() {
		return nexusDialupTelefone;
	}

	public String getNexusDialupId() {
		return nexusDialupId;
	}

	public String getNexusDialupUsuarioParaComunicacao() {
		return nexusDialupUsuarioParaComunicacao;
	}

	public String getNexusDialupSenhaParaComunicacao() {
		return nexusDialupSenhaParaComunicacao;
	}

	public String getNexusDialupColetarFormasOnda() {
		return nexusDialupColetarFormasOnda;
	}

	public String getComunicacaoNexusEth() {
		return comunicacaoNexusEth;
	}

	public String getNexusEthEnderecoOuIp() {
		return nexusEthEnderecoOuIp;
	}

	public String getNexusEthPorta() {
		return nexusEthPorta;
	}

	public String getNexusEthId() {
		return nexusEthId;
	}

	public String getNexusEthColetarFormasOnda() {
		return nexusEthColetarFormasOnda;
	}

	public String getNexusEthSeg() {
		return nexusEthSeg;
	}

	public String getNexusEthTer() {
		return nexusEthTer;
	}

	public String getNexusEthQua() {
		return nexusEthQua;
	}

	public String getNexusEthQui() {
		return nexusEthQui;
	}

	public String getNexusEthSex() {
		return nexusEthSex;
	}

	public String getNexusEthSab() {
		return nexusEthSab;
	}

	public String getNexusEthDom() {
		return nexusEthDom;
	}

	public String getNexusEthHoraInicioDaColeta() {
		return nexusEthHoraInicioDaColeta;
	}

	public String getNexusEthMinutoInicioDaColeta() {
		return nexusEthMinutoInicioDaColeta;
	}

	public String getNexusEthHoraFimDaColeta() {
		return nexusEthHoraFimDaColeta;
	}

	public String getNexusEthMinutoFimDaColeta() {
		return nexusEthMinutoFimDaColeta;
	}

	public String getNexusEthUsuarioParaComunicacao() {
		return nexusEthUsuarioParaComunicacao;
	}

	public String getNexusEthSenhaParaComunicacao() {
		return nexusEthSenhaParaComunicacao;
	}

	public String getComunicacaoNexus1250() {
		return comunicacaoNexus1250;
	}

	public String getNexus1250EnderecoOuIp() {
		return nexus1250EnderecoOuIp;
	}

	public String getNexus1250DeviceAddress() {
		return nexus1250DeviceAddress;
	}

	public String getNexus1250Porta() {
		return nexus1250Porta;
	}

	public String getNexus1250Timeout() {
		return nexus1250Timeout;
	}

	public String getNexus1250Retentativas() {
		return nexus1250Retentativas;
	}

	public String getNexus1250SenhaNivel1() {
		return nexus1250SenhaNivel1;
	}

	public String getNexus1250SenhaNivel2() {
		return nexus1250SenhaNivel2;
	}

	public String getNexus1250UsuarioEstendido() {
		return nexus1250UsuarioEstendido;
	}

	public String getNexus1250SenhaUsuarioEstendido() {
		return nexus1250SenhaUsuarioEstendido;
	}

	public String getComunicacaoNexus1500() {
		return comunicacaoNexus1500;
	}

	public String getNexus1500EnderecoOuIp() {
		return nexus1500EnderecoOuIp;
	}

	public String getNexus1500DeviceAddress() {
		return nexus1500DeviceAddress;
	}

	public String getNexus1500Porta() {
		return nexus1500Porta;
	}

	public String getNexus1500Timeout() {
		return nexus1500Timeout;
	}

	public String getNexus1500Retentativas() {
		return nexus1500Retentativas;
	}

	public String getNexus1500SenhaNivel1() {
		return nexus1500SenhaNivel1;
	}

	public String getNexus1500SenhaNivel2() {
		return nexus1500SenhaNivel2;
	}

	public String getNexus1500UsuarioEstendido() {
		return nexus1500UsuarioEstendido;
	}

	public String getNexus1500SenhaUsuarioEstendido() {
		return nexus1500SenhaUsuarioEstendido;
	}

	public String getComunicacaoOneRfEth() {
		return comunicacaoOneRfEth;
	}

	public String getOnerfEthEnderecoOuIp() {
		return onerfEthEnderecoOuIp;
	}

	public String getOnerfEthPorta() {
		return onerfEthPorta;
	}

	public String getComunicacaoPQubeEthFtp() {
		return comunicacaoPQubeEthFtp;
	}

	public String getPqube3EthFtpEnderecoOuIp() {
		return pqube3EthFtpEnderecoOuIp;
	}

	public String getPqube3EthFtpPorta() {
		return pqube3EthFtpPorta;
	}

	public String getPqube3EthFtpId() {
		return pqube3EthFtpId;
	}

	public String getPqube3EthFtpPortaFt() {
		return pqube3EthFtpPortaFt;
	}

	public String getPqube3EthFtpUsuario() {
		return pqube3EthFtpUsuario;
	}

	public String getPqube3EthFtpSenha() {
		return pqube3EthFtpSenha;
	}

	public String getPqube3EthFtpOffsetDeMemoria() {
		return pqube3EthFtpOffsetDeMemoria;
	}

	public String getComunicacaoQ1000EtgTerm() {
		return comunicacaoQ1000EtgTerm;
	}

	public String getQ1000EtgTermEnderecoOuIp() {
		return q1000EtgTermEnderecoOuIp;
	}

	public String getQ1000EtgTermPorta() {
		return q1000EtgTermPorta;
	}

	public String getQ1000EtgTermId() {
		return q1000EtgTermId;
	}

	public String getQ1000EtgTermUsuarioParaComunicacao() {
		return q1000EtgTermUsuarioParaComunicacao;
	}

	public String getQ1000EtgTermSenhaParaComunicacao() {
		return q1000EtgTermSenhaParaComunicacao;
	}

	public String getComunicacaoQ1000Eth() {
		return comunicacaoQ1000Eth;
	}

	public String getQ1000EthEnderecoOuIp() {
		return q1000EthEnderecoOuIp;
	}

	public String getQ1000EthPorta() {
		return q1000EthPorta;
	}

	public String getQ1000EthId() {
		return q1000EthId;
	}

	public String getQ1000EthTimeout() {
		return q1000EthTimeout;
	}

	public String getQ1000EthRetentativas() {
		return q1000EthRetentativas;
	}

	public String getQ1000EthSenha() {
		return q1000EthSenha;
	}

	public String getComunicacaoSadM2m() {
		return comunicacaoSadM2m;
	}

	public String getSadM2mUrl() {
		return sadM2mUrl;
	}

	public String getSadM2mRemota() {
		return sadM2mRemota;
	}

	public String getComunicacaoSaga1000DialUp() {
		return comunicacaoSaga1000DialUp;
	}

	public String getSaga1000DialupEnderecoOuIp() {
		return saga1000DialupEnderecoOuIp;
	}

	public String getSaga1000DialupId() {
		return saga1000DialupId;
	}

	public String getSaga1000DialupSeg() {
		return saga1000DialupSeg;
	}

	public String getSaga1000DialupTer() {
		return saga1000DialupTer;
	}

	public String getSaga1000DialupQua() {
		return saga1000DialupQua;
	}

	public String getSaga1000DialupQui() {
		return saga1000DialupQui;
	}

	public String getSaga1000DialupSex() {
		return saga1000DialupSex;
	}

	public String getSaga1000DialupSab() {
		return saga1000DialupSab;
	}

	public String getSaga1000DialupDom() {
		return saga1000DialupDom;
	}

	public String getSaga1000DialupHoraInicioDaColeta() {
		return saga1000DialupHoraInicioDaColeta;
	}

	public String getSaga1000DialupMinutoInicioDaColeta() {
		return saga1000DialupMinutoInicioDaColeta;
	}

	public String getSaga1000DialupHoraFimDaColeta() {
		return saga1000DialupHoraFimDaColeta;
	}

	public String getSaga1000DialupMinutoFimDaColeta() {
		return saga1000DialupMinutoFimDaColeta;
	}

	public String getComunicacaoSaga1000EtgTerm() {
		return comunicacaoSaga1000EtgTerm;
	}

	public String getSaga1000EtgEnderecoOuIp() {
		return saga1000EtgEnderecoOuIp;
	}

	public String getSaga1000EtgPorta() {
		return saga1000EtgPorta;
	}

	public String getSaga1000EtgId() {
		return saga1000EtgId;
	}

	public String getComunicacaoSaga1000Mp() {
		return comunicacaoSaga1000Mp;
	}

	public String getSaga1000MpEnderecoOuIp() {
		return saga1000MpEnderecoOuIp;
	}

	public String getSaga1000MpPorta() {
		return saga1000MpPorta;
	}

	public String getComunicacaoSaga2000Mp() {
		return comunicacaoSaga2000Mp;
	}

	public String getSaga2000MpEnderecoOuIp() {
		return saga2000MpEnderecoOuIp;
	}

	public String getSaga2000MpPorta() {
		return saga2000MpPorta;
	}

	public String getComunicacaoSatLink2Satelite() {
		return comunicacaoSatLink2Satelite;
	}

	public String getComunicacaoSharkModBusTcpIp() {
		return comunicacaoSharkModBusTcpIp;
	}

	public String getSharkModbusEnderecoOuIp() {
		return sharkModbusEnderecoOuIp;
	}

	public String getSharkModbusPorta() {
		return sharkModbusPorta;
	}

	public String getSharkModbusId() {
		return sharkModbusId;
	}

	public String getComunicacaoSistemaChip() {
		return comunicacaoSistemaChip;
	}

	public String getSistemaChipNumeroDoTransponder() {
		return sistemaChipNumeroDoTransponder;
	}

	public String getSistemaChipUrlDoWebService() {
		return sistemaChipUrlDoWebService;
	}

	public String getSistemaChipUsuario() {
		return sistemaChipUsuario;
	}

	public String getSistemaChipSenha() {
		return sistemaChipSenha;
	}

	public String getComunicacaoSl7000EtgTerm() {
		return comunicacaoSl7000EtgTerm;
	}

	public String getSl7000EtgTermEnderecoOuIp() {
		return sl7000EtgTermEnderecoOuIp;
	}

	public String getSl7000EtgTermDeviceAddress() {
		return sl7000EtgTermDeviceAddress;
	}

	public String getSl7000EtgTermPorta() {
		return sl7000EtgTermPorta;
	}

	public String getSl7000EtgTermUsuarioParaComunicacao() {
		return sl7000EtgTermUsuarioParaComunicacao;
	}

	public String getSl7000EtgTermSenhaParaComunicacao() {
		return sl7000EtgTermSenhaParaComunicacao;
	}

	public String getComunicacaoSl7000Tcp() {
		return comunicacaoSl7000Tcp;
	}

	public String getSl7000TcpEnderecoOuIp() {
		return sl7000TcpEnderecoOuIp;
	}

	public String getSl7000TcpPorta() {
		return sl7000TcpPorta;
	}

	public String getSl7000TcpId() {
		return sl7000TcpId;
	}

	public String getSl7000TcpTimeout() {
		return sl7000TcpTimeout;
	}

	public String getSl7000TcpRetentativas() {
		return sl7000TcpRetentativas;
	}

	public String getSl7000TcpTiposDeCliente() {
		return sl7000TcpTiposDeCliente;
	}

	public String getSl7000TcpSenha() {
		return sl7000TcpSenha;
	}

	public String getComunicacaoSl7000EthE3Rt() {
		return comunicacaoSl7000EthE3Rt;
	}

	public String getSl7000EthE3RtEnderecoOuIp() {
		return sl7000EthE3RtEnderecoOuIp;
	}

	public String getSl7000EthE3RtPorta() {
		return sl7000EthE3RtPorta;
	}

	public String getSl7000EthE3RtId() {
		return sl7000EthE3RtId;
	}

	public String getSl7000EthE3RtColetarFormasOnda() {
		return sl7000EthE3RtColetarFormasOnda;
	}

	public String getSl7000EthE3RtUsuarioParaComunicacao() {
		return sl7000EthE3RtUsuarioParaComunicacao;
	}

	public String getSl7000EthE3RtSenhaParaComunicacao() {
		return sl7000EthE3RtSenhaParaComunicacao;
	}

	public String getComunicacaoSmartGreen() {
		return comunicacaoSmartGreen;
	}

	public String getSmartgreenDeviceId() {
		return smartgreenDeviceId;
	}

	public String getComunicacaoStevensDotLoggerSatelite() {
		return comunicacaoStevensDotLoggerSatelite;
	}

	public String getComunicacaoStm() {
		return comunicacaoStm;
	}

	public String getStmStrRemoto() {
		return stmStrRemoto;
	}

	public String getComunicacaoTekpea() {
		return comunicacaoTekpea;
	}

	public String getTekpeaDeviceId() {
		return tekpeaDeviceId;
	}

	public String getComunicacaoTns() {
		return comunicacaoTns;
	}

	public String getTnsNumeroDoTransponder() {
		return tnsNumeroDoTransponder;
	}

	public String getComunicacaoWebservice() {
		return comunicacaoWebservice;
	}

	public String getWebserviceIdentificadorRemoto() {
		return webserviceIdentificadorRemoto;
	}

	public String getComunicacaoWebserviceHidrologia() {
		return comunicacaoWebserviceHidrologia;
	}

	public String getWsHidrologiaCodigoSensorChuva() {
		return wsHidrologiaCodigoSensorChuva;
	}

	public String getWsHidrologiaCodigoSensorNivel() {
		return wsHidrologiaCodigoSensorNivel;
	}

	public String getComunicacaoWegModBusEth() {
		return comunicacaoWegModBusEth;
	}

	public String getWegModbusEnderecoOuIp() {
		return wegModbusEnderecoOuIp;
	}

	public String getWegModbusPorta() {
		return wegModbusPorta;
	}

	public String getComunicacaoWits() {
		return comunicacaoWits;
	}

	public String getComunicacaoZivEth() {
		return comunicacaoZivEth;
	}

	public String getZivEthEnderecoOuIp() {
		return zivEthEnderecoOuIp;
	}

	public String getZivEthPorta() {
		return zivEthPorta;
	}

	public String getZivEthId() {
		return zivEthId;
	}

	public String getZivEthUsuarioParaComunicacao() {
		return zivEthUsuarioParaComunicacao;
	}

	public String getZivEthSenhaParaComunicacao() {
		return zivEthSenhaParaComunicacao;
	}

}