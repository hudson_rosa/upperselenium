package br.com.upperselenium.test.stage.configuracoes.administracao.usuarios;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.dpo.NavegarCadastroUsuariosDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page.CadastroUsuariosPage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page.UsuariosPage;

public class NavegarCadastroUsuariosStage extends BaseStage {
	private NavegarCadastroUsuariosDPO navegarCadastroUsuariosDPO;
	private UsuariosPage usuariosPage;
	private CadastroUsuariosPage cadastroUsuariosPage;
	
	public NavegarCadastroUsuariosStage(String dp) {
		navegarCadastroUsuariosDPO = loadDataProviderFile(NavegarCadastroUsuariosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		usuariosPage = initElementsFromPage(UsuariosPage.class);
		cadastroUsuariosPage = initElementsFromPage(CadastroUsuariosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarCadastroUsuariosDPO.getIssue());
		execute();
	}
	
	private void execute() {
		usuariosPage.clickNovo();
		cadastroUsuariosPage.getBreadcrumbsText(navegarCadastroUsuariosDPO.getBreadcrumbs());
		cadastroUsuariosPage.clickUpToBreadcrumbs(navegarCadastroUsuariosDPO.getUpToBreadcrumb());
		cadastroUsuariosPage.getWelcomeTitle(navegarCadastroUsuariosDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
