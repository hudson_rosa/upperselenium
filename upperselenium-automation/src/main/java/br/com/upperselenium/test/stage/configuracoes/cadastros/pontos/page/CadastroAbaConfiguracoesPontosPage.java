package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaConfiguracoesPontosPage extends BaseHelperPage{
	
	private static final String LINK_ABA_CONFIGURACOES = "//*[@id='tabs']/ul/li[2]/a";  
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 	
	private static final String SELECT_QUADRANTE = "//*[@id='Quadrante']"; 
	private static final String SELECT_ENERGIA_DEL_ENERGIA_REC = "//*[@id='EnergiaDelEnergiaRec']"; 
	private static final String SELECT_SITUACAO_COMERCIAL = "//*[@id='SituacaoComercial']"; 
	private static final String SELECT_MEDICAO_A = "//*[@id='ElementosDeMedicao']"; 
	private static final String SELECT_TENSAO_DE_MEDICAO = "//*[@id='TensaoDeMedicao']"; 
	private static final String TEXT_LIMITE_DE_POTENCIA_DE_CONSUMO = "//*[@id='LimiteDePotenciaDeConsumo']"; 
	private static final String TEXT_LIMITE_DE_POTENCIA_DE_GERACAO = "//*[@id='LimiteDePotenciaDeGeracao']"; 
	private static final String TEXT_CONEXAO = "//*[@id='Conexao']"; 
	private static final String TEXT_CAPACIDADE_DE_CONEXAO = "//*[@id='CapacidadeDeConexao']";
	private static final String TEXT_DEMANDA_DIMENSIONADA = "//*[@id='DemandaDimensionada']"; 
	private static final String CHECK_COMPENSACAO_DE_PERDAS = "//*[@id='CompensacaoDePerdas']"; 
	
	public void typeSelectQuadrante(String value){
		typeSelectComboOption(SELECT_QUADRANTE, value);
	}
	
	public void typeSelectEnergiaDelEnergiaRec(String value){
		typeSelectComboOption(SELECT_ENERGIA_DEL_ENERGIA_REC, value);
	}
	
	public void typeSelectSituacaoComercial(String value){
		typeSelectComboOption(SELECT_SITUACAO_COMERCIAL, value);
	}
	
	public void typeSelectMedicaoA(String value){
		typeSelectComboOption(SELECT_MEDICAO_A, value);
	}
	
	public void typeSelectTensaoDeMedicao(String value){
		typeSelectComboOption(SELECT_TENSAO_DE_MEDICAO, value);
	}
	
	public void typeTextLimiteDePotenciaDeConsumo(String value){
		typeText(TEXT_LIMITE_DE_POTENCIA_DE_CONSUMO, value);
	}
	
	public void typeTextLimiteDePotenciaDeGeracao(String value){
		typeText(TEXT_LIMITE_DE_POTENCIA_DE_GERACAO, value);
	}
	
	public void typeTextConexao(String value){
		typeText(TEXT_CONEXAO, value);
	}
	
	public void typeTextCapacidadeDeConexao(String value){
		typeText(TEXT_CAPACIDADE_DE_CONEXAO, value);
	}
	
	public void typeTextDemandaDimensionada(String value){
		typeText(TEXT_DEMANDA_DIMENSIONADA, value);
	}
	
	public void typeCheckBoxCompensacaoDePerdas(String value){
		typeCheckOption(CHECK_COMPENSACAO_DE_PERDAS, value);
	}
	
	public void keyPageDown(){
		useKey(SELECT_QUADRANTE, Keys.PAGE_DOWN);
	}	
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
				
	public void clickAbaConfiguracoes(){
		clickOnElement(LINK_ABA_CONFIGURACOES);
	}
	
}