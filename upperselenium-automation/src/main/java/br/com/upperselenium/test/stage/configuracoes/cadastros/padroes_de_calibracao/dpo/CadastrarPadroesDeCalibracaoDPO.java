package br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarPadroesDeCalibracaoDPO extends BaseHelperDPO {

	private String identificacao;
	private String modelo;
	private String numeroDeSerie;
	private String numeroDoCertificadoDeCalibracao;
	private String classe;
	private String dataDeValidade;
	private String cargaNominalEnergiaAtivaFornecida;
	private String cargaNominalEnergiaAtivaRecebida;
	private String cargaNominalEnergiaReativaFornecida;
	private String cargaNominalEnergiaReativaRecebida;
	private String cargaIndutivaEnergiaAtivaFornecida;
	private String cargaIndutivaEnergiaAtivaRecebida;
	private String cargaIndutivaEnergiaReativaFornecida;
	private String cargaIndutivaEnergiaReativaRecebida;
	private String cargaCapacitivaEnergiaAtivaFornecida;
	private String cargaCapacitivaEnergiaAtivaRecebida;
	private String cargaCapacitivaEnergiaReativaFornecida;
	private String cargaCapacitivaEnergiaReativaRecebida;
	private String cargaPequenaEnergiaAtivaFornecida;
	private String cargaPequenaEnergiaAtivaRecebida;
	private String cargaPequenaEnergiaReativaFornecida;
	private String cargaPequenaEnergiaReativaRecebida;

	public String getIdentificacao() {
		return identificacao;
	}

	public void setIdentificacao(String identificacao) {
		this.identificacao = identificacao;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getNumeroDeSerie() {
		return numeroDeSerie;
	}

	public void setNumeroDeSerie(String numeroDeSerie) {
		this.numeroDeSerie = numeroDeSerie;
	}

	public String getNumeroDoCertificadoDeCalibracao() {
		return numeroDoCertificadoDeCalibracao;
	}

	public void setNumeroDoCertificadoDeCalibracao(String numeroDoCertificadoDeCalibracao) {
		this.numeroDoCertificadoDeCalibracao = numeroDoCertificadoDeCalibracao;
	}

	public String getClasse() {
		return classe;
	}

	public void setClasse(String classe) {
		this.classe = classe;
	}

	public String getDataDeValidade() {
		return dataDeValidade;
	}

	public void setDataDeValidade(String dataDeValidade) {
		this.dataDeValidade = dataDeValidade;
	}

	public String getCargaNominalEnergiaAtivaFornecida() {
		return cargaNominalEnergiaAtivaFornecida;
	}

	public void setCargaNominalEnergiaAtivaFornecida(String cargaNominalEnergiaAtivaFornecida) {
		this.cargaNominalEnergiaAtivaFornecida = cargaNominalEnergiaAtivaFornecida;
	}

	public String getCargaNominalEnergiaAtivaRecebida() {
		return cargaNominalEnergiaAtivaRecebida;
	}

	public void setCargaNominalEnergiaAtivaRecebida(String cargaNominalEnergiaAtivaRecebida) {
		this.cargaNominalEnergiaAtivaRecebida = cargaNominalEnergiaAtivaRecebida;
	}

	public String getCargaNominalEnergiaReativaFornecida() {
		return cargaNominalEnergiaReativaFornecida;
	}

	public void setCargaNominalEnergiaReativaFornecida(String cargaNominalEnergiaReativaFornecida) {
		this.cargaNominalEnergiaReativaFornecida = cargaNominalEnergiaReativaFornecida;
	}

	public String getCargaNominalEnergiaReativaRecebida() {
		return cargaNominalEnergiaReativaRecebida;
	}

	public void setCargaNominalEnergiaReativaRecebida(String cargaNominalEnergiaReativaRecebida) {
		this.cargaNominalEnergiaReativaRecebida = cargaNominalEnergiaReativaRecebida;
	}

	public String getCargaIndutivaEnergiaAtivaFornecida() {
		return cargaIndutivaEnergiaAtivaFornecida;
	}

	public void setCargaIndutivaEnergiaAtivaFornecida(String cargaIndutivaEnergiaAtivaFornecida) {
		this.cargaIndutivaEnergiaAtivaFornecida = cargaIndutivaEnergiaAtivaFornecida;
	}

	public String getCargaIndutivaEnergiaAtivaRecebida() {
		return cargaIndutivaEnergiaAtivaRecebida;
	}

	public void setCargaIndutivaEnergiaAtivaRecebida(String cargaIndutivaEnergiaAtivaRecebida) {
		this.cargaIndutivaEnergiaAtivaRecebida = cargaIndutivaEnergiaAtivaRecebida;
	}

	public String getCargaIndutivaEnergiaReativaFornecida() {
		return cargaIndutivaEnergiaReativaFornecida;
	}

	public void setCargaIndutivaEnergiaReativaFornecida(String cargaIndutivaEnergiaReativaFornecida) {
		this.cargaIndutivaEnergiaReativaFornecida = cargaIndutivaEnergiaReativaFornecida;
	}

	public String getCargaIndutivaEnergiaReativaRecebida() {
		return cargaIndutivaEnergiaReativaRecebida;
	}

	public void setCargaIndutivaEnergiaReativaRecebida(String cargaIndutivaEnergiaReativaRecebida) {
		this.cargaIndutivaEnergiaReativaRecebida = cargaIndutivaEnergiaReativaRecebida;
	}

	public String getCargaCapacitivaEnergiaAtivaFornecida() {
		return cargaCapacitivaEnergiaAtivaFornecida;
	}

	public void setCargaCapacitivaEnergiaAtivaFornecida(String cargaCapacitivaEnergiaAtivaFornecida) {
		this.cargaCapacitivaEnergiaAtivaFornecida = cargaCapacitivaEnergiaAtivaFornecida;
	}

	public String getCargaCapacitivaEnergiaAtivaRecebida() {
		return cargaCapacitivaEnergiaAtivaRecebida;
	}

	public void setCargaCapacitivaEnergiaAtivaRecebida(String cargaCapacitivaEnergiaAtivaRecebida) {
		this.cargaCapacitivaEnergiaAtivaRecebida = cargaCapacitivaEnergiaAtivaRecebida;
	}

	public String getCargaCapacitivaEnergiaReativaFornecida() {
		return cargaCapacitivaEnergiaReativaFornecida;
	}

	public void setCargaCapacitivaEnergiaReativaFornecida(String cargaCapacitivaEnergiaReativaFornecida) {
		this.cargaCapacitivaEnergiaReativaFornecida = cargaCapacitivaEnergiaReativaFornecida;
	}

	public String getCargaCapacitivaEnergiaReativaRecebida() {
		return cargaCapacitivaEnergiaReativaRecebida;
	}

	public void setCargaCapacitivaEnergiaReativaRecebida(String cargaCapacitivaEnergiaReativaRecebida) {
		this.cargaCapacitivaEnergiaReativaRecebida = cargaCapacitivaEnergiaReativaRecebida;
	}

	public String getCargaPequenaEnergiaAtivaFornecida() {
		return cargaPequenaEnergiaAtivaFornecida;
	}

	public void setCargaPequenaEnergiaAtivaFornecida(String cargaPequenaEnergiaAtivaFornecida) {
		this.cargaPequenaEnergiaAtivaFornecida = cargaPequenaEnergiaAtivaFornecida;
	}

	public String getCargaPequenaEnergiaAtivaRecebida() {
		return cargaPequenaEnergiaAtivaRecebida;
	}

	public void setCargaPequenaEnergiaAtivaRecebida(String cargaPequenaEnergiaAtivaRecebida) {
		this.cargaPequenaEnergiaAtivaRecebida = cargaPequenaEnergiaAtivaRecebida;
	}

	public String getCargaPequenaEnergiaReativaFornecida() {
		return cargaPequenaEnergiaReativaFornecida;
	}

	public void setCargaPequenaEnergiaReativaFornecida(String cargaPequenaEnergiaReativaFornecida) {
		this.cargaPequenaEnergiaReativaFornecida = cargaPequenaEnergiaReativaFornecida;
	}

	public String getCargaPequenaEnergiaReativaRecebida() {
		return cargaPequenaEnergiaReativaRecebida;
	}

	public void setCargaPequenaEnergiaReativaRecebida(String cargaPequenaEnergiaReativaRecebida) {
		this.cargaPequenaEnergiaReativaRecebida = cargaPequenaEnergiaReativaRecebida;
	}

}