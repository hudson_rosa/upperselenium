package br.com.upperselenium.test.stage.configuracoes.cadastros.gateways;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.AreasMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.DropDownMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.SideBarCadastrosPage;

public class GoToGatewaysStage extends BaseStage {
	private AreasMenuPage areasMenuPage;
	private DropDownMenuPage dropDownMenuPage;
	private SideBarCadastrosPage sideBarCadastrosPage;
	
	public GoToGatewaysStage() {}

	@Override
	public void initMappedPages() {
		areasMenuPage = initElementsFromPage(AreasMenuPage.class);
		dropDownMenuPage = initElementsFromPage(DropDownMenuPage.class);
		sideBarCadastrosPage = initElementsFromPage(SideBarCadastrosPage.class);
	}

	@Override
	public void runStage() {
		navigateToPage();	
	}
	
	private void navigateToPage() {
		waitForPageToLoad(TimePRM._20_SECS);
		areasMenuPage.clickConfiguracoes();
		waitForPageToLoadUntil10s();
		dropDownMenuPage.clickCadastros();
		waitForPageToLoadUntil10s();
		sideBarCadastrosPage.clickGateways();
		waitForPageToLoadUntil10s();		
	}

	@Override
	public void runValidations() {}

}
