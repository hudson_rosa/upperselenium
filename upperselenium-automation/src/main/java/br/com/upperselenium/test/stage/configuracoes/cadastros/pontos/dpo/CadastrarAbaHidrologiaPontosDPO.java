package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaHidrologiaPontosDPO extends BaseHelperDPO {

	private String codigoDaEstacaoHidrometeorogica;
	private String siglaDaEstacao;
	private String situacao;
	private String tipoDeEstacao;
	private String login;
	private String senha;
	private String codigoFluviometrico;
	private String codigoPluviometrico;
	private String parametroA;
	private String parametroB;
	private String parametroC;
	private String utilizarLoginESenhaEstacaoANA;
	private String tooltipUtilizarLoginESenhaEstacaoANA;

	public String getCodigoDaEstacaoHidrometeorogica() {
		return codigoDaEstacaoHidrometeorogica;
	}

	public void setCodigoDaEstacaoHidrometeorogica(String codigoDaEstacaoHidrometeorogica) {
		this.codigoDaEstacaoHidrometeorogica = codigoDaEstacaoHidrometeorogica;
	}

	public String getSiglaDaEstacao() {
		return siglaDaEstacao;
	}

	public void setSiglaDaEstacao(String siglaDaEstacao) {
		this.siglaDaEstacao = siglaDaEstacao;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public String getTipoDeEstacao() {
		return tipoDeEstacao;
	}

	public void setTipoDeEstacao(String tipoDeEstacao) {
		this.tipoDeEstacao = tipoDeEstacao;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getCodigoFluviometrico() {
		return codigoFluviometrico;
	}

	public void setCodigoFluviometrico(String codigoFluviometrico) {
		this.codigoFluviometrico = codigoFluviometrico;
	}

	public String getCodigoPluviometrico() {
		return codigoPluviometrico;
	}

	public void setCodigoPluviometrico(String codigoPluviometrico) {
		this.codigoPluviometrico = codigoPluviometrico;
	}

	public String getParametroA() {
		return parametroA;
	}

	public void setParametroA(String parametroA) {
		this.parametroA = parametroA;
	}

	public String getParametroB() {
		return parametroB;
	}

	public void setParametroB(String parametroB) {
		this.parametroB = parametroB;
	}

	public String getParametroC() {
		return parametroC;
	}

	public void setParametroC(String parametroC) {
		this.parametroC = parametroC;
	}

	public String getUtilizarLoginESenhaEstacaoANA() {
		return utilizarLoginESenhaEstacaoANA;
	}

	public void setUtilizarLoginESenhaEstacaoANA(String utilizarLoginESenhaEstacaoANA) {
		this.utilizarLoginESenhaEstacaoANA = utilizarLoginESenhaEstacaoANA;
	}

	public String getTooltipUtilizarLoginESenhaEstacaoANA() {
		return tooltipUtilizarLoginESenhaEstacaoANA;
	}

	public void setTooltipUtilizarLoginESenhaEstacaoANA(String tooltipUtilizarLoginESenhaEstacaoANA) {
		this.tooltipUtilizarLoginESenhaEstacaoANA = tooltipUtilizarLoginESenhaEstacaoANA;
	}

}