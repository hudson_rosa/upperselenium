package br.com.upperselenium.test.stage.configuracoes.administracao.pastas;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.dpo.NavegarAlteracaoPastasDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.page.AlteracaoPastasPage;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.page.PastasPage;

public class NavegarAlteracaoPastasStage extends BaseStage {
	private NavegarAlteracaoPastasDPO navegarAlteracaoPastasDPO;
	private PastasPage pastasPage;
	private AlteracaoPastasPage alteracaoPastasPage;
	
	public NavegarAlteracaoPastasStage(String dp) {
		navegarAlteracaoPastasDPO = loadDataProviderFile(NavegarAlteracaoPastasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		pastasPage = initElementsFromPage(PastasPage.class);
		alteracaoPastasPage = initElementsFromPage(AlteracaoPastasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAlteracaoPastasDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		pastasPage.clickGridButtonFirstLineEditar(navegarAlteracaoPastasDPO.getClickItem());
		alteracaoPastasPage.getBreadcrumbsText(navegarAlteracaoPastasDPO.getBreadcrumbs());
		alteracaoPastasPage.clickUpToBreadcrumbs(navegarAlteracaoPastasDPO.getUpToBreadcrumb());
		alteracaoPastasPage.getWelcomeTitle(navegarAlteracaoPastasDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
