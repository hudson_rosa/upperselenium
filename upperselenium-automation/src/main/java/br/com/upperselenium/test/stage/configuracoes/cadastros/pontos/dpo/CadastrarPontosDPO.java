package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarPontosDPO extends BaseHelperDPO {

	private String nome;
	private String pasta;
	private String tipo;
	private String subestacao;
	private String uc;
	private String trafoBt;
	private String descricao;
	private String latitude;
	private String longitude;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPasta() {
		return pasta;
	}

	public void setPasta(String pasta) {
		this.pasta = pasta;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getSubestacao() {
		return subestacao;
	}

	public void setSubestacao(String subestacao) {
		this.subestacao = subestacao;
	}

	public String getUc() {
		return uc;
	}

	public void setUc(String uc) {
		this.uc = uc;
	}

	public String getTrafoBt() {
		return trafoBt;
	}

	public void setTrafoBt(String trafoBt) {
		this.trafoBt = trafoBt;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

}