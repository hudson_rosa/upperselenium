package br.com.upperselenium.test.stage.configuracoes.cadastros.regras.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoRegrasPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR_REGRA = ".//div[2]/div/div[2]/form/div[2]/button";  
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String SELECT_PASTA = "//*[@id='IdDaPasta']";
	private static final String TEXT_FORMULA = "//*[@id='container-termos']/div[3]/div/input";
	private static final String GRID_LABEL_TERMO = "//*[@id='container-termos']/table/tbody/tr[%i%]/td[1]";
	private static final String GRID_SELECT_SE = "//*[@id='container-termos']/table/tbody/tr[%i%]/td[2]/select";
	private static final String GRID_SELECT_OPERADOR = "//*[@id='container-termos']/table/tbody/tr[%i%]/td[3]/select";
	private static final String GRID_SELECT_VALOR = "//*[@id='container-termos']/table/tbody/tr[%i%]/td[4]/select";
	private static final String GRID_BUTTON_ADD = "//*[@id='container-termos']/table/tbody/tr[%i%]/td[5]/div/a[2]";
	private static final String GRID_BUTTON_EXCLUDE = "//*[@id='container-termos']/table/tbody/tr[%i%]/td[5]/div/a[1]";  
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	
		
	public void typeSelectPasta(String value){		
		typeSelectComboOption(SELECT_PASTA, value);
	}
	
	public void typeTextFormula(String value){
		typeText(TEXT_FORMULA, value);
	}	
		
	public String getGridLabelTermo(String value, int index){
		return getGridLabel(GRID_LABEL_TERMO, value, index);
	}	
		
	public void typeGridSelectSe(String value, int index){		
		typeGridSelectComboOption(GRID_SELECT_SE, value, index);
	}
	
	public void typeGridSelectOperador(String value, int index){		
		typeGridSelectComboOption(GRID_SELECT_OPERADOR, value, index);
	}
	
	public void typeGridSelectValor(String value, int index){		
		typeGridSelectComboOption(GRID_SELECT_VALOR, value, index);
	}
			
	public void clickGridAdd(String value, int index){
		clickGridButtonOnElement(GRID_BUTTON_ADD, value, index);
	}
	
	public void clickGridExclude(String value, int index){
		clickGridButtonOnElement(GRID_BUTTON_EXCLUDE, value, index);
	}			
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR_REGRA);
	}
	
}