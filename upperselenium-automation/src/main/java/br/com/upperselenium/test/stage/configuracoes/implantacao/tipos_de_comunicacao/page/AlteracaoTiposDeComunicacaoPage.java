package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_comunicacao.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoTiposDeComunicacaoPage extends BaseHelperPage {

	private static final String LABEL_HEADER_NOME = "//*[@id='dataTableList']/thead/tr/th[contains(text(),'Nome')]";
	private static final String LABEL_HEADER_DESCRICAO = "//*[@id='dataTableList']/thead/tr/th[contains(text(),'Descrição')]";
	private static final String LABEL_HEADER_ATIVO = "//*[@id='dataTableList']/thead/tr/th[contains(text(),'Ativo')]";

	public void getLabelHeaderNome(String value) {
		getLabel(LABEL_HEADER_NOME, value);
	}

	public void getLabelHeaderDescricao(String value) {
		getLabel(LABEL_HEADER_DESCRICAO, value);
	}

	public void getLabelHeaderAtivo(String value) {
		getLabel(LABEL_HEADER_ATIVO, value);
	}

}