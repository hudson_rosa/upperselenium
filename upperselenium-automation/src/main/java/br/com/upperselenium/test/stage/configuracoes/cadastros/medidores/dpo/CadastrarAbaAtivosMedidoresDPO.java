package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaAtivosMedidoresDPO extends BaseHelperDPO {

	private String fabricacao;
	private String inicioDaGarantia;
	private String fimDaGarantia;
	private String lacre;
	private String propriedade;

	public String getFabricacao() {
		return fabricacao;
	}

	public void setFabricacao(String fabricacao) {
		this.fabricacao = fabricacao;
	}

	public String getInicioDaGarantia() {
		return inicioDaGarantia;
	}

	public void setInicioDaGarantia(String inicioDaGarantia) {
		this.inicioDaGarantia = inicioDaGarantia;
	}

	public String getFimDaGarantia() {
		return fimDaGarantia;
	}

	public void setFimDaGarantia(String fimDaGarantia) {
		this.fimDaGarantia = fimDaGarantia;
	}

	public String getLacre() {
		return lacre;
	}

	public void setLacre(String lacre) {
		this.lacre = lacre;
	}

	public String getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}

}