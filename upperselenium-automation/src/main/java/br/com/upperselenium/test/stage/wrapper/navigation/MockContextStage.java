package br.com.upperselenium.test.stage.wrapper.navigation;

import br.com.upperselenium.base.BaseContext;
import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.EnumContextPRM;
import br.com.upperselenium.base.util.RandomDataGeneratorUtil;

public class MockContextStage extends BaseStage {
	private String customContext;
	
	/**
	 * Na classe de Flow, declare o Stage da seguinte maneira: <br><br>
	 * 				addStage(new MockContextStage("essaEhMinhaStringRandômicaParaOTeste"));
	 * @param url
	 */
	
	public MockContextStage(String customContextMock) {
		customContext = customContextMock;
	}
	
	@Override
	public void initMappedPages() {}
	
	@Override
	public void runStage() {
		if (customContext == null){
			BaseContext.setParameter(EnumContextPRM.RANDOM_NUMBER.getValue(),RandomDataGeneratorUtil.generateRandomNumber(7));
			BaseContext.setParameter(EnumContextPRM.RANDOM_CRYPTED_STRING.getValue(),RandomDataGeneratorUtil.generateRandomData(7));
			BaseContext.setParameter(EnumContextPRM.RANDOM_PURE_SHA1_STRING.getValue(),RandomDataGeneratorUtil.generateRandomData(39));
		} else {
			BaseContext.setParameter(EnumContextPRM.RANDOM_NUMBER.getValue(),customContext);
			BaseContext.setParameter(EnumContextPRM.RANDOM_CRYPTED_STRING.getValue(),customContext);
			BaseContext.setParameter(EnumContextPRM.RANDOM_PURE_SHA1_STRING.getValue(),customContext);
		}
		BaseContext.getParameter(EnumContextPRM.RANDOM_NUMBER.getValue());
		BaseContext.getParameter(EnumContextPRM.RANDOM_CRYPTED_STRING.getValue());
		BaseContext.getParameter(EnumContextPRM.RANDOM_PURE_SHA1_STRING.getValue());
	}

	@Override
	public void runValidations() {}

}
