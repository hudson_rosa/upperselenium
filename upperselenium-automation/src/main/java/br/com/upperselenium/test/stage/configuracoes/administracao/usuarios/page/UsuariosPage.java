package br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class UsuariosPage extends BaseHelperPage{
	
	private static final String LINK_NOVO = ".//div[2]/div/div[2]/p/a";
	private static final String GRID_LINK_NOME_PERFIL = "//*[@id='usuarios']/tbody/tr[1]/td[1]/a";
	private static final String GRID_BUTTON_EDITAR = "//*[@id='usuarios']/tbody/tr%%/td[5]/div/a/i";
	private static final String TEXT_FILTER_NOME = "//*[@id='usuarios']/thead/tr[1]/th[1]/span/input";
	private static final String TEXT_FILTER_LOGIN = "//*[@id='usuarios']/thead/tr[1]/th[2]/span/input";
	private static final String TEXT_FILTER_PERFIL = "//*[@id='usuarios']/thead/tr[1]/th[3]/span/input";
	private static final String TEXT_FILTER_STATUS = "//*[@id='usuarios']/thead/tr[1]/th[4]/span/input";
	private static final String GRID_LABEL_NOME = "//*[@id='usuarios']/tbody/tr[1]/td[1]/a";
	private static final String GRID_LABEL_LOGIN = "//*[@id='usuarios']/tbody/tr[1]/td[2]";
	private static final String GRID_LABEL_PERFIL = "//*[@id='usuarios']/tbody/tr[1]/td[3]";
	
	public boolean isClickableLinkNova() {
		return isDisplayedElement(LINK_NOVO);
	}
		
	public void clickNovo(){
		clickOnElement(LINK_NOVO);
		waitForPageToLoadUntil10s();
	}
	
	public void typeTextFilterNome(String value){
		typeText(TEXT_FILTER_NOME, value);
	}	
	
	public void typeTextFilterLogin(String value){
		typeText(TEXT_FILTER_LOGIN, value);
	}	
	
	public void typeTextFilterPerfil(String value){
		typeText(TEXT_FILTER_PERFIL, value);
	}	
	
	public void typeTextFilterStatus(String value){
		typeText(TEXT_FILTER_STATUS, value);
	}	

	public void getGridLabelItemNome(String value, int index){
		getGridLabel(GRID_LABEL_NOME, value, index);
	}	
	
	public void getGridLabelItemLogin(String value, int index){
		getGridLabel(GRID_LABEL_LOGIN, value, index);
	}	
	
	public void getGridLabelItemPerfil(String value, int index){
		getGridLabel(GRID_LABEL_PERFIL, value, index);
	}	
	
	public void clickGridFirstLineLinkNome(String value){
		waitForPageToLoadUntil10s();
		clickGridButtonOnElement(GRID_LINK_NOME_PERFIL, value, 1);
	}

	public void clickGridButtonFirstLineEditar(String value){
		waitForPageToLoadUntil10s();
		clickGridButtonOnElement(GRID_BUTTON_EDITAR, value, 1);
	}
}