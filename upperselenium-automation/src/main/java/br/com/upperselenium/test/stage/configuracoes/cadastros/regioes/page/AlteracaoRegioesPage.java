package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoRegioesPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/form/fieldset/div[6]/input";   
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String TEXT_DESCRICAO = "//*[@id='Descricao']";
	private static final String SELECT_AREA_GEOGRAFICA = "//*[@id='AreaGeograficaId']";
	private static final String SELECT_PONTO_ENERGIA_REQUERIDA = "//*[@id='PontoDeEnergiaRequerida']";
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}
	
	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}

	public void typeSelectAreaGeografica(String value){
		typeSelectComboOption(SELECT_AREA_GEOGRAFICA, value);
	}
	
	public void typeTextPontoDeEnergiaRequerida(String value){
		typeTextAutoCompleteSelect(SELECT_PONTO_ENERGIA_REQUERIDA, value);
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}