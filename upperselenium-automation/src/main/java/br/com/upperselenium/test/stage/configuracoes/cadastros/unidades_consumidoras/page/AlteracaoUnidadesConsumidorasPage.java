package br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoUnidadesConsumidorasPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='salvar']";   
	private static final String TEXT_NUMERO = "//*[@id='UnidadeConsumidora_Numero']";
	private static final String TEXT_NOME = "//*[@id='UnidadeConsumidora_Nome']";
	private static final String SELECT_RAMO_DE_ATIVIDADE = "//*[@id='RamoDeAtividadeId']";
	private static final String SELECT_ETAPA = "//*[@id='EtapaId']";
	private static final String TEXT_POSTO_TARIFARIO = "//*[@id='NomeDoPostoTarifario']";

	public void typeTextNumero(String value){
		typeText(TEXT_NUMERO, value);
	}
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	
	
	public void typeSelectRamoDeAtividade(String value){
		typeSelectComboOption(SELECT_RAMO_DE_ATIVIDADE, value);
	}
	
	public void typeSelectEtapa(String value){
		typeSelectComboOption(SELECT_ETAPA, value);
	}
	
	public void typeTextPostoTarifario(String value){
		typeTextAutoCompleteSelect(TEXT_POSTO_TARIFARIO, value);
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}