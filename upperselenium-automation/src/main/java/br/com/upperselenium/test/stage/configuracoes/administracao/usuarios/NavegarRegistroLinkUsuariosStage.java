package br.com.upperselenium.test.stage.configuracoes.administracao.usuarios;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.dpo.NavegarRegistroLinkUsuariosDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page.AlteracaoUsuariosPage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page.UsuariosPage;

public class NavegarRegistroLinkUsuariosStage extends BaseStage {
	private NavegarRegistroLinkUsuariosDPO navegarRegistroLinkUsuariosDPO;
	private UsuariosPage usuariosPage;
	private AlteracaoUsuariosPage alteracaoUsuariosPage;
	
	public NavegarRegistroLinkUsuariosStage(String dp) {
		navegarRegistroLinkUsuariosDPO = loadDataProviderFile(NavegarRegistroLinkUsuariosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		usuariosPage = initElementsFromPage(UsuariosPage.class);
		alteracaoUsuariosPage = initElementsFromPage(AlteracaoUsuariosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarRegistroLinkUsuariosDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		usuariosPage.clickGridFirstLineLinkNome(navegarRegistroLinkUsuariosDPO.getClickItem());
		alteracaoUsuariosPage.getBreadcrumbsText(navegarRegistroLinkUsuariosDPO.getBreadcrumbs());
		alteracaoUsuariosPage.clickUpToBreadcrumbs(navegarRegistroLinkUsuariosDPO.getUpToBreadcrumb());
		alteracaoUsuariosPage.getWelcomeTitle(navegarRegistroLinkUsuariosDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
