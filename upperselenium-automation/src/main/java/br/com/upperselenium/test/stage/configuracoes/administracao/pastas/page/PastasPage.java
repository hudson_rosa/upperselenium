package br.com.upperselenium.test.stage.configuracoes.administracao.pastas.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class PastasPage extends BaseHelperPage{
	
	private static final String LINK_NOVA = ".//div[2]/div/div[2]/p/a";
	private static final String GRID_LINK_NOME_PASTA = "//*[@id='dataTableList']/tbody/tr[1]/td[1]/a";
	private static final String GRID_BUTTON_EDITAR = "//*[@id='dataTableList']/tbody/tr%%/td[3]/div/a"; 
				
	public boolean isClickableLinkNova() {
		return isDisplayedElement(LINK_NOVA);
	}
		
	public void clickNova(){
		clickOnElement(LINK_NOVA);
		waitForPageToLoadUntil10s();
	}

	public void clickGridFirstLineLinkNome(String value){
		waitForPageToLoadUntil10s();
		clickGridButtonOnElement(GRID_LINK_NOME_PASTA, value, 1);
	}

	public void clickGridButtonFirstLineEditar(String value){
		waitForPageToLoadUntil10s();
		clickGridButtonOnElement(GRID_BUTTON_EDITAR, value, 1);
	}
}