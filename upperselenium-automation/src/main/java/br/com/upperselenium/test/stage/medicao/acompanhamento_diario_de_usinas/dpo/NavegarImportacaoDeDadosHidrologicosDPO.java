package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarImportacaoDeDadosHidrologicosDPO extends BaseHelperDPO {

	private String labelPeriodoDeImportacao;
	private String labelHeaderInstanteDeImportacao;
	private String labelHeaderTipoDeImportacao;
	private String labelHeaderStatusDeImportacao;
	private String modalLabelAlteracaoDePontoAplicado;
	private String modalLabelTipoDeImportacao;
	private String modalLabelArquivo;
	private String modalButtonLabelImportar;

	public String getLabelPeriodoDeImportacao() {
		return labelPeriodoDeImportacao;
	}

	public void setLabelPeriodoDeImportacao(String labelPeriodoDeImportacao) {
		this.labelPeriodoDeImportacao = labelPeriodoDeImportacao;
	}

	public String getLabelHeaderInstanteDeImportacao() {
		return labelHeaderInstanteDeImportacao;
	}

	public void setLabelHeaderInstanteDeImportacao(String labelHeaderInstanteDeImportacao) {
		this.labelHeaderInstanteDeImportacao = labelHeaderInstanteDeImportacao;
	}

	public String getLabelHeaderTipoDeImportacao() {
		return labelHeaderTipoDeImportacao;
	}

	public void setLabelHeaderTipoDeImportacao(String labelHeaderTipoDeImportacao) {
		this.labelHeaderTipoDeImportacao = labelHeaderTipoDeImportacao;
	}

	public String getLabelHeaderStatusDeImportacao() {
		return labelHeaderStatusDeImportacao;
	}

	public void setLabelHeaderStatusDeImportacao(String labelHeaderStatusDeImportacao) {
		this.labelHeaderStatusDeImportacao = labelHeaderStatusDeImportacao;
	}

	public String getModalLabelAlteracaoDePontoAplicado() {
		return modalLabelAlteracaoDePontoAplicado;
	}

	public void setModalLabelAlteracaoDePontoAplicado(String modalLabelAlteracaoDePontoAplicado) {
		this.modalLabelAlteracaoDePontoAplicado = modalLabelAlteracaoDePontoAplicado;
	}

	public String getModalLabelTipoDeImportacao() {
		return modalLabelTipoDeImportacao;
	}

	public void setModalLabelTipoDeImportacao(String modalLabelTipoDeImportacao) {
		this.modalLabelTipoDeImportacao = modalLabelTipoDeImportacao;
	}

	public String getModalLabelArquivo() {
		return modalLabelArquivo;
	}

	public void setModalLabelArquivo(String modalLabelArquivo) {
		this.modalLabelArquivo = modalLabelArquivo;
	}

	public String getModalButtonLabelImportar() {
		return modalButtonLabelImportar;
	}

	public void setModalButtonLabelImportar(String modalButtonLabelImportar) {
		this.modalButtonLabelImportar = modalButtonLabelImportar;
	}

	@Override
	public String toString() {
		return "NavegarImportacaoDeDadosHidrologicosDPO [labelPeriodoDeImportacao=" + labelPeriodoDeImportacao
				+ ", labelHeaderInstanteDeImportacao=" + labelHeaderInstanteDeImportacao
				+ ", labelHeaderTipoDeImportacao=" + labelHeaderTipoDeImportacao + ", labelHeaderStatusDeImportacao="
				+ labelHeaderStatusDeImportacao + ", modalLabelAlteracaoDePontoAplicado="
				+ modalLabelAlteracaoDePontoAplicado + ", modalLabelTipoDeImportacao=" + modalLabelTipoDeImportacao
				+ ", modalLabelArquivo=" + modalLabelArquivo + ", modalButtonLabelImportar=" + modalButtonLabelImportar
				+ "]";
	}

}