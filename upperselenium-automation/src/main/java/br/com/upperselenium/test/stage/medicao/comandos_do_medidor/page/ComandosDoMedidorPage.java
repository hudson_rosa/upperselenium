package br.com.upperselenium.test.stage.medicao.comandos_do_medidor.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class ComandosDoMedidorPage extends BaseHelperPage{
	
	private static final String LINK_AGENDAR = "//*[@id='tree-content']/div[6]/p/a[contains(text(),'Agendar')]";
	private static final String LINK_AGENDAR_POR_PARAMETROS = "//*[@id='tree-content']/div[6]/p/a[contains(text(),'Agendar por Parâmetros')]";
	private static final String LINK_HISTORICO = "//*[@id='tree-content']/div[6]/p/a[contains(text(),'Histórico')]";
	private static final String LABEL_FILTRAR_DATA_PROXIMA_EXECUCAO = "//*[@id='filtros']/div/label";
	private static final String BUTTON_FILTRAR = "//*[@id='filterbtn']";
	private static final String LABEL_HEADER_MEDIDOR_PARAMETROS = "//*[@id='comandos']/thead/tr[2]/th[1]";
	private static final String LABEL_HEADER_INSERCAO = "//*[@id='comandos']/thead/tr[2]/th[2]";
	private static final String LABEL_HEADER_PROXIMA_EXECUCAO = "//*[@id='comandos']/thead/tr[2]/th[3]";
	private static final String LABEL_HEADER_DESCRICAO = "//*[@id='comandos']/thead/tr[2]/th[4]";
	private static final String LABEL_HEADER_ULTIMAS_EXECUCOES = "//*[@id='comandos']/thead/tr[2]/th[5]";
	private static final String LABEL_HEADER_EXPIRACAO = "//*[@id='comandos']/thead/tr[2]/th[6]";
	
	public void clickLinkAgendar(){
		waitForPageToLoadUntil10s();		
		clickOnElement(LINK_AGENDAR);
		waitForPageToLoadUntil10s();		
	}
	
	public void clickLinkAgendarPorParametros(){
		clickOnElement(LINK_AGENDAR_POR_PARAMETROS);
		waitForPageToLoadUntil10s();
	}
	
	public void clickLinkHistorico(){
		clickOnElement(LINK_HISTORICO);
		waitForPageToLoadUntil10s();
	}
	
	public void clickButtonFiltrar(String value){
		clickOnElementByCondition(BUTTON_FILTRAR, value);
		waitForPageToLoadUntil10s();
	}	
	
	public void getLabelFiltrarPelaDataDaProximaExecucao(String value){
		getLabel(LABEL_FILTRAR_DATA_PROXIMA_EXECUCAO, value);
	}

	public void getLabelHeaderMedidorParametros(String value){
		getLabel(LABEL_HEADER_MEDIDOR_PARAMETROS, value);
	}
	
	public void getLabelHeaderInsercao(String value){
		getLabel(LABEL_HEADER_INSERCAO, value);
	}
	
	public void getLabelHeaderProximaExecucao(String value){
		getLabel(LABEL_HEADER_PROXIMA_EXECUCAO, value);
	}
	
	public void getLabelHeaderDescricao(String value){
		getLabel(LABEL_HEADER_DESCRICAO, value);
	}
	
	public void getLabelHeaderUltimasExecucoes(String value){
		getLabel(LABEL_HEADER_ULTIMAS_EXECUCOES, value);
	}
	
	public void getLabelHeaderExpiracao(String value){
		getLabel(LABEL_HEADER_EXPIRACAO, value);
	}

}