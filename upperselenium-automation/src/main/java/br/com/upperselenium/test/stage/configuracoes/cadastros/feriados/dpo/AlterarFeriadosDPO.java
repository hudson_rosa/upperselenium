package br.com.upperselenium.test.stage.configuracoes.cadastros.feriados.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarFeriadosDPO extends BaseHelperDPO {

	private String nome;
	private String dataDia;
	private String descricao;
	private String estado;
	private String cidade;
	private String regiao;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDataDia() {
		return dataDia;
	}

	public void setDataDia(String dataDia) {
		this.dataDia = dataDia;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getRegiao() {
		return regiao;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

}
