package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.administracao.relatorio_de_auditoria.GoToRelatorioDeAuditoriaStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.relatorio_de_auditoria.NavegarRelatorioDeAuditoriaStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0003RelatorioDeAuditoriaNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0003-RelatorioDeAuditoria", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Relatório de Auditoria validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0003RelatorioDeAuditoriaNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToRelatorioDeAuditoriaStage());
		addStage(new NavegarRelatorioDeAuditoriaStage(getDP("NavegarRelatorioDeAuditoriaDP.json")));
	}	
}
