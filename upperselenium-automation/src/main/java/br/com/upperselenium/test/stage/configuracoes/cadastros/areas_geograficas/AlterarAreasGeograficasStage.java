package br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas.dpo.AlterarAreasGeograficasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas.page.AlteracaoAreasGeograficasPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas.page.AreasGeograficasPage;

public class AlterarAreasGeograficasStage extends BaseStage {
	private AlterarAreasGeograficasDPO alterarAreasGeograficasDPO;
	private AreasGeograficasPage areasGeograficasPage;
	private AlteracaoAreasGeograficasPage alteracaoAreasGeograficasPage;
	
	public AlterarAreasGeograficasStage(String dp) {
		alterarAreasGeograficasDPO = loadDataProviderFile(AlterarAreasGeograficasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		areasGeograficasPage = initElementsFromPage(AreasGeograficasPage.class);
		alteracaoAreasGeograficasPage = initElementsFromPage(AlteracaoAreasGeograficasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarAreasGeograficasDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();		
		alteracaoAreasGeograficasPage.typeTextNome(alterarAreasGeograficasDPO.getNome());
		alteracaoAreasGeograficasPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoAreasGeograficasPage.validateMessageDefault(alterarAreasGeograficasDPO.getOperationMessage());
	}


}
