package br.com.upperselenium.test.stage.configuracoes.cadastros.tensoes;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.tensoes.dpo.AlterarTensoesDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.tensoes.page.AlteracaoTensoesPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.tensoes.page.TensoesPage;

public class AlterarTensoesStage extends BaseStage {
	private AlterarTensoesDPO alterarTensoesDPO;
	private TensoesPage tensoesPage;
	private AlteracaoTensoesPage alteracaoTensoesPage;
	
	public AlterarTensoesStage(String dp) {
		alterarTensoesDPO = loadDataProviderFile(AlterarTensoesDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tensoesPage = initElementsFromPage(TensoesPage.class);
		alteracaoTensoesPage = initElementsFromPage(AlteracaoTensoesPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarTensoesDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoTensoesPage.typeTextValor(alterarTensoesDPO.getValor()+getRandomNumber());		
		alteracaoTensoesPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoTensoesPage.validateMessageDefault(alterarTensoesDPO.getOperationMessage());
	}

}
