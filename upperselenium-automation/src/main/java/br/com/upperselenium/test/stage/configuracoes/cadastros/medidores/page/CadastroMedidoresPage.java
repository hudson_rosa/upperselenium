package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroMedidoresPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='salvar']";   
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String TEXT_DESCRICAO = "//*[@id='Descricao']";
	private static final String TEXT_NUMERO_INTERNO = "//*[@id='NumeroInterno']";
	private static final String TEXT_DIGITO_CONTROLE = "//*[@id='DigitoControle']";	
	private static final String SELECT_TIPO = "//*[@id='IdDoFabricanteEModelo']";
	private static final String TEXT_VERSAO_DO_FIRMWARE = "//*[@id='VersaoDoFirmware']";
	private static final String TEXT_NUMERO_DE_SERIE = "//*[@id='NumeroDeSerie']";
	private static final String TEXT_INICIO_DA_VIGENCIA = "//*[@id='InicioDaVigencia']";
	private static final String SELECT_FUNCAO = "//*[@id='IdListaDaFuncao']";
		
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	

	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}	
	
	public void typeTextNumeroInterno(String value){
		typeText(TEXT_NUMERO_INTERNO, value);
	}	
	
	public void typeTextDigitoControle(String value){
		typeText(TEXT_DIGITO_CONTROLE, value);
	}	

	public void typeSelectTipo(String value){
		typeSelectComboOption(SELECT_TIPO, value);
	}
	
	public void typeTextVersaoDoFirmware(String value){
		typeText(TEXT_VERSAO_DO_FIRMWARE, value);
	}
	
	public void typeTextNumeroDeSerie(String value){
		typeText(TEXT_NUMERO_DE_SERIE, value);
	}
	
	public void typeTextInicioDaVigencia(String value){
		typeDatePickerDefault(TEXT_INICIO_DA_VIGENCIA, value);		
	}
	
	public void typeSelectFuncao(String value){
		typeSelectComboOption(SELECT_FUNCAO, value);
	}
	
	public void keyPageDown(){
		useKey(TEXT_NUMERO_DE_SERIE, Keys.PAGE_DOWN);
	}	

	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}