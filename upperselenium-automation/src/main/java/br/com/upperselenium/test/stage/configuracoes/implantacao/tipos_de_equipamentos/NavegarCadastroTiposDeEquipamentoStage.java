package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.dpo.NavegarCadastroTiposDeEquipamentoDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.page.CadastroTiposDeEquipamentoPage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.page.TiposDeEquipamentoPage;

public class NavegarCadastroTiposDeEquipamentoStage extends BaseStage {
	private NavegarCadastroTiposDeEquipamentoDPO navegarCadastroTiposDeEquipamentoDPO;
	private TiposDeEquipamentoPage tiposDeEquipamentoPage;
	private CadastroTiposDeEquipamentoPage cadastroTiposDeEquipamentoPage;
	
	public NavegarCadastroTiposDeEquipamentoStage(String dp) {
		navegarCadastroTiposDeEquipamentoDPO = loadDataProviderFile(NavegarCadastroTiposDeEquipamentoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDeEquipamentoPage = initElementsFromPage(TiposDeEquipamentoPage.class);
		cadastroTiposDeEquipamentoPage = initElementsFromPage(CadastroTiposDeEquipamentoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarCadastroTiposDeEquipamentoDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		tiposDeEquipamentoPage.clickNovo();
		cadastroTiposDeEquipamentoPage.getBreadcrumbsText(navegarCadastroTiposDeEquipamentoDPO.getBreadcrumbs());
		cadastroTiposDeEquipamentoPage.clickUpToBreadcrumbs(navegarCadastroTiposDeEquipamentoDPO.getUpToBreadcrumb());
		cadastroTiposDeEquipamentoPage.getWelcomeTitle(navegarCadastroTiposDeEquipamentoDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
