package br.com.upperselenium.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseSuite;
import br.com.upperselenium.base.annotation.SuiteParams;
import br.com.upperselenium.test.flow.valida_cadastros.T0001AgentesCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0002AlimentadoresCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0003AreasGeograficasCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0004AutoDiscoveryCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0005CidadesCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0006CircuitosCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0007ClassesDeIsolacaoCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0008ClientsSCDECadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0009ConnectionManagerCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0010CurvasTipicasSalvamentoFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0011FeriadosCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0012FronteirasCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0013GatewaysCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0014MedidoresCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0014MedidoresRetaguardaCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0015PadroesDeCalibracaoCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0016PontosCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0016PontosVirtuaisCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0017PostosTarifariosCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0018RamoDeAtividadeCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0019RegioesCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0020RegrasCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0021SimcardCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0022SubestacaoCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0023SuprimentosRegionaisCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0024TensoesCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0025TrafosBTCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0026UnidadesConsumidorasCadastrosFlow;
import br.com.upperselenium.test.flow.valida_cadastros.T0027UsuariosCadastrosFlow;


@RunWith(Suite.class)

@SuiteClasses({
	T0001AgentesCadastrosFlow.class,
	T0002AlimentadoresCadastrosFlow.class,
	T0003AreasGeograficasCadastrosFlow.class,
	T0004AutoDiscoveryCadastrosFlow.class,
	T0005CidadesCadastrosFlow.class,
	T0006CircuitosCadastrosFlow.class,
	T0007ClassesDeIsolacaoCadastrosFlow.class,
	T0008ClientsSCDECadastrosFlow.class,
	T0009ConnectionManagerCadastrosFlow.class,
	T0010CurvasTipicasSalvamentoFlow.class,
	T0011FeriadosCadastrosFlow.class,
	T0012FronteirasCadastrosFlow.class,
	T0013GatewaysCadastrosFlow.class,
	T0014MedidoresCadastrosFlow.class,
	T0014MedidoresRetaguardaCadastrosFlow.class,
	T0015PadroesDeCalibracaoCadastrosFlow.class,
	T0016PontosCadastrosFlow.class,
	T0016PontosVirtuaisCadastrosFlow.class,
	T0017PostosTarifariosCadastrosFlow.class,
	T0018RamoDeAtividadeCadastrosFlow.class,
	T0019RegioesCadastrosFlow.class,
	T0020RegrasCadastrosFlow.class,
	T0021SimcardCadastrosFlow.class,
	T0022SubestacaoCadastrosFlow.class,
	T0023SuprimentosRegionaisCadastrosFlow.class,
	T0024TensoesCadastrosFlow.class,
	T0025TrafosBTCadastrosFlow.class,
	T0026UnidadesConsumidorasCadastrosFlow.class,
	T0027UsuariosCadastrosFlow.class
	})

@SuiteParams(description="PIM - Validação de Cadastros")
public class S001PIMAreaCadastrosSuite extends BaseSuite {}
