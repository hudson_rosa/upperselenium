package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.agentes.CadastrarAgentesStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.agentes.GoToAgentesStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0001AgentesCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0001-Agentes", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Agentes realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0001AgentesCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToAgentesStage());
		addStage(new CadastrarAgentesStage(getDP("CadastrarAgentesDP.json")));
		//addStage(new EditarAgentesStage(getDP("AlterarAgentesDP.json")));
	}	
}
