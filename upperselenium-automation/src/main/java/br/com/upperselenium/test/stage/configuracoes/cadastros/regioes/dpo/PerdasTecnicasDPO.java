package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class PerdasTecnicasDPO extends BaseHelperDPO {

	private String mesAno;
	private String filterRegiao;
	private String filterPerdaTecnica;
	private String regiao;
	private String mesAnoCol;
	private String perdaTecnica;

	public String getMesAno() {
		return mesAno;
	}

	public void setMesAno(String mesAno) {
		this.mesAno = mesAno;
	}

	public String getFilterRegiao() {
		return filterRegiao;
	}

	public void setFilterRegiao(String filterRegiao) {
		this.filterRegiao = filterRegiao;
	}

	public String getFilterPerdaTecnica() {
		return filterPerdaTecnica;
	}

	public void setFilterPerdaTecnica(String filterPerdaTecnica) {
		this.filterPerdaTecnica = filterPerdaTecnica;
	}

	public String getRegiao() {
		return regiao;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

	public String getMesAnoCol() {
		return mesAnoCol;
	}

	public void setMesAnoCol(String mesAnoCol) {
		this.mesAnoCol = mesAnoCol;
	}

	public String getPerdaTecnica() {
		return perdaTecnica;
	}

	public void setPerdaTecnica(String perdaTecnica) {
		this.perdaTecnica = perdaTecnica;
	}

}