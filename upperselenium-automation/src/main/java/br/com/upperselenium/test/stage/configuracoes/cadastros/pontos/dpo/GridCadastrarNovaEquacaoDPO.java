package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

public class GridCadastrarNovaEquacaoDPO {

	private String identificador;
	private String nome;
	private String quadrante;
	private String cp;

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getQuadrante() {
		return quadrante;
	}

	public void setQuadrante(String quadrante) {
		this.quadrante = quadrante;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

}
