package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class VisualizacaoEquacoesPontosVirtuaisPage extends BaseHelperPage{
	
	private static final String LINK_EQUACOES = ".//div[2]/div/div[2]/ul[2]/li[1]/a[contains(text(),'Equações')]";
	private static final String LINK_NOVA_EQUACAO = "/html/body/div[2]/div/div[2]/a";
	
	// VISUALIZAÇÃO DE EQUAÇÕES
	private static final String GROUP_LABEL_VIGENCIA_EXPRESSAO = ".//div[2]/div/div[2]/fieldset%%/legend"; 
	private static final String GROUP_LABEL_SEQUENCIA_DE_CALCULO = ".//div[2]/div/div[2]/fieldset%%/p"; 
	private static final String GRID_VALUE_IDENTIFICADOR = ".//div[2]/div/div[2]/fieldset%%/table/tbody/tr[%i%]/td[1]"; 
	private static final String GRID_VALUE_PONTO = ".//div[2]/div/div[2]/fieldset%%/table/tbody/tr[%i%]/td[2]"; 
	private static final String GRID_VALUE_QUADRANTE =".//div[2]/div/div[2]/fieldset%%/table/tbody/tr[%i%]/td[3]"; 
	private static final String GRID_VALUE_COMPENSACAO_DE_PERDAS =".//div[2]/div/div[2]/fieldset%%/table/tbody/tr[%i%]/td[4]";	
	private static final String GROUP_BUTTON_EDITAR = ".//div[2]/div/div[2]/fieldset%%/div/a[1]";
	private static final String GROUP_BUTTON_REMOVER = ".//div[2]/div/div[2]/fieldset%%/div/a[2]";
	private static final String GROUP_BUTTON_COPIAR_EM_NOVA_EQUACAO = ".//div[2]/div/div[2]/fieldset%%/div/a[3]";
	
	public void clickEquacoes(){
		clickOnElement(LINK_EQUACOES);
	}
	
	public void clickNovaEquacao(){
		clickOnElement(LINK_NOVA_EQUACAO);
	}
		
	public void getGroupLabelVigenciaExpressao(String value, int index1){
		getGridLabel(GROUP_LABEL_VIGENCIA_EXPRESSAO, value, index1);
	}	
	
	public void getGroupLabelSequenciaDeCalculo(String value, int index1){
		getGridLabel(GROUP_LABEL_SEQUENCIA_DE_CALCULO, value, index1);
	}
	
	public void getGridValueIdentificador(String value, int index1, int index2, int index3){
		getGridLabel(GRID_VALUE_IDENTIFICADOR, value, index1, index2, index3);
	}	
	
	public void getGridValuePonto(String value, int index1, int index2, int index3){
		getGridLabel(GRID_VALUE_PONTO, value, index1, index2, index3);
	}	
	
	public void getGridValueQuadrante(String value, int index1, int index2, int index3){
		getGridLabel(GRID_VALUE_QUADRANTE, value, index1, index2, index3);
	}	
	
	public void getGridValueCompensacaoDePerdas(String value, int index1, int index2, int index3){
		getGridLabel(GRID_VALUE_COMPENSACAO_DE_PERDAS, value, index1, index2, index3);
	}	
	
	public void clickGroupButtonEditar(String value, int index1){
		clickGridButtonOnElement(GROUP_BUTTON_EDITAR, value, index1);
	}
	
	public void clickGroupButtonRemover(String value, int index1){
		clickGridButtonOnElement(GROUP_BUTTON_REMOVER, value, index1);
	}
	
	public void clickGroupButtonCopiarEmNovaEquacao(String value, int index1){
		clickGridButtonOnElement(GROUP_BUTTON_COPIAR_EM_NOVA_EQUACAO, value, index1);
	}

	public void keyPageDown(){
		useKey(GROUP_LABEL_SEQUENCIA_DE_CALCULO, Keys.PAGE_DOWN);
	}	

	public void keyPageUp(){
		useKey(GROUP_BUTTON_EDITAR, Keys.PAGE_UP);
	}	

	public void clickAbaOpcoesDeManobras(){
		clickOnElement(LINK_EQUACOES);
	}
	
}