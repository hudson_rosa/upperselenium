package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.alimentadores.CadastrarAlimentadoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.alimentadores.GoToAlimentadoresStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0002AlimentadoresCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0002-Alimentadores", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Alimentadores realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0002AlimentadoresCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToAlimentadoresStage());
		addStage(new CadastrarAlimentadoresStage(getDP("CadastrarAlimentadoresDP.json")));
	}	
}
