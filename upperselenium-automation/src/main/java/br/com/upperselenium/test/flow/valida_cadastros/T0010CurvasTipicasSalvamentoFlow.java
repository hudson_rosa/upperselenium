package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.curvas_tipicas.GoToCurvasTipicasStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.curvas_tipicas.SalvarCurvasTipicasStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0010CurvasTipicasSalvamentoFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0010-CurvasTipicas", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de salvamento de Curvas Típicas realiza a operação corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0010CurvasTipicasSalvamentoFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToCurvasTipicasStage());
		addStage(new SalvarCurvasTipicasStage(getDP("SalvarCurvasTipicasDP.json")));
	}	
}
