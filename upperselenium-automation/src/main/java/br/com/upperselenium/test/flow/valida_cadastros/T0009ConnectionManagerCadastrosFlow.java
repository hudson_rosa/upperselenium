package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager.CadastrarConnectionManagerStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager.GoToConnectionManagerStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0009ConnectionManagerCadastrosFlow.class)
@FlowParams(idTest = "PPIM-Cadastros-T0009-ConnectionManager", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Connection Manager realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0009ConnectionManagerCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToConnectionManagerStage());
		addStage(new CadastrarConnectionManagerStage(getDP("CadastrarConnectionManagerDP.json")));
	}	
}
