package br.com.upperselenium.test.stage.configuracoes.administracao.pastas.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroPastasPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/form/fieldset/div[3]/input";
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String SELECT_PASTA_PAI = "//*[@id='IdPastaPai']";
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	

	public void typeSelectPastaPai(String value){
		typeSelectComboOption(SELECT_PASTA_PAI, value);
	}
	
	public void keyPageDown(){
		useKey(TEXT_NOME, Keys.PAGE_DOWN);
	}	

	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}