package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.GoToAcompDiarioDeUsinasStage;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.NavegarAcompDiarioDeUsinasStage;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.NavegarImportacaoDeDadosHidrologicosStage;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.NavegarMonitoramentoDeUsinasStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0015AcompDiarioDeUsinasNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0015-AcompDiarioDeUsinas", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Acompanhamento Diário de Usinas validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0015AcompDiarioDeUsinasNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToAcompDiarioDeUsinasStage());
		addStage(new NavegarAcompDiarioDeUsinasStage(getDP("NavegarListagemAcompDiarioDeUsinasDP.json")));
		addStage(new GoToAcompDiarioDeUsinasStage());
		addStage(new NavegarMonitoramentoDeUsinasStage(getDP("NavegarMonitoramentoDeUsinasDP.json")));
		addStage(new NavegarImportacaoDeDadosHidrologicosStage(getDP("NavegarImportacaoDeDadosHidrologicosDP.json")));
	}	
}
