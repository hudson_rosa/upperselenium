package br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class SubestacaoPage extends BaseHelperPage{
	
	private static final String LINK_NOVA = ".//div[2]/div/div[2]/p/a";
				
	public boolean isClickableLinkNova() {
		return isDisplayedElement(LINK_NOVA);
	}
	
	public void clickNova(){
		clickOnElement(LINK_NOVA);
		waitForPageToLoadUntil10s();
	}

}