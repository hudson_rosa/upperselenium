package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.fronteiras.CadastrarFronteirasStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.fronteiras.GoToFronteirasStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0012FronteirasCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0012-Fronteiras", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Fronteiras realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0012FronteirasCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToFronteirasStage());
		addStage(new CadastrarFronteirasStage(getDP("CadastrarFronteirasDP.json")));
	}	
}
