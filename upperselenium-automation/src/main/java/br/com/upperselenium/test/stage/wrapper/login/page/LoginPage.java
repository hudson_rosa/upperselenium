package br.com.upperselenium.test.stage.wrapper.login.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class LoginPage extends BaseHelperPage{
	
	private static final String BUTTON_ENTRAR = ".//div[2]/div/form/div/button";	
	private static final String TEXT_USUARIO = "//*[@id='UserName']";	
	private static final String TEXT_SENHA = "//*[@id='Password']";	
		
	public boolean isClickableButtonEntrar() {
		return isDisplayedElement(BUTTON_ENTRAR);
	}

	public void clickEntrar(){
		clickOnElement(BUTTON_ENTRAR);
	}

	public void typeTextUsuario(String value){
		typeText(TEXT_USUARIO, value);
	}

	public void typeTextSenha(String value){
		typeText(TEXT_SENHA, value);
	}
	
	public String getFilledFieldSenha(){
		return getWebElement(TEXT_SENHA).getText();
	}

	public void getTabFieldUsuario(){
		getWebElement(TEXT_SENHA).sendKeys(Keys.TAB);
	}
}