package br.com.upperselenium.test.flow.pre_condicoes;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMPreCondicoesSuite;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.CadastrarTiposDePontoStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.GoToTiposDePontoStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(P0001TiposDePontoPreCondicoesFlow.class)
@FlowParams(idTest = "PIM-PreCondicoes-T0001-TiposDePonto", testDirPath = "", loginDirPath = "",
	goal = "Verifica a existência de Tipo de Ponto e realiza o cadastro caso registro não seja encontrado.",
	suiteClass = S000PIMPreCondicoesSuite.class)
public class P0001TiposDePontoPreCondicoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP")));
		addStage(new GoToTiposDePontoStage());
		addStage(new CadastrarTiposDePontoStage(getDP("CadastrarTiposDePontoDP")));
	}	
}
