package br.com.upperselenium.test.stage.configuracoes.implantacao.configuracoes_de_tarefas;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.configuracoes_de_tarefas.dpo.NavegarAlterarConfsDeTarefasDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.configuracoes_de_tarefas.page.AlteracaoConfsDeTarefasPage;

public class NavegarAlteracaoConfsDeTarefasStage extends BaseStage {
	private NavegarAlterarConfsDeTarefasDPO navegarAlteracaoConfsDeTarefasDPO;
	private AlteracaoConfsDeTarefasPage alteracaoConfsDeTarefasPage;
	
	public NavegarAlteracaoConfsDeTarefasStage(String dp) {
		navegarAlteracaoConfsDeTarefasDPO = loadDataProviderFile(NavegarAlterarConfsDeTarefasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alteracaoConfsDeTarefasPage = initElementsFromPage(AlteracaoConfsDeTarefasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAlteracaoConfsDeTarefasDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		alteracaoConfsDeTarefasPage.getLabelButtonReagendar(navegarAlteracaoConfsDeTarefasDPO.getButtonReagendar());
		alteracaoConfsDeTarefasPage.getLabelButtonExecutar(navegarAlteracaoConfsDeTarefasDPO.getButtonExecutar());
		alteracaoConfsDeTarefasPage.getLabelButtonHabilitar(navegarAlteracaoConfsDeTarefasDPO.getButtonHabilitar());
		alteracaoConfsDeTarefasPage.getBreadcrumbsText(navegarAlteracaoConfsDeTarefasDPO.getBreadcrumbs());
		alteracaoConfsDeTarefasPage.clickUpToBreadcrumbs(navegarAlteracaoConfsDeTarefasDPO.getUpToBreadcrumb());
		alteracaoConfsDeTarefasPage.getWelcomeTitle(navegarAlteracaoConfsDeTarefasDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
