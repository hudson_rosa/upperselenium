package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo;

public class GridAlterarConstantesDPO {

	private String flagItem;
	private String grandeza;
	private String valorKeConstante;

	public String getFlagItem() {
		return flagItem;
	}

	public void setFlagItem(String flagItem) {
		this.flagItem = flagItem;
	}

	public String getGrandeza() {
		return grandeza;
	}

	public void setGrandeza(String grandeza) {
		this.grandeza = grandeza;
	}

	public String getValorKeConstante() {
		return valorKeConstante;
	}

	public void setValorKeConstante(String valorKeConstante) {
		this.valorKeConstante = valorKeConstante;
	}

}
