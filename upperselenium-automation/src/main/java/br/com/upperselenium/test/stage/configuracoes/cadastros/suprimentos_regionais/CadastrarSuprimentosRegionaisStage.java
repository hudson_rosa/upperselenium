package br.com.upperselenium.test.stage.configuracoes.cadastros.suprimentos_regionais;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.suprimentos_regionais.dpo.CadastrarSuprimentosRegionaisDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.suprimentos_regionais.page.CadastroSuprimentosRegionaisPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.suprimentos_regionais.page.SuprimentosRegionaisPage;

public class CadastrarSuprimentosRegionaisStage extends BaseStage {
	private CadastrarSuprimentosRegionaisDPO cadastrarSuprimentosRegionaisDPO;
	private SuprimentosRegionaisPage suprimentosRegionaisPage;
	private CadastroSuprimentosRegionaisPage cadastroSuprimentosRegionaisPage;
	
	public CadastrarSuprimentosRegionaisStage(String dp) {
		cadastrarSuprimentosRegionaisDPO = loadDataProviderFile(CadastrarSuprimentosRegionaisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		suprimentosRegionaisPage = initElementsFromPage(SuprimentosRegionaisPage.class);
		cadastroSuprimentosRegionaisPage = initElementsFromPage(CadastroSuprimentosRegionaisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarSuprimentosRegionaisDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		suprimentosRegionaisPage.clickNovo();
		cadastroSuprimentosRegionaisPage.typeTextNome(cadastrarSuprimentosRegionaisDPO.getNome()+getRandomStringSpaced());		
		cadastroSuprimentosRegionaisPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroSuprimentosRegionaisPage.validateMessageDefault(cadastrarSuprimentosRegionaisDPO.getOperationMessage());
	}

}
