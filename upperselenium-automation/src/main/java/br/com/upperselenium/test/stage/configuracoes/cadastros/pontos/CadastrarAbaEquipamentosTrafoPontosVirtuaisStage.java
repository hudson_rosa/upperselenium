package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarAbaEquipamentosTrafoPontosVirtuaisDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastrarAbaEquipamentosTrafoPontosVirtuaisPage;

public class CadastrarAbaEquipamentosTrafoPontosVirtuaisStage extends BaseStage {
	private CadastrarAbaEquipamentosTrafoPontosVirtuaisDPO cadastrarTrafoPontosDPO;
	private CadastrarAbaEquipamentosTrafoPontosVirtuaisPage cadastroTrafoPontosPage;
	
	public CadastrarAbaEquipamentosTrafoPontosVirtuaisStage(String dp) {
		cadastrarTrafoPontosDPO = loadDataProviderFile(CadastrarAbaEquipamentosTrafoPontosVirtuaisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroTrafoPontosPage = initElementsFromPage(CadastrarAbaEquipamentosTrafoPontosVirtuaisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarTrafoPontosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();
		cadastroTrafoPontosPage.clickEquipamentos();
		FindElementUtil.acceptAlert();
		cadastroTrafoPontosPage.clickSubmenuTrafo();
		cadastroTrafoPontosPage.typeSelectTipo(cadastrarTrafoPontosDPO.getTipo());		
		cadastroTrafoPontosPage.typeTextAnoDeFabricacao(cadastrarTrafoPontosDPO.getAnoDeFabricacao());		
		cadastroTrafoPontosPage.typeTextDescricao(cadastrarTrafoPontosDPO.getDescricao());		
		cadastroTrafoPontosPage.typeTextNumeroDeSerie(cadastrarTrafoPontosDPO.getNumeroDeSerie()+getRandomStringNonSpaced());		
		cadastroTrafoPontosPage.typeTextCodigoInterno(cadastrarTrafoPontosDPO.getCodigoInterno()+getRandomStringNonSpaced());		
		cadastroTrafoPontosPage.keyPageDown1();		
		cadastroTrafoPontosPage.typeTextPropriedade(cadastrarTrafoPontosDPO.getPropriedade());		
		cadastroTrafoPontosPage.typeTextPotenciaONAN(cadastrarTrafoPontosDPO.getPotenciaONAN());		
		cadastroTrafoPontosPage.typeTextPotenciaONAF(cadastrarTrafoPontosDPO.getPotenciaONAF());		
		cadastroTrafoPontosPage.typeTextPotenciaNominal(cadastrarTrafoPontosDPO.getPotenciaNominal());		
		cadastroTrafoPontosPage.typeTextUtilizacaoONAF(cadastrarTrafoPontosDPO.getUtilizacaoONAF());		
		cadastroTrafoPontosPage.keyPageDown2();		
		cadastroTrafoPontosPage.typeTextTensaoPrimaria(cadastrarTrafoPontosDPO.getTensaoPrimaria());		
		cadastroTrafoPontosPage.typeTextTensaoSecundaria(cadastrarTrafoPontosDPO.getTensaoSecundaria());		
		cadastroTrafoPontosPage.typeTextPerdasNoFerro(cadastrarTrafoPontosDPO.getPerdasNoFerro());		
		cadastroTrafoPontosPage.typeTextPerdasNoCobre(cadastrarTrafoPontosDPO.getPerdasNoCobre());		
		cadastroTrafoPontosPage.typeTextImpedancia(cadastrarTrafoPontosDPO.getImpedancia());		
		cadastroTrafoPontosPage.keyPageDown3();		
		cadastroTrafoPontosPage.typeTextExitacao(cadastrarTrafoPontosDPO.getExitacao());		
		cadastroTrafoPontosPage.typeTextTapAcima(cadastrarTrafoPontosDPO.getTapAcima());		
		cadastroTrafoPontosPage.typeTextTapAbaixo(cadastrarTrafoPontosDPO.getTapAbaixo());		
		cadastroTrafoPontosPage.typeTextMedicaoNaAlta(cadastrarTrafoPontosDPO.getMedicaoNaAlta());		
		cadastroTrafoPontosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroTrafoPontosPage.validateMessageDefault(cadastrarTrafoPontosDPO.getOperationMessage());
	}
	
}
