package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.suprimentos_regionais.CadastrarSuprimentosRegionaisStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.suprimentos_regionais.GoToSuprimentosRegionaisStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0023SuprimentosRegionaisCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0023-SuprimentosRegionais", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Suprimentos Regionais realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0023SuprimentosRegionaisCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToSuprimentosRegionaisStage());
		addStage(new CadastrarSuprimentosRegionaisStage(getDP("CadastrarSuprimentosRegionaisDP.json")));
	}	
}
