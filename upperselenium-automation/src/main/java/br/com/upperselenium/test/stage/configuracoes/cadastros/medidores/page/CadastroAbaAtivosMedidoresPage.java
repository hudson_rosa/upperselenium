package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaAtivosMedidoresPage extends BaseHelperPage{
	
	private static final String LINK_ABA_ATIVOS = ".//div[2]/div/div[2]/ul[2]/li[5]/a";  
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 	
	private static final String TEXT_FABRICACAO = "//*[@id='Fabricacao']";
	private static final String TEXT_INICIO_DA_GARANTIA = "//*[@id='InicioDaGarantia']";	
	private static final String TEXT_FIM_DA_GARANTIA = "//*[@id='TerminoDaGarantia']";
	private static final String TEXT_LACRE = "//*[@id='LacreRastreado']";	
	private static final String TEXT_PROPRIEDADE= "//*[@id='Propriedade']";	
	
	public void typeTextFabricacao(String value){
		typeDatePickerDefault(TEXT_FABRICACAO, value);
	}	
	
	public void typeTextInicioDaGarantia(String value){
		typeDatePickerDefault(TEXT_INICIO_DA_GARANTIA, value);
	}	
	
	public void typeTextFimDaGarantia(String value){
		typeDatePickerDefault(TEXT_FIM_DA_GARANTIA, value);
	}	
	
	public void typeTextLacre(String value){
		typeText(TEXT_LACRE, value);
	}	
	
	public void typeTextPropriedade(String value){
		typeText(TEXT_PROPRIEDADE, value);
	}	
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
		
	public void clickAbaAtivos(){
		clickOnElement(LINK_ABA_ATIVOS);
	}

	public void keyPAgeDown(){
		useKey(TEXT_FABRICACAO, Keys.PAGE_DOWN);
	}	
	
}