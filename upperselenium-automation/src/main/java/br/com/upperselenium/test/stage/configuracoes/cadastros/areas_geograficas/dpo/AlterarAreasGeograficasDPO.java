package br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarAreasGeograficasDPO extends BaseHelperDPO {

	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
