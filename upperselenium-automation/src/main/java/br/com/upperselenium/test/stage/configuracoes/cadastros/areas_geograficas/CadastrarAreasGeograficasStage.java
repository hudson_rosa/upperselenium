package br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas.dpo.CadastrarAreasGeograficasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas.page.AreasGeograficasPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas.page.CadastroAreasGeograficasPage;

public class CadastrarAreasGeograficasStage extends BaseStage {
	private CadastrarAreasGeograficasDPO cadastrarAreasGeograficasDPO;
	private AreasGeograficasPage areasGeograficasPage;
	private CadastroAreasGeograficasPage cadastroAreasGeograficasPage;
	
	public CadastrarAreasGeograficasStage(String dp) {
		cadastrarAreasGeograficasDPO = loadDataProviderFile(CadastrarAreasGeograficasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		areasGeograficasPage = initElementsFromPage(AreasGeograficasPage.class);
		cadastroAreasGeograficasPage = initElementsFromPage(CadastroAreasGeograficasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAreasGeograficasDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		areasGeograficasPage.clickNova();
		if(StringUtil.isNotBlankOrNotNull(cadastrarAreasGeograficasDPO.getNomeRandom())){
			cadastroAreasGeograficasPage.typeTextNomeRandom(cadastrarAreasGeograficasDPO.getNomeRandom()+getRandomStringSpaced());		
		} else {
			cadastroAreasGeograficasPage.typeTextNomeDefault(cadastrarAreasGeograficasDPO.getNomeDefault());		
		}		
		cadastroAreasGeograficasPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAreasGeograficasPage.validateMessageDefault(cadastrarAreasGeograficasDPO.getOperationMessage());
	}

}
