package br.com.upperselenium.test.stage.configuracoes.cadastros.gateways;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.dpo.AlterarAbaMedidoresGatewaysDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.page.AlteracaoAbaMedidoresGatewaysPage;

public class AlterarAbaEnderecoGatewaysStage extends BaseStage {
	private AlterarAbaMedidoresGatewaysDPO alterarAbaMedidoresGatewaysDPO;
	private AlteracaoAbaMedidoresGatewaysPage alteracaoAbaMedidoresGatewaysPage;
	
	public AlterarAbaEnderecoGatewaysStage(String dp) {
		alterarAbaMedidoresGatewaysDPO = loadDataProviderFile(AlterarAbaMedidoresGatewaysDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alteracaoAbaMedidoresGatewaysPage = initElementsFromPage(AlteracaoAbaMedidoresGatewaysPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarAbaMedidoresGatewaysDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoAbaMedidoresGatewaysPage.clickAbaMedidores();
		alteracaoAbaMedidoresGatewaysPage.clickAdicionar();
		alteracaoAbaMedidoresGatewaysPage.typeModalTextSlot(alterarAbaMedidoresGatewaysDPO.getSlot());			
		alteracaoAbaMedidoresGatewaysPage.typeModalTextMedidor(alterarAbaMedidoresGatewaysDPO.getMedidor());	
		alteracaoAbaMedidoresGatewaysPage.keyTab();
		alteracaoAbaMedidoresGatewaysPage.clickModalSalvar();		
	}

	@Override
	public void runValidations() {
		alteracaoAbaMedidoresGatewaysPage.validateMessageDefault(alterarAbaMedidoresGatewaysDPO.getOperationMessage());
	}

}
