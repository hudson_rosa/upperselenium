package br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.dpo;

public class GridAlterarUsuariosDPO {

	private String nome;
	private String login;
	private String perfil;
	private String status;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "GridAlterarUsuariosDPO [nome=" + nome + ", login=" + login + ", perfil=" + perfil + ", status=" + status
				+ "]";
	}

}
