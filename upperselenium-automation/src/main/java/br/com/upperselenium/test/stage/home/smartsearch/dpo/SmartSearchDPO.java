package br.com.upperselenium.test.stage.home.smartsearch.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class SmartSearchDPO extends BaseHelperDPO {

	private String textForSearch;
	private List<ListItensSmartSearchDPO> entitiesList;

	public String getTextForSearch() {
		return textForSearch;
	}

	public void setTextForSearch(String textForSearch) {
		this.textForSearch = textForSearch;
	}

	public List<ListItensSmartSearchDPO> getEntitiesList() {
		return entitiesList;
	}

	public void setEntitiesList(List<ListItensSmartSearchDPO> entitiesList) {
		this.entitiesList = entitiesList;
	}

}