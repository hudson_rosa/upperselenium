package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarMedidoresDPO extends BaseHelperDPO {

	private String nome;
	private String descricao;
	private String numeroInterno;
	private String digitoControle;
	private String tipo;
	private String versaoDoFirmware;
	private String numeroDeSerie;
	private String inicioDaVigencia;
	private String funcao;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNumeroInterno() {
		return numeroInterno;
	}

	public void setNumeroInterno(String numeroInterno) {
		this.numeroInterno = numeroInterno;
	}

	public String getDigitoControle() {
		return digitoControle;
	}

	public void setDigitoControle(String digitoControle) {
		this.digitoControle = digitoControle;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getVersaoDoFirmware() {
		return versaoDoFirmware;
	}

	public void setVersaoDoFirmware(String versaoDoFirmware) {
		this.versaoDoFirmware = versaoDoFirmware;
	}

	public String getNumeroDeSerie() {
		return numeroDeSerie;
	}

	public void setNumeroDeSerie(String numeroDeSerie) {
		this.numeroDeSerie = numeroDeSerie;
	}

	public String getInicioDaVigencia() {
		return inicioDaVigencia;
	}

	public void setInicioDaVigencia(String inicioDaVigencia) {
		this.inicioDaVigencia = inicioDaVigencia;
	}

	public String getFuncao() {
		return funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

}