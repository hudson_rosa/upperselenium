package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarAbaEquipamentosRelePontosVirtuaisDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastrarAbaEquipamentosRelePontosVirtuaisPage;

public class CadastrarAbaEquipamentosRelePontosVirtuaisStage extends BaseStage {
	private CadastrarAbaEquipamentosRelePontosVirtuaisDPO cadastrarRelePontosDPO;
	private CadastrarAbaEquipamentosRelePontosVirtuaisPage cadastroRelePontosPage;
	
	public CadastrarAbaEquipamentosRelePontosVirtuaisStage(String dp) {
		cadastrarRelePontosDPO = loadDataProviderFile(CadastrarAbaEquipamentosRelePontosVirtuaisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroRelePontosPage = initElementsFromPage(CadastrarAbaEquipamentosRelePontosVirtuaisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarRelePontosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();
		cadastroRelePontosPage.clickEquipamentos();
		FindElementUtil.acceptAlert();
		cadastroRelePontosPage.clickSubmenuRele();
		cadastroRelePontosPage.typeSelectTipo(cadastrarRelePontosDPO.getTipo());		
		cadastroRelePontosPage.typeTextNumeroDeSerie(cadastrarRelePontosDPO.getNumeroDeSerie()+getRandomStringNonSpaced());		
		cadastroRelePontosPage.typeTextCorrenteDePickup(cadastrarRelePontosDPO.getCorrenteDePickUp());		
		cadastroRelePontosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroRelePontosPage.validateMessageDefault(cadastrarRelePontosDPO.getOperationMessage());
	}
	
}
