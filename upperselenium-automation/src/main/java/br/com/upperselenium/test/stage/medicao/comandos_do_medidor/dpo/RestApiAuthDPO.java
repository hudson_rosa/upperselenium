package br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class RestApiAuthDPO extends BaseHelperDPO {

	private String uri;
	private String pimAuth;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getPimAuth() {
		return pimAuth;
	}

	public void setPimAuth(String pimAuth) {
		this.pimAuth = pimAuth;
	}

}