package br.com.upperselenium.test.stage.configuracoes.cadastros.feriados.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroFeriadosPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='container-feriado']/div[7]/input";   
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String TEXT_DATA = "//*[@id='Data']";
	private static final String TEXT_DESCRICAO = "//*[@id='Descricao']";
	private static final String SELECT_ESTADO = "//*[@id='IdEstado']";
	private static final String SELECT_CIDADE = "//*[@id='IdCidade']";
	private static final String SELECT_REGIAO = "//*[@id='IdRegiao']";	
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	
	
	public void typeTextData(String value){
		typeDatePickerDefault(TEXT_DATA, value);		
	}
	
	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}
	
	public void typeSelectEstado(String value){		
		typeSelectComboOption(SELECT_ESTADO, value);
	}
	
	public void typeSelectCidade(String value){		
		typeSelectComboOption(SELECT_CIDADE, value);
	}
	
	public void typeSelectRegiao(String value){		
		typeSelectComboOption(SELECT_REGIAO, value);
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}