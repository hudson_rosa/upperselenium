package br.com.upperselenium.test.stage.wrapper.menu_area.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class SideBarImplantacaoPage extends BaseHelperPage{
	
	private static final String LINK_AUTO_DISCOVERY = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Auto-Discovery')]";
	private static final String LINK_CONFIGURACAO_DO_SISTEMA = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Configuração do Sistema')]";
	private static final String LINK_CONFIGURACOES_DE_TAREFAS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Configurações de Tarefas')]";
	private static final String LINK_TIPOS_DE_COMUNICACAO = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Tipos de Comunicação')]";
	private static final String LINK_TIPOS_DE_ALARMES = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Tipos de Alarmes')]";
	private static final String LINK_TIPOS_DE_EQUIPAMENTO = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Tipos de Equipamento')]";
	private static final String LINK_TIPOS_DE_MEDIDOR = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Tipos de Medidor')]";
	private static final String LINK_TIPOS_DE_PONTO = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Tipos de Ponto')]";
	private static final String LINK_TIPOS_DE_PONTO_CCEE = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Tipos de Ponto CCEE')]";
		
	public boolean isClickableLinkAutoDiscovery() {
		return isDisplayedElement(LINK_AUTO_DISCOVERY);
	}

	public boolean isClickableLinkConfiguracoesDoSistema() {
		return isDisplayedElement(LINK_CONFIGURACAO_DO_SISTEMA);
	}

	public boolean isClickableLinkConfiguracoesDeTarefas() {
		return isDisplayedElement(LINK_CONFIGURACOES_DE_TAREFAS);
	}

	public boolean isClickableLinkTiposDeComunicacao() {		
		return isDisplayedElement(LINK_TIPOS_DE_COMUNICACAO);
	}
	
	public boolean isClickableTiposDeAlarmes() {		
		return isDisplayedElement(LINK_TIPOS_DE_ALARMES);
	}	
	
	public boolean isClickableLinkTiposDeEquipamento() {		
		return isDisplayedElement(LINK_TIPOS_DE_EQUIPAMENTO);
	}	
	
	public boolean isClickableLinkTiposDeMedidor() {
		return isDisplayedElement(LINK_TIPOS_DE_MEDIDOR);
	}

	public boolean isClickableLinkTiposDePonto() {
		return isDisplayedElement(LINK_TIPOS_DE_PONTO);
	}
	
	public boolean isClickableLinkTiposDePontoCCEE() {
		return isDisplayedElement(LINK_TIPOS_DE_PONTO_CCEE);
	}
	
	
	public void clickAutoDiscovery(){
		waitForElementToBeClickable(LINK_AUTO_DISCOVERY);
		clickOnElement(LINK_AUTO_DISCOVERY);
	}
	
	public void clickConfiguracaoDoSistema(){
		waitForElementToBeClickable(LINK_CONFIGURACAO_DO_SISTEMA);
		clickOnElement(LINK_CONFIGURACAO_DO_SISTEMA);
	}
	
	public void clickConfiguracoesDeTarefas(){
		waitForElementToBeClickable(LINK_CONFIGURACOES_DE_TAREFAS);
		clickOnElement(LINK_CONFIGURACOES_DE_TAREFAS);
	}
	
	public void clickTiposDeComunicacao(){
		waitForElementToBeClickable(LINK_TIPOS_DE_COMUNICACAO);
		clickOnElement(LINK_TIPOS_DE_COMUNICACAO);
	}
	
	public void clickTiposDeAlarmes(){
		waitForElementToBeClickable(LINK_TIPOS_DE_ALARMES);
		clickOnElement(LINK_TIPOS_DE_ALARMES);
	}
	
	public void clickTiposDeEquipamento(){
		waitForElementToBeClickable(LINK_TIPOS_DE_EQUIPAMENTO);
		clickOnElement(LINK_TIPOS_DE_EQUIPAMENTO);
	}
	
	public void clickTiposDeMedidor(){
		waitForElementToBeClickable(LINK_TIPOS_DE_MEDIDOR);
		clickOnElement(LINK_TIPOS_DE_MEDIDOR);
	}
	
	public void clickTiposDePonto(){
		waitForElementToBeClickable(LINK_TIPOS_DE_PONTO);
		clickOnElement(LINK_TIPOS_DE_PONTO);
	}
	
	public void clickTiposDePontoCCEE(){
		waitForElementToBeClickable(LINK_TIPOS_DE_PONTO_CCEE);
		clickOnElement(LINK_TIPOS_DE_PONTO_CCEE);
	}
	
	public void keyPageDown(){
		useKey(LINK_AUTO_DISCOVERY, Keys.PAGE_DOWN);
	}

}