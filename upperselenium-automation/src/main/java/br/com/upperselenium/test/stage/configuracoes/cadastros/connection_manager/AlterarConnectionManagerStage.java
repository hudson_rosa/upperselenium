package br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.base.util.WaitElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager.dpo.AlterarConnectionManagerDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager.page.AlteracaoConnectionManagerPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager.page.ConnectionManagerPage;

public class AlterarConnectionManagerStage extends BaseStage {
	private AlterarConnectionManagerDPO alterarConnectionManagerDPO;
	private ConnectionManagerPage connectionManagerPage;
	private AlteracaoConnectionManagerPage alteracaoConnectionManagerPage;
	
	public AlterarConnectionManagerStage(String dp) {
		alterarConnectionManagerDPO = loadDataProviderFile(AlterarConnectionManagerDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		connectionManagerPage = initElementsFromPage(ConnectionManagerPage.class);
		alteracaoConnectionManagerPage = initElementsFromPage(AlteracaoConnectionManagerPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarConnectionManagerDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();
		alteracaoConnectionManagerPage.typeTextHub(alterarConnectionManagerDPO.getHub());
		alteracaoConnectionManagerPage.typeCheckBoxHabilitado(alterarConnectionManagerDPO.getHabilitado());
		alteracaoConnectionManagerPage.typeCheckBoxRegistrarConexoes(alterarConnectionManagerDPO.getRegistrarConexoes());
		for (int index=0; index < alterarConnectionManagerDPO.getGridConnectionManager().size(); index++){
			alteracaoConnectionManagerPage.typeGridTextCanal(alterarConnectionManagerDPO.getGridConnectionManager().get(index).getCanal(), index+1);
			alteracaoConnectionManagerPage.typeGridTextIP(alterarConnectionManagerDPO.getGridConnectionManager().get(index).getIp(), index+1);
			alteracaoConnectionManagerPage.typeGridTextPorta(alterarConnectionManagerDPO.getGridConnectionManager().get(index).getPorta(), index+1);
			alteracaoConnectionManagerPage.typeGridTextTimeout(alterarConnectionManagerDPO.getGridConnectionManager().get(index).getTimeout(), index+1);
			alteracaoConnectionManagerPage.typeGridSelectTipo(alterarConnectionManagerDPO.getGridConnectionManager().get(index).getTipo(), index+1);
			alteracaoConnectionManagerPage.typeGridCheckBoxNovaConexaoFechaAntiga(alterarConnectionManagerDPO.getGridConnectionManager().get(index).getNovaConexaoFechaAntiga(), index);
			alteracaoConnectionManagerPage.typeGridCheckBoxReiniciarHubAoFecharAntiga(alterarConnectionManagerDPO.getGridConnectionManager().get(index).getReiniciarHubAoFecharAntiga(), index);
			alteracaoConnectionManagerPage.typeGridCheckBoxAlwaysOn(alterarConnectionManagerDPO.getGridConnectionManager().get(index).getAlwaysOn(), index);
			alteracaoConnectionManagerPage.clickGridAdd(alterarConnectionManagerDPO.getGridConnectionManager().get(index).getButtonAdd(), index+1);
			alteracaoConnectionManagerPage.typeGridSelectPrioridade(alterarConnectionManagerDPO.getGridConnectionManager().get(index).getPrioridade(), index+1);
			alteracaoConnectionManagerPage.clickGridExclude(alterarConnectionManagerDPO.getGridConnectionManager().get(index).getButtonExclude(), index+1);
			WaitElementUtil.waitForATime(TimePRM._3_SECS);
		}
		alteracaoConnectionManagerPage.useKeyPageDown();
		alteracaoConnectionManagerPage.clickSalvar();
		alteracaoConnectionManagerPage.useKeyPageUp();
	}

	@Override
	public void runValidations() {
		alteracaoConnectionManagerPage.validateMessageDefault(alterarConnectionManagerDPO.getOperationMessage());
	}

}
