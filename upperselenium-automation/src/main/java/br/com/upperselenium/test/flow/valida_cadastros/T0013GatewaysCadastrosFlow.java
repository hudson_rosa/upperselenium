package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.CadastrarAbaEnderecoGatewaysStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.CadastrarAbaMedidoresGatewaysStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.CadastrarGatewaysStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.GoToGatewaysStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0013GatewaysCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0013-Gateways", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Gateways realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0013GatewaysCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToGatewaysStage());
		addStage(new CadastrarGatewaysStage(getDP("CadastrarGatewaysNansenDP.json")));
		addStage(new GoToGatewaysStage());
		addStage(new CadastrarGatewaysStage(getDP("CadastrarGatewaysLandisGyrC750DP.json")));
		addStage(new GoToGatewaysStage());
		addStage(new CadastrarGatewaysStage(getDP("CadastrarGatewaysDP.json")));
		addStage(new CadastrarAbaMedidoresGatewaysStage(getDP("CadastrarAbaMedidoresGatewaysDP.json")));
		addStage(new CadastrarAbaEnderecoGatewaysStage(getDP("CadastrarAbaEnderecoGatewaysDP.json")));		
	}	
}
