package br.com.upperselenium.test.stage.medicao.comandos_do_gateway.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class ComandosDoGatewayAgendarPage extends BaseHelperPage{
	
	private static final String LABEL_TIPO = "//*[@id='tree-content']/div[6]/form/fieldset/div[1]/label";
	private static final String LABEL_INSTANTE = "//*[@id='tree-content']/div[6]/form/fieldset/div[2]/label";
	private static final String LABEL_EXPIRACAO = "//*[@id='tree-content']/div[6]/form/fieldset/div[3]/label";
	private static final String LABEL_DIGITE_UM_OU_MAIS_GATEWAYS = "//*[@id='tree-content']/div[6]/form/fieldset/div[4]/label";
	private static final String LABEL_REPETICAO = "//*[@id='controlesRecorrencia']/legend";
	
	public void getLabelTipo(String value){
		getLabel(LABEL_TIPO, value);
	}
	
	public void getLabelInstante(String value){
		getLabel(LABEL_INSTANTE, value);
	}
	
	public void getLabelExpiracao(String value){
		getLabel(LABEL_EXPIRACAO, value);
	}
	
	public void getLabelDigiteUmOuMaisGateways(String value){
		getLabel(LABEL_DIGITE_UM_OU_MAIS_GATEWAYS, value);
	}
	
	public void getLabelRepeticao(String value){
		getLabel(LABEL_REPETICAO, value);
	}

}