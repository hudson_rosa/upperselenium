package br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaMedidoresGatewaysDPO extends BaseHelperDPO {

	private String slot;
	private String medidor;
	private String infoAlert;

	public String getSlot() {
		return slot;
	}

	public void setSlot(String slot) {
		this.slot = slot;
	}

	public String getMedidor() {
		return medidor;
	}

	public void setMedidor(String medidor) {
		this.medidor = medidor;
	}

	public String getInfoAlert() {
		return infoAlert;
	}

	public void setInfoAlert(String infoAlert) {
		this.infoAlert = infoAlert;
	}

}