package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

public class GroupCadastrarNovaDemandaContratadaDPO {

	private String inicio;
	private String limitePonta;
	private String valorPonta;
	private String limiteForaDePonta;
	private String valorForaDePonta;
	private String tipo;

	public String getInicio() {
		return inicio;
	}

	public void setInicio(String inicio) {
		this.inicio = inicio;
	}

	public String getLimitePonta() {
		return limitePonta;
	}

	public void setLimitePonta(String limitePonta) {
		this.limitePonta = limitePonta;
	}

	public String getValorPonta() {
		return valorPonta;
	}

	public void setValorPonta(String valorPonta) {
		this.valorPonta = valorPonta;
	}

	public String getLimiteForaDePonta() {
		return limiteForaDePonta;
	}

	public void setLimiteForaDePonta(String limiteForaDePonta) {
		this.limiteForaDePonta = limiteForaDePonta;
	}

	public String getValorForaDePonta() {
		return valorForaDePonta;
	}

	public void setValorForaDePonta(String valorForaDePonta) {
		this.valorForaDePonta = valorForaDePonta;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
