package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class TiposDeEquipamentoPage extends BaseHelperPage{
	
	private static final String LINK_NOVO = ".//div[2]/div/div[2]/p/a";
	private static final String GRID_BUTTON_EDITAR = "//*[@id='dataTableList']/tbody/tr%%/td[4]/div/a";
	private static final String GRID_LINK_MARCA = "//*[@id='dataTableList']/tbody/tr[1]/td[1]/a";
	private static final String LABEL_HEADER_MARCA = "//*[@id='dataTableList']/thead/tr[2]/th[contains(text(),'Marca')]";	
	private static final String LABEL_HEADER_MODELO = "//*[@id='dataTableList']/thead/tr[2]/th[contains(text(),'Modelo')]";	
	private static final String LABEL_HEADER_ESPECIE = "//*[@id='dataTableList']/thead/tr[2]/th[contains(text(),'Espécie')]";	
		
	public void clickNovo(){
		clickOnElement(LINK_NOVO);
		waitForPageToLoadUntil10s();
	}
	
	public void clickGridButtonFirstLineEditar(String value){
		waitForPageToLoadUntil10s();
		clickGridButtonOnElement(GRID_BUTTON_EDITAR, value, 1);
	}
	
	public void clickGridFirstLineLinkMarca(String value){
		waitForPageToLoadUntil10s();
		clickGridButtonOnElement(GRID_LINK_MARCA, value, 1);
	}	
	
	public void getLabelHeaderMarca(String value){
		getLabel(LABEL_HEADER_MARCA, value);
	}
	
	public void getLabelHeaderModelo(String value){
		getLabel(LABEL_HEADER_MODELO, value);
	}
	
	public void getLabelHeaderEspecie(String value){
		getLabel(LABEL_HEADER_ESPECIE, value);
	}
	
}