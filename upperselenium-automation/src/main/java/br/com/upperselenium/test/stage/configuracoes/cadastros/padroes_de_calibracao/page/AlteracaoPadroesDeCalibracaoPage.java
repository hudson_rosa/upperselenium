package br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoPadroesDeCalibracaoPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/padroes-de-calibracao/div/form-with-validation/form/button";   
	private static final String TEXT_IDENTIFICACAO = "//*[@id='input-identificacao']";
	private static final String TEXT_MODELO = "//*[@id='input-modelo']";
	private static final String TEXT_NUMERO_DE_SERIE = "//*[@id='input-numero-de-serie']";
	private static final String TEXT_NUMERO_DO_CERTIFICADO_DE_CALIBRACAO = "//*[@id='input-numero-certificado']";
	private static final String TEXT_CLASSE = ".//div[2]/div/div[2]/padroes-de-calibracao/div/form-with-validation/form/text-input[5]/div/div/input";
	private static final String TEXT_DATA_DE_VALIDADE = ".//div[2]/div/div[2]/padroes-de-calibracao/div/form-with-validation/form/div/date-picker/div/div/input";	
	
	private static final String TEXT_CARGA_NOMINAL_ENERGIA_ATIVA_FORNECIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[1]/td[2]/div/input";
	private static final String TEXT_CARGA_NOMINAL_ENERGIA_ATIVA_RECEBIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[2]/td[2]/div/input";
	private static final String TEXT_CARGA_NOMINAL_ENERGIA_REATIVA_FORNECIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[3]/td[2]/div/input";
	private static final String TEXT_CARGA_NOMINAL_ENERGIA_REATIVA_RECEBIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[4]/td[2]/div/input";

	private static final String TEXT_CARGA_INDUTIVA_ENERGIA_ATIVA_FORNECIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[1]/td[3]/div/input";
	private static final String TEXT_CARGA_INDUTIVA_ENERGIA_ATIVA_RECEBIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[2]/td[3]/div/input";
	private static final String TEXT_CARGA_INDUTIVA_ENERGIA_REATIVA_FORNECIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[3]/td[3]/div/input";
	private static final String TEXT_CARGA_INDUTIVA_ENERGIA_REATIVA_RECEBIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[4]/td[3]/div/input";
	
	private static final String TEXT_CARGA_CAPACITIVA_ENERGIA_ATIVA_FORNECIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[1]/td[4]/div/input";
	private static final String TEXT_CARGA_CAPACITIVA_ENERGIA_ATIVA_RECEBIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[2]/td[4]/div/input";
	private static final String TEXT_CARGA_CAPACITIVA_ENERGIA_REATIVA_FORNECIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[3]/td[4]/div/input";
	private static final String TEXT_CARGA_CAPACITIVA_ENERGIA_REATIVA_RECEBIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[4]/td[4]/div/input";
	
	private static final String TEXT_CARGA_PEQUENA_ENERGIA_ATIVA_FORNECIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[1]/td[5]/div/input";
	private static final String TEXT_CARGA_PEQUENA_ENERGIA_ATIVA_RECEBIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[2]/td[5]/div/input";
	private static final String TEXT_CARGA_PEQUENA_ENERGIA_REATIVA_FORNECIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[3]/td[5]/div/input";
	private static final String TEXT_CARGA_PEQUENA_ENERGIA_REATIVA_RECEBIDA = ".//padroes-de-calibracao/div/form-with-validation/form/table/tbody/tr[4]/td[5]/div/input";
		
	public void typeTextIdentificacao(String value){
		typeText(TEXT_IDENTIFICACAO, value);
	}	
	
	public void typeTextModelo(String value){
		typeText(TEXT_MODELO, value);
	}	
	
	public void typeTextNumeroDeSerie(String value){
		typeText(TEXT_NUMERO_DE_SERIE, value);
	}	
	
	public void typeTextNumeroDoCertificadoDeCalibracao(String value){
		typeText(TEXT_NUMERO_DO_CERTIFICADO_DE_CALIBRACAO, value);
	}	
	
	public void typeTextClasse(String value){
		typeText(TEXT_CLASSE, value);
	}	

	public void typeTextDataDeValidade(String value){
		typeDatePickerDefault(TEXT_DATA_DE_VALIDADE, value);		
	}
	
	public void typeTextCargaNominalEnergiaAtivaFornecida(String value){
		typeText(TEXT_CARGA_NOMINAL_ENERGIA_ATIVA_FORNECIDA, value);
	}	
	
	public void typeTextCargaNominalEnergiaAtivaRecebida(String value){
		typeText(TEXT_CARGA_NOMINAL_ENERGIA_ATIVA_RECEBIDA, value);
	}	
	
	public void typeTextCargaNominalEnergiaReativaFornecida(String value){
		typeText(TEXT_CARGA_NOMINAL_ENERGIA_REATIVA_FORNECIDA, value);
	}	
	
	public void typeTextCargaNominalEnergiaReativaRecebida(String value){
		typeText(TEXT_CARGA_NOMINAL_ENERGIA_REATIVA_RECEBIDA, value);
	}	
	
	public void typeTextCargaIndutivaEnergiaAtivaFornecida(String value){
		typeText(TEXT_CARGA_INDUTIVA_ENERGIA_ATIVA_FORNECIDA, value);
	}	
	
	public void typeTextCargaIndutivaEnergiaAtivaRecebida(String value){
		typeText(TEXT_CARGA_INDUTIVA_ENERGIA_ATIVA_RECEBIDA, value);
	}	
	
	public void typeTextCargaIndutivaEnergiaReativaFornecida(String value){
		typeText(TEXT_CARGA_INDUTIVA_ENERGIA_REATIVA_FORNECIDA, value);
	}	
	
	public void typeTextCargaIndutivaEnergiaReativaRecebida(String value){
		typeText(TEXT_CARGA_INDUTIVA_ENERGIA_REATIVA_RECEBIDA, value);
	}	
		
	public void typeTextCargaCapacitivaEnergiaAtivaFornecida(String value){
		typeText(TEXT_CARGA_CAPACITIVA_ENERGIA_ATIVA_FORNECIDA, value);
	}	
	
	public void typeTextCargaCapacitivaEnergiaAtivaRecebida(String value){
		typeText(TEXT_CARGA_CAPACITIVA_ENERGIA_ATIVA_RECEBIDA, value);
	}	
	
	public void typeTextCargaCapacitivaEnergiaReativaFornecida(String value){
		typeText(TEXT_CARGA_CAPACITIVA_ENERGIA_REATIVA_FORNECIDA, value);
	}	
	
	public void typeTextCargaCapacitivaEnergiaReativaRecebida(String value){
		typeText(TEXT_CARGA_CAPACITIVA_ENERGIA_REATIVA_RECEBIDA, value);
	}	
		
	public void typeTextCargaPequenaEnergiaAtivaFornecida(String value){
		typeText(TEXT_CARGA_PEQUENA_ENERGIA_ATIVA_FORNECIDA, value);
	}	
	
	public void typeTextCargaPequenaEnergiaAtivaRecebida(String value){
		typeText(TEXT_CARGA_PEQUENA_ENERGIA_ATIVA_RECEBIDA, value);
	}	
	
	public void typeTextCargaPequenaEnergiaReativaFornecida(String value){
		typeText(TEXT_CARGA_PEQUENA_ENERGIA_REATIVA_FORNECIDA, value);
	}	
	
	public void typeTextCargaPequenaEnergiaReativaRecebida(String value){
		typeText(TEXT_CARGA_PEQUENA_ENERGIA_REATIVA_RECEBIDA, value);
	}	
			
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}