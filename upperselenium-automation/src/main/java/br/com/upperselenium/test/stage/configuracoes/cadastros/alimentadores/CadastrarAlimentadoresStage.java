package br.com.upperselenium.test.stage.configuracoes.cadastros.alimentadores;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.alimentadores.dpo.CadastrarAlimentadoresDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.alimentadores.page.AlimentadoresPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.alimentadores.page.CadastroAlimentadoresPage;

public class CadastrarAlimentadoresStage extends BaseStage {
	private CadastrarAlimentadoresDPO cadastrarAlimentadoresDPO;
	private AlimentadoresPage alimentadoresPage;
	private CadastroAlimentadoresPage cadastroAlimentadoresPage;
	
	public CadastrarAlimentadoresStage(String dp) {
		cadastrarAlimentadoresDPO = loadDataProviderFile(CadastrarAlimentadoresDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alimentadoresPage = initElementsFromPage(AlimentadoresPage.class);
		cadastroAlimentadoresPage = initElementsFromPage(CadastroAlimentadoresPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAlimentadoresDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		alimentadoresPage.clickNovo();				
		cadastroAlimentadoresPage.typeTextNome(cadastrarAlimentadoresDPO.getNome()+getRandomStringSpaced());
		cadastroAlimentadoresPage.typeTextPontoDeMedicao(cadastrarAlimentadoresDPO.getPontoDeMedicao());
		cadastroAlimentadoresPage.typeTextPseudonimoPerdasTecnicas(cadastrarAlimentadoresDPO.getPseudonimoPerdasTecnicas()+getRandomStringNonSpaced());
		cadastroAlimentadoresPage.typeTextPseudonimoConsumo(cadastrarAlimentadoresDPO.getPseudonimoConsumo()+getRandomStringNonSpaced());
		cadastroAlimentadoresPage.typeTextPontoDeConsumoAdicional(cadastrarAlimentadoresDPO.getPontoDeConsumoAdicional());
		cadastroAlimentadoresPage.typeSelectRegiao(cadastrarAlimentadoresDPO.getRegiao());
		cadastroAlimentadoresPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAlimentadoresPage.validateMessageDefault(cadastrarAlimentadoresDPO.getOperationMessage());
	}

}
