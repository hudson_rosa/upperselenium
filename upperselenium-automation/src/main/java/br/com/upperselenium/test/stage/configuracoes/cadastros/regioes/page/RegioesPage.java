package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class RegioesPage extends BaseHelperPage{
	
	private static final String LINK_NOVA = ".//div[2]/div/div[2]/ul[2]/li[1]/a";
	private static final String LINK_PERDAS_TECNICAS = ".//div[2]/div/div[2]/ul[2]/li[2]/a";
				
	public boolean isClickableLinkNova() {
		return isDisplayedElement(LINK_NOVA);
	}
	
	public void clickNova(){
		clickOnElement(LINK_NOVA);
		waitForPageToLoadUntil10s();
	}
	
	public boolean isClickableLinkPerdasTecnicas() {
		return isDisplayedElement(LINK_PERDAS_TECNICAS);
	}
	
	public void clickPerdasTecnicas(){
		clickOnElement(LINK_PERDAS_TECNICAS);
		waitForPageToLoadUntil10s();
	}

}