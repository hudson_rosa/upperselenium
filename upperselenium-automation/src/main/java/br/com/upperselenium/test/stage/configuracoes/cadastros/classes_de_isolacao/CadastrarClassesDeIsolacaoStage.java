package br.com.upperselenium.test.stage.configuracoes.cadastros.classes_de_isolacao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.classes_de_isolacao.dpo.CadastrarClassesDeIsolacaoDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.classes_de_isolacao.page.CadastroClassesDeIsolacaoPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.classes_de_isolacao.page.ClassesDeIsolacaoPage;

public class CadastrarClassesDeIsolacaoStage extends BaseStage {
	private CadastrarClassesDeIsolacaoDPO cadastrarClassesDeIsolacaoDPO;
	private ClassesDeIsolacaoPage classesDeIsolacaoPage;
	private CadastroClassesDeIsolacaoPage cadastroClassesDeIsolacaoPage;
	
	public CadastrarClassesDeIsolacaoStage(String dp) {
		cadastrarClassesDeIsolacaoDPO = loadDataProviderFile(CadastrarClassesDeIsolacaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		classesDeIsolacaoPage = initElementsFromPage(ClassesDeIsolacaoPage.class);
		cadastroClassesDeIsolacaoPage = initElementsFromPage(CadastroClassesDeIsolacaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarClassesDeIsolacaoDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		classesDeIsolacaoPage.clickNovo();
		cadastroClassesDeIsolacaoPage.typeTextValor(cadastrarClassesDeIsolacaoDPO.getValor()+getRandomNumber());
		cadastroClassesDeIsolacaoPage.typeTextDescricao(cadastrarClassesDeIsolacaoDPO.getDescricao()+getRandomStringSpaced());
		cadastroClassesDeIsolacaoPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroClassesDeIsolacaoPage.validateMessageDefault(cadastrarClassesDeIsolacaoDPO.getOperationMessage());
	}

}
