package br.com.upperselenium.test.stage.configuracoes.implantacao.configuracoes_de_tarefas.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoConfsDeTarefasPage extends BaseHelperPage{
	
	private static final String BUTTON_REAGENDAR = ".//div[2]/div/div[2]/form[1]/div/div/div/input[2]";	
	private static final String BUTTON_EXECUTAR = ".//div[2]/div/div[2]/form[1]/div/div/div/input[3]";
	private static final String BUTTON_HABILITAR = ".//div[2]/div/div[2]/form[1]/div/div/div/input[4]";
		
	public void getLabelButtonReagendar(String value){
		getLabel(BUTTON_REAGENDAR, value);
	}
	
	public void getLabelButtonExecutar(String value){
		getLabel(BUTTON_EXECUTAR, value);
	}
	
	public void getLabelButtonHabilitar(String value){
		getLabel(BUTTON_HABILITAR, value);
	}
	
}