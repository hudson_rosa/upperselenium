package br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarUsuariosDPO extends BaseHelperDPO {

	private String exportacaoExcel;

	public String getExportacaoExcel() {
		return exportacaoExcel;
	}

	public void setExportacaoExcel(String exportacaoExcel) {
		this.exportacaoExcel = exportacaoExcel;
	}

}