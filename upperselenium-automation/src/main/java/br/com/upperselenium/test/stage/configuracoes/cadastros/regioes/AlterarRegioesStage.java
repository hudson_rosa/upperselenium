package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.dpo.AlterarRegioesDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.page.AlteracaoRegioesPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.page.RegioesPage;

public class AlterarRegioesStage extends BaseStage {
	private AlterarRegioesDPO alterarRegioesDPO;
	private RegioesPage regioesPage;
	private AlteracaoRegioesPage alteracaoRegioesPage;
	
	public AlterarRegioesStage(String dp) {
		alterarRegioesDPO = loadDataProviderFile(AlterarRegioesDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		regioesPage = initElementsFromPage(RegioesPage.class);
		alteracaoRegioesPage = initElementsFromPage(AlteracaoRegioesPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarRegioesDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoRegioesPage.typeTextNome(alterarRegioesDPO.getNome()+getRandomStringSpaced());		
		alteracaoRegioesPage.typeTextDescricao(alterarRegioesDPO.getDescricao()+getRandomStringSpaced());		
		alteracaoRegioesPage.typeSelectAreaGeografica(alterarRegioesDPO.getAreaGeografica());		
		alteracaoRegioesPage.typeTextPontoDeEnergiaRequerida(alterarRegioesDPO.getPontoDeEnergiaRequerida());		
		alteracaoRegioesPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoRegioesPage.validateMessageDefault(alterarRegioesDPO.getOperationMessage());
	}

}
