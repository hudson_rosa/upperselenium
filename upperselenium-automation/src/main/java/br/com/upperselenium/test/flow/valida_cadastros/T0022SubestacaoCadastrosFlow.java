package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.CadastrarAbaContatoSubestacaoStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.CadastrarAbaEnderecoSubestacaoStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.CadastrarSubestacaoStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.GoToSubestacaoStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0022SubestacaoCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0022-Subestacao", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Subestação realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0022SubestacaoCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToSubestacaoStage());
		addStage(new CadastrarSubestacaoStage(getDP("CadastrarSubestacaoDP.json")));
		addStage(new CadastrarAbaEnderecoSubestacaoStage(getDP("CadastrarAbaEnderecoSubestacaoDP.json")));
		addStage(new CadastrarAbaContatoSubestacaoStage(getDP("CadastrarAbaContatoSubestacaoDP.json")));
	}	
}
