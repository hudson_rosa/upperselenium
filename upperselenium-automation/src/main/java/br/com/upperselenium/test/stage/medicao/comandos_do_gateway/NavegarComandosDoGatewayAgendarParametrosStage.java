package br.com.upperselenium.test.stage.medicao.comandos_do_gateway;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.dpo.NavegarComandosDoGatewayAgendarParametrosDPO;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.page.ComandosDoGatewayAgendarParametrosPage;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.page.ComandosDoGatewayPage;

public class NavegarComandosDoGatewayAgendarParametrosStage extends BaseStage {
	private NavegarComandosDoGatewayAgendarParametrosDPO navegarComandosDoGatewayAgendarParamDPO;
	private ComandosDoGatewayPage comandosDoGatewayPage;	
	private ComandosDoGatewayAgendarParametrosPage comandosDoGatewayAgendarParamPage;	
	
	public NavegarComandosDoGatewayAgendarParametrosStage(String dp) {
		navegarComandosDoGatewayAgendarParamDPO = loadDataProviderFile(NavegarComandosDoGatewayAgendarParametrosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		comandosDoGatewayPage = initElementsFromPage(ComandosDoGatewayPage.class);
		comandosDoGatewayAgendarParamPage = initElementsFromPage(ComandosDoGatewayAgendarParametrosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarComandosDoGatewayAgendarParamDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		comandosDoGatewayPage.clickLinkAgendarPorParametros();
		comandosDoGatewayAgendarParamPage.getLabelTipo(navegarComandosDoGatewayAgendarParamDPO.getLabelTipo());
		comandosDoGatewayAgendarParamPage.getLabelInstante(navegarComandosDoGatewayAgendarParamDPO.getLabelInstante());
		comandosDoGatewayAgendarParamPage.getLabelExpiracao(navegarComandosDoGatewayAgendarParamDPO.getLabelExpiracao());
		comandosDoGatewayAgendarParamPage.getLabelDigiteUmOuMaisTags(navegarComandosDoGatewayAgendarParamDPO.getLabelDigiteUmOuMaisTags());		
		comandosDoGatewayAgendarParamPage.getLabelRepeticao(navegarComandosDoGatewayAgendarParamDPO.getLabelRepeticao());		
		comandosDoGatewayAgendarParamPage.getBreadcrumbsText(navegarComandosDoGatewayAgendarParamDPO.getBreadcrumbs());
		comandosDoGatewayAgendarParamPage.clickUpToBreadcrumbs(navegarComandosDoGatewayAgendarParamDPO.getUpToBreadcrumb());
		comandosDoGatewayAgendarParamPage.getWelcomeTitle(navegarComandosDoGatewayAgendarParamDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
