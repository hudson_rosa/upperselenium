package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaComunicacaoMedidoresPage extends BaseHelperPage{
	
	private static final String LINK_ABA_COMUNICACAO = ".//div[2]/div/div[2]/ul[2]/li[3]/a";  
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 
	private static final String SELECT_COMUNICACAO = "//*[@id='medidor-comunicacao']/form/div[1]/div/select";
	private static final String CHECK_HABILITAR = "//*[@id='HabilitarComunicacao']";
	private static final String TEXT_PROTOCOLO = "//*[@id='ProtocoloComunicacao']";

	//ABS SSU Modbus - Ethernet
	private static final String ABS_SSU_ENDERECO_OU_IP = "//*[@id='229']";
	private static final String ABS_SSU_PORTA = "//*[@id='230']";
	private static final String ABS_SSU_ID = "//*[@id='231']";

	//AMI Cylec *

	//ANSI-C12
	private static final String ANSI_C12_ENDERECO_OU_IP = "//*[@id='118']";
	private static final String ANSI_C12_PORTA = "//*[@id='119']";
	private static final String ANSI_C12_MODELO_DE_MEDIDOR = "//*[@id='120']";

	//ApiConstruserv
	private static final String CONSTRUSERV_IDENTIFICADOR_DA_ESTACAO = "//*[@id='242']";

	//ArquivosAmmonit
	private static final String AMMONIT_VEL_VENTO_MEDIA_ANEM_SUPERIOR = "//*[@id='194']";
	private static final String AMMONIT_VEL_VENTO_MAXIMA_ANEM_SUPERIOR = "//*[@id='195']";
	private static final String AMMONIT_VEL_VENTO_MINIMA_ANEM_SUPERIOR = "//*[@id='196']";
	private static final String AMMONIT_VEL_VENTO_DESVIO_PADRAO_ANEM_SUPERIOR = "//*[@id='197']";
	private static final String AMMONIT_VEL_VENTO_MEDIA_ANEM_2 = "//*[@id='198']";
	private static final String AMMONIT_VEL_VENTO_MAXIMA_ANEM_2 = "//*[@id='199']";
	private static final String AMMONIT_VEL_VENTO_MINIMA_ANEM_2 = "//*[@id='200']";
	private static final String AMMONIT_VEL_VENTO_DESVIO_PADRAO_ANEM_2 = "//*[@id='201']";
	private static final String AMMONIT_VEL_VENTO_MEDIA_ANEM_3 = "//*[@id='202']";
	private static final String AMMONIT_VEL_VENTO_MAXIMA_ANEM_3 = "//*[@id='203']";
	private static final String AMMONIT_VEL_VENTO_MINIMA_ANEM_3 = "//*[@id='204']";
	private static final String AMMONIT_VEL_VENTO_DESVIO_PADRAO_ANEM_3 = "//*[@id='205']";
	private static final String AMMONIT_DIR_VENTO_MEDIA_WIND_VANE_SUPERIOR = "//*[@id='206']";
	private static final String AMMONIT_DIR_VENTO_DESVIO_PADRAO_WIND_VANE_SUPERIOR = "//*[@id='207']";
	private static final String AMMONIT_DIR_VENTO_MEDIA_WIND_VANE_2 = "//*[@id='208']";
	private static final String AMMONIT_DIR_VENTO_DESVIO_PADRAO_WIND_VANE_2 = "//*[@id='209']";
	private static final String AMMONIT_PRESSAO_DO_AR = "//*[@id='210']";
	private static final String AMMONIT_TEMPERATURA_DO_AR = "//*[@id='211']";
	private static final String AMMONIT_UMIDADE_RELATIVA_DO_AR = "//*[@id='212']";

	//ArquivosNOMAD
	private static final String NOMAD_VEL_VENTO_MEDIA_ANEM_SUPERIOR = "//*[@id='133']";
	private static final String NOMAD_VEL_VENTO_MAXIMA_ANEM_SUPERIOR = "//*[@id='134']";
	private static final String NOMAD_VEL_VENTO_MINIMA_ANEM_SUPERIOR = "//*[@id='135']";
	private static final String NOMAD_VEL_VENTO_DESVIO_PADRAO_ANEM_SUPERIOR = "//*[@id='136']";
	private static final String NOMAD_VEL_VENTO_MEDIA_ANEM_2 = "//*[@id='137']";
	private static final String NOMAD_VEL_VENTO_MAXIMA_ANEM_2 = "//*[@id='138']";
	private static final String NOMAD_VEL_VENTO_MINIMA_ANEM_2 = "//*[@id='139']";
	private static final String NOMAD_VEL_VENTO_DESVIO_PADRAO_ANEM_2 = "//*[@id='140']";
	private static final String NOMAD_VEL_VENTO_MEDIA_ANEM_3 = "//*[@id='141']";
	private static final String NOMAD_VEL_VENTO_MAXIMA_ANEM_3 = "//*[@id='142']";
	private static final String NOMAD_VEL_VENTO_MINIMA_ANEM_3 = "//*[@id='143']";
	private static final String NOMAD_VEL_VENTO_DESVIO_PADRAO_ANEM_3 = "//*[@id='144']";
	private static final String NOMAD_DIR_VENTO_MEDIA_WIND_VANE_SUPERIOR = "//*[@id='145']";
	private static final String NOMAD_DIR_VENTO_DESVIO_PADRAO_WIND_VANE_SUPERIOR = "//*[@id='146']";
	private static final String NOMAD_DIR_VENTO_MEDIA_WIND_VANE_2 = "//*[@id='147']";
	private static final String NOMAD_DIR_VENTO_DESVIO_PADRAO_WIND_VANE_2 = "//*[@id='148']";
	private static final String NOMAD_PRESSAO_DO_AR = "//*[@id='149']";
	private static final String NOMAD_TEMPERATURA_DO_AR = "//*[@id='150']";
	private static final String NOMAD_UMIDADE_RELATIVA_DO_AR = "//*[@id='151']";

	//Bancada virtual Way2
	private static final String WAY2_ENDERECO_OU_IP = "//*[@id='180']";
	private static final String WAY2_PORTA = "//*[@id='181']";
	private static final String WAY2_ID = "//*[@id='182']";

	//CCK
	private static final String CCK_STR_REMOTO = "//*[@id='37']";

	//CCK 7550S - Ethernet
	private static final String CCK7550_ETH_ENDERECO_OU_IP = "//*[@id='239']";
	private static final String CCK7550_ETH_PORTA = "//*[@id='240']";
	private static final String CCK7550_ETH_ID = "//*[@id='241']";

	//CD103
	private static final String CD103_GATEWAY = "//*[@id='155']";

	//Coletor Titan
	private static final String TITAN_CAIXA = "//*[@id='98']";

	//C750
	private static final String C750_GATEWAY = "//*[@id='156']";
	
	//ELO 2113 - ABNT Multiponto Serial Ethernet
	private static final String ELO2113_MPS_ETH_ENDERECO_OU_IP = "//*[@id='222']";
	private static final String ELO2113_MPS_ETH_PORTA = "//*[@id='223']";
	
	//ELO 2180 - Ethernet
	private static final String ELO2180_ETH_ENDERECO_OU_IP = "//*[@id='66']";
	private static final String ELO2180_ETH_PORTA = "//*[@id='67']";
	private static final String ELO2180_ETH_ID = "//*[@id='68']";
	
	//ELO 2180 - V2.xx - Ethernet
	private static final String ELO2180_V2XX_ENDERECO_OU_IP = "//*[@id='75']";
	private static final String ELO2180_V2XX_PORTA = "//*[@id='76']";
	private static final String ELO2180_V2XX_ID = "//*[@id='77']";
	
	//Elster A3RBR - Monponto
	private static final String ELSTER_A3RBR_ENDERECO_OU_IP = "//*[@id='227']";
	private static final String ELSTER_A3RBR_PORTA = "//*[@id='228']";
	
	//E650 - Monoponto
	private static final String E650_MP_ENDERECO_OU_IP = "//*[@id='218']";
	private static final String E650_MP_PORTA = "//*[@id='219']";
	
	//E750 - Ethernet
	private static final String E750_ETH_ENDERECO_OU_IP = "//*[@id='213']";
	private static final String E750_ETH_PORTA = "//*[@id='214']";
	private static final String E750_ETH_ID = "//*[@id='215']";
	
	//E750 - Monoponto
	private static final String E750_MP_ENDERECO_OU_IP = "//*[@id='220']";
	private static final String E750_MP_PORTA = "//*[@id='221']";
	
	//Garnet Elster *
	
	//Gateway Reader
	private static final String READER_GATEWAY = "//*[@id='95']";
	
	//Hidrometrologia Skywave *
	
	//Integração Banco de Dados Automação
	private static final String BD_AUTOMACAO_IDENTIFICADOR_REMOTO = "//*[@id='97']";
	
	//ION Dial-Up
	private static final String ION_DIAL_UP_TELEFONE = "//*[@id='7']";
	private static final String ION_DIAL_UP_ID = "//*[@id='8']";
	
	//ION - Ethergate ou Terminal Server
	private static final String ION_ETG_TERM_ENDERECO_OU_IP = "//*[@id='1']";
	private static final String ION_ETG_TERM_PORTA = "//*[@id='2']";
	private static final String ION_ETG_TERM_ID = "//*[@id='3']";
	private static final String ION_ETG_TERM_COLETAR_FORMAS_ONDA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[4]/label/input";
	
	//ION - Ethernet
	private static final String ION_ETH_ENDERECO_OU_IP = "//*[@id='5']";
	private static final String ION_ETH_PORTA = "//*[@id='19']";
	private static final String ION_ETH_ID = "//*[@id='20']";
	private static final String ION_ETH_COLETAR_FORMAS_ONDA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[4]/label/input";
	
	//ION - GPRS
	private static final String ION_GPRS_ENDERECO_OU_IP = "//*[@id='13']";
	private static final String ION_GPRS_PORTA = "//*[@id='14']";
	private static final String ION_GPRS_ID = "//*[@id='15']";
	private static final String ION_GPRS_COLETAR_FORMAS_ONDA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[4]/label/input";
	
	//ION - Serial
	private static final String ION_SERIAL_ENDERECO_OU_IP = "//*[@id='17']";
	private static final String ION_SERIAL_ID = "//*[@id='18']";
	
	//ION-Ethernet-E3-RT
	private static final String ION_ETH_E3_RT_ENDERECO_OU_IP = "//*[@id='124']";
	private static final String ION_ETH_E3_RT_PORTA = "//*[@id='126']";
	private static final String ION_ETH_E3_RT_ID = "//*[@id='127']";
	private static final String ION_ETH_E3_RT_COLETAR_FORMAS_ONDA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[4]/label/input";
	 
	//ION7650 - Ethernet
	private static final String ION7650_ETH_ENDERECO_OU_IP = "//*[@id='78']";
	private static final String ION7650_ETH_PORTA = "//*[@id='79']";
	private static final String ION7650_ETH_ID = "//*[@id='80']";
	private static final String ION7650_ETH_COLETAR_FORMAS_ONDA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[4]/label/input";

	//iTech UTW *

	//Landis E34 - ABNT Multiponto Serial Ethernet
	private static final String LANDIS_E34_MP_ETH_ENDERECO_OU_IP = "//*[@id='192']";
	private static final String LANDIS_E34_MP_ETH_PORTA = "//*[@id='193']";

	//M2M
	private static final String M2M_ID = "//*[@id='183']";

	//Nansen K Art - ABNT Multiponto Serial Ethernet
	private static final String NANSEN_K_ART_ENDERECO_OU_IP = "//*[@id='188']";
	private static final String NANSEN_K_ART_PORTA = "//*[@id='189']";

	//Nansen Spectrun SX-ABNT Multiponto Serial Ethernet
	private static final String NANSEN_SPECTRUN_SX_ENDERECO_OU_IP = "//*[@id='190']";
	private static final String NANSEN_SPECTRUN_SX_PORTA = "//*[@id='191']";

	//NEXUS - Dial-Up
	private static final String NEXUS_DIALUP_TELEFONE = "//*[@id='87']";
	private static final String NEXUS_DIALUP_ID = "//*[@id='85']";
	private static final String NEXUS_DIALUP_USUARIO_PARA_COMUNICACAO = "//*[@id='164']";
	private static final String NEXUS_DIALUP_SENHA_PARA_COMUNICACAO = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[5]/div/input[@id='165']";
	private static final String NEXUS_DIALUP_COLETAR_FORMAS_ONDA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[6]/label/input";

	//NEXUS - Ethernet
	private static final String NEXUS_ETH_ENDERECO_OU_IP = "//*[@id='21']";
	private static final String NEXUS_ETH_PORTA = "//*[@id='22']";
	private static final String NEXUS_ETH_ID = "//*[@id='23']";
	private static final String NEXUS_ETH_COLETAR_FORMAS_ONDA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[4]/label/input[@id='24']";
	private static final String NEXUS_ETH_SEG = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[5]/div/div/label[1]/input";
	private static final String NEXUS_ETH_TER = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[5]/div/div/label[2]/input";
	private static final String NEXUS_ETH_QUA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[5]/div/div/label[3]/input";
	private static final String NEXUS_ETH_QUI = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[5]/div/div/label[4]/input";
	private static final String NEXUS_ETH_SEX = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[5]/div/div/label[5]/input";
	private static final String NEXUS_ETH_SAB = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[5]/div/div/label[6]/input";
	private static final String NEXUS_ETH_DOM = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[5]/div/div/label[7]/input";
	private static final String NEXUS_ETH_HORA_INICIO_DA_COLETA = "//*[@id='hora35']";
	private static final String NEXUS_ETH_MINUTO_INICIO_DA_COLETA = "//*[@id='minuto35']";
	private static final String NEXUS_ETH_HORA_FIM_DA_COLETA = "//*[@id='hora36']";
	private static final String NEXUS_ETH_MINUTO_FIM_DA_COLETA = "//*[@id='minuto36']";
	private static final String NEXUS_ETH_USUARIO_PARA_COMUNICACAO = "//*[@id='166']";
	private static final String NEXUS_ETH_SENHA_PARA_COMUNICACAO = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[10]/div/input[@id='167']";

	//Nexus 1250
	private static final String NEXUS1250_ENDERECO_OU_IP = "//*[@id='105']";
	private static final String NEXUS1250_DEVICE_ADDRESS = "//*[@id='106']";
	private static final String NEXUS1250_PORTA = "//*[@id='107']";
	private static final String NEXUS1250_TIMEOUT = "//*[@id='277']";
	private static final String NEXUS1250_RETENTATIVAS = "//*[@id='278']";
	private static final String NEXUS1250_SENHA_NIVEL_1 = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[7]/div/input[@id='335']";
	private static final String NEXUS1250_SENHA_NIVEL_2 = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[9]/div/input[@id='336']";
	private static final String NEXUS1250_USUARIO_ESTENDIDO = "//*[@id='337']";
	private static final String NEXUS1250_SENHA_USUARIO_ESTENDIDO = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[12]/div/input[@id='338']";

	//Nexus 1500
	private static final String NEXUS1500_ENDERECO_OU_IP = "//*[@id='102']";
	private static final String NEXUS1500_DEVICE_ADDRESS = "//*[@id='103']";
	private static final String NEXUS1500_PORTA = "//*[@id='104']";
	private static final String NEXUS1500_TIMEOUT = "//*[@id='275']";
	private static final String NEXUS1500_RETENTATIVAS = "//*[@id='276']";
	private static final String NEXUS1500_SENHA_NIVEL_1 = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[7]/div/input[@id='331']";
	private static final String NEXUS1500_SENHA_NIVEL_2 = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[9]/div/input[@id='332']";
	private static final String NEXUS1500_USUARIO_ESTENDIDO = "//*[@id='333']";
	private static final String NEXUS1500_SENHA_USUARIO_ESTENDIDO = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[12]/div/input[@id='334']";

	//OneRF - Ethernet
	private static final String ONERF_ETH_ENDERECO_OU_IP = "//*[@id='245']";
	private static final String ONERF_ETH_PORTA = "//*[@id='246']";

	//PQube3 - Ethernet e FTP
	private static final String PQUBE3_ETH_FTP_ENDERECO_OU_IP = "//*[@id='232']";
	private static final String PQUBE3_ETH_FTP_PORTA = "//*[@id='233']";
	private static final String PQUBE3_ETH_FTP_ID = "//*[@id='234']";
	private static final String PQUBE3_ETH_FTP_PORTA_FT = "//*[@id='235']";
	private static final String PQUBE3_ETH_FTP_USUARIO = "//*[@id='236']";
	private static final String PQUBE3_ETH_FTP_SENHA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[7]/div/input[@id='237']";
	private static final String PQUBE3_ETH_FTP_OFFSET_DE_MEMORIA = "//*[@id='238']";

	//Q1000 - Ethergate ou Terminal Server
	private static final String Q1000_ETG_TERM_ENDERECO_OU_IP = "//*[@id='69']";
	private static final String Q1000_ETG_TERM_PORTA = "//*[@id='70']";
	private static final String Q1000_ETG_TERM_ID = "//*[@id='71']";
	private static final String Q1000_ETG_TERM_USUARIO_PARA_COMUNICACAO = "//*[@id='168']";
	private static final String Q1000_ETG_TERM_SENHA_PARA_COMUNICACAO = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[6]/div/input[@id='169']";

	//Q1000 - Ethernet
	private static final String Q1000_ETH_ENDERECO_OU_IP = "//*[@id='121']";
	private static final String Q1000_ETH_PORTA = "//*[@id='122']";
	private static final String Q1000_ETH_ID = "//*[@id='123']";
	private static final String Q1000_ETH_TIMEOUT = "//*[@id='283']";
	private static final String Q1000_ETH_RETENTATIVAS = "//*[@id='284']";
	private static final String Q1000_ETH_SENHA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[7]/div/input[@id='343']";

	//SAD - M2M
	private static final String SAD_M2M_URL = "//*[@id='111']";
	private static final String SAD_M2M_REMOTA = "//*[@id='112']";

	//SAGA1000 - Dial-Up
	private static final String SAGA1000_DIALUP_ENDERECO_OU_IP = "//*[@id='25']";
	private static final String SAGA1000_DIALUP_ID = "//*[@id='26']";
	private static final String SAGA1000_DIALUP_SEG = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[3]/div/div/label[1]/input";
	private static final String SAGA1000_DIALUP_TER = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[3]/div/div/label[2]/input";
	private static final String SAGA1000_DIALUP_QUA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[3]/div/div/label[3]/input";
	private static final String SAGA1000_DIALUP_QUI = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[3]/div/div/label[4]/input";
	private static final String SAGA1000_DIALUP_SEX = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[3]/div/div/label[5]/input";
	private static final String SAGA1000_DIALUP_SAB = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[3]/div/div/label[6]/input";
	private static final String SAGA1000_DIALUP_DOM = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[3]/div/div/label[7]/input";
	private static final String SAGA1000_DIALUP_HORA_INICIO_DA_COLETA = "//*[@id='hora28']";
	private static final String SAGA1000_DIALUP_MINUTO_INICIO_DA_COLETA = "//*[@id='minuto28']";
	private static final String SAGA1000_DIALUP_HORA_FIM_DA_COLETA = "//*[@id='hora29']";
	private static final String SAGA1000_DIALUP_MINUTO_FIM_DA_COLETA = "//*[@id='minuto29']";

	//SAGA1000 - Ethergate ou Terminal Server
	private static final String SAGA1000_ETG_ENDERECO_OU_IP = "//*[@id='30']";
	private static final String SAGA1000_ETG_PORTA = "//*[@id='31']";
	private static final String SAGA1000_ETG_ID = "//*[@id='32']";

	//SAGA1000 - Monoponto
	private static final String SAGA1000_MP_ENDERECO_OU_IP = "//*[@id='216']";
	private static final String SAGA1000_MP_PORTA = "//*[@id='217']";

	//SAGA2000 - Monoponto
	private static final String SAGA2000_MP_ENDERECO_OU_IP = "//*[@id='184']";
	private static final String SAGA2000_MP_PORTA = "//*[@id='185']";

	//SatLink2 - Satélite *

	//Shark - Modbus TCP/IP
	private static final String SHARK_MODBUS_ENDERECO_OU_IP = "//*[@id='225']";
	private static final String SHARK_MODBUS_PORTA = "//*[@id='226']";
	private static final String SHARK_MODBUS_ID = "//*[@id='224']";

	//Sistema Chip
	private static final String SISTEMA_CHIP_NUMERO_DO_TRANSPONDER = "//*[@id='154']";
	private static final String SISTEMA_CHIP_URL_DO_WEB_SERVICE = "//*[@id='157']";
	private static final String SISTEMA_CHIP_USUARIO = "//*[@id='158']";
	private static final String SISTEMA_CHIP_SENHA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[5]/div/input[@id='159']";

	//SL7000 - Ethergate ou Terminal Server
	private static final String SL7000_ETG_TERM_ENDERECO_OU_IP = "//*[@id='72']";
	private static final String SL7000_ETG_TERM_DEVICE_ADDRESS = "//*[@id='73']";
	private static final String SL7000_ETG_TERM_PORTA = "//*[@id='74']";
	private static final String SL7000_ETG_TERM_USUARIO_PARA_COMUNICACAO = "//*[@id='172']";
	private static final String SL7000_ETG_TERM_SENHA_PARA_COMUNICACAO = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[6]/div/input[@id='173']";
	
	//SL7000 - TCP
	private static final String SL7000_TCP_ENDERECO_OU_IP = "//*[@id='108']";
	private static final String SL7000_TCP_PORTA = "//*[@id='109']";
	private static final String SL7000_TCP_ID = "//*[@id='110']";
	private static final String SL7000_TCP_TIMEOUT = "//*[@id='279']";
	private static final String SL7000_TCP_RETENTATIVAS = "//*[@id='280']";
	private static final String SL7000_TCP_TIPOS_DE_CLIENTE = "//*[@id='329']";
	private static final String SL7000_TCP_SENHA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[8]/div/input[@id='330']";

	//SL7000-Ethernet-E3-RT
	private static final String SL7000_ETH_E3_RT_ENDERECO_OU_IP = "//*[@id='128']";
	private static final String SL7000_ETH_E3_RT_PORTA = "//*[@id='130']";
	private static final String SL7000_ETH_E3_RT_ID = "//*[@id='131']";
	private static final String SL7000_ETH_E3_RT_COLETAR_FORMAS_ONDA = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[4]/label/input";
	private static final String SL7000_ETH_E3_RT_USUARIO_PARA_COMUNICACAO = "//*[@id='174']";
	private static final String SL7000_ETH_E3_RT_SENHA_PARA_COMUNICACAO = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[7]/div/input[@id='175']";
	
	//SmartGreen
	private static final String SMARTGREEN_DEVICE_ID = "//*[@id='153']";
	
	//Stevens DOT Logger - Satélite *
	
	//STM
	private static final String STM_STR_REMOTO = "//*[@id='33']";
	
	//Tekpea
	private static final String TEKPEA_DEVICE_ID = "//*[@id='medidor-comunicacao']/form/fieldset/div/div/div/input[@id='152']";
	
	//TNS
	private static final String TNS_NUMERO_DO_TRANSPONDER = "//*[@id='132']";
	
	//WebService
	private static final String WEBSERVICE_IDENTIFICADOR_REMOTO = "//*[@id='96']";
	
	//WebService - Hidrologia
	private static final String WS_HIDROLOGIA_CODIGO_SENSOR_CHUVA = "//*[@id='243']";
	private static final String WS_HIDROLOGIA_CODIGO_SENSOR_NIVEL = "//*[@id='244']";
	
	//Weg ModBus - Ethernet = 
	private static final String WEG_MODBUS_ENDERECO_OU_IP = "//*[@id='186']";
	private static final String WEG_MODBUS_PORTA = "//*[@id='187']";
	
	//Wits *
	
	// ZIV - Ethernet
	private static final String ZIV_ETH_ENDERECO_OU_IP = "//*[@id='115']";
	private static final String ZIV_ETH_PORTA = "//*[@id='116']";
	private static final String ZIV_ETH_ID = "//*[@id='117']";
	private static final String ZIV_ETH_USUARIO_PARA_COMUNICACAO = "//*[@id='178']";
	private static final String ZIV_ETH_SENHA_PARA_COMUNICACAO = "//*[@id='medidor-comunicacao']/form/fieldset/div/div[6]/div/input[@id='179']";

	public void typeSelectComunicacao(String value){		
		typeSelectComboOption(SELECT_COMUNICACAO, value);
	}
	
	public void typeCheckBoxHabilitar(String value){
		typeCheckOption(CHECK_HABILITAR, value);
	}
	
	public void typeTextProtocolo(String value){
		typeText(TEXT_PROTOCOLO, value);
	}
	
	public void keyPageDown(){
		useKey(TEXT_PROTOCOLO, Keys.PAGE_DOWN);
		waitForATime(TimePRM._1_SEC);
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
				
	public void clickAbaComunicacao(){
		clickOnElement(LINK_ABA_COMUNICACAO);
	}
	
	// ABS SSU Modbus - Ethernet
	public void typeTextAbsSsuEnderecoOuIp(String value) {
		waitForPresenceOfElementLocated(ABS_SSU_ENDERECO_OU_IP);
		typeText(ABS_SSU_ENDERECO_OU_IP, value);
	}
	public void typeTextAbsSsuPorta(String value) {
		typeText(ABS_SSU_PORTA, value);
	}
	public void typeTextAbsSsuId(String value) {
		typeText(ABS_SSU_ID, value);
	}
	
	// ANSI-C12	
	public void typeTextAnsiC12EnderecoOuIp(String value) {
		waitForPresenceOfElementLocated(ANSI_C12_ENDERECO_OU_IP);
		typeText(ANSI_C12_ENDERECO_OU_IP, value);
	}
	public void typeTextAnsiC12Porta(String value) {
		typeText(ANSI_C12_PORTA, value);
	}
	public void typeTextAnsiC12ModeloDeMedidor(String value) {
		typeText(ANSI_C12_MODELO_DE_MEDIDOR, value);
	}
	
	// ApiConstruserv
	public void typeTextConstruservIdentificadorDaEstacao(String value) {
		waitForPresenceOfElementLocated(CONSTRUSERV_IDENTIFICADOR_DA_ESTACAO);
		typeText(CONSTRUSERV_IDENTIFICADOR_DA_ESTACAO, value);
	}
	
	// ArquivosAmmonit
	public void typeTextAmmonitVelVentoMediaAnemSuperior(String value){
		waitForPresenceOfElementLocated(AMMONIT_VEL_VENTO_MEDIA_ANEM_SUPERIOR);
		typeText(AMMONIT_VEL_VENTO_MEDIA_ANEM_SUPERIOR, value);
	}		
	public void typeTextAmmonitVelVentoMaximaAnemSuperior(String value){
		typeText(AMMONIT_VEL_VENTO_MAXIMA_ANEM_SUPERIOR, value);
	}		
	public void typeTextAmmonitVelVentoMinimaAnemSuperior(String value){
		typeText(AMMONIT_VEL_VENTO_MINIMA_ANEM_SUPERIOR, value);
	}		
	public void typeTextAmmonitVelVentoDesvioPadraoAnemSuperior(String value){
		typeText(AMMONIT_VEL_VENTO_DESVIO_PADRAO_ANEM_SUPERIOR, value);
	}		
	public void typeTextAmmonitVelVentoMediaAnem2(String value){
		typeText(AMMONIT_VEL_VENTO_MEDIA_ANEM_2, value);
	}		
	public void typeTextAmmonitVelVentoMaximaAnem2(String value){
		typeText(AMMONIT_VEL_VENTO_MAXIMA_ANEM_2, value);
	}		
	public void typeTextAmmonitVelVentoMinimaAnem2(String value){
		typeText(AMMONIT_VEL_VENTO_MINIMA_ANEM_2, value);
	}		
	public void typeTextAmmonitVelVentoDesvioPadraoAnem2(String value){
		typeText(AMMONIT_VEL_VENTO_DESVIO_PADRAO_ANEM_2, value);
	}		
	public void typeTextAmmonitVelVentoMediaAnem3(String value){
		typeText(AMMONIT_VEL_VENTO_MEDIA_ANEM_3, value);
	}		
	public void typeTextAmmonitVelVentoMaximaAnem3(String value){
		typeText(AMMONIT_VEL_VENTO_MAXIMA_ANEM_3, value);
	}		
	public void typeTextAmmonitVelVentoMinimaAnem3(String value){
		typeText(AMMONIT_VEL_VENTO_MINIMA_ANEM_3, value);
	}		
	public void typeTextAmmonitVelVentoDesvioPadraoAnem3(String value){
		typeText(AMMONIT_VEL_VENTO_DESVIO_PADRAO_ANEM_3, value);
	}		
	public void typeTextAmmonitDirVentoMediaWindVaneSuperior(String value){
		typeText(AMMONIT_DIR_VENTO_MEDIA_WIND_VANE_SUPERIOR, value);
	}		
	public void typeTextAmmonitDirVentoDesvioPadraoWindVaneSuperior(String value){
		typeText(AMMONIT_DIR_VENTO_DESVIO_PADRAO_WIND_VANE_SUPERIOR, value);
	}		
	public void typeTextAmmonitDirVentoMediaWindVane2(String value){
		typeText(AMMONIT_DIR_VENTO_MEDIA_WIND_VANE_2, value);
	}		
	public void typeTextAmmonitDirVentoDesvioPadraoWindVane2(String value){
		typeText(AMMONIT_DIR_VENTO_DESVIO_PADRAO_WIND_VANE_2, value);
	}		
	public void typeTextAmmonitPressaoDoAr(String value){
		typeText(AMMONIT_PRESSAO_DO_AR, value);
	}		
	public void typeTextAmmonitTemperaturaDoAr(String value){
		typeText(AMMONIT_TEMPERATURA_DO_AR, value);
	}		
	public void typeTextAmmonitUmidadeRelativaDoAr(String value){
		typeText(AMMONIT_UMIDADE_RELATIVA_DO_AR, value);
	}				
	
	// ArquivosNOMAD
	public void typeTextNomadVelVentoMediaAnemSuperior(String value){
		waitForPresenceOfElementLocated(NOMAD_VEL_VENTO_MEDIA_ANEM_SUPERIOR);
		typeText(NOMAD_VEL_VENTO_MEDIA_ANEM_SUPERIOR, value);
	}		
	public void typeTextNomadVelVentoMaximaAnemSuperior(String value){
		typeText(NOMAD_VEL_VENTO_MAXIMA_ANEM_SUPERIOR, value);
	}		
	public void typeTextNomadVelVentoMinimaAnemSuperior(String value){
		typeText(NOMAD_VEL_VENTO_MINIMA_ANEM_SUPERIOR, value);
	}		
	public void typeTextNomadVelVentoDesvioPadraoAnemSuperior(String value){
		typeText(NOMAD_VEL_VENTO_DESVIO_PADRAO_ANEM_SUPERIOR, value);
	}		
	public void typeTextNomadVelVentoMediaAnem2(String value){
		typeText(NOMAD_VEL_VENTO_MEDIA_ANEM_2, value);
	}		
	public void typeTextNomadVelVentoMaximaAnem2(String value){
		typeText(NOMAD_VEL_VENTO_MAXIMA_ANEM_2, value);
	}		
	public void typeTextNomadVelVentoMinimaAnem2(String value){
		typeText(NOMAD_VEL_VENTO_MINIMA_ANEM_2, value);
	}		
	public void typeTextNomadVelVentoDesvioPadraoAnem2(String value){
		typeText(NOMAD_VEL_VENTO_DESVIO_PADRAO_ANEM_2, value);
	}		
	public void typeTextNomadVelVentoMediaAnem3(String value){
		typeText(NOMAD_VEL_VENTO_MEDIA_ANEM_3, value);
	}		
	public void typeTextNomadVelVentoMaximaAnem3(String value){
		typeText(NOMAD_VEL_VENTO_MAXIMA_ANEM_3, value);
	}		
	public void typeTextNomadVelVentoMinimaAnem3(String value){
		typeText(NOMAD_VEL_VENTO_MINIMA_ANEM_3, value);
	}		
	public void typeTextNomadVelVentoDesvioPadraoAnem3(String value){
		typeText(NOMAD_VEL_VENTO_DESVIO_PADRAO_ANEM_3, value);
	}		
	public void typeTextNomadDirVentoMediaWindVaneSuperior(String value){
		typeText(NOMAD_DIR_VENTO_MEDIA_WIND_VANE_SUPERIOR, value);
	}		
	public void typeTextNomadDirVentoDesvioPadraoWindVaneSuperior(String value){
		typeText(NOMAD_DIR_VENTO_DESVIO_PADRAO_WIND_VANE_SUPERIOR, value);
	}		
	public void typeTextNomadDirVentoMediaWindVane2(String value){
		typeText(NOMAD_DIR_VENTO_MEDIA_WIND_VANE_2, value);
	}		
	public void typeTextNomadDirVentoDesvioPadraoWindVane2(String value){
		typeText(NOMAD_DIR_VENTO_DESVIO_PADRAO_WIND_VANE_2, value);
	}		
	public void typeTextNomadPressaoDoAr(String value){
		typeText(NOMAD_PRESSAO_DO_AR, value);
	}		
	public void typeTextNomadTemperaturaDoAr(String value){
		typeText(NOMAD_TEMPERATURA_DO_AR, value);
	}		
	public void typeTextNomadUmidadeRelativaDoAr(String value){
		typeText(NOMAD_UMIDADE_RELATIVA_DO_AR, value);
	}	
		
	// Bancada virtual Way2
	public void typeTextWay2EnderecoOuIp(String value){
		waitForPresenceOfElementLocated(WAY2_ENDERECO_OU_IP);
		typeText(WAY2_ENDERECO_OU_IP, value);
	}		
	public void typeTextWay2Porta(String value){
		typeText(WAY2_PORTA, value);
	}		
	public void typeTextWay2Id(String value){
		typeText(WAY2_ID, value);
	}		
	
	// CCK
	public void typeTextCckStrRemoto(String value){
		waitForPresenceOfElementLocated(CCK_STR_REMOTO);
		typeText(CCK_STR_REMOTO, value);
	}			
	
	// CCK 7550S - Ethernet		
	public void typeTextCck7550EthEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(CCK7550_ETH_ENDERECO_OU_IP);
		typeText(CCK7550_ETH_ENDERECO_OU_IP, value);
	}		
	public void typeTextCck7550EthPorta(String value){
		typeText(CCK7550_ETH_PORTA, value);
	}		
	public void typeTextCck7550EthId(String value){
		typeText(CCK7550_ETH_ID, value);
	}		
	
	// CD103
	public void typeTextCd103Gateway(String value){
		waitForPresenceOfElementLocated(CD103_GATEWAY);
		typeText(CD103_GATEWAY, value);
	}		
	
	// Coletor Titan
	public void typeTextTitanCaixa(String value){
		waitForPresenceOfElementLocated(TITAN_CAIXA);
		typeText(TITAN_CAIXA, value);
	}	
	
	// C750
	public void typeTextC750Gateway(String value){
		waitForPresenceOfElementLocated(C750_GATEWAY);
		typeText(C750_GATEWAY, value);
	}		
	
	// ELO 2113 - ABNT Multiponto Serial Ethernet
	public void typeTextElo2113MpsEthEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(ELO2113_MPS_ETH_ENDERECO_OU_IP);
		typeText(ELO2113_MPS_ETH_ENDERECO_OU_IP, value);
	}		
	public void typeTextElo2113MpsEthPorta(String value){
		typeText(ELO2113_MPS_ETH_PORTA, value);
	}		
	
	// ELO 2180 - Ethernet	
	public void typeTextElo2180EthEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(ELO2180_ETH_ENDERECO_OU_IP);
		typeText(ELO2180_ETH_ENDERECO_OU_IP, value);
	}		
	public void typeTextElo2180EthPorta(String value){
		typeText(ELO2180_ETH_PORTA, value);
	}		
	public void typeTextElo2180EthId(String value){
		typeText(ELO2180_ETH_ID, value);
	}		
	
	// ELO 2180 - V2.xx - Ethernet
	public void typeTextElo2180V2xxEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(ELO2180_V2XX_ENDERECO_OU_IP);
		typeText(ELO2180_V2XX_ENDERECO_OU_IP, value);
	}		
	public void typeTextElo2180V2xxPorta(String value){
		typeText(ELO2180_V2XX_PORTA, value);
	}		
	public void typeTextElo2180V2xxId(String value){
		typeText(ELO2180_V2XX_ID, value);
	}	
	
	// Elster A3RBR - Monoponto
	public void typeTextElsterA3rbrEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(ELSTER_A3RBR_ENDERECO_OU_IP);
		typeText(ELSTER_A3RBR_ENDERECO_OU_IP, value);
	}		
	public void typeTextElsterA3rbrPorta(String value){
		typeText(ELSTER_A3RBR_PORTA, value);
	}		
	
	// E650 - Monoponto
	public void typeTextE650MpEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(E650_MP_ENDERECO_OU_IP);
		typeText(E650_MP_ENDERECO_OU_IP, value);
	}		
	public void typeTextE650MpPorta(String value){
		typeText(E650_MP_PORTA, value);
	}		

	// E750 - Ethernet
	public void typeTextE750EthEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(E750_ETH_ENDERECO_OU_IP);
		typeText(E750_ETH_ENDERECO_OU_IP, value);
	}		
	public void typeTextE750EthPorta(String value){
		typeText(E750_ETH_PORTA, value);
	}	
	public void typeTextE750EthId(String value){
		typeText(E750_ETH_ID, value);
	}		
	
	// E750 - Monoponto
	public void typeTextE750MpEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(E750_MP_ENDERECO_OU_IP);
		typeText(E750_MP_ENDERECO_OU_IP, value);
	}		
	public void typeTextE750MpPorta(String value){
		typeText(E750_MP_PORTA, value);
	}	
	
	// Gateway Reader
	public void typeTextReaderGateway(String value){
		waitForPresenceOfElementLocated(READER_GATEWAY);
		typeText(READER_GATEWAY, value);
	}		
	
	// Integração Banco de Dados Automação
	public void typeTextBdAutomacaoIdentificadorRemoto(String value){
		waitForPresenceOfElementLocated(BD_AUTOMACAO_IDENTIFICADOR_REMOTO);
		typeText(BD_AUTOMACAO_IDENTIFICADOR_REMOTO, value);
	}		
	
	// ION Dial-Up
	public void typeTextIonDialUpTelefone(String value){
		waitForPresenceOfElementLocated(ION_DIAL_UP_TELEFONE);
		typeText(ION_DIAL_UP_TELEFONE, value);
	}		
	public void typeTextIonDialUpId(String value){
		typeText(ION_DIAL_UP_ID, value);
	}		
	
	// ION - Ethergate ou Terminal Server
	public void typeTextIonEtgTermEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(ION_ETG_TERM_ENDERECO_OU_IP);
		typeText(ION_ETG_TERM_ENDERECO_OU_IP, value);
	}		
	public void typeTextIonEtgTermPorta(String value){
		typeText(ION_ETG_TERM_PORTA, value);
	}		
	public void typeTextIonEtgTermId(String value){
		typeText(ION_ETG_TERM_ID, value);
	}		
	public void typeCheckBoxIonEtgTermColetarFormasOnda(String value) {
		typeCheckOption(ION_ETG_TERM_COLETAR_FORMAS_ONDA, value);
	}		
	
	// ION - Ethernet
	public void typeTextIonEthEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(ION_ETH_ENDERECO_OU_IP);
		typeText(ION_ETH_ENDERECO_OU_IP, value);
	}		
	public void typeTextIonEthPorta(String value){
		typeText(ION_ETH_PORTA, value);
	}		
	public void typeTextIonEthId(String value){
		typeText(ION_ETH_ID, value);
	}		
	public void typeCheckBoxIonEthColetarFormasOnda(String value) {
		typeCheckOption(ION_ETH_COLETAR_FORMAS_ONDA, value);
	}		
	
	// ION - GPRS
	public void typeTextIonGprsEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(ION_GPRS_ENDERECO_OU_IP);
		typeText(ION_GPRS_ENDERECO_OU_IP, value);
	}		
	public void typeTextIonGprsPorta(String value){
		typeText(ION_GPRS_PORTA, value);
	}		
	public void typeTextIonGprsId(String value){
		typeText(ION_GPRS_ID, value);
	}		
	public void typeCheckBoxIonGprsColetarFormasOnda(String value) {
		typeCheckOption(ION_GPRS_COLETAR_FORMAS_ONDA, value);
	}		
	
	// ION - Serial
	public void typeTextIonSerialEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(ION_SERIAL_ENDERECO_OU_IP);
		typeText(ION_SERIAL_ENDERECO_OU_IP, value);
	}		
	public void typeTextIonSerialId(String value){
		typeText(ION_SERIAL_ID, value);
	}		
	
	// ION-Ethernet-E3-RT
	public void typeTextIonEthE3RtEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(ION_ETH_E3_RT_ENDERECO_OU_IP);
		typeText(ION_ETH_E3_RT_ENDERECO_OU_IP, value);
	}		
	public void typeTextIonEthE3RtPorta(String value){
		typeText(ION_ETH_E3_RT_PORTA, value);
	}		
	public void typeTextIonEthE3RtId(String value){
		typeText(ION_ETH_E3_RT_ID, value);
	}		
	public void typeCheckIonEthE3RtColetarFormasOnda(String value){
		typeCheckOption(ION_ETH_E3_RT_COLETAR_FORMAS_ONDA, value);
	}		
	
	// ION7650 - Ethernet
	public void typeTextIon7650EthEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(ION7650_ETH_ENDERECO_OU_IP);
		typeText(ION7650_ETH_ENDERECO_OU_IP, value);
	}		
	public void typeTextIon7650EthPorta(String value){
		typeText(ION7650_ETH_PORTA, value);
	}		
	public void typeTextIon7650EthId(String value){
		typeText(ION7650_ETH_ID, value);
	}		
	public void typeCheckIon7650EthColetarFormasOnda(String value){
		typeCheckOption(ION7650_ETH_COLETAR_FORMAS_ONDA, value);
	}		
	
	// Landis E34 - ABNT Multiponto Serial Ethernet
	public void typeTextLandisE34MpEthEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(LANDIS_E34_MP_ETH_ENDERECO_OU_IP);
		typeText(LANDIS_E34_MP_ETH_ENDERECO_OU_IP, value);
	}		
	public void typeTextLandisE34MpEthPorta(String value){
		typeText(LANDIS_E34_MP_ETH_PORTA, value);
	}		
	
	// M2M
	public void typeTextM2mId(String value){
		waitForPresenceOfElementLocated(M2M_ID);
		typeText(M2M_ID, value);
	}
	
	// Nansen K Art - ABNT Multiponto Serial Ethernet
	public void typeTextNansenKArtEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(NANSEN_K_ART_ENDERECO_OU_IP);
		typeText(NANSEN_K_ART_ENDERECO_OU_IP, value);
	}		
	public void typeTextNansenKArtPorta(String value){
		typeText(NANSEN_K_ART_PORTA, value);
	}
	
	// Nansen Spectrun SX-ABNT Multiponto Serial Ethernet
	public void typeTextNansenSpectrunSxEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(NANSEN_SPECTRUN_SX_ENDERECO_OU_IP);
		typeText(NANSEN_SPECTRUN_SX_ENDERECO_OU_IP, value);
	}		
	public void typeTextNansenSpectrunSxPorta(String value){
		typeText(NANSEN_SPECTRUN_SX_PORTA, value);
	}		
	
	// NEXUS - Dial-Up
	public void typeTextNexusDialupTelefone(String value){
		waitForPresenceOfElementLocated(NEXUS_DIALUP_TELEFONE);
		typeText(NEXUS_DIALUP_TELEFONE, value);
	}		
	public void typeTextNexusDialupId(String value){
		typeText(NEXUS_DIALUP_ID, value);
	}		
	public void typeTextNexusDialupUsuarioParaComunicacao(String value){
		typeText(NEXUS_DIALUP_USUARIO_PARA_COMUNICACAO, value);
	}		
	public void typeTextNexusDialupSenhaParaComunicacao(String value){
		typeText(NEXUS_DIALUP_SENHA_PARA_COMUNICACAO, value);
	}		
	public void typeCheckNexusDialupColetarFormasOnda(String value){
		try {
			typeCheckOption(NEXUS_DIALUP_COLETAR_FORMAS_ONDA, value);
		} catch (Exception ex){
			useKey(NEXUS_DIALUP_SENHA_PARA_COMUNICACAO, Keys.TAB);
			useKey(NEXUS_DIALUP_SENHA_PARA_COMUNICACAO, Keys.SPACE);
		}
	}	
	
	// NEXUS - Ethernet
	public void typeTextNexusEthEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(NEXUS_ETH_ENDERECO_OU_IP);
		typeText(NEXUS_ETH_ENDERECO_OU_IP, value);
	}		
	public void typeTextNexusEthPorta(String value){
		typeText(NEXUS_ETH_PORTA, value);
	}		
	public void typeTextNexusEthId(String value){
		typeText(NEXUS_ETH_ID, value);
	}	
	public void typeCheckBoxNexusEthColetarFormasOnda(String value){
		try {
			typeCheckOption(NEXUS_ETH_COLETAR_FORMAS_ONDA, value);
		} catch (Exception ex){
			useKey(NEXUS_ETH_ID, Keys.TAB);
			useKey(NEXUS_ETH_ID, Keys.SPACE);
		}
	}
	public void typeCheckBoxNexusEthSeg(String value){
		typeCheckOption(NEXUS_ETH_SEG, value);
	}
	public void typeCheckBoxNexusEthTer(String value){
		typeCheckOption(NEXUS_ETH_TER, value);
	}
	public void typeCheckBoxNexusEthQua(String value){
		typeCheckOption(NEXUS_ETH_QUA, value);
	}
	public void typeCheckBoxNexusEthQui(String value){
		typeCheckOption(NEXUS_ETH_QUI, value);
	}
	public void typeCheckBoxNexusEthSex(String value){
		typeCheckOption(NEXUS_ETH_SEX, value);
	}
	public void typeCheckBoxNexusEthSab(String value){
		typeCheckOption(NEXUS_ETH_SAB, value);
	}
	public void typeCheckBoxNexusEthDom(String value){
		typeCheckOption(NEXUS_ETH_DOM, value);
	}
	public void typeTextNexusEthHoraInicioDaColeta(String value){
		typeText(NEXUS_ETH_HORA_INICIO_DA_COLETA, value);
	}		
	public void typeTextNexusEthMinutoInicioDaColeta(String value){
		typeText(NEXUS_ETH_MINUTO_INICIO_DA_COLETA, value);
	}		
	public void typeTextNexusEthHoraFimDaColeta(String value){
		typeText(NEXUS_ETH_HORA_FIM_DA_COLETA, value);
	}	
	public void typeTextNexusEthMinutoFimDaColeta(String value){
		typeText(NEXUS_ETH_MINUTO_FIM_DA_COLETA, value);
	}
	public void typeTextNexusEthUsuarioParaComunicacao(String value){
		typeText(NEXUS_ETH_USUARIO_PARA_COMUNICACAO, value);
	}	
	public void typeTextNexusEthSenhaParaComunicacao(String value){
		typeText(NEXUS_ETH_SENHA_PARA_COMUNICACAO, value);
	}
	
	// Nexus 1250
	public void typeTextNexus1250EnderecoOuIp(String value){
		waitForPresenceOfElementLocated(NEXUS1250_ENDERECO_OU_IP);
		typeText(NEXUS1250_ENDERECO_OU_IP, value);
	}		
	public void typeTextNexus1250DeviceAddress(String value){
		typeText(NEXUS1250_DEVICE_ADDRESS, value);
	}		
	public void typeTextNexus1250Porta(String value){
		typeText(NEXUS1250_PORTA, value);
	}		
	public void typeTextNexus1250Timeout(String value){
		typeText(NEXUS1250_TIMEOUT, value);
	}		
	public void typeTextNexus1250Retentativas(String value){
		typeText(NEXUS1250_RETENTATIVAS, value);
	}				
	public void typeTextNexus1250SenhaNivel1(String value){
		waitForPresenceOfElementLocated(NEXUS1250_SENHA_NIVEL_1);
		typeText(NEXUS1250_SENHA_NIVEL_1, value);
	}		
	public void typeTextNexus1250SenhaNivel2(String value){
		waitForPresenceOfElementLocated(NEXUS1250_SENHA_NIVEL_2);
		typeText(NEXUS1250_SENHA_NIVEL_2, value);
	}				
	public void typeTextNexus1250UsuarioEstendido(String value){
		typeText(NEXUS1250_USUARIO_ESTENDIDO, value);
	}		
	public void typeTextNexus1250SenhaUsuarioEstendido(String value){
		waitForPresenceOfElementLocated(NEXUS1250_SENHA_USUARIO_ESTENDIDO);
		typeText(NEXUS1250_SENHA_USUARIO_ESTENDIDO, value);
	}				

	// Nexus 1500
	public void typeTextNexus1500EnderecoOuIp(String value){
		waitForPresenceOfElementLocated(NEXUS1500_ENDERECO_OU_IP);
		typeText(NEXUS1500_ENDERECO_OU_IP, value);
	}		
	public void typeTextNexus1500DeviceAddress(String value){
		typeText(NEXUS1500_DEVICE_ADDRESS, value);
	}		
	public void typeTextNexus1500Porta(String value){
		typeText(NEXUS1500_PORTA, value);
	}		
	public void typeTextNexus1500Timeout(String value){
		typeText(NEXUS1500_TIMEOUT, value);
	}		
	public void typeTextNexus1500Retentativas(String value){
		typeText(NEXUS1500_RETENTATIVAS, value);
	}				
	public void typeTextNexus1500SenhaNivel1(String value){
		waitForPresenceOfElementLocated(NEXUS1500_SENHA_NIVEL_1);
		typeText(NEXUS1500_SENHA_NIVEL_1, value);
	}		
	public void typeTextNexus1500SenhaNivel2(String value){
		waitForPresenceOfElementLocated(NEXUS1500_SENHA_NIVEL_2);
		typeText(NEXUS1500_SENHA_NIVEL_2, value);
	}				
	public void typeTextNexus1500UsuarioEstendido(String value){
		typeText(NEXUS1500_USUARIO_ESTENDIDO, value);
	}		
	public void typeTextNexus1500SenhaUsuarioEstendido(String value){
		waitForPresenceOfElementLocated(NEXUS1500_SENHA_USUARIO_ESTENDIDO);
		typeText(NEXUS1500_SENHA_USUARIO_ESTENDIDO, value);
	}
	
	// OneRF - Ethernet
	public void typeTextOneRfEthEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(ONERF_ETH_ENDERECO_OU_IP);
		typeText(ONERF_ETH_ENDERECO_OU_IP, value);
	}		
	public void typeTextOneRfEthPorta(String value){
		typeText(ONERF_ETH_PORTA, value);
	}		
	
	// PQube3 - Ethernet e FTP
	public void typeTextPqube3EthFtpEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(PQUBE3_ETH_FTP_ENDERECO_OU_IP);
		typeText(PQUBE3_ETH_FTP_ENDERECO_OU_IP, value);
	}		
	public void typeTextPqube3EthFtpPorta(String value){
		typeText(PQUBE3_ETH_FTP_PORTA, value);
	}	
	public void typeTextPqube3EthFtpId(String value){
		typeText(PQUBE3_ETH_FTP_ID, value);
	}		
	public void typeTextPqube3EthFtpPortaFt(String value){
		typeText(PQUBE3_ETH_FTP_PORTA_FT, value);
	}	
	public void typeTextPqube3EthFtpUsuario(String value){
		typeText(PQUBE3_ETH_FTP_USUARIO, value);
	}		
	public void typeTextPqube3EthFtpSenha(String value){
		typeText(PQUBE3_ETH_FTP_SENHA, value);
	}	
	public void typeTextPqube3EthFtpOffsetDeMemoria(String value){
		typeText(PQUBE3_ETH_FTP_OFFSET_DE_MEMORIA, value);
	}	
	
	// Q1000 - Ethergate ou Terminal Server
	public void typeTextQ1000EtgTermEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(Q1000_ETG_TERM_ENDERECO_OU_IP);
		typeText(Q1000_ETG_TERM_ENDERECO_OU_IP, value);
	}		
	public void typeTextQ1000EtgTermPorta(String value){
		typeText(Q1000_ETG_TERM_PORTA, value);
	}	
	public void typeTextQ1000EtgTermId(String value){
		typeText(Q1000_ETG_TERM_ID, value);
	}		
	public void typeTextQ1000EtgTermUsuarioParaComunicacao(String value){
		typeText(Q1000_ETG_TERM_USUARIO_PARA_COMUNICACAO, value);
	}	
	public void typeTextQ1000EtgTermSenhaParaComunicacao(String value){
		typeText(Q1000_ETG_TERM_SENHA_PARA_COMUNICACAO, value);
	}		
	
	// Q1000 - Ethernet
	public void typeTextQ1000EthEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(Q1000_ETH_ENDERECO_OU_IP);
		typeText(Q1000_ETH_ENDERECO_OU_IP, value);
	}		
	public void typeTextQ1000EthPorta(String value){
		typeText(Q1000_ETH_PORTA, value);
	}	
	public void typeTextQ1000EthId(String value){
		typeText(Q1000_ETH_ID, value);
	}		
	public void typeTextQ1000EthTimeout(String value){
		typeText(Q1000_ETH_TIMEOUT, value);
	}	
	public void typeTextQ1000EthRetentativas(String value){
		typeText(Q1000_ETH_RETENTATIVAS, value);
	}	
	public void typeTextQ1000EthSenha(String value){
		typeText(Q1000_ETH_SENHA, value);
	}	

	// SAD - M2M
	public void typeTextSadM2mUrl(String value){
		waitForPresenceOfElementLocated(SAD_M2M_URL);
		typeText(SAD_M2M_URL, value);
	}		
	public void typeTextSadM2mRemota(String value){
		typeText(SAD_M2M_REMOTA, value);
	}	
	
	// SAGA1000 - Dial-Up;
	public void typeTextSaga1000DialupEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(SAGA1000_DIALUP_ENDERECO_OU_IP);
		typeText(SAGA1000_DIALUP_ENDERECO_OU_IP, value);
	}		
	public void typeTextSaga1000DialupId(String value){
		typeText(SAGA1000_DIALUP_ID, value);
	}	
	public void typeCheckBoxSaga1000DialupSeg(String value){
		typeCheckOption(SAGA1000_DIALUP_SEG, value);
	}
	public void typeCheckBoxSaga1000DialupTer(String value){
		typeCheckOption(SAGA1000_DIALUP_TER, value);
	}
	public void typeCheckBoxSaga1000DialupQua(String value){
		typeCheckOption(SAGA1000_DIALUP_QUA, value);
	}
	public void typeCheckBoxSaga1000DialupQui(String value){
		typeCheckOption(SAGA1000_DIALUP_QUI, value);
	}
	public void typeCheckBoxSaga1000DialupSex(String value){
		typeCheckOption(SAGA1000_DIALUP_SEX, value);
	}
	public void typeCheckBoxSaga1000DialupSab(String value){
		typeCheckOption(SAGA1000_DIALUP_SAB, value);
	}
	public void typeCheckBoxSaga1000DialupDom(String value){
		typeCheckOption(SAGA1000_DIALUP_DOM, value);
	}
	public void typeTextSaga1000DialupHoraInicioDaColeta(String value){
		typeText(SAGA1000_DIALUP_HORA_INICIO_DA_COLETA, value);
	}		
	public void typeTextSaga1000DialupMinutoInicioDaColeta(String value){
		typeText(SAGA1000_DIALUP_MINUTO_INICIO_DA_COLETA, value);
	}		
	public void typeTextSaga1000DialupHoraFimDaColeta(String value){
		typeText(SAGA1000_DIALUP_HORA_FIM_DA_COLETA, value);
	}	
	public void typeTextSaga1000DialupMinutoFimDaColeta(String value){
		typeText(SAGA1000_DIALUP_MINUTO_FIM_DA_COLETA, value);
	}
	
	// SAGA1000 - Ethergate ou Terminal Server
	public void typeTextSaga1000EtgEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(SAGA1000_ETG_ENDERECO_OU_IP);
		typeText(SAGA1000_ETG_ENDERECO_OU_IP, value);
	}		
	public void typeTextSaga1000EtgPorta(String value){
		typeText(SAGA1000_ETG_PORTA, value);
	}	
	public void typeTextSaga1000EtgId(String value){
		typeText(SAGA1000_ETG_ID, value);
	}	
	
	// SAGA1000 - Monoponto
	public void typeTextSaga1000MpEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(SAGA1000_MP_ENDERECO_OU_IP);
		typeText(SAGA1000_MP_ENDERECO_OU_IP, value);
	}		
	public void typeTextSaga1000MpPorta(String value){
		typeText(SAGA1000_MP_PORTA, value);
	}	
	
	// SAGA2000 - Monoponto
	public void typeTextSaga2000MpEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(SAGA2000_MP_ENDERECO_OU_IP);
		typeText(SAGA2000_MP_ENDERECO_OU_IP, value);
	}		
	public void typeTextSaga2000MpPorta(String value){
		typeText(SAGA2000_MP_PORTA, value);
	}	
	
	// Shark - Modbus TCP/IP	
	public void typeTextSharkModbusEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(SHARK_MODBUS_ENDERECO_OU_IP);
		typeText(SHARK_MODBUS_ENDERECO_OU_IP, value);
	}		
	public void typeTextSharkModbusPorta(String value){
		typeText(SHARK_MODBUS_PORTA, value);
	}	
	public void typeTextSharkModbusId(String value){
		typeText(SHARK_MODBUS_ID, value);
	}	
	
	// Sistema Chip;	
	public void typeTextSistemaChipNumeroDoTransponder(String value){
		waitForPresenceOfElementLocated(SISTEMA_CHIP_NUMERO_DO_TRANSPONDER);
		typeText(SISTEMA_CHIP_NUMERO_DO_TRANSPONDER, value);
	}		
	public void typeTextSistemaChipUrlDoWebService(String value){
		typeText(SISTEMA_CHIP_URL_DO_WEB_SERVICE, value);
	}		
	public void typeTextSistemaChipUsuario(String value){
		typeText(SISTEMA_CHIP_USUARIO, value);
	}	
	public void typeTextSistemaChipSenha(String value){
		typeText(SISTEMA_CHIP_SENHA, value);
	}	
	
	// SL7000 - Ethergate ou Terminal Server;
	public void typeTextSl7000EtgTermEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(SL7000_ETG_TERM_ENDERECO_OU_IP);
		typeText(SL7000_ETG_TERM_ENDERECO_OU_IP, value);
	}		
	public void typeTextSl7000EtgTermDeviceAddress(String value){
		typeText(SL7000_ETG_TERM_DEVICE_ADDRESS, value);
	}		
	public void typeTextSl7000EtgTermPorta(String value){
		typeText(SL7000_ETG_TERM_PORTA, value);
	}		
	public void typeTextSl7000EtgTermUsuarioParaComunicacao(String value){
		typeText(SL7000_ETG_TERM_USUARIO_PARA_COMUNICACAO, value);
	}		
	public void typeTextSl7000EtgTermSenhaParaComunicacao(String value){
		typeText(SL7000_ETG_TERM_SENHA_PARA_COMUNICACAO, value);
	}		
	
	// SL7000 - TCP;
	public void typeTextSl7000TcpEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(SL7000_TCP_ENDERECO_OU_IP);
		typeText(SL7000_TCP_ENDERECO_OU_IP, value);
	}		
	public void typeTextSl7000TcpPorta(String value){
		typeText(SL7000_TCP_PORTA, value);
	}		
	public void typeTextSl7000TcpId(String value){
		typeText(SL7000_TCP_ID, value);
	}		
	public void typeTextSl7000TcpTimeout(String value){
		typeText(SL7000_TCP_TIMEOUT, value);
	}		
	public void typeTextSl7000TcpRetentativas(String value){
		typeText(SL7000_TCP_RETENTATIVAS, value);
	}		
	public void typeSelectSl7000TcpTiposDeCliente(String value){
		typeSelectComboOption(SL7000_TCP_TIPOS_DE_CLIENTE, value);
	}		
	public void typeTextSl7000TcpSenha(String value){
		typeText(SL7000_TCP_SENHA, value);
	}		
	

	// SL7000-Ethernet-E3-RT;
	public void typeTextSl7000EthE3RtEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(SL7000_ETH_E3_RT_ENDERECO_OU_IP);
		typeText(SL7000_ETH_E3_RT_ENDERECO_OU_IP, value);
	}		
	public void typeTextSl7000EthE3RtPorta(String value){
		typeText(SL7000_ETH_E3_RT_PORTA, value);
	}		
	public void typeTextSl7000EthE3RtId(String value){
		typeText(SL7000_ETH_E3_RT_ID, value);
	}		
	public void typeCheckBoxSl7000EthE3RtColetarFormasOnda(String value){
		typeCheckOption(SL7000_ETH_E3_RT_COLETAR_FORMAS_ONDA, value);
	}
	public void typeTextSl7000EthE3RtUsuarioParaComunicacao(String value){
		typeText(SL7000_ETH_E3_RT_USUARIO_PARA_COMUNICACAO, value);
	}		
	public void typeTextSl7000EthE3RtSenhaParaComunicacao(String value){
		typeText(SL7000_ETH_E3_RT_SENHA_PARA_COMUNICACAO, value);
	}			
	
	// SmartGreen
	public void typeTextsSmartgreenDeviceId(String value){
		waitForPresenceOfElementLocated(SMARTGREEN_DEVICE_ID);
		typeText(SMARTGREEN_DEVICE_ID, value);
	}		
	
	// STM
	public void typeTextStmStrRemoto(String value){
		waitForPresenceOfElementLocated(STM_STR_REMOTO);
		typeText(STM_STR_REMOTO, value);
	}	
	
	// Tekpea
	public void typeTextTekpeaDeviceId(String value){
		waitForPresenceOfElementLocated(TEKPEA_DEVICE_ID);
		typeText(TEKPEA_DEVICE_ID, value);
	}
	
	// Tekpea
	public void typeTextTnsNumeroDoTransponder(String value){
		waitForPresenceOfElementLocated(TNS_NUMERO_DO_TRANSPONDER);
		typeText(TNS_NUMERO_DO_TRANSPONDER, value);
	}		
	// WebService
	public void typeTextWebserviceIdentificadorRemoto(String value){
		waitForPresenceOfElementLocated(WEBSERVICE_IDENTIFICADOR_REMOTO);
		typeText(WEBSERVICE_IDENTIFICADOR_REMOTO, value);
	}	
	
	// WebService - Hidrologia
	public void typeTextWsHidrologiaCodigoSensorChuva(String value){
		waitForPresenceOfElementLocated(WS_HIDROLOGIA_CODIGO_SENSOR_CHUVA);
		typeText(WS_HIDROLOGIA_CODIGO_SENSOR_CHUVA, value);
	}		
	public void typeTextWsHidrologiaCodigoSensorNivel(String value){
		typeText(WS_HIDROLOGIA_CODIGO_SENSOR_NIVEL, value);
	}		
	
	// Weg ModBus - Ethernet
	public void typeTextWegModbusEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(WEG_MODBUS_ENDERECO_OU_IP);
		typeText(WEG_MODBUS_ENDERECO_OU_IP, value);
	}		
	public void typeTextWegModbusPorta(String value){
		typeText(WEG_MODBUS_PORTA, value);
	}		
	
	// ZIV - Ethernet;
	public void typeTextZivEthEnderecoOuIp(String value){
		waitForPresenceOfElementLocated(ZIV_ETH_ENDERECO_OU_IP);
		typeText(ZIV_ETH_ENDERECO_OU_IP, value);
	}		
	public void typeTextZivEthPorta(String value){
		typeText(ZIV_ETH_PORTA, value);
	}		
	public void typeTextZivEthId(String value){
		typeText(ZIV_ETH_ID, value);
	}		
	public void typeTextZivEthUsuarioParaComunicacao(String value){
		typeText(ZIV_ETH_USUARIO_PARA_COMUNICACAO, value);
	}		
	public void typeTextZivEthSenhaParaComunicacao(String value){
		typeText(ZIV_ETH_SENHA_PARA_COMUNICACAO, value);
	}		
	
}