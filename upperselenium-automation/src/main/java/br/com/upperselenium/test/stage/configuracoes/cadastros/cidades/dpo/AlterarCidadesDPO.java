package br.com.upperselenium.test.stage.configuracoes.cadastros.cidades.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarCidadesDPO extends BaseHelperDPO {

	private String nome;
	private String estado;
	private String regiao;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getRegiao() {
		return regiao;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

}
