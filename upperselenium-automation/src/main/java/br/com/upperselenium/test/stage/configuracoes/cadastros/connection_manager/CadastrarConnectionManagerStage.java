package br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.base.util.WaitElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager.dpo.CadastrarConnectionManagerDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager.page.CadastroConnectionManagerPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager.page.ConnectionManagerPage;

public class CadastrarConnectionManagerStage extends BaseStage {
	private CadastrarConnectionManagerDPO cadastrarConnectionManagerDPO;
	private ConnectionManagerPage connectionManagerPage;
	private CadastroConnectionManagerPage cadastroConnectionManagerPage;
	
	public CadastrarConnectionManagerStage(String dp) {
		cadastrarConnectionManagerDPO = loadDataProviderFile(CadastrarConnectionManagerDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		connectionManagerPage = initElementsFromPage(ConnectionManagerPage.class);
		cadastroConnectionManagerPage = initElementsFromPage(CadastroConnectionManagerPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarConnectionManagerDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		connectionManagerPage.clickNovo();
		cadastroConnectionManagerPage.typeTextHub(cadastrarConnectionManagerDPO.getHub()+getRandomStringNonSpaced());
		cadastroConnectionManagerPage.typeCheckBoxHabilitado(cadastrarConnectionManagerDPO.getHabilitado());
		cadastroConnectionManagerPage.typeCheckBoxRegistrarConexoes(cadastrarConnectionManagerDPO.getRegistrarConexoes());
		for (int index=0; index < cadastrarConnectionManagerDPO.getGridConnectionManager().size(); index++){
			cadastroConnectionManagerPage.typeGridTextCanal(cadastrarConnectionManagerDPO.getGridConnectionManager().get(index).getCanal(), index+1);
			cadastroConnectionManagerPage.typeGridTextIP(cadastrarConnectionManagerDPO.getGridConnectionManager().get(index).getIp(), index+1);
			cadastroConnectionManagerPage.typeGridTextPorta(cadastrarConnectionManagerDPO.getGridConnectionManager().get(index).getPorta(), index+1);
			cadastroConnectionManagerPage.typeGridTextTimeout(cadastrarConnectionManagerDPO.getGridConnectionManager().get(index).getTimeout(), index+1);
			cadastroConnectionManagerPage.typeGridSelectTipo(cadastrarConnectionManagerDPO.getGridConnectionManager().get(index).getTipo(), index+1);
			cadastroConnectionManagerPage.typeGridCheckBoxNovaConexaoFechaAntiga(cadastrarConnectionManagerDPO.getGridConnectionManager().get(index).getNovaConexaoFechaAntiga(), index);
			cadastroConnectionManagerPage.typeGridCheckBoxReiniciarHubAoFecharAntiga(cadastrarConnectionManagerDPO.getGridConnectionManager().get(index).getReiniciarHubAoFecharAntiga(), index);
			cadastroConnectionManagerPage.typeGridCheckBoxAlwaysOn(cadastrarConnectionManagerDPO.getGridConnectionManager().get(index).getAlwaysOn(), index);
			cadastroConnectionManagerPage.clickGridAdd(cadastrarConnectionManagerDPO.getGridConnectionManager().get(index).getButtonAdd(), index+1);
			cadastroConnectionManagerPage.typeGridSelectPrioridade(cadastrarConnectionManagerDPO.getGridConnectionManager().get(index).getPrioridade(), index+1);
			cadastroConnectionManagerPage.clickGridExclude(cadastrarConnectionManagerDPO.getGridConnectionManager().get(index).getButtonExclude(), index+1);
		}
		WaitElementUtil.waitForATime(TimePRM._3_SECS);
		cadastroConnectionManagerPage.useKeyPageDown();
		cadastroConnectionManagerPage.clickSalvar();
		cadastroConnectionManagerPage.useKeyPageDown();
	}

	@Override
	public void runValidations() {
		cadastroConnectionManagerPage.validateMessageDefault(cadastrarConnectionManagerDPO.getOperationMessage());
	}

}
