package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaAnemometriaPontosDPO extends BaseHelperDPO {

	private String codigoDaEstacao;
	private List<GroupSalvarInstrumentoAnemometriaDPO> groupSalvarInstrumento;
	private List<GridAdicionarVigenciaAnemometriaDPO> gridVigenciaAnemometria;

	public String getCodigoDaEstacao() {
		return codigoDaEstacao;
	}

	public void setCodigoDaEstacao(String codigoDaEstacao) {
		this.codigoDaEstacao = codigoDaEstacao;
	}

	public List<GroupSalvarInstrumentoAnemometriaDPO> getGroupSalvarInstrumento() {
		return groupSalvarInstrumento;
	}

	public void setGroupSalvarInstrumento(List<GroupSalvarInstrumentoAnemometriaDPO> groupSalvarInstrumento) {
		this.groupSalvarInstrumento = groupSalvarInstrumento;
	}

	public List<GridAdicionarVigenciaAnemometriaDPO> getGridVigenciaAnemometria() {
		return gridVigenciaAnemometria;
	}

	public void setGridVigenciaAnemometria(List<GridAdicionarVigenciaAnemometriaDPO> gridVigenciaAnemometria) {
		this.gridVigenciaAnemometria = gridVigenciaAnemometria;
	}

}