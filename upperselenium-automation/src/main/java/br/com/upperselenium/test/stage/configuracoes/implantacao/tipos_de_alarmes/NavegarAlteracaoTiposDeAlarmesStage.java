package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_alarmes;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_alarmes.dpo.NavegarAlterarTiposDeAlarmesDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_alarmes.page.AlteracaoTiposDeAlarmesPage;

public class NavegarAlteracaoTiposDeAlarmesStage extends BaseStage {
	private NavegarAlterarTiposDeAlarmesDPO navegarAlteracaoTiposDeAlarmesDPO;
	private AlteracaoTiposDeAlarmesPage alteracaoTiposDeAlarmesPage;
	
	public NavegarAlteracaoTiposDeAlarmesStage(String dp) {
		navegarAlteracaoTiposDeAlarmesDPO = loadDataProviderFile(NavegarAlterarTiposDeAlarmesDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alteracaoTiposDeAlarmesPage = initElementsFromPage(AlteracaoTiposDeAlarmesPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAlteracaoTiposDeAlarmesDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		alteracaoTiposDeAlarmesPage.getLabelHeaderNome(navegarAlteracaoTiposDeAlarmesDPO.getLabelNome());
		alteracaoTiposDeAlarmesPage.getLabelHeaderMotivo(navegarAlteracaoTiposDeAlarmesDPO.getLabelMotivo());
		alteracaoTiposDeAlarmesPage.getLabelHeaderCriticidade(navegarAlteracaoTiposDeAlarmesDPO.getLabelCriticidade());
		alteracaoTiposDeAlarmesPage.getLabelHeaderStatus(navegarAlteracaoTiposDeAlarmesDPO.getLabelStatus());
		alteracaoTiposDeAlarmesPage.getBreadcrumbsText(navegarAlteracaoTiposDeAlarmesDPO.getBreadcrumbs());
		alteracaoTiposDeAlarmesPage.clickUpToBreadcrumbs(navegarAlteracaoTiposDeAlarmesDPO.getUpToBreadcrumb());
		alteracaoTiposDeAlarmesPage.getWelcomeTitle(navegarAlteracaoTiposDeAlarmesDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
