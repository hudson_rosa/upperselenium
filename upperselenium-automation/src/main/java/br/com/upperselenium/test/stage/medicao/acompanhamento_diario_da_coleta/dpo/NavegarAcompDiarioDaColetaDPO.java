package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_da_coleta.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarAcompDiarioDaColetaDPO extends BaseHelperDPO {

	private String labelPasta;
	private String labelTipoDeVisualizacao;
	private String labelMes;
	private String labelStatus;
	private String labelCationVazio;
	private String labelCationParcial;
	private String labelCationCompleto;
	private String labelHeaderNome;
	private String labelHeaderNumeroDeSerie;
	private String labelHeaderCodigoScde;
	private String labelHeaderFuncao;
	private String label1;
	private String label2;
	private String label3;
	private String label4;
	private String label5;
	private String label6;
	private String label7;
	private String label8;
	private String label9;
	private String label10;
	private String label11;
	private String label12;
	private String label13;
	private String label14;
	private String label15;
	private String label16;
	private String label17;
	private String label18;
	private String label19;
	private String label20;
	private String label21;
	private String label22;
	private String label23;
	private String label24;
	private String label25;
	private String label26;
	private String label27;
	private String label28;
	private String label29;
	private String label30;

	public String getLabelPasta() {
		return labelPasta;
	}

	public void setLabelPasta(String labelPasta) {
		this.labelPasta = labelPasta;
	}

	public String getLabelTipoDeVisualizacao() {
		return labelTipoDeVisualizacao;
	}

	public void setLabelTipoDeVisualizacao(String labelTipoDeVisualizacao) {
		this.labelTipoDeVisualizacao = labelTipoDeVisualizacao;
	}

	public String getLabelMes() {
		return labelMes;
	}

	public void setLabelMes(String labelMes) {
		this.labelMes = labelMes;
	}

	public String getLabelStatus() {
		return labelStatus;
	}

	public void setLabelStatus(String labelStatus) {
		this.labelStatus = labelStatus;
	}

	public String getLabelCationVazio() {
		return labelCationVazio;
	}

	public void setLabelCationVazio(String labelCationVazio) {
		this.labelCationVazio = labelCationVazio;
	}

	public String getLabelCationParcial() {
		return labelCationParcial;
	}

	public void setLabelCationParcial(String labelCationParcial) {
		this.labelCationParcial = labelCationParcial;
	}

	public String getLabelCationCompleto() {
		return labelCationCompleto;
	}

	public void setLabelCationCompleto(String labelCationCompleto) {
		this.labelCationCompleto = labelCationCompleto;
	}

	public String getLabelHeaderNome() {
		return labelHeaderNome;
	}

	public void setLabelHeaderNome(String labelHeaderNome) {
		this.labelHeaderNome = labelHeaderNome;
	}

	public String getLabelHeaderNumeroDeSerie() {
		return labelHeaderNumeroDeSerie;
	}

	public void setLabelHeaderNumeroDeSerie(String labelHeaderNumeroDeSerie) {
		this.labelHeaderNumeroDeSerie = labelHeaderNumeroDeSerie;
	}

	public String getLabelHeaderCodigoScde() {
		return labelHeaderCodigoScde;
	}

	public void setLabelHeaderCodigoScde(String labelHeaderCodigoScde) {
		this.labelHeaderCodigoScde = labelHeaderCodigoScde;
	}

	public String getLabelHeaderFuncao() {
		return labelHeaderFuncao;
	}

	public void setLabelHeaderFuncao(String labelHeaderFuncao) {
		this.labelHeaderFuncao = labelHeaderFuncao;
	}

	public String getLabel1() {
		return label1;
	}

	public void setLabel1(String label1) {
		this.label1 = label1;
	}

	public String getLabel2() {
		return label2;
	}

	public void setLabel2(String label2) {
		this.label2 = label2;
	}

	public String getLabel3() {
		return label3;
	}

	public void setLabel3(String label3) {
		this.label3 = label3;
	}

	public String getLabel4() {
		return label4;
	}

	public void setLabel4(String label4) {
		this.label4 = label4;
	}

	public String getLabel5() {
		return label5;
	}

	public void setLabel5(String label5) {
		this.label5 = label5;
	}

	public String getLabel6() {
		return label6;
	}

	public void setLabel6(String label6) {
		this.label6 = label6;
	}

	public String getLabel7() {
		return label7;
	}

	public void setLabel7(String label7) {
		this.label7 = label7;
	}

	public String getLabel8() {
		return label8;
	}

	public void setLabel8(String label8) {
		this.label8 = label8;
	}

	public String getLabel9() {
		return label9;
	}

	public void setLabel9(String label9) {
		this.label9 = label9;
	}

	public String getLabel10() {
		return label10;
	}

	public void setLabel10(String label10) {
		this.label10 = label10;
	}

	public String getLabel11() {
		return label11;
	}

	public void setLabel11(String label11) {
		this.label11 = label11;
	}

	public String getLabel12() {
		return label12;
	}

	public void setLabel12(String label12) {
		this.label12 = label12;
	}

	public String getLabel13() {
		return label13;
	}

	public void setLabel13(String label13) {
		this.label13 = label13;
	}

	public String getLabel14() {
		return label14;
	}

	public void setLabel14(String label14) {
		this.label14 = label14;
	}

	public String getLabel15() {
		return label15;
	}

	public void setLabel15(String label15) {
		this.label15 = label15;
	}

	public String getLabel16() {
		return label16;
	}

	public void setLabel16(String label16) {
		this.label16 = label16;
	}

	public String getLabel17() {
		return label17;
	}

	public void setLabel17(String label17) {
		this.label17 = label17;
	}

	public String getLabel18() {
		return label18;
	}

	public void setLabel18(String label18) {
		this.label18 = label18;
	}

	public String getLabel19() {
		return label19;
	}

	public void setLabel19(String label19) {
		this.label19 = label19;
	}

	public String getLabel20() {
		return label20;
	}

	public void setLabel20(String label20) {
		this.label20 = label20;
	}

	public String getLabel21() {
		return label21;
	}

	public void setLabel21(String label21) {
		this.label21 = label21;
	}

	public String getLabel22() {
		return label22;
	}

	public void setLabel22(String label22) {
		this.label22 = label22;
	}

	public String getLabel23() {
		return label23;
	}

	public void setLabel23(String label23) {
		this.label23 = label23;
	}

	public String getLabel24() {
		return label24;
	}

	public void setLabel24(String label24) {
		this.label24 = label24;
	}

	public String getLabel25() {
		return label25;
	}

	public void setLabel25(String label25) {
		this.label25 = label25;
	}

	public String getLabel26() {
		return label26;
	}

	public void setLabel26(String label26) {
		this.label26 = label26;
	}

	public String getLabel27() {
		return label27;
	}

	public void setLabel27(String label27) {
		this.label27 = label27;
	}

	public String getLabel28() {
		return label28;
	}

	public void setLabel28(String label28) {
		this.label28 = label28;
	}

	public String getLabel29() {
		return label29;
	}

	public void setLabel29(String label29) {
		this.label29 = label29;
	}

	public String getLabel30() {
		return label30;
	}

	public void setLabel30(String label30) {
		this.label30 = label30;
	}

	@Override
	public String toString() {
		return "NavegarAcompDiarioDaColetaDPO [labelPasta=" + labelPasta + ", labelTipoDeVisualizacao="
				+ labelTipoDeVisualizacao + ", labelMes=" + labelMes + ", labelStatus=" + labelStatus
				+ ", labelCationVazio=" + labelCationVazio + ", labelCationParcial=" + labelCationParcial
				+ ", labelCationCompleto=" + labelCationCompleto + ", labelHeaderNome=" + labelHeaderNome
				+ ", labelHeaderNumeroDeSerie=" + labelHeaderNumeroDeSerie + ", labelHeaderCodigoScde="
				+ labelHeaderCodigoScde + ", labelHeaderFuncao=" + labelHeaderFuncao + ", label1=" + label1
				+ ", label2=" + label2 + ", label3=" + label3 + ", label4=" + label4 + ", label5=" + label5
				+ ", label6=" + label6 + ", label7=" + label7 + ", label8=" + label8 + ", label9=" + label9
				+ ", label10=" + label10 + ", label11=" + label11 + ", label12=" + label12 + ", label13=" + label13
				+ ", label14=" + label14 + ", label15=" + label15 + ", label16=" + label16 + ", label17=" + label17
				+ ", label18=" + label18 + ", label19=" + label19 + ", label20=" + label20 + ", label21=" + label21
				+ ", label22=" + label22 + ", label23=" + label23 + ", label24=" + label24 + ", label25=" + label25
				+ ", label26=" + label26 + ", label27=" + label27 + ", label28=" + label28 + ", label29=" + label29
				+ ", label30=" + label30 + "]";
	}

}