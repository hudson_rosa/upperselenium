package br.com.upperselenium.test.stage.configuracoes.cadastros.classes_de_isolacao.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarClassesDeIsolacaoDPO extends BaseHelperDPO {

	private String valor;
	private String descricao;

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
