package br.com.upperselenium.test.stage.configuracoes.cadastros.tensoes.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarTensoesDPO extends BaseHelperDPO {

	private String valor;

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}