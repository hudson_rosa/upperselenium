package br.com.upperselenium.test.stage.configuracoes.cadastros.ramo_de_atividade;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.AreasMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.DropDownMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.SideBarCadastrosPage;

public class GoToRamoDeAtividadeStage extends BaseStage {
	private AreasMenuPage areasMenuPage;
	private DropDownMenuPage dropDownMenuPage;
	private SideBarCadastrosPage sideBarCadastrosPage;
	
	public GoToRamoDeAtividadeStage() {}

	@Override
	public void initMappedPages() {
		areasMenuPage = initElementsFromPage(AreasMenuPage.class);
		dropDownMenuPage = initElementsFromPage(DropDownMenuPage.class);
		sideBarCadastrosPage = initElementsFromPage(SideBarCadastrosPage.class);
	}

	@Override
	public void runStage() {
		navigateToPage();	
	}
	
	private void navigateToPage() {
		waitForPageToLoad(TimePRM._20_SECS);
		areasMenuPage.clickConfiguracoes();
		waitForPageToLoadUntil10s();
		dropDownMenuPage.clickCadastros();
		waitForPageToLoadUntil10s();
		sideBarCadastrosPage.clickRamoDeAtividade();
		waitForPageToLoadUntil10s();		
	}

	@Override
	public void runValidations() {}

}
