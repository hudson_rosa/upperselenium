package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarAcompDiarioDeUsinasDPO extends BaseHelperDPO {

	private String labelUsina;
	private String labelData;
	private String clickFiltrar;
	private String labelHeaderGeracao;
	private String labelHeaderNivel;
	private String labelHeaderEficiencia;
	private String labelHeaderVazao;

	public String getLabelUsina() {
		return labelUsina;
	}

	public void setLabelUsina(String labelUsina) {
		this.labelUsina = labelUsina;
	}

	public String getLabelData() {
		return labelData;
	}

	public void setLabelData(String labelData) {
		this.labelData = labelData;
	}

	public String getClickFiltrar() {
		return clickFiltrar;
	}

	public void setClickFiltrar(String clickFiltrar) {
		this.clickFiltrar = clickFiltrar;
	}

	public String getLabelHeaderGeracao() {
		return labelHeaderGeracao;
	}

	public void setLabelHeaderGeracao(String labelHeaderGeracao) {
		this.labelHeaderGeracao = labelHeaderGeracao;
	}

	public String getLabelHeaderNivel() {
		return labelHeaderNivel;
	}

	public void setLabelHeaderNivel(String labelHeaderNivel) {
		this.labelHeaderNivel = labelHeaderNivel;
	}

	public String getLabelHeaderEficiencia() {
		return labelHeaderEficiencia;
	}

	public void setLabelHeaderEficiencia(String labelHeaderEficiencia) {
		this.labelHeaderEficiencia = labelHeaderEficiencia;
	}

	public String getLabelHeaderVazao() {
		return labelHeaderVazao;
	}

	public void setLabelHeaderVazao(String labelHeaderVazao) {
		this.labelHeaderVazao = labelHeaderVazao;
	}

	@Override
	public String toString() {
		return "NavegarAcompDiarioDaColetaDPO [labelUsina=" + labelUsina + ", labelData=" + labelData
				+ ", clickFiltrar=" + clickFiltrar + ", labelHeaderGeracao=" + labelHeaderGeracao
				+ ", labelHeaderNivel=" + labelHeaderNivel + ", labelHeaderEficiencia=" + labelHeaderEficiencia
				+ ", labelHeaderVazao=" + labelHeaderVazao + "]";
	}

}