package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarAbaAtivosMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarAbaCCEEMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarAbaComunicacaoMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarAbaConfiguracoesMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarAbaInspecaoLogicaMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarAbaSincronismoMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.GoToMedidoresStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0014MedidoresRetaguardaCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0014-MedidoresRetaguarda", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Medidores Retaguarda realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0014MedidoresRetaguardaCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToMedidoresStage());
		addStage(new CadastrarMedidoresStage(getDP("CadastrarMedidoresRetaguardaDP.json")));
		addStage(new CadastrarAbaConfiguracoesMedidoresStage(getDP("CadastrarAbaConfiguracoesMedidoresRetaguardaDP.json")));
		addStage(new CadastrarAbaComunicacaoMedidoresStage(getDP("CadastrarAbaComunicacaoMedidoresRetaguardaDP.json")));
		addStage(new CadastrarAbaCCEEMedidoresStage(getDP("CadastrarAbaCCEEMedidoresRetaguardaDP.json")));
		addStage(new CadastrarAbaAtivosMedidoresStage(getDP("CadastrarAbaAtivosMedidoresRetaguardaDP.json")));
		addStage(new CadastrarAbaSincronismoMedidoresStage(getDP("CadastrarAbaSincronismoMedidoresRetaguardaDP.json")));
		addStage(new CadastrarAbaInspecaoLogicaMedidoresStage(getDP("CadastrarAbaInspecaoLogicaMedidoresRetaguardaDP.json")));
	}	
}
