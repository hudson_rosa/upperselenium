package br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.page;

import org.openqa.selenium.By;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroClientsSCDEPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='client-scde']/div[7]/input"; 
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String SELECT_TIPO = "//*[@id='PastaPrimaria_Tipo']";
	private static final String TEXT_PATH = "//*[@id='PastaPrimaria_Path']";
		
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	
	
	public void typeSelectTipo(String value){		
		typeSelectComboOption(SELECT_TIPO, value);
	}
	
	public void typeTextPath(String value){
		typeText(TEXT_PATH, value);
	}	
			
	public boolean isClickableButtonSalvar() {
		return isDisplayedElement(By.xpath(BUTTON_SALVAR));
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}