package br.com.upperselenium.test.stage.wrapper.navigation;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.WebDriverMaster;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.base.util.WaitElementUtil;
import br.com.upperselenium.test.stage.wrapper.navigation.page.MockElementValidationPage;

public class MockElementValidationStage extends BaseStage {
	private String urlMocked;
	private MockElementValidationPage mockElementPage;
	private String value1 = "Integralização / Expressão da Equação";
	private String value2 = "Integralização / Expressão da Equação";
	private String value3 = "TESTE3";
	
	/**
	 * Na classe de Flow, declare o Stage da seguinte maneira: <br><br>
	 * 				addStage(new MockElementValidationStage("http://localhost/app..."));
	 * @param url
	 */
	
	public MockElementValidationStage(String url) {
		urlMocked = url;
	}
	
	@Override
	public void initMappedPages() {
		mockElementPage = initElementsFromPage(MockElementValidationPage.class);
	}
	
	@Override
	public void runStage() {		
		validateElements();
		ValidateFirstElement();
		validateSecondElement();
		validateThirdElement();
	}
	
	private void validateElements() {
		WebDriverMaster.getWebDriver().get(urlMocked);
		waitForPageToLoadUntil10s();	
	}

	private void ValidateFirstElement() {
//		mockElementPage.clickToValidate1();
//		mockElementPage.getLabelToValidate1(value1);
		mockElementPage.getGridLabelToValidate1(value1, 1);
//		mockElementPage.typeTextToValidate1(value1);
//		mockElementPage.typeCheckToValidate1(value1);
//		mockElementPage.typeSelectToValidate1(value1);
	}

	private void validateSecondElement() {
		WaitElementUtil.waitForATime(TimePRM._2_SECS);		
//		mockElementPage.clickToValidate2();
//		mockElementPage.getLabelToValidate2(value2);
		mockElementPage.getGridLabelToValidate2(value2, 2);
//		mockElementPage.typeTextToValidate2(value2);
//		mockElementPage.typeCheckToValidate2(value2);
//		mockElementPage.typeSelectToValidate2(value2);
	}

	private void validateThirdElement() {
//		mockElementPage.clickToValidate3();
//		mockElementPage.getLabelToValidate3(value3);
		mockElementPage.getGridLabelToValidate3(value3, 3);
//		mockElementPage.typeTextToValidate3(value3);
//		mockElementPage.typeCheckToValidate3(value3);
//		mockElementPage.typeSelectToValidate3(value3);
	}
		
	@Override
	public void runValidations() {}

}
