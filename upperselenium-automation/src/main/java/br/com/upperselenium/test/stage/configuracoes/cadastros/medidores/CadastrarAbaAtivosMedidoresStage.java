package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo.CadastrarAbaAtivosMedidoresDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page.CadastroAbaAtivosMedidoresPage;

public class CadastrarAbaAtivosMedidoresStage extends BaseStage {
	private CadastrarAbaAtivosMedidoresDPO cadastrarAbaAtivosMedidoresDPO;
	private CadastroAbaAtivosMedidoresPage cadastroAbaAtivosMedidoresPage;
	
	public CadastrarAbaAtivosMedidoresStage(String dp) {
		cadastrarAbaAtivosMedidoresDPO = loadDataProviderFile(CadastrarAbaAtivosMedidoresDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaAtivosMedidoresPage = initElementsFromPage(CadastroAbaAtivosMedidoresPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaAtivosMedidoresDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaAtivosMedidoresPage.clickAbaAtivos();
		FindElementUtil.acceptAlert();
		cadastroAbaAtivosMedidoresPage.typeTextFabricacao(cadastrarAbaAtivosMedidoresDPO.getFabricacao());		
		cadastroAbaAtivosMedidoresPage.typeTextInicioDaGarantia(cadastrarAbaAtivosMedidoresDPO.getInicioDaGarantia());			
		cadastroAbaAtivosMedidoresPage.typeTextFimDaGarantia(cadastrarAbaAtivosMedidoresDPO.getFimDaGarantia());			
		cadastroAbaAtivosMedidoresPage.typeTextLacre(cadastrarAbaAtivosMedidoresDPO.getLacre());			
		cadastroAbaAtivosMedidoresPage.typeTextPropriedade(cadastrarAbaAtivosMedidoresDPO.getPropriedade());			
		cadastroAbaAtivosMedidoresPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaAtivosMedidoresPage.validateMessageDefault(cadastrarAbaAtivosMedidoresDPO.getOperationMessage());
	}

}
