package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarAbaColetasMedidoresDPO extends BaseHelperDPO {

	private String filterGrandeza;
	private String filterDataDaUltimaColeta;
	private String modalDataDaUltimaColeta;
	private String checkAll;
	private List<GridAlterarColetasDPO> gridColetas;

	public String getFilterGrandeza() {
		return filterGrandeza;
	}

	public void setFilterGrandeza(String filterGrandeza) {
		this.filterGrandeza = filterGrandeza;
	}

	public String getFilterDataDaUltimaColeta() {
		return filterDataDaUltimaColeta;
	}

	public void setFilterDataDaUltimaColeta(String filterDataDaUltimaColeta) {
		this.filterDataDaUltimaColeta = filterDataDaUltimaColeta;
	}

	public String getModalDataDaUltimaColeta() {
		return modalDataDaUltimaColeta;
	}

	public void setModalDataDaUltimaColeta(String modalDataDaUltimaColeta) {
		this.modalDataDaUltimaColeta = modalDataDaUltimaColeta;
	}

	public String getCheckAll() {
		return checkAll;
	}

	public void setCheckAll(String checkAll) {
		this.checkAll = checkAll;
	}

	public List<GridAlterarColetasDPO> getGridColetas() {
		return gridColetas;
	}

	public void setGridColetas(List<GridAlterarColetasDPO> gridColetas) {
		this.gridColetas = gridColetas;
	}

}