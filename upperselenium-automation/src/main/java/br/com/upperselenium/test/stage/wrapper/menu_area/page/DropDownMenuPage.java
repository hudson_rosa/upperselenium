package br.com.upperselenium.test.stage.wrapper.menu_area.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class DropDownMenuPage extends BaseHelperPage{
	
	private static final String XPATH_MAIN_MENU = ".//div[1]/div/div/div/ul/";
	
	// CONFIGURAÇÕES
	private static final String LINK_ADMINISTRACAO = XPATH_MAIN_MENU + "li[1]/ul/li[1]/a[contains(text(), 'Administração')]";
	private static final String LINK_CADASTROS = XPATH_MAIN_MENU + "li[1]/ul/li[2]/a[contains(text(), 'Cadastros')]";
	private static final String LINK_IMPLANTACAO = XPATH_MAIN_MENU + "li[1]/ul/li[3]/a[contains(text(), 'Implantação')]";
	
	// MEDIÇÃO
	private static final String LINK_MEDICAO = XPATH_MAIN_MENU + "li[2]/a[contains(text(), 'Medição')]";

	// ANÁLISES
	private static final String LINK_ANALITICO_DE_ALARME = XPATH_MAIN_MENU + "li[3]/ul/li[1]/a[contains(text(), 'Analítico de Alarme')]";
	private static final String LINK_ANEMOMETRIA = XPATH_MAIN_MENU + "li[3]/ul/li[2]/a[contains(text(), 'Anemometria')]";
	private static final String LINK_BALANCO_ENERGETICO = XPATH_MAIN_MENU + "li[3]/ul/li[3]/a[contains(text(), 'Balanço Energético')]";
	private static final String LINK_EXTRATOR = XPATH_MAIN_MENU + "li[3]/ul/li[4]/a[contains(text(), 'Extrator')]";
	private static final String LINK_GRAFICO_HISTORICO = XPATH_MAIN_MENU + "li[3]/ul/li[5]/a[contains(text(), 'Gráfico Histórico')]";
	private static final String LINK_GRAFICOS = XPATH_MAIN_MENU + "li[3]/ul/li[6]/a[contains(text(), 'Gráficos')]";
	private static final String LINK_GRAFICOS_CHARTFX = XPATH_MAIN_MENU + "li[3]/ul/li[7]/a[contains(text(), 'Gráficos ChartFX')]";
	private static final String LINK_MESA_DE_OPERACOES = XPATH_MAIN_MENU + "li[3]/ul/li[8]/a[contains(text(), 'Mesa de Operações')]";
	private static final String LINK_QUALIDADE = XPATH_MAIN_MENU + "li[3]/ul/li[9]/a[contains(text(), 'Qualidade')]";
	private static final String LINK_RELATORIOS = XPATH_MAIN_MENU + "li[3]/ul/li[10]/a[contains(text(), 'Relatórios')]";
	
	// CCEE
	private static final String LINK_ENVIO_DE_XML = XPATH_MAIN_MENU + "li[4]/ul/li[1]/a[contains(text(), 'Envio de XML')]";
	private static final String LINK_INSPECAO_LOGICA = XPATH_MAIN_MENU + "li[4]/ul/li[2]/a[contains(text(), 'Inspeção Lógica')]";
	private static final String LINK_MEDIDAS_CONSOLIDADAS = XPATH_MAIN_MENU + "li[4]/ul/li[3]/a[contains(text(), 'Medidas Consolidadas')]";

	// MONITORAMENTO
	private static final String LINK_ALARMES = XPATH_MAIN_MENU + "li[7]/ul/li[1]/a[contains(text(), 'Alarmes')]";
	private static final String LINK_MAPA = XPATH_MAIN_MENU + "li[7]/ul/li[2]/a[contains(text(), 'Mapa')]";
	private static final String LINK_PAGINAS_FISCAIS = XPATH_MAIN_MENU + "li[7]/ul/li[3]/a[contains(text(), 'Páginas Fiscais')]";
	private static final String LINK_TEMPO_REAL = XPATH_MAIN_MENU + "li[7]/ul/li[4]/a[contains(text(), 'Tempo Real')]";
	
		
	public boolean isClickableLinkAdministracao() {
		return isDisplayedElement(LINK_ADMINISTRACAO);
	}
	
	public boolean isClickableLinkCadastros() {
		return isDisplayedElement(LINK_CADASTROS);
	}
	
	public boolean isClickableLinkImplantacao() {
		return isDisplayedElement(LINK_IMPLANTACAO);
	}
	
	public boolean isClickableLinkMedicao() {
		return isDisplayedElement(LINK_MEDICAO);
	}
	
	public boolean isClickableLinkAnaliticoDeAlarme() {
		return isDisplayedElement(LINK_ANALITICO_DE_ALARME);
	}
	
	public boolean isClickableLinkAnemometria() {
		return isDisplayedElement(LINK_ANEMOMETRIA);
	}
	
	public boolean isClickableLinkBalancoEnergetico() {
		return isDisplayedElement(LINK_BALANCO_ENERGETICO);
	}
	
	public boolean isClickableLinkExtrator() {
		return isDisplayedElement(LINK_EXTRATOR);
	}
	
	public boolean isClickableLinkGraficoHistorico() {
		return isDisplayedElement(LINK_GRAFICO_HISTORICO);
	}
	
	public boolean isClickableLinkGraficos() {
		return isDisplayedElement(LINK_GRAFICOS);
	}
	
	public boolean isClickableLinkGraficosChartFX() {
		return isDisplayedElement(LINK_GRAFICOS_CHARTFX);
	}
	
	public boolean isClickableLinkMesaDeOperacoes() {
		return isDisplayedElement(LINK_MESA_DE_OPERACOES);
	}
	
	public boolean isClickableLinkQualidade() {
		return isDisplayedElement(LINK_QUALIDADE);
	}
	
	public boolean isClickableLinkRelatorios() {
		return isDisplayedElement(LINK_RELATORIOS);
	}
	
	public boolean isClickableLinkEnviodeXML() {
		return isDisplayedElement(LINK_ENVIO_DE_XML);
	}
	
	public boolean isClickableLinkInspecaoLogica() {
		return isDisplayedElement(LINK_INSPECAO_LOGICA);
	}
	
	public boolean isClickableLinkMedidasConsolidadas() {
		return isDisplayedElement(LINK_MEDIDAS_CONSOLIDADAS);
	}
	
	public boolean isClickableLinkAlarmes() {
		return isDisplayedElement(LINK_ALARMES);
	}
	
	public boolean isClickableLinkMapa() {
		return isDisplayedElement(LINK_MAPA);
	}
	
	public boolean isClickableLinkPaginasFiscais() {
		return isDisplayedElement(LINK_PAGINAS_FISCAIS);
	}
	
	public boolean isClickableLinkTempoReal() {
		return isDisplayedElement(LINK_TEMPO_REAL);
	}

	public void clickAdministracao(){
		clickOnElement(LINK_ADMINISTRACAO);
	}
	
	public void clickCadastros(){
		clickOnElement(LINK_CADASTROS);
	}
	
	public void clickImplantacao(){
		clickOnElement(LINK_IMPLANTACAO);
	}
	
	public void clickMedicao(){
		clickOnElement(LINK_MEDICAO);
	}
	
	public void clickAnaliticoDeAlarme(){
		clickOnElement(LINK_ANALITICO_DE_ALARME);
	}
	
	public void clickAnemometria(){
		clickOnElement(LINK_ANEMOMETRIA);
	}
	
	public void clickBalancoEnergetico(){
		clickOnElement(LINK_BALANCO_ENERGETICO);
	}
	
	public void clickExtrator(){
		clickOnElement(LINK_EXTRATOR);
	}
	
	public void clickGraficoHistorico(){
		clickOnElement(LINK_GRAFICO_HISTORICO);
	}
	
	public void clickGraficos(){
		clickOnElement(LINK_GRAFICOS);
	}
	
	public void clickGraficosChartFX(){
		clickOnElement(LINK_GRAFICOS_CHARTFX);
	}
	
	public void clickMesaDeOperacoes(){
		clickOnElement(LINK_MESA_DE_OPERACOES);
	}
	
	public void clickQualidade(){
		clickOnElement(LINK_QUALIDADE);
	}
	
	public void clickRelatorios(){
		clickOnElement(LINK_RELATORIOS);
	}
	
	public void clickEnviodeXML(){
		clickOnElement(LINK_ENVIO_DE_XML);
	}
	
	public void clickInspecaoLogica(){
		clickOnElement(LINK_INSPECAO_LOGICA);
	}
	
	public void clickMedidasConsolidadas(){
		clickOnElement(LINK_MEDIDAS_CONSOLIDADAS);
	}
	
	public void clickAlarmes(){
		clickOnElement(LINK_ALARMES);
	}
	
	public void clickMapa(){
		clickOnElement(LINK_MAPA);
	}
	
	public void clickPaginasFiscais(){
		clickOnElement(LINK_PAGINAS_FISCAIS);
	}
	
	public void clickTempoReal(){
		clickOnElement(LINK_TEMPO_REAL);
	}

}