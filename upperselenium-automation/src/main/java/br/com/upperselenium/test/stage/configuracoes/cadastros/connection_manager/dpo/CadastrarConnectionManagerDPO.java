package br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarConnectionManagerDPO extends BaseHelperDPO {

	private String hub;
	private String habilitado;
	private String registrarConexoes;
	private List<GridCadastrarConnectionManagerDPO> gridConnectionManager;

	public String getHub() {
		return hub;
	}

	public void setHub(String hub) {
		this.hub = hub;
	}

	public String getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}

	public String getRegistrarConexoes() {
		return registrarConexoes;
	}

	public void setRegistrarConexoes(String registrarConexoes) {
		this.registrarConexoes = registrarConexoes;
	}

	public List<GridCadastrarConnectionManagerDPO> getGridConnectionManager() {
		return gridConnectionManager;
	}

	public void setGridConnectionManager(List<GridCadastrarConnectionManagerDPO> gridConnectionManager) {
		this.gridConnectionManager = gridConnectionManager;
	}

}