package br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaEnderecoSubestacaoPage extends BaseHelperPage{
	
	private static final String LINK_ABA_ENDERECO = ".//div[2]/div/div[2]/ul[2]/li[2]/a";  
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 	
	private static final String TEXT_LOGRADOURO = "//*[@id='Logradouro']"; 
	private static final String TEXT_NUMERO = "//*[@id='Numero']"; 
	private static final String TEXT_BAIRRO = "//*[@id='Bairro']"; 
	private static final String TEXT_CEP = "//*[@id='Cep']";	
	private static final String SELECT_CIDADE = "//*[@id='Cidade']";
	private static final String SELECT_REGIAO = "//*[@id='Regiao']";
	private static final String TEXT_LATITUDE = "//*[@id='Latitude']";	
	private static final String TEXT_LONGITUDE = "//*[@id='Longitude']";	
	
	public void typeTextLogradouro(String value){
		typeText(TEXT_LOGRADOURO, value);
	}	
	
	public void typeTextNumero(String value){
		typeText(TEXT_NUMERO, value);
	}	
	
	public void typeTextBairro(String value){
		typeText(TEXT_BAIRRO, value);
	}	
	
	public void typeTextCEP(String value){
		typeText(TEXT_CEP, value);
	}	
	
	public void typeSelectCidade(String value){		
		typeSelectComboOption(SELECT_CIDADE, value);
	}

	public void typeSelectRegiao(String value){		
		typeSelectComboOption(SELECT_REGIAO, value);
	}
	
	public void typeTextLatitude(String value){
		typeText(TEXT_LATITUDE, value);
	}	
	
	public void typeTextLongitude(String value){
		typeText(TEXT_LONGITUDE, value);
	}	
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
				
	public void clickAbaEndereco(){
		clickOnElement(LINK_ABA_ENDERECO);
	}
	
}