package br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.dpo.CadastrarAbaContatoSubestacaoDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.page.CadastroAbaContatoSubestacaoPage;

public class CadastrarAbaContatoSubestacaoStage extends BaseStage {
	private CadastrarAbaContatoSubestacaoDPO cadastrarAbaContatoSubestacaoDPO;
	private CadastroAbaContatoSubestacaoPage cadastroAbaContatoSubestacaoPage;
	
	public CadastrarAbaContatoSubestacaoStage(String dp) {
		cadastrarAbaContatoSubestacaoDPO = loadDataProviderFile(CadastrarAbaContatoSubestacaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaContatoSubestacaoPage = initElementsFromPage(CadastroAbaContatoSubestacaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaContatoSubestacaoDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaContatoSubestacaoPage.clickAbaContato();
		cadastroAbaContatoSubestacaoPage.typeTextTelefoneSE1(cadastrarAbaContatoSubestacaoDPO.getTelefoneSE1());			
		cadastroAbaContatoSubestacaoPage.typeTextTelefoneSE2(cadastrarAbaContatoSubestacaoDPO.getTelefoneSE2());			
		cadastroAbaContatoSubestacaoPage.typeTextContato1(cadastrarAbaContatoSubestacaoDPO.getContato1());			
		cadastroAbaContatoSubestacaoPage.typeTextEmail1(cadastrarAbaContatoSubestacaoDPO.getEmail1());			
		cadastroAbaContatoSubestacaoPage.typeTextTelContato1(cadastrarAbaContatoSubestacaoDPO.getTelContato1());			
		cadastroAbaContatoSubestacaoPage.typeTextContato2(cadastrarAbaContatoSubestacaoDPO.getContato2());			
		cadastroAbaContatoSubestacaoPage.typeTextEmail2(cadastrarAbaContatoSubestacaoDPO.getEmail2());			
		cadastroAbaContatoSubestacaoPage.typeTextTelContato2(cadastrarAbaContatoSubestacaoDPO.getTelContato2());			
		cadastroAbaContatoSubestacaoPage.typeTextTelContato3(cadastrarAbaContatoSubestacaoDPO.getTelContato3());			
		cadastroAbaContatoSubestacaoPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaContatoSubestacaoPage.validateMessageDefault(cadastrarAbaContatoSubestacaoDPO.getOperationMessage());
	}

}
