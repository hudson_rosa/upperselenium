package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class ImportacaoDeDadosHidrologicosPage extends BaseHelperPage{
	
	private static final String LINK_IMPORTAR_NOVOS_DADOS_HIDROLOGICOS = ".//listagem-de-importacoes/div/a";
	private static final String LABEL_PERIODO_DE_IMPORTACAO = ".//listagem-de-importacoes/div/filtro/div/span";
	private static final String LABEL_HEADER_INSTANTE_DE_IMPORTACAO = "//*[@id='tabela-importacoes']/table/thead/tr[2]/th[1]/span";
	private static final String LABEL_HEADER_TIPO_DE_IMPORTACAO = "//*[@id='tabela-importacoes']/table/thead/tr[2]/th[2]/span";
	private static final String LABEL_HEADER_STATUS_DE_IMPORTACAO = "//*[@id='tabela-importacoes']/table/thead/tr[2]/th[3]/span";
	
	private static final String MODAL_LABEL_ALTERACAO_DE_PONTO_APLICADO = "//*[@id='modal-alteracao-de-ponto-aplicado']/form/div[1]/h4/span";
	private static final String MODAL_LABEL_TIPO_DE_IMPORTACAO = "//*[@id='modal-alteracao-de-ponto-aplicado']/form/div[2]/label";
	private static final String MODAL_LABEL_ARQUIVO = "//*[@id='modal-alteracao-de-ponto-aplicado']/form/div[2]/file-upload/div/label";
	private static final String MODAL_BUTTON_IMPORTAR = "//*[@id='modal-alteracao-de-ponto-aplicado']/form/div[3]/button";
	private static final String MODAL_BUTTON_CLOSE = "//*[@id='modal-alteracao-de-ponto-aplicado']/form/div[1]/button";
	
	public void clickImportarNovosDadosHidrologicos(){
		clickOnElement(LINK_IMPORTAR_NOVOS_DADOS_HIDROLOGICOS);
		waitForPageToLoadUntil10s();
	}	

	public void clickModalClose(){
		clickOnElement(MODAL_BUTTON_CLOSE);
		waitForPageToLoadUntil10s();
	}	
	
	public void getLabelPeriodoDeImportacao(String value){
		getLabel(LABEL_PERIODO_DE_IMPORTACAO, value);
	}
	
	public void getLabeldHeaderInstanteDeImportacao(String value){
		getLabel(LABEL_HEADER_INSTANTE_DE_IMPORTACAO, value);
	}
	
	public void getLabelHeaderTipoDeImportacao(String value){
		getLabel(LABEL_HEADER_TIPO_DE_IMPORTACAO, value);
	}
	
	public void getLabelHeaderStatusDeImportacao(String value){
		getLabel(LABEL_HEADER_STATUS_DE_IMPORTACAO, value);
	}
	
	public void getModalLabelAlteracaoDePontoAplicado(String value){
		getLabel(MODAL_LABEL_ALTERACAO_DE_PONTO_APLICADO, value);
	}
	
	public void getModalLabelTipoDeImportacao(String value){
		getLabel(MODAL_LABEL_TIPO_DE_IMPORTACAO, value);
	}
	
	public void getModalLabelArquivo(String value){
		getLabel(MODAL_LABEL_ARQUIVO, value);
	}
	
	public void getModalLabelButtonImportar(String value){
		getLabel(MODAL_BUTTON_IMPORTAR, value);
	}
		
}