package br.com.upperselenium.test.stage.configuracoes.administracao.usuarios;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.dpo.NavegarUsuariosDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page.UsuariosPage;

public class NavegarUsuariosStage extends BaseStage {
	private NavegarUsuariosDPO navegarUsuariosDPO;
	private UsuariosPage usuariosPage;
	
	public NavegarUsuariosStage(String dp) {
		navegarUsuariosDPO = loadDataProviderFile(NavegarUsuariosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		usuariosPage = initElementsFromPage(UsuariosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarUsuariosDPO.getIssue());
		execute();
	}
	
	private void execute() {
		usuariosPage.getBreadcrumbsText(navegarUsuariosDPO.getBreadcrumbs());
		usuariosPage.clickUpToBreadcrumbs(navegarUsuariosDPO.getUpToBreadcrumb());
		usuariosPage.getWelcomeTitle(navegarUsuariosDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
