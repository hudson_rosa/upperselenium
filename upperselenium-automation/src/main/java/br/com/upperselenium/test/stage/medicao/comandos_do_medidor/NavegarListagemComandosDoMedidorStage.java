package br.com.upperselenium.test.stage.medicao.comandos_do_medidor;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo.NavegarListagemComandosDoMedidorDPO;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.page.ComandosDoMedidorPage;

public class NavegarListagemComandosDoMedidorStage extends BaseStage {
	private NavegarListagemComandosDoMedidorDPO navegarComandosDoGatewayDPO;
	private ComandosDoMedidorPage comandosDoGatewayPage;	
	
	public NavegarListagemComandosDoMedidorStage(String dp) {
		navegarComandosDoGatewayDPO = loadDataProviderFile(NavegarListagemComandosDoMedidorDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		comandosDoGatewayPage = initElementsFromPage(ComandosDoMedidorPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarComandosDoGatewayDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		comandosDoGatewayPage.getLabelFiltrarPelaDataDaProximaExecucao(navegarComandosDoGatewayDPO.getLabelFiltrarDataProximaExecucao());
		comandosDoGatewayPage.clickButtonFiltrar(navegarComandosDoGatewayDPO.getClickFiltrar());
		if(StringUtil.isNotBlankOrNotNull(navegarComandosDoGatewayDPO.getClickFiltrar())){
			comandosDoGatewayPage.getLabelHeaderMedidorParametros(navegarComandosDoGatewayDPO.getLabelHeaderMedidorParametros());
			comandosDoGatewayPage.getLabelHeaderInsercao(navegarComandosDoGatewayDPO.getLabelHeaderInsercao());
			comandosDoGatewayPage.getLabelHeaderProximaExecucao(navegarComandosDoGatewayDPO.getLabelHeaderProximaExecucao());
			comandosDoGatewayPage.getLabelHeaderDescricao(navegarComandosDoGatewayDPO.getLabelHeaderDescricao());
			comandosDoGatewayPage.getLabelHeaderUltimasExecucoes(navegarComandosDoGatewayDPO.getLabelHeaderUltimasExecucoes());
			comandosDoGatewayPage.getLabelHeaderExpiracao(navegarComandosDoGatewayDPO.getLabelHeaderExpiracao());
		}
		comandosDoGatewayPage.getBreadcrumbsText(navegarComandosDoGatewayDPO.getBreadcrumbs());
		comandosDoGatewayPage.clickUpToBreadcrumbs(navegarComandosDoGatewayDPO.getUpToBreadcrumb());
		comandosDoGatewayPage.getWelcomeTitle(navegarComandosDoGatewayDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
