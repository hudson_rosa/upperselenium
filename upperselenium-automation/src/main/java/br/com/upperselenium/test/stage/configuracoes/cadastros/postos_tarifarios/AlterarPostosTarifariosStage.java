package br.com.upperselenium.test.stage.configuracoes.cadastros.postos_tarifarios;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.postos_tarifarios.dpo.AlterarPostosTarifariosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.postos_tarifarios.page.AlteracaoPostosTarifariosPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.postos_tarifarios.page.PostosTarifariosPage;

public class AlterarPostosTarifariosStage extends BaseStage {
	private AlterarPostosTarifariosDPO alterarPostosTarifariosDPO;
	private PostosTarifariosPage postosTarifariosPage;
	private AlteracaoPostosTarifariosPage alteracaoPostosTarifariosPage;
	
	public AlterarPostosTarifariosStage(String dp) {
		alterarPostosTarifariosDPO = loadDataProviderFile(AlterarPostosTarifariosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		postosTarifariosPage = initElementsFromPage(PostosTarifariosPage.class);
		alteracaoPostosTarifariosPage = initElementsFromPage(AlteracaoPostosTarifariosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarPostosTarifariosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoPostosTarifariosPage.typeTextNome(alterarPostosTarifariosDPO.getNome()+getRandomStringSpaced());		
		alteracaoPostosTarifariosPage.typeCheckBoxDeslocarNoHorarioDeVerao(alterarPostosTarifariosDPO.getDeslocarNoHorarioDeVerao());		
		alteracaoPostosTarifariosPage.typeTextHorarioDePontaInicio(alterarPostosTarifariosDPO.getHorarioDePontaInicio());		
		alteracaoPostosTarifariosPage.typeTextHorarioDePontaFim(alterarPostosTarifariosDPO.getHorarioDePontaFim());
		alteracaoPostosTarifariosPage.typeCheckSegundaHPonta(alterarPostosTarifariosDPO.getSegundaHPonta());		
		alteracaoPostosTarifariosPage.typeCheckTercaHPonta(alterarPostosTarifariosDPO.getTercaHPonta());		
		alteracaoPostosTarifariosPage.typeCheckQuartaHPonta(alterarPostosTarifariosDPO.getQuartaHPonta());		
		alteracaoPostosTarifariosPage.typeCheckQuintaHPonta(alterarPostosTarifariosDPO.getQuintaHPonta());		
		alteracaoPostosTarifariosPage.typeCheckSextaHPonta(alterarPostosTarifariosDPO.getSextaHPonta());		
		alteracaoPostosTarifariosPage.typeCheckSabadoHPonta(alterarPostosTarifariosDPO.getSabadoHPonta());		
		alteracaoPostosTarifariosPage.typeCheckDomingoHPonta(alterarPostosTarifariosDPO.getDomingoHPonta());		
		alteracaoPostosTarifariosPage.typeCheckFeriadoHPonta(alterarPostosTarifariosDPO.getFeriadoHPonta());		
		alteracaoPostosTarifariosPage.typeTextHorarioCapacitivoInicio(alterarPostosTarifariosDPO.getHorarioCapacitivoInicio());		
		alteracaoPostosTarifariosPage.typeTextHorarioCapacitivoFim(alterarPostosTarifariosDPO.getHorarioCapacitivoFim());
		alteracaoPostosTarifariosPage.typeCheckSegundaHCapacitivo(alterarPostosTarifariosDPO.getSegundaHCapacitivo());		
		alteracaoPostosTarifariosPage.typeCheckTercaHCapacitivo(alterarPostosTarifariosDPO.getTercaHCapacitivo());		
		alteracaoPostosTarifariosPage.typeCheckQuartaHCapacitivo(alterarPostosTarifariosDPO.getQuartaHCapacitivo());		
		alteracaoPostosTarifariosPage.typeCheckQuintaHCapacitivo(alterarPostosTarifariosDPO.getQuintaHCapacitivo());		
		alteracaoPostosTarifariosPage.typeCheckSextaHCapacitivo(alterarPostosTarifariosDPO.getSextaHCapacitivo());		
		alteracaoPostosTarifariosPage.typeCheckSabadoHCapacitivo(alterarPostosTarifariosDPO.getSabadoHCapacitivo());		
		alteracaoPostosTarifariosPage.typeCheckDomingoHCapacitivo(alterarPostosTarifariosDPO.getDomingoHCapacitivo());		
		alteracaoPostosTarifariosPage.typeCheckFeriadoHCapacitivo(alterarPostosTarifariosDPO.getFeriadoHCapacitivo());	
		alteracaoPostosTarifariosPage.typeCheckBoxHorarioReservado(alterarPostosTarifariosDPO.getHorarioReservado());		
		alteracaoPostosTarifariosPage.typeTextHorarioReservadoInicio(alterarPostosTarifariosDPO.getHorarioReservadoInicio());		
		alteracaoPostosTarifariosPage.typeTextHorarioReservadoFim(alterarPostosTarifariosDPO.getHorarioReservadoFim());
		alteracaoPostosTarifariosPage.typeCheckSegundaHReservado(alterarPostosTarifariosDPO.getSegundaHReservado());		
		alteracaoPostosTarifariosPage.typeCheckTercaHReservado(alterarPostosTarifariosDPO.getTercaHReservado());		
		alteracaoPostosTarifariosPage.typeCheckQuartaHReservado(alterarPostosTarifariosDPO.getQuartaHReservado());		
		alteracaoPostosTarifariosPage.typeCheckQuintaHReservado(alterarPostosTarifariosDPO.getQuintaHReservado());		
		alteracaoPostosTarifariosPage.typeCheckSextaHReservado(alterarPostosTarifariosDPO.getSextaHReservado());		
		alteracaoPostosTarifariosPage.typeCheckSabadoHReservado(alterarPostosTarifariosDPO.getSabadoHReservado());		
		alteracaoPostosTarifariosPage.typeCheckDomingoHReservado(alterarPostosTarifariosDPO.getDomingoHReservado());		
		alteracaoPostosTarifariosPage.typeCheckFeriadoHReservado(alterarPostosTarifariosDPO.getFeriadoHReservado());	
		alteracaoPostosTarifariosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		validateLabelHorarioDeForaPontaInicio();
		validateLabelHorarioDeForaPontaFim();
		alteracaoPostosTarifariosPage.validateMessageDefault(alterarPostosTarifariosDPO.getOperationMessage());
	}

	private void validateLabelHorarioDeForaPontaInicio() {
		alteracaoPostosTarifariosPage.validateEqualsToValues(
				alterarPostosTarifariosDPO.getHorarioDeForaPontaInicio(), 
				alteracaoPostosTarifariosPage.getLabelHorarioDeForaPontaInicio());		
	}
	
	private void validateLabelHorarioDeForaPontaFim() {
		alteracaoPostosTarifariosPage.validateEqualsToValues(
				alterarPostosTarifariosDPO.getHorarioDeForaPontaFim(), 
				alteracaoPostosTarifariosPage.getLabelHorarioDeForaPontaFim());	
	}

}
