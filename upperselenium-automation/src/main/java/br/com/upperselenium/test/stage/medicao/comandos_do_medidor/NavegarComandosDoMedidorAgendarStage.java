package br.com.upperselenium.test.stage.medicao.comandos_do_medidor;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo.NavegarComandosDeMedidorAgendarDPO;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.page.ComandosDoMedidorAgendarPage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.page.ComandosDoMedidorPage;

public class NavegarComandosDoMedidorAgendarStage extends BaseStage {
	private NavegarComandosDeMedidorAgendarDPO navegarComandosDoMedidorAgendarDPO;
	private ComandosDoMedidorPage comandosDoMedidorPage;	
	private ComandosDoMedidorAgendarPage comandosDoMedidorAgendarPage;	
	
	public NavegarComandosDoMedidorAgendarStage(String dp) {
		navegarComandosDoMedidorAgendarDPO = loadDataProviderFile(NavegarComandosDeMedidorAgendarDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		comandosDoMedidorPage = initElementsFromPage(ComandosDoMedidorPage.class);
		comandosDoMedidorAgendarPage = initElementsFromPage(ComandosDoMedidorAgendarPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarComandosDoMedidorAgendarDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		comandosDoMedidorPage.clickLinkAgendar();
		comandosDoMedidorAgendarPage.getLabelComandos(navegarComandosDoMedidorAgendarDPO.getLabelComandos());
		comandosDoMedidorAgendarPage.getLabelInstante(navegarComandosDoMedidorAgendarDPO.getLabelInstante());
		comandosDoMedidorAgendarPage.getLabelExpiracao(navegarComandosDoMedidorAgendarDPO.getLabelExpiracao());
		comandosDoMedidorAgendarPage.getLabelDigiteUmOuMaisMedidores(navegarComandosDoMedidorAgendarDPO.getLabelDigiteUmOuMaisMedidores());		
		comandosDoMedidorAgendarPage.getLabelRepeticao(navegarComandosDoMedidorAgendarDPO.getLabelRepeticao());		
		comandosDoMedidorAgendarPage.getBreadcrumbsText(navegarComandosDoMedidorAgendarDPO.getBreadcrumbs());
		comandosDoMedidorAgendarPage.clickUpToBreadcrumbs(navegarComandosDoMedidorAgendarDPO.getUpToBreadcrumb());
		comandosDoMedidorAgendarPage.getWelcomeTitle(navegarComandosDoMedidorAgendarDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
