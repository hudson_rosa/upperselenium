package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class TiposDePontoPage extends BaseHelperPage{
	
	private static final String LINK_NOVO = ".//div[2]/div/div[2]/p/a";
	private static final String GRID_BUTTON_EDITAR = "//*[@id='dataTableList']/tbody/tr%%/td[5]/div/a";
	private static final String GRID_LINK_NOME_TIPO_DE_PONTO = "//*[@id='dataTableList']/tbody/tr[1]/td[1]/a";
	private static final String LABEL_HEADER_NOME = "//*[@id='dataTableList']/thead/tr[2]/th[contains(text(),'Nome')]";
	private static final String LABEL_HEADER_DESCRICAO = "//*[@id='dataTableList']/thead/tr[2]/th[contains(text(),'Descrição')]";	
	private static final String LABEL_HEADER_GRUPO_DE_MEDICAO = "//*[@id='dataTableList']/thead/tr[2]/th[contains(text(),'Grupo de  Medição')]";	
	private static final String LABEL_HEADER_ABAS = "//*[@id='dataTableList']/thead/tr[2]/th[contains(text(),'Abas')]";
	
	public boolean isClickableLinkNovo() {
		return isDisplayedElement(LINK_NOVO);
	}
		
	public void clickNovo(){
		clickOnElement(LINK_NOVO);
		waitForPageToLoadUntil10s();
	}
	
	public void getLabelHeaderNome(String value){
		getLabel(LABEL_HEADER_NOME, value);
	}
	
	public void getLabelHeaderDescricao(String value){
		getLabel(LABEL_HEADER_DESCRICAO, value);
	}
	
	public void getLabelHeaderGrupoDeMedicao(String value){
		getLabel(LABEL_HEADER_GRUPO_DE_MEDICAO, value);
	}
	
	public void getLabelHeaderAbas(String value){
		getLabel(LABEL_HEADER_ABAS, value);
	}
		
	public void clickGridButtonFirstLineEditar(String value){
		waitForPageToLoadUntil10s();
		clickGridButtonOnElement(GRID_BUTTON_EDITAR, value, 1);
	}
	
	public void clickGridFirstLineLinkNome(String value){
		waitForPageToLoadUntil10s();
		clickGridButtonOnElement(GRID_LINK_NOME_TIPO_DE_PONTO, value, 1);
	}

}