package br.com.upperselenium.test.stage.configuracoes.cadastros.trafos_bt;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.trafos_bt.dpo.AlterarTrafosBTDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.trafos_bt.page.AlteracaoTrafosBTPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.trafos_bt.page.TrafosBTPage;

public class AlterarTrafosBTStage extends BaseStage {
	private AlterarTrafosBTDPO alterarTrafosBTDPO;
	private TrafosBTPage trafosBTPage;
	private AlteracaoTrafosBTPage alteracaoTrafosBTPage;
	
	public AlterarTrafosBTStage(String dp) {
		alterarTrafosBTDPO = loadDataProviderFile(AlterarTrafosBTDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		trafosBTPage = initElementsFromPage(TrafosBTPage.class);
		alteracaoTrafosBTPage = initElementsFromPage(AlteracaoTrafosBTPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarTrafosBTDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoTrafosBTPage.typeTextNome(alterarTrafosBTDPO.getNome()+getRandomNumberSpaced());		
		alteracaoTrafosBTPage.typeTextDescricao(alterarTrafosBTDPO.getDescricao()+getRandomStringSpaced());		
		alteracaoTrafosBTPage.typeTextIdentificador(alterarTrafosBTDPO.getIdentificador()+getRandomNumber());		
		alteracaoTrafosBTPage.typeTextPotencia(alterarTrafosBTDPO.getPotencia());		
		alteracaoTrafosBTPage.typeSelectNumeroDeFases(alterarTrafosBTDPO.getNumeroDeFases());		
		alteracaoTrafosBTPage.typeTextMedicao(alterarTrafosBTDPO.getMedicao());		
		alteracaoTrafosBTPage.typeTextAlimentadores(alterarTrafosBTDPO.getAlimentadores());
		alteracaoTrafosBTPage.keyPageDown();	
		alteracaoTrafosBTPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoTrafosBTPage.validateMessageDefault(alterarTrafosBTDPO.getOperationMessage());
	}
	
}
