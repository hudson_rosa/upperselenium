package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.dpo.NavegarRegistroLinkTiposDePontoCCEEDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.page.AlteracaoTiposDePontoCCEEPage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.page.TiposDePontoCCEEPage;

public class NavegarRegistroLinkTiposDePontoCCEEStage extends BaseStage {
	private NavegarRegistroLinkTiposDePontoCCEEDPO navegarRegistroLinkTiposDePontoCCEEDPO;
	private TiposDePontoCCEEPage tiposDePontoCCEEPage;
	private AlteracaoTiposDePontoCCEEPage alteracaoTiposDePontoCCEEPage;
	
	public NavegarRegistroLinkTiposDePontoCCEEStage(String dp) {
		navegarRegistroLinkTiposDePontoCCEEDPO = loadDataProviderFile(NavegarRegistroLinkTiposDePontoCCEEDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDePontoCCEEPage = initElementsFromPage(TiposDePontoCCEEPage.class);
		alteracaoTiposDePontoCCEEPage = initElementsFromPage(AlteracaoTiposDePontoCCEEPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarRegistroLinkTiposDePontoCCEEDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		tiposDePontoCCEEPage.clickGridFirstLineLinkNome(navegarRegistroLinkTiposDePontoCCEEDPO.getClickItem());
		alteracaoTiposDePontoCCEEPage.getLabelButtonSalvar(navegarRegistroLinkTiposDePontoCCEEDPO.getLabelButtonSalvar());
		alteracaoTiposDePontoCCEEPage.getBreadcrumbsText(navegarRegistroLinkTiposDePontoCCEEDPO.getBreadcrumbs());
		alteracaoTiposDePontoCCEEPage.clickUpToBreadcrumbs(navegarRegistroLinkTiposDePontoCCEEDPO.getUpToBreadcrumb());
		alteracaoTiposDePontoCCEEPage.getWelcomeTitle(navegarRegistroLinkTiposDePontoCCEEDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
