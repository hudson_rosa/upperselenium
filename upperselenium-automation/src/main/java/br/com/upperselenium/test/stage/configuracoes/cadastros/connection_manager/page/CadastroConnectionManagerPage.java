package br.com.upperselenium.test.stage.configuracoes.cadastros.connection_manager.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroConnectionManagerPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='connection-manager-form']/div[2]/button";  
	private static final String TEXT_HUB = "//*[@id='nome']";
	private static final String CHECK_HABILITADO = "//*[@id='habilitado']";
	private static final String CHECK_REGISTRAR_CONEXOES = "//*[@id='registrarConexoes']";
	private static final String GRID_TEXT_CANAL = "//*[@id='container-canais']/table/tbody/tr[%i%]/td[1]/input";
	private static final String GRID_TEXT_IP = "//*[@id='container-canais']/table/tbody/tr[%i%]/td[2]/input";
	private static final String GRID_TEXT_PORTA = "//*[@id='container-canais']/table/tbody/tr[%i%]/td[3]/input";
	private static final String GRID_TEXT_TIMEOUT = "//*[@id='container-canais']/table/tbody/tr[%i%]/td[4]/input";
	private static final String GRID_SELECT_TIPO = "//*[@id='container-canais']/table/tbody/tr[%i%]/td[5]/select";
	private static final String GRID_SELECT_PRIORIDADE = "//*[@id='container-canais']/table/tbody/tr[%i%]/td[6]/select";
	private static final String GRID_CHECK_NOVA_CONEXAO_FECHA_ANTIGA = "//*[@id='fechaConexaoAntiga%k%']";
	private static final String GRID_CHECK_REINICIAR_HUB_AO_FECHAR_ANTIGA = "//*[@id='reiniciarHub%k%']";
	private static final String GRID_CHECK_ALWAYS_ON = "//*[@id='alwaysOn%k%']";
	private static final String GRID_BUTTON_ADD = "//*[@id='container-canais']/table/tbody/tr[%i%]/td[7]/div/a[2]";
	private static final String GRID_BUTTON_EXCLUDE = "//*[@id='container-canais']/table/tbody/tr[%i%]/td[7]/div/a[1]";  
	
	public void typeTextHub(String value){
		typeText(TEXT_HUB, value);
	}	
	
	public void typeCheckBoxHabilitado(String value){
		typeCheckOption(CHECK_HABILITADO, value);
	}	
	
	public void typeCheckBoxRegistrarConexoes(String value){
		typeCheckOption(CHECK_REGISTRAR_CONEXOES, value);
	}	
	
	public void typeGridTextCanal(String value, int index){
		typeGridText(GRID_TEXT_CANAL, value, index);
	}	
	
	public void typeGridTextIP(String value, int index){
		typeGridText(GRID_TEXT_IP, value, index);
	}	
	
	public void typeGridTextPorta(String value, int index){
		typeGridText(GRID_TEXT_PORTA, value, index);
	}	
	
	public void typeGridTextTimeout(String value, int index){
		typeGridText(GRID_TEXT_TIMEOUT, value, index);
	}	
		
	public void typeGridSelectTipo(String value, int index){		
		typeGridSelectComboOption(GRID_SELECT_TIPO, value, index);
	}
	
	public void typeGridCheckBoxNovaConexaoFechaAntiga(String value, int index){
		typeGridCheckOption(GRID_CHECK_NOVA_CONEXAO_FECHA_ANTIGA, value, index);
	}	

	public void typeGridCheckBoxReiniciarHubAoFecharAntiga(String value, int index){
		typeGridCheckOption(GRID_CHECK_REINICIAR_HUB_AO_FECHAR_ANTIGA, value, index);
	}	
	
	public void typeGridCheckBoxAlwaysOn(String value, int index){
		typeGridCheckOption(GRID_CHECK_ALWAYS_ON, value, index);
	}	
	
	public void typeGridSelectPrioridade(String value, int index){		
		typeGridSelectComboOption(GRID_SELECT_PRIORIDADE, value, index);
	}
		
	public void clickGridAdd(String value, int index){
		clickGridButtonOnElement(GRID_BUTTON_ADD, value, index);
	}
	
	public void clickGridExclude(String value, int index){
		clickGridButtonOnElement(GRID_BUTTON_EXCLUDE, value, index);
	}
	
	public void useKeyPageDown() {
		useKey(TEXT_HUB, Keys.PAGE_DOWN);
	}	
	
	public void useKeyPageUp() {
		useKey(TEXT_HUB, Keys.PAGE_UP);
	}	
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}