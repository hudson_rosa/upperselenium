package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastroAbaOpcoesDeManobrasPontosVirtuaisPage;

public class CadastrarAbaOpcoesDeManobrasPontosVirtuaisStage extends BaseStage {
	private CadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO;
	private CadastroAbaOpcoesDeManobrasPontosVirtuaisPage cadastroAbaOpcoesDeManobrasPontosVirtuaisPage;
	
	public CadastrarAbaOpcoesDeManobrasPontosVirtuaisStage(String dp) {
		cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO = loadDataProviderFile(CadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaOpcoesDeManobrasPontosVirtuaisPage = initElementsFromPage(CadastroAbaOpcoesDeManobrasPontosVirtuaisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.clickAbaOpcoesDeManobras();
		FindElementUtil.acceptAlert();
		for (int index = 0; index < cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridChave().size(); index++) {
			if (StringUtil.isNotBlankOrNotNull(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridChave().get(index).getChave())) {
				cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.typeTextChave(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridChave().get(index).getChave()+getRandomStringNonSpaced());
				cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.clickAdicionar();
				cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.typeTextFilterChave(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridChave().get(index).getFilterChave()+getRandomStringNonSpaced(), 1);
				cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.getGridValueChave(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridChave().get(index).getGridValueChave()+getRandomStringNonSpaced(), 1);
				removeChave(index);
			}
		}
		waitForPageToLoadUntil10s();
		cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.clickGerarCombinacoes();
		cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.keyPageDown();
		for (int index=0; index < cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridCombinacoesGeradas().size(); index++){
			if(StringUtil.isNotBlankOrNotNull(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridCombinacoesGeradas().get(index).getFlagItem())){
				cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.typeTextFilterChaveCombinacao(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridCombinacoesGeradas().get(index).getFilterChaveCombinacao(), 1);
				cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.typeCheckBoxGridItem(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridCombinacoesGeradas().get(index).getFlagItem(), 1);
				editManobra(index);
				cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.typeTextFilterChaveCombinacao(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridCombinacoesGeradas().get(index).getFilterChaveCombinacao(), 1);
				cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.typeTextFilterPontoAplicado(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridCombinacoesGeradas().get(index).getFilterPontoAplicado(), 1);
				cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.getGridValueChaveCombinacao(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridCombinacoesGeradas().get(index).getGridValueChaveCombinacao(), index+1);
				cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.getGridValuePontoAplicado(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridCombinacoesGeradas().get(index).getGridValuePontoAplicado(), index+1);
			}
		}
		cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.keyPageUp();
	}

	private void editManobra(int index) {
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridCombinacoesGeradas().get(index).getFlagEdit())) {
		cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.clickEditar();
		cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.typeModalTextPonto(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridCombinacoesGeradas().get(index).getModalPonto());
		cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.clickModalSalvar();
		}
	}

	private void removeChave(int index) {
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridChave().get(index).getButtonRemove())){
			cadastroAbaOpcoesDeManobrasPontosVirtuaisPage.clickGridButtonRemove(cadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO.getGridChave().get(index).getButtonRemove(), 1);
		}
	}

	@Override
	public void runValidations() {}

}
