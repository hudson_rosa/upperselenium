package br.com.upperselenium.test.stage.medicao.comandos_do_gateway.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarComandosDoGatewayAgendarDPO extends BaseHelperDPO {

	private String labelTipo;
	private String labelInstante;
	private String labelExpiracao;
	private String labelDigiteUmOuMaisGateways;
	private String labelRepeticao;

	public String getLabelTipo() {
		return labelTipo;
	}

	public void setLabelTipo(String labelTipo) {
		this.labelTipo = labelTipo;
	}

	public String getLabelInstante() {
		return labelInstante;
	}

	public void setLabelInstante(String labelInstante) {
		this.labelInstante = labelInstante;
	}

	public String getLabelExpiracao() {
		return labelExpiracao;
	}

	public void setLabelExpiracao(String labelExpiracao) {
		this.labelExpiracao = labelExpiracao;
	}

	public String getLabelDigiteUmOuMaisGateways() {
		return labelDigiteUmOuMaisGateways;
	}

	public void setLabelDigiteUmOuMaisGateways(String labelDigiteUmOuMaisGateways) {
		this.labelDigiteUmOuMaisGateways = labelDigiteUmOuMaisGateways;
	}

	public String getLabelRepeticao() {
		return labelRepeticao;
	}

	public void setLabelRepeticao(String labelRepeticao) {
		this.labelRepeticao = labelRepeticao;
	}

	@Override
	public String toString() {
		return "NavegarComandosDeGatewayAgendarDPO [labelTipo=" + labelTipo + ", labelInstante=" + labelInstante
				+ ", labelExpiracao=" + labelExpiracao + ", labelDigiteUmOuMaisGateways=" + labelDigiteUmOuMaisGateways
				+ ", labelRepeticao=" + labelRepeticao + "]";
	}

}