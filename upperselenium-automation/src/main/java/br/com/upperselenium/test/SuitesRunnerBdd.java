package br.com.upperselenium.test;

import java.util.List;

import br.com.upperselenium.base.BaseSuitesExecutor;
import br.com.upperselenium.base.sample.test.suite.S000SampleSuiteBdd;

/**
 * Define todas as Suites para execução.
 * @author Hudson
 *
 */
public class SuitesRunnerBdd extends BaseSuitesExecutor {

	public static void main(String args[]) {
		runAll("bdd");
	}

	public static List<Class<?>> runSuites() {
		suites.add(S000SampleSuiteBdd.class);
		return suites;
	}

}