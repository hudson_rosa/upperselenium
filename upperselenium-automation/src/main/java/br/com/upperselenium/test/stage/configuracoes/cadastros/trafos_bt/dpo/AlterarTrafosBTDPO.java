package br.com.upperselenium.test.stage.configuracoes.cadastros.trafos_bt.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarTrafosBTDPO extends BaseHelperDPO {

	private String nome;
	private String descricao;
	private String identificador;
	private String potencia;
	private String numeroDeFases;
	private String medicao;
	private String alimentadores;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getIdentificador() {
		return identificador;
	}

	public void setIdentificador(String identificador) {
		this.identificador = identificador;
	}

	public String getPotencia() {
		return potencia;
	}

	public void setPotencia(String potencia) {
		this.potencia = potencia;
	}

	public String getNumeroDeFases() {
		return numeroDeFases;
	}

	public void setNumeroDeFases(String numeroDeFases) {
		this.numeroDeFases = numeroDeFases;
	}

	public String getMedicao() {
		return medicao;
	}

	public void setMedicao(String medicao) {
		this.medicao = medicao;
	}

	public String getAlimentadores() {
		return alimentadores;
	}

	public void setAlimentadores(String alimentadores) {
		this.alimentadores = alimentadores;
	}

}