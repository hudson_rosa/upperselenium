package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.dpo.NavegarAcompDiarioDeUsinasDPO;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.page.AcompDiarioDeUsinasPage;

public class NavegarAcompDiarioDeUsinasStage extends BaseStage {
	private NavegarAcompDiarioDeUsinasDPO navegarAcompDiarioDeUsinasDPO;
	private AcompDiarioDeUsinasPage acompDiarioDeUsinasPage;	
	
	public NavegarAcompDiarioDeUsinasStage(String dp) {
		navegarAcompDiarioDeUsinasDPO = loadDataProviderFile(NavegarAcompDiarioDeUsinasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		acompDiarioDeUsinasPage = initElementsFromPage(AcompDiarioDeUsinasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAcompDiarioDeUsinasDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		acompDiarioDeUsinasPage.getLabelUsina(navegarAcompDiarioDeUsinasDPO.getLabelUsina());
		acompDiarioDeUsinasPage.getLabelData(navegarAcompDiarioDeUsinasDPO.getLabelData());
		acompDiarioDeUsinasPage.clickButtonFiltrar(navegarAcompDiarioDeUsinasDPO.getClickFiltrar());
		if(StringUtil.isNotBlankOrNotNull(navegarAcompDiarioDeUsinasDPO.getClickFiltrar())){
			acompDiarioDeUsinasPage.getLabelHeaderGeracao(navegarAcompDiarioDeUsinasDPO.getLabelHeaderGeracao());
			acompDiarioDeUsinasPage.getLabelHeaderNivel(navegarAcompDiarioDeUsinasDPO.getLabelHeaderNivel());
			acompDiarioDeUsinasPage.getLabelHeaderEficiência(navegarAcompDiarioDeUsinasDPO.getLabelHeaderEficiencia());
			acompDiarioDeUsinasPage.getLabelHeaderVazao(navegarAcompDiarioDeUsinasDPO.getLabelHeaderVazao());
		}
		acompDiarioDeUsinasPage.getBreadcrumbsText(navegarAcompDiarioDeUsinasDPO.getBreadcrumbs());
		acompDiarioDeUsinasPage.clickUpToBreadcrumbs(navegarAcompDiarioDeUsinasDPO.getUpToBreadcrumb());
		acompDiarioDeUsinasPage.getWelcomeTitle(navegarAcompDiarioDeUsinasDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
