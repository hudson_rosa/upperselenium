package br.com.upperselenium.test.stage.medicao.comandos_do_medidor;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo.AgendarComandosDoMedidorDPO;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.page.ComandosDoMedidorAgendarPage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.page.ComandosDoMedidorPage;

public class AgendarComandosDoMedidorStage extends BaseStage {
	private AgendarComandosDoMedidorDPO agendarComandosDoMedidorDPO;
	private ComandosDoMedidorPage comandosDoMedidorPage;	
	private ComandosDoMedidorAgendarPage comandosDoMedidorAgendarPage;	
	
	public AgendarComandosDoMedidorStage(String dp) {
		agendarComandosDoMedidorDPO = loadDataProviderFile(AgendarComandosDoMedidorDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		comandosDoMedidorPage = initElementsFromPage(ComandosDoMedidorPage.class);
		comandosDoMedidorAgendarPage = initElementsFromPage(ComandosDoMedidorAgendarPage.class);
	}

	@Override
	public void runStage() {
		getIssue(agendarComandosDoMedidorDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		comandosDoMedidorPage.clickLinkAgendar();
		comandosDoMedidorAgendarPage.clickButtonComandos();		
		comandosDoMedidorAgendarPage.typeTextFiltrarComando(agendarComandosDoMedidorDPO.getFiltrarComando());
		comandosDoMedidorAgendarPage.typeCheckBoxFilteredComando(agendarComandosDoMedidorDPO.getFilteredComando());		
		comandosDoMedidorAgendarPage.clickButtonConfigurarParametros();		
		for (int index = 0; index < agendarComandosDoMedidorDPO.getDigiteUmOuMaisMedidores().size(); index++) {
			comandosDoMedidorAgendarPage.typeTextAutocompleteMedidores(agendarComandosDoMedidorDPO.getDigiteUmOuMaisMedidores().get(index));
			comandosDoMedidorAgendarPage.keyTab();
		}
		comandosDoMedidorAgendarPage.clickButtonAgendar();
	}

	@Override
	public void runValidations() {}
	
}
