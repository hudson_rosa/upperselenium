package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas.CadastrarAreasGeograficasStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas.GoToAreasGeograficasStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0003AreasGeograficasCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0003-AreasGeograficas", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Areas Geográficas realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0003AreasGeograficasCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToAreasGeograficasStage());
		addStage(new CadastrarAreasGeograficasStage(getDP("CadastrarAreasGeograficasDP.json")));
	}	
}
