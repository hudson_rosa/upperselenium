package br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas.page;

import org.openqa.selenium.By;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoAreasGeograficasPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//form/fieldset/div[3]/input"; 
	private static final String TEXT_NOME = "//*[@id='Nome']";
		
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	
	
	public boolean isClickableButtonSalvar() {
		return isDisplayedElement(By.xpath(BUTTON_SALVAR));
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}

}