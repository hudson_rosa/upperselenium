package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_comunicacao.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarAlterarTiposDeComunicacaoDPO extends BaseHelperDPO {

	private String labelNome;
	private String labelDescricao;
	private String labelAtivo;

	public String getLabelNome() {
		return labelNome;
	}

	public void setLabelNome(String labelNome) {
		this.labelNome = labelNome;
	}

	public String getLabelDescricao() {
		return labelDescricao;
	}

	public void setLabelDescricao(String labelDescricao) {
		this.labelDescricao = labelDescricao;
	}

	public String getLabelAtivo() {
		return labelAtivo;
	}

	public void setLabelAtivo(String labelAtivo) {
		this.labelAtivo = labelAtivo;
	}

	@Override
	public String toString() {
		return "NavegarAlterarTiposDeComunicacaoDPO [labelNome=" + labelNome + ", labelDescricao=" + labelDescricao
				+ ", labelAtivo=" + labelAtivo + "]";
	}

}