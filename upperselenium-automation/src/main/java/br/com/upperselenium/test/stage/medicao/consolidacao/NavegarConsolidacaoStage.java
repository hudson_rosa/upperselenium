package br.com.upperselenium.test.stage.medicao.consolidacao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.medicao.consolidacao.dpo.NavegarConsolidacaoDPO;
import br.com.upperselenium.test.stage.medicao.consolidacao.page.ConsolidacaoPage;

public class NavegarConsolidacaoStage extends BaseStage {
	private NavegarConsolidacaoDPO navegarConsolidacaoDPO;
	private ConsolidacaoPage consolidacaoPage;	
	
	public NavegarConsolidacaoStage(String dp) {
		navegarConsolidacaoDPO = loadDataProviderFile(NavegarConsolidacaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		consolidacaoPage = initElementsFromPage(ConsolidacaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarConsolidacaoDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		consolidacaoPage.getLabelHeaderFonteDeDados(navegarConsolidacaoDPO.getLabelHeaderFonteDeDados());
		consolidacaoPage.getLabelHeaderDataHora(navegarConsolidacaoDPO.getLabelHeaderCpKWhDel());
		consolidacaoPage.getLabelHeaderCPKwhDel(navegarConsolidacaoDPO.getLabelHeaderCpKWhRec());
		consolidacaoPage.getLabelHeaderCPKwhRec(navegarConsolidacaoDPO.getLabelHeaderCpKVARhDel());
		consolidacaoPage.getLabelHeaderCPKvarDel(navegarConsolidacaoDPO.getLabelHeaderCpKVARhRec());
		consolidacaoPage.getLabelHeaderCPKvarRec(navegarConsolidacaoDPO.getLabelHeaderScpKWhDel());
		consolidacaoPage.getLabelHeaderSCPKwhDel(navegarConsolidacaoDPO.getLabelHeaderScpKWhRec());
		consolidacaoPage.getLabelHeaderSCPKwhRec(navegarConsolidacaoDPO.getLabelHeaderScpKVARhDel());
		consolidacaoPage.getLabelHeaderSCPKvarDel(navegarConsolidacaoDPO.getLabelHeaderScpKVARhRec());
		consolidacaoPage.getBreadcrumbsText(navegarConsolidacaoDPO.getBreadcrumbs());
		consolidacaoPage.clickUpToBreadcrumbs(navegarConsolidacaoDPO.getUpToBreadcrumb());
		consolidacaoPage.getWelcomeTitle(navegarConsolidacaoDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
