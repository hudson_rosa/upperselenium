package br.com.upperselenium.test.stage.configuracoes.cadastros.gateways;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.dpo.AlterarGatewaysDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.page.AlteracaoGatewaysPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.page.GatewaysPage;

public class AlterarGatewaysStage extends BaseStage {
	private AlterarGatewaysDPO alterarGatewaysDPO;
	private GatewaysPage gatewaysPage;
	private AlteracaoGatewaysPage alteracaoGatewaysPage;
	
	public AlterarGatewaysStage(String dp) {
		alterarGatewaysDPO = loadDataProviderFile(AlterarGatewaysDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		gatewaysPage = initElementsFromPage(GatewaysPage.class);
		alteracaoGatewaysPage = initElementsFromPage(AlteracaoGatewaysPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarGatewaysDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoGatewaysPage.typeTextID(alterarGatewaysDPO.getId()+getRandomStringNonSpaced());				
		alteracaoGatewaysPage.typeSelectModelo(alterarGatewaysDPO.getModelo());				
		alteracaoGatewaysPage.typeTextNome(alterarGatewaysDPO.getNome()+getRandomStringSpaced());				
		alteracaoGatewaysPage.typeTextDescricao(alterarGatewaysDPO.getDescricao()+getRandomStringSpaced());				
		alteracaoGatewaysPage.typeTextEntradaDigital1(alterarGatewaysDPO.getEntradaDigital1());				
		alteracaoGatewaysPage.typeTextEntradaDigital2(alterarGatewaysDPO.getEntradaDigital2());				
		alteracaoGatewaysPage.typeTextEntradaDigital3(alterarGatewaysDPO.getEntradaDigital3());				
		alteracaoGatewaysPage.typeTextEntradaDigital4(alterarGatewaysDPO.getEntradaDigital4());				
		alteracaoGatewaysPage.typeSelectTipoDeComunicacao(alterarGatewaysDPO.getTipoDeComunicacao());				
		alteracaoGatewaysPage.typeTextEnderecoIPPorta(alterarGatewaysDPO.getEnderecoIPPorta());				
		alteracaoGatewaysPage.typeTextEnderecoSecundarioIPPorta(alterarGatewaysDPO.getEnderecoSecundarioIPPorta());
		alteracaoGatewaysPage.keyPageDown();
		alteracaoGatewaysPage.typeTextIdDeComunicacao(alterarGatewaysDPO.getIdDeComunicacao());				
		alteracaoGatewaysPage.typeTextUsuario(alterarGatewaysDPO.getUsuario());				
		alteracaoGatewaysPage.typeTextSenha(alterarGatewaysDPO.getSenha());				
		alteracaoGatewaysPage.typeCheckComunicacaoCriptografada(alterarGatewaysDPO.getComunicacaoCriptografada());				
		alteracaoGatewaysPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoGatewaysPage.validateMessageDefault(alterarGatewaysDPO.getOperationMessage());
	}

}
