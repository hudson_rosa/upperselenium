package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class PerdasTecnicasPage extends BaseHelperPage{
	
	private static final String BUTTON_FILTRAR = ".//div[2]/div/div[2]/form/div/div[2]/input";
	private static final String LINK_IMPORTACAO_DE_DADOS_PT_REGIAO = ".//div[2]/div/div[2]/ul[2]/li/a";
	private static final String TEXT_MES = "//*[@id='MesAno']";
	private static final String TEXT_FILTER_REGIAO = "//*[@id='DataTables_Table_0']/thead/tr[1]/th[1]/span/input";
	private static final String TEXT_FILTER_PERDA_TECNICA = "//*[@id='DataTables_Table_0']/thead/tr[1]/th[3]/span/input";
	private static final String GRID_VALUE_REGIAO = "//*[@id='DataTables_Table_0']/tbody/tr/td[1]";
	private static final String GRID_VALUE_MES_ANO = "//*[@id='DataTables_Table_0']/tbody/tr/td[2]";
	private static final String GRID_VALUE_PERDA_TECNICA = "//*[@id='DataTables_Table_0']/tbody/tr/td[3]";
				
	public boolean isClickableImportacaoDeDadosDePerdasTecnicasPorRegiao() {
		return isDisplayedElement(LINK_IMPORTACAO_DE_DADOS_PT_REGIAO);
	}
	
	public void clickImportacaoDeDadosDePerdasTecnicasPorRegiao(){
		clickOnElement(LINK_IMPORTACAO_DE_DADOS_PT_REGIAO);
		waitForPageToLoadUntil10s();
	}
	
	public void typeTextMes(String value){
		typeText(TEXT_MES, value);
	}
	
	public void clickFiltrar(){
		clickOnElement(BUTTON_FILTRAR);
	}
	
	public void typeTextFilterRegiao(String value){
		typeText(TEXT_FILTER_REGIAO, value);
	}	
	
	public void typeTextFilterPerdaTecnica(String value){
		typeText(TEXT_FILTER_PERDA_TECNICA, value);
	}
	
	public String getGridTextRegiao (){
		return getGridFirstLineLabel(GRID_VALUE_REGIAO);
	}
	
	public String getGridTextMesAno (){
		return getGridFirstLineLabel(GRID_VALUE_MES_ANO);
	}
	
	public String getGridTextPerdaTecnica (){
		return getGridFirstLineLabel(GRID_VALUE_PERDA_TECNICA);
	}

}