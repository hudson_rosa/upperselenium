package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_alarmes.GoToTiposDeAlarmesStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_alarmes.NavegarAlteracaoTiposDeAlarmesStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0009TiposDeAlarmesNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0009-TiposDeAlarmes", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Tipos de Alarmes validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0009TiposDeAlarmesNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToTiposDeAlarmesStage());
		addStage(new NavegarAlteracaoTiposDeAlarmesStage(getDP("NavegarAlteracaoTiposDeAlarmesDP.json")));
	}	
}
