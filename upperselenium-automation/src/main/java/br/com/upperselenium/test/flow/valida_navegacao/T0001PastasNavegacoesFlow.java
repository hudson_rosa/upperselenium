package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.GoToPastasStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.NavegarAlteracaoPastasStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.NavegarCadastroPastasStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.NavegarPastasStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.pastas.NavegarRegistroLinkPastasStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0001PastasNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0001-Pastas", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Pastas validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0001PastasNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToPastasStage());
		addStage(new NavegarPastasStage(getDP("NavegarPastasDP.json")));
		addStage(new GoToPastasStage());
		addStage(new NavegarCadastroPastasStage(getDP("NavegarCadastroPastasDP.json")));
		addStage(new NavegarRegistroLinkPastasStage(getDP("NavegarRegistroLinkPastasDP.json")));
		addStage(new NavegarAlteracaoPastasStage(getDP("NavegarAlteracaoPastasDP.json")));
	}	
}
