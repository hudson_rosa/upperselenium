package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.tensoes.CadastrarTensoesStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.tensoes.GoToTensoesStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0024TensoesCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0024-Tensoes", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Tensões realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0024TensoesCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToTensoesStage());
		addStage(new CadastrarTensoesStage(getDP("CadastrarTensoesDP.json")));
	}	
}
