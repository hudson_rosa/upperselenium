package br.com.upperselenium.test.flow.pre_condicoes;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0001ModeloFlow.class)
@FlowParams(idTest = "PIM-PreCondicoes-T0001-Modelo", testDirPath = "", loginDirPath = "",
	goal = "Realiza as Pré-Condições para as demais suites da PIM.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0001ModeloFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP")));
	}	
}
