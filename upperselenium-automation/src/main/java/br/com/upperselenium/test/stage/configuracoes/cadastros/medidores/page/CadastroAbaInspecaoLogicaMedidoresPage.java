package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaInspecaoLogicaMedidoresPage extends BaseHelperPage{
	
	private static final String LINK_ABA_INSPECAO_LOGICA = ".//div[2]/div/div[2]/ul[2]/li[7]/a";  
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 	
	private static final String CHECK_HABILITADA = "//*[@id='InspecaoLogicaHabilitada']"; 
	private static final String TEXT_ENDERECO_IP = "//*[@id='EnderecoIp']"; 
	private static final String TEXT_NAT_SCDE = "//*[@id='NatScde']"; 
	private static final String CHECK_TCP_CLIENT = "//*[@id='IntegracaoComServerGatewayHabilitada']"; 
	private static final String CHECK_1 = "//*[@id='Hora1']"; 
	private static final String CHECK_2 = "//*[@id='Hora2']"; 
	private static final String CHECK_3 = "//*[@id='Hora3']"; 
	private static final String CHECK_4 = "//*[@id='Hora4']"; 
	private static final String CHECK_5 = "//*[@id='Hora5']"; 
	private static final String CHECK_6 = "//*[@id='Hora6']"; 
	private static final String CHECK_7 = "//*[@id='Hora7']"; 
	private static final String CHECK_8 = "//*[@id='Hora8']"; 
	private static final String CHECK_9 = "//*[@id='Hora9']"; 
	private static final String CHECK_10 = "//*[@id='Hora10']"; 
	private static final String CHECK_11 = "//*[@id='Hora11']"; 
	private static final String CHECK_12 = "//*[@id='Hora12']"; 
	private static final String CHECK_13 = "//*[@id='Hora13']"; 
	private static final String CHECK_14 = "//*[@id='Hora14']"; 
	private static final String CHECK_15 = "//*[@id='Hora15']"; 
	private static final String CHECK_16 = "//*[@id='Hora16']"; 
	private static final String CHECK_17 = "//*[@id='Hora17']"; 
	private static final String CHECK_18 = "//*[@id='Hora18']"; 
	private static final String CHECK_19 = "//*[@id='Hora19']"; 
	private static final String CHECK_20 = "//*[@id='Hora20']"; 
	private static final String CHECK_21 = "//*[@id='Hora21']"; 
	private static final String CHECK_22 = "//*[@id='Hora22']"; 
	private static final String CHECK_23 = "//*[@id='Hora23']"; 
	private static final String CHECK_24 = "//*[@id='Hora24']"; 
	
	public void typeCheckHabilitada(String value){
		typeCheckOption(CHECK_HABILITADA, value);
	}	
	
	public void typeTextEnderecoIP(String value){
		typeText(TEXT_ENDERECO_IP, value);
	}	
	
	public void typeTextNatScde(String value){
		typeText(TEXT_NAT_SCDE, value);
	}	
	
	public void typeCheckTcpClient(String value){
		typeCheckOption(CHECK_TCP_CLIENT, value);
	}	
	
	public void typeCheck1(String value){
		typeCheckOption(CHECK_1, value);
	}	
	
	public void typeCheck2(String value){
		typeCheckOption(CHECK_2, value);
	}	
	
	public void typeCheck3(String value){
		typeCheckOption(CHECK_3, value);
	}	
	
	public void typeCheck4(String value){
		typeCheckOption(CHECK_4, value);
	}	
	
	public void typeCheck5(String value){
		typeCheckOption(CHECK_5, value);
	}	
	
	public void typeCheck6(String value){
		typeCheckOption(CHECK_6, value);
	}	
	
	public void typeCheck7(String value){
		typeCheckOption(CHECK_7, value);
	}	
	
	public void typeCheck8(String value){
		typeCheckOption(CHECK_8, value);
	}	
	
	public void typeCheck9(String value){
		typeCheckOption(CHECK_9, value);
	}	
	
	public void typeCheck10(String value){
		typeCheckOption(CHECK_10, value);
	}	
	
	public void typeCheck11(String value){
		typeCheckOption(CHECK_11, value);
	}	
	
	public void typeCheck12(String value){
		typeCheckOption(CHECK_12, value);
	}	
	
	public void typeCheck13(String value){
		typeCheckOption(CHECK_13, value);
	}	
	
	public void typeCheck14(String value){
		typeCheckOption(CHECK_14, value);
	}	
	
	public void typeCheck15(String value){
		typeCheckOption(CHECK_15, value);
	}	
	
	public void typeCheck16(String value){
		typeCheckOption(CHECK_16, value);
	}	
	
	public void typeCheck17(String value){
		typeCheckOption(CHECK_17, value);
	}	
	
	public void typeCheck18(String value){
		typeCheckOption(CHECK_18, value);
	}	
	
	public void typeCheck19(String value){
		typeCheckOption(CHECK_19, value);
	}	
	
	public void typeCheck20(String value){
		typeCheckOption(CHECK_20, value);
	}	
	
	public void typeCheck21(String value){
		typeCheckOption(CHECK_21, value);
	}	
	
	public void typeCheck22(String value){
		typeCheckOption(CHECK_22, value);
	}	
	
	public void typeCheck23(String value){
		typeCheckOption(CHECK_23, value);
	}	
	
	public void typeCheck24(String value){
		typeCheckOption(CHECK_24, value);
	}	
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
				
	public void clickAbaInspecaoLogica(){
		clickOnElement(LINK_ABA_INSPECAO_LOGICA);
	}
	
}