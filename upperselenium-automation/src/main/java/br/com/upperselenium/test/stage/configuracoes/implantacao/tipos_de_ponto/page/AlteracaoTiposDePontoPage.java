package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoTiposDePontoPage extends BaseHelperPage {

	private static final String BUTTON_SALVAR = "//*[@id='salvar']";
	private static final String LINK_ABA_GERAL = "//*[@id='abastemplates']//li//a[contains(text(),'Geral')]";
	private static final String LINK_ABA_ABAS = "//*[@id='abastemplates']//li//a[contains(text(),'Abas')]";
	private static final String LINK_ABA_TEMPLATES = "//*[@id='abastemplates']//li//a[contains(text(),'Templates')]";
	private static final String LINK_ABA_ICONES = "//*[@id='abastemplates']//li//a[contains(text(),'Ícones')]";

	// ABA GERAL
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String TEXT_DESCRICAO = "//*[@id='Descricao']";
	private static final String SELECT_GRUPO_DE_MEDICAO = "//*[@id='IdListaGrupoMedicao']";
	private static final String CHECK_MONITORA_DADOS_DUPLICADOS = "//*[@id='MonitoraDuplicados']";
	private static final String LABEL_MONITORA_DADOS_DUPLICADOS = "//*[@id='1']/div[4]/label/label";

	// ABA ABAS
	private static final String LABEL_HEADER_ABAS = "//*[@id='dataTableList']/thead/tr/th[contains(text(),'Abas')]";
	private static final String LABEL_HEADER_VISUALIZAR = "//*[@id='dataTableList']/thead/tr/th[contains(text(),'Visualizar')]";

	// ABA TEMPLATES
	private static final String LABEL_HEADER_HABILITADO = "//*[@id='dataTableListTemplate']/thead/tr[2]/th[contains(text(),'Habilitado')]";
	private static final String LABEL_HEADER_GRANDEZA = "//*[@id='dataTableListTemplate']/thead/tr[2]/th[contains(text(),'Grandeza')]";
	private static final String LABEL_HEADER_TIPO_DE_GRANDEZA = "//*[@id='dataTableListTemplate']/thead/tr[2]/th[contains(text(),'Tipo de Grandeza')]";
	private static final String LABEL_HEADER_INTEGRALIZACAO = "//*[@id='dataTableListTemplate']/thead/tr[2]/th[contains(text(),'Integralização')]";
	private static final String LABEL_HEADER_EH_MEDIDA = "//*[@id='dataTableListTemplate']/thead/tr[2]/th[contains(text(),'É Medida?')]";

	public void clickAbaGeral() {
		waitForPageToLoadUntil10s();
		clickOnElement(LINK_ABA_GERAL);
	}

	public void clickAbaAbas() {
		waitForPageToLoadUntil10s();
		clickOnElement(LINK_ABA_ABAS);
	}

	public void clickAbaTemplates() {
		waitForPageToLoadUntil10s();
		clickOnElement(LINK_ABA_TEMPLATES);
	}

	public void clickAbaIcones() {
		waitForPageToLoadUntil10s();
		clickOnElement(LINK_ABA_ICONES);
	}

	public void keyPageDown() {
		useKey(TEXT_NOME, Keys.PAGE_DOWN);
	}

	public void clickSalvar() {
		clickOnElement(BUTTON_SALVAR);
	}
	
	public void getLabelButtonSalvar(String value){
		getLabel(BUTTON_SALVAR, value);
	}

	public void typeTextNomeDefault(String value) {
		typeText(TEXT_NOME, value);
	}

	public void typeTextNomeRandom(String value) {
		typeText(TEXT_NOME, value);
	}

	public void typeTextDescricao(String value) {
		typeText(TEXT_DESCRICAO, value);
	}

	public void typeSelectGrupoDeMedicao(String value) {
		typeSelectComboOption(SELECT_GRUPO_DE_MEDICAO, value);
	}

	public void typeCheckMonitoraDadosDuplicados(String value) {
		typeCheckOptionByLabel(CHECK_MONITORA_DADOS_DUPLICADOS, value);
	}
	
	public void getLabelMonitoraDadosDuplicados(String value) {
		getLabel(LABEL_MONITORA_DADOS_DUPLICADOS, value);
	}

	public void getLabelHeaderAbas(String value) {
		getLabel(LABEL_HEADER_ABAS, value);
	}

	public void getLabelHeaderVisualizar(String value) {
		getLabel(LABEL_HEADER_VISUALIZAR, value);
	}

	public void getLabelHeaderHabilitado(String value) {
		getLabel(LABEL_HEADER_HABILITADO, value);
	}

	public void getLabelHeaderGrandeza(String value) {
		getLabel(LABEL_HEADER_GRANDEZA, value);
	}

	public void getLabelHeaderTipoDeGrandeza(String value) {
		getLabel(LABEL_HEADER_TIPO_DE_GRANDEZA, value);
	}

	public void getLabelHeaderIntegralizacao(String value) {
		getLabel(LABEL_HEADER_INTEGRALIZACAO, value);
	}

	public void getLabelHeaderEhMedida(String value) {
		getLabel(LABEL_HEADER_EH_MEDIDA, value);
	}

}