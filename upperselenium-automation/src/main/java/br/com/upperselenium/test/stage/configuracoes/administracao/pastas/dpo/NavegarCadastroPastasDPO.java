package br.com.upperselenium.test.stage.configuracoes.administracao.pastas.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarCadastroPastasDPO extends BaseHelperDPO {

	private String nome;
	private String pastaPai;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPastaPai() {
		return pastaPai;
	}

	public void setPastaPai(String pastaPai) {
		this.pastaPai = pastaPai;
	}

}