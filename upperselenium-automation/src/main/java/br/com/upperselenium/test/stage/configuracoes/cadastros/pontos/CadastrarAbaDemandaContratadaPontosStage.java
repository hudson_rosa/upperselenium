package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarAbaDemandaContratadaPontosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastroAbaDemandaContratadaPontosPage;

public class CadastrarAbaDemandaContratadaPontosStage extends BaseStage {
	private CadastrarAbaDemandaContratadaPontosDPO cadastrarAbaDemandaContratadaPontosDPO;
	private CadastroAbaDemandaContratadaPontosPage cadastroAbaDemandaContratadaPontosPage;
	
	public CadastrarAbaDemandaContratadaPontosStage(String dp) {
		cadastrarAbaDemandaContratadaPontosDPO = loadDataProviderFile(CadastrarAbaDemandaContratadaPontosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaDemandaContratadaPontosPage = initElementsFromPage(CadastroAbaDemandaContratadaPontosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaDemandaContratadaPontosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaDemandaContratadaPontosPage.clickAbaDemandaContratada();
		FindElementUtil.acceptAlert();
		
		for (int index=0; index < cadastrarAbaDemandaContratadaPontosDPO.getGroupNovaDemandaContratada().size(); index++){
			cadastroAbaDemandaContratadaPontosPage.typeTextInicio(cadastrarAbaDemandaContratadaPontosDPO.getGroupNovaDemandaContratada().get(index).getInicio());
			cadastroAbaDemandaContratadaPontosPage.typeTextLimitePonta(cadastrarAbaDemandaContratadaPontosDPO.getGroupNovaDemandaContratada().get(index).getLimitePonta());
			cadastroAbaDemandaContratadaPontosPage.typeTextValorPonta(cadastrarAbaDemandaContratadaPontosDPO.getGroupNovaDemandaContratada().get(index).getValorPonta());
			cadastroAbaDemandaContratadaPontosPage.typeTextLimiteForaPonta(cadastrarAbaDemandaContratadaPontosDPO.getGroupNovaDemandaContratada().get(index).getLimiteForaDePonta());
			cadastroAbaDemandaContratadaPontosPage.typeTextValorForaPonta(cadastrarAbaDemandaContratadaPontosDPO.getGroupNovaDemandaContratada().get(index).getValorForaDePonta());
			cadastroAbaDemandaContratadaPontosPage.keyPageDown();
			cadastroAbaDemandaContratadaPontosPage.typeSelectTipo(cadastrarAbaDemandaContratadaPontosDPO.getGroupNovaDemandaContratada().get(index).getTipo());
			cadastroAbaDemandaContratadaPontosPage.clickAdicionar();
			cadastroAbaDemandaContratadaPontosPage.keyPageUp();
		}
		for (int index=0; index < cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().size(); index++){
			if(StringUtil.isNotBlankOrNotNull(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getFlagItem())){
				cadastroAbaDemandaContratadaPontosPage.keyPageDown();
				cadastroAbaDemandaContratadaPontosPage.typeTextFilterInicioDaVigencia(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getFilterInicioDaVigencia());
				cadastroAbaDemandaContratadaPontosPage.typeTextFilterLimitePonta(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getFilterLimitePonta());
				cadastroAbaDemandaContratadaPontosPage.typeTextFilterLimiteForaPonta(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getFilterLimiteForaPonta());
				cadastroAbaDemandaContratadaPontosPage.typeTextFilterValorPonta(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getFilterValorPonta());
				cadastroAbaDemandaContratadaPontosPage.typeTextFilterValorForaPonta(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getFilterValorForaPonta());
				cadastroAbaDemandaContratadaPontosPage.typeTextFilterTipo(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getFilterTipo());
				cadastroAbaDemandaContratadaPontosPage.getGridLabelInicioDaVigencia(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getInicioDaVigencia(), 1);
				cadastroAbaDemandaContratadaPontosPage.getGridLabelLimitePonta(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getLimitePonta(), 1);
				cadastroAbaDemandaContratadaPontosPage.getGridLabelLimiteForaPonta(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getLimiteForaPonta(), 1);
				cadastroAbaDemandaContratadaPontosPage.getGridLabelValorPonta(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getValorPonta(), 1);
				cadastroAbaDemandaContratadaPontosPage.getGridLabelValorForaPonta(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getValorForaPonta(), 1);
				cadastroAbaDemandaContratadaPontosPage.getGridLabelTipo(cadastrarAbaDemandaContratadaPontosDPO.getGridDemandasContratadas().get(index).getTipo(), 1);
			}
		}
		cadastroAbaDemandaContratadaPontosPage.keyPageUp();
		cadastroAbaDemandaContratadaPontosPage.clickNova();
	}

	@Override
	public void runValidations() {}

}
