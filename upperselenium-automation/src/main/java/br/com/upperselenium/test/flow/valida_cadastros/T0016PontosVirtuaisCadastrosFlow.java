package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarAbaCCEEPontosVirtuaisStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarAbaConfiguracoesPontosVirtuaisStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarAbaDemandaContratadaPontosVirtuaisStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarAbaEquipamentosRelePontosVirtuaisStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarAbaEquipamentosTrafoPontosVirtuaisStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarAbaOpcoesDeManobrasPontosVirtuaisStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarEquacoesPontosVirtuaisStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.CadastrarPontosVirtuaisStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.GoToPontosStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0016PontosVirtuaisCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0016-PontosVirtuais", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Pontos Virtuais realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0016PontosVirtuaisCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToPontosStage());
		addStage(new CadastrarPontosVirtuaisStage(getDP("CadastrarPontosVirtuaisDP.json")));
		addStage(new CadastrarAbaConfiguracoesPontosVirtuaisStage(getDP("CadastrarAbaConfiguracoesPontosVirtuaisDP.json")));
		addStage(new CadastrarAbaCCEEPontosVirtuaisStage(getDP("CadastrarAbaCCEEPontosVirtuaisDP.json")));
		addStage(new CadastrarAbaDemandaContratadaPontosVirtuaisStage(getDP("CadastrarAbaDemandaContratadaPontosVirtuaisDP.json")));
		addStage(new CadastrarAbaOpcoesDeManobrasPontosVirtuaisStage(getDP("CadastrarAbaOpcoesDeManobrasPontosVirtuaisDP.json")));
		addStage(new CadastrarAbaEquipamentosTrafoPontosVirtuaisStage(getDP("CadastrarAbaEquipamentosTrafoPontosVirtuaisDP.json")));
		addStage(new CadastrarAbaEquipamentosRelePontosVirtuaisStage(getDP("CadastrarAbaEquipamentosRelePontosVirtuaisDP.json")));
		addStage(new CadastrarEquacoesPontosVirtuaisStage(getDP("CadastrarEquacoesPontosVirtuaisDP.json")));
	}	
}
