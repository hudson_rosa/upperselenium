package br.com.upperselenium.test.stage.configuracoes.cadastros.trafos_bt;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.trafos_bt.dpo.CadastrarTrafosBTDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.trafos_bt.page.CadastroTrafosBTPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.trafos_bt.page.TrafosBTPage;

public class CadastrarTrafosBTStage extends BaseStage {
	private CadastrarTrafosBTDPO cadastrarTrafosBTDPO;
	private TrafosBTPage trafosBTPage;
	private CadastroTrafosBTPage cadastroTrafosBTPage;
	
	public CadastrarTrafosBTStage(String dp) {
		cadastrarTrafosBTDPO = loadDataProviderFile(CadastrarTrafosBTDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		trafosBTPage = initElementsFromPage(TrafosBTPage.class);
		cadastroTrafosBTPage = initElementsFromPage(CadastroTrafosBTPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarTrafosBTDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		trafosBTPage.clickNovo();
		cadastroTrafosBTPage.typeTextNome(cadastrarTrafosBTDPO.getNome()+getRandomNumberSpaced());		
		cadastroTrafosBTPage.typeTextDescricao(cadastrarTrafosBTDPO.getDescricao()+getRandomStringSpaced());		
		cadastroTrafosBTPage.typeTextIdentificador(cadastrarTrafosBTDPO.getIdentificador()+getRandomNumber());		
		cadastroTrafosBTPage.typeTextPotencia(cadastrarTrafosBTDPO.getPotencia());		
		cadastroTrafosBTPage.typeSelectNumeroDeFases(cadastrarTrafosBTDPO.getNumeroDeFases());		
		cadastroTrafosBTPage.typeTextMedicao(cadastrarTrafosBTDPO.getMedicao());		
		cadastroTrafosBTPage.typeTextAlimentadores(cadastrarTrafosBTDPO.getAlimentadores());		
		cadastroTrafosBTPage.keyPageDown();		
		cadastroTrafosBTPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroTrafosBTPage.validateMessageDefault(cadastrarTrafosBTDPO.getOperationMessage());
	}

}
