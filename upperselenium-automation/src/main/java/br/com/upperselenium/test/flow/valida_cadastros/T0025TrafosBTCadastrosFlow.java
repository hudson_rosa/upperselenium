package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.trafos_bt.CadastrarTrafosBTStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.trafos_bt.GoToTrafosBTStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0025TrafosBTCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0025-TrafosBT", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Trafos BT realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0025TrafosBTCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToTrafosBTStage());
		addStage(new CadastrarTrafosBTStage(getDP("CadastrarTrafosBTDP.json")));
	}	
}
