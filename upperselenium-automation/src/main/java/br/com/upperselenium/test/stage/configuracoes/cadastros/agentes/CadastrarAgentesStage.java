package br.com.upperselenium.test.stage.configuracoes.cadastros.agentes;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.agentes.dpo.CadastrarAgentesDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.agentes.page.AgentesPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.agentes.page.CadastroAgentesPage;

public class CadastrarAgentesStage extends BaseStage {
	private CadastrarAgentesDPO cadastrarAgentesDPO;
	private AgentesPage agentesPage;
	private CadastroAgentesPage cadastroAgentesPage;
	
	public CadastrarAgentesStage(String dp) {
		cadastrarAgentesDPO = loadDataProviderFile(CadastrarAgentesDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		agentesPage = initElementsFromPage(AgentesPage.class);
		cadastroAgentesPage = initElementsFromPage(CadastroAgentesPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAgentesDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		agentesPage.clickLinkNovo();
		cadastroAgentesPage.typeTextNome(cadastrarAgentesDPO.getNome()+getRandomStringSpaced());
		cadastroAgentesPage.typeTextDescricao(cadastrarAgentesDPO.getDescricao()+getRandomStringSpaced());
		cadastroAgentesPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAgentesPage.validateMessageDefault(cadastrarAgentesDPO.getOperationMessage());
	}
}
