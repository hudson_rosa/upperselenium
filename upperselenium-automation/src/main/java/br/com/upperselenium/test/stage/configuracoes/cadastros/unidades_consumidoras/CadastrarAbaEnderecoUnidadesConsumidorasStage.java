package br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.dpo.CadastrarAbaEnderecoUnidadesConsumidorasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.page.CadastroAbaEnderecoUnidadesConsumidorasPage;

public class CadastrarAbaEnderecoUnidadesConsumidorasStage extends BaseStage {
	private CadastrarAbaEnderecoUnidadesConsumidorasDPO cadastrarAbaEnderecoUnidadesConsumidorasDPO;
	private CadastroAbaEnderecoUnidadesConsumidorasPage cadastroAbaEnderecoUnidadesConsumidorasPage;
	
	public CadastrarAbaEnderecoUnidadesConsumidorasStage(String dp) {
		cadastrarAbaEnderecoUnidadesConsumidorasDPO = loadDataProviderFile(CadastrarAbaEnderecoUnidadesConsumidorasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaEnderecoUnidadesConsumidorasPage = initElementsFromPage(CadastroAbaEnderecoUnidadesConsumidorasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaEnderecoUnidadesConsumidorasDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaEnderecoUnidadesConsumidorasPage.clickAbaEndereco();
		cadastroAbaEnderecoUnidadesConsumidorasPage.typeTextLogradouro(cadastrarAbaEnderecoUnidadesConsumidorasDPO.getLogradouro());			
		cadastroAbaEnderecoUnidadesConsumidorasPage.typeTextNumero(cadastrarAbaEnderecoUnidadesConsumidorasDPO.getNumero());			
		cadastroAbaEnderecoUnidadesConsumidorasPage.typeTextBairro(cadastrarAbaEnderecoUnidadesConsumidorasDPO.getBairro());			
		cadastroAbaEnderecoUnidadesConsumidorasPage.typeTextCEP(cadastrarAbaEnderecoUnidadesConsumidorasDPO.getCep());			
		cadastroAbaEnderecoUnidadesConsumidorasPage.typeSelectCidade(cadastrarAbaEnderecoUnidadesConsumidorasDPO.getCidade());			
		cadastroAbaEnderecoUnidadesConsumidorasPage.typeSelectRegiao(cadastrarAbaEnderecoUnidadesConsumidorasDPO.getRegiao());
		cadastroAbaEnderecoUnidadesConsumidorasPage.typeTextLatitude(cadastrarAbaEnderecoUnidadesConsumidorasDPO.getLatitude());			
		cadastroAbaEnderecoUnidadesConsumidorasPage.typeTextLongitude(cadastrarAbaEnderecoUnidadesConsumidorasDPO.getLongitude());			
		cadastroAbaEnderecoUnidadesConsumidorasPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaEnderecoUnidadesConsumidorasPage.validateMessageDefault(cadastrarAbaEnderecoUnidadesConsumidorasDPO.getOperationMessage());
	}

}
