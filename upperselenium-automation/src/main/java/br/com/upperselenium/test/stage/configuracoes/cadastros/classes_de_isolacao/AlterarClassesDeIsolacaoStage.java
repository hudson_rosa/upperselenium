package br.com.upperselenium.test.stage.configuracoes.cadastros.classes_de_isolacao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.configuracoes.cadastros.classes_de_isolacao.dpo.AlterarClassesDeIsolacaoDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.classes_de_isolacao.page.AlteracaoClassesDeIsolacaoPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.classes_de_isolacao.page.ClassesDeIsolacaoPage;

public class AlterarClassesDeIsolacaoStage extends BaseStage {
	private AlterarClassesDeIsolacaoDPO alterarClassesDeIsolacaoDPO;
	private ClassesDeIsolacaoPage classesDeIsolacaoPage;
	private AlteracaoClassesDeIsolacaoPage alteracaoClassesDeIsolacaoPage;
	
	public AlterarClassesDeIsolacaoStage(String dp) {
		alterarClassesDeIsolacaoDPO = loadDataProviderFile(AlterarClassesDeIsolacaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		classesDeIsolacaoPage = initElementsFromPage(ClassesDeIsolacaoPage.class);
		alteracaoClassesDeIsolacaoPage = initElementsFromPage(AlteracaoClassesDeIsolacaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarClassesDeIsolacaoDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();		
		alteracaoClassesDeIsolacaoPage.typeTextValor(alterarClassesDeIsolacaoDPO.getValor());
		alteracaoClassesDeIsolacaoPage.typeTextDescricao(alterarClassesDeIsolacaoDPO.getDescricao()+getRandomStringSpaced());
		alteracaoClassesDeIsolacaoPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoClassesDeIsolacaoPage.validateMessageDefault(alterarClassesDeIsolacaoDPO.getOperationMessage());
	}

}
