package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroTiposDeMedidorPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 
	private static final String TEXT_MARCA = "//*[@id='Marca']";
	private static final String TEXT_MODELO = "//*[@id='Modelo']";
	private static final String SELECT_ESPECIE = "//*[@id='Especie']";
	private static final String TEXT_DESCRICAO = "//*[@id='Descricao']";
	
	public void typeTextMarca(String value){
		typeText(TEXT_MARCA, value);
	}	
	
	public void typeTextModelo(String value){
		typeText(TEXT_MODELO, value);
	}	

	public void typeSelectEspecie(String value){
		typeSelectComboOption(SELECT_ESPECIE, value);
	}
	
	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}	
	
	public void keyPageDown(){
		useKey(TEXT_MARCA, Keys.PAGE_DOWN);
	}	

	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}