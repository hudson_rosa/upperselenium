package br.com.upperselenium.test.stage.medicao.comandos_do_gateway.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class ComandosDoGatewayHistoricoPage extends BaseHelperPage{
	
	private static final String LABEL_HEADER_GATEWAY = "//*[@id='comandos']/thead/tr[2]/th[1]";
	private static final String LABEL_HEADER_INSTANTE = "//*[@id='comandos']/thead/tr[2]/th[2]";
	private static final String LABEL_HEADER_DESCRICAO = "//*[@id='comandos']/thead/tr[2]/th[3]";
	private static final String LABEL_HEADER_STATUS = "//*[@id='comandos']/thead/tr[2]/th[4]";
	private static final String LABEL_HEADER_ERRO = "//*[@id='comandos']/thead/tr[2]/th[5]";
	
	public void getLabelHeaderGateway(String value){
		getLabel(LABEL_HEADER_GATEWAY, value);
	}
	public void getLabelHeaderInstante(String value){
		getLabel(LABEL_HEADER_INSTANTE, value);
	}
	public void getLabelHeaderDescricao(String value){
		getLabel(LABEL_HEADER_DESCRICAO, value);
	}
	public void getLabelHeaderStatus(String value){
		getLabel(LABEL_HEADER_STATUS, value);
	}
	public void getLabelHeaderErro(String value){
		getLabel(LABEL_HEADER_ERRO, value);
	}
	
}