package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.AreasMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.DropDownMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.SideBarCadastrosPage;

public class GoToMedidoresStage extends BaseStage {
	private AreasMenuPage areasMenuPage;
	private DropDownMenuPage dropDownMenuPage;
	private SideBarCadastrosPage sideBarCadastrosPage;
	
	public GoToMedidoresStage() {}

	@Override
	public void initMappedPages() {
		areasMenuPage = initElementsFromPage(AreasMenuPage.class);
		dropDownMenuPage = initElementsFromPage(DropDownMenuPage.class);
		sideBarCadastrosPage = initElementsFromPage(SideBarCadastrosPage.class);
	}

	@Override
	public void runStage() {
		navigateToPage();	
	}
	
	private void navigateToPage() {
		waitForPageToLoad(TimePRM._20_SECS);
		areasMenuPage.clickConfiguracoes();
		waitForPageToLoadUntil10s();
		dropDownMenuPage.clickCadastros();
		waitForPageToLoadUntil10s();
		sideBarCadastrosPage.clickMedidores();
		waitForPageToLoadUntil10s();		
	}

	@Override
	public void runValidations() {}

}
