package br.com.upperselenium.test.stage.configuracoes.implantacao.configuracao_do_sistema.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarAlterarConfDoSistemaDPO extends BaseHelperDPO {

	private String labelChave;
	private String labelValor;

	public String getLabelChave() {
		return labelChave;
	}

	public void setLabelChave(String labelChave) {
		this.labelChave = labelChave;
	}

	public String getLabelValor() {
		return labelValor;
	}

	public void setLabelValor(String labelValor) {
		this.labelValor = labelValor;
	}

	@Override
	public String toString() {
		return "NavegarAlterarConfsDoSistemaDPO [labelChave=" + labelChave + ", labelValor=" + labelValor + "]";
	}

}