package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class NovaEquacaoPontosVirtuaisPage extends BaseHelperPage{
	
	// CADASTRO DE EQUAÇÕES
	private static final String BUTTON_SALVAR = "//*[@id='salvar']";
	private static final String GROUP_TEXT_VIGENCIA = "//*[@id='Vigencia']"; 
	private static final String GROUP_SELECT_SEQUENCIA_DE_CALCULO = "//*[@id='SequenciaDeCalculo']";
	private static final String GROUP_TOOLTIP_SEQUENCIA_DE_CALCULO = "//*[@id='ponto-equacoes']/form/div[2]/div/a/i";
	private static final String GROUP_TEXT_PONTOS = "//*[@id='s2id_PontosSelecionados']/ul/li/input"; 
	private static final String GROUP_TEXT_REGRAS = "//*[@id='s2id_RegrasSelecionadas']/ul/li/input";
	
	private static final String GRID_VALUE_IDENTIFICADOR = "//*[@id='termos']/tbody/tr%%/td[1]"; 
	private static final String GRID_VALUE_NOME = "//*[@id='termos']/tbody/tr/td[2]"; 
	private static final String GRID_SELECT_QUADRANTE ="//*[@id='termos']/tbody/tr%%/td[3]/select";
	private static final String GRID_CHECK_CP ="//*[@id='termos']/tbody/tr%%/td[4]/input";	 
	
	private static final String GROUP_BUTTON_SOMAR_TODOS = "//*[@id='somarTodos']";
	private static final String GROUP_TEXT_EXPRESSAO = "//*[@id='Expressao']";
	private static final String GROUP_TOOLTIP_EXPRESSAO = "//*[@id='ponto-equacoes']/form/div[7]/div/a/i"; //*[@id="ponto-equacoes"]/form/div[7]/div/a

	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
	public void typeGroupTextVigencia(String value){
		typeDatePickerDefault(GROUP_TEXT_VIGENCIA, value);
	}
	
	public void typeGroupSelectSequenciaDeCalculo(String value){
		typeSelectComboOption(GROUP_SELECT_SEQUENCIA_DE_CALCULO, value);
	}
		
	public void getGroupTooltipSequenciaDeCalculo(String value){
		getTooltip(GROUP_TOOLTIP_SEQUENCIA_DE_CALCULO, value);
	}

	public void typeGroupTextPontos(String value){
		typeTextMultipleAutocomplete(GROUP_TEXT_PONTOS, value);
	}
	
	public void typeGroupTextRegras(String value){
		typeTextMultipleAutocomplete(GROUP_TEXT_REGRAS, value);
	}
	
	public void getGridValueIdentificador(String value, int index){
		getGridLabel(GRID_VALUE_IDENTIFICADOR, value, index);
	}	

	public void getGridValueNome(String value, int index){
		getGridLabel(GRID_VALUE_NOME, value, index);
	}	
	
	public void typeGridSelectQuadrante(String value, int index){
		typeGridSelectComboOption(GRID_SELECT_QUADRANTE, value, index);
	}	
	
	public void typeGridCheckCP(String value, int index){
		typeGridCheckOption(GRID_CHECK_CP, value, index);
	}	
		
	public void clickGroupSomarTodos(){
		clickOnElement(GROUP_BUTTON_SOMAR_TODOS);
	}

	public void typeGroupTextExpressao(String value){
		typeText(GROUP_TEXT_EXPRESSAO, value);
	}
	
	public void getGroupTooltipExpressao(String value){
		getTooltip(GROUP_TOOLTIP_EXPRESSAO, value);
	}	
	
	public void keyPageDown1(){
		useKey(GROUP_SELECT_SEQUENCIA_DE_CALCULO, Keys.PAGE_DOWN);
	}	

	public void keyPageDown2(){
		useKey(GROUP_TEXT_EXPRESSAO, Keys.PAGE_DOWN);
	}	

	public void keyPageUp(){
		useKey(BUTTON_SALVAR, Keys.PAGE_UP);
	}	
	
}