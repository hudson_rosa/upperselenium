package br.com.upperselenium.test.stage.configuracoes.cadastros.postos_tarifarios.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoPostosTarifariosPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/posto-tarifario/section/button";   
	private static final String TEXT_NOME = ".//div[2]/div/div[2]/posto-tarifario/section/div[1]/div[1]/input";
	private static final String CHECK_DESLOCAR_NO_HORARIO_DE_VERAO = ".//div/div[2]/posto-tarifario/section/div[1]/div[2]/label/input";
	
	private static final String TEXT_HORARIO_DE_PONTA_INICIO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[1]/configuracao-de-horario/section/div[1]/input";
	private static final String TEXT_HORARIO_DE_PONTA_FIM = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[1]/configuracao-de-horario/section/div[2]/input";	
	private static final String CHECK_SEGUNDA_H_PONTA = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[1]/configuracao-de-horario/section/label[2]/input";
	private static final String CHECK_TERCA_H_PONTA = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[1]/configuracao-de-horario/section/label[3]/input";
	private static final String CHECK_QUARTA_H_PONTA = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[1]/configuracao-de-horario/section/label[4]/input";
	private static final String CHECK_QUINTA_H_PONTA = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[1]/configuracao-de-horario/section/label[5]/input";
	private static final String CHECK_SEXTA_H_PONTA = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[1]/configuracao-de-horario/section/label[6]/input";
	private static final String CHECK_SABADO_H_PONTA = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[1]/configuracao-de-horario/section/label[7]/input";
	private static final String CHECK_DOMINGO_H_PONTA = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[1]/configuracao-de-horario/section/label[8]/input";
	private static final String CHECK_FERIADO_H_PONTA = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[1]/configuracao-de-horario/section/label[9]/input";
		
	private static final String LABEL_HORARIO_DE_FORA_PONTA_INICIO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[2]/label[2]";
	private static final String LABEL_HORARIO_DE_FORA_PONTA_FIM = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[2]/label[4]";	
	
	private static final String TEXT_HORARIO_CAPACITIVO_INICIO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[3]/configuracao-de-horario/section/div[1]/input";
	private static final String TEXT_HORARIO_CAPACITIVO_FIM = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[3]/configuracao-de-horario/section/div[2]/input";	
	private static final String CHECK_SEGUNDA_CAPACITIVO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[3]/configuracao-de-horario/section/label[2]/input";
	private static final String CHECK_TERCA_CAPACITIVO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[3]/configuracao-de-horario/section/label[3]/input";
	private static final String CHECK_QUARTA_CAPACITIVO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[3]/configuracao-de-horario/section/label[4]/input";
	private static final String CHECK_QUINTA_CAPACITIVO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[3]/configuracao-de-horario/section/label[5]/input";
	private static final String CHECK_SEXTA_CAPACITIVO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[3]/configuracao-de-horario/section/label[6]/input";
	private static final String CHECK_SABADO_CAPACITIVO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[3]/configuracao-de-horario/section/label[7]/input";
	private static final String CHECK_DOMINGO_CAPACITIVO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[3]/configuracao-de-horario/section/label[8]/input";
	private static final String CHECK_FERIADO_CAPACITIVO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[3]/configuracao-de-horario/section/label[9]/input";
	
	private static final String CHECK_HORARIO_RESERVADO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[4]/label/input";
	private static final String TEXT_HORARIO_RESERVADO_INICIO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[4]/configuracao-de-horario/section/div[1]/input";
	private static final String TEXT_HORARIO_RESERVADO_FIM = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[4]/configuracao-de-horario/section/div[2]/input";	
	private static final String CHECK_SEGUNDA_RESERVADO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[4]/configuracao-de-horario/section/label[2]/input";
	private static final String CHECK_TERCA_RESERVADO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[4]/configuracao-de-horario/section/label[3]/input";
	private static final String CHECK_QUARTA_RESERVADO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[4]/configuracao-de-horario/section/label[4]/input";
	private static final String CHECK_QUINTA_RESERVADO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[4]/configuracao-de-horario/section/label[5]/input";
	private static final String CHECK_SEXTA_RESERVADO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[4]/configuracao-de-horario/section/label[6]/input";
	private static final String CHECK_SABADO_RESERVADO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[4]/configuracao-de-horario/section/label[7]/input";
	private static final String CHECK_DOMINGO_RESERVADO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[4]/configuracao-de-horario/section/label[8]/input";
	private static final String CHECK_FERIADO_RESERVADO = ".//div[2]/div/div[2]/posto-tarifario/section/div[3]/div[4]/configuracao-de-horario/section/label[9]/input";
		
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}
	
	public void typeCheckBoxDeslocarNoHorarioDeVerao(String value){
		typeCheckOption(CHECK_DESLOCAR_NO_HORARIO_DE_VERAO, value);
	}		
	
	public void typeTextHorarioDePontaInicio(String value){
		typeText(TEXT_HORARIO_DE_PONTA_INICIO, value);
	}	

	public void typeTextHorarioDePontaFim(String value){
		typeText(TEXT_HORARIO_DE_PONTA_FIM, value);
	}	
	
	public void typeCheckSegundaHPonta(String value){
		typeCheckOption(CHECK_SEGUNDA_H_PONTA, value);
	}		
	
	public void typeCheckTercaHPonta(String value){
		typeCheckOption(CHECK_TERCA_H_PONTA, value);
	}		
	
	public void typeCheckQuartaHPonta(String value){
		typeCheckOption(CHECK_QUARTA_H_PONTA, value);
	}		
	
	public void typeCheckQuintaHPonta(String value){
		typeCheckOption(CHECK_QUINTA_H_PONTA, value);
	}		
	
	public void typeCheckSextaHPonta(String value){
		typeCheckOption(CHECK_SEXTA_H_PONTA, value);
	}		
	
	public void typeCheckSabadoHPonta(String value){
		typeCheckOption(CHECK_SABADO_H_PONTA, value);
	}		
	
	public void typeCheckDomingoHPonta(String value){
		typeCheckOption(CHECK_DOMINGO_H_PONTA, value);
	}		
	
	public void typeCheckFeriadoHPonta(String value){
		typeCheckOption(CHECK_FERIADO_H_PONTA, value);
	}		
	
	public String getLabelHorarioDeForaPontaInicio(){
		return getValueFromElement(LABEL_HORARIO_DE_FORA_PONTA_INICIO);
	}	
	
	public String getLabelHorarioDeForaPontaFim(){
		return getValueFromElement(LABEL_HORARIO_DE_FORA_PONTA_FIM);
	}		
	
	
	public void typeTextHorarioCapacitivoInicio(String value){
		typeText(TEXT_HORARIO_CAPACITIVO_INICIO, value);
	}	

	public void typeTextHorarioCapacitivoFim(String value){
		typeText(TEXT_HORARIO_CAPACITIVO_FIM, value);
	}	
	
	public void typeCheckSegundaHCapacitivo(String value){
		typeCheckOption(CHECK_SEGUNDA_CAPACITIVO, value);
	}		
	
	public void typeCheckTercaHCapacitivo(String value){
		typeCheckOption(CHECK_TERCA_CAPACITIVO, value);
	}		
	
	public void typeCheckQuartaHCapacitivo(String value){
		typeCheckOption(CHECK_QUARTA_CAPACITIVO, value);
	}		
	
	public void typeCheckQuintaHCapacitivo(String value){
		typeCheckOption(CHECK_QUINTA_CAPACITIVO, value);
	}		
	
	public void typeCheckSextaHCapacitivo(String value){
		typeCheckOption(CHECK_SEXTA_CAPACITIVO, value);
	}		
	
	public void typeCheckSabadoHCapacitivo(String value){
		typeCheckOption(CHECK_SABADO_CAPACITIVO, value);
	}		
	
	public void typeCheckDomingoHCapacitivo(String value){
		typeCheckOption(CHECK_DOMINGO_CAPACITIVO, value);
	}		
	
	public void typeCheckFeriadoHCapacitivo(String value){
		typeCheckOption(CHECK_FERIADO_CAPACITIVO, value);
	}		
	
	
	public void typeCheckBoxHorarioReservado(String value){
		typeCheckOption(CHECK_HORARIO_RESERVADO, value);
	}	
	
	public void typeTextHorarioReservadoInicio(String value){
		typeText(TEXT_HORARIO_RESERVADO_INICIO, value);
	}	

	public void typeTextHorarioReservadoFim(String value){
		typeText(TEXT_HORARIO_RESERVADO_FIM, value);
	}	
	
	public void typeCheckSegundaHReservado(String value){
		typeCheckOption(CHECK_SEGUNDA_RESERVADO, value);
	}		
	
	public void typeCheckTercaHReservado(String value){
		typeCheckOption(CHECK_TERCA_RESERVADO, value);
	}		
	
	public void typeCheckQuartaHReservado(String value){
		typeCheckOption(CHECK_QUARTA_RESERVADO, value);
	}		
	
	public void typeCheckQuintaHReservado(String value){
		typeCheckOption(CHECK_QUINTA_RESERVADO, value);
	}		
	
	public void typeCheckSextaHReservado(String value){
		typeCheckOption(CHECK_SEXTA_RESERVADO, value);
	}		
	
	public void typeCheckSabadoHReservado(String value){
		typeCheckOption(CHECK_SABADO_RESERVADO, value);
	}		
	
	public void typeCheckDomingoHReservado(String value){
		typeCheckOption(CHECK_DOMINGO_RESERVADO, value);
	}		
	
	public void typeCheckFeriadoHReservado(String value){
		typeCheckOption(CHECK_FERIADO_RESERVADO, value);
	}		
		
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}