package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarRegioesDPO extends BaseHelperDPO {

	private String nome;
	private String descricao;
	private String areaGeografica;
	private String pontoDeEnergiaRequerida;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getAreaGeografica() {
		return areaGeografica;
	}

	public void setAreaGeografica(String areaGeografica) {
		this.areaGeografica = areaGeografica;
	}

	public String getPontoDeEnergiaRequerida() {
		return pontoDeEnergiaRequerida;
	}

	public void setPontoDeEnergiaRequerida(String pontoDeEnergiaRequerida) {
		this.pontoDeEnergiaRequerida = pontoDeEnergiaRequerida;
	}

}