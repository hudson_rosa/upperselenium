package br.com.upperselenium.test.stage.configuracoes.administracao.pastas.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarAlteracaoPastasDPO extends BaseHelperDPO {

	private String nome;
	private String pastaPai;
	private String clickItem;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPastaPai() {
		return pastaPai;
	}

	public void setPastaPai(String pastaPai) {
		this.pastaPai = pastaPai;
	}

	public String getClickItem() {
		return clickItem;
	}

	public void setClickItem(String clickItem) {
		this.clickItem = clickItem;
	}

	@Override
	public String toString() {
		return "NavegarAlteracaoPastasDPO [nome=" + nome + ", pastaPai=" + pastaPai + ", clickItem=" + clickItem + "]";
	}

}