package br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaPastasSecundariasClientsSCDEPage extends BaseHelperPage{
	
	private static final String LINK_ABA_PASTAS_SECUNDARIAS = ".//div[2]/div/div[2]/ul[2]/li[3]/a"; 
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/div[6]/form/fieldset/div[3]/input";
	
	private static final String CHECK_HABILITADA_PASTA_1 = "//*[@id='PastaSecundaria01_Habilitada']"; 
	private static final String CHECK_HABILITADA_PASTA_2 = "//*[@id='PastaSecundaria02_Habilitada']";
	private static final String CHECK_HABILITADA_PASTA_3 = "//*[@id='PastaSecundaria03_Habilitada']"; 
	private static final String CHECK_HABILITADA_PASTA_4 = "//*[@id='PastaSecundaria04_Habilitada']"; 
	private static final String CHECK_HABILITADA_PASTA_5 = "//*[@id='PastaSecundaria05_Habilitada']"; 
	private static final String CHECK_HABILITADA_PASTA_6 = "//*[@id='PastaSecundaria06_Habilitada']"; 
	private static final String CHECK_HABILITADA_PASTA_7 = "//*[@id='PastaSecundaria07_Habilitada']"; 
	private static final String CHECK_HABILITADA_PASTA_8 = "//*[@id='PastaSecundaria08_Habilitada']"; 
	private static final String CHECK_HABILITADA_PASTA_9 = "//*[@id='PastaSecundaria09_Habilitada']"; 
	private static final String CHECK_HABILITADA_PASTA_10 = "//*[@id='PastaSecundaria10_Habilitada']"; 
	
	private static final String SELECT_TIPO_PASTA_1 = "//*[@id='PastaSecundaria01_Tipo']"; 
	private static final String SELECT_TIPO_PASTA_2 = "//*[@id='PastaSecundaria02_Tipo']"; 
	private static final String SELECT_TIPO_PASTA_3 = "//*[@id='PastaSecundaria03_Tipo']"; 
	private static final String SELECT_TIPO_PASTA_4 = "//*[@id='PastaSecundaria04_Tipo']"; 
	private static final String SELECT_TIPO_PASTA_5 = "//*[@id='PastaSecundaria05_Tipo']"; 
	private static final String SELECT_TIPO_PASTA_6 = "//*[@id='PastaSecundaria06_Tipo']"; 
	private static final String SELECT_TIPO_PASTA_7 = "//*[@id='PastaSecundaria07_Tipo']"; 
	private static final String SELECT_TIPO_PASTA_8 = "//*[@id='PastaSecundaria08_Tipo']"; 
	private static final String SELECT_TIPO_PASTA_9 = "//*[@id='PastaSecundaria09_Tipo']"; 
	private static final String SELECT_TIPO_PASTA_10 = "//*[@id='PastaSecundaria10_Tipo']"; 

	private static final String TEXT_PATH_PASTA_1 = "//*[@id='PastaSecundaria01_Path']"; 
	private static final String TEXT_PATH_PASTA_2 = "//*[@id='PastaSecundaria02_Path']"; 
	private static final String TEXT_PATH_PASTA_3 = "//*[@id='PastaSecundaria03_Path']"; 
	private static final String TEXT_PATH_PASTA_4 = "//*[@id='PastaSecundaria04_Path']"; 
	private static final String TEXT_PATH_PASTA_5 = "//*[@id='PastaSecundaria05_Path']"; 
	private static final String TEXT_PATH_PASTA_6 = "//*[@id='PastaSecundaria06_Path']"; 
	private static final String TEXT_PATH_PASTA_7 = "//*[@id='PastaSecundaria07_Path']"; 
	private static final String TEXT_PATH_PASTA_8 = "//*[@id='PastaSecundaria08_Path']"; 
	private static final String TEXT_PATH_PASTA_9 = "//*[@id='PastaSecundaria09_Path']"; 
	private static final String TEXT_PATH_PASTA_10 = "//*[@id='PastaSecundaria10_Path']"; 
	
	private static final String TEXT_USUARIO_PASTA_1 = "//*[@id='PastaSecundaria01_Usuario']";
	private static final String TEXT_USUARIO_PASTA_2 = "//*[@id='PastaSecundaria02_Usuario']";
	private static final String TEXT_USUARIO_PASTA_3 = "//*[@id='PastaSecundaria03_Usuario']";
	private static final String TEXT_USUARIO_PASTA_4 = "//*[@id='PastaSecundaria04_Usuario']";
	private static final String TEXT_USUARIO_PASTA_5 = "//*[@id='PastaSecundaria05_Usuario']";
	private static final String TEXT_USUARIO_PASTA_6 = "//*[@id='PastaSecundaria06_Usuario']";
	private static final String TEXT_USUARIO_PASTA_7 = "//*[@id='PastaSecundaria07_Usuario']";
	private static final String TEXT_USUARIO_PASTA_8 = "//*[@id='PastaSecundaria08_Usuario']";
	private static final String TEXT_USUARIO_PASTA_9 = "//*[@id='PastaSecundaria09_Usuario']";
	private static final String TEXT_USUARIO_PASTA_10 = "//*[@id='PastaSecundaria10_Usuario']";
	
	private static final String TEXT_SENHA_PASTA_1 = "//*[@id='PastaSecundaria01_Senha']";
	private static final String TEXT_SENHA_PASTA_2 = "//*[@id='PastaSecundaria02_Senha']";
	private static final String TEXT_SENHA_PASTA_3 = "//*[@id='PastaSecundaria03_Senha']";
	private static final String TEXT_SENHA_PASTA_4 = "//*[@id='PastaSecundaria04_Senha']";
	private static final String TEXT_SENHA_PASTA_5 = "//*[@id='PastaSecundaria05_Senha']";
	private static final String TEXT_SENHA_PASTA_6 = "//*[@id='PastaSecundaria06_Senha']";
	private static final String TEXT_SENHA_PASTA_7 = "//*[@id='PastaSecundaria07_Senha']";
	private static final String TEXT_SENHA_PASTA_8 = "//*[@id='PastaSecundaria08_Senha']";
	private static final String TEXT_SENHA_PASTA_9 = "//*[@id='PastaSecundaria09_Senha']";
	private static final String TEXT_SENHA_PASTA_10 = "//*[@id='PastaSecundaria10_Senha']";
	
	private static final String TEXT_DOMINIO_PASTA_1 = "//*[@id='PastaSecundaria01_Dominio']";
	private static final String TEXT_DOMINIO_PASTA_2 = "//*[@id='PastaSecundaria02_Dominio']";
	private static final String TEXT_DOMINIO_PASTA_3 = "//*[@id='PastaSecundaria03_Dominio']";
	private static final String TEXT_DOMINIO_PASTA_4 = "//*[@id='PastaSecundaria04_Dominio']";
	private static final String TEXT_DOMINIO_PASTA_5 = "//*[@id='PastaSecundaria05_Dominio']";
	private static final String TEXT_DOMINIO_PASTA_6 = "//*[@id='PastaSecundaria06_Dominio']";
	private static final String TEXT_DOMINIO_PASTA_7 = "//*[@id='PastaSecundaria07_Dominio']";
	private static final String TEXT_DOMINIO_PASTA_8 = "//*[@id='PastaSecundaria08_Dominio']";
	private static final String TEXT_DOMINIO_PASTA_9 = "//*[@id='PastaSecundaria09_Dominio']";
	private static final String TEXT_DOMINIO_PASTA_10 = "//*[@id='PastaSecundaria10_Dominio']";
	
	public void typeCheckBoxHabilitadaPasta1(String value){
		typeCheckOption(CHECK_HABILITADA_PASTA_1, value);
	}	
	
	public void typeCheckBoxHabilitadaPasta2(String value){
		typeCheckOption(CHECK_HABILITADA_PASTA_2, value);
	}	
	
	public void typeCheckBoxHabilitadaPasta3(String value){
		typeCheckOption(CHECK_HABILITADA_PASTA_3, value);
	}	
	
	public void typeCheckBoxHabilitadaPasta4(String value){
		typeCheckOption(CHECK_HABILITADA_PASTA_4, value);
	}	
	
	public void typeCheckBoxHabilitadaPasta5(String value){
		typeCheckOption(CHECK_HABILITADA_PASTA_5, value);
	}	
	
	public void typeCheckBoxHabilitadaPasta6(String value){
		typeCheckOption(CHECK_HABILITADA_PASTA_6, value);
	}	
	
	public void typeCheckBoxHabilitadaPasta7(String value){
		typeCheckOption(CHECK_HABILITADA_PASTA_7, value);
	}	
	
	public void typeCheckBoxHabilitadaPasta8(String value){
		typeCheckOption(CHECK_HABILITADA_PASTA_8, value);
	}	
	
	public void typeCheckBoxHabilitadaPasta9(String value){
		typeCheckOption(CHECK_HABILITADA_PASTA_9, value);
	}	
	
	public void typeCheckBoxHabilitadaPasta10(String value){
		typeCheckOption(CHECK_HABILITADA_PASTA_10, value);
	}	
	
	
	public void typeSelectTipoPasta1(String value){		
		typeSelectComboOption(SELECT_TIPO_PASTA_1, value);
	}
	
	public void typeSelectTipoPasta2(String value){		
		typeSelectComboOption(SELECT_TIPO_PASTA_2, value);
	}
	
	public void typeSelectTipoPasta3(String value){		
		typeSelectComboOption(SELECT_TIPO_PASTA_3, value);
	}
	
	public void typeSelectTipoPasta4(String value){		
		typeSelectComboOption(SELECT_TIPO_PASTA_4, value);
	}
	
	public void typeSelectTipoPasta5(String value){		
		typeSelectComboOption(SELECT_TIPO_PASTA_5, value);
	}
	
	public void typeSelectTipoPasta6(String value){		
		typeSelectComboOption(SELECT_TIPO_PASTA_6, value);
	}
	
	public void typeSelectTipoPasta7(String value){		
		typeSelectComboOption(SELECT_TIPO_PASTA_7, value);
	}
	
	public void typeSelectTipoPasta8(String value){		
		typeSelectComboOption(SELECT_TIPO_PASTA_8, value);
	}
	
	public void typeSelectTipoPasta9(String value){		
		typeSelectComboOption(SELECT_TIPO_PASTA_9, value);
	}
	
	public void typeSelectTipoPasta10(String value){		
		typeSelectComboOption(SELECT_TIPO_PASTA_10, value);
	}

	
	public void typeTextPathPasta1(String value) {
		typeText(TEXT_PATH_PASTA_1, value);
	}
	
	public void typeTextPathPasta2(String value) {
		typeText(TEXT_PATH_PASTA_2, value);
	}
	
	public void typeTextPathPasta3(String value) {
		typeText(TEXT_PATH_PASTA_3, value);
	}
	
	public void typeTextPathPasta4(String value) {
		typeText(TEXT_PATH_PASTA_4, value);
	}
	
	public void typeTextPathPasta5(String value) {
		typeText(TEXT_PATH_PASTA_5, value);
	}
	
	public void typeTextPathPasta6(String value) {
		typeText(TEXT_PATH_PASTA_6, value);
	}
	
	public void typeTextPathPasta7(String value) {
		typeText(TEXT_PATH_PASTA_7, value);
	}
	
	public void typeTextPathPasta8(String value) {
		typeText(TEXT_PATH_PASTA_8, value);
	}
	
	public void typeTextPathPasta9(String value) {
		typeText(TEXT_PATH_PASTA_9, value);
	}
	
	public void typeTextPathPasta10(String value) {
		typeText(TEXT_PATH_PASTA_10, value);
	}
	
	
	public void typeTextUsuarioPasta1(String value) {
		typeText(TEXT_USUARIO_PASTA_1, value);
	}
	
	public void typeTextUsuarioPasta2(String value) {
		typeText(TEXT_USUARIO_PASTA_2, value);
	}
	
	public void typeTextUsuarioPasta3(String value) {
		typeText(TEXT_USUARIO_PASTA_3, value);
	}
	
	public void typeTextUsuarioPasta4(String value) {
		typeText(TEXT_USUARIO_PASTA_4, value);
	}
	
	public void typeTextUsuarioPasta5(String value) {
		typeText(TEXT_USUARIO_PASTA_5, value);
	}
	
	public void typeTextUsuarioPasta6(String value) {
		typeText(TEXT_USUARIO_PASTA_6, value);
	}
	
	public void typeTextUsuarioPasta7(String value) {
		typeText(TEXT_USUARIO_PASTA_7, value);
	}
	
	public void typeTextUsuarioPasta8(String value) {
		typeText(TEXT_USUARIO_PASTA_8, value);
	}
	
	public void typeTextUsuarioPasta9(String value) {
		typeText(TEXT_USUARIO_PASTA_9, value);
	}
	
	public void typeTextUsuarioPasta10(String value) {
		typeText(TEXT_USUARIO_PASTA_10, value);
	}
	
	
	public void typeTextSenhaPasta1(String value) {
		typeText(TEXT_SENHA_PASTA_1, value);
	}
	
	public void typeTextSenhaPasta2(String value) {
		typeText(TEXT_SENHA_PASTA_2, value);
	}
	
	public void typeTextSenhaPasta3(String value) {
		typeText(TEXT_SENHA_PASTA_3, value);
	}
	
	public void typeTextSenhaPasta4(String value) {
		typeText(TEXT_SENHA_PASTA_4, value);
	}
	
	public void typeTextSenhaPasta5(String value) {
		typeText(TEXT_SENHA_PASTA_5, value);
	}
	
	public void typeTextSenhaPasta6(String value) {
		typeText(TEXT_SENHA_PASTA_6, value);
	}
	
	public void typeTextSenhaPasta7(String value) {
		typeText(TEXT_SENHA_PASTA_7, value);
	}
	
	public void typeTextSenhaPasta8(String value) {
		typeText(TEXT_SENHA_PASTA_8, value);
	}
	
	public void typeTextSenhaPasta9(String value) {
		typeText(TEXT_SENHA_PASTA_9, value);
	}
	
	public void typeTextSenhaPasta10(String value) {
		typeText(TEXT_SENHA_PASTA_10, value);
	}
	
	
	public void typeTextDominioPasta1(String value) {
		typeText(TEXT_DOMINIO_PASTA_1, value);
	}
	
	public void typeTextDominioPasta2(String value) {
		typeText(TEXT_DOMINIO_PASTA_2, value);
	}
	
	public void typeTextDominioPasta3(String value) {
		typeText(TEXT_DOMINIO_PASTA_3, value);
	}
	
	public void typeTextDominioPasta4(String value) {
		typeText(TEXT_DOMINIO_PASTA_4, value);
	}
	
	public void typeTextDominioPasta5(String value) {
		typeText(TEXT_DOMINIO_PASTA_5, value);
	}
	
	public void typeTextDominioPasta6(String value) {
		typeText(TEXT_DOMINIO_PASTA_6, value);
	}
	
	public void typeTextDominioPasta7(String value) {
		typeText(TEXT_DOMINIO_PASTA_7, value);
	}
	
	public void typeTextDominioPasta8(String value) {
		typeText(TEXT_DOMINIO_PASTA_8, value);
	}
	
	public void typeTextDominioPasta9(String value) {
		typeText(TEXT_DOMINIO_PASTA_9, value);
	}
	
	public void typeTextDominioPasta10(String value) {
		typeText(TEXT_DOMINIO_PASTA_10, value);
	}
	
	public void useKeyPageDown() {
		useKey(LINK_ABA_PASTAS_SECUNDARIAS, Keys.PAGE_DOWN);
	}
	
	public boolean isClickableButtonSalvar() {
		return isDisplayedElement(By.xpath(BUTTON_SALVAR));
	}

	public void clickSalvar() {
		clickOnElement(BUTTON_SALVAR);
	}

	public void clickAbaPastasSecundarias() {
		clickOnElement(LINK_ABA_PASTAS_SECUNDARIAS);
	}
	
}