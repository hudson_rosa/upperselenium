package br.com.upperselenium.test.stage.configuracoes.cadastros.ramo_de_atividade;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.ramo_de_atividade.dpo.AlterarRamoDeAtividadeDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.ramo_de_atividade.page.AlteracaoRamoDeAtividadePage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.ramo_de_atividade.page.RamoDeAtividadePage;

public class AlterarRamoDeAtividadeStage extends BaseStage {
	private AlterarRamoDeAtividadeDPO cadastrarRamoDeAtividadeDPO;
	private RamoDeAtividadePage ramoDeAtividadePage;
	private AlteracaoRamoDeAtividadePage cadastroRamoDeAtividadePage;
	
	public AlterarRamoDeAtividadeStage(String dp) {
		cadastrarRamoDeAtividadeDPO = loadDataProviderFile(AlterarRamoDeAtividadeDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		ramoDeAtividadePage = initElementsFromPage(RamoDeAtividadePage.class);
		cadastroRamoDeAtividadePage = initElementsFromPage(AlteracaoRamoDeAtividadePage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarRamoDeAtividadeDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroRamoDeAtividadePage.typeTextNome(cadastrarRamoDeAtividadeDPO.getNome()+getRandomStringSpaced());		
		cadastroRamoDeAtividadePage.typeTextDescricao(cadastrarRamoDeAtividadeDPO.getDescricao()+getRandomStringSpaced());		
		cadastroRamoDeAtividadePage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroRamoDeAtividadePage.validateMessageDefault(cadastrarRamoDeAtividadeDPO.getOperationMessage());
	}

}
