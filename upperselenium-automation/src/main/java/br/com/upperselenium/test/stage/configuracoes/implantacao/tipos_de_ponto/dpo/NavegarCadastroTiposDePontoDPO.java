package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarCadastroTiposDePontoDPO extends BaseHelperDPO {

	private String nome;
	private String labelButtonSalvar;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLabelButtonSalvar() {
		return labelButtonSalvar;
	}

	public void setLabelButtonSalvar(String labelButtonSalvar) {
		this.labelButtonSalvar = labelButtonSalvar;
	}

	@Override
	public String toString() {
		return "NavegarCadastroTiposDePontoDPO [nome=" + nome + ", labelButtonSalvar=" + labelButtonSalvar + "]";
	}

}