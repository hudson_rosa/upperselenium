package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarTiposDePontoCCEEDPO extends BaseHelperDPO {

	private String labelNome;
	private String labelDescricao;

	public String getLabelNome() {
		return labelNome;
	}

	public void setLabelNome(String labelNome) {
		this.labelNome = labelNome;
	}

	public String getLabelDescricao() {
		return labelDescricao;
	}

	public void setLabelDescricao(String labelDescricao) {
		this.labelDescricao = labelDescricao;
	}

	@Override
	public String toString() {
		return "NavegarTiposDePontoDPO [labelNome=" + labelNome + ", labelDescricao=" + labelDescricao + "]";
	}

}