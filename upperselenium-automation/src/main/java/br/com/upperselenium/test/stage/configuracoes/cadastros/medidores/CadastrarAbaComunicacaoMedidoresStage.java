package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo.CadastrarAbaComunicacaoMedidoresAtoLDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo.CadastrarAbaComunicacaoMedidoresDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo.CadastrarAbaComunicacaoMedidoresMtoZDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page.CadastroAbaComunicacaoMedidoresPage;

public class CadastrarAbaComunicacaoMedidoresStage extends BaseStage {
	private CadastrarAbaComunicacaoMedidoresDPO cadastrarAbaComunicacaoDPO;
	private CadastrarAbaComunicacaoMedidoresAtoLDPO cadastrarAbaComunicacaoAtoLDPO;
	private CadastrarAbaComunicacaoMedidoresMtoZDPO cadastrarAbaComunicacaoMtoZDPO;
	private CadastroAbaComunicacaoMedidoresPage cadastrarAbaComunicacaoMedidoresPage;

	public CadastrarAbaComunicacaoMedidoresStage(String dp) {
		cadastrarAbaComunicacaoDPO = loadDataProviderFile(CadastrarAbaComunicacaoMedidoresDPO.class, dp);
		cadastrarAbaComunicacaoAtoLDPO = loadDataProviderFile(CadastrarAbaComunicacaoMedidoresAtoLDPO.class, dp);
		cadastrarAbaComunicacaoMtoZDPO = loadDataProviderFile(CadastrarAbaComunicacaoMedidoresMtoZDPO.class, dp);
	}

	@Override
	public void initMappedPages() {
		cadastrarAbaComunicacaoMedidoresPage = initElementsFromPage(CadastroAbaComunicacaoMedidoresPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaComunicacaoDPO.getIssue());
		execute();
	}

	private void execute() {
		cadastrarAbaComunicacaoMedidoresPage.clickAbaComunicacao();
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getAbsSsuEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoAbsSsuModBus());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAbsSsuEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getAbsSsuEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAbsSsuPorta(cadastrarAbaComunicacaoAtoLDPO.getAbsSsuPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAbsSsuId(cadastrarAbaComunicacaoAtoLDPO.getAbsSsuId());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getAnsiC12EnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoAnsiC12());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAnsiC12EnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getAnsiC12EnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAnsiC12Porta(cadastrarAbaComunicacaoAtoLDPO.getAnsiC12Porta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAnsiC12ModeloDeMedidor(cadastrarAbaComunicacaoAtoLDPO.getAnsiC12ModeloDeMedidor());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getConstruservIdentificadorDaEstacao())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoApiConstruserv());
			cadastrarAbaComunicacaoMedidoresPage.typeTextConstruservIdentificadorDaEstacao(cadastrarAbaComunicacaoAtoLDPO.getConstruservIdentificadorDaEstacao());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoMediaAnemSuperior())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoArqAmmonit());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitVelVentoMediaAnemSuperior(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoMediaAnemSuperior());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitVelVentoMaximaAnemSuperior(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoMaximaAnemSuperior());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitVelVentoMinimaAnemSuperior(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoMinimaAnemSuperior());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitVelVentoDesvioPadraoAnemSuperior(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoDesvioPadraoAnemSuperior());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitVelVentoMediaAnem2(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoMediaAnem2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitVelVentoMaximaAnem2(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoMaximaAnem2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitVelVentoMinimaAnem2(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoMinimaAnem2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitVelVentoDesvioPadraoAnem2(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoDesvioPadraoAnem2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitVelVentoMediaAnem3(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoMediaAnem3());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitVelVentoMaximaAnem3(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoMaximaAnem3());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitVelVentoMinimaAnem3(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoMinimaAnem3());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitVelVentoDesvioPadraoAnem3(cadastrarAbaComunicacaoAtoLDPO.getAmmonitVelVentoDesvioPadraoAnem3());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitDirVentoMediaWindVaneSuperior(cadastrarAbaComunicacaoAtoLDPO.getAmmonitDirVentoMediaWindVaneSuperior());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitDirVentoDesvioPadraoWindVaneSuperior(cadastrarAbaComunicacaoAtoLDPO.getAmmonitDirVentoDesvioPadraoWindVaneSuperior());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitDirVentoMediaWindVane2(cadastrarAbaComunicacaoAtoLDPO.getAmmonitDirVentoMediaWindVane2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitDirVentoDesvioPadraoWindVane2(cadastrarAbaComunicacaoAtoLDPO.getAmmonitDirVentoDesvioPadraoWindVane2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitPressaoDoAr(cadastrarAbaComunicacaoAtoLDPO.getAmmonitPressaoDoAr());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitTemperaturaDoAr(cadastrarAbaComunicacaoAtoLDPO.getAmmonitTemperaturaDoAr());
			cadastrarAbaComunicacaoMedidoresPage.typeTextAmmonitUmidadeRelativaDoAr(cadastrarAbaComunicacaoAtoLDPO.getAmmonitUmidadeRelativaDoAr());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoMediaAnemSuperior())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoArqNomad());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadVelVentoMediaAnemSuperior(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoMediaAnemSuperior());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadVelVentoMaximaAnemSuperior(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoMaximaAnemSuperior());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadVelVentoMinimaAnemSuperior(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoMinimaAnemSuperior());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadVelVentoDesvioPadraoAnemSuperior(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoDesvioPadraoAnemSuperior());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadVelVentoMediaAnem2(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoMediaAnem2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadVelVentoMaximaAnem2(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoMaximaAnem2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadVelVentoMinimaAnem2(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoMinimaAnem2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadVelVentoDesvioPadraoAnem2(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoDesvioPadraoAnem2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadVelVentoMediaAnem3(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoMediaAnem3());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadVelVentoMaximaAnem3(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoMaximaAnem3());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadVelVentoMinimaAnem3(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoMinimaAnem3());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadVelVentoDesvioPadraoAnem3(cadastrarAbaComunicacaoAtoLDPO.getNomadVelVentoDesvioPadraoAnem3());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadDirVentoMediaWindVaneSuperior(cadastrarAbaComunicacaoAtoLDPO.getNomadDirVentoMediaWindVaneSuperior());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadDirVentoDesvioPadraoWindVaneSuperior(cadastrarAbaComunicacaoAtoLDPO.getNomadDirVentoDesvioPadraoWindVaneSuperior());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadDirVentoMediaWindVane2(cadastrarAbaComunicacaoAtoLDPO.getNomadDirVentoMediaWindVane2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadDirVentoDesvioPadraoWindVane2(cadastrarAbaComunicacaoAtoLDPO.getNomadDirVentoDesvioPadraoWindVane2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadPressaoDoAr(cadastrarAbaComunicacaoAtoLDPO.getNomadPressaoDoAr());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadTemperaturaDoAr(cadastrarAbaComunicacaoAtoLDPO.getNomadTemperaturaDoAr());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNomadUmidadeRelativaDoAr(cadastrarAbaComunicacaoAtoLDPO.getNomadUmidadeRelativaDoAr());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getCckStrRemoto())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoCck());
			cadastrarAbaComunicacaoMedidoresPage.typeTextCckStrRemoto(cadastrarAbaComunicacaoAtoLDPO.getCckStrRemoto());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getCck7550EthEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoCck7550SEth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextCck7550EthEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getCck7550EthEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextCck7550EthPorta(cadastrarAbaComunicacaoAtoLDPO.getCck7550EthPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextCck7550EthId(cadastrarAbaComunicacaoAtoLDPO.getCck7550EthId());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getCd103Gateway())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoCd103());
			cadastrarAbaComunicacaoMedidoresPage.typeTextCd103Gateway(cadastrarAbaComunicacaoAtoLDPO.getCd103Gateway());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getTitanCaixa())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoColetorTitan());
			cadastrarAbaComunicacaoMedidoresPage.typeTextTitanCaixa(cadastrarAbaComunicacaoAtoLDPO.getTitanCaixa());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getC750Gateway())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoC750());
			cadastrarAbaComunicacaoMedidoresPage.typeTextC750Gateway(cadastrarAbaComunicacaoAtoLDPO.getC750Gateway());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getElo2113MpsEthEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoElo2113Eth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextElo2113MpsEthEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getElo2113MpsEthEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextElo2113MpsEthPorta(cadastrarAbaComunicacaoAtoLDPO.getElo2113MpsEthPorta());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getElo2180EthEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoElo2180Eth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextElo2180EthEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getElo2180EthEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextElo2180EthPorta(cadastrarAbaComunicacaoAtoLDPO.getElo2180EthPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextElo2180EthId(cadastrarAbaComunicacaoAtoLDPO.getElo2180EthId());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getElo2180V2xxEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoElo2180V2XXEth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextElo2180V2xxEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getElo2180V2xxEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextElo2180V2xxPorta(cadastrarAbaComunicacaoAtoLDPO.getElo2180V2xxPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextElo2180V2xxId(cadastrarAbaComunicacaoAtoLDPO.getElo2180V2xxId());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getElsterA3rbrEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoElsterA3RBRMp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextElsterA3rbrEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getElsterA3rbrEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextElsterA3rbrPorta(cadastrarAbaComunicacaoAtoLDPO.getElsterA3rbrPorta());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getE650MpEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoE650Mp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextE650MpEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getE650MpEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextE650MpPorta(cadastrarAbaComunicacaoAtoLDPO.getE650MpPorta());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getE750EthEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoE750Eth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextE750EthEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getE750EthEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextE750EthPorta(cadastrarAbaComunicacaoAtoLDPO.getE750EthPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextE750EthId(cadastrarAbaComunicacaoAtoLDPO.getE750EthId());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getE750MpEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoE750Mp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextE750MpEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getE750MpEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextE750MpPorta(cadastrarAbaComunicacaoAtoLDPO.getE750MpPorta());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getReaderGateway())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoGatewayReader());
			cadastrarAbaComunicacaoMedidoresPage.typeTextReaderGateway(cadastrarAbaComunicacaoAtoLDPO.getReaderGateway());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getBdAutomacaoIdentificadorRemoto())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoIntegBDAutomacao());
			cadastrarAbaComunicacaoMedidoresPage.typeTextBdAutomacaoIdentificadorRemoto(cadastrarAbaComunicacaoAtoLDPO.getBdAutomacaoIdentificadorRemoto());

		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getIonDialUpTelefone())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoIonDialUp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonDialUpTelefone(cadastrarAbaComunicacaoAtoLDPO.getIonDialUpTelefone());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonDialUpId(cadastrarAbaComunicacaoAtoLDPO.getIonDialUpId());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getIonEtgTermEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoIonEtgTerm());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonEtgTermEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getIonEtgTermEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonEtgTermPorta(cadastrarAbaComunicacaoAtoLDPO.getIonEtgTermPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonEtgTermId(cadastrarAbaComunicacaoAtoLDPO.getIonEtgTermId());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxIonEtgTermColetarFormasOnda(cadastrarAbaComunicacaoAtoLDPO.getIonEtgTermColetarFormasOnda());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getIonEthEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoIonEth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonEthEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getIonEthEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonEthPorta(cadastrarAbaComunicacaoAtoLDPO.getIonEthPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonEthId(cadastrarAbaComunicacaoAtoLDPO.getIonEthId());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxIonEthColetarFormasOnda(cadastrarAbaComunicacaoAtoLDPO.getIonEthColetarFormasOnda());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getIonGprsEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoIonGprs());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonGprsEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getIonGprsEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonGprsPorta(cadastrarAbaComunicacaoAtoLDPO.getIonGprsPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonGprsId(cadastrarAbaComunicacaoAtoLDPO.getIonGprsId());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxIonGprsColetarFormasOnda(cadastrarAbaComunicacaoAtoLDPO.getIonGprsColetarFormasOnda());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getIonSerialEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoIonSer());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonSerialEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getIonSerialEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonSerialId(cadastrarAbaComunicacaoAtoLDPO.getIonSerialId());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getIonEthE3RtEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoIonEthE3Rt());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonEthE3RtEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getIonEthE3RtEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonEthE3RtPorta(cadastrarAbaComunicacaoAtoLDPO.getIonEthE3RtPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIonEthE3RtId(cadastrarAbaComunicacaoAtoLDPO.getIonEthE3RtId());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckIonEthE3RtColetarFormasOnda(cadastrarAbaComunicacaoAtoLDPO.getIonEthE3RtColetarFormasOnda());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getIon7650EthEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoIon7650Eth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIon7650EthEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getIon7650EthEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIon7650EthPorta(cadastrarAbaComunicacaoAtoLDPO.getIon7650EthPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextIon7650EthId(cadastrarAbaComunicacaoAtoLDPO.getIon7650EthId());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckIon7650EthColetarFormasOnda(cadastrarAbaComunicacaoAtoLDPO.getIon7650EthColetarFormasOnda());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getLandisE34MpEthEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoLandisE34Eth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextLandisE34MpEthEnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getLandisE34MpEthEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextLandisE34MpEthPorta(cadastrarAbaComunicacaoAtoLDPO.getLandisE34MpEthPorta());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getM2mId())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoM2m());
			cadastrarAbaComunicacaoMedidoresPage.typeTextM2mId(cadastrarAbaComunicacaoMtoZDPO.getM2mId());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getNansenKArtEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoNansenKArtEth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNansenKArtEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getNansenKArtEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNansenKArtPorta(cadastrarAbaComunicacaoMtoZDPO.getNansenKArtPorta());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getNansenSpectrunSxEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoNansenSpectrumSxEth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNansenSpectrunSxEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getNansenSpectrunSxEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNansenSpectrunSxPorta(cadastrarAbaComunicacaoMtoZDPO.getNansenSpectrunSxPorta());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getNexusDialupTelefone())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoNexusDialUp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusDialupTelefone(cadastrarAbaComunicacaoMtoZDPO.getNexusDialupTelefone());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusDialupId(cadastrarAbaComunicacaoMtoZDPO.getNexusDialupId());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusDialupUsuarioParaComunicacao(cadastrarAbaComunicacaoMtoZDPO.getNexusDialupUsuarioParaComunicacao());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusDialupSenhaParaComunicacao(cadastrarAbaComunicacaoMtoZDPO.getNexusDialupSenhaParaComunicacao());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckNexusDialupColetarFormasOnda(cadastrarAbaComunicacaoMtoZDPO.getNexusDialupColetarFormasOnda());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getNexusEthEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoNexusEth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusEthEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getNexusEthEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusEthPorta(cadastrarAbaComunicacaoMtoZDPO.getNexusEthPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusEthId(cadastrarAbaComunicacaoMtoZDPO.getNexusEthId());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxNexusEthColetarFormasOnda(cadastrarAbaComunicacaoMtoZDPO.getNexusEthColetarFormasOnda());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxNexusEthSeg(cadastrarAbaComunicacaoMtoZDPO.getNexusEthSeg());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxNexusEthTer(cadastrarAbaComunicacaoMtoZDPO.getNexusEthTer());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxNexusEthQua(cadastrarAbaComunicacaoMtoZDPO.getNexusEthQua());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxNexusEthQui(cadastrarAbaComunicacaoMtoZDPO.getNexusEthQui());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxNexusEthSex(cadastrarAbaComunicacaoMtoZDPO.getNexusEthSex());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxNexusEthSab(cadastrarAbaComunicacaoMtoZDPO.getNexusEthSab());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxNexusEthDom(cadastrarAbaComunicacaoMtoZDPO.getNexusEthDom());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusEthHoraInicioDaColeta(cadastrarAbaComunicacaoMtoZDPO.getNexusEthHoraInicioDaColeta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusEthMinutoInicioDaColeta(cadastrarAbaComunicacaoMtoZDPO.getNexusEthMinutoInicioDaColeta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusEthHoraFimDaColeta(cadastrarAbaComunicacaoMtoZDPO.getNexusEthHoraFimDaColeta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusEthMinutoFimDaColeta(cadastrarAbaComunicacaoMtoZDPO.getNexusEthMinutoFimDaColeta());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusEthUsuarioParaComunicacao(cadastrarAbaComunicacaoMtoZDPO.getNexusEthUsuarioParaComunicacao());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexusEthSenhaParaComunicacao(cadastrarAbaComunicacaoMtoZDPO.getNexusEthSenhaParaComunicacao());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getNexus1250EnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoNexus1250());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1250EnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getNexus1250EnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1250DeviceAddress(cadastrarAbaComunicacaoMtoZDPO.getNexus1250DeviceAddress());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1250Porta(cadastrarAbaComunicacaoMtoZDPO.getNexus1250Porta());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1250Timeout(cadastrarAbaComunicacaoMtoZDPO.getNexus1250Timeout());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1250Retentativas(cadastrarAbaComunicacaoMtoZDPO.getNexus1250Retentativas());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1250SenhaNivel1(cadastrarAbaComunicacaoMtoZDPO.getNexus1250SenhaNivel1());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1250SenhaNivel2(cadastrarAbaComunicacaoMtoZDPO.getNexus1250SenhaNivel2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1250UsuarioEstendido(cadastrarAbaComunicacaoMtoZDPO.getNexus1250UsuarioEstendido());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1250SenhaUsuarioEstendido(cadastrarAbaComunicacaoMtoZDPO.getNexus1250SenhaUsuarioEstendido());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getNexus1500EnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoNexus1500());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1500EnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getNexus1500EnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1500DeviceAddress(cadastrarAbaComunicacaoMtoZDPO.getNexus1500DeviceAddress());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1500Porta(cadastrarAbaComunicacaoMtoZDPO.getNexus1500Porta());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1500Timeout(cadastrarAbaComunicacaoMtoZDPO.getNexus1500Timeout());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1500Retentativas(cadastrarAbaComunicacaoMtoZDPO.getNexus1500Retentativas());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1500SenhaNivel1(cadastrarAbaComunicacaoMtoZDPO.getNexus1500SenhaNivel1());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1500SenhaNivel2(cadastrarAbaComunicacaoMtoZDPO.getNexus1500SenhaNivel2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1500UsuarioEstendido(cadastrarAbaComunicacaoMtoZDPO.getNexus1500UsuarioEstendido());
			cadastrarAbaComunicacaoMedidoresPage.typeTextNexus1500SenhaUsuarioEstendido(cadastrarAbaComunicacaoMtoZDPO.getNexus1500SenhaUsuarioEstendido());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getOnerfEthEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoOneRfEth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextOneRfEthEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getOnerfEthEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextOneRfEthPorta(cadastrarAbaComunicacaoMtoZDPO.getOnerfEthPorta());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getPqube3EthFtpEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoPQubeEthFtp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextPqube3EthFtpEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getPqube3EthFtpEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextPqube3EthFtpPorta(cadastrarAbaComunicacaoMtoZDPO.getPqube3EthFtpPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextPqube3EthFtpId(cadastrarAbaComunicacaoMtoZDPO.getPqube3EthFtpId());
			cadastrarAbaComunicacaoMedidoresPage.typeTextPqube3EthFtpPortaFt(cadastrarAbaComunicacaoMtoZDPO.getPqube3EthFtpPortaFt());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextPqube3EthFtpUsuario(cadastrarAbaComunicacaoMtoZDPO.getPqube3EthFtpUsuario());
			cadastrarAbaComunicacaoMedidoresPage.typeTextPqube3EthFtpSenha(cadastrarAbaComunicacaoMtoZDPO.getPqube3EthFtpSenha());
			cadastrarAbaComunicacaoMedidoresPage.typeTextPqube3EthFtpOffsetDeMemoria(cadastrarAbaComunicacaoMtoZDPO.getPqube3EthFtpOffsetDeMemoria());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getQ1000EtgTermEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoQ1000EtgTerm());
			cadastrarAbaComunicacaoMedidoresPage.typeTextQ1000EtgTermEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getQ1000EtgTermEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextQ1000EtgTermPorta(cadastrarAbaComunicacaoMtoZDPO.getQ1000EtgTermPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextQ1000EtgTermId(cadastrarAbaComunicacaoMtoZDPO.getQ1000EtgTermId());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextQ1000EtgTermUsuarioParaComunicacao(cadastrarAbaComunicacaoMtoZDPO.getQ1000EtgTermUsuarioParaComunicacao());
			cadastrarAbaComunicacaoMedidoresPage.typeTextQ1000EtgTermSenhaParaComunicacao(cadastrarAbaComunicacaoMtoZDPO.getQ1000EtgTermSenhaParaComunicacao());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getQ1000EthEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoQ1000Eth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextQ1000EthEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getQ1000EthEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextQ1000EthPorta(cadastrarAbaComunicacaoMtoZDPO.getQ1000EthPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextQ1000EthId(cadastrarAbaComunicacaoMtoZDPO.getQ1000EthId());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextQ1000EthTimeout(cadastrarAbaComunicacaoMtoZDPO.getQ1000EthTimeout());
			cadastrarAbaComunicacaoMedidoresPage.typeTextQ1000EthRetentativas(cadastrarAbaComunicacaoMtoZDPO.getQ1000EthRetentativas());
			cadastrarAbaComunicacaoMedidoresPage.typeTextQ1000EthSenha(cadastrarAbaComunicacaoMtoZDPO.getQ1000EthSenha());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getSadM2mUrl())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSadM2m());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSadM2mUrl(cadastrarAbaComunicacaoMtoZDPO.getSadM2mUrl());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSadM2mRemota(cadastrarAbaComunicacaoMtoZDPO.getSadM2mRemota());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSaga1000DialUp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga1000DialupEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga1000DialupId(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupId());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxSaga1000DialupSeg(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupSeg());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxSaga1000DialupTer(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupTer());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxSaga1000DialupQua(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupQua());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxSaga1000DialupQui(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupQui());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxSaga1000DialupSex(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupSex());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxSaga1000DialupSab(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupSab());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxSaga1000DialupDom(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupDom());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga1000DialupHoraInicioDaColeta(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupHoraInicioDaColeta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga1000DialupMinutoInicioDaColeta(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupMinutoInicioDaColeta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga1000DialupHoraFimDaColeta(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupHoraFimDaColeta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga1000DialupMinutoFimDaColeta(cadastrarAbaComunicacaoMtoZDPO.getSaga1000DialupMinutoFimDaColeta());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getSaga1000EtgEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSaga1000EtgTerm());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga1000EtgEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getSaga1000EtgEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga1000EtgPorta(cadastrarAbaComunicacaoMtoZDPO.getSaga1000EtgPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga1000EtgId(cadastrarAbaComunicacaoMtoZDPO.getSaga1000EtgId());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getSaga1000MpEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSaga1000Mp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga1000MpEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getSaga1000MpEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga1000MpPorta(cadastrarAbaComunicacaoMtoZDPO.getSaga1000MpPorta());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getSaga2000MpEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSaga2000Mp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga2000MpEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getSaga2000MpEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSaga2000MpPorta(cadastrarAbaComunicacaoMtoZDPO.getSaga2000MpPorta());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getSharkModbusEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSharkModBusTcpIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSharkModbusEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getSharkModbusEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSharkModbusPorta(cadastrarAbaComunicacaoMtoZDPO.getSharkModbusPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSharkModbusId(cadastrarAbaComunicacaoMtoZDPO.getSharkModbusId());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getSistemaChipNumeroDoTransponder())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSistemaChip());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSistemaChipNumeroDoTransponder(cadastrarAbaComunicacaoMtoZDPO.getSistemaChipNumeroDoTransponder());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSistemaChipUrlDoWebService(cadastrarAbaComunicacaoMtoZDPO.getSistemaChipUrlDoWebService());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSistemaChipUsuario(cadastrarAbaComunicacaoMtoZDPO.getSistemaChipUsuario());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSistemaChipSenha(cadastrarAbaComunicacaoMtoZDPO.getSistemaChipSenha());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getSl7000EtgTermEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSl7000EtgTerm());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000EtgTermEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getSl7000EtgTermEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000EtgTermDeviceAddress(cadastrarAbaComunicacaoMtoZDPO.getSl7000EtgTermDeviceAddress());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000EtgTermPorta(cadastrarAbaComunicacaoMtoZDPO.getSl7000EtgTermPorta());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000EtgTermUsuarioParaComunicacao(cadastrarAbaComunicacaoMtoZDPO.getSl7000EtgTermUsuarioParaComunicacao());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000EtgTermSenhaParaComunicacao(cadastrarAbaComunicacaoMtoZDPO.getSl7000EtgTermSenhaParaComunicacao());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getSl7000TcpSenha())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSl7000Tcp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000TcpEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getSl7000TcpEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000TcpPorta(cadastrarAbaComunicacaoMtoZDPO.getSl7000TcpPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000TcpId(cadastrarAbaComunicacaoMtoZDPO.getSl7000TcpId());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000TcpTimeout(cadastrarAbaComunicacaoMtoZDPO.getSl7000TcpTimeout());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000TcpRetentativas(cadastrarAbaComunicacaoMtoZDPO.getSl7000TcpRetentativas());
			cadastrarAbaComunicacaoMedidoresPage.typeSelectSl7000TcpTiposDeCliente(cadastrarAbaComunicacaoMtoZDPO.getSl7000TcpTiposDeCliente());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000TcpSenha(cadastrarAbaComunicacaoMtoZDPO.getSl7000TcpSenha());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getSl7000EthE3RtEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSl7000EthE3Rt());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000EthE3RtEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getSl7000EthE3RtEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000EthE3RtPorta(cadastrarAbaComunicacaoMtoZDPO.getSl7000EthE3RtPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000EthE3RtId(cadastrarAbaComunicacaoMtoZDPO.getSl7000EthE3RtId());
			cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxSl7000EthE3RtColetarFormasOnda(cadastrarAbaComunicacaoMtoZDPO.getSl7000EthE3RtColetarFormasOnda());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000EthE3RtUsuarioParaComunicacao(cadastrarAbaComunicacaoMtoZDPO.getSl7000EthE3RtUsuarioParaComunicacao());
			cadastrarAbaComunicacaoMedidoresPage.typeTextSl7000EthE3RtSenhaParaComunicacao(cadastrarAbaComunicacaoMtoZDPO.getSl7000EthE3RtSenhaParaComunicacao());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getSmartgreenDeviceId())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSmartGreen());
			cadastrarAbaComunicacaoMedidoresPage.typeTextsSmartgreenDeviceId(cadastrarAbaComunicacaoMtoZDPO.getSmartgreenDeviceId());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getStmStrRemoto())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoStm());
			cadastrarAbaComunicacaoMedidoresPage.typeTextStmStrRemoto(cadastrarAbaComunicacaoMtoZDPO.getStmStrRemoto());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getTekpeaDeviceId())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoTekpea());
			cadastrarAbaComunicacaoMedidoresPage.typeTextTekpeaDeviceId(cadastrarAbaComunicacaoMtoZDPO.getTekpeaDeviceId());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getTnsNumeroDoTransponder())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoTns());
			cadastrarAbaComunicacaoMedidoresPage.typeTextTnsNumeroDoTransponder(cadastrarAbaComunicacaoMtoZDPO.getTnsNumeroDoTransponder());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getWebserviceIdentificadorRemoto())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoWebservice());
			cadastrarAbaComunicacaoMedidoresPage.typeTextWebserviceIdentificadorRemoto(cadastrarAbaComunicacaoMtoZDPO.getWebserviceIdentificadorRemoto());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getWebserviceIdentificadorRemoto())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoWebserviceHidrologia());
			cadastrarAbaComunicacaoMedidoresPage.typeTextWsHidrologiaCodigoSensorChuva(cadastrarAbaComunicacaoMtoZDPO.getWsHidrologiaCodigoSensorChuva());
			cadastrarAbaComunicacaoMedidoresPage.typeTextWsHidrologiaCodigoSensorNivel(cadastrarAbaComunicacaoMtoZDPO.getWsHidrologiaCodigoSensorNivel());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getWegModbusEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoWegModBusEth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextWegModbusEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getWegModbusEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextWegModbusPorta(cadastrarAbaComunicacaoMtoZDPO.getWegModbusPorta());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getZivEthEnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoZivEth());
			cadastrarAbaComunicacaoMedidoresPage.typeTextZivEthEnderecoOuIp(cadastrarAbaComunicacaoMtoZDPO.getZivEthEnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextZivEthPorta(cadastrarAbaComunicacaoMtoZDPO.getZivEthPorta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextZivEthId(cadastrarAbaComunicacaoMtoZDPO.getZivEthId());
			cadastrarAbaComunicacaoMedidoresPage.keyPageDown();
			cadastrarAbaComunicacaoMedidoresPage.typeTextZivEthUsuarioParaComunicacao(cadastrarAbaComunicacaoMtoZDPO.getZivEthUsuarioParaComunicacao());
			cadastrarAbaComunicacaoMedidoresPage.typeTextZivEthSenhaParaComunicacao(cadastrarAbaComunicacaoMtoZDPO.getZivEthSenhaParaComunicacao());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoAmyCylec())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoAmyCylec());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoGarnetElster())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoGarnetElster());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoHidroSkywave())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoHidroSkywave());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoIntegBDAutomacao())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoIntegBDAutomacao());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoITechUtw())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoITechUtw());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSatLink2Satelite())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoSatLink2Satelite());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoStevensDotLoggerSatelite())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoStevensDotLoggerSatelite());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoWits())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoMtoZDPO.getComunicacaoWits());
		}
		if (StringUtil.isNotBlankOrNotNull(cadastrarAbaComunicacaoAtoLDPO.getWay2EnderecoOuIp())) {
			cadastrarAbaComunicacaoMedidoresPage.typeSelectComunicacao(cadastrarAbaComunicacaoAtoLDPO.getComunicacaoBancVirtualWay2());
			cadastrarAbaComunicacaoMedidoresPage.typeTextWay2EnderecoOuIp(cadastrarAbaComunicacaoAtoLDPO.getWay2EnderecoOuIp());
			cadastrarAbaComunicacaoMedidoresPage.typeTextWay2Porta(cadastrarAbaComunicacaoAtoLDPO.getWay2Porta());
			cadastrarAbaComunicacaoMedidoresPage.typeTextWay2Id(cadastrarAbaComunicacaoAtoLDPO.getWay2Id());
		}
		cadastrarAbaComunicacaoMedidoresPage.typeCheckBoxHabilitar(cadastrarAbaComunicacaoDPO.getCheckHabilitar());
		cadastrarAbaComunicacaoMedidoresPage.typeTextProtocolo(cadastrarAbaComunicacaoDPO.getTextProtocolo());
		cadastrarAbaComunicacaoMedidoresPage.clickSalvar();
		FindElementUtil.acceptAlert();
	}

	@Override
	public void runValidations() {
		cadastrarAbaComunicacaoMedidoresPage.validateMessageDefault(cadastrarAbaComunicacaoDPO.getOperationMessage());
	}

}
