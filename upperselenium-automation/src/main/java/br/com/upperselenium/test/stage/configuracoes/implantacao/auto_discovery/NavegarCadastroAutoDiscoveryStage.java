package br.com.upperselenium.test.stage.configuracoes.implantacao.auto_discovery;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.auto_discovery.dpo.NavegarCadastroAutoDiscoveryDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.auto_discovery.page.CadastroAutoDiscoveryPage;

public class NavegarCadastroAutoDiscoveryStage extends BaseStage {
	private NavegarCadastroAutoDiscoveryDPO navegarCadastroAutoDiscoveryDPO;
	private CadastroAutoDiscoveryPage cadastroAutoDiscoveryPage;
	
	public NavegarCadastroAutoDiscoveryStage(String dp) {
		navegarCadastroAutoDiscoveryDPO = loadDataProviderFile(NavegarCadastroAutoDiscoveryDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAutoDiscoveryPage = initElementsFromPage(CadastroAutoDiscoveryPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarCadastroAutoDiscoveryDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		cadastroAutoDiscoveryPage.getBreadcrumbsText(navegarCadastroAutoDiscoveryDPO.getBreadcrumbs());
		cadastroAutoDiscoveryPage.clickUpToBreadcrumbs(navegarCadastroAutoDiscoveryDPO.getUpToBreadcrumb());
		cadastroAutoDiscoveryPage.getWelcomeTitle(navegarCadastroAutoDiscoveryDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
