package br.com.upperselenium.test.stage.medicao.comandos_do_gateway;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.dpo.NavegarComandosDoGatewayAgendarDPO;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.page.ComandosDoGatewayAgendarPage;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.page.ComandosDoGatewayPage;

public class NavegarComandosDoGatewayAgendarStage extends BaseStage {
	private NavegarComandosDoGatewayAgendarDPO navegarComandosDoGatewayAgendarDPO;
	private ComandosDoGatewayPage comandosDoGatewayPage;	
	private ComandosDoGatewayAgendarPage comandosDoGatewayAgendarPage;	
	
	public NavegarComandosDoGatewayAgendarStage(String dp) {
		navegarComandosDoGatewayAgendarDPO = loadDataProviderFile(NavegarComandosDoGatewayAgendarDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		comandosDoGatewayPage = initElementsFromPage(ComandosDoGatewayPage.class);
		comandosDoGatewayAgendarPage = initElementsFromPage(ComandosDoGatewayAgendarPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarComandosDoGatewayAgendarDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		comandosDoGatewayPage.clickLinkAgendar();
		comandosDoGatewayAgendarPage.getLabelTipo(navegarComandosDoGatewayAgendarDPO.getLabelTipo());
		comandosDoGatewayAgendarPage.getLabelInstante(navegarComandosDoGatewayAgendarDPO.getLabelInstante());
		comandosDoGatewayAgendarPage.getLabelExpiracao(navegarComandosDoGatewayAgendarDPO.getLabelExpiracao());
		comandosDoGatewayAgendarPage.getLabelDigiteUmOuMaisGateways(navegarComandosDoGatewayAgendarDPO.getLabelDigiteUmOuMaisGateways());		
		comandosDoGatewayAgendarPage.getLabelRepeticao(navegarComandosDoGatewayAgendarDPO.getLabelRepeticao());		
		comandosDoGatewayAgendarPage.getBreadcrumbsText(navegarComandosDoGatewayAgendarDPO.getBreadcrumbs());
		comandosDoGatewayAgendarPage.clickUpToBreadcrumbs(navegarComandosDoGatewayAgendarDPO.getUpToBreadcrumb());
		comandosDoGatewayAgendarPage.getWelcomeTitle(navegarComandosDoGatewayAgendarDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
