package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarEquacoesPontosVirtuaisDPO extends BaseHelperDPO {

	private List<GroupNovaEquacaoDPO> groupNovaEquacao;
	private List<GroupVisualizarEquacoesDPO> groupVisualizarEquacoes;

	public List<GroupNovaEquacaoDPO> getGroupNovaEquacao() {
		return groupNovaEquacao;
	}

	public void setGroupNovaEquacao(List<GroupNovaEquacaoDPO> groupNovaEquacao) {
		this.groupNovaEquacao = groupNovaEquacao;
	}

	public List<GroupVisualizarEquacoesDPO> getGroupVisualizarEquacoes() {
		return groupVisualizarEquacoes;
	}

	public void setGroupVisualizarEquacoes(List<GroupVisualizarEquacoesDPO> groupVisualizarEquacoes) {
		this.groupVisualizarEquacoes = groupVisualizarEquacoes;
	}

}