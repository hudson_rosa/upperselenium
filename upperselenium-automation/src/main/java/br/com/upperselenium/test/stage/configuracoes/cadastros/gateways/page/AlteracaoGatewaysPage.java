package br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoGatewaysPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='salvar']";   
	private static final String TEXT_ID = "//*[@id='IdDoFabricante']";
	private static final String SELECT_MODELO = "//*[@id='Modelo']";
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String TEXT_DESCRICAO = "//*[@id='Descricao']";
	private static final String TEXT_ENTRADA_DIGITAL_1 = "//*[@name='EntradaDigital1']";
	private static final String TEXT_ENTRADA_DIGITAL_2 = "//*[@name='EntradaDigital2']";
	private static final String TEXT_ENTRADA_DIGITAL_3 = "//*[@name='EntradaDigital3']";
	private static final String TEXT_ENTRADA_DIGITAL_4 = "//*[@name='EntradaDigital4']";
	private static final String SELECT_TIPO_DE_COMUNICACAO = "//*[@id='TipoDeComunicacao']";	
	private static final String TEXT_ENDERECO_IP_PORTA = "//*[@id='EnderecoIpPorta']";
	private static final String TEXT_ENDERECO_SECUNDARIO_IP_PORTA = "//*[@id='EnderecoSecundarioIpPorta']"; 
	private static final String TEXT_ID_DE_COMUNICACAO = "//*[@name='IdDeComunicacao']";
	private static final String TEXT_USUARIO = "//*[@name='Usuario']";
	private static final String TEXT_SENHA = "//*[@name='Senha']";
	private static final String CHECK_COMUNICACAO_CRIPTOGRAFADA = "//*[@name='PossuiComunicacaoCriptografada']";
	
	public void typeTextID(String value){
		typeText(TEXT_ID, value);
	}	
	
	public void typeSelectModelo(String value){		
		typeSelectComboOption(SELECT_MODELO, value);
	}
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	
	
	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}	
	
	public void typeTextEntradaDigital1(String value){
		typeText(TEXT_ENTRADA_DIGITAL_1, value);
	}	
	
	public void typeTextEntradaDigital2(String value){
		typeText(TEXT_ENTRADA_DIGITAL_2, value);
	}	
	
	public void typeTextEntradaDigital3(String value){
		typeText(TEXT_ENTRADA_DIGITAL_3, value);
	}	
	
	public void typeTextEntradaDigital4(String value){
		typeText(TEXT_ENTRADA_DIGITAL_4, value);
	}	
	
	public void typeSelectTipoDeComunicacao(String value){		
		typeSelectComboOption(SELECT_TIPO_DE_COMUNICACAO, value);
	}
		
	public void typeTextEnderecoIPPorta(String value){
		typeText(TEXT_ENDERECO_IP_PORTA, value);
	}
	
	public void typeTextEnderecoSecundarioIPPorta(String value){
		typeText(TEXT_ENDERECO_SECUNDARIO_IP_PORTA, value);
	}
	
	public void typeTextIdDeComunicacao(String value){
		typeText(TEXT_ID_DE_COMUNICACAO, value);
	}
	
	public void typeTextUsuario(String value){
		typeText(TEXT_USUARIO, value);
	}
	
	public void typeTextSenha(String value){
		typeText(TEXT_SENHA, value);
	}
	
	public void typeCheckComunicacaoCriptografada(String value){
		typeCheckOption(CHECK_COMUNICACAO_CRIPTOGRAFADA, value);
	}
		
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}

	public void keyPageDown(){
		useKey(TEXT_ENDERECO_SECUNDARIO_IP_PORTA, Keys.PAGE_DOWN);
	}	

}