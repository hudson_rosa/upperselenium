package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.dpo.NavegarTiposDePontoCCEEDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.page.TiposDePontoCCEEPage;

public class NavegarTiposDePontoCCEEStage extends BaseStage {
	private NavegarTiposDePontoCCEEDPO navegarTiposDePontoCCEEDPO;
	private TiposDePontoCCEEPage tiposDePontoCCEEPage;	
	
	public NavegarTiposDePontoCCEEStage(String dp) {
		navegarTiposDePontoCCEEDPO = loadDataProviderFile(NavegarTiposDePontoCCEEDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDePontoCCEEPage = initElementsFromPage(TiposDePontoCCEEPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarTiposDePontoCCEEDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		tiposDePontoCCEEPage.getLabelHeaderNome(navegarTiposDePontoCCEEDPO.getLabelNome());
		tiposDePontoCCEEPage.getLabelHeaderDescricao(navegarTiposDePontoCCEEDPO.getLabelDescricao());
		tiposDePontoCCEEPage.getBreadcrumbsText(navegarTiposDePontoCCEEDPO.getBreadcrumbs());
		tiposDePontoCCEEPage.clickUpToBreadcrumbs(navegarTiposDePontoCCEEDPO.getUpToBreadcrumb());
		tiposDePontoCCEEPage.getWelcomeTitle(navegarTiposDePontoCCEEDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
