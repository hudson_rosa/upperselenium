package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.dpo.NavegarTiposDePontoDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.page.TiposDePontoPage;

public class NavegarTiposDePontoStage extends BaseStage {
	private NavegarTiposDePontoDPO navegarTiposDePontoDPO;
	private TiposDePontoPage tiposDePontoPage;
	
	public NavegarTiposDePontoStage(String dp) {
		navegarTiposDePontoDPO = loadDataProviderFile(NavegarTiposDePontoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDePontoPage = initElementsFromPage(TiposDePontoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarTiposDePontoDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		tiposDePontoPage.getLabelHeaderNome(navegarTiposDePontoDPO.getLabelNome());
		tiposDePontoPage.getLabelHeaderDescricao(navegarTiposDePontoDPO.getLabelDescricao());
		tiposDePontoPage.getLabelHeaderGrupoDeMedicao(navegarTiposDePontoDPO.getLabelGrupoDeMedicao());
		tiposDePontoPage.getLabelHeaderAbas(navegarTiposDePontoDPO.getLabelAbas());
		tiposDePontoPage.getBreadcrumbsText(navegarTiposDePontoDPO.getBreadcrumbs());
		tiposDePontoPage.clickUpToBreadcrumbs(navegarTiposDePontoDPO.getUpToBreadcrumb());
		tiposDePontoPage.getWelcomeTitle(navegarTiposDePontoDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
