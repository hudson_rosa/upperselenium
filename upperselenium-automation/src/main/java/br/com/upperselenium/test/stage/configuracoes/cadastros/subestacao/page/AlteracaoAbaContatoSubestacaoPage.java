package br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoAbaContatoSubestacaoPage extends BaseHelperPage{
	
	private static final String LINK_ABA_CONTATO = ".//div[2]/div/div[2]/ul[2]/li[3]/a";  
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 	
	private static final String TEXT_TEL_SE_1 = "//*[@id='Subestacao_Telefone1']"; 
	private static final String TEXT_TEL_SE_2 = "//*[@id='Subestacao_Telefone2']"; 
	private static final String TEXT_CONTATO_1 = "//*[@id='Subestacao_Contato1']"; 
	private static final String TEXT_EMAIL_1 = "//*[@id='Subestacao_EmailDoContato1']"; 
	private static final String TEXT_TEL_CONTATO_1 = "//*[@id='Subestacao_TelefoneDoContato1']"; 
	private static final String TEXT_CONTATO_2 = "//*[@id='Subestacao_Contato2']"; 
	private static final String TEXT_EMAIL_2 = "//*[@id='Subestacao_EmailDoContato2']"; 
	private static final String TEXT_TEL_CONTATO_2 = "//*[@id='Subestacao_TelefoneDoContato2']"; 
	private static final String TEXT_TEL_CONTATO_3 = "//*[@id='Subestacao_TelefoneDoContato3']"; 
	
	public void typeTextTelefoneSE1(String value) {
		typeText(TEXT_TEL_SE_1, value);
	}

	public void typeTextTelefoneSE2(String value) {
		typeText(TEXT_TEL_SE_2, value);
	}

	public void typeTextContato1(String value) {
		typeText(TEXT_CONTATO_1, value);
	}

	public void typeTextEmail1(String value) {
		typeText(TEXT_EMAIL_1, value);
	}

	public void typeTextTelContato1(String value) {
		typeText(TEXT_TEL_CONTATO_1, value);
	}

	public void typeTextContato2(String value) {
		typeText(TEXT_CONTATO_2, value);
	}

	public void typeTextEmail2(String value) {
		typeText(TEXT_EMAIL_2, value);
	}

	public void typeTextTelContato2(String value) {
		typeText(TEXT_TEL_CONTATO_2, value);
	}

	public void typeTextTelContato3(String value) {
		typeText(TEXT_TEL_CONTATO_3, value);
	}

	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
				
	public void clickAbaContato(){
		clickOnElement(LINK_ABA_CONTATO);
	}
	
}