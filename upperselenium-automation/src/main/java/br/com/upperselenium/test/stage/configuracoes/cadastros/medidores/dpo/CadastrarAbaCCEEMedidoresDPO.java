package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaCCEEMedidoresDPO extends BaseHelperDPO {

	private String codigoDoSCDE;
	private String quadranteDEL;
	private String quadranteREC;
	private String algoritmoDeCompDePerdas;
	private String geracaoDeXML;

	public String getCodigoDoSCDE() {
		return codigoDoSCDE;
	}

	public void setCodigoDoSCDE(String codigoDoSCDE) {
		this.codigoDoSCDE = codigoDoSCDE;
	}

	public String getQuadranteDEL() {
		return quadranteDEL;
	}

	public void setQuadranteDEL(String quadranteDEL) {
		this.quadranteDEL = quadranteDEL;
	}

	public String getQuadranteREC() {
		return quadranteREC;
	}

	public void setQuadranteREC(String quadranteREC) {
		this.quadranteREC = quadranteREC;
	}

	public String getAlgoritmoDeCompDePerdas() {
		return algoritmoDeCompDePerdas;
	}

	public void setAlgoritmoDeCompDePerdas(String algoritmoDeCompDePerdas) {
		this.algoritmoDeCompDePerdas = algoritmoDeCompDePerdas;
	}

	public String getGeracaoDeXML() {
		return geracaoDeXML;
	}

	public void setGeracaoDeXML(String geracaoDeXML) {
		this.geracaoDeXML = geracaoDeXML;
	}

}