package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.GoToPerfisStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.NavegarAlteracaoPerfisStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.NavegarCadastroPerfisStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.NavegarPerfisStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.NavegarRegistroLinkPerfisStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0002PerfisNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0002-Perfis", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Perfis validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0002PerfisNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToPerfisStage());
		addStage(new NavegarPerfisStage(getDP("NavegarPerfisDP.json")));
		addStage(new GoToPerfisStage());
		addStage(new NavegarCadastroPerfisStage(getDP("NavegarCadastroPerfisDP.json")));
		addStage(new NavegarRegistroLinkPerfisStage(getDP("NavegarRegistroLinkPerfisDP.json")));
		addStage(new NavegarAlteracaoPerfisStage(getDP("NavegarAlteracaoPerfisDP.json")));
	}	
}
