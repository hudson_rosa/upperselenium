package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.implantacao.configuracao_do_sistema.GoToConfiguracaoDoSistemaStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.configuracao_do_sistema.NavegarAlteracaoConfDoSistemaStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0006ConfiguracaoDoSistemaNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0006-ConfiguracaoDoSistema", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Configuração do Sistema validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0006ConfiguracaoDoSistemaNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToConfiguracaoDoSistemaStage());
		addStage(new NavegarAlteracaoConfDoSistemaStage(getDP("NavegarAlteracaoConfDoSistemaDP.json")));
	}	
}
