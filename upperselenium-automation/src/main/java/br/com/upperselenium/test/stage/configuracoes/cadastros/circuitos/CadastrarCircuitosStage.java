package br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos.dpo.CadastrarCircuitosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos.page.CadastroCircuitosPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos.page.CircuitosPage;

public class CadastrarCircuitosStage extends BaseStage {
	private CadastrarCircuitosDPO cadastrarCircuitosDPO;
	private CircuitosPage circuitosPage;
	private CadastroCircuitosPage cadastroCircuitosPage;
	
	public CadastrarCircuitosStage(String dp) {
		cadastrarCircuitosDPO = loadDataProviderFile(CadastrarCircuitosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		circuitosPage = initElementsFromPage(CircuitosPage.class);
		cadastroCircuitosPage = initElementsFromPage(CadastroCircuitosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarCircuitosDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		circuitosPage.clickNovo();
		cadastroCircuitosPage.typeTextNome(cadastrarCircuitosDPO.getNome()+getRandomStringSpaced());
		cadastroCircuitosPage.typeTextAlias(cadastrarCircuitosDPO.getAlias()+getRandomStringSpaced());
		cadastroCircuitosPage.typeTextMedidor(cadastrarCircuitosDPO.getMedidor());
		cadastroCircuitosPage.typeRadioEntradaDigital(cadastrarCircuitosDPO.getEntradaDigitalRDO());
		cadastroCircuitosPage.typeRadioArquivo(cadastrarCircuitosDPO.getArquivoRDO());
		cadastroCircuitosPage.typeCheckBoxCreg(cadastrarCircuitosDPO.getCreg());
		cadastroCircuitosPage.typeSelectEntradaDigital(cadastrarCircuitosDPO.getEntradaDigitalCBX());
		cadastroCircuitosPage.isClickableButtonSalvar();
		cadastroCircuitosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroCircuitosPage.validateMessageDefault(cadastrarCircuitosDPO.getOperationMessage());
	}

}
