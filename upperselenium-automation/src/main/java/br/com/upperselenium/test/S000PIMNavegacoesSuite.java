package br.com.upperselenium.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseSuite;
import br.com.upperselenium.base.annotation.SuiteParams;
import br.com.upperselenium.test.flow.valida_navegacao.T0001PastasNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0002PerfisNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0003RelatorioDeAuditoriaNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0004UsuariosNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0005AutoDiscoveryNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0006ConfiguracaoDoSistemaNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0007ConfiguracoesDeTarefasNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0008TiposDeComunicacaoNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0009TiposDeAlarmesNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0010TiposDeEquipamentoNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0011TiposDeMedidorNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0012TiposDePontoNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0013TiposDePontoCCEENavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0014AcompDiarioDaColetaNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0015AcompDiarioDeUsinasNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0016ComandosDoGatewayNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0017ComandosDoMedidorNavegacoesFlow;
import br.com.upperselenium.test.flow.valida_navegacao.T0018ConsolidacaoNavegacoesFlow;


@RunWith(Suite.class)

@SuiteClasses({
	T0001PastasNavegacoesFlow.class,	
	T0002PerfisNavegacoesFlow.class,
	T0003RelatorioDeAuditoriaNavegacoesFlow.class,
	T0004UsuariosNavegacoesFlow.class,
	T0005AutoDiscoveryNavegacoesFlow.class,
	T0006ConfiguracaoDoSistemaNavegacoesFlow.class,
	T0007ConfiguracoesDeTarefasNavegacoesFlow.class,
	T0008TiposDeComunicacaoNavegacoesFlow.class,
	T0009TiposDeAlarmesNavegacoesFlow.class,
	T0010TiposDeEquipamentoNavegacoesFlow.class,
	T0011TiposDeMedidorNavegacoesFlow.class,
	T0012TiposDePontoNavegacoesFlow.class,
	T0013TiposDePontoCCEENavegacoesFlow.class,
	T0014AcompDiarioDaColetaNavegacoesFlow.class,
	T0015AcompDiarioDeUsinasNavegacoesFlow.class,
	T0016ComandosDoGatewayNavegacoesFlow.class,
	T0017ComandosDoMedidorNavegacoesFlow.class,
	T0018ConsolidacaoNavegacoesFlow.class
	})

@SuiteParams(description="PIM - Navegações")
public class S000PIMNavegacoesSuite extends BaseSuite {}
