package br.com.upperselenium.test.stage.configuracoes.implantacao.auto_discovery.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarCadastroAutoDiscoveryDPO extends BaseHelperDPO {

	private String modelo;

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	@Override
	public String toString() {
		return "NavegarCadastroAutoDiscoveryDPO [modelo=" + modelo + "]";
	}

}