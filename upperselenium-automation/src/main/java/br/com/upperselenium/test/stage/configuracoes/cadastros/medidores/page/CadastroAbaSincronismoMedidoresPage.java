package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaSincronismoMedidoresPage extends BaseHelperPage{
	
	private static final String LINK_ABA_SINCRONISMO = ".//div[2]/div/div[2]/ul[2]/li[6]/a";  
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 	
	private static final String CHECK_ACERTO_DE_RELOGIO = "//*[@id='AcertoDeRelogio']"; 
	private static final String SELECT_SINCRONISMO_HORA = "//*[@id='HoraDeSincronismo']";
	private static final String SELECT_SINCRONISMO_MINUTO = "//*[@id='MinutoDeSincronismo']";
	
	public void typeCheckAcertoDeRelogio(String value){		
		typeCheckOption(CHECK_ACERTO_DE_RELOGIO, value);
	}
	
	public void typeSelectSincronismoHora(String value){		
		typeSelectComboOption(SELECT_SINCRONISMO_HORA, value);
	}

	public void typeSelectSincronismoMinuto(String value){		
		typeSelectComboOption(SELECT_SINCRONISMO_MINUTO, value);
	}

	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
				
	public void clickAbaSincronismo(){
		clickOnElement(LINK_ABA_SINCRONISMO);
	}
	
}