package br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao.dpo.AlterarPadroesDeCalibracaoDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao.page.AlteracaoPadroesDeCalibracaoPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao.page.PadroesDeCalibracaoPage;

public class AlterarPadroesDeCalibracaoStage extends BaseStage {
	private AlterarPadroesDeCalibracaoDPO alterarPadroesDeCalibracaoDPO;
	private PadroesDeCalibracaoPage padroesDeCalibracaoPage;
	private AlteracaoPadroesDeCalibracaoPage alteracaoPadroesDeCalibracaoPage;
	
	public AlterarPadroesDeCalibracaoStage(String dp) {
		alterarPadroesDeCalibracaoDPO = loadDataProviderFile(AlterarPadroesDeCalibracaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		padroesDeCalibracaoPage = initElementsFromPage(PadroesDeCalibracaoPage.class);
		alteracaoPadroesDeCalibracaoPage = initElementsFromPage(AlteracaoPadroesDeCalibracaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarPadroesDeCalibracaoDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoPadroesDeCalibracaoPage.typeTextIdentificacao(alterarPadroesDeCalibracaoDPO.getIdentificacao()+getRandomStringSpaced());		
		alteracaoPadroesDeCalibracaoPage.typeTextModelo(alterarPadroesDeCalibracaoDPO.getModelo());		
		alteracaoPadroesDeCalibracaoPage.typeTextNumeroDeSerie(alterarPadroesDeCalibracaoDPO.getNumeroDeSerie()+getRandomStringNonSpaced());		
		alteracaoPadroesDeCalibracaoPage.typeTextNumeroDoCertificadoDeCalibracao(alterarPadroesDeCalibracaoDPO.getNumeroDoCertificadoDeCalibracao()+getRandomStringNonSpaced());		
		alteracaoPadroesDeCalibracaoPage.typeTextClasse(alterarPadroesDeCalibracaoDPO.getClasse());		
		alteracaoPadroesDeCalibracaoPage.typeTextDataDeValidade(alterarPadroesDeCalibracaoDPO.getDataDeValidade());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaNominalEnergiaAtivaFornecida(alterarPadroesDeCalibracaoDPO.getCargaNominalEnergiaAtivaFornecida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaNominalEnergiaAtivaRecebida(alterarPadroesDeCalibracaoDPO.getCargaNominalEnergiaAtivaRecebida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaNominalEnergiaReativaFornecida(alterarPadroesDeCalibracaoDPO.getCargaNominalEnergiaReativaFornecida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaNominalEnergiaReativaRecebida(alterarPadroesDeCalibracaoDPO.getCargaNominalEnergiaReativaRecebida());
		alteracaoPadroesDeCalibracaoPage.typeTextCargaIndutivaEnergiaAtivaFornecida(alterarPadroesDeCalibracaoDPO.getCargaIndutivaEnergiaAtivaFornecida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaIndutivaEnergiaAtivaRecebida(alterarPadroesDeCalibracaoDPO.getCargaIndutivaEnergiaAtivaRecebida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaIndutivaEnergiaReativaFornecida(alterarPadroesDeCalibracaoDPO.getCargaIndutivaEnergiaReativaFornecida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaIndutivaEnergiaReativaRecebida(alterarPadroesDeCalibracaoDPO.getCargaIndutivaEnergiaReativaRecebida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaCapacitivaEnergiaAtivaFornecida(alterarPadroesDeCalibracaoDPO.getCargaCapacitivaEnergiaAtivaFornecida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaCapacitivaEnergiaAtivaRecebida(alterarPadroesDeCalibracaoDPO.getCargaCapacitivaEnergiaAtivaRecebida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaCapacitivaEnergiaReativaFornecida(alterarPadroesDeCalibracaoDPO.getCargaCapacitivaEnergiaReativaFornecida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaCapacitivaEnergiaReativaRecebida(alterarPadroesDeCalibracaoDPO.getCargaCapacitivaEnergiaReativaRecebida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaPequenaEnergiaAtivaFornecida(alterarPadroesDeCalibracaoDPO.getCargaPequenaEnergiaAtivaFornecida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaPequenaEnergiaAtivaRecebida(alterarPadroesDeCalibracaoDPO.getCargaPequenaEnergiaAtivaRecebida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaPequenaEnergiaReativaFornecida(alterarPadroesDeCalibracaoDPO.getCargaPequenaEnergiaReativaFornecida());		
		alteracaoPadroesDeCalibracaoPage.typeTextCargaPequenaEnergiaReativaRecebida(alterarPadroesDeCalibracaoDPO.getCargaPequenaEnergiaReativaRecebida());		
		alteracaoPadroesDeCalibracaoPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoPadroesDeCalibracaoPage.validateMessageDefault(alterarPadroesDeCalibracaoDPO.getOperationMessage());
	}

}
