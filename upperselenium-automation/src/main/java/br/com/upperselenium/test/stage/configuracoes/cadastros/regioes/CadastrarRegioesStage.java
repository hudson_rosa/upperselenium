package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.dpo.CadastrarRegioesDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.page.CadastroRegioesPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.page.RegioesPage;

public class CadastrarRegioesStage extends BaseStage {
	private CadastrarRegioesDPO cadastrarRegioesDPO;
	private RegioesPage regioesPage;
	private CadastroRegioesPage cadastroRegioesPage;
	
	public CadastrarRegioesStage(String dp) {
		cadastrarRegioesDPO = loadDataProviderFile(CadastrarRegioesDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		regioesPage = initElementsFromPage(RegioesPage.class);
		cadastroRegioesPage = initElementsFromPage(CadastroRegioesPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarRegioesDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		regioesPage.clickNova();
		cadastroRegioesPage.typeTextNome(cadastrarRegioesDPO.getNome()+getRandomStringSpaced());		
		cadastroRegioesPage.typeTextDescricao(cadastrarRegioesDPO.getDescricao()+getRandomStringSpaced());		
		cadastroRegioesPage.typeSelectAreaGeografica(cadastrarRegioesDPO.getAreaGeografica());		
		cadastroRegioesPage.typeTextPontoDeEnergiaRequerida(cadastrarRegioesDPO.getPontoDeEnergiaRequerida());		
		cadastroRegioesPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroRegioesPage.validateMessageDefault(cadastrarRegioesDPO.getOperationMessage());
	}

}
