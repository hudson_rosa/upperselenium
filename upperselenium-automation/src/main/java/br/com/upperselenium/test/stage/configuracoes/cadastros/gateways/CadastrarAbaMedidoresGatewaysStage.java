package br.com.upperselenium.test.stage.configuracoes.cadastros.gateways;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.dpo.CadastrarAbaMedidoresGatewaysDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.page.CadastroAbaMedidoresGatewaysPage;

public class CadastrarAbaMedidoresGatewaysStage extends BaseStage {
	private CadastrarAbaMedidoresGatewaysDPO cadastrarAbaMedidoresGatewaysDPO;
	private CadastroAbaMedidoresGatewaysPage cadastroAbaMedidoresGatewaysPage;
	
	public CadastrarAbaMedidoresGatewaysStage(String dp) {
		cadastrarAbaMedidoresGatewaysDPO = loadDataProviderFile(CadastrarAbaMedidoresGatewaysDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaMedidoresGatewaysPage = initElementsFromPage(CadastroAbaMedidoresGatewaysPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaMedidoresGatewaysDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaMedidoresGatewaysPage.clickAbaMedidores();
		cadastroAbaMedidoresGatewaysPage.clickAdicionar();
		cadastroAbaMedidoresGatewaysPage.typeModalTextSlot(cadastrarAbaMedidoresGatewaysDPO.getSlot());			
		cadastroAbaMedidoresGatewaysPage.typeModalAutocompleteTextMedidor(cadastrarAbaMedidoresGatewaysDPO.getMedidor());
		cadastroAbaMedidoresGatewaysPage.keyTab();
		cadastroAbaMedidoresGatewaysPage.clickModalSalvar();	
	}

	@Override
	public void runValidations() {}

}
