package br.com.upperselenium.test.stage.wrapper.navigation.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class MockElementValidationPage extends BaseHelperPage{
	
	private static final String XPATH_TO_VALIDATE_1 = ".//div[2]/div/div[2]/fieldset[%x%]/p";
	private static final String XPATH_TO_VALIDATE_2 = ".//div[2]/div/div[2]/fieldset[%x%]/p";
	private static final String XPATH_TO_VALIDATE_3 = "";	
		
	// ELEMENT 1
	public boolean isClickableButtonToValidate1() {
		return isDisplayedElement(XPATH_TO_VALIDATE_1);
	}
	
	public void getLabelToValidate1(String value){
		getLabel(XPATH_TO_VALIDATE_1, value);
	}
	
	public void getGridLabelToValidate1(String value, int index){
		getGridLabel(XPATH_TO_VALIDATE_1, value, index);
	}

	public void clickToValidate1(){
		clickOnElement(XPATH_TO_VALIDATE_1);
	}

	public void typeTextToValidate1(String value){
		typeText(XPATH_TO_VALIDATE_1, value);
	}
	
	public void typeCheckToValidate1(String value){
		typeCheckOption(XPATH_TO_VALIDATE_1, value);
	}
	
	public void typeSelectToValidate1(String value){
		typeSelectComboOption(XPATH_TO_VALIDATE_1, value);
	}
	
	// ELEMENT 2
	public boolean isClickableButtonToValidate2() {
		return isDisplayedElement(XPATH_TO_VALIDATE_2);
	}
	
	public void getLabelToValidate2(String value){
		getLabel(XPATH_TO_VALIDATE_2, value);
	}
	
	public void getGridLabelToValidate2(String value, int index){
		getGridLabel(XPATH_TO_VALIDATE_2, value, index);
	}	
	
	public void clickToValidate2(){
		clickOnElement(XPATH_TO_VALIDATE_2);
	}
	
	public void typeTextToValidate2(String value){
		typeText(XPATH_TO_VALIDATE_2, value);
	}
	
	public void typeCheckToValidate2(String value){
		typeCheckOption(XPATH_TO_VALIDATE_2, value);
	}
	
	public void typeSelectToValidate2(String value){
		typeSelectComboOption(XPATH_TO_VALIDATE_2, value);
	}
	
	// ELEMENT 3
	public boolean isClickableButtonToValidate3() {
		return isDisplayedElement(XPATH_TO_VALIDATE_3);
	}
	
	public void getGridLabelToValidate3(String value, int index){
		getGridLabel(XPATH_TO_VALIDATE_3, value, index);
	}	
	
	public void getLabelToValidate3(String value){
		getLabel(XPATH_TO_VALIDATE_3, value);
	}
	
	public void clickToValidate3(){
		clickOnElement(XPATH_TO_VALIDATE_3);
	}
	
	public void typeTextToValidate3(String value){
		typeText(XPATH_TO_VALIDATE_3, value);
	}
	
	public void typeCheckToValidate3(String value){
		typeCheckOption(XPATH_TO_VALIDATE_3, value);
	}
	
	public void typeSelectToValidate3(String value){
		typeSelectComboOption(XPATH_TO_VALIDATE_3, value);
	}

}