package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_comunicacao.GoToTiposDeComunicacaoStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_comunicacao.NavegarAlteracaoTiposDeComunicacaoStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0008TiposDeComunicacaoNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0008-TiposDeComunicacao", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Tipos de Comunicação validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0008TiposDeComunicacaoNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToTiposDeComunicacaoStage());
		addStage(new NavegarAlteracaoTiposDeComunicacaoStage(getDP("NavegarAlteracaoTiposDeComunicacaoDP.json")));
	}	
}
