package br.com.upperselenium.test.stage.home.smartsearch.page;

import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class SmartSearchPage extends BaseHelperPage {

	private static final String TEXT_SMART_SEARCH = ".//div[1]/div/div/p/search-bar/div/input";
	private static final String LINK_FOUND_TITULO = ".//div[1]/div/div/p/search-bar/div/div/div/a[1]/div[2]/span[1]";
	private static final String LINK_FOUND_DESCRICAO = ".//div[1]/div/div/p/search-bar/div/div/div/a[1]/div[2]/span[2]";
	private static final String LINK_LOGO = ".//body/div[1]/div/div/a/div";

	public void typeTextSmartSearch(String value) {
		waitForATime(TimePRM._1_SEC);
		typeText(TEXT_SMART_SEARCH, value);
	}
	
	public boolean isClickableLogo() {
		return isDisplayedElement(LINK_LOGO);
	}
	
	public void clickLinkLogo() {
		waitForPageToLoadUntil10s();
		clickOnElement(LINK_LOGO, false);
		waitForPageToLoadUntil10s();
	}

	public void clickFoundItemTitulo(String expected) {
		waitForPageToLoadUntil10s();
		waitForATime(TimePRM._3_SECS);
		isClickableFoundItem();
		String found = getFoundItemLabelTitulo(expected);
		clickOnElementByComparation(LINK_FOUND_TITULO, expected, found);
		waitForPageToLoadUntil10s();
	}

	public void clickFoundItemDescricao(String expected) {
		waitForPageToLoadUntil10s();
		waitForATime(TimePRM._2_SECS);
		isClickableFoundItem();
		String found = getFoundItemLabelDescricao(expected);
		clickOnElementByComparation(LINK_FOUND_DESCRICAO, expected, found);
		waitForPageToLoadUntil10s();
	}

	public boolean isClickableFoundItem() {
		return isDisplayedElement(LINK_FOUND_TITULO);
	}

	public String getFoundItemLabelTitulo(String value) {
		return getLabel(LINK_FOUND_TITULO, value);
	}

	public String getFoundItemLabelDescricao(String value) {
		return getLabel(LINK_FOUND_DESCRICAO, value);
	}

}