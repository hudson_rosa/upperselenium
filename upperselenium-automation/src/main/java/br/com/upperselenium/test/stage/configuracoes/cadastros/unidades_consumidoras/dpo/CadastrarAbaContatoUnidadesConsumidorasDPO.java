package br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaContatoUnidadesConsumidorasDPO extends BaseHelperDPO {

	private String buttonAdicionar;
	private String modalNome;
	private String modalEmail;
	private String modalTelefone;
	private String filterNome;
	private String filterEmail;
	private String colNome;
	private String colEmail;
	private String colTelefone;

	public String getButtonAdicionar() {
		return buttonAdicionar;
	}

	public void setButtonAdicionar(String buttonAdicionar) {
		this.buttonAdicionar = buttonAdicionar;
	}

	public String getModalNome() {
		return modalNome;
	}

	public void setModalNome(String modalNome) {
		this.modalNome = modalNome;
	}

	public String getModalEmail() {
		return modalEmail;
	}

	public void setModalEmail(String modalEmail) {
		this.modalEmail = modalEmail;
	}

	public String getModalTelefone() {
		return modalTelefone;
	}

	public void setModalTelefone(String modalTelefone) {
		this.modalTelefone = modalTelefone;
	}

	public String getFilterNome() {
		return filterNome;
	}

	public void setFilterNome(String filterNome) {
		this.filterNome = filterNome;
	}

	public String getFilterEmail() {
		return filterEmail;
	}

	public void setFilterEmail(String filterEmail) {
		this.filterEmail = filterEmail;
	}

	public String getColNome() {
		return colNome;
	}

	public void setColNome(String colNome) {
		this.colNome = colNome;
	}

	public String getColEmail() {
		return colEmail;
	}

	public void setColEmail(String colEmail) {
		this.colEmail = colEmail;
	}

	public String getColTelefone() {
		return colTelefone;
	}

	public void setColTelefone(String colTelefone) {
		this.colTelefone = colTelefone;
	}

}