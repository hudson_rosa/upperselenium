package br.com.upperselenium.test.stage.configuracoes.cadastros.ramo_de_atividade;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.ramo_de_atividade.dpo.CadastrarRamoDeAtividadeDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.ramo_de_atividade.page.CadastroRamoDeAtividadePage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.ramo_de_atividade.page.RamoDeAtividadePage;

public class CadastrarRamoDeAtividadeStage extends BaseStage {
	private CadastrarRamoDeAtividadeDPO cadastrarRamoDeAtividadeDPO;
	private RamoDeAtividadePage ramoDeAtividadePage;
	private CadastroRamoDeAtividadePage cadastroRamoDeAtividadePage;
	
	public CadastrarRamoDeAtividadeStage(String dp) {
		cadastrarRamoDeAtividadeDPO = loadDataProviderFile(CadastrarRamoDeAtividadeDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		ramoDeAtividadePage = initElementsFromPage(RamoDeAtividadePage.class);
		cadastroRamoDeAtividadePage = initElementsFromPage(CadastroRamoDeAtividadePage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarRamoDeAtividadeDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		ramoDeAtividadePage.clickNovo();
		cadastroRamoDeAtividadePage.typeTextNome(cadastrarRamoDeAtividadeDPO.getNome()+getRandomStringSpaced());		
		cadastroRamoDeAtividadePage.typeTextDescricao(cadastrarRamoDeAtividadeDPO.getDescricao()+getRandomStringSpaced());		
		cadastroRamoDeAtividadePage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroRamoDeAtividadePage.validateMessageDefault(cadastrarRamoDeAtividadeDPO.getOperationMessage());
	}

}
