package br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.dpo.CadastrarAbaEnderecoSubestacaoDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.page.CadastroAbaEnderecoSubestacaoPage;

public class CadastrarAbaEnderecoSubestacaoStage extends BaseStage {
	private CadastrarAbaEnderecoSubestacaoDPO cadastrarAbaEnderecoSubestacaoDPO;
	private CadastroAbaEnderecoSubestacaoPage cadastroAbaEnderecoSubestacaoPage;
	
	public CadastrarAbaEnderecoSubestacaoStage(String dp) {
		cadastrarAbaEnderecoSubestacaoDPO = loadDataProviderFile(CadastrarAbaEnderecoSubestacaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaEnderecoSubestacaoPage = initElementsFromPage(CadastroAbaEnderecoSubestacaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaEnderecoSubestacaoDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaEnderecoSubestacaoPage.clickAbaEndereco();
		cadastroAbaEnderecoSubestacaoPage.typeTextLogradouro(cadastrarAbaEnderecoSubestacaoDPO.getLogradouro());			
		cadastroAbaEnderecoSubestacaoPage.typeTextNumero(cadastrarAbaEnderecoSubestacaoDPO.getNumero());			
		cadastroAbaEnderecoSubestacaoPage.typeTextBairro(cadastrarAbaEnderecoSubestacaoDPO.getBairro());			
		cadastroAbaEnderecoSubestacaoPage.typeTextCEP(cadastrarAbaEnderecoSubestacaoDPO.getCep());			
		cadastroAbaEnderecoSubestacaoPage.typeSelectCidade(cadastrarAbaEnderecoSubestacaoDPO.getCidade());			
		cadastroAbaEnderecoSubestacaoPage.typeSelectRegiao(cadastrarAbaEnderecoSubestacaoDPO.getRegiao());
		cadastroAbaEnderecoSubestacaoPage.typeTextLatitude(cadastrarAbaEnderecoSubestacaoDPO.getLatitude());			
		cadastroAbaEnderecoSubestacaoPage.typeTextLongitude(cadastrarAbaEnderecoSubestacaoDPO.getLongitude());			
		cadastroAbaEnderecoSubestacaoPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaEnderecoSubestacaoPage.validateMessageDefault(cadastrarAbaEnderecoSubestacaoDPO.getOperationMessage());
	}

}
