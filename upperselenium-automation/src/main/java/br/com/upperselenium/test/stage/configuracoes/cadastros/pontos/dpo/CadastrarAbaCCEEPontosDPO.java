package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaCCEEPontosDPO extends BaseHelperDPO {

	private String codigoCCEE;
	private String codigoDoAtivo;
	private String nomeDoAtivo;
	private String agenteConectante;
	private String agenteConectado;
	private String agenteDeMedicao;
	private String tipo;
	private String codigoDoAgente;
	private String nomeDoAgente;
	private String clientScde;

	public String getCodigoCCEE() {
		return codigoCCEE;
	}

	public void setCodigoCCEE(String codigoCCEE) {
		this.codigoCCEE = codigoCCEE;
	}

	public String getCodigoDoAtivo() {
		return codigoDoAtivo;
	}

	public void setCodigoDoAtivo(String codigoDoAtivo) {
		this.codigoDoAtivo = codigoDoAtivo;
	}

	public String getNomeDoAtivo() {
		return nomeDoAtivo;
	}

	public void setNomeDoAtivo(String nomeDoAtivo) {
		this.nomeDoAtivo = nomeDoAtivo;
	}

	public String getAgenteConectante() {
		return agenteConectante;
	}

	public void setAgenteConectante(String agenteConectante) {
		this.agenteConectante = agenteConectante;
	}

	public String getAgenteConectado() {
		return agenteConectado;
	}

	public void setAgenteConectado(String agenteConectado) {
		this.agenteConectado = agenteConectado;
	}

	public String getAgenteDeMedicao() {
		return agenteDeMedicao;
	}

	public void setAgenteDeMedicao(String agenteDeMedicao) {
		this.agenteDeMedicao = agenteDeMedicao;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCodigoDoAgente() {
		return codigoDoAgente;
	}

	public void setCodigoDoAgente(String codigoDoAgente) {
		this.codigoDoAgente = codigoDoAgente;
	}

	public String getNomeDoAgente() {
		return nomeDoAgente;
	}

	public void setNomeDoAgente(String nomeDoAgente) {
		this.nomeDoAgente = nomeDoAgente;
	}

	public String getClientScde() {
		return clientScde;
	}

	public void setClientScde(String clientScde) {
		this.clientScde = clientScde;
	}

}