package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos.CadastrarCircuitosStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos.GoToCircuitosStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0006CircuitosCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0006-Circuitos", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Circuitos realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0006CircuitosCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToCircuitosStage());
		addStage(new CadastrarCircuitosStage(getDP("CadastrarCircuitosDP.json")));
	}	
}
