package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaDemandaContratadaPontosVirtuaisDPO extends BaseHelperDPO {

	private List<GroupCadastrarNovaDemandaContratadaDPO> groupNovaDemandaContratada;
	private List<GridCadastrarDemandasContratadasDPO> gridDemandasContratadas;

	public List<GroupCadastrarNovaDemandaContratadaDPO> getGroupNovaDemandaContratada() {
		return groupNovaDemandaContratada;
	}

	public void setGroupNovaDemandaContratada(List<GroupCadastrarNovaDemandaContratadaDPO> groupNovaDemandaContratada) {
		this.groupNovaDemandaContratada = groupNovaDemandaContratada;
	}

	public List<GridCadastrarDemandasContratadasDPO> getGridDemandasContratadas() {
		return gridDemandasContratadas;
	}

	public void setGridDemandasContratadas(List<GridCadastrarDemandasContratadasDPO> gridDemandasContratadas) {
		this.gridDemandasContratadas = gridDemandasContratadas;
	}

}