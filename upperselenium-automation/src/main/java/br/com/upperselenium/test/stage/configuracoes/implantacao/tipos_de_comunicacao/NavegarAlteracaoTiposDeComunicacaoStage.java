package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_comunicacao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_comunicacao.dpo.NavegarAlterarTiposDeComunicacaoDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_comunicacao.page.AlteracaoTiposDeComunicacaoPage;

public class NavegarAlteracaoTiposDeComunicacaoStage extends BaseStage {
	private NavegarAlterarTiposDeComunicacaoDPO navegarAlteracaoTiposDeComunicacaoDPO;
	private AlteracaoTiposDeComunicacaoPage alteracaoTiposDeComunicacaoPage;
	
	public NavegarAlteracaoTiposDeComunicacaoStage(String dp) {
		navegarAlteracaoTiposDeComunicacaoDPO = loadDataProviderFile(NavegarAlterarTiposDeComunicacaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alteracaoTiposDeComunicacaoPage = initElementsFromPage(AlteracaoTiposDeComunicacaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAlteracaoTiposDeComunicacaoDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		alteracaoTiposDeComunicacaoPage.getLabelHeaderNome(navegarAlteracaoTiposDeComunicacaoDPO.getLabelNome());
		alteracaoTiposDeComunicacaoPage.getLabelHeaderDescricao(navegarAlteracaoTiposDeComunicacaoDPO.getLabelDescricao());
		alteracaoTiposDeComunicacaoPage.getLabelHeaderAtivo(navegarAlteracaoTiposDeComunicacaoDPO.getLabelAtivo());
		alteracaoTiposDeComunicacaoPage.getBreadcrumbsText(navegarAlteracaoTiposDeComunicacaoDPO.getBreadcrumbs());
		alteracaoTiposDeComunicacaoPage.clickUpToBreadcrumbs(navegarAlteracaoTiposDeComunicacaoDPO.getUpToBreadcrumb());
		alteracaoTiposDeComunicacaoPage.getWelcomeTitle(navegarAlteracaoTiposDeComunicacaoDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
