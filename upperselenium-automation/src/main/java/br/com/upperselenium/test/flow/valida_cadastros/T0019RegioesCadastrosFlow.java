package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.CadastrarRegioesStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.GoToRegioesStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.ImportacaoDePerdasTecnicasPorRegiaoStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.LinkToImportacaoDePerdasTecnicasPorRegiaoStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.LinkToPerdasTecnicasStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.PerdasTecnicasStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;
import br.com.upperselenium.test.stage.wrapper.navigation.BackToStage;

@SuiteClasses(T0019RegioesCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0019-Regioes", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Regiões realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0019RegioesCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToRegioesStage());
		addStage(new LinkToPerdasTecnicasStage());
		addStage(new LinkToImportacaoDePerdasTecnicasPorRegiaoStage());
		addStage(new ImportacaoDePerdasTecnicasPorRegiaoStage(getDP("ImportacaoDePerdasTecnicasPorRegiaoDP.json")));
		addStage(new BackToStage());
		addStage(new PerdasTecnicasStage(getDP("PerdasTecnicasDP.json")));
		addStage(new BackToStage());
		addStage(new CadastrarRegioesStage(getDP("CadastroRegioesDP.json")));
	}	
}
