package br.com.upperselenium.test.stage.configuracoes.cadastros.auto_discovery_pendentes.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AutoDiscoveryPage extends BaseHelperPage{
	
	private static final String BUTTON_FINALIZAR_SELECIONADOS = "//*[@id='auto-discovery-pendentes']/form/input";
	
	public boolean isClickableButtonFinalizarSelecionados() {
		return isDisplayedElement(BUTTON_FINALIZAR_SELECIONADOS);
	}
	
	public void clickFinalizarSelecionados(){
		clickOnElement(BUTTON_FINALIZAR_SELECIONADOS);
	}

}