package br.com.upperselenium.test.stage.configuracoes.cadastros.regioes;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.dpo.ImportacaoDePerdasTecnicasPorRegiaoDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regioes.page.ImportacaoDePerdasTecnicasPorRegiaoPage;

public class ImportacaoDePerdasTecnicasPorRegiaoStage extends BaseStage {
	private ImportacaoDePerdasTecnicasPorRegiaoDPO importacaoDePerdasTecnicasPorRegiaoDPO;
	private ImportacaoDePerdasTecnicasPorRegiaoPage importacaoDePerdasTecnicasPorRegiaoPage;
	
	public ImportacaoDePerdasTecnicasPorRegiaoStage(String dp) {
		importacaoDePerdasTecnicasPorRegiaoDPO = loadDataProviderFile(ImportacaoDePerdasTecnicasPorRegiaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		importacaoDePerdasTecnicasPorRegiaoPage = initElementsFromPage(ImportacaoDePerdasTecnicasPorRegiaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(importacaoDePerdasTecnicasPorRegiaoDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		importacaoDePerdasTecnicasPorRegiaoPage.clickImportar();
	}

	@Override
	public void runValidations() {
		validateMessageErrorArquivo();
	}
	
	private void validateMessageErrorArquivo() {
		importacaoDePerdasTecnicasPorRegiaoPage.validateEqualsToValues(
				importacaoDePerdasTecnicasPorRegiaoPage.getMessageErrorArquivo(), 
				importacaoDePerdasTecnicasPorRegiaoPage.getMessageErrorArquivo());
	}
	
}
