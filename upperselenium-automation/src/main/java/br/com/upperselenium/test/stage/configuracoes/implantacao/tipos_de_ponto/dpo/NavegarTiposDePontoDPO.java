package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarTiposDePontoDPO extends BaseHelperDPO {

	private String labelNome;
	private String labelDescricao;
	private String labelGrupoDeMedicao;
	private String labelAbas;

	public String getLabelNome() {
		return labelNome;
	}

	public void setLabelNome(String labelNome) {
		this.labelNome = labelNome;
	}

	public String getLabelDescricao() {
		return labelDescricao;
	}

	public void setLabelDescricao(String labelDescricao) {
		this.labelDescricao = labelDescricao;
	}

	public String getLabelGrupoDeMedicao() {
		return labelGrupoDeMedicao;
	}

	public void setLabelGrupoDeMedicao(String labelGrupoDeMedicao) {
		this.labelGrupoDeMedicao = labelGrupoDeMedicao;
	}

	public String getLabelAbas() {
		return labelAbas;
	}

	public void setLabelAbas(String labelAbas) {
		this.labelAbas = labelAbas;
	}

	@Override
	public String toString() {
		return "NavegarTiposDePontoDPO [labelNome=" + labelNome + ", labelDescricao=" + labelDescricao
				+ ", labelGrupoDeMedicao=" + labelGrupoDeMedicao + ", labelAbas=" + labelAbas + "]";
	}

}