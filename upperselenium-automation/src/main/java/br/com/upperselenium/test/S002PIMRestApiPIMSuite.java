package br.com.upperselenium.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseSuite;
import br.com.upperselenium.base.annotation.SuiteParams;
import br.com.upperselenium.test.flow.valida_rest_api.T0001PedidoDeMedidoresRequestApiFlow;


@RunWith(Suite.class)

@SuiteClasses({
	T0001PedidoDeMedidoresRequestApiFlow.class,
	})

@SuiteParams(description="PIM - Validação de Rest API")
public class S002PIMRestApiPIMSuite extends BaseSuite {}
