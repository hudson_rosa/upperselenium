package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.dpo.NavegarAlteracaoTiposDePontoDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.page.AlteracaoTiposDePontoPage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.page.TiposDePontoPage;

public class NavegarAlteracaoTiposDePontoStage extends BaseStage {
	private NavegarAlteracaoTiposDePontoDPO navegarAlteracaoTiposDePontoDPO;
	private TiposDePontoPage tiposDePontoPage;
	private AlteracaoTiposDePontoPage alteracaoTiposDePontoPage;
	
	public NavegarAlteracaoTiposDePontoStage(String dp) {
		navegarAlteracaoTiposDePontoDPO = loadDataProviderFile(NavegarAlteracaoTiposDePontoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDePontoPage = initElementsFromPage(TiposDePontoPage.class);
		alteracaoTiposDePontoPage = initElementsFromPage(AlteracaoTiposDePontoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAlteracaoTiposDePontoDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		tiposDePontoPage.clickGridButtonFirstLineEditar(navegarAlteracaoTiposDePontoDPO.getClickItem());
		alteracaoTiposDePontoPage.getLabelMonitoraDadosDuplicados(navegarAlteracaoTiposDePontoDPO.getLabelMonitoraDadosDuplicados());
		alteracaoTiposDePontoPage.clickAbaAbas();
		alteracaoTiposDePontoPage.getLabelHeaderAbas(navegarAlteracaoTiposDePontoDPO.getLabelHeaderAbas());
		alteracaoTiposDePontoPage.getLabelHeaderVisualizar(navegarAlteracaoTiposDePontoDPO.getLabelHeaderVisualizar());
		alteracaoTiposDePontoPage.clickAbaTemplates();
		alteracaoTiposDePontoPage.getLabelHeaderHabilitado(navegarAlteracaoTiposDePontoDPO.getLabelHeaderHabilitado());
		alteracaoTiposDePontoPage.getLabelHeaderGrandeza(navegarAlteracaoTiposDePontoDPO.getLabelHeaderGrandeza());
		alteracaoTiposDePontoPage.getLabelHeaderTipoDeGrandeza(navegarAlteracaoTiposDePontoDPO.getLabelHeaderTipoDeGrandeza());
		alteracaoTiposDePontoPage.getLabelHeaderIntegralizacao(navegarAlteracaoTiposDePontoDPO.getLabelHeaderIntegralizacao());
		alteracaoTiposDePontoPage.getLabelHeaderEhMedida(navegarAlteracaoTiposDePontoDPO.getLabelHeaderEhMedida());
		alteracaoTiposDePontoPage.clickAbaIcones();
		alteracaoTiposDePontoPage.getLabelButtonSalvar(navegarAlteracaoTiposDePontoDPO.getLabelButtonSalvar());
		alteracaoTiposDePontoPage.getBreadcrumbsText(navegarAlteracaoTiposDePontoDPO.getBreadcrumbs());
		alteracaoTiposDePontoPage.clickUpToBreadcrumbs(navegarAlteracaoTiposDePontoDPO.getUpToBreadcrumb());
		alteracaoTiposDePontoPage.getWelcomeTitle(navegarAlteracaoTiposDePontoDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
