package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoAbaConstantesMedidoresPage extends BaseHelperPage{
	
	private static final String LINK_ABA_COLETAS = ".//div[2]/div/div[2]/ul[2]/li[9]/a";
	private static final String BUTTON_EDITAR = "//*[@id='editarConstante']"; 
	private static final String TEXT_FILTER_GRANDEZA = "//*[@id='tbConstantes']/thead/tr[1]/th[2]/span/input";
	private static final String TEXT_FILTER_VALOR_KE_CONSTANTE = "//*[@id='tbConstantes']/thead/tr[1]/th[3]/span/input";
	private static final String GRID_CHECK_ALL = "//*[@id='tbConstantes']/thead/tr[2]/th[1]/input";
	private static final String GRID_CHECK_GRANDEZA = "//*[@id='tbConstantes']/tbody/tr[1]/td[1]/input";
	private static final String GRID_CHECK_ITEM = "//*[@id='tbConstantes']/tbody/tr[%i%]/td[1]/input";
	private static final String GRID_VALUE_GRANDEZA = "//*[@id='tbConstantes']/tbody/tr[%i%]/td[2]";
	private static final String GRID_VALUE_VALOR_KE_CONSTANTE = "//*[@id='tbConstantes']/tbody/tr[%i%]/td[3]";
	private static final String MODAL_TEXT_DATA_DA_ULTIMA_COLETA = "//*[@id='valorKe']";
	private static final String MODAL_BUTTON_SALVAR = "//*[@id='salvarConstante']";
		
	public void typeTextFilterGrandeza(String value){
		typeText(TEXT_FILTER_GRANDEZA, value);
	}	
	
	public void typeTextFilterValorKeContante(String value){
		typeText(TEXT_FILTER_VALOR_KE_CONSTANTE, value);
	}	

	public void typeCheckAll(String value){
		typeCheckOption(GRID_CHECK_ALL, value);
	}
	
	public void typeCheckBoxGridItemGrandeza(String value){
		typeGridCheckFirstFilteredItem(GRID_CHECK_GRANDEZA, value);
	}	

	public void typeCheckBoxGridItems(String value,int index){
		typeGridCheckOption(GRID_CHECK_ITEM, value, index);
	}
	
	public void getGridLabelValueGrandeza(String value, int index){
		getGridLabel(GRID_VALUE_GRANDEZA, value, index);
	}	

	public void getGridLabelValueValorKeConstante(String value, int index){
		getGridLabel(GRID_VALUE_VALOR_KE_CONSTANTE, value, index);
	}	
				
	public void typeModalTextValorKeConstante(String value){
		typeText(MODAL_TEXT_DATA_DA_ULTIMA_COLETA, value);
	}
	
	public boolean isClickableButtonEditar() {
		useKey(BUTTON_EDITAR, Keys.END);
		return isDisplayedElement(By.xpath(BUTTON_EDITAR));
	}
	
	public void clickEditar(){
		clickOnElement(BUTTON_EDITAR);
	}
	
	public boolean isClickableButtonModalSalvar() {
		return isDisplayedElement(By.xpath(MODAL_BUTTON_SALVAR));
	}
	
	public void clickModalSalvar(){
		clickOnElement(MODAL_BUTTON_SALVAR);
	}

	public void clickAbaConstantes(){
		clickOnElement(LINK_ABA_COLETAS);
	}	
}