package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastrarAbaEquipamentosRelePontosVirtuaisPage extends BaseHelperPage{
	
	private static final String LINK_EQUIPAMENTOS = "//*[@id='tabs']/ul/li[@class='dropdown']/a";
	private static final String LINK_SUBMENU_RELE = "//*[@id='tabs']/ul/li[contains(@class,'dropdown')]/ul[contains(@class,'dropdown-menu')]/li[2]/a[1]";
	private static final String BUTTON_SALVAR = "//*[@id='salvar']";   
	private static final String SELECT_TIPO = "//*[@id='Fabricante']";
	private static final String TEXT_NUMERO_DE_SERIE = "//*[@id='NumeroDeSerie']";
	private static final String TEXT_CORRENTE_DE_PICKUP = "//*[@id='NumeroDeCorrentePickup']";

	public void clickEquipamentos(){
		clickOnElement(LINK_EQUIPAMENTOS);
	}

	public void clickSubmenuRele(){		
		clickOnElement(LINK_SUBMENU_RELE);
	}
	
	public void typeSelectTipo(String value){
		typeSelectComboOption(SELECT_TIPO, value);
	}
	
	public void typeTextNumeroDeSerie(String value){
		typeText(TEXT_NUMERO_DE_SERIE, value);
	}	
	
	public void typeTextCorrenteDePickup(String value){
		typeText(TEXT_CORRENTE_DE_PICKUP, value);
	}	
		
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}