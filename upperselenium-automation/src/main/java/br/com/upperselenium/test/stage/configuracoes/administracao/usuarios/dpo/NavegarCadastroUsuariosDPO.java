package br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarCadastroUsuariosDPO extends BaseHelperDPO {

	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "NavegarCadastroPerfisDPO [nome=" + nome + "]";
	}

}