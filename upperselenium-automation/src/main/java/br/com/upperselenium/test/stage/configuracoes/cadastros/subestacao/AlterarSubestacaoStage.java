package br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.dpo.AlterarSubestacaoDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.page.AlteracaoSubestacaoPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.page.SubestacaoPage;

public class AlterarSubestacaoStage extends BaseStage {
	private AlterarSubestacaoDPO alterarSubestacaoDPO;
	private SubestacaoPage subestacaoPage;
	private AlteracaoSubestacaoPage alteracaoSubestacaoPage;
	
	public AlterarSubestacaoStage(String dp) {
		alterarSubestacaoDPO = loadDataProviderFile(AlterarSubestacaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		subestacaoPage = initElementsFromPage(SubestacaoPage.class);
		alteracaoSubestacaoPage = initElementsFromPage(AlteracaoSubestacaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarSubestacaoDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		subestacaoPage.clickNova();
		alteracaoSubestacaoPage.typeTextNome(alterarSubestacaoDPO.getNome()+getRandomStringSpaced());		
		alteracaoSubestacaoPage.typeTextDescricao(alterarSubestacaoDPO.getDescricao()+getRandomStringSpaced());		
		alteracaoSubestacaoPage.typeTextQuantidadeDePontos(alterarSubestacaoDPO.getQuantidadeDePontos());		
		alteracaoSubestacaoPage.typeTextAlimentacaoAuxiliar(alterarSubestacaoDPO.getAlimentacaoAuxiliar());		
		alteracaoSubestacaoPage.typeTextObservacoes(alterarSubestacaoDPO.getObservacoes());		
		alteracaoSubestacaoPage.typeTextPropriedade(alterarSubestacaoDPO.getPropriedade());		
		alteracaoSubestacaoPage.typeTextFronteira(alterarSubestacaoDPO.getFronteira());		
		alteracaoSubestacaoPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoSubestacaoPage.validateMessageDefault(alterarSubestacaoDPO.getOperationMessage());
	}

}
