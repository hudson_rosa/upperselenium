package br.com.upperselenium.test.stage.configuracoes.administracao.usuarios;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.dpo.CadastrarUsuariosDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page.CadastroUsuariosPage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page.UsuariosPage;

public class CadastrarUsuariosStage extends BaseStage {
	private CadastrarUsuariosDPO cadastrarUsuariosDPO;
	private UsuariosPage usuariosPage;
	private CadastroUsuariosPage cadastroUsuariosPage;
	
	public CadastrarUsuariosStage(String dp) {
		cadastrarUsuariosDPO = loadDataProviderFile(CadastrarUsuariosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		usuariosPage = initElementsFromPage(UsuariosPage.class);
		cadastroUsuariosPage = initElementsFromPage(CadastroUsuariosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarUsuariosDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		usuariosPage.clickNovo();
		cadastroUsuariosPage.typeTextNome(cadastrarUsuariosDPO.getNome()+getRandomStringNonSpaced());
		cadastroUsuariosPage.typeTextLogin(cadastrarUsuariosDPO.getLogin()+getRandomStringNonSpaced());
		cadastroUsuariosPage.typeTextSenha(cadastrarUsuariosDPO.getSenha());
		cadastroUsuariosPage.typeTextConfirmacao(cadastrarUsuariosDPO.getConfirmacao());
		cadastroUsuariosPage.typeTextEmail(cadastrarUsuariosDPO.getEmail());
		cadastroUsuariosPage.typeTextTelefone(cadastrarUsuariosDPO.getTelefone());
		cadastroUsuariosPage.keyPageDown();
		cadastroUsuariosPage.typeSelectPerfil(cadastrarUsuariosDPO.getPerfil());
		cadastroUsuariosPage.typeSelectPasta(cadastrarUsuariosDPO.getPasta());
		cadastroUsuariosPage.typeCheckBloqueado(cadastrarUsuariosDPO.getBloqueado());
		cadastroUsuariosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroUsuariosPage.validateMessageDefault(cadastrarUsuariosDPO.getOperationMessage());
	}
}
