package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_alarmes.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoTiposDeAlarmesPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='btnSalvar']"; 
	private static final String LABEL_HEADER_NOME = "//*[@id='dataTableList']/thead/tr[2]/th[contains(text(),'Nome')]";	
	private static final String LABEL_HEADER_MOTIVO = "//*[@id='dataTableList']/thead/tr[2]/th[contains(text(),'Motivo')]";	
	private static final String LABEL_HEADER_CRITICIDADE = "//*[@id='dataTableList']/thead/tr[2]/th[contains(text(),'Criticidade')]";	
	private static final String LABEL_HEADER_STATUS = "//*[@id='dataTableList']/thead/tr[2]/th[contains(text(),'Status')]";
		
	public void getLabelHeaderNome(String value){
		getLabel(LABEL_HEADER_NOME, value);
	}
	
	public void getLabelHeaderMotivo(String value){
		getLabel(LABEL_HEADER_MOTIVO, value);
	}
	
	public void getLabelHeaderCriticidade(String value){
		getLabel(LABEL_HEADER_CRITICIDADE, value);
	}
	
	public void getLabelHeaderStatus(String value){
		getLabel(LABEL_HEADER_STATUS, value);
	}

	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}