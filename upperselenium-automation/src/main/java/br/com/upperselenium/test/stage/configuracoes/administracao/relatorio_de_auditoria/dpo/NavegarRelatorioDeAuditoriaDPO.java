package br.com.upperselenium.test.stage.configuracoes.administracao.relatorio_de_auditoria.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarRelatorioDeAuditoriaDPO extends BaseHelperDPO {

	private String exportacaoExcel;

	public String getExportacaoExcel() {
		return exportacaoExcel;
	}

	public void setExportacaoExcel(String exportacaoExcel) {
		this.exportacaoExcel = exportacaoExcel;
	}

}