package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

public class GroupSalvarInstrumentoAnemometriaDPO {

	private String selecioneInstrumento;
	private String altura;

	public String getSelecioneInstrumento() {
		return selecioneInstrumento;
	}

	public void setSelecioneInstrumento(String selecioneInstrumento) {
		this.selecioneInstrumento = selecioneInstrumento;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

}
