package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarCadastroTiposDeEquipamentoDPO extends BaseHelperDPO {

	private String marca;

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	@Override
	public String toString() {
		return "NavegarCadastroTiposDeEquipamentoDPO [marca=" + marca + "]";
	}

}