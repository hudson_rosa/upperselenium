package br.com.upperselenium.test.stage.configuracoes.cadastros.fronteiras;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.fronteiras.dpo.AlterarFronteirasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.fronteiras.page.AlteracaoFronteirasPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.fronteiras.page.FronteirasPage;

public class AlterarFronteirasStage extends BaseStage {
	private AlterarFronteirasDPO alterarFronteirasDPO;
	private FronteirasPage fronteirasPage;
	private AlteracaoFronteirasPage alteracaoFronteirasPage;
	
	public AlterarFronteirasStage(String dp) {
		alterarFronteirasDPO = loadDataProviderFile(AlterarFronteirasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		fronteirasPage = initElementsFromPage(FronteirasPage.class);
		alteracaoFronteirasPage = initElementsFromPage(AlteracaoFronteirasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarFronteirasDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();
		alteracaoFronteirasPage.typeTextNome(alterarFronteirasDPO.getNome()+getRandomStringSpaced());				
		alteracaoFronteirasPage.typeTextPontoDeEnergiaRequerida(alterarFronteirasDPO.getPontoDeEnergiaRequerida());				
		alteracaoFronteirasPage.typeTextPontoDePerdaTecnicaATMedida(alterarFronteirasDPO.getPontoDeperdaTecnicaATMedida());				
		alteracaoFronteirasPage.typeTextPseudonimoDeSistemasDePerdasTecnicas(alterarFronteirasDPO.getPseudonimoDeSistemasDePerdasTecnicas());				
		alteracaoFronteirasPage.typeTextSuprimentoRegional(alterarFronteirasDPO.getSuprimentoRegional());				
		alteracaoFronteirasPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoFronteirasPage.validateMessageDefault(alterarFronteirasDPO.getOperationMessage());
	}

}
