package br.com.upperselenium.test.stage.configuracoes.cadastros.trafos_bt.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoTrafosBTPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='salvar']";   
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String TEXT_DESCRICAO = "//*[@id='Descricao']";
	private static final String TEXT_IDENTIFICADOR = "//*[@id='Identificador']";
	private static final String TEXT_POTENCIA = "//*[@id='PotenciaKva']";
	private static final String TEXT_NUMERO_DE_FASES = "//*[@id='NumeroDeFases']";
	private static final String TEXT_MEDICAO = "//*[@id='Medicao']";
	private static final String TEXT_ALIMENTADORES = "//*[@id='Alimentador_Nome']";
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}
	
	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}
	
	public void typeTextIdentificador(String value){
		typeText(TEXT_IDENTIFICADOR, value);
	}
	
	public void typeTextPotencia(String value){
		typeText(TEXT_POTENCIA, value);
	}
	
	public void typeSelectNumeroDeFases(String value){
		typeSelectComboOption(TEXT_NUMERO_DE_FASES, value);
	}
	
	public void typeTextMedicao(String value){
		typeTextAutoCompleteSelect(TEXT_MEDICAO, value);
	}
	
	public void typeTextAlimentadores(String value){
		typeTextAutoCompleteSelect(TEXT_ALIMENTADORES, value);
	}
	
	public void keyPageDown(){
		useKey(TEXT_NOME, Keys.PAGE_DOWN);
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}