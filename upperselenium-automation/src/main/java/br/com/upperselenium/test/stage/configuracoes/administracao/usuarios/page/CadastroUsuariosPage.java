package br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroUsuariosPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/form/fieldset/div[11]/input"; 
	private static final String TEXT_NOME = "//*[@id='Usuario_Nome']";
	private static final String TEXT_LOGIN = "//*[@id='Usuario_Login']";
	private static final String TEXT_SENHA = "//*[@id='Senha']";
	private static final String TEXT_CONFIRMACAO = "//*[@id='Confirmacao']";
	private static final String TEXT_EMAIL = "//*[@id='Usuario_Email']";
	private static final String TEXT_TELEFONE = "//*[@id='Usuario_Telefone']";
	private static final String SELECT_PERFIL = "//*[@id='PerfilId']";
	private static final String SELECT_PASTA = "//*[@id='PastaId']";
	private static final String CHECK_BLOQUEADO = "//*[@id='Bloqueado']";
	private static final String TOGGLE_TOKEN = "//*[@id='tokenLabel']/i";
	private static final String BUTTON_GERAR = "//*[@id='gerarToken']";
	private static final String BUTTON_EXCLUIR = "//*[@id='excluirToken']";
	private static final String LABEL_TOKEN = "//*[@id='tokenValueField']";
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	
	
	public void typeTextLogin(String value){
		typeText(TEXT_LOGIN, value);
	}	
	
	public void typeTextSenha(String value){
		typeText(TEXT_SENHA, value);
	}	
	
	public void typeTextConfirmacao(String value){
		typeText(TEXT_CONFIRMACAO, value);
	}	
	
	public void typeTextEmail(String value){
		typeText(TEXT_EMAIL, value);
	}	
	
	public void typeTextTelefone(String value){
		typeText(TEXT_TELEFONE, value);
	}	
	
	public void typeSelectPerfil(String value){
		typeSelectComboOption(SELECT_PERFIL, value);
	}
	
	public void typeSelectPasta(String value){
		typeSelectComboOption(SELECT_PASTA, value);
	}
	
	public void typeCheckBloqueado(String value){
		typeCheckOption(CHECK_BLOQUEADO, value);
	}
	
	public void getLabelGeneratedToken(String value){
		getLabel(LABEL_TOKEN, value);
	}	

	public void keyPageDown(){
		useKey(TEXT_TELEFONE, Keys.PAGE_DOWN);
	}	

	public void clickToggleToken(){
		clickOnElement(TOGGLE_TOKEN);
	}
	
	public void clickGerar(){
		clickOnElement(BUTTON_GERAR);
	}
	
	public void clickExcluir(){
		clickOnElement(BUTTON_EXCLUIR);
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}