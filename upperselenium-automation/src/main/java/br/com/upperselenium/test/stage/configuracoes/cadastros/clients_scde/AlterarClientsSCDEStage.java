package br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.dpo.AlterarClientsSCDEDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.page.AlteracaoClientsSCDEPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.page.ClientsSCDEPage;

public class AlterarClientsSCDEStage extends BaseStage {
	private AlterarClientsSCDEDPO alterarClientsSCDEDPO;
	private ClientsSCDEPage clientsSCDEPage;
	private AlteracaoClientsSCDEPage alteracaoClientsSCDEPage;
	
	public AlterarClientsSCDEStage(String dp) {
		alterarClientsSCDEDPO = loadDataProviderFile(AlterarClientsSCDEDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		clientsSCDEPage = initElementsFromPage(ClientsSCDEPage.class);
		alteracaoClientsSCDEPage = initElementsFromPage(AlteracaoClientsSCDEPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarClientsSCDEDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		waitForPageToLoadUntil10s();		
		alteracaoClientsSCDEPage.typeTextNome(alterarClientsSCDEDPO.getNome());
		alteracaoClientsSCDEPage.typeSelectTipo(alterarClientsSCDEDPO.getTipo());
		alteracaoClientsSCDEPage.typeTextPath(alterarClientsSCDEDPO.getPath());
		alteracaoClientsSCDEPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoClientsSCDEPage.validateMessageDefault(alterarClientsSCDEDPO.getOperationMessage());
	}

}
