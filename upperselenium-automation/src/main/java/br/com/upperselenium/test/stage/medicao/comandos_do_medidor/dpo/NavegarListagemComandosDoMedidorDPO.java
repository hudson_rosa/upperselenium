package br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarListagemComandosDoMedidorDPO extends BaseHelperDPO {

	private String labelFiltrarDataProximaExecucao;
	private String clickFiltrar;
	private String labelHeaderMedidorParametros;
	private String labelHeaderInsercao;
	private String labelHeaderProximaExecucao;
	private String labelHeaderDescricao;
	private String labelHeaderUltimasExecucoes;
	private String labelHeaderExpiracao;

	public String getLabelFiltrarDataProximaExecucao() {
		return labelFiltrarDataProximaExecucao;
	}

	public void setLabelFiltrarDataProximaExecucao(String labelFiltrarDataProximaExecucao) {
		this.labelFiltrarDataProximaExecucao = labelFiltrarDataProximaExecucao;
	}

	public String getClickFiltrar() {
		return clickFiltrar;
	}

	public void setClickFiltrar(String clickFiltrar) {
		this.clickFiltrar = clickFiltrar;
	}

	public String getLabelHeaderMedidorParametros() {
		return labelHeaderMedidorParametros;
	}

	public void setLabelHeaderMedidorParametros(String labelHeaderMedidorParametros) {
		this.labelHeaderMedidorParametros = labelHeaderMedidorParametros;
	}

	public String getLabelHeaderInsercao() {
		return labelHeaderInsercao;
	}

	public void setLabelHeaderInsercao(String labelHeaderInsercao) {
		this.labelHeaderInsercao = labelHeaderInsercao;
	}

	public String getLabelHeaderProximaExecucao() {
		return labelHeaderProximaExecucao;
	}

	public void setLabelHeaderProximaExecucao(String labelHeaderProximaExecucao) {
		this.labelHeaderProximaExecucao = labelHeaderProximaExecucao;
	}

	public String getLabelHeaderDescricao() {
		return labelHeaderDescricao;
	}

	public void setLabelHeaderDescricao(String labelHeaderDescricao) {
		this.labelHeaderDescricao = labelHeaderDescricao;
	}

	public String getLabelHeaderUltimasExecucoes() {
		return labelHeaderUltimasExecucoes;
	}

	public void setLabelHeaderUltimasExecucoes(String labelHeaderUltimasExecucoes) {
		this.labelHeaderUltimasExecucoes = labelHeaderUltimasExecucoes;
	}

	public String getLabelHeaderExpiracao() {
		return labelHeaderExpiracao;
	}

	public void setLabelHeaderExpiracao(String labelHeaderExpiracao) {
		this.labelHeaderExpiracao = labelHeaderExpiracao;
	}

	@Override
	public String toString() {
		return "NavegarComandosDoMedidorDPO [labelFiltrarDataProximaExecucao=" + labelFiltrarDataProximaExecucao
				+ ", clickFiltrar=" + clickFiltrar + ", labelHeaderMedidorParametros=" + labelHeaderMedidorParametros
				+ ", labelHeaderInsercao=" + labelHeaderInsercao + ", labelHeaderProximaExecucao="
				+ labelHeaderProximaExecucao + ", labelHeaderDescricao=" + labelHeaderDescricao
				+ ", labelHeaderUltimasExecucoes=" + labelHeaderUltimasExecucoes + ", labelHeaderExpiracao="
				+ labelHeaderExpiracao + "]";
	}

}