package br.com.upperselenium.test.stage.wrapper.menu_area.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public abstract class MenuNavegationPage extends BaseHelperPage {

	private AreasMenuPage areas;
	private DropDownMenuPage dropdownSubAreas;
	private SideBarCadastrosPage sidebarCadastros;

	public AreasMenuPage getAreas() {
		return areas;
	}

	public void setAreas(AreasMenuPage areas) {
		this.areas = areas;
	}

	public DropDownMenuPage getDropdownSubAreas() {
		return dropdownSubAreas;
	}

	public void setDropdownSubAreas(DropDownMenuPage dropdownSubAreas) {
		this.dropdownSubAreas = dropdownSubAreas;
	}

	public SideBarCadastrosPage getSidebarCadastros() {
		return sidebarCadastros;
	}

	public void setSidebarCadastros(SideBarCadastrosPage sidebarCadastros) {
		this.sidebarCadastros = sidebarCadastros;
	}

}