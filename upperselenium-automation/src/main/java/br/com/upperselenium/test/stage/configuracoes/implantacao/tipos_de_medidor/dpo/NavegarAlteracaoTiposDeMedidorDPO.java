package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarAlteracaoTiposDeMedidorDPO extends BaseHelperDPO {

	private String clickItem;
	private String labelAbas;
	private String labelVisualizar;
	private String labelButtonSalvar;

	public String getClickItem() {
		return clickItem;
	}

	public void setClickItem(String clickItem) {
		this.clickItem = clickItem;
	}

	public String getLabelAbas() {
		return labelAbas;
	}

	public void setLabelAbas(String labelAbas) {
		this.labelAbas = labelAbas;
	}

	public String getLabelVisualizar() {
		return labelVisualizar;
	}

	public void setLabelVisualizar(String labelVisualizar) {
		this.labelVisualizar = labelVisualizar;
	}

	public String getLabelButtonSalvar() {
		return labelButtonSalvar;
	}

	public void setLabelButtonSalvar(String labelButtonSalvar) {
		this.labelButtonSalvar = labelButtonSalvar;
	}

	@Override
	public String toString() {
		return "NavegarAlteracaoTiposDeMedidorDPO [clickItem=" + clickItem + ", labelAbas=" + labelAbas
				+ ", labelVisualizar=" + labelVisualizar + ", labelButtonSalvar=" + labelButtonSalvar + "]";
	}

}