package br.com.upperselenium.test.stage.wrapper.menu_area.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class SideBarAdministracaoPage extends BaseHelperPage{
	
	private static final String LINK_PASTAS = "//*[@id='Pastas']/a[contains(text(), 'Pastas')]";
	private static final String LINK_PERFIS = "//*[@id='Perfis']/a[contains(text(), 'Perfis')]";
	private static final String LINK_RELATORIO_DE_AUDITORIA = "//*[@id='RelatorioAuditoria']/a[contains(text(), 'Relatório de Auditoria')]";
	private static final String LINK_USUARIOS = "//*[@id='Usuarios']/a[contains(text(), 'Usuários')]";
	
	public boolean isClickableLinkPastas() {
		return isDisplayedElement(LINK_PASTAS);
	}

	public boolean isClickableLinkPerfis() {
		return isDisplayedElement(LINK_PERFIS);
	}

	public boolean isClickableLinkRelatorioDeAuditoria() {
		return isDisplayedElement(LINK_RELATORIO_DE_AUDITORIA);
	}

	public boolean isClickableLinkUsuarios() {		
		return isDisplayedElement(LINK_USUARIOS);
	}
	
	public void clickPastas(){
		waitForElementToBeClickable(LINK_PASTAS);
		clickOnElement(LINK_PASTAS);
	}
	
	public void clickPerfis(){
		waitForElementToBeClickable(LINK_PERFIS);
		clickOnElement(LINK_PERFIS);
	}
	
	public void clickRelatorioDeAuditoria(){
		waitForElementToBeClickable(LINK_RELATORIO_DE_AUDITORIA);
		clickOnElement(LINK_RELATORIO_DE_AUDITORIA);
	}	

	public void clickUsuarios(){
		waitForElementToBeClickable(LINK_USUARIOS);
		clickOnElement(LINK_USUARIOS);
	}	
	
	public void keyPageDown(){
		useKey(LINK_USUARIOS, Keys.PAGE_DOWN);
	}

}