package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaConfiguracoesMedidoresPage extends BaseHelperPage{
	
	private static final String LINK_ABA_CONFIGURACOES = ".//div[2]/div/div[2]/ul[2]/li[2]/a";  
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 	
	private static final String CHECK_INFORMAR_RTP_RTC = "//*[@id='InformarTermos']"; 
	private static final String TEXT_NUMERADOR_RTP = "//*[@id='NumeradorRtp']"; 
	private static final String TEXT_NUMERADOR_RTC = "//*[@id='NumeradorRtc']"; 
	private static final String TEXT_DENOMINADOR_RTP = "//*[@id='DenominadorRtp']"; 
	private static final String TEXT_DENOMINADOR_RTC = "//*[@id='DenominadorRtc']"; 
	private static final String TEXT_RELACAO_RTP = "//*[@id='RelacaoRtp']";
	private static final String TEXT_RELACAO_RTC = "//*[@id='RelacaoRtc']"; 
	private static final String TEXT_CLASSE = "//*[@id='Classe']"; 
	private static final String TEXT_CLASSE_DE_EXATIDAO = "//*[@id='ClasseDeExatidao']"; 
	private static final String TEXT_QUANTIDADE_DE_ELEMENTOS = "//*[@id='QuantidadeDeElementos']";
	private static final String TEXT_QUANTIDADE_DE_FIOS = "//*[@id='QuantidadeDeFios']"; 
	private static final String TEXT_CONSTANTE_DE_INTEGRACAO = "//*[@id='ConstanteDaIntegracao']"; 
	private static final String TEXT_CONSTANTE_DE_ENERGIA_ATIVA = "//*[@id='ConstanteDoPulsoDeEnergiaAtiva']"; 
	private static final String TEXT_CONSTANTE_DE_ENERGIA_REATIVA = "//*[@id='ConstanteDoPulsoDeEnergiaReativa']"; 
	private static final String TEXT_CORRENTE_MAXIMA = "//*[@id='CorrenteMaxima']"; 
	private static final String TEXT_CORRENTE_NOMINAL = "//*[@id='CorrenteNominal']"; 
	private static final String TEXT_TENSAO_NOMINAL = "//*[@id='TensaoNominal']"; 
	private static final String TEXT_TENSAO_MAXIMA = "//*[@id='TensaoMaxima']"; 
	private static final String TEXT_FREQUENCIA = "//*[@id='Frequencia']"; 
	private static final String TEXT_FREQUENCIA_MAXIMA = "//*[@id='FrequenciaMaxima']"; 
	private static final String TEXT_CONSTANTE_PRN = "//*[@id='ConstantePrn']"; 
	private static final String TEXT_CODIGO_ELETROBRAS = "//*[@id='CodigoEletrobras']"; 
	private static final String TEXT_CONSTANTE_DE_COMBUSTIVEL = "//*[@id='ConstanteCombustivelEletrobras']"; 
	private static final String TEXT_NUMERO_DO_EQUIPAMENTO_SAP = "//*[@id='NumeroDoEquipamentoSap']"; 
	private static final String SELECT_TIPO_DE_LIGACAO = "//*[@id='TipoDeLigacaoSelecionado']";
	
	public void typeCheckBoxInformarNumeradorEDenominadorRtpRtc(String value){
		typeCheckOption(CHECK_INFORMAR_RTP_RTC, value);
	}
	
	public void typeTextNumeradorRTP(String value){
		waitForWebElementToBeClickable(TEXT_NUMERADOR_RTP);
		typeText(TEXT_NUMERADOR_RTP, value);
	}
		
	public String getNumeradorRTP(){
		return getValueFromElement(TEXT_NUMERADOR_RTP);
	}	
	
	public void typeTextNumeradorRTC(String value){
		typeText(TEXT_NUMERADOR_RTC, value);
	}	
	
	public String getNumeradorRTC(){
		return getValueFromElement(TEXT_NUMERADOR_RTC);
	}	
	
	public void typeTextDenominadorRTP(String value){
		typeText(TEXT_DENOMINADOR_RTP, value);
	}	
	
	public String getDenominadorRTP(){
		return getValueFromElement(TEXT_DENOMINADOR_RTP);
	}	
	
	public void typeTextDenominadorRTC(String value){
		typeText(TEXT_DENOMINADOR_RTC, value);
	}
	
	public String getDenominadorRTC(){
		return getValueFromElement(TEXT_DENOMINADOR_RTC);
	}		
	
	public void typeTextRelacaoRTP(String value){
		typeText(TEXT_RELACAO_RTP, value);
	}
		
	public String getRelacaoRTP(){
		return getValueFromElementUsingAttributeValue(TEXT_RELACAO_RTP);
	}		
	
	public void typeTextRelacaoRTC(String value){
		typeText(TEXT_RELACAO_RTC, value);
	}	
	
	public String getRelacaoRTC(){
		return getValueFromElementUsingAttributeValue(TEXT_RELACAO_RTC);
	}		
	
	public void typeTextClasse(String value){
		typeText(TEXT_CLASSE, value);
	}	
	
	public void typeTextClasseDeExatidao(String value){
		typeText(TEXT_CLASSE_DE_EXATIDAO, value);
	}	
	
	public void typeSelectQuantidadeDeElementos(String value){
		typeSelectComboOption(TEXT_QUANTIDADE_DE_ELEMENTOS, value);
	}	
	
	public void typeTextQuantidadeDeFios(String value){
		typeText(TEXT_QUANTIDADE_DE_FIOS, value);
	}	
	
	public void typeTextConstanteDeIntegracao(String value){
		typeText(TEXT_CONSTANTE_DE_INTEGRACAO, value);
	}	
	
	public void typeTextConstanteDeEnergiaAtiva(String value){
		typeText(TEXT_CONSTANTE_DE_ENERGIA_ATIVA, value);
	}	
	
	public void typeTextConstanteDeEnergiaReativa(String value){
		typeText(TEXT_CONSTANTE_DE_ENERGIA_REATIVA, value);
	}	
	
	public void typeTextCorrenteMaxima(String value){
		typeText(TEXT_CORRENTE_MAXIMA, value);
	}	
	
	public void typeTextCorrenteNominal(String value){
		typeText(TEXT_CORRENTE_NOMINAL, value);
	}	
	
	public void typeTextTensaoNominal(String value){
		typeText(TEXT_TENSAO_NOMINAL, value);
	}
	
	public void typeTextTensaoMaxima(String value){
		typeText(TEXT_TENSAO_MAXIMA, value);
	}	
	
	public void typeTextFrequencia(String value){
		typeText(TEXT_FREQUENCIA, value);
	}	
	
	public void typeTextFrequenciaMaxima(String value){
		typeText(TEXT_FREQUENCIA_MAXIMA, value);
	}
	
	public void typeTextConstantePRN(String value){
		typeText(TEXT_CONSTANTE_PRN, value);
	}	
	
	public void typeTextCodigoEletrobras(String value){
		typeText(TEXT_CODIGO_ELETROBRAS, value);
	}	
	
	public void typeTextConstanteDeCombustivel(String value){
		typeText(TEXT_CONSTANTE_DE_COMBUSTIVEL, value);
	}
	
	public void typeTextNumeroDoEquipamentoSAP(String value){
		typeText(TEXT_NUMERO_DO_EQUIPAMENTO_SAP, value);
	}
	
	public void typeSelectTipoDeLigacao(String value){
		typeSelectComboOption(SELECT_TIPO_DE_LIGACAO, value);
	}
	
	public void keyPageDown1(){
		useKey(TEXT_CLASSE, Keys.PAGE_DOWN);
	}	
	
	public void keyPageDown2(){
		useKey(TEXT_TENSAO_MAXIMA, Keys.PAGE_DOWN);
	}	
	
	public void keyPageDown3(){
		useKey(TEXT_CONSTANTE_DE_COMBUSTIVEL, Keys.PAGE_DOWN);
	}	
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
				
	public void clickAbaConfiguracoes(){
		clickOnElement(LINK_ABA_CONFIGURACOES);
	}
	
}