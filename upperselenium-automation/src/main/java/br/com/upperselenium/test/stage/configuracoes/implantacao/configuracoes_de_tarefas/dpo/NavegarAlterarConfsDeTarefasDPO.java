package br.com.upperselenium.test.stage.configuracoes.implantacao.configuracoes_de_tarefas.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarAlterarConfsDeTarefasDPO extends BaseHelperDPO {

	private String buttonReagendar;
	private String buttonExecutar;
	private String buttonHabilitar;

	public String getButtonReagendar() {
		return buttonReagendar;
	}

	public void setButtonReagendar(String buttonReagendar) {
		this.buttonReagendar = buttonReagendar;
	}

	public String getButtonExecutar() {
		return buttonExecutar;
	}

	public void setButtonExecutar(String buttonExecutar) {
		this.buttonExecutar = buttonExecutar;
	}

	public String getButtonHabilitar() {
		return buttonHabilitar;
	}

	public void setButtonHabilitar(String buttonHabilitar) {
		this.buttonHabilitar = buttonHabilitar;
	}

	@Override
	public String toString() {
		return "NavegarAlterarConfsDeTarefasDPO [buttonReagendar=" + buttonReagendar + ", buttonExecutar="
				+ buttonExecutar + ", buttonHabilitar=" + buttonHabilitar + "]";
	}

}