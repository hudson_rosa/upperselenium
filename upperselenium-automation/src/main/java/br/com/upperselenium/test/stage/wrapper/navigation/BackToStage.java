package br.com.upperselenium.test.stage.wrapper.navigation;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.wrapper.navigation.page.BackToPage;

public class BackToStage extends BaseStage {
	private BackToPage backToPage;
	
	@Override
	public void initMappedPages() {
		backToPage = initElementsFromPage(BackToPage.class);
	}
	
	@Override
	public void runStage() {
		back();		
	}

	private void back() {
		waitForPageToLoadUntil10s();
		backToPage.clickVoltar();
		waitForPageToLoadUntil10s();
	}

	@Override
	public void runValidations() {}

}
