package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.simcard.CadastrarSimcardStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.simcard.GoToSimcardStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0021SimcardCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0021-Simcard", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Simcard realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0021SimcardCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToSimcardStage());
		addStage(new CadastrarSimcardStage(getDP("CadastrarSimcardDP.json")));
	}	
}
