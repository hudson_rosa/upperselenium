package br.com.upperselenium.test.stage.configuracoes.cadastros.agentes.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoAgentesPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//form/fieldset/div[contains(@class, 'actions')]/input";
	private static final String TEXT_NOME = "//*[@id='Agente_Nome']";
	private static final String TEXT_DESCRICAO = "//*[@id='Agente_Descricao']";
		
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}

	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}
	
	public boolean isClickableButtonSalvar() {
		return isDisplayedElement(BUTTON_SALVAR);
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}

}