package br.com.upperselenium.test.stage.configuracoes.cadastros.curvas_tipicas.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class SalvarCurvasTipicasDPO extends BaseHelperDPO {

	private String filterPontoDeMedicao;
	private String filterTipoDePonto;
	private String filterPasta;
	private String filterSituacaoComercial;
	private String filterCriterioDeDados;
	private List<GridAlterarCurvasTipicasDPO> gridCurvasTipicas;

	public String getFilterPontoDeMedicao() {
		return filterPontoDeMedicao;
	}

	public void setFilterPontoDeMedicao(String filterPontoDeMedicao) {
		this.filterPontoDeMedicao = filterPontoDeMedicao;
	}

	public String getFilterTipoDePonto() {
		return filterTipoDePonto;
	}

	public void setFilterTipoDePonto(String filterTipoDePonto) {
		this.filterTipoDePonto = filterTipoDePonto;
	}

	public String getFilterPasta() {
		return filterPasta;
	}

	public void setFilterPasta(String filterPasta) {
		this.filterPasta = filterPasta;
	}

	public String getFilterSituacaoComercial() {
		return filterSituacaoComercial;
	}

	public void setFilterSituacaoComercial(String filterSituacaoComercial) {
		this.filterSituacaoComercial = filterSituacaoComercial;
	}

	public String getFilterCriterioDeDados() {
		return filterCriterioDeDados;
	}

	public void setFilterCriterioDeDados(String filterCriterioDeDados) {
		this.filterCriterioDeDados = filterCriterioDeDados;
	}

	public List<GridAlterarCurvasTipicasDPO> getGridCurvasTipicas() {
		return gridCurvasTipicas;
	}

	public void setGridCurvasTipicas(List<GridAlterarCurvasTipicasDPO> gridCurvasTipicas) {
		this.gridCurvasTipicas = gridCurvasTipicas;
	}

}