package br.com.upperselenium.test.stage.configuracoes.cadastros.cidades.page;

import org.openqa.selenium.By;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroCidadesPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//form/fieldset/div[5]/input";
	private static final String BUTTON_CANCELAR = ".//form/fieldset/div[5]/a";  
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String SELECT_ESTADO = "//*[@id='EstadoId']";
	private static final String SELECT_REGIAO = "//*[@id='RegiaoId']";
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	
	
	public boolean isClickableButtonSalvar() {
		return isDisplayedElement(By.xpath(BUTTON_SALVAR));
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
	public boolean isClickableButtonCancelar() {
		return isDisplayedElement(By.xpath(BUTTON_CANCELAR));
	}
	
	public void clickCancelar(){
		clickOnElement(BUTTON_CANCELAR);
	}
	
	public void typeSelectEstado(String value){		
		typeSelectComboOption(SELECT_ESTADO, value);
	}
	
	public void typeSelectRegiao(String value){		
		typeSelectComboOption(SELECT_REGIAO, value);
	}

}