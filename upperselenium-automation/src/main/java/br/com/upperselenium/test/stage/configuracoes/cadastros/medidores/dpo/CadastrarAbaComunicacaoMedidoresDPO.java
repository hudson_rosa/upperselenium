package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaComunicacaoMedidoresDPO extends BaseHelperDPO {

	private String selectComunicacao;
	private String checkHabilitar;
	private String textProtocolo;

	public String getSelectComunicacao() {
		return selectComunicacao;
	}

	public void setSelectComunicacao(String selectComunicacao) {
		this.selectComunicacao = selectComunicacao;
	}

	public String getCheckHabilitar() {
		return checkHabilitar;
	}

	public void setCheckHabilitar(String checkHabilitar) {
		this.checkHabilitar = checkHabilitar;
	}

	public String getTextProtocolo() {
		return textProtocolo;
	}

	public void setTextProtocolo(String textProtocolo) {
		this.textProtocolo = textProtocolo;
	}

}