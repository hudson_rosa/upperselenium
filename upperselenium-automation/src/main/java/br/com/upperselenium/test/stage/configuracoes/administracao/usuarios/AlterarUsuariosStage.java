package br.com.upperselenium.test.stage.configuracoes.administracao.usuarios;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.base.util.WaitElementUtil;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.dpo.AlterarUsuariosDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page.AlteracaoUsuariosPage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page.UsuariosPage;

public class AlterarUsuariosStage extends BaseStage {
	private AlterarUsuariosDPO alterarUsuariosDPO;
	private UsuariosPage usuariosPage;
	private AlteracaoUsuariosPage alteracaoUsuariosPage;
	
	public AlterarUsuariosStage(String dp) {
		alterarUsuariosDPO = loadDataProviderFile(AlterarUsuariosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		usuariosPage = initElementsFromPage(UsuariosPage.class);
		alteracaoUsuariosPage = initElementsFromPage(AlteracaoUsuariosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarUsuariosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		usuariosPage.typeTextFilterNome(alterarUsuariosDPO.getFilterNome()+getRandomStringNonSpaced());
		usuariosPage.typeTextFilterLogin(alterarUsuariosDPO.getFilterLogin()+getRandomStringNonSpaced());
		usuariosPage.typeTextFilterPerfil(alterarUsuariosDPO.getFilterPerfil());
		usuariosPage.typeTextFilterStatus(alterarUsuariosDPO.getFilterStatus());
		editFilteredRegister();
		alteracaoUsuariosPage.typeTextNome(alterarUsuariosDPO.getNome()+getRandomStringNonSpaced());
		alteracaoUsuariosPage.typeTextLogin(alterarUsuariosDPO.getLogin()+getRandomStringNonSpaced());
		alteracaoUsuariosPage.typeTextSenha(alterarUsuariosDPO.getSenha());
		alteracaoUsuariosPage.typeTextConfirmacao(alterarUsuariosDPO.getConfirmacao());
		alteracaoUsuariosPage.typeTextEmail(alterarUsuariosDPO.getEmail());
		alteracaoUsuariosPage.typeTextTelefone(alterarUsuariosDPO.getTelefone());
		alteracaoUsuariosPage.keyPageDown();
		alteracaoUsuariosPage.typeSelectPerfil(alterarUsuariosDPO.getPerfil());
		alteracaoUsuariosPage.typeSelectPasta(alterarUsuariosDPO.getPasta());
		alteracaoUsuariosPage.typeCheckBloqueado(alterarUsuariosDPO.getBloqueado());
		alteracaoUsuariosPage.clickToggleToken();
		if(StringUtil.isNotBlankOrNotNull(alterarUsuariosDPO.getGenerateToken())){
			alteracaoUsuariosPage.clickGerar();
		}
		alteracaoUsuariosPage.clickSalvar();
	}

	private void editFilteredRegister() {
		for (int index=0; index < alterarUsuariosDPO.getGridUsuarios().size(); index++){
			usuariosPage.getGridLabelItemNome(alterarUsuariosDPO.getGridUsuarios().get(index).getNome()+getRandomStringNonSpaced(), index+1);
			usuariosPage.getGridLabelItemLogin(alterarUsuariosDPO.getGridUsuarios().get(index).getLogin()+getRandomStringNonSpaced(), index+1);
			usuariosPage.getGridLabelItemPerfil(alterarUsuariosDPO.getGridUsuarios().get(index).getPerfil(), index+1);
			usuariosPage.clickGridButtonFirstLineEditar(alterarUsuariosDPO.getGridUsuarios().get(index).getNome());
			WaitElementUtil.waitForATime(TimePRM._3_SECS);
		}
	}

	@Override
	public void runValidations() {
		alteracaoUsuariosPage.validateMessageDefault(alterarUsuariosDPO.getOperationMessage());
	}

}
