package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class PontosPage extends BaseHelperPage{
	
	private static final String LINK_NOVO = ".//div[2]/div/div[2]/ul[2]/li[1]/a";
	private static final String LINK_NOVO_VIRTUAL = ".//div[2]/div/div[2]/ul[2]/li[2]/a";
	private static final String LINK_EXCLUIDOS = ".//div[2]/div/div[2]/ul[2]/li[3]/a";
				
	public boolean isClickableLinkNovo() {
		return isDisplayedElement(LINK_NOVO);
	}
		
	public void clickNovo(){
		clickOnElement(LINK_NOVO);
		waitForPageToLoadUntil10s();
	}
	
	public boolean isClickableLinkNovoVirtual() {
		return isDisplayedElement(LINK_NOVO_VIRTUAL);
	}
	
	public void clickNovoVirtual(){
		clickOnElement(LINK_NOVO_VIRTUAL);
		waitForPageToLoadUntil10s();
	}
	
	public boolean isClickableLinkExcluidos() {
		return isDisplayedElement(LINK_EXCLUIDOS);
	}
	
	public void clickExcluidos(){
		clickOnElement(LINK_EXCLUIDOS);
		waitForPageToLoadUntil10s();
	}	

}