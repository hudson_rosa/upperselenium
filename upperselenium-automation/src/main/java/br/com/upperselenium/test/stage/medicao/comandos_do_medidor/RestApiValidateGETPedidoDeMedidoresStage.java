package br.com.upperselenium.test.stage.medicao.comandos_do_medidor;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasItems;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo.NavegarListagemComandosDoMedidorDPO;
import br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo.RestApiAuthDPO;
import io.restassured.http.ContentType;

public class RestApiValidateGETPedidoDeMedidoresStage extends BaseStage {

	private RestApiAuthDPO restApiDpo;
	
	public RestApiValidateGETPedidoDeMedidoresStage(String dp) {
		restApiDpo = loadDataProviderFile(RestApiAuthDPO.class, dp);
	}

	@Override
	public void initMappedPages() {
	}

	@Override
	public void runStage() {
		waitForPageToLoadUntil10s();
		String uri = "http://192.168.0.149/pim/api/pedidos-de-medidores";
		int status = 200;
		given().body("7121c992-1fb4-465a-8844-1c9950015100")
//		given().relaxedHTTPSValidation()
//			.param("TipoDeDriver", 18)
//		.when()
			.get(uri)
		.then()
			.statusCode(status)
			.header("Server", "Microsoft-IIS/10.0").contentType(ContentType.JSON)
			.body("Data.NumeroDeSerie", hasItems("medteste2GrupoA", "medteste1MedCCEE"))
//			.body("Data.TipoDeDriver", hasItems("18", "18"))
//			.body("Data.NomeDaComunicacao", hasItems("Bancada virtual Way2", "Bancada virtual Way2"))
//			.body("Data.ParametrosDeComunicacao.Valor", hasItems("pmv1msdn1.southcentralus.cloudapp.azure.com", "pmv1msdn1.southcentralus.cloudapp.azure.com"))
			.assertThat();
	}

	@Override
	public void runValidations() {}

}
