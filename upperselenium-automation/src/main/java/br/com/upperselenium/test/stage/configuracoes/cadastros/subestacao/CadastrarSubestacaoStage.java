package br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.dpo.CadastrarSubestacaoDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.page.CadastroSubestacaoPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.page.SubestacaoPage;

public class CadastrarSubestacaoStage extends BaseStage {
	private CadastrarSubestacaoDPO cadastrarSubestacaoDPO;
	private SubestacaoPage subestacaoPage;
	private CadastroSubestacaoPage cadastroSubestacaoPage;
	
	public CadastrarSubestacaoStage(String dp) {
		cadastrarSubestacaoDPO = loadDataProviderFile(CadastrarSubestacaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		subestacaoPage = initElementsFromPage(SubestacaoPage.class);
		cadastroSubestacaoPage = initElementsFromPage(CadastroSubestacaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarSubestacaoDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		subestacaoPage.clickNova();
		cadastroSubestacaoPage.typeTextNome(cadastrarSubestacaoDPO.getNome()+getRandomStringSpaced());		
		cadastroSubestacaoPage.typeTextDescricao(cadastrarSubestacaoDPO.getDescricao()+getRandomStringSpaced());		
		cadastroSubestacaoPage.typeTextQuantidadeDePontos(cadastrarSubestacaoDPO.getQuantidadeDePontos());		
		cadastroSubestacaoPage.typeTextAlimentacaoAuxiliar(cadastrarSubestacaoDPO.getAlimentacaoAuxiliar());		
		cadastroSubestacaoPage.typeTextObservacoes(cadastrarSubestacaoDPO.getObservacoes());		
		cadastroSubestacaoPage.typeTextPropriedade(cadastrarSubestacaoDPO.getPropriedade());	
		cadastroSubestacaoPage.keyPageDown();	
		cadastroSubestacaoPage.typeTextFronteira(cadastrarSubestacaoDPO.getFronteira());		
		cadastroSubestacaoPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroSubestacaoPage.validateMessageDefault(cadastrarSubestacaoDPO.getOperationMessage());
	}

}
