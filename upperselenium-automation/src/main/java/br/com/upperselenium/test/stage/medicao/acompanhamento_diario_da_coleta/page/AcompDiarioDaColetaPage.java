package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_da_coleta.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AcompDiarioDaColetaPage extends BaseHelperPage{
	
	private static final String LABEL_PASTA = ".//filtro-acompanhamento-diario-coleta/section/form/div[1]/label";
	private static final String LABEL_TIPO_DE_VISUALIZACAO = ".//filtro-acompanhamento-diario-coleta/section/form/div[2]/label";
	private static final String LABEL_MES = ".//filtro-acompanhamento-diario-coleta/section/form/div[3]/date-picker/div/label";
	private static final String LABEL_STATUS = ".//filtro-acompanhamento-diario-coleta/section/form/div[4]/label";
	private static final String LABEL_CATION_VAZIO = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/div[1]/span[2]";
	private static final String LABEL_CATION_PARCIAL = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/div[1]/span[4]";
	private static final String LABEL_CATION_COMPLETO = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/div[1]/span[6]";
	private static final String LABEL_HEADER_NOME = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[1]/span";
	private static final String LABEL_HEADER_NUMERO_DE_SERIE = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[2]/span";
	private static final String LABEL_HEADER_CODIGO_SCDE = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[3]/span";
	private static final String LABEL_HEADER_FUNCAO = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[4]/span";
	private static final String LABEL_1 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[5]";
	private static final String LABEL_2 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[6]";
	private static final String LABEL_3 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[7]";
	private static final String LABEL_4 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[8]";
	private static final String LABEL_5 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[9]";
	private static final String LABEL_6 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[10]";
	private static final String LABEL_7 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[11]";
	private static final String LABEL_8 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[12]";
	private static final String LABEL_9 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[13]";
	private static final String LABEL_10 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[14]";
	private static final String LABEL_11 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[15]";
	private static final String LABEL_12 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[16]";
	private static final String LABEL_13 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[17]";
	private static final String LABEL_14 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[18]";
	private static final String LABEL_15 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[19]";
	private static final String LABEL_16 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[20]";
	private static final String LABEL_17 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[21]";
	private static final String LABEL_18 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[22]";
	private static final String LABEL_19 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[23]";
	private static final String LABEL_20 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[24]";
	private static final String LABEL_21 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[25]";
	private static final String LABEL_22 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[26]";
	private static final String LABEL_23 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[27]";
	private static final String LABEL_24 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[28]";
	private static final String LABEL_25 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[29]";
	private static final String LABEL_26 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[30]";
	private static final String LABEL_27 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[31]";
	private static final String LABEL_28 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[32]";
	private static final String LABEL_29 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[33]";
	private static final String LABEL_30 = "//*[@id='acompanhamento-diario-coleta-dados-medidor']/table/thead/tr[2]/th[34]";
	
	public void getLabelPasta(String value){
		getLabel(LABEL_PASTA, value);
	}
	
	public void getLabelTipoDeVisualizacao(String value){
		getLabel(LABEL_TIPO_DE_VISUALIZACAO, value);
	}
	
	public void getLabelMes(String value){
		getLabel(LABEL_MES, value);
	}
	
	public void getLabelStatus(String value){
		getLabel(LABEL_STATUS, value);
	}
	
	public void getLabelVazio(String value){
		getLabel(LABEL_CATION_VAZIO, value);
	}
	
	public void getLabelParcial(String value){
		getLabel(LABEL_CATION_PARCIAL, value);
	}
	
	public void getLabelCompleto(String value){
		getLabel(LABEL_CATION_COMPLETO, value);
	}
	
	public void getLabelNome(String value){
		getLabel(LABEL_HEADER_NOME, value);
	}
	
	public void getLabelNumeroDeSerie(String value){
		getLabel(LABEL_HEADER_NUMERO_DE_SERIE, value);
	}
	
	public void getLabelCodigoScde(String value){
		getLabel(LABEL_HEADER_CODIGO_SCDE, value);
	}
	
	public void getLabelFuncao(String value){
		getLabel(LABEL_HEADER_FUNCAO, value);
	}
	
	public void getLabel01(String value){
		getLabel(LABEL_1, value);
	}
	
	public void getLabel02(String value){
		getLabel(LABEL_2, value);
	}
	
	public void getLabel03(String value){
		getLabel(LABEL_3, value);
	}
	
	public void getLabel04(String value){
		getLabel(LABEL_4, value);
	}
	
	public void getLabel05(String value){
		getLabel(LABEL_5, value);
	}
	
	public void getLabel06(String value){
		getLabel(LABEL_6, value);
	}
	
	public void getLabel07(String value){
		getLabel(LABEL_7, value);
	}
	
	public void getLabel08(String value){
		getLabel(LABEL_8, value);
	}
	
	public void getLabel09(String value){
		getLabel(LABEL_9, value);
	}
	
	public void getLabel10(String value){
		getLabel(LABEL_10, value);
	}
	
	public void getLabel11(String value){
		getLabel(LABEL_11, value);
	}
	
	public void getLabel12(String value){
		getLabel(LABEL_12, value);
	}
	
	public void getLabel13(String value){
		getLabel(LABEL_13, value);
	}
	
	public void getLabel14(String value){
		getLabel(LABEL_14, value);
	}
	
	public void getLabel15(String value){
		getLabel(LABEL_15, value);
	}
	
	public void getLabel16(String value){
		getLabel(LABEL_16, value);
	}
	
	public void getLabel17(String value){
		getLabel(LABEL_17, value);
	}
	
	public void getLabel18(String value){
		getLabel(LABEL_18, value);
	}
	
	public void getLabel19(String value){
		getLabel(LABEL_19, value);
	}
	
	public void getLabel20(String value){
		getLabel(LABEL_20, value);
	}
	
	public void getLabel21(String value){
		getLabel(LABEL_21, value);
	}
	
	public void getLabel22(String value){
		getLabel(LABEL_22, value);
	}
	
	public void getLabel23(String value){
		getLabel(LABEL_23, value);
	}
	
	public void getLabel24(String value){
		getLabel(LABEL_24, value);
	}
	
	public void getLabel25(String value){
		getLabel(LABEL_25, value);
	}
	
	public void getLabel26(String value){
		getLabel(LABEL_26, value);
	}
	
	public void getLabel27(String value){
		getLabel(LABEL_27, value);
	}
	
	public void getLabel28(String value){
		getLabel(LABEL_28, value);
	}
	
	public void getLabel29(String value){
		getLabel(LABEL_29, value);
	}
	
	public void getLabel30(String value){
		getLabel(LABEL_30, value);
	}

}