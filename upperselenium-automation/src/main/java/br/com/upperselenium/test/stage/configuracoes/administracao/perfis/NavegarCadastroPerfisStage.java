package br.com.upperselenium.test.stage.configuracoes.administracao.perfis;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.dpo.NavegarCadastroPerfisDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.page.CadastroPerfisPage;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.page.PerfisPage;

public class NavegarCadastroPerfisStage extends BaseStage {
	private NavegarCadastroPerfisDPO navegarCadastroPerfisDPO;
	private PerfisPage perfisPage;
	private CadastroPerfisPage cadastroPerfisPage;
	
	public NavegarCadastroPerfisStage(String dp) {
		navegarCadastroPerfisDPO = loadDataProviderFile(NavegarCadastroPerfisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		perfisPage = initElementsFromPage(PerfisPage.class);
		cadastroPerfisPage = initElementsFromPage(CadastroPerfisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarCadastroPerfisDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		perfisPage.clickNovo();
		cadastroPerfisPage.getBreadcrumbsText(navegarCadastroPerfisDPO.getBreadcrumbs());
		cadastroPerfisPage.clickUpToBreadcrumbs(navegarCadastroPerfisDPO.getUpToBreadcrumb());
		cadastroPerfisPage.getWelcomeTitle(navegarCadastroPerfisDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
