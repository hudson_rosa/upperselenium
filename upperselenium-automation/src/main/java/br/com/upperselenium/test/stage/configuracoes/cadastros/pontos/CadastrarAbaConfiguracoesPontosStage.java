package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarAbaConfiguracoesPontosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastroAbaConfiguracoesPontosPage;

public class CadastrarAbaConfiguracoesPontosStage extends BaseStage {
	private CadastrarAbaConfiguracoesPontosDPO cadastrarAbaConfiguracoesPontosDPO;
	private CadastroAbaConfiguracoesPontosPage cadastroAbaConfiguracoesPontosPage;
	
	public CadastrarAbaConfiguracoesPontosStage(String dp) {
		cadastrarAbaConfiguracoesPontosDPO = loadDataProviderFile(CadastrarAbaConfiguracoesPontosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaConfiguracoesPontosPage = initElementsFromPage(CadastroAbaConfiguracoesPontosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaConfiguracoesPontosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaConfiguracoesPontosPage.clickAbaConfiguracoes();
		FindElementUtil.acceptAlert();
		cadastroAbaConfiguracoesPontosPage.typeSelectQuadrante(cadastrarAbaConfiguracoesPontosDPO.getQuadrante());
		cadastroAbaConfiguracoesPontosPage.typeSelectEnergiaDelEnergiaRec(cadastrarAbaConfiguracoesPontosDPO.getEnergiaDelEnergiaRec());
		cadastroAbaConfiguracoesPontosPage.typeSelectSituacaoComercial(cadastrarAbaConfiguracoesPontosDPO.getSituacaoComercial());
		cadastroAbaConfiguracoesPontosPage.typeSelectMedicaoA(cadastrarAbaConfiguracoesPontosDPO.getMedicaoA());
		cadastroAbaConfiguracoesPontosPage.typeSelectTensaoDeMedicao(cadastrarAbaConfiguracoesPontosDPO.getTensaoDeMedicao());
		cadastroAbaConfiguracoesPontosPage.typeTextLimiteDePotenciaDeConsumo(cadastrarAbaConfiguracoesPontosDPO.getLimiteDePotenciaDeConsumo());
		cadastroAbaConfiguracoesPontosPage.keyPageDown();
		cadastroAbaConfiguracoesPontosPage.typeTextLimiteDePotenciaDeGeracao(cadastrarAbaConfiguracoesPontosDPO.getLimiteDePotenciaDeGeracao());
		cadastroAbaConfiguracoesPontosPage.typeTextConexao(cadastrarAbaConfiguracoesPontosDPO.getConexao());
		cadastroAbaConfiguracoesPontosPage.typeTextCapacidadeDeConexao(cadastrarAbaConfiguracoesPontosDPO.getCapacidadeDeConexao());
		cadastroAbaConfiguracoesPontosPage.typeTextDemandaDimensionada(cadastrarAbaConfiguracoesPontosDPO.getDemandaDimensionada());
		cadastroAbaConfiguracoesPontosPage.typeCheckBoxCompensacaoDePerdas(cadastrarAbaConfiguracoesPontosDPO.getCompensacaoDePerdas());
		cadastroAbaConfiguracoesPontosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaConfiguracoesPontosPage.validateMessageDefault(cadastrarAbaConfiguracoesPontosDPO.getOperationMessage());
	}

}
