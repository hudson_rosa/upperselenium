package br.com.upperselenium.test.stage.configuracoes.administracao.usuarios;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.dpo.NavegarAlteracaoUsuariosDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page.AlteracaoUsuariosPage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.page.UsuariosPage;

public class NavegarAlteracaoUsuariosStage extends BaseStage {
	private NavegarAlteracaoUsuariosDPO navegarAlteracaoUsuariosDPO;
	private UsuariosPage usuariosPage;
	private AlteracaoUsuariosPage alteracaoUsuariosPage;
	
	public NavegarAlteracaoUsuariosStage(String dp) {
		navegarAlteracaoUsuariosDPO = loadDataProviderFile(NavegarAlteracaoUsuariosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		usuariosPage = initElementsFromPage(UsuariosPage.class);
		alteracaoUsuariosPage = initElementsFromPage(AlteracaoUsuariosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAlteracaoUsuariosDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		usuariosPage.clickGridButtonFirstLineEditar(navegarAlteracaoUsuariosDPO.getClickItem());
		alteracaoUsuariosPage.getBreadcrumbsText(navegarAlteracaoUsuariosDPO.getBreadcrumbs());
		alteracaoUsuariosPage.clickUpToBreadcrumbs(navegarAlteracaoUsuariosDPO.getUpToBreadcrumb());
		alteracaoUsuariosPage.getWelcomeTitle(navegarAlteracaoUsuariosDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
