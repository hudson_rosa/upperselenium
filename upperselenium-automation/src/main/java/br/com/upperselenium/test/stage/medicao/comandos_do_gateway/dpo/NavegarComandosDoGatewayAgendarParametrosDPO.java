package br.com.upperselenium.test.stage.medicao.comandos_do_gateway.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarComandosDoGatewayAgendarParametrosDPO extends BaseHelperDPO {

	private String labelTipo;
	private String labelInstante;
	private String labelExpiracao;
	private String labelDigiteUmOuMaisTags;
	private String labelRepeticao;

	public String getLabelTipo() {
		return labelTipo;
	}

	public void setLabelTipo(String labelTipo) {
		this.labelTipo = labelTipo;
	}

	public String getLabelInstante() {
		return labelInstante;
	}

	public void setLabelInstante(String labelInstante) {
		this.labelInstante = labelInstante;
	}

	public String getLabelExpiracao() {
		return labelExpiracao;
	}

	public void setLabelExpiracao(String labelExpiracao) {
		this.labelExpiracao = labelExpiracao;
	}

	public String getLabelDigiteUmOuMaisTags() {
		return labelDigiteUmOuMaisTags;
	}

	public void setLabelDigiteUmOuMaisTags(String labelDigiteUmOuMaisTags) {
		this.labelDigiteUmOuMaisTags = labelDigiteUmOuMaisTags;
	}

	public String getLabelRepeticao() {
		return labelRepeticao;
	}

	public void setLabelRepeticao(String labelRepeticao) {
		this.labelRepeticao = labelRepeticao;
	}

	@Override
	public String toString() {
		return "NavegarComandosDoGatewayAgendarParametrosDPO [labelTipo=" + labelTipo + ", labelInstante="
				+ labelInstante + ", labelExpiracao=" + labelExpiracao + ", labelDigiteUmOuMaisTags="
				+ labelDigiteUmOuMaisTags + ", labelRepeticao=" + labelRepeticao + "]";
	}

}