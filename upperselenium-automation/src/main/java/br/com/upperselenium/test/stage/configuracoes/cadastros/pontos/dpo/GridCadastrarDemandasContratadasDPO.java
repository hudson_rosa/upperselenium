package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

public class GridCadastrarDemandasContratadasDPO {

	private String flagItem;
	private String filterInicioDaVigencia;
	private String filterLimitePonta;
	private String filterValorPonta;
	private String filterLimiteForaPonta;
	private String filterValorForaPonta;
	private String filterTipo;
	private String inicioDaVigencia;
	private String limitePonta;
	private String limiteForaPonta;
	private String valorPonta;
	private String valorForaPonta;
	private String tipo;

	public String getFlagItem() {
		return flagItem;
	}

	public void setFlagItem(String flagItem) {
		this.flagItem = flagItem;
	}

	public String getFilterInicioDaVigencia() {
		return filterInicioDaVigencia;
	}

	public void setFilterInicioDaVigencia(String filterInicioDaVigencia) {
		this.filterInicioDaVigencia = filterInicioDaVigencia;
	}

	public String getFilterLimitePonta() {
		return filterLimitePonta;
	}

	public void setFilterLimitePonta(String filterLimitePonta) {
		this.filterLimitePonta = filterLimitePonta;
	}

	public String getFilterValorPonta() {
		return filterValorPonta;
	}

	public void setFilterValorPonta(String filterValorPonta) {
		this.filterValorPonta = filterValorPonta;
	}

	public String getFilterLimiteForaPonta() {
		return filterLimiteForaPonta;
	}

	public void setFilterLimiteForaPonta(String filterLimiteForaPonta) {
		this.filterLimiteForaPonta = filterLimiteForaPonta;
	}

	public String getFilterValorForaPonta() {
		return filterValorForaPonta;
	}

	public void setFilterValorForaPonta(String filterValorForaPonta) {
		this.filterValorForaPonta = filterValorForaPonta;
	}

	public String getFilterTipo() {
		return filterTipo;
	}

	public void setFilterTipo(String filterTipo) {
		this.filterTipo = filterTipo;
	}

	public String getInicioDaVigencia() {
		return inicioDaVigencia;
	}

	public void setInicioDaVigencia(String inicioDaVigencia) {
		this.inicioDaVigencia = inicioDaVigencia;
	}

	public String getLimitePonta() {
		return limitePonta;
	}

	public void setLimitePonta(String limitePonta) {
		this.limitePonta = limitePonta;
	}

	public String getLimiteForaPonta() {
		return limiteForaPonta;
	}

	public void setLimiteForaPonta(String limiteForaPonta) {
		this.limiteForaPonta = limiteForaPonta;
	}

	public String getValorPonta() {
		return valorPonta;
	}

	public void setValorPonta(String valorPonta) {
		this.valorPonta = valorPonta;
	}

	public String getValorForaPonta() {
		return valorForaPonta;
	}

	public void setValorForaPonta(String valorForaPonta) {
		this.valorForaPonta = valorForaPonta;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
