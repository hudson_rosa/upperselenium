package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.CadastrarAbaPastasSecundariasClientsSCDEStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.CadastrarAbaPontosClientsSCDEStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.CadastrarClientsSCDEStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.GoToClientsSCDEStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0008ClientsSCDECadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0008-ClientsSCDE", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Clients SCDE realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0008ClientsSCDECadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToClientsSCDEStage());
		addStage(new CadastrarClientsSCDEStage(getDP("CadastrarClientsSCDEDP.json")));
		addStage(new CadastrarAbaPontosClientsSCDEStage(getDP("CadastrarAbaPontosClientsSCDEDP.json")));
		addStage(new CadastrarAbaPastasSecundariasClientsSCDEStage(getDP("CadastrarAbaPastasSecundariasClientsSCDEDP.json")));
	}	
}
