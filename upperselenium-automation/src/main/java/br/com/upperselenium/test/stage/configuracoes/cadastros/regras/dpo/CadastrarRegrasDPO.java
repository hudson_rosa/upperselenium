package br.com.upperselenium.test.stage.configuracoes.cadastros.regras.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarRegrasDPO extends BaseHelperDPO {

	private String nome;
	private String pasta;
	private String formula;
	private List<GridCadastrarRegrasDPO> gridRegras;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPasta() {
		return pasta;
	}

	public void setPasta(String pasta) {
		this.pasta = pasta;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public List<GridCadastrarRegrasDPO> getGridRegras() {
		return gridRegras;
	}

	public void setGridRegras(List<GridCadastrarRegrasDPO> gridRegras) {
		this.gridRegras = gridRegras;
	}

}