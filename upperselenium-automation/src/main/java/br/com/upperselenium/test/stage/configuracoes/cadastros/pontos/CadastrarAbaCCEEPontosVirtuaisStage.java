package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarAbaCCEEPontosVirtuaisDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastroAbaCCEEPontosVirtuaisPage;

public class CadastrarAbaCCEEPontosVirtuaisStage extends BaseStage {
	private CadastrarAbaCCEEPontosVirtuaisDPO cadastrarAbaCCEEPontosVirtuaisDPO;
	private CadastroAbaCCEEPontosVirtuaisPage cadastroAbaCCEEPontosVirtuaisPage;
	
	public CadastrarAbaCCEEPontosVirtuaisStage(String dp) {
		cadastrarAbaCCEEPontosVirtuaisDPO = loadDataProviderFile(CadastrarAbaCCEEPontosVirtuaisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaCCEEPontosVirtuaisPage = initElementsFromPage(CadastroAbaCCEEPontosVirtuaisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaCCEEPontosVirtuaisDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaCCEEPontosVirtuaisPage.clickAbaCCEE();
		FindElementUtil.acceptAlert();
		cadastroAbaCCEEPontosVirtuaisPage.typeTextCodigoCCEE(cadastrarAbaCCEEPontosVirtuaisDPO.getCodigoCCEE()+getRandomStringNonSpaced());
		cadastroAbaCCEEPontosVirtuaisPage.typeTextCodigoDoAtivo(cadastrarAbaCCEEPontosVirtuaisDPO.getCodigoDoAtivo()+getRandomStringNonSpaced());
		cadastroAbaCCEEPontosVirtuaisPage.typeTextNomeDoAtivo(cadastrarAbaCCEEPontosVirtuaisDPO.getNomeDoAtivo()+getRandomStringNonSpaced());
		cadastroAbaCCEEPontosVirtuaisPage.typeSelectAgenteConectante(cadastrarAbaCCEEPontosVirtuaisDPO.getAgenteConectante());
		cadastroAbaCCEEPontosVirtuaisPage.typeSelectAgenteConectado(cadastrarAbaCCEEPontosVirtuaisDPO.getAgenteConectado());
		cadastroAbaCCEEPontosVirtuaisPage.keyPageDown();
		cadastroAbaCCEEPontosVirtuaisPage.typeSelectAgenteDeMedicao(cadastrarAbaCCEEPontosVirtuaisDPO.getAgenteDeMedicao());
		cadastroAbaCCEEPontosVirtuaisPage.typeSelectTipo(cadastrarAbaCCEEPontosVirtuaisDPO.getTipo());
		cadastroAbaCCEEPontosVirtuaisPage.typeTextCodigoDoAgente(cadastrarAbaCCEEPontosVirtuaisDPO.getCodigoDoAgente()+getRandomStringNonSpaced());
		cadastroAbaCCEEPontosVirtuaisPage.typeTextNomeDoAgente(cadastrarAbaCCEEPontosVirtuaisDPO.getNomeDoAgente()+getRandomStringSpaced());
		cadastroAbaCCEEPontosVirtuaisPage.typeSelectClientScde(cadastrarAbaCCEEPontosVirtuaisDPO.getClientScde());
		cadastroAbaCCEEPontosVirtuaisPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaCCEEPontosVirtuaisPage.validateMessageDefault(cadastrarAbaCCEEPontosVirtuaisDPO.getOperationMessage());	
	}

}
