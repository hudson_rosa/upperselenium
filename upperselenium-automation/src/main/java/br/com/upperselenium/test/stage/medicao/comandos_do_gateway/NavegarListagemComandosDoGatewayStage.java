package br.com.upperselenium.test.stage.medicao.comandos_do_gateway;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.dpo.NavegarListagemComandosDoGatewayDPO;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.page.ComandosDoGatewayPage;

public class NavegarListagemComandosDoGatewayStage extends BaseStage {
	private NavegarListagemComandosDoGatewayDPO navegarComandosDoGatewayDPO;
	private ComandosDoGatewayPage comandosDoGatewayPage;	
	
	public NavegarListagemComandosDoGatewayStage(String dp) {
		navegarComandosDoGatewayDPO = loadDataProviderFile(NavegarListagemComandosDoGatewayDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		comandosDoGatewayPage = initElementsFromPage(ComandosDoGatewayPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarComandosDoGatewayDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		comandosDoGatewayPage.getLabelFiltrarPelaDataDaProximaExecucao(navegarComandosDoGatewayDPO.getLabelFiltrarDataProximaExecucao());
		comandosDoGatewayPage.clickButtonFiltrar(navegarComandosDoGatewayDPO.getClickFiltrar());
		if(StringUtil.isNotBlankOrNotNull(navegarComandosDoGatewayDPO.getClickFiltrar())){
			comandosDoGatewayPage.getLabelHeaderGatewaysTags(navegarComandosDoGatewayDPO.getLabelHeaderGatewaysTags());
			comandosDoGatewayPage.getLabelHeaderInsercao(navegarComandosDoGatewayDPO.getLabelHeaderInsercao());
			comandosDoGatewayPage.getLabelHeaderProximaExecucao(navegarComandosDoGatewayDPO.getLabelHeaderProximaExecucao());
			comandosDoGatewayPage.getLabelHeaderDescricao(navegarComandosDoGatewayDPO.getLabelHeaderDescricao());
			comandosDoGatewayPage.getLabelHeaderUltimasExecucoes(navegarComandosDoGatewayDPO.getLabelHeaderUltimasExecucoes());
			comandosDoGatewayPage.getLabelHeaderExpiracao(navegarComandosDoGatewayDPO.getLabelHeaderExpiracao());
		}
		comandosDoGatewayPage.getBreadcrumbsText(navegarComandosDoGatewayDPO.getBreadcrumbs());
		comandosDoGatewayPage.clickUpToBreadcrumbs(navegarComandosDoGatewayDPO.getUpToBreadcrumb());
		comandosDoGatewayPage.getWelcomeTitle(navegarComandosDoGatewayDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
