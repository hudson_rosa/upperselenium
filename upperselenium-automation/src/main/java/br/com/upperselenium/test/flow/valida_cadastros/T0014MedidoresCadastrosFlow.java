package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.AlterarAbaColetasMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.AlterarAbaConstantesMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarAbaAtivosMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarAbaCCEEMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarAbaComunicacaoMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarAbaConfiguracoesMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarAbaInspecaoLogicaMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarAbaSincronismoMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.CadastrarMedidoresStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.GoToMedidoresStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;
import br.com.upperselenium.test.stage.wrapper.navigation.MockUrlStage;

@SuiteClasses(T0014MedidoresCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0014-Medidores", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Medidores realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0014MedidoresCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToMedidoresStage());
		addStage(new CadastrarMedidoresStage(getDP("CadastrarMedidoresDP.json")));
		addStage(new CadastrarAbaConfiguracoesMedidoresStage(getDP("CadastrarAbaConfiguracoesMedidoresDP.json")));
		addStage(new CadastrarAbaComunicacaoMedidoresStage(getDP("CadastrarAbaComunicacaoMedidoresDP.json")));
		addStage(new CadastrarAbaCCEEMedidoresStage(getDP("CadastrarAbaCCEEMedidoresDP.json")));
		addStage(new CadastrarAbaAtivosMedidoresStage(getDP("CadastrarAbaAtivosMedidoresDP.json")));
		addStage(new CadastrarAbaSincronismoMedidoresStage(getDP("CadastrarAbaSincronismoMedidoresDP.json")));
		addStage(new CadastrarAbaInspecaoLogicaMedidoresStage(getDP("CadastrarAbaInspecaoLogicaMedidoresDP.json")));
		
//			addStage(new MockUrlStage("http://localhost/pim/AreaCadastros/MedidorColetas/Edit/101"));
//			addStage(new AlterarAbaColetasMedidoresStage(getDP("AlterarAbaColetasMedidoresDP.json")));
//			addStage(new AlterarAbaConstantesMedidoresStage(getDP("AlterarAbaConstantesMedidoresDP.json")));
	}	
}
