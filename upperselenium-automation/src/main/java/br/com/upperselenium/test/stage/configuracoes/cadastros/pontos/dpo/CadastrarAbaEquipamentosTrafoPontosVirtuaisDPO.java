package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaEquipamentosTrafoPontosVirtuaisDPO extends BaseHelperDPO {

	private String tipo;
	private String anoDeFabricacao;
	private String descricao;
	private String numeroDeSerie;
	private String codigoInterno;
	private String propriedade;
	private String potenciaONAN;
	private String potenciaONAF;
	private String potenciaNominal;
	private String utilizacaoONAF;
	private String tensaoPrimaria;
	private String tensaoSecundaria;
	private String perdasNoFerro;
	private String perdasNoCobre;
	private String impedancia;
	private String exitacao;
	private String tapAcima;
	private String tapAbaixo;
	private String medicaoNaAlta;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getAnoDeFabricacao() {
		return anoDeFabricacao;
	}

	public void setAnoDeFabricacao(String anoDeFabricacao) {
		this.anoDeFabricacao = anoDeFabricacao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNumeroDeSerie() {
		return numeroDeSerie;
	}

	public void setNumeroDeSerie(String numeroDeSerie) {
		this.numeroDeSerie = numeroDeSerie;
	}

	public String getCodigoInterno() {
		return codigoInterno;
	}

	public void setCodigoInterno(String codigoInterno) {
		this.codigoInterno = codigoInterno;
	}

	public String getPropriedade() {
		return propriedade;
	}

	public void setPropriedade(String propriedade) {
		this.propriedade = propriedade;
	}

	public String getPotenciaONAN() {
		return potenciaONAN;
	}

	public void setPotenciaONAN(String potenciaONAN) {
		this.potenciaONAN = potenciaONAN;
	}

	public String getPotenciaONAF() {
		return potenciaONAF;
	}

	public void setPotenciaONAF(String potenciaONAF) {
		this.potenciaONAF = potenciaONAF;
	}

	public String getPotenciaNominal() {
		return potenciaNominal;
	}

	public void setPotenciaNominal(String potenciaNominal) {
		this.potenciaNominal = potenciaNominal;
	}

	public String getUtilizacaoONAF() {
		return utilizacaoONAF;
	}

	public void setUtilizacaoONAF(String utilizacaoONAF) {
		this.utilizacaoONAF = utilizacaoONAF;
	}

	public String getTensaoPrimaria() {
		return tensaoPrimaria;
	}

	public void setTensaoPrimaria(String tensaoPrimaria) {
		this.tensaoPrimaria = tensaoPrimaria;
	}

	public String getTensaoSecundaria() {
		return tensaoSecundaria;
	}

	public void setTensaoSecundaria(String tensaoSecundaria) {
		this.tensaoSecundaria = tensaoSecundaria;
	}

	public String getPerdasNoFerro() {
		return perdasNoFerro;
	}

	public void setPerdasNoFerro(String perdasNoFerro) {
		this.perdasNoFerro = perdasNoFerro;
	}

	public String getPerdasNoCobre() {
		return perdasNoCobre;
	}

	public void setPerdasNoCobre(String perdasNoCobre) {
		this.perdasNoCobre = perdasNoCobre;
	}

	public String getImpedancia() {
		return impedancia;
	}

	public void setImpedancia(String impedancia) {
		this.impedancia = impedancia;
	}

	public String getExitacao() {
		return exitacao;
	}

	public void setExitacao(String exitacao) {
		this.exitacao = exitacao;
	}

	public String getTapAcima() {
		return tapAcima;
	}

	public void setTapAcima(String tapAcima) {
		this.tapAcima = tapAcima;
	}

	public String getTapAbaixo() {
		return tapAbaixo;
	}

	public void setTapAbaixo(String tapAbaixo) {
		this.tapAbaixo = tapAbaixo;
	}

	public String getMedicaoNaAlta() {
		return medicaoNaAlta;
	}

	public void setMedicaoNaAlta(String medicaoNaAlta) {
		this.medicaoNaAlta = medicaoNaAlta;
	}

}