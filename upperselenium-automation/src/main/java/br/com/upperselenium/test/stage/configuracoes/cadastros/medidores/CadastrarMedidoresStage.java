package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo.CadastrarMedidoresDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page.CadastroMedidoresPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page.MedidoresPage;

public class CadastrarMedidoresStage extends BaseStage {
	private CadastrarMedidoresDPO cadastrarMedidoresDPO;
	private MedidoresPage medidoresPage;
	private CadastroMedidoresPage cadastroMedidoresPage;
	
	public CadastrarMedidoresStage(String dp) {
		cadastrarMedidoresDPO = loadDataProviderFile(CadastrarMedidoresDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		medidoresPage = initElementsFromPage(MedidoresPage.class);
		cadastroMedidoresPage = initElementsFromPage(CadastroMedidoresPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarMedidoresDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		medidoresPage.clickNovo();
		cadastroMedidoresPage.typeTextNome(cadastrarMedidoresDPO.getNome()+getRandomStringSpaced());		
		cadastroMedidoresPage.typeTextDescricao(cadastrarMedidoresDPO.getDescricao()+getRandomStringSpaced());		
		cadastroMedidoresPage.typeTextNumeroInterno(cadastrarMedidoresDPO.getNumeroInterno());		
		cadastroMedidoresPage.typeTextDigitoControle(cadastrarMedidoresDPO.getDigitoControle());		
		cadastroMedidoresPage.typeSelectTipo(cadastrarMedidoresDPO.getTipo());	
		cadastroMedidoresPage.typeTextVersaoDoFirmware(cadastrarMedidoresDPO.getVersaoDoFirmware());		
		cadastroMedidoresPage.typeTextNumeroDeSerie(cadastrarMedidoresDPO.getNumeroDeSerie()+getRandomSHA1NonSpaced());		
		cadastroMedidoresPage.keyPageDown();		
		cadastroMedidoresPage.typeTextInicioDaVigencia(cadastrarMedidoresDPO.getInicioDaVigencia());
		cadastroMedidoresPage.typeSelectFuncao(cadastrarMedidoresDPO.getFuncao());		
		cadastroMedidoresPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroMedidoresPage.validateMessageDefault(cadastrarMedidoresDPO.getOperationMessage());
	}

}
