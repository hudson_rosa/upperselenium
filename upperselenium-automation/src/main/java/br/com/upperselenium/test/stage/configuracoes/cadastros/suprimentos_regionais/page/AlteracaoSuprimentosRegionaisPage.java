package br.com.upperselenium.test.stage.configuracoes.cadastros.suprimentos_regionais.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoSuprimentosRegionaisPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/form/fieldset/div[2]/input";   
	private static final String TEXT_NOME = "//*[@id='Nome']";
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
		
}