package br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaPastasSecundariasClientsSCDEDPO extends BaseHelperDPO {

	private String habilitadaPasta1;
	private String habilitadaPasta2;
	private String habilitadaPasta3;
	private String habilitadaPasta4;
	private String habilitadaPasta5;
	private String habilitadaPasta6;
	private String habilitadaPasta7;
	private String habilitadaPasta8;
	private String habilitadaPasta9;
	private String habilitadaPasta10;
	private String tipoPasta1;
	private String tipoPasta2;
	private String tipoPasta3;
	private String tipoPasta4;
	private String tipoPasta5;
	private String tipoPasta6;
	private String tipoPasta7;
	private String tipoPasta8;
	private String tipoPasta9;
	private String tipoPasta10;
	private String pathPasta1;
	private String pathPasta2;
	private String pathPasta3;
	private String pathPasta4;
	private String pathPasta5;
	private String pathPasta6;
	private String pathPasta7;
	private String pathPasta8;
	private String pathPasta9;
	private String pathPasta10;
	private String dominioPasta1;
	private String dominioPasta2;
	private String dominioPasta3;
	private String dominioPasta4;
	private String dominioPasta5;
	private String dominioPasta6;
	private String dominioPasta7;
	private String dominioPasta8;
	private String dominioPasta9;
	private String dominioPasta10;
	private String usuarioPasta1;
	private String usuarioPasta2;
	private String usuarioPasta3;
	private String usuarioPasta4;
	private String usuarioPasta5;
	private String usuarioPasta6;
	private String usuarioPasta7;
	private String usuarioPasta8;
	private String usuarioPasta9;
	private String usuarioPasta10;
	private String senhaPasta1;
	private String senhaPasta2;
	private String senhaPasta3;
	private String senhaPasta4;
	private String senhaPasta5;
	private String senhaPasta6;
	private String senhaPasta7;
	private String senhaPasta8;
	private String senhaPasta9;
	private String senhaPasta10;

	public String getHabilitadaPasta1() {
		return habilitadaPasta1;
	}

	public void setHabilitadaPasta1(String habilitadaPasta1) {
		this.habilitadaPasta1 = habilitadaPasta1;
	}

	public String getHabilitadaPasta2() {
		return habilitadaPasta2;
	}

	public void setHabilitadaPasta2(String habilitadaPasta2) {
		this.habilitadaPasta2 = habilitadaPasta2;
	}

	public String getHabilitadaPasta3() {
		return habilitadaPasta3;
	}

	public void setHabilitadaPasta3(String habilitadaPasta3) {
		this.habilitadaPasta3 = habilitadaPasta3;
	}

	public String getHabilitadaPasta4() {
		return habilitadaPasta4;
	}

	public void setHabilitadaPasta4(String habilitadaPasta4) {
		this.habilitadaPasta4 = habilitadaPasta4;
	}

	public String getHabilitadaPasta5() {
		return habilitadaPasta5;
	}

	public void setHabilitadaPasta5(String habilitadaPasta5) {
		this.habilitadaPasta5 = habilitadaPasta5;
	}

	public String getHabilitadaPasta6() {
		return habilitadaPasta6;
	}

	public void setHabilitadaPasta6(String habilitadaPasta6) {
		this.habilitadaPasta6 = habilitadaPasta6;
	}

	public String getHabilitadaPasta7() {
		return habilitadaPasta7;
	}

	public void setHabilitadaPasta7(String habilitadaPasta7) {
		this.habilitadaPasta7 = habilitadaPasta7;
	}

	public String getHabilitadaPasta8() {
		return habilitadaPasta8;
	}

	public void setHabilitadaPasta8(String habilitadaPasta8) {
		this.habilitadaPasta8 = habilitadaPasta8;
	}

	public String getHabilitadaPasta9() {
		return habilitadaPasta9;
	}

	public void setHabilitadaPasta9(String habilitadaPasta9) {
		this.habilitadaPasta9 = habilitadaPasta9;
	}

	public String getHabilitadaPasta10() {
		return habilitadaPasta10;
	}

	public void setHabilitadaPasta10(String habilitadaPasta10) {
		this.habilitadaPasta10 = habilitadaPasta10;
	}

	public String getTipoPasta1() {
		return tipoPasta1;
	}

	public void setTipoPasta1(String tipoPasta1) {
		this.tipoPasta1 = tipoPasta1;
	}

	public String getTipoPasta2() {
		return tipoPasta2;
	}

	public void setTipoPasta2(String tipoPasta2) {
		this.tipoPasta2 = tipoPasta2;
	}

	public String getTipoPasta3() {
		return tipoPasta3;
	}

	public void setTipoPasta3(String tipoPasta3) {
		this.tipoPasta3 = tipoPasta3;
	}

	public String getTipoPasta4() {
		return tipoPasta4;
	}

	public void setTipoPasta4(String tipoPasta4) {
		this.tipoPasta4 = tipoPasta4;
	}

	public String getTipoPasta5() {
		return tipoPasta5;
	}

	public void setTipoPasta5(String tipoPasta5) {
		this.tipoPasta5 = tipoPasta5;
	}

	public String getTipoPasta6() {
		return tipoPasta6;
	}

	public void setTipoPasta6(String tipoPasta6) {
		this.tipoPasta6 = tipoPasta6;
	}

	public String getTipoPasta7() {
		return tipoPasta7;
	}

	public void setTipoPasta7(String tipoPasta7) {
		this.tipoPasta7 = tipoPasta7;
	}

	public String getTipoPasta8() {
		return tipoPasta8;
	}

	public void setTipoPasta8(String tipoPasta8) {
		this.tipoPasta8 = tipoPasta8;
	}

	public String getTipoPasta9() {
		return tipoPasta9;
	}

	public void setTipoPasta9(String tipoPasta9) {
		this.tipoPasta9 = tipoPasta9;
	}

	public String getTipoPasta10() {
		return tipoPasta10;
	}

	public void setTipoPasta10(String tipoPasta10) {
		this.tipoPasta10 = tipoPasta10;
	}

	public String getPathPasta1() {
		return pathPasta1;
	}

	public void setPathPasta1(String pathPasta1) {
		this.pathPasta1 = pathPasta1;
	}

	public String getPathPasta2() {
		return pathPasta2;
	}

	public void setPathPasta2(String pathPasta2) {
		this.pathPasta2 = pathPasta2;
	}

	public String getPathPasta3() {
		return pathPasta3;
	}

	public void setPathPasta3(String pathPasta3) {
		this.pathPasta3 = pathPasta3;
	}

	public String getPathPasta4() {
		return pathPasta4;
	}

	public void setPathPasta4(String pathPasta4) {
		this.pathPasta4 = pathPasta4;
	}

	public String getPathPasta5() {
		return pathPasta5;
	}

	public void setPathPasta5(String pathPasta5) {
		this.pathPasta5 = pathPasta5;
	}

	public String getPathPasta6() {
		return pathPasta6;
	}

	public void setPathPasta6(String pathPasta6) {
		this.pathPasta6 = pathPasta6;
	}

	public String getPathPasta7() {
		return pathPasta7;
	}

	public void setPathPasta7(String pathPasta7) {
		this.pathPasta7 = pathPasta7;
	}

	public String getPathPasta8() {
		return pathPasta8;
	}

	public void setPathPasta8(String pathPasta8) {
		this.pathPasta8 = pathPasta8;
	}

	public String getPathPasta9() {
		return pathPasta9;
	}

	public void setPathPasta9(String pathPasta9) {
		this.pathPasta9 = pathPasta9;
	}

	public String getPathPasta10() {
		return pathPasta10;
	}

	public void setPathPasta10(String pathPasta10) {
		this.pathPasta10 = pathPasta10;
	}

	public String getDominioPasta1() {
		return dominioPasta1;
	}

	public void setDominioPasta1(String dominioPasta1) {
		this.dominioPasta1 = dominioPasta1;
	}

	public String getDominioPasta2() {
		return dominioPasta2;
	}

	public void setDominioPasta2(String dominioPasta2) {
		this.dominioPasta2 = dominioPasta2;
	}

	public String getDominioPasta3() {
		return dominioPasta3;
	}

	public void setDominioPasta3(String dominioPasta3) {
		this.dominioPasta3 = dominioPasta3;
	}

	public String getDominioPasta4() {
		return dominioPasta4;
	}

	public void setDominioPasta4(String dominioPasta4) {
		this.dominioPasta4 = dominioPasta4;
	}

	public String getDominioPasta5() {
		return dominioPasta5;
	}

	public void setDominioPasta5(String dominioPasta5) {
		this.dominioPasta5 = dominioPasta5;
	}

	public String getDominioPasta6() {
		return dominioPasta6;
	}

	public void setDominioPasta6(String dominioPasta6) {
		this.dominioPasta6 = dominioPasta6;
	}

	public String getDominioPasta7() {
		return dominioPasta7;
	}

	public void setDominioPasta7(String dominioPasta7) {
		this.dominioPasta7 = dominioPasta7;
	}

	public String getDominioPasta8() {
		return dominioPasta8;
	}

	public void setDominioPasta8(String dominioPasta8) {
		this.dominioPasta8 = dominioPasta8;
	}

	public String getDominioPasta9() {
		return dominioPasta9;
	}

	public void setDominioPasta9(String dominioPasta9) {
		this.dominioPasta9 = dominioPasta9;
	}

	public String getDominioPasta10() {
		return dominioPasta10;
	}

	public void setDominioPasta10(String dominioPasta10) {
		this.dominioPasta10 = dominioPasta10;
	}

	public String getUsuarioPasta1() {
		return usuarioPasta1;
	}

	public void setUsuarioPasta1(String usuarioPasta1) {
		this.usuarioPasta1 = usuarioPasta1;
	}

	public String getUsuarioPasta2() {
		return usuarioPasta2;
	}

	public void setUsuarioPasta2(String usuarioPasta2) {
		this.usuarioPasta2 = usuarioPasta2;
	}

	public String getUsuarioPasta3() {
		return usuarioPasta3;
	}

	public void setUsuarioPasta3(String usuarioPasta3) {
		this.usuarioPasta3 = usuarioPasta3;
	}

	public String getUsuarioPasta4() {
		return usuarioPasta4;
	}

	public void setUsuarioPasta4(String usuarioPasta4) {
		this.usuarioPasta4 = usuarioPasta4;
	}

	public String getUsuarioPasta5() {
		return usuarioPasta5;
	}

	public void setUsuarioPasta5(String usuarioPasta5) {
		this.usuarioPasta5 = usuarioPasta5;
	}

	public String getUsuarioPasta6() {
		return usuarioPasta6;
	}

	public void setUsuarioPasta6(String usuarioPasta6) {
		this.usuarioPasta6 = usuarioPasta6;
	}

	public String getUsuarioPasta7() {
		return usuarioPasta7;
	}

	public void setUsuarioPasta7(String usuarioPasta7) {
		this.usuarioPasta7 = usuarioPasta7;
	}

	public String getUsuarioPasta8() {
		return usuarioPasta8;
	}

	public void setUsuarioPasta8(String usuarioPasta8) {
		this.usuarioPasta8 = usuarioPasta8;
	}

	public String getUsuarioPasta9() {
		return usuarioPasta9;
	}

	public void setUsuarioPasta9(String usuarioPasta9) {
		this.usuarioPasta9 = usuarioPasta9;
	}

	public String getUsuarioPasta10() {
		return usuarioPasta10;
	}

	public void setUsuarioPasta10(String usuarioPasta10) {
		this.usuarioPasta10 = usuarioPasta10;
	}

	public String getSenhaPasta1() {
		return senhaPasta1;
	}

	public void setSenhaPasta1(String senhaPasta1) {
		this.senhaPasta1 = senhaPasta1;
	}

	public String getSenhaPasta2() {
		return senhaPasta2;
	}

	public void setSenhaPasta2(String senhaPasta2) {
		this.senhaPasta2 = senhaPasta2;
	}

	public String getSenhaPasta3() {
		return senhaPasta3;
	}

	public void setSenhaPasta3(String senhaPasta3) {
		this.senhaPasta3 = senhaPasta3;
	}

	public String getSenhaPasta4() {
		return senhaPasta4;
	}

	public void setSenhaPasta4(String senhaPasta4) {
		this.senhaPasta4 = senhaPasta4;
	}

	public String getSenhaPasta5() {
		return senhaPasta5;
	}

	public void setSenhaPasta5(String senhaPasta5) {
		this.senhaPasta5 = senhaPasta5;
	}

	public String getSenhaPasta6() {
		return senhaPasta6;
	}

	public void setSenhaPasta6(String senhaPasta6) {
		this.senhaPasta6 = senhaPasta6;
	}

	public String getSenhaPasta7() {
		return senhaPasta7;
	}

	public void setSenhaPasta7(String senhaPasta7) {
		this.senhaPasta7 = senhaPasta7;
	}

	public String getSenhaPasta8() {
		return senhaPasta8;
	}

	public void setSenhaPasta8(String senhaPasta8) {
		this.senhaPasta8 = senhaPasta8;
	}

	public String getSenhaPasta9() {
		return senhaPasta9;
	}

	public void setSenhaPasta9(String senhaPasta9) {
		this.senhaPasta9 = senhaPasta9;
	}

	public String getSenhaPasta10() {
		return senhaPasta10;
	}

	public void setSenhaPasta10(String senhaPasta10) {
		this.senhaPasta10 = senhaPasta10;
	}

}
