package br.com.upperselenium.test.stage.configuracoes.administracao.perfis.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarRegistroLinkPerfisDPO extends BaseHelperDPO {

	private String clickItem;

	public String getClickItem() {
		return clickItem;
	}

	public void setClickItem(String clickItem) {
		this.clickItem = clickItem;
	}

	@Override
	public String toString() {
		return "NavegarRegistroLinkPerfisDPO [clickItem=" + clickItem + "]";
	}

}