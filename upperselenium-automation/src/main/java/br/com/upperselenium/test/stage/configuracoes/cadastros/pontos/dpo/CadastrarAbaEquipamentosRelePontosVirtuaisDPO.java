package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaEquipamentosRelePontosVirtuaisDPO extends BaseHelperDPO {

	private String tipo;
	private String numeroDeSerie;
	private String correnteDePickUp;

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getNumeroDeSerie() {
		return numeroDeSerie;
	}

	public void setNumeroDeSerie(String numeroDeSerie) {
		this.numeroDeSerie = numeroDeSerie;
	}

	public String getCorrenteDePickUp() {
		return correnteDePickUp;
	}

	public void setCorrenteDePickUp(String correnteDePickUp) {
		this.correnteDePickUp = correnteDePickUp;
	}

}