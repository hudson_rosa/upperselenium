package br.com.upperselenium.test.stage.configuracoes.cadastros.clients_scde.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarAbaPontosClientsSCDEDPO extends BaseHelperDPO {

	private String filterNome;
	private String filterPasta;
	private String filterTipoDePonto;

	public String getFilterNome() {
		return filterNome;
	}

	public void setFilterNome(String filterNome) {
		this.filterNome = filterNome;
	}

	public String getFilterPasta() {
		return filterPasta;
	}

	public void setFilterPasta(String filterPasta) {
		this.filterPasta = filterPasta;
	}

	public String getFilterTipoDePonto() {
		return filterTipoDePonto;
	}

	public void setFilterTipoDePonto(String filterTipoDePonto) {
		this.filterTipoDePonto = filterTipoDePonto;
	}

}
