package br.com.upperselenium.test.stage.configuracoes.administracao.relatorio_de_auditoria;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.dpo.NavegarPerfisDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.page.PerfisPage;

public class NavegarRelatorioDeAuditoriaStage extends BaseStage {
	private NavegarPerfisDPO navegarPerfisDPO;
	private PerfisPage perfisPage;
	
	public NavegarRelatorioDeAuditoriaStage(String dp) {
		navegarPerfisDPO = loadDataProviderFile(NavegarPerfisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		perfisPage = initElementsFromPage(PerfisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarPerfisDPO.getIssue());
		execute();
	}
	
	private void execute() {
		perfisPage.getBreadcrumbsText(navegarPerfisDPO.getBreadcrumbs());
		perfisPage.clickUpToBreadcrumbs(navegarPerfisDPO.getUpToBreadcrumb());
		perfisPage.getWelcomeTitle(navegarPerfisDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
