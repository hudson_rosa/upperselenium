package br.com.upperselenium.test.stage.configuracoes.administracao.perfis;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.dpo.NavegarRegistroLinkPerfisDPO;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.page.AlteracaoPerfisPage;
import br.com.upperselenium.test.stage.configuracoes.administracao.perfis.page.PerfisPage;

public class NavegarRegistroLinkPerfisStage extends BaseStage {
	private NavegarRegistroLinkPerfisDPO navegarRegistroLinkPerfisDPO;
	private PerfisPage perfisPage;
	private AlteracaoPerfisPage alteracaoPerfisPage;
	
	public NavegarRegistroLinkPerfisStage(String dp) {
		navegarRegistroLinkPerfisDPO = loadDataProviderFile(NavegarRegistroLinkPerfisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		perfisPage = initElementsFromPage(PerfisPage.class);
		alteracaoPerfisPage = initElementsFromPage(AlteracaoPerfisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarRegistroLinkPerfisDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		perfisPage.clickGridFirstLineLinkNome(navegarRegistroLinkPerfisDPO.getClickItem());
		alteracaoPerfisPage.clickAbaPermissoes();
		alteracaoPerfisPage.getBreadcrumbsText(navegarRegistroLinkPerfisDPO.getBreadcrumbs());
		alteracaoPerfisPage.clickUpToBreadcrumbs(navegarRegistroLinkPerfisDPO.getUpToBreadcrumb());
		alteracaoPerfisPage.getWelcomeTitle(navegarRegistroLinkPerfisDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
