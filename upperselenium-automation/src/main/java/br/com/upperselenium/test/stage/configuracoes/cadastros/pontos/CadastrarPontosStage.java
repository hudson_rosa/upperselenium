package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarPontosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastroPontosPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.PontosPage;

public class CadastrarPontosStage extends BaseStage {
	private CadastrarPontosDPO cadastrarPontosDPO;
	private PontosPage pontosPage;
	private CadastroPontosPage cadastroPontosPage;
	
	public CadastrarPontosStage(String dp) {
		cadastrarPontosDPO = loadDataProviderFile(CadastrarPontosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		pontosPage = initElementsFromPage(PontosPage.class);
		cadastroPontosPage = initElementsFromPage(CadastroPontosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarPontosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		pontosPage.clickNovo();
		cadastroPontosPage.typeTextNome(cadastrarPontosDPO.getNome()+getRandomStringSpaced());		
		cadastroPontosPage.typeSelectPasta(cadastrarPontosDPO.getPasta());		
		cadastroPontosPage.typeSelectTipo(cadastrarPontosDPO.getTipo());		
		cadastroPontosPage.typeSelectSubestacao(cadastrarPontosDPO.getSubestacao());		
		cadastroPontosPage.typeTextUC(cadastrarPontosDPO.getUc());		
		cadastroPontosPage.keyPageDown();		
		cadastroPontosPage.typeTextTrafoBT(cadastrarPontosDPO.getTrafoBt());		
		cadastroPontosPage.typeTextDescricao(cadastrarPontosDPO.getDescricao()+getRandomStringSpaced());		
		cadastroPontosPage.typeTextLatitude(cadastrarPontosDPO.getLatitude());		
		cadastroPontosPage.typeTextLongitude(cadastrarPontosDPO.getLongitude());		
		cadastroPontosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroPontosPage.validateMessageDefault(cadastrarPontosDPO.getOperationMessage());
	}
	
}
