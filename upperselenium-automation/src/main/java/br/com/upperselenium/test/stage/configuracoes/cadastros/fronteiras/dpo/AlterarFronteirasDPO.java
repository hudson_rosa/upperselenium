package br.com.upperselenium.test.stage.configuracoes.cadastros.fronteiras.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarFronteirasDPO extends BaseHelperDPO {

	private String nome;
	private String pontoDeEnergiaRequerida;
	private String pontoDeperdaTecnicaATMedida;
	private String pseudonimoDeSistemasDePerdasTecnicas;
	private String suprimentoRegional;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPontoDeEnergiaRequerida() {
		return pontoDeEnergiaRequerida;
	}

	public void setPontoDeEnergiaRequerida(String pontoDeEnergiaRequerida) {
		this.pontoDeEnergiaRequerida = pontoDeEnergiaRequerida;
	}

	public String getPontoDeperdaTecnicaATMedida() {
		return pontoDeperdaTecnicaATMedida;
	}

	public void setPontoDeperdaTecnicaATMedida(String pontoDeperdaTecnicaATMedida) {
		this.pontoDeperdaTecnicaATMedida = pontoDeperdaTecnicaATMedida;
	}

	public String getPseudonimoDeSistemasDePerdasTecnicas() {
		return pseudonimoDeSistemasDePerdasTecnicas;
	}

	public void setPseudonimoDeSistemasDePerdasTecnicas(String pseudonimoDeSistemasDePerdasTecnicas) {
		this.pseudonimoDeSistemasDePerdasTecnicas = pseudonimoDeSistemasDePerdasTecnicas;
	}

	public String getSuprimentoRegional() {
		return suprimentoRegional;
	}

	public void setSuprimentoRegional(String suprimentoRegional) {
		this.suprimentoRegional = suprimentoRegional;
	}

}