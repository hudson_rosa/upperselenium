package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import java.util.List;

public class GroupNovaEquacaoDPO {

	private String vigencia;
	private String sequenciaDeCalculo;
	private String tooltipSequencia;
	private String pontos;
	private String regras;
	private List<GridCadastrarNovaEquacaoDPO> gridCadastrarNovaEquacao;
	private String somarTodos;
	private String equacao;
	private String tootltipExpressao;

	public String getVigencia() {
		return vigencia;
	}

	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}

	public String getSequenciaDeCalculo() {
		return sequenciaDeCalculo;
	}

	public void setSequenciaDeCalculo(String sequenciaDeCalculo) {
		this.sequenciaDeCalculo = sequenciaDeCalculo;
	}

	public String getTooltipSequencia() {
		return tooltipSequencia;
	}

	public void setTooltipSequencia(String tooltipSequencia) {
		this.tooltipSequencia = tooltipSequencia;
	}

	public String getPontos() {
		return pontos;
	}

	public void setPontos(String pontos) {
		this.pontos = pontos;
	}

	public String getRegras() {
		return regras;
	}

	public void setRegras(String regras) {
		this.regras = regras;
	}

	public List<GridCadastrarNovaEquacaoDPO> getGridCadastrarNovaEquacao() {
		return gridCadastrarNovaEquacao;
	}

	public void setGridCadastrarNovaEquacao(List<GridCadastrarNovaEquacaoDPO> gridCadastrarNovaEquacao) {
		this.gridCadastrarNovaEquacao = gridCadastrarNovaEquacao;
	}

	public String getSomarTodos() {
		return somarTodos;
	}

	public void setSomarTodos(String somarTodos) {
		this.somarTodos = somarTodos;
	}

	public String getEquacao() {
		return equacao;
	}

	public void setEquacao(String equacao) {
		this.equacao = equacao;
	}

	public String getTootltipExpressao() {
		return tootltipExpressao;
	}

	public void setTootltipExpressao(String tootltipExpressao) {
		this.tootltipExpressao = tootltipExpressao;
	}

}
