package br.com.upperselenium.test.stage.medicao.comandos_do_gateway.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarListagemComandosDoGatewayDPO extends BaseHelperDPO {

	private String labelFiltrarDataProximaExecucao;
	private String clickFiltrar;
	private String labelHeaderGatewaysTags;
	private String labelHeaderInsercao;
	private String labelHeaderProximaExecucao;
	private String labelHeaderDescricao;
	private String labelHeaderUltimasExecucoes;
	private String labelHeaderExpiracao;

	public String getLabelFiltrarDataProximaExecucao() {
		return labelFiltrarDataProximaExecucao;
	}

	public void setLabelFiltrarDataProximaExecucao(String labelFiltrarDataProximaExecucao) {
		this.labelFiltrarDataProximaExecucao = labelFiltrarDataProximaExecucao;
	}

	public String getClickFiltrar() {
		return clickFiltrar;
	}

	public void setClickFiltrar(String clickFiltrar) {
		this.clickFiltrar = clickFiltrar;
	}

	public String getLabelHeaderGatewaysTags() {
		return labelHeaderGatewaysTags;
	}

	public void setLabelHeaderGatewaysTags(String labelHeaderGatewaysTags) {
		this.labelHeaderGatewaysTags = labelHeaderGatewaysTags;
	}

	public String getLabelHeaderInsercao() {
		return labelHeaderInsercao;
	}

	public void setLabelHeaderInsercao(String labelHeaderInsercao) {
		this.labelHeaderInsercao = labelHeaderInsercao;
	}

	public String getLabelHeaderProximaExecucao() {
		return labelHeaderProximaExecucao;
	}

	public void setLabelHeaderProximaExecucao(String labelHeaderProximaExecucao) {
		this.labelHeaderProximaExecucao = labelHeaderProximaExecucao;
	}

	public String getLabelHeaderDescricao() {
		return labelHeaderDescricao;
	}

	public void setLabelHeaderDescricao(String labelHeaderDescricao) {
		this.labelHeaderDescricao = labelHeaderDescricao;
	}

	public String getLabelHeaderUltimasExecucoes() {
		return labelHeaderUltimasExecucoes;
	}

	public void setLabelHeaderUltimasExecucoes(String labelHeaderUltimasExecucoes) {
		this.labelHeaderUltimasExecucoes = labelHeaderUltimasExecucoes;
	}

	public String getLabelHeaderExpiracao() {
		return labelHeaderExpiracao;
	}

	public void setLabelHeaderExpiracao(String labelHeaderExpiracao) {
		this.labelHeaderExpiracao = labelHeaderExpiracao;
	}

	@Override
	public String toString() {
		return "NavegarComandosDoGatewayDPO [labelFiltrarDataProximaExecucao=" + labelFiltrarDataProximaExecucao
				+ ", clickFiltrar=" + clickFiltrar + ", labelHeaderGatewaysTags=" + labelHeaderGatewaysTags
				+ ", labelHeaderInsercao=" + labelHeaderInsercao + ", labelHeaderProximaExecucao="
				+ labelHeaderProximaExecucao + ", labelHeaderDescricao=" + labelHeaderDescricao
				+ ", labelHeaderUltimasExecucoes=" + labelHeaderUltimasExecucoes + ", labelHeaderExpiracao="
				+ labelHeaderExpiracao + "]";
	}

}