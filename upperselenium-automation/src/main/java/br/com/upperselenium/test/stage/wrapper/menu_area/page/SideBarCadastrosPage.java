package br.com.upperselenium.test.stage.wrapper.menu_area.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class SideBarCadastrosPage extends BaseHelperPage{
	
	private static final String LINK_AGENTES = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Agentes')]";
	private static final String LINK_ALIMENTADORES = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Alimentadores')]";
	private static final String LINK_AREAS_GEOGRAFICAS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Áreas')]";
	private static final String LINK_AUTODISCOVERY_PENDENTES = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Auto Discovery')]";
	private static final String LINK_CIDADES = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Cidades')]";
	private static final String LINK_CIRCUITOS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Circuitos')]";
	private static final String LINK_CLASSES_DE_ISOLACAO = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Classes de Isolação')]";
	private static final String LINK_CLIENTS_SCDE = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Clients SCDE')]";
	private static final String LINK_CONNECTION_MANAGER = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Connection Manager')]";
	private static final String LINK_CURVAS_TIPICAS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Curvas Típicas')]";
	private static final String LINK_FERIADOS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Feriados')]";
	private static final String LINK_FRONTEIRAS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Fronteiras')]";
	private static final String LINK_GATEWAYS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Gateways')]";
	private static final String LINK_MEDIDORES = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Medidores')]";
	private static final String LINK_PADROES_DE_CALIBRACAO = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Padrões de Calibração')]";	
	private static final String LINK_PONTOS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Pontos')]";
	private static final String LINK_POSTOS_TARIFARIOS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Postos Tarifários')]";
	private static final String LINK_RAMO_DE_ATIVIDADE = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Ramo de Atividade')]";
	private static final String LINK_REGIOES = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Regiões')]";
	private static final String LINK_REGRAS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Regras')]";
	private static final String LINK_SIMCARD = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Simcard')]";
	private static final String LINK_SUBESTACAO = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Subestação')]";	
	private static final String LINK_SUPRIMENTOS_REGIONAIS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Suprimentos Regionais')]";
	private static final String LINK_TENSOES = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Tensões')]";
	private static final String LINK_TRAFOS_BT = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Trafos BT')]";
	private static final String LINK_UNIDADES_CONSUMIDORAS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Unidades Consumidoras')]";
	private static final String LINK_USINAS = ".//div[2]/div/div[1]/div/ul/li/a[contains(text(), 'Usinas')]";
	
	public boolean isClickableLinkAgentes() {
		return isDisplayedElement(LINK_AGENTES);
	}

	public boolean isClickableLinkAlimentadores() {
		return isDisplayedElement(LINK_ALIMENTADORES);
	}

	public boolean isClickableLinkAreasGeograficas() {
		return isDisplayedElement(LINK_AREAS_GEOGRAFICAS);
	}

	public boolean isClickableLinkAutoDiscoveryPendentes() {		
		return isDisplayedElement(LINK_AUTODISCOVERY_PENDENTES);
	}
	
	public boolean isClickableLinkCidades() {		
		return isDisplayedElement(LINK_CIDADES);
	}	
	
	public boolean isClickableLinkCircuitos() {		
		return isDisplayedElement(LINK_CIRCUITOS);
	}	
	
	public boolean isClickableLinkClassesDeIsolacao() {
		return isDisplayedElement(LINK_CLASSES_DE_ISOLACAO);
	}

	public boolean isClickableLinkClientsSCDE() {
		return isDisplayedElement(LINK_CLIENTS_SCDE);
	}
	
	public boolean isClickableLinkConnectionManager() {
		return isDisplayedElement(LINK_CONNECTION_MANAGER);
	}
	
	public boolean isClickableLinkCurvasTipicas() {
		return isDisplayedElement(LINK_CURVAS_TIPICAS);
	}
	
	public boolean isClickableLinkFeriados() {
		return isDisplayedElement(LINK_FERIADOS);
	}
	
	public boolean isClickableLinkFronteiras() {
		return isDisplayedElement(LINK_FRONTEIRAS);
	}
	
	public boolean isClickableLinkGateways() {
		return isDisplayedElement(LINK_GATEWAYS);
	}
	
	public boolean isClickableLinkMedidores() {
		return isDisplayedElement(LINK_MEDIDORES);
	}
	
	public boolean isClickableLinkPadroesDeCalibracao() {
		return isDisplayedElement(LINK_PADROES_DE_CALIBRACAO);
	}	
	
	public boolean isClickableLinkPontos() {
		return isDisplayedElement(LINK_PONTOS);
	}
	
	public boolean isClickableLinkPostosTarifarios() {
		return isDisplayedElement(LINK_POSTOS_TARIFARIOS);
	}
	
	public boolean isClickableLinkRamoDeAtividade() {
		return isDisplayedElement(LINK_RAMO_DE_ATIVIDADE);
	}
	
	public boolean isClickableLinkRegioes() {
		return isDisplayedElement(LINK_REGIOES);
	}
	
	public boolean isClickableLinkRegras() {
		return isDisplayedElement(LINK_REGRAS);
	}
	
	public boolean isClickableLinkSimcard() {
		return isDisplayedElement(LINK_SIMCARD);
	}
	
	public boolean isClickableLinkSubestacao() {
		return isDisplayedElement(LINK_SUBESTACAO);
	}
	
	public boolean isClickableLinkSuprimentosRegionais() {
		return isDisplayedElement(LINK_SUPRIMENTOS_REGIONAIS);
	}
	
	public boolean isClickableLinkTensoes() {
		return isDisplayedElement(LINK_TENSOES);
	}
	
	public boolean isClickableLinkTrafoBT() {
		return isDisplayedElement(LINK_TRAFOS_BT);
	}
	
	public boolean isClickableLinkUnidadesConsumidoras() {
		return isDisplayedElement(LINK_UNIDADES_CONSUMIDORAS);
	}
	
	public boolean isClickableLinkUsinas() {
		return isDisplayedElement(LINK_USINAS);
	}	
	
	public void clickAgentes(){
		waitForElementToBeClickable(LINK_AGENTES);
		clickOnElement(LINK_AGENTES);
	}
	
	public void clickAlimentadores(){
		waitForElementToBeClickable(LINK_ALIMENTADORES);
		clickOnElement(LINK_ALIMENTADORES);
	}
	
	public void clickAreasGeograficas(){
		waitForElementToBeClickable(LINK_AREAS_GEOGRAFICAS);
		clickOnElement(LINK_AREAS_GEOGRAFICAS);
	}
	
	public void clickAutoDiscoveryPendentes(){
		waitForElementToBeClickable(LINK_AUTODISCOVERY_PENDENTES);
		clickOnElement(LINK_AUTODISCOVERY_PENDENTES);
	}
	
	public void clickCidades(){
		waitForElementToBeClickable(LINK_CIDADES);
		clickOnElement(LINK_CIDADES);
	}
	
	public void clickCircuitos(){
		waitForElementToBeClickable(LINK_CIRCUITOS);
		clickOnElement(LINK_CIRCUITOS);
	}
	
	public void clickClassesDeIsolacao(){
		waitForElementToBeClickable(LINK_CLASSES_DE_ISOLACAO);
		clickOnElement(LINK_CLASSES_DE_ISOLACAO);
	}
	
	public void clickClientsSCDE(){
		waitForElementToBeClickable(LINK_CLIENTS_SCDE);
		clickOnElement(LINK_CLIENTS_SCDE);
	}
	
	public void clickConnectionManager(){
		waitForElementToBeClickable(LINK_CONNECTION_MANAGER);
		clickOnElement(LINK_CONNECTION_MANAGER);
	}
	
	public void clickCurvasTipicas(){
		waitForElementToBeClickable(LINK_CURVAS_TIPICAS);
		clickOnElement(LINK_CURVAS_TIPICAS);
	}
	
	public void clickFeriados(){
		waitForElementToBeClickable(LINK_FERIADOS);
		clickOnElement(LINK_FERIADOS);
	}
	
	public void clickFronteiras(){
		waitForElementToBeClickable(LINK_FRONTEIRAS);
		clickOnElement(LINK_FRONTEIRAS);
	}
	
	public void clickGateways(){
		waitForElementToBeClickable(LINK_GATEWAYS);
		clickOnElement(LINK_GATEWAYS);
	}
	
	public void clickMedidores(){
		waitForElementToBeClickable(LINK_MEDIDORES);
		clickOnElement(LINK_MEDIDORES);
	}
	
	public void clickPadroesDeCalibracao(){
		waitForElementToBeClickable(LINK_PADROES_DE_CALIBRACAO);
		clickOnElement(LINK_PADROES_DE_CALIBRACAO);
	}
	
	public void clickPontos(){
		waitForElementToBeClickable(LINK_PONTOS);
		clickOnElement(LINK_PONTOS);
	}
	
	public void clickPostosTarifarios(){
		waitForElementToBeClickable(LINK_POSTOS_TARIFARIOS);
		clickOnElement(LINK_POSTOS_TARIFARIOS);
	}
	
	public void clickRamoDeAtividade(){
		waitForElementToBeClickable(LINK_RAMO_DE_ATIVIDADE);
		clickOnElement(LINK_RAMO_DE_ATIVIDADE);
	}
	
	public void clickRegioes(){
		waitForElementToBeClickable(LINK_REGIOES);
		clickOnElement(LINK_REGIOES);
	}
	
	public void clickRegras(){
		waitForElementToBeClickable(LINK_REGRAS);
		clickOnElement(LINK_REGRAS);
	}
	
	public void clickSimcard(){
		waitForElementToBeClickable(LINK_SIMCARD);
		clickOnElement(LINK_SIMCARD);
	}
	
	public void clickSubestacao(){
		waitForElementToBeClickable(LINK_SUBESTACAO);
		clickOnElement(LINK_SUBESTACAO);
	}
	
	public void clickSuprimentosRegionais(){
		waitForElementToBeClickable(LINK_SUPRIMENTOS_REGIONAIS);
		clickOnElement(LINK_SUPRIMENTOS_REGIONAIS);
	}
	
	public void clickTensoes(){
		waitForElementToBeClickable(LINK_SUBESTACAO);
		clickOnElement(LINK_TENSOES);
	}
	
	public void clickTrafosBT(){
		waitForElementToBeClickable(LINK_TRAFOS_BT);
		clickOnElement(LINK_TRAFOS_BT);
	}
	
	public void clickUnidadesConsumidoras(){
		waitForElementToBeClickable(LINK_UNIDADES_CONSUMIDORAS);
		clickOnElement(LINK_UNIDADES_CONSUMIDORAS);
	}
	
	public void clickUsinas(){
		waitForElementToBeClickable(LINK_USINAS);
		clickOnElement(LINK_USINAS);
	}

	public void keyPageDown(){
		useKey(LINK_AGENTES, Keys.PAGE_DOWN);
	}

}