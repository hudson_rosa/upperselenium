package br.com.upperselenium.test.stage.configuracoes.cadastros.alimentadores.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class AlterarAlimentadoresDPO extends BaseHelperDPO {

	private String nome;
	private String pontoDeMedicao;
	private String pseudonimoPerdasTecnicas;
	private String pseudonimoConsumo;
	private String pontoDeConsumoAdicional;
	private String regiao;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPontoDeMedicao() {
		return pontoDeMedicao;
	}

	public void setPontoDeMedicao(String pontoDeMedicao) {
		this.pontoDeMedicao = pontoDeMedicao;
	}

	public String getPseudonimoPerdasTecnicas() {
		return pseudonimoPerdasTecnicas;
	}

	public void setPseudonimoPerdasTecnicas(String pseudonimoPerdasTecnicas) {
		this.pseudonimoPerdasTecnicas = pseudonimoPerdasTecnicas;
	}

	public String getPseudonimoConsumo() {
		return pseudonimoConsumo;
	}

	public void setPseudonimoConsumo(String pseudonimoConsumo) {
		this.pseudonimoConsumo = pseudonimoConsumo;
	}

	public String getPontoDeConsumoAdicional() {
		return pontoDeConsumoAdicional;
	}

	public void setPontoDeConsumoAdicional(String pontoDeConsumoAdicional) {
		this.pontoDeConsumoAdicional = pontoDeConsumoAdicional;
	}

	public String getRegiao() {
		return regiao;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

}
