package br.com.upperselenium.test.stage.medicao.comandos_do_gateway.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarComandosDoGatewayHistoricoDPO extends BaseHelperDPO {

	private String labelHeaderGateway;
	private String labelHeaderInstante;
	private String labelHeaderDescricao;
	private String labelHeaderStatus;
	private String labelHeaderErro;

	public String getLabelHeaderGateway() {
		return labelHeaderGateway;
	}

	public void setLabelHeaderGateway(String labelHeaderGateway) {
		this.labelHeaderGateway = labelHeaderGateway;
	}

	public String getLabelHeaderInstante() {
		return labelHeaderInstante;
	}

	public void setLabelHeaderInstante(String labelHeaderInstante) {
		this.labelHeaderInstante = labelHeaderInstante;
	}

	public String getLabelHeaderDescricao() {
		return labelHeaderDescricao;
	}

	public void setLabelHeaderDescricao(String labelHeaderDescricao) {
		this.labelHeaderDescricao = labelHeaderDescricao;
	}

	public String getLabelHeaderStatus() {
		return labelHeaderStatus;
	}

	public void setLabelHeaderStatus(String labelHeaderStatus) {
		this.labelHeaderStatus = labelHeaderStatus;
	}

	public String getLabelHeaderErro() {
		return labelHeaderErro;
	}

	public void setLabelHeaderErro(String labelHeaderErro) {
		this.labelHeaderErro = labelHeaderErro;
	}

	@Override
	public String toString() {
		return "NavegarComandosDoGatewayHistoricoDPO [labelHeaderGateway=" + labelHeaderGateway
				+ ", labelHeaderInstante=" + labelHeaderInstante + ", labelHeaderDescricao=" + labelHeaderDescricao
				+ ", labelHeaderStatus=" + labelHeaderStatus + ", labelHeaderErro=" + labelHeaderErro + "]";
	}

}