package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.dpo.NavegarImportacaoDeDadosHidrologicosDPO;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.page.AcompDiarioDeUsinasPage;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_de_usinas.page.ImportacaoDeDadosHidrologicosPage;

public class NavegarImportacaoDeDadosHidrologicosStage extends BaseStage {
	private NavegarImportacaoDeDadosHidrologicosDPO navegarMonitoramentoDeUsinasDPO;
	private AcompDiarioDeUsinasPage acompDiarioDeUsinasPage;	
	private ImportacaoDeDadosHidrologicosPage monitoramentoDeUsinasPage;	
	
	public NavegarImportacaoDeDadosHidrologicosStage(String dp) {
		navegarMonitoramentoDeUsinasDPO = loadDataProviderFile(NavegarImportacaoDeDadosHidrologicosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		acompDiarioDeUsinasPage = initElementsFromPage(AcompDiarioDeUsinasPage.class);
		monitoramentoDeUsinasPage = initElementsFromPage(ImportacaoDeDadosHidrologicosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarMonitoramentoDeUsinasDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		acompDiarioDeUsinasPage.clickLinkImportacaoDeDadosHidrologicos();
		monitoramentoDeUsinasPage.getLabeldHeaderInstanteDeImportacao(navegarMonitoramentoDeUsinasDPO.getLabelHeaderInstanteDeImportacao());
		monitoramentoDeUsinasPage.getLabelHeaderTipoDeImportacao(navegarMonitoramentoDeUsinasDPO.getLabelHeaderTipoDeImportacao());
		monitoramentoDeUsinasPage.getLabelHeaderStatusDeImportacao(navegarMonitoramentoDeUsinasDPO.getLabelHeaderStatusDeImportacao());
		monitoramentoDeUsinasPage.clickImportarNovosDadosHidrologicos();
		monitoramentoDeUsinasPage.getModalLabelAlteracaoDePontoAplicado(navegarMonitoramentoDeUsinasDPO.getModalLabelAlteracaoDePontoAplicado());
		monitoramentoDeUsinasPage.getModalLabelTipoDeImportacao(navegarMonitoramentoDeUsinasDPO.getModalLabelTipoDeImportacao());
		monitoramentoDeUsinasPage.getModalLabelArquivo(navegarMonitoramentoDeUsinasDPO.getModalLabelArquivo());
		monitoramentoDeUsinasPage.getModalLabelButtonImportar(navegarMonitoramentoDeUsinasDPO.getModalButtonLabelImportar());
		monitoramentoDeUsinasPage.clickModalClose();
		monitoramentoDeUsinasPage.getBreadcrumbsText(navegarMonitoramentoDeUsinasDPO.getBreadcrumbs());
		monitoramentoDeUsinasPage.clickUpToBreadcrumbs(navegarMonitoramentoDeUsinasDPO.getUpToBreadcrumb());
		monitoramentoDeUsinasPage.getWelcomeTitle(navegarMonitoramentoDeUsinasDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
