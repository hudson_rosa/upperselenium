package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.base.util.WaitElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo.AlterarAbaColetasMedidoresDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page.AlteracaoAbaColetasMedidoresPage;

public class AlterarAbaColetasMedidoresStage extends BaseStage {
	private AlterarAbaColetasMedidoresDPO alterarAbaColetasMedidoresDPO;
	private AlteracaoAbaColetasMedidoresPage alteracaoAbaColetasMedidoresPage;
	
	public AlterarAbaColetasMedidoresStage(String dp) {
		alterarAbaColetasMedidoresDPO = loadDataProviderFile(AlterarAbaColetasMedidoresDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alteracaoAbaColetasMedidoresPage = initElementsFromPage(AlteracaoAbaColetasMedidoresPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarAbaColetasMedidoresDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoAbaColetasMedidoresPage.clickAbaColetas();
		FindElementUtil.acceptAlert();
		alteracaoAbaColetasMedidoresPage.typeCheckAll(alterarAbaColetasMedidoresDPO.getCheckAll());
		if(StringUtil.isNotBlankOrNotNull(alterarAbaColetasMedidoresDPO.getCheckAll())){
			editModalDataDaUltimaColeta();
		} else {
			editModalDataDaUltimaColetaForFilteredRegister();
		}
		alteracaoAbaColetasMedidoresPage.typeTextFilterGrandeza(alterarAbaColetasMedidoresDPO.getFilterGrandeza());
		alteracaoAbaColetasMedidoresPage.typeTextFilterDataDaUltimaColeta(alterarAbaColetasMedidoresDPO.getFilterDataDaUltimaColeta());
	}

	private void editModalDataDaUltimaColetaForFilteredRegister() {
		alteracaoAbaColetasMedidoresPage.typeTextFilterGrandeza(alterarAbaColetasMedidoresDPO.getFilterGrandeza());			
		alteracaoAbaColetasMedidoresPage.typeCheckBoxGridItemGrandeza(alterarAbaColetasMedidoresDPO.getFilterGrandeza());
		for (int index=0; index < alterarAbaColetasMedidoresDPO.getGridColetas().size(); index++){
			if(StringUtil.isNotBlankOrNotNull(alterarAbaColetasMedidoresDPO.getGridColetas().get(index).getFlagItem())) {
				alteracaoAbaColetasMedidoresPage.typeCheckBoxGridItems(alterarAbaColetasMedidoresDPO.getGridColetas().get(index).getFlagItem(), index+1);
				alteracaoAbaColetasMedidoresPage.getGridLabelValueGrandeza(alterarAbaColetasMedidoresDPO.getGridColetas().get(index).getGrandeza(), index+1);
				alteracaoAbaColetasMedidoresPage.getGridLabelValueDataDaUltimaColeta(alterarAbaColetasMedidoresDPO.getGridColetas().get(index).getDataDataUltimaColeta(), index+1);
				WaitElementUtil.waitForATime(TimePRM._3_SECS);
				editModalDataDaUltimaColeta();
			}
		}
	}

	private void editModalDataDaUltimaColeta() {
		alteracaoAbaColetasMedidoresPage.isClickableButtonEditar();
		alteracaoAbaColetasMedidoresPage.clickEditar();
		alteracaoAbaColetasMedidoresPage.typeModalTextDataDaUltimaColeta(alterarAbaColetasMedidoresDPO.getModalDataDaUltimaColeta());
		alteracaoAbaColetasMedidoresPage.clickModalSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoAbaColetasMedidoresPage.validateMessageDefault(alterarAbaColetasMedidoresDPO.getOperationMessage());
		validateGridFiltered();
	}
	
	private void validateGridFiltered() {
		alteracaoAbaColetasMedidoresPage.getGridLabelValueGrandeza(alterarAbaColetasMedidoresDPO.getGridColetas().get(0).getGrandeza(), 1);
		alteracaoAbaColetasMedidoresPage.getGridLabelValueDataDaUltimaColeta(alterarAbaColetasMedidoresDPO.getGridColetas().get(0).getDataDataUltimaColeta(), 1);
	}
}
