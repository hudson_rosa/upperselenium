package br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos.page;

import org.openqa.selenium.By;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoCircuitosPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//form/fieldset/div[7]/input";
	private static final String TEXT_NOME = "//*[@id='Circuito_Nome']"; 
	private static final String TEXT_ALIAS = "//*[@id='Circuito_Alias']"; 
	private static final String TEXT_MEDIDOR = "//*[@id='Medidor']"; 
	private static final String RDO_ENTRADA_DIGITAL = "//*[@id='Circuito_FonteDeDados']"; 
	private static final String RDO_ARQUIVO = "//*[@id='Circuito_FonteDeDados']";
	private static final String SELECT_ENTRADA_DIGITAL = "//*[@id='Circuito_EntradaDigital']";
	private static final String CHECK_CREG = "//*[@id='Circuito_GeraCreg']"; 
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	
	
	public void typeTextAlias(String value){
		typeText(TEXT_ALIAS, value);
	}	
	
	public void typeTextMedidor(String value){
		typeTextAutoCompleteSelect(TEXT_MEDIDOR, value);
		getBlockOverlay();
	}	
	
	public void typeRadioEntradaDigital(String value){
		typeRadioListOption(RDO_ENTRADA_DIGITAL, value);
	}
	
	public void typeRadioArquivo(String value){
		typeRadioListOption(RDO_ARQUIVO, value);
	}	
	
	public void typeSelectEntradaDigital(String value){		
		typeSelectComboOption(SELECT_ENTRADA_DIGITAL, value);
	}
	
	public void typeCheckBoxCreg(String value){
		typeCheckOptionByLabel(CHECK_CREG, value);
	}	
	
	public boolean isClickableButtonSalvar() {
		return isDisplayedElement(By.xpath(BUTTON_SALVAR));
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
		
}