package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

public class GridVisualizarEquacoesDPO {

	private String valueIdentificador;
	private String valuePonto;
	private String valueQuadrante;
	private String valueCompensacaoDePerdas;

	public String getValueIdentificador() {
		return valueIdentificador;
	}

	public void setValueIdentificador(String valueIdentificador) {
		this.valueIdentificador = valueIdentificador;
	}

	public String getValuePonto() {
		return valuePonto;
	}

	public void setValuePonto(String valuePonto) {
		this.valuePonto = valuePonto;
	}

	public String getValueQuadrante() {
		return valueQuadrante;
	}

	public void setValueQuadrante(String valueQuadrante) {
		this.valueQuadrante = valueQuadrante;
	}

	public String getValueCompensacaoDePerdas() {
		return valueCompensacaoDePerdas;
	}

	public void setValueCompensacaoDePerdas(String valueCompensacaoDePerdas) {
		this.valueCompensacaoDePerdas = valueCompensacaoDePerdas;
	}

}
