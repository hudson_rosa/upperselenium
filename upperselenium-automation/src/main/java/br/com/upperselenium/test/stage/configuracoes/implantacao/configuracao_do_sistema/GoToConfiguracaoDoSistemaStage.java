package br.com.upperselenium.test.stage.configuracoes.implantacao.configuracao_do_sistema;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.AreasMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.DropDownMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.SideBarImplantacaoPage;

public class GoToConfiguracaoDoSistemaStage extends BaseStage {
	private AreasMenuPage areasMenuPage;
	private DropDownMenuPage dropDownMenuPage;
	private SideBarImplantacaoPage sideBarImplantacaoPage;
	
	public GoToConfiguracaoDoSistemaStage() {}

	@Override
	public void initMappedPages() {
		areasMenuPage = initElementsFromPage(AreasMenuPage.class);
		dropDownMenuPage = initElementsFromPage(DropDownMenuPage.class);
		sideBarImplantacaoPage = initElementsFromPage(SideBarImplantacaoPage.class);
	}

	@Override
	public void runStage() {
		navigateToPage();	
	}
	
	private void navigateToPage() {
		waitForPageToLoad(TimePRM._20_SECS);
		areasMenuPage.clickConfiguracoes();
		waitForPageToLoadUntil10s();
		dropDownMenuPage.clickImplantacao();
		waitForPageToLoadUntil10s();
		sideBarImplantacaoPage.clickConfiguracaoDoSistema();
		waitForPageToLoadUntil10s();		
	}

	@Override
	public void runValidations() {}

}
