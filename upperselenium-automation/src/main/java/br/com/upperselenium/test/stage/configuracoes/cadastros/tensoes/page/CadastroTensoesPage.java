package br.com.upperselenium.test.stage.configuracoes.cadastros.tensoes.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroTensoesPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/form/fieldset/div[2]/input";   
	private static final String TEXT_VALOR = "//*[@id='Valor']";
	
	public void typeTextValor(String value){
		typeText(TEXT_VALOR, value);
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}