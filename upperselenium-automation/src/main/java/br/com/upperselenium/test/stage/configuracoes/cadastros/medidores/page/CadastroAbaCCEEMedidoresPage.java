package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaCCEEMedidoresPage extends BaseHelperPage{
	
	private static final String LINK_ABA_CCEE = ".//div[2]/div/div[2]/ul[2]/li[4]/a";
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 	
	private static final String TEXT_CODIGO_SCDE = "//*[@id='CodigoDoScde']"; 
	private static final String SELECT_QUADRANTE_DEL = "//*[@id='QuadranteDeEnergiaDel']";
	private static final String SELECT_QUADRANTE_REC = "//*[@id='QuadranteDeEnergiaRec']";
	private static final String TEXT_ALGORITMO_DE_COMP_DE_PERDAS = "//*[@id='AlgoritimoDeCompensacaoDePerdas']"; 
	private static final String CHECK_GERACAO_DE_XML = "//*[@id='GeracaoDeXmlDoScdeHabilitada']"; 
	
	public void typeTextCodigoSCDE(String value){
		typeText(TEXT_CODIGO_SCDE, value);
	}	

	public void typeSelectQuadranteDEL(String value){		
		typeSelectComboOption(SELECT_QUADRANTE_DEL, value);
	}
	
	public void typeSelectQuadranteREC(String value){		
		typeSelectComboOption(SELECT_QUADRANTE_REC, value);
	}
	
	public void typeTextAlgoritmoDeCompDePerdas(String value){
		typeText(TEXT_ALGORITMO_DE_COMP_DE_PERDAS, value);
	}	
	
	public void typeCheckGeracaoDeXML(String value){
		typeCheckOption(CHECK_GERACAO_DE_XML, value);
	}	
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
				
	public void clickAbaCCEE(){
		clickOnElement(LINK_ABA_CCEE);
	}
	
}