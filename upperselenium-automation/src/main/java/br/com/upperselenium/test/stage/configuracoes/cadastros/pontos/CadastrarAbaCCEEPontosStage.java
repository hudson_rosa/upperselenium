package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarAbaCCEEPontosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastroAbaCCEEPontosPage;

public class CadastrarAbaCCEEPontosStage extends BaseStage {
	private CadastrarAbaCCEEPontosDPO cadastrarAbaCCEEPontosDPO;
	private CadastroAbaCCEEPontosPage cadastroAbaCCEEPontosPage;
	
	public CadastrarAbaCCEEPontosStage(String dp) {
		cadastrarAbaCCEEPontosDPO = loadDataProviderFile(CadastrarAbaCCEEPontosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaCCEEPontosPage = initElementsFromPage(CadastroAbaCCEEPontosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaCCEEPontosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaCCEEPontosPage.clickAbaCCEE();
		FindElementUtil.acceptAlert();
		cadastroAbaCCEEPontosPage.typeTextCodigoCCEE(cadastrarAbaCCEEPontosDPO.getCodigoCCEE()+getRandomStringNonSpaced());
		cadastroAbaCCEEPontosPage.typeTextCodigoDoAtivo(cadastrarAbaCCEEPontosDPO.getCodigoDoAtivo()+getRandomStringNonSpaced());
		cadastroAbaCCEEPontosPage.typeTextNomeDoAtivo(cadastrarAbaCCEEPontosDPO.getNomeDoAtivo()+getRandomStringNonSpaced());
		cadastroAbaCCEEPontosPage.typeSelectAgenteConectante(cadastrarAbaCCEEPontosDPO.getAgenteConectante());
		cadastroAbaCCEEPontosPage.typeSelectAgenteConectado(cadastrarAbaCCEEPontosDPO.getAgenteConectado());
		cadastroAbaCCEEPontosPage.keyPageDown();
		cadastroAbaCCEEPontosPage.typeSelectAgenteDeMedicao(cadastrarAbaCCEEPontosDPO.getAgenteDeMedicao());
		cadastroAbaCCEEPontosPage.typeSelectTipo(cadastrarAbaCCEEPontosDPO.getTipo());
		cadastroAbaCCEEPontosPage.typeTextCodigoDoAgente(cadastrarAbaCCEEPontosDPO.getCodigoDoAgente()+getRandomStringNonSpaced());
		cadastroAbaCCEEPontosPage.typeTextNomeDoAgente(cadastrarAbaCCEEPontosDPO.getNomeDoAgente()+getRandomStringSpaced());
		cadastroAbaCCEEPontosPage.typeSelectClientScde(cadastrarAbaCCEEPontosDPO.getClientScde());
		cadastroAbaCCEEPontosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaCCEEPontosPage.validateMessageDefault(cadastrarAbaCCEEPontosDPO.getOperationMessage());	
	}

}
