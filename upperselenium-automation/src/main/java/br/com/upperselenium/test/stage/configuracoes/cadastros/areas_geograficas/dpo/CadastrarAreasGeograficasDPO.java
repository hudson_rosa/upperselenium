package br.com.upperselenium.test.stage.configuracoes.cadastros.areas_geograficas.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAreasGeograficasDPO extends BaseHelperDPO {

	private String nomeRandom;
	private String nomeDefault;

	public String getNomeRandom() {
		return nomeRandom;
	}

	public void setNomeRandom(String nomeRandom) {
		this.nomeRandom = nomeRandom;
	}

	public String getNomeDefault() {
		return nomeDefault;
	}

	public void setNomeDefault(String nomeDefault) {
		this.nomeDefault = nomeDefault;
	}

}
