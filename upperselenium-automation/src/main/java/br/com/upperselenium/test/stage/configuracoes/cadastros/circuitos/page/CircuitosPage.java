package br.com.upperselenium.test.stage.configuracoes.cadastros.circuitos.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CircuitosPage extends BaseHelperPage{
	
	private static final String LINK_NOVO = ".//div[2]/div/div[2]/p/a";
	
	public boolean isClickableLinkNovo() {
		return isDisplayedElement(LINK_NOVO);
	}
	
	public void clickNovo(){
		clickOnElement(LINK_NOVO);
		waitForPageToLoadUntil10s();
	}

}