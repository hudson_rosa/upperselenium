package br.com.upperselenium.test.flow.valida_busca;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.home.smartsearch.SmartSearchStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0002SmartSearchComandosMedidorFlow.class)
@FlowParams(idTest = "PIM-SmartSearch-T0002-ComandosMedidor", testDirPath = "", loginDirPath = "",
	goal = "Realiza a pesquisa, acesso ao link obtido e validar se o breadcrumbs corresponde ao título 'Template de Agendamento de Comandos de Medidor'.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0002SmartSearchComandosMedidorFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new SmartSearchStage(getDP("LoginDP.json"), getDP("SmartSearchPageComandosMedidorDP.json")));
	}	
}
