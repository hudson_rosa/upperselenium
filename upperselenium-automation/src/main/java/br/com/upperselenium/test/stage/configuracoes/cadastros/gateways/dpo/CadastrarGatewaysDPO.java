package br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarGatewaysDPO extends BaseHelperDPO {

	private String id;
	private String modelo;
	private String nome;
	private String descricao;
	private String entradaDigital1;
	private String entradaDigital2;
	private String entradaDigital3;
	private String entradaDigital4;
	private String tipoDeComunicacao;
	private String enderecoIPPorta;
	private String enderecoSecundarioIPPorta;
	private String idDeComunicacao;
	private String usuario;
	private String senha;
	private String comunicacaoCriptografada;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getEntradaDigital1() {
		return entradaDigital1;
	}

	public void setEntradaDigital1(String entradaDigital1) {
		this.entradaDigital1 = entradaDigital1;
	}

	public String getEntradaDigital2() {
		return entradaDigital2;
	}

	public void setEntradaDigital2(String entradaDigital2) {
		this.entradaDigital2 = entradaDigital2;
	}

	public String getEntradaDigital3() {
		return entradaDigital3;
	}

	public void setEntradaDigital3(String entradaDigital3) {
		this.entradaDigital3 = entradaDigital3;
	}

	public String getEntradaDigital4() {
		return entradaDigital4;
	}

	public void setEntradaDigital4(String entradaDigital4) {
		this.entradaDigital4 = entradaDigital4;
	}

	public String getTipoDeComunicacao() {
		return tipoDeComunicacao;
	}

	public void setTipoDeComunicacao(String tipoDeComunicacao) {
		this.tipoDeComunicacao = tipoDeComunicacao;
	}

	public String getEnderecoIPPorta() {
		return enderecoIPPorta;
	}

	public void setEnderecoIPPorta(String enderecoIPPorta) {
		this.enderecoIPPorta = enderecoIPPorta;
	}

	public String getEnderecoSecundarioIPPorta() {
		return enderecoSecundarioIPPorta;
	}

	public void setEnderecoSecundarioIPPorta(String enderecoSecundarioIPPorta) {
		this.enderecoSecundarioIPPorta = enderecoSecundarioIPPorta;
	}

	public String getIdDeComunicacao() {
		return idDeComunicacao;
	}

	public void setIdDeComunicacao(String idDeComunicacao) {
		this.idDeComunicacao = idDeComunicacao;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getComunicacaoCriptografada() {
		return comunicacaoCriptografada;
	}

	public void setComunicacaoCriptografada(String comunicacaoCriptografada) {
		this.comunicacaoCriptografada = comunicacaoCriptografada;
	}

}