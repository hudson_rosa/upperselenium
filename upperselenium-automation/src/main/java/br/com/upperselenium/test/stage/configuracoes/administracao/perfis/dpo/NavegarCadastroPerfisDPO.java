package br.com.upperselenium.test.stage.configuracoes.administracao.perfis.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarCadastroPerfisDPO extends BaseHelperDPO {

	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public String toString() {
		return "NavegarCadastroPerfisDPO [nome=" + nome + "]";
	}

}