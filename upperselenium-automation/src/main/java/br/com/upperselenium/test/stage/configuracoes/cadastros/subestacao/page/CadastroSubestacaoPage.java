package br.com.upperselenium.test.stage.configuracoes.cadastros.subestacao.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroSubestacaoPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='salvar']";   
	private static final String TEXT_NOME = "//*[@id='Subestacao_Nome']";
	private static final String TEXT_DESCRICAO = "//*[@id='Subestacao_Descricao']";
	private static final String TEXT_QUANTIDADE_DE_PONTOS = "//*[@id='QuantidadeDePontos']";
	private static final String TEXT_ALIMENTACAO_AUXILIAR = "//*[@id='Subestacao_AlimentacaoAuxiliar']";
	private static final String TEXT_OBSERVACOES = "//*[@id='Subestacao_Observacao']";
	private static final String TEXT_PROPRIEDADE = "//*[@id='Subestacao_Propriedade']";
	private static final String TEXT_FRONTEIRA = "//*[@id='Fronteira']";
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}
	
	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}
	
	public void typeTextQuantidadeDePontos(String value){
		typeText(TEXT_QUANTIDADE_DE_PONTOS, value);
	}
	
	public void typeTextAlimentacaoAuxiliar(String value){
		typeText(TEXT_ALIMENTACAO_AUXILIAR, value);
	}
	
	public void typeTextObservacoes(String value){
		typeText(TEXT_OBSERVACOES, value);
	}
	
	public void typeTextPropriedade(String value){
		typeText(TEXT_PROPRIEDADE, value);
	}
	
	public void typeTextFronteira(String value){
		typeTextAutoCompleteSelect(TEXT_FRONTEIRA, value);
	}

	public void keyPageDown(){
		useKey(TEXT_PROPRIEDADE, Keys.PAGE_DOWN);
	}
		
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}