package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.implantacao.auto_discovery.GoToAutoDiscoveryStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.auto_discovery.NavegarCadastroAutoDiscoveryStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0005AutoDiscoveryNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0005-AutoDiscovery", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Auto-Discovery validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0005AutoDiscoveryNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToAutoDiscoveryStage());
		addStage(new NavegarCadastroAutoDiscoveryStage(getDP("NavegarCadastroAutoDiscoveryDP.json")));
	}	
}
