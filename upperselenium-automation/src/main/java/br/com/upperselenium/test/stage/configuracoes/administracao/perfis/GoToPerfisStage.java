package br.com.upperselenium.test.stage.configuracoes.administracao.perfis;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.AreasMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.DropDownMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.SideBarAdministracaoPage;

public class GoToPerfisStage extends BaseStage {
	private AreasMenuPage areasMenuPage;
	private DropDownMenuPage dropDownMenuPage;
	private SideBarAdministracaoPage sideBarAdministracaoPage;
	
	public GoToPerfisStage() {}

	@Override
	public void initMappedPages() {
		areasMenuPage = initElementsFromPage(AreasMenuPage.class);
		dropDownMenuPage = initElementsFromPage(DropDownMenuPage.class);
		sideBarAdministracaoPage = initElementsFromPage(SideBarAdministracaoPage.class);
	}

	@Override
	public void runStage() {
		navigateToPage();	
	}
	
	private void navigateToPage() {
		waitForPageToLoad(TimePRM._20_SECS);
		areasMenuPage.clickConfiguracoes();
		waitForPageToLoadUntil10s();
		dropDownMenuPage.clickAdministracao();
		waitForPageToLoadUntil10s();
		sideBarAdministracaoPage.clickPerfis();
		waitForPageToLoadUntil10s();		
	}

	@Override
	public void runValidations() {}

}
