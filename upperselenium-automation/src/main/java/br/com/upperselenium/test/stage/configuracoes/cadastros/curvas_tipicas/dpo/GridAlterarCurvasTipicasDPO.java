package br.com.upperselenium.test.stage.configuracoes.cadastros.curvas_tipicas.dpo;

public class GridAlterarCurvasTipicasDPO {

	private String flagItem;
	private String criterioDeDados;
	private String f1;

	public String getFlagItem() {
		return flagItem;
	}

	public void setFlagItem(String flagItem) {
		this.flagItem = flagItem;
	}

	public String getCriterioDeDados() {
		return criterioDeDados;
	}

	public void setCriterioDeDados(String criterioDeDados) {
		this.criterioDeDados = criterioDeDados;
	}

	public String getF1() {
		return f1;
	}

	public void setF1(String f1) {
		this.f1 = f1;
	}

}
