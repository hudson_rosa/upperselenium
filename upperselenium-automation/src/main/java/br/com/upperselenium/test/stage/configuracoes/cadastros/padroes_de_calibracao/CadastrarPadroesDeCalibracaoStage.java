package br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao.dpo.CadastrarPadroesDeCalibracaoDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao.page.CadastroPadroesDeCalibracaoPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao.page.PadroesDeCalibracaoPage;

public class CadastrarPadroesDeCalibracaoStage extends BaseStage {
	private CadastrarPadroesDeCalibracaoDPO cadastrarPadroesDeCalibracaoDPO;
	private PadroesDeCalibracaoPage padroesDeCalibracaoPage;
	private CadastroPadroesDeCalibracaoPage cadastroPadroesDeCalibracaoPage;
	
	public CadastrarPadroesDeCalibracaoStage(String dp) {
		cadastrarPadroesDeCalibracaoDPO = loadDataProviderFile(CadastrarPadroesDeCalibracaoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		padroesDeCalibracaoPage = initElementsFromPage(PadroesDeCalibracaoPage.class);
		cadastroPadroesDeCalibracaoPage = initElementsFromPage(CadastroPadroesDeCalibracaoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarPadroesDeCalibracaoDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		padroesDeCalibracaoPage.clickNovo();
		cadastroPadroesDeCalibracaoPage.typeTextIdentificacao(cadastrarPadroesDeCalibracaoDPO.getIdentificacao()+getRandomStringSpaced());		
		cadastroPadroesDeCalibracaoPage.typeTextModelo(cadastrarPadroesDeCalibracaoDPO.getModelo());		
		cadastroPadroesDeCalibracaoPage.typeTextNumeroDeSerie(cadastrarPadroesDeCalibracaoDPO.getNumeroDeSerie()+getRandomStringNonSpaced());		
		cadastroPadroesDeCalibracaoPage.typeTextNumeroDoCertificadoDeCalibracao(cadastrarPadroesDeCalibracaoDPO.getNumeroDoCertificadoDeCalibracao()+getRandomStringNonSpaced());		
		cadastroPadroesDeCalibracaoPage.keyPageDown();
		cadastroPadroesDeCalibracaoPage.typeTextClasse(cadastrarPadroesDeCalibracaoDPO.getClasse());
		cadastroPadroesDeCalibracaoPage.typeTextDataDeValidade(cadastrarPadroesDeCalibracaoDPO.getDataDeValidade());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaNominalEnergiaAtivaFornecida(cadastrarPadroesDeCalibracaoDPO.getCargaNominalEnergiaAtivaFornecida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaNominalEnergiaAtivaRecebida(cadastrarPadroesDeCalibracaoDPO.getCargaNominalEnergiaAtivaRecebida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaNominalEnergiaReativaFornecida(cadastrarPadroesDeCalibracaoDPO.getCargaNominalEnergiaReativaFornecida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaNominalEnergiaReativaRecebida(cadastrarPadroesDeCalibracaoDPO.getCargaNominalEnergiaReativaRecebida());
		cadastroPadroesDeCalibracaoPage.typeTextCargaIndutivaEnergiaAtivaFornecida(cadastrarPadroesDeCalibracaoDPO.getCargaIndutivaEnergiaAtivaFornecida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaIndutivaEnergiaAtivaRecebida(cadastrarPadroesDeCalibracaoDPO.getCargaIndutivaEnergiaAtivaRecebida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaIndutivaEnergiaReativaFornecida(cadastrarPadroesDeCalibracaoDPO.getCargaIndutivaEnergiaReativaFornecida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaIndutivaEnergiaReativaRecebida(cadastrarPadroesDeCalibracaoDPO.getCargaIndutivaEnergiaReativaRecebida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaCapacitivaEnergiaAtivaFornecida(cadastrarPadroesDeCalibracaoDPO.getCargaCapacitivaEnergiaAtivaFornecida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaCapacitivaEnergiaAtivaRecebida(cadastrarPadroesDeCalibracaoDPO.getCargaCapacitivaEnergiaAtivaRecebida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaCapacitivaEnergiaReativaFornecida(cadastrarPadroesDeCalibracaoDPO.getCargaCapacitivaEnergiaReativaFornecida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaCapacitivaEnergiaReativaRecebida(cadastrarPadroesDeCalibracaoDPO.getCargaCapacitivaEnergiaReativaRecebida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaPequenaEnergiaAtivaFornecida(cadastrarPadroesDeCalibracaoDPO.getCargaPequenaEnergiaAtivaFornecida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaPequenaEnergiaAtivaRecebida(cadastrarPadroesDeCalibracaoDPO.getCargaPequenaEnergiaAtivaRecebida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaPequenaEnergiaReativaFornecida(cadastrarPadroesDeCalibracaoDPO.getCargaPequenaEnergiaReativaFornecida());		
		cadastroPadroesDeCalibracaoPage.typeTextCargaPequenaEnergiaReativaRecebida(cadastrarPadroesDeCalibracaoDPO.getCargaPequenaEnergiaReativaRecebida());		
		cadastroPadroesDeCalibracaoPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroPadroesDeCalibracaoPage.validateMessageDefault(cadastrarPadroesDeCalibracaoDPO.getOperationMessage());
	}

}
