package br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.dpo.AlterarUnidadesConsumidorasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.page.AlteracaoUnidadesConsumidorasPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.page.UnidadesConsumidorasPage;

public class AlterarUnidadesConsumidorasStage extends BaseStage {
	private AlterarUnidadesConsumidorasDPO alterarUnidadesConsumidorasDPO;
	private UnidadesConsumidorasPage unidadesConsumidorasPage;
	private AlteracaoUnidadesConsumidorasPage alteracaoUnidadesConsumidorasPage;
	
	public AlterarUnidadesConsumidorasStage(String dp) {
		alterarUnidadesConsumidorasDPO = loadDataProviderFile(AlterarUnidadesConsumidorasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		unidadesConsumidorasPage = initElementsFromPage(UnidadesConsumidorasPage.class);
		alteracaoUnidadesConsumidorasPage = initElementsFromPage(AlteracaoUnidadesConsumidorasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(alterarUnidadesConsumidorasDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		alteracaoUnidadesConsumidorasPage.typeTextNumero(alterarUnidadesConsumidorasDPO.getNumero()+getRandomNumber());		
		alteracaoUnidadesConsumidorasPage.typeTextNome(alterarUnidadesConsumidorasDPO.getNome()+getRandomNumberSpaced());		
		alteracaoUnidadesConsumidorasPage.typeSelectRamoDeAtividade(alterarUnidadesConsumidorasDPO.getRamoDeAtividade());		
		alteracaoUnidadesConsumidorasPage.typeSelectEtapa(alterarUnidadesConsumidorasDPO.getEtapa());		
		alteracaoUnidadesConsumidorasPage.typeTextPostoTarifario(alterarUnidadesConsumidorasDPO.getPostoTarifario());		
		alteracaoUnidadesConsumidorasPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		alteracaoUnidadesConsumidorasPage.validateMessageDefault(alterarUnidadesConsumidorasDPO.getOperationMessage());
	}
	
}
