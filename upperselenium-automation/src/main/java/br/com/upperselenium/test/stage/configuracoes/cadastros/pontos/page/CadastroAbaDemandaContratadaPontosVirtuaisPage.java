package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaDemandaContratadaPontosVirtuaisPage extends BaseHelperPage{
	
	private static final String LINK_ABA_DEMANDA_CONTRATADA = "//*[@id='tabs']/ul/li[4]/a";
	private static final String LINK_NOVA = "//*[@id='geral']/div[1]/p/a";
	private static final String BUTTON_ADICIONAR = "//*[@id='salvar']"; 	
	private static final String TEXT_INICIO = "//*[@id='FormData_InicioVigencia']"; 
	private static final String TEXT_LIMITE_PONTA = "//*[@id='FormData_LimitePonta']"; 
	private static final String TEXT_VALOR_PONTA = "//*[@id='FormData_ValorPonta']"; 
	private static final String TEXT_LIMITE_FORA_PONTA = "//*[@id='FormData_LimiteForaPonta']"; 
	private static final String TEXT_VALOR_FORA_PONTA = "//*[@id='FormData_ValorForaPonta']"; 
	private static final String SELECT_TIPO = "//*[@id='FormData_NaturezaSelecionada']";
	
	private static final String GRID_FILTER_TEXT_INICIO_DA_VIGENCIA = "//*[@id='demandasContratadas']/thead/tr[1]/th[1]/span/input";
	private static final String GRID_FILTER_TEXT_LIMITE_PONTA = "//*[@id='demandasContratadas']/thead/tr[1]/th[2]/span/input";
	private static final String GRID_FILTER_TEXT_LIMITE_FORA_PONTA = "//*[@id='demandasContratadas']/thead/tr[1]/th[3]/span/input";
	private static final String GRID_FILTER_TEXT_VALOR_PONTA = "//*[@id='demandasContratadas']/thead/tr[1]/th[4]/span/input";
	private static final String GRID_FILTER_TEXT_VALOR_FORA_PONTA = "//*[@id='demandasContratadas']/thead/tr[1]/th[5]/span/input";
	private static final String GRID_FILTER_TEXT_TIPO = "//*[@id='demandasContratadas']/thead/tr[1]/th[6]/span/input";
	
	private static final String GRID_TEXT_INICIO_DA_VIGENCIA = "//*[@id='demandasContratadas']/tbody/tr[%i%]/td[1]";
	private static final String GRID_TEXT_LIMITE_PONTA = "//*[@id='demandasContratadas']/tbody/tr[%i%]/td[2]";
	private static final String GRID_TEXT_LIMITE_FORA_PONTA = "//*[@id='demandasContratadas']/tbody/tr[%i%]/td[3]";
	private static final String GRID_TEXT_VALOR_PONTA = "//*[@id='demandasContratadas']/tbody/tr[%i%]/td[4]";
	private static final String GRID_TEXT_VALOR_FORA_PONTA = "//*[@id='demandasContratadas']/tbody/tr[%i%]/td[5]";
	private static final String GRID_TEXT_TIPO = "//*[@id='demandasContratadas']/tbody/tr[%i%]/td[6]";
	
	public void typeTextInicio(String value){
		typeDatePickerDefault(TEXT_INICIO, value);
	}	
	
	public void typeTextLimitePonta(String value){
		typeText(TEXT_LIMITE_PONTA, value);
	}	
	
	public void typeTextValorPonta(String value){
		typeText(TEXT_VALOR_PONTA, value);
	}	
	
	public void typeTextLimiteForaPonta(String value){
		typeText(TEXT_LIMITE_FORA_PONTA, value);
	}	
	
	public void typeTextValorForaPonta(String value){
		typeText(TEXT_VALOR_FORA_PONTA, value);
	}	
	
	public void typeSelectTipo(String value){		
		typeSelectComboOption(SELECT_TIPO, value);
	}
	
	public void typeTextFilterInicioDaVigencia(String value){
		typeText(GRID_FILTER_TEXT_INICIO_DA_VIGENCIA, value);
	}	
	
	public void typeTextFilterLimitePonta(String value){
		typeText(GRID_FILTER_TEXT_LIMITE_PONTA, value);
	}	
	
	public void typeTextFilterLimiteForaPonta(String value){
		typeText(GRID_FILTER_TEXT_LIMITE_FORA_PONTA, value);
	}	
	
	public void typeTextFilterValorPonta(String value){
		typeText(GRID_FILTER_TEXT_VALOR_PONTA, value);
	}	
	
	public void typeTextFilterValorForaPonta(String value){
		typeText(GRID_FILTER_TEXT_VALOR_FORA_PONTA, value);
	}	
	
	public void typeTextFilterTipo(String value){
		typeText(GRID_FILTER_TEXT_TIPO, value);
	}	
	
	public void getGridLabelInicioDaVigencia(String value, int index){		
		waitForATime(TimePRM._2_SECS);
		getGridLabel(GRID_TEXT_INICIO_DA_VIGENCIA, value, index);
	}
	
	public void getGridLabelLimitePonta(String value, int index){		
		getGridLabel(GRID_TEXT_LIMITE_PONTA, value, index);
	}
	
	public void getGridLabelLimiteForaPonta(String value, int index){		
		getGridLabel(GRID_TEXT_LIMITE_FORA_PONTA, value, index);
	}
	
	public void getGridLabelValorPonta(String value, int index){		
		getGridLabel(GRID_TEXT_VALOR_PONTA, value, index);
	}
	
	public void getGridLabelValorForaPonta(String value, int index){		
		getGridLabel(GRID_TEXT_VALOR_FORA_PONTA, value, index);
	}
	
	public void getGridLabelTipo(String value, int index){		
		getGridLabel(GRID_TEXT_TIPO, value, index);
	}
	
	public void keyPageUp(){
		useKey(BUTTON_ADICIONAR, Keys.PAGE_UP);
	}	
	
	public void keyPageDown(){
		useKey(TEXT_LIMITE_FORA_PONTA, Keys.PAGE_DOWN);
	}	
	
	public void clickAdicionar(){
		clickOnElement(BUTTON_ADICIONAR);
	}
	
	public void clickNova(){
		clickOnElement(LINK_NOVA);
	}
				
	public void clickAbaDemandaContratada(){
		clickOnElement(LINK_ABA_DEMANDA_CONTRATADA);
	}
	
}