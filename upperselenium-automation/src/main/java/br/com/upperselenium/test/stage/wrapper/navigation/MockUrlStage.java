package br.com.upperselenium.test.stage.wrapper.navigation;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.WebDriverMaster;

public class MockUrlStage extends BaseStage {
	private String urlMocked;
	
	/**
	 * Na classe de Flow, declare o Stage da seguinte maneira: <br><br>
	 * 				addStage(new MockUrlStage("http://localhost/app..."));
	 * @param url
	 */
	
	public MockUrlStage(String url) {
		urlMocked = url;
	}
	
	@Override
	public void initMappedPages() {}
	
	@Override
	public void runStage() {
		WebDriverMaster.getWebDriver().get(urlMocked);			
	}

	@Override
	public void runValidations() {}

}
