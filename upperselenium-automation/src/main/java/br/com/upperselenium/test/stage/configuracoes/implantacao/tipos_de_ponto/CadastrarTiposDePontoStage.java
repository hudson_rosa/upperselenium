package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.StringUtil;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.dpo.CadastrarTiposDePontoDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.page.CadastroTiposDePontoPage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto.page.TiposDePontoPage;

public class CadastrarTiposDePontoStage extends BaseStage {
	private CadastrarTiposDePontoDPO cadastrarTiposDePontoDPO;
	private TiposDePontoPage tiposDePontoPage;
	private CadastroTiposDePontoPage cadastroTiposDePontoPage;
	
	public CadastrarTiposDePontoStage(String dp) {
		cadastrarTiposDePontoDPO = loadDataProviderFile(CadastrarTiposDePontoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDePontoPage = initElementsFromPage(TiposDePontoPage.class);
		cadastroTiposDePontoPage = initElementsFromPage(CadastroTiposDePontoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarTiposDePontoDPO.getIssue());	
		execute();
	}
	
	private void execute() {
		tiposDePontoPage.clickNovo();
		if(StringUtil.isNotBlankOrNotNull(cadastrarTiposDePontoDPO.getNomeRandom())){
			cadastroTiposDePontoPage.typeTextNomeRandom(cadastrarTiposDePontoDPO.getNomeRandom()+getRandomStringSpaced());		
		} else {
			cadastroTiposDePontoPage.typeTextNomeDefault(cadastrarTiposDePontoDPO.getNomeDefault());		
		}
		cadastroTiposDePontoPage.typeTextDescricao(cadastrarTiposDePontoDPO.getDescricao()+getRandomStringSpaced());		
		cadastroTiposDePontoPage.typeSelectGrupoDeMedicao(cadastrarTiposDePontoDPO.getGrupoDeMedicao());		
		cadastroTiposDePontoPage.typeCheckMonitoraDadosDuplicados(cadastrarTiposDePontoDPO.getMonitoraDadosDuplicados());		
		cadastroTiposDePontoPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroTiposDePontoPage.validateMessageDefault(cadastrarTiposDePontoDPO.getOperationMessage());
	}
	
}
