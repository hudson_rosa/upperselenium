package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarTiposDeMedidorDPO extends BaseHelperDPO {

	private String labelMarca;
	private String labelModelo;
	private String labelDescricao;
	private String labelAbas;

	public String getLabelMarca() {
		return labelMarca;
	}

	public void setLabelMarca(String labelMarca) {
		this.labelMarca = labelMarca;
	}

	public String getLabelModelo() {
		return labelModelo;
	}

	public void setLabelModelo(String labelModelo) {
		this.labelModelo = labelModelo;
	}

	public String getLabelDescricao() {
		return labelDescricao;
	}

	public void setLabelDescricao(String labelDescricao) {
		this.labelDescricao = labelDescricao;
	}

	public String getLabelAbas() {
		return labelAbas;
	}

	public void setLabelAbas(String labelAbas) {
		this.labelAbas = labelAbas;
	}

	@Override
	public String toString() {
		return "NavegarTiposDeMedidorDPO [labelMarca=" + labelMarca + ", labelModelo=" + labelModelo
				+ ", labelDescricao=" + labelDescricao + ", labelAbas=" + labelAbas + "]";
	}

}