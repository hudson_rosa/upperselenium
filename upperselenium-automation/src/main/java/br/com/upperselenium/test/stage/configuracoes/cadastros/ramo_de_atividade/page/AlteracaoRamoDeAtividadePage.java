package br.com.upperselenium.test.stage.configuracoes.cadastros.ramo_de_atividade.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class AlteracaoRamoDeAtividadePage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/form/fieldset/div[3]/input";   
	private static final String TEXT_NOME = "//*[@id='RamoDeAtividade_Nome']";
	private static final String TEXT_DESCRICAO = "//*[@id='RamoDeAtividade_Descricao']";
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}
	
	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}