package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.GoToUsuariosStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.NavegarAlteracaoUsuariosStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.NavegarCadastroUsuariosStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.NavegarRegistroLinkUsuariosStage;
import br.com.upperselenium.test.stage.configuracoes.administracao.usuarios.NavegarUsuariosStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0004UsuariosNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0004-Usuarios", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Usuários validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0004UsuariosNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToUsuariosStage());
		addStage(new NavegarUsuariosStage(getDP("NavegarUsuariosDP.json")));
		addStage(new GoToUsuariosStage());
		addStage(new NavegarCadastroUsuariosStage(getDP("NavegarCadastroUsuariosDP.json")));
		addStage(new NavegarRegistroLinkUsuariosStage(getDP("NavegarRegistroLinkUsuariosDP.json")));
		addStage(new NavegarAlteracaoUsuariosStage(getDP("NavegarAlteracaoUsuariosDP.json")));
	}	
}
