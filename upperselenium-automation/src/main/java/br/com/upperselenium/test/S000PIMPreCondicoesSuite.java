package br.com.upperselenium.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseSuite;
import br.com.upperselenium.base.annotation.SuiteParams;
import br.com.upperselenium.test.flow.pre_condicoes.P0001TiposDePontoPreCondicoesFlow;


@RunWith(Suite.class)

@SuiteClasses({
	P0001TiposDePontoPreCondicoesFlow.class	
	})

@SuiteParams(description="PIM - Validação de Cadastros")
public class S000PIMPreCondicoesSuite extends BaseSuite {}
