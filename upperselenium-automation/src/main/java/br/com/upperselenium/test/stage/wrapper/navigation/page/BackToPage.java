package br.com.upperselenium.test.stage.wrapper.navigation.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class BackToPage extends BaseHelperPage{
	
	private static final String LINK_VOLTAR = ".//div//a[contains(text(), 'Voltar')]";	
		
	public boolean isClickableButtonVoltar() {
		return isDisplayedElement(LINK_VOLTAR);
	}

	public void clickVoltar(){
		clickOnElement(LINK_VOLTAR);
	}

}