package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.auto_discovery_pendentes.GoToAutoDiscoveryStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0004AutoDiscoveryCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0004-AutoDiscovery", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de listagem de Auto Discovery Pendentes está acessível.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0004AutoDiscoveryCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToAutoDiscoveryStage());
	}	
}
