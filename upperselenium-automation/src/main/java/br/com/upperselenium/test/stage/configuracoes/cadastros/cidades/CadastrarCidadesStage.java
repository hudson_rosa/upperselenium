package br.com.upperselenium.test.stage.configuracoes.cadastros.cidades;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.cidades.dpo.CadastrarCidadesDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.cidades.page.CadastroCidadesPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.cidades.page.CidadesPage;

public class CadastrarCidadesStage extends BaseStage {
	private CadastrarCidadesDPO cadastrarCidadesDPO;
	private CidadesPage cidadesPage;
	private CadastroCidadesPage cadastroCidadesPage;
	
	public CadastrarCidadesStage(String dp) {
		cadastrarCidadesDPO = loadDataProviderFile(CadastrarCidadesDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cidadesPage = initElementsFromPage(CidadesPage.class);
		cadastroCidadesPage = initElementsFromPage(CadastroCidadesPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarCidadesDPO.getIssue());
		execute();		
	}
	
	private void execute() {
		cidadesPage.clickNova();
		cadastroCidadesPage.typeTextNome(cadastrarCidadesDPO.getNome()+getRandomStringSpaced());
		cadastroCidadesPage.typeSelectEstado(cadastrarCidadesDPO.getEstado());
		cadastroCidadesPage.typeSelectRegiao(cadastrarCidadesDPO.getRegiao());
		cadastroCidadesPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroCidadesPage.validateMessageDefault(cadastrarCidadesDPO.getOperationMessage());
	}
	
}
