package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo;

import java.util.List;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class CadastrarAbaOpcoesDeManobrasPontosVirtuaisDPO extends BaseHelperDPO {

	private List<GridAdicionarChaveManobrasDPO> gridChave;
	private List<GridCombinacoesGeradasDPO> gridCombinacoesGeradas;
	public List<GridAdicionarChaveManobrasDPO> getGridChave() {
		return gridChave;
	}
	public void setGridChave(List<GridAdicionarChaveManobrasDPO> gridChave) {
		this.gridChave = gridChave;
	}
	public List<GridCombinacoesGeradasDPO> getGridCombinacoesGeradas() {
		return gridCombinacoesGeradas;
	}
	public void setGridCombinacoesGeradas(List<GridCombinacoesGeradasDPO> gridCombinacoesGeradas) {
		this.gridCombinacoesGeradas = gridCombinacoesGeradas;
	}


}