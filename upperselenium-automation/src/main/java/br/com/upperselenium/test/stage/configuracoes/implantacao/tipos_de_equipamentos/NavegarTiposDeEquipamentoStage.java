package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.dpo.NavegarTiposDeEquipamentoDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.page.TiposDeEquipamentoPage;

public class NavegarTiposDeEquipamentoStage extends BaseStage {
	private NavegarTiposDeEquipamentoDPO navegarTiposDeEquipamentoDPO;
	private TiposDeEquipamentoPage tiposDeEquipamentoPage;
	
	public NavegarTiposDeEquipamentoStage(String dp) {
		navegarTiposDeEquipamentoDPO = loadDataProviderFile(NavegarTiposDeEquipamentoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		tiposDeEquipamentoPage = initElementsFromPage(TiposDeEquipamentoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarTiposDeEquipamentoDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		tiposDeEquipamentoPage.getLabelHeaderMarca(navegarTiposDeEquipamentoDPO.getLabelMarca());
		tiposDeEquipamentoPage.getLabelHeaderModelo(navegarTiposDeEquipamentoDPO.getLabelModelo());
		tiposDeEquipamentoPage.getLabelHeaderEspecie(navegarTiposDeEquipamentoDPO.getLabelEspecie());
		tiposDeEquipamentoPage.getBreadcrumbsText(navegarTiposDeEquipamentoDPO.getBreadcrumbs());
		tiposDeEquipamentoPage.clickUpToBreadcrumbs(navegarTiposDeEquipamentoDPO.getUpToBreadcrumb());
		tiposDeEquipamentoPage.getWelcomeTitle(navegarTiposDeEquipamentoDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
