package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarAbaMedidoresPontosDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastroAbaMedidoresPontosPage;

public class CadastrarAbaMedidoresPontosStage extends BaseStage {
	private CadastrarAbaMedidoresPontosDPO cadastrarAbaMedidoresPontosDPO;
	private CadastroAbaMedidoresPontosPage cadastroAbaMedidoresPontosPage;
	
	public CadastrarAbaMedidoresPontosStage(String dp) {
		cadastrarAbaMedidoresPontosDPO = loadDataProviderFile(CadastrarAbaMedidoresPontosDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaMedidoresPontosPage = initElementsFromPage(CadastroAbaMedidoresPontosPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaMedidoresPontosDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaMedidoresPontosPage.clickAbaMedidores();
		FindElementUtil.acceptAlert();
		cadastroAbaMedidoresPontosPage.typeTextPrincipal(cadastrarAbaMedidoresPontosDPO.getPrincipal()+getRandomStringSpaced());
		cadastroAbaMedidoresPontosPage.getLabelNumeroDeSeriePrincipal(cadastrarAbaMedidoresPontosDPO.getNumeroDeSeriePrincipal());
		cadastroAbaMedidoresPontosPage.getLabelNumeroInternoPrincipal(cadastrarAbaMedidoresPontosDPO.getNumeroInternoPrincipal());
		cadastroAbaMedidoresPontosPage.getLabelCodigoScdePrincipal(cadastrarAbaMedidoresPontosDPO.getCodigoSCDEPrincipal()+getRandomStringNonSpaced());
		cadastroAbaMedidoresPontosPage.keyPageDown();
		cadastroAbaMedidoresPontosPage.typeTextRetaguarda(cadastrarAbaMedidoresPontosDPO.getRetaguarda()+getRandomStringSpaced());
		cadastroAbaMedidoresPontosPage.getLabelNumeroDeSerieRetaguarda(cadastrarAbaMedidoresPontosDPO.getNumeroDeSerieRetaguarda());
		cadastroAbaMedidoresPontosPage.getLabelNumeroInternoRetaguarda(cadastrarAbaMedidoresPontosDPO.getNumeroInternoRetaguarda());
		cadastroAbaMedidoresPontosPage.getLabelCodigoScdeRetaguarda(cadastrarAbaMedidoresPontosDPO.getCodigoSCDERetaguarda()+getRandomStringNonSpaced());
		cadastroAbaMedidoresPontosPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaMedidoresPontosPage.validateMessageDefault(cadastrarAbaMedidoresPontosDPO.getOperationMessage());	
	}

}
