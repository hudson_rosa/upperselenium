package br.com.upperselenium.test.stage.medicao.comandos_do_medidor.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarComandosDeMedidorAgendarParametrosDPO extends BaseHelperDPO {

	private String labelTipo;
	private String labelInstante;
	private String labelExpiracao;
	private String labelMedidoresPorParametros;
	private String labelPasta;
	private String labelTags;
	private String labelEtapa;
	private String labelTipoDeComunicacao;
	private String labelTipoDePonto;
	private String labelRepeticao;

	public String getLabelTipo() {
		return labelTipo;
	}

	public void setLabelTipo(String labelTipo) {
		this.labelTipo = labelTipo;
	}

	public String getLabelInstante() {
		return labelInstante;
	}

	public void setLabelInstante(String labelInstante) {
		this.labelInstante = labelInstante;
	}

	public String getLabelExpiracao() {
		return labelExpiracao;
	}

	public void setLabelExpiracao(String labelExpiracao) {
		this.labelExpiracao = labelExpiracao;
	}

	public String getLabelMedidoresPorParametros() {
		return labelMedidoresPorParametros;
	}

	public void setLabelMedidoresPorParametros(String labelMedidoresPorParametros) {
		this.labelMedidoresPorParametros = labelMedidoresPorParametros;
	}

	public String getLabelPasta() {
		return labelPasta;
	}

	public void setLabelPasta(String labelPasta) {
		this.labelPasta = labelPasta;
	}

	public String getLabelTags() {
		return labelTags;
	}

	public void setLabelTags(String labelTags) {
		this.labelTags = labelTags;
	}

	public String getLabelEtapa() {
		return labelEtapa;
	}

	public void setLabelEtapa(String labelEtapa) {
		this.labelEtapa = labelEtapa;
	}

	public String getLabelTipoDeComunicacao() {
		return labelTipoDeComunicacao;
	}

	public void setLabelTipoDeComunicacao(String labelTipoDeComunicacao) {
		this.labelTipoDeComunicacao = labelTipoDeComunicacao;
	}

	public String getLabelTipoDePonto() {
		return labelTipoDePonto;
	}

	public void setLabelTipoDePonto(String labelTipoDePonto) {
		this.labelTipoDePonto = labelTipoDePonto;
	}

	public String getLabelRepeticao() {
		return labelRepeticao;
	}

	public void setLabelRepeticao(String labelRepeticao) {
		this.labelRepeticao = labelRepeticao;
	}

	@Override
	public String toString() {
		return "NavegarComandosDeMedidorAgendarParametrosDPO [labelTipo=" + labelTipo + ", labelInstante="
				+ labelInstante + ", labelExpiracao=" + labelExpiracao + ", labelMedidoresPorParametros="
				+ labelMedidoresPorParametros + ", labelPasta=" + labelPasta + ", labelTags=" + labelTags
				+ ", labelEtapa=" + labelEtapa + ", labelTipoDeComunicacao=" + labelTipoDeComunicacao
				+ ", labelTipoDePonto=" + labelTipoDePonto + ", labelRepeticao=" + labelRepeticao + "]";
	}

}