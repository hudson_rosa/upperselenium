package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo.CadastrarAbaInspecaoLogicaMedidoresDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page.CadastroAbaInspecaoLogicaMedidoresPage;

public class CadastrarAbaInspecaoLogicaMedidoresStage extends BaseStage {
	private CadastrarAbaInspecaoLogicaMedidoresDPO cadastrarAbaInspecaoLogicaMedidoresDPO;
	private CadastroAbaInspecaoLogicaMedidoresPage cadastroAbaInspecaoLogicaMedidoresPage;
	
	public CadastrarAbaInspecaoLogicaMedidoresStage(String dp) {
		cadastrarAbaInspecaoLogicaMedidoresDPO = loadDataProviderFile(CadastrarAbaInspecaoLogicaMedidoresDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaInspecaoLogicaMedidoresPage = initElementsFromPage(CadastroAbaInspecaoLogicaMedidoresPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaInspecaoLogicaMedidoresDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaInspecaoLogicaMedidoresPage.clickAbaInspecaoLogica();
		FindElementUtil.acceptAlert();
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheckHabilitada(cadastrarAbaInspecaoLogicaMedidoresDPO.getHabilitada());
		cadastroAbaInspecaoLogicaMedidoresPage.typeTextEnderecoIP(cadastrarAbaInspecaoLogicaMedidoresDPO.getEnderecoIP());
		cadastroAbaInspecaoLogicaMedidoresPage.typeTextNatScde(cadastrarAbaInspecaoLogicaMedidoresDPO.getNatScde());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheckTcpClient(cadastrarAbaInspecaoLogicaMedidoresDPO.getTcpClient());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck1(cadastrarAbaInspecaoLogicaMedidoresDPO.get_1());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck2(cadastrarAbaInspecaoLogicaMedidoresDPO.get_2());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck3(cadastrarAbaInspecaoLogicaMedidoresDPO.get_3());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck4(cadastrarAbaInspecaoLogicaMedidoresDPO.get_4());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck5(cadastrarAbaInspecaoLogicaMedidoresDPO.get_5());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck6(cadastrarAbaInspecaoLogicaMedidoresDPO.get_6());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck7(cadastrarAbaInspecaoLogicaMedidoresDPO.get_7());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck8(cadastrarAbaInspecaoLogicaMedidoresDPO.get_8());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck9(cadastrarAbaInspecaoLogicaMedidoresDPO.get_9());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck10(cadastrarAbaInspecaoLogicaMedidoresDPO.get_10());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck11(cadastrarAbaInspecaoLogicaMedidoresDPO.get_11());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck12(cadastrarAbaInspecaoLogicaMedidoresDPO.get_12());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck13(cadastrarAbaInspecaoLogicaMedidoresDPO.get_13());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck14(cadastrarAbaInspecaoLogicaMedidoresDPO.get_14());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck15(cadastrarAbaInspecaoLogicaMedidoresDPO.get_15());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck16(cadastrarAbaInspecaoLogicaMedidoresDPO.get_16());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck17(cadastrarAbaInspecaoLogicaMedidoresDPO.get_17());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck18(cadastrarAbaInspecaoLogicaMedidoresDPO.get_18());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck19(cadastrarAbaInspecaoLogicaMedidoresDPO.get_19());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck20(cadastrarAbaInspecaoLogicaMedidoresDPO.get_20());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck21(cadastrarAbaInspecaoLogicaMedidoresDPO.get_21());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck22(cadastrarAbaInspecaoLogicaMedidoresDPO.get_22());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck23(cadastrarAbaInspecaoLogicaMedidoresDPO.get_23());
		cadastroAbaInspecaoLogicaMedidoresPage.typeCheck24(cadastrarAbaInspecaoLogicaMedidoresDPO.get_24());
		cadastroAbaInspecaoLogicaMedidoresPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaInspecaoLogicaMedidoresPage.validateMessageDefault(cadastrarAbaInspecaoLogicaMedidoresDPO.getOperationMessage());
	}

}
