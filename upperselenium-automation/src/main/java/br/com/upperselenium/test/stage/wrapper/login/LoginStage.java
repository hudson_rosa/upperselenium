package br.com.upperselenium.test.stage.wrapper.login;

import br.com.upperselenium.base.BaseContext;
import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.WebDriverMaster;
import br.com.upperselenium.base.exception.RunningException;
import br.com.upperselenium.base.logger.BaseLogger;
import br.com.upperselenium.base.parameter.MessagePRM;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.constant.ConstantContext;
import br.com.upperselenium.test.stage.wrapper.login.dpo.LoginDPO;
import br.com.upperselenium.test.stage.wrapper.login.page.LoginPage;

public class LoginStage extends BaseStage {
	private LoginDPO loginDPO;
	private LoginPage loginPage;
	
	public LoginStage(String dpLogin) {
		loginDPO = loadDataProviderFile(LoginDPO.class, dpLogin);
	}
	
	@Override
	public void initMappedPages() {
		loginPage = initElementsFromPage(LoginPage.class);
	}
	
	@Override
	public void runStage() {
		try {
			WebDriverMaster.getWebDriver().get(loginDPO.getUrl());			
			authenticateUser();			
			setUserOnContext();		
		} catch (Exception t) {
			FindElementUtil.cancelAlert();
			authenticateUser();			
			setUserOnContext();		
			throw new RunningException(BaseLogger.logException(MessagePRM.AsException.ALERT_NOT_EXPECTED), t);
		}
	}

	private void authenticateUser() {
		waitForPageToLoadUntil10s();
		loginPage.typeTextUsuario(loginDPO.getUsuario());
		loginPage.getTabFieldUsuario();
		loginPage.typeTextSenha(loginDPO.getSenha());
		loginPage.clickEntrar();
	}

	private void setUserOnContext() {
		BaseContext.setParameter(ConstantContext.USUARIO.getValue(), loginDPO.getUsuario());
		waitForPageToLoadUntil10s();
	}

	@Override
	public void runValidations() {}

}
