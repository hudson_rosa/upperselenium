package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_equipamentos.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarTiposDeEquipamentoDPO extends BaseHelperDPO {

	private String labelMarca;
	private String labelModelo;
	private String labelEspecie;

	public String getLabelMarca() {
		return labelMarca;
	}

	public void setLabelMarca(String labelMarca) {
		this.labelMarca = labelMarca;
	}

	public String getLabelModelo() {
		return labelModelo;
	}

	public void setLabelModelo(String labelModelo) {
		this.labelModelo = labelModelo;
	}

	public String getLabelEspecie() {
		return labelEspecie;
	}

	public void setLabelEspecie(String labelEspecie) {
		this.labelEspecie = labelEspecie;
	}

	@Override
	public String toString() {
		return "NavegarAlterarTiposDeEquipamentoDPO [labelMarca=" + labelMarca + ", labelModelo=" + labelModelo
				+ ", labelEspecie=" + labelEspecie + "]";
	}

}