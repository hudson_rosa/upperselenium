package br.com.upperselenium.test.stage.configuracoes.cadastros.padroes_de_calibracao.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class PadroesDeCalibracaoPage extends BaseHelperPage{
	
	private static final String LINK_NOVO = "//*[@class='link-nova']";
				
	public boolean isClickableLinkNovo() {
		return isDisplayedElement(LINK_NOVO);
	}
	
	public void clickNovo(){
		clickOnElement(LINK_NOVO);
		waitForPageToLoadUntil10s();
	}

}