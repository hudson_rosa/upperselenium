package br.com.upperselenium.test.stage.configuracoes.implantacao.configuracao_do_sistema;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.configuracao_do_sistema.dpo.NavegarAlterarConfDoSistemaDPO;
import br.com.upperselenium.test.stage.configuracoes.implantacao.configuracao_do_sistema.page.AlteracaoConfDoSistemaPage;

public class NavegarAlteracaoConfDoSistemaStage extends BaseStage {
	private NavegarAlterarConfDoSistemaDPO navegarAlteracaoConfsDoSistemaDPO;
	private AlteracaoConfDoSistemaPage alteracaoConfsDoSistemaPage;
	
	public NavegarAlteracaoConfDoSistemaStage(String dp) {
		navegarAlteracaoConfsDoSistemaDPO = loadDataProviderFile(NavegarAlterarConfDoSistemaDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		alteracaoConfsDoSistemaPage = initElementsFromPage(AlteracaoConfDoSistemaPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAlteracaoConfsDoSistemaDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		alteracaoConfsDoSistemaPage.clickAbaAdministracao();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaAlarmes();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaApi();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaConnectionManager();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaCreg();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaDashboard();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaDemo();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaDiagramaFasorial();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaEmail();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaExportacaoParaUE();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaExportacoes();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaFaturamento();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaGraficos();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaIntegracaoScde();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaIntegracaoUE();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaLdap();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaLoadProfile();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaMapaPorTrafoBt();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaPontos();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaRelatorios();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaSAPPM();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaSateliteNOAA();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaSOA();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaTasks();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaTempoReal();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaWebServices();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.clickAbaOOO();
		getLabelGridConfiguracoes();
		alteracaoConfsDoSistemaPage.getBreadcrumbsText(navegarAlteracaoConfsDoSistemaDPO.getBreadcrumbs());
		alteracaoConfsDoSistemaPage.clickUpToBreadcrumbs(navegarAlteracaoConfsDoSistemaDPO.getUpToBreadcrumb());
		alteracaoConfsDoSistemaPage.getWelcomeTitle(navegarAlteracaoConfsDoSistemaDPO.getWelcomeTitle());
	}

	private void getLabelGridConfiguracoes() {
		alteracaoConfsDoSistemaPage.getLabelHeaderChave(navegarAlteracaoConfsDoSistemaDPO.getLabelChave());
		alteracaoConfsDoSistemaPage.getLabelHeaderChave(navegarAlteracaoConfsDoSistemaDPO.getLabelValor());
	}

	@Override
	public void runValidations() {}
	
}
