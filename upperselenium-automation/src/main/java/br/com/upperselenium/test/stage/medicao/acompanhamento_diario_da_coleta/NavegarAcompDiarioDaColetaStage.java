package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_da_coleta;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_da_coleta.dpo.NavegarAcompDiarioDaColetaDPO;
import br.com.upperselenium.test.stage.medicao.acompanhamento_diario_da_coleta.page.AcompDiarioDaColetaPage;

public class NavegarAcompDiarioDaColetaStage extends BaseStage {
	private NavegarAcompDiarioDaColetaDPO navegarAcompDiarioDaColetaDPO;
	private AcompDiarioDaColetaPage acompDiarioDaColetaPage;	
	
	public NavegarAcompDiarioDaColetaStage(String dp) {
		navegarAcompDiarioDaColetaDPO = loadDataProviderFile(NavegarAcompDiarioDaColetaDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		acompDiarioDaColetaPage = initElementsFromPage(AcompDiarioDaColetaPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarAcompDiarioDaColetaDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		acompDiarioDaColetaPage.getLabelPasta(navegarAcompDiarioDaColetaDPO.getLabelPasta());
		acompDiarioDaColetaPage.getLabelTipoDeVisualizacao(navegarAcompDiarioDaColetaDPO.getLabelTipoDeVisualizacao());
		acompDiarioDaColetaPage.getLabelMes(navegarAcompDiarioDaColetaDPO.getLabelMes());
		acompDiarioDaColetaPage.getLabelStatus(navegarAcompDiarioDaColetaDPO.getLabelStatus());
		acompDiarioDaColetaPage.getLabelVazio(navegarAcompDiarioDaColetaDPO.getLabelCationVazio());
		acompDiarioDaColetaPage.getLabelParcial(navegarAcompDiarioDaColetaDPO.getLabelCationParcial());
		acompDiarioDaColetaPage.getLabelCompleto(navegarAcompDiarioDaColetaDPO.getLabelCationCompleto());
		acompDiarioDaColetaPage.getLabelNome(navegarAcompDiarioDaColetaDPO.getLabelHeaderNome());
		acompDiarioDaColetaPage.getLabelNumeroDeSerie(navegarAcompDiarioDaColetaDPO.getLabelHeaderNumeroDeSerie());
		acompDiarioDaColetaPage.getLabelCodigoScde(navegarAcompDiarioDaColetaDPO.getLabelHeaderCodigoScde());
		acompDiarioDaColetaPage.getLabelFuncao(navegarAcompDiarioDaColetaDPO.getLabelHeaderFuncao());
		acompDiarioDaColetaPage.getLabel01(navegarAcompDiarioDaColetaDPO.getLabel1());
		acompDiarioDaColetaPage.getLabel02(navegarAcompDiarioDaColetaDPO.getLabel2());
		acompDiarioDaColetaPage.getLabel03(navegarAcompDiarioDaColetaDPO.getLabel3());
		acompDiarioDaColetaPage.getLabel04(navegarAcompDiarioDaColetaDPO.getLabel4());
		acompDiarioDaColetaPage.getLabel05(navegarAcompDiarioDaColetaDPO.getLabel5());
		acompDiarioDaColetaPage.getLabel06(navegarAcompDiarioDaColetaDPO.getLabel6());
		acompDiarioDaColetaPage.getLabel07(navegarAcompDiarioDaColetaDPO.getLabel7());
		acompDiarioDaColetaPage.getLabel08(navegarAcompDiarioDaColetaDPO.getLabel8());
		acompDiarioDaColetaPage.getLabel09(navegarAcompDiarioDaColetaDPO.getLabel9());
		acompDiarioDaColetaPage.getLabel10(navegarAcompDiarioDaColetaDPO.getLabel10());
		acompDiarioDaColetaPage.getLabel11(navegarAcompDiarioDaColetaDPO.getLabel11());
		acompDiarioDaColetaPage.getLabel12(navegarAcompDiarioDaColetaDPO.getLabel12());
		acompDiarioDaColetaPage.getLabel13(navegarAcompDiarioDaColetaDPO.getLabel13());
		acompDiarioDaColetaPage.getLabel14(navegarAcompDiarioDaColetaDPO.getLabel14());
		acompDiarioDaColetaPage.getLabel15(navegarAcompDiarioDaColetaDPO.getLabel15());
		acompDiarioDaColetaPage.getLabel16(navegarAcompDiarioDaColetaDPO.getLabel16());
		acompDiarioDaColetaPage.getLabel17(navegarAcompDiarioDaColetaDPO.getLabel17());
		acompDiarioDaColetaPage.getLabel18(navegarAcompDiarioDaColetaDPO.getLabel18());
		acompDiarioDaColetaPage.getLabel19(navegarAcompDiarioDaColetaDPO.getLabel19());
		acompDiarioDaColetaPage.getLabel20(navegarAcompDiarioDaColetaDPO.getLabel20());
		acompDiarioDaColetaPage.getLabel21(navegarAcompDiarioDaColetaDPO.getLabel21());
		acompDiarioDaColetaPage.getLabel22(navegarAcompDiarioDaColetaDPO.getLabel22());
		acompDiarioDaColetaPage.getLabel23(navegarAcompDiarioDaColetaDPO.getLabel23());
		acompDiarioDaColetaPage.getLabel24(navegarAcompDiarioDaColetaDPO.getLabel24());
		acompDiarioDaColetaPage.getLabel25(navegarAcompDiarioDaColetaDPO.getLabel25());
		acompDiarioDaColetaPage.getLabel26(navegarAcompDiarioDaColetaDPO.getLabel26());
		acompDiarioDaColetaPage.getLabel27(navegarAcompDiarioDaColetaDPO.getLabel27());
		acompDiarioDaColetaPage.getLabel28(navegarAcompDiarioDaColetaDPO.getLabel28());
		acompDiarioDaColetaPage.getLabel29(navegarAcompDiarioDaColetaDPO.getLabel29());
		acompDiarioDaColetaPage.getLabel30(navegarAcompDiarioDaColetaDPO.getLabel30());
		acompDiarioDaColetaPage.getBreadcrumbsText(navegarAcompDiarioDaColetaDPO.getBreadcrumbs());
		acompDiarioDaColetaPage.clickUpToBreadcrumbs(navegarAcompDiarioDaColetaDPO.getUpToBreadcrumb());
		acompDiarioDaColetaPage.getWelcomeTitle(navegarAcompDiarioDaColetaDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
