package br.com.upperselenium.test.stage.medicao.comandos_do_gateway;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.dpo.NavegarComandosDoGatewayHistoricoDPO;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.page.ComandosDoGatewayHistoricoPage;
import br.com.upperselenium.test.stage.medicao.comandos_do_gateway.page.ComandosDoGatewayPage;

public class NavegarComandosDoGatewayHistoricoStage extends BaseStage {
	private NavegarComandosDoGatewayHistoricoDPO navegarComandosDoGatewayHistoricoDPO;
	private ComandosDoGatewayPage comandosDoGatewayPage;	
	private ComandosDoGatewayHistoricoPage comandosDoGatewayHistoricoPage;	
	
	public NavegarComandosDoGatewayHistoricoStage(String dp) {
		navegarComandosDoGatewayHistoricoDPO = loadDataProviderFile(NavegarComandosDoGatewayHistoricoDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		comandosDoGatewayPage = initElementsFromPage(ComandosDoGatewayPage.class);
		comandosDoGatewayHistoricoPage = initElementsFromPage(ComandosDoGatewayHistoricoPage.class);
	}

	@Override
	public void runStage() {
		getIssue(navegarComandosDoGatewayHistoricoDPO.getIssue());	
		execute();
	}
	
	private void execute() {		
		comandosDoGatewayPage.clickLinkHistorico();
		comandosDoGatewayHistoricoPage.getLabelHeaderGateway(navegarComandosDoGatewayHistoricoDPO.getLabelHeaderGateway());
		comandosDoGatewayHistoricoPage.getLabelHeaderInstante(navegarComandosDoGatewayHistoricoDPO.getLabelHeaderInstante());
		comandosDoGatewayHistoricoPage.getLabelHeaderDescricao(navegarComandosDoGatewayHistoricoDPO.getLabelHeaderDescricao());
		comandosDoGatewayHistoricoPage.getLabelHeaderStatus(navegarComandosDoGatewayHistoricoDPO.getLabelHeaderStatus());
		comandosDoGatewayHistoricoPage.getLabelHeaderErro(navegarComandosDoGatewayHistoricoDPO.getLabelHeaderErro());
		comandosDoGatewayHistoricoPage.getBreadcrumbsText(navegarComandosDoGatewayHistoricoDPO.getBreadcrumbs());
		comandosDoGatewayHistoricoPage.clickUpToBreadcrumbs(navegarComandosDoGatewayHistoricoDPO.getUpToBreadcrumb());
		comandosDoGatewayHistoricoPage.getWelcomeTitle(navegarComandosDoGatewayHistoricoDPO.getWelcomeTitle());
	}

	@Override
	public void runValidations() {}
	
}
