package br.com.upperselenium.test.stage.wrapper.navigation;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;

public class ConfirmAlertStage extends BaseStage {
	
	@Override
	public void initMappedPages() {}
	
	@Override
	public void runStage() {
		confirm();		
	}

	private void confirm() {
		waitForPageToLoadUntil10s();
		FindElementUtil.acceptAlert();
		waitForPageToLoadUntil10s();
	}

	@Override
	public void runValidations() {}

}
