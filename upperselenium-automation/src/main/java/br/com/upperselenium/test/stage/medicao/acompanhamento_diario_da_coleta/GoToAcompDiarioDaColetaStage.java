package br.com.upperselenium.test.stage.medicao.acompanhamento_diario_da_coleta;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.AreasMenuPage;
import br.com.upperselenium.test.stage.wrapper.menu_area.page.SideBarMedicaoPage;

public class GoToAcompDiarioDaColetaStage extends BaseStage {
	private AreasMenuPage areasMenuPage;
	private SideBarMedicaoPage sideBarMedicaoPage;
	
	public GoToAcompDiarioDaColetaStage() {}

	@Override
	public void initMappedPages() {
		areasMenuPage = initElementsFromPage(AreasMenuPage.class);
		sideBarMedicaoPage = initElementsFromPage(SideBarMedicaoPage.class);
	}

	@Override
	public void runStage() {
		navigateToPage();	
	}
	
	private void navigateToPage() {
		waitForPageToLoad(TimePRM._20_SECS);
		areasMenuPage.clickMedicao();
		waitForPageToLoadUntil10s();
		sideBarMedicaoPage.clickAcompanhamentoDiarioDaColeta();
		waitForPageToLoadUntil10s();		
	}

	@Override
	public void runValidations() {}

}
