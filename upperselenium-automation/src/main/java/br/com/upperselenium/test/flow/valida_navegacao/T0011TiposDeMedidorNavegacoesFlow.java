package br.com.upperselenium.test.flow.valida_navegacao;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S000PIMNavegacoesSuite;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.GoToTiposDeMedidorStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.NavegarAlteracaoTiposDeMedidorStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.NavegarCadastroTiposDeMedidorStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.NavegarRegistroLinkTiposDeMedidorStage;
import br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_medidor.NavegarTiposDeMedidorStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0011TiposDeMedidorNavegacoesFlow.class)
@FlowParams(idTest = "PIM-Navegacoes-T0011-TiposDeMedidor", testDirPath = "", loginDirPath = "",
	goal = "Realiza a navegação básica sobre a tela de Tipos de Medidor validando-se breadcrumbs e welcome title.",
	suiteClass = S000PIMNavegacoesSuite.class)
public class T0011TiposDeMedidorNavegacoesFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToTiposDeMedidorStage());
		addStage(new NavegarTiposDeMedidorStage(getDP("NavegarTiposDeMedidorDP.json")));
		addStage(new GoToTiposDeMedidorStage());
		addStage(new NavegarCadastroTiposDeMedidorStage(getDP("NavegarCadastroTiposDeMedidorDP.json")));
		addStage(new NavegarRegistroLinkTiposDeMedidorStage(getDP("NavegarRegistroLinkTiposDeMedidorDP.json")));
		addStage(new NavegarAlteracaoTiposDeMedidorStage(getDP("NavegarAlteracaoTiposDeMedidorDP.json")));
	}	
}
