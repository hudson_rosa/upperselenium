package br.com.upperselenium.test.stage.configuracoes.cadastros.gateways.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaMedidoresGatewaysPage extends BaseHelperPage{
	
	private static final String LINK_ABA_MEDIDORES = ".//div[2]/div/div[2]/ul[2]/li[2]/a";   
	private static final String BUTTON_ADICIONAR = "//*[@id='btnAddMedidor']";   
	private static final String MODAL_BUTTON_SALVAR = "//*[@id='compomenteSlotMedidor']/form/div[3]/button[1]";   
	private static final String MODAL_BUTTON_CANCELAR = "//*[@id='compomenteSlotMedidor']/form/div[3]/button[2]";   
	private static final String MODAL_TEXT_SLOT = "//*[@id='input-slot']";
	private static final String MODAL_TEXT_MEDIDOR = "//*[@id='medidor']";	
	private static final String MESSAGE_INFO = "//*[@id='gateway-medidores']/aba-medidores/div/div";		

	public String getInfoMessage() {
		return getAlertMessage(MESSAGE_INFO);
	}
	
	public void typeModalTextSlot(String value){
		typeText(MODAL_TEXT_SLOT, value);
	}	
	
	public void typeModalTextMedidor(String value){
		typeText(MODAL_TEXT_MEDIDOR, value);
	}
	
	public void typeModalAutocompleteTextMedidor(String value){
		typeTextAutoCompleteSelect(MODAL_TEXT_MEDIDOR, value);
	}		
	
	public void clickModalSalvar(){
		clickOnElement(MODAL_BUTTON_SALVAR);
		getBlockOverlay();
		waitForATime(TimePRM._4_SECS);
	}
		
	public void clickModalCancelar(){
		clickOnElement(MODAL_BUTTON_CANCELAR);
	}
	
	public void clickAdicionar(){
		clickOnElement(BUTTON_ADICIONAR);
	}
	
	public void clickAbaMedidores(){
		clickOnElement(LINK_ABA_MEDIDORES);
	}
	
	public void keyTab(){
		waitForATime(TimePRM._2_SECS);
		useKey(MODAL_TEXT_MEDIDOR, Keys.PAGE_DOWN);
	}	
	
}