package br.com.upperselenium.test.stage.configuracoes.cadastros.regras;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.base.util.WaitElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regras.dpo.CadastrarRegrasDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regras.page.CadastroRegrasPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.regras.page.RegrasPage;

public class CadastrarRegrasStage extends BaseStage {
	private CadastrarRegrasDPO cadastrarRegrasDPO;
	private RegrasPage regrasPage;
	private CadastroRegrasPage cadastroRegrasPage;
	
	public CadastrarRegrasStage(String dp) {
		cadastrarRegrasDPO = loadDataProviderFile(CadastrarRegrasDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		regrasPage = initElementsFromPage(RegrasPage.class);
		cadastroRegrasPage = initElementsFromPage(CadastroRegrasPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarRegrasDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		regrasPage.clickNova();
		cadastroRegrasPage.typeTextNome(cadastrarRegrasDPO.getNome()+getRandomStringSpaced());
		cadastroRegrasPage.typeSelectPasta(cadastrarRegrasDPO.getPasta());
		for (int index=0; index < cadastrarRegrasDPO.getGridRegras().size(); index++){
			cadastroRegrasPage.typeGridSelectSe(cadastrarRegrasDPO.getGridRegras().get(index).getSe(), index+1);
			cadastroRegrasPage.typeGridSelectOperador(cadastrarRegrasDPO.getGridRegras().get(index).getOperador(), index+1);
			cadastroRegrasPage.typeGridSelectValor(cadastrarRegrasDPO.getGridRegras().get(index).getValor(), index+1);
			cadastroRegrasPage.clickGridAdd(cadastrarRegrasDPO.getGridRegras().get(index).getButtonAdd(), index+1);
			cadastroRegrasPage.clickGridExclude(cadastrarRegrasDPO.getGridRegras().get(index).getButtonExclude(), index+1);
		}
		cadastroRegrasPage.typeTextFormula(cadastrarRegrasDPO.getFormula());
		WaitElementUtil.waitForATime(TimePRM._3_SECS);
		cadastroRegrasPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroRegrasPage.validateMessageDefault(cadastrarRegrasDPO.getOperationMessage());
	}

}
