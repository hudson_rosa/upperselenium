package br.com.upperselenium.test.stage.medicao.consolidacao.page;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class ConsolidacaoPage extends BaseHelperPage{
	
	private static final String BUTTON_PROCESSAR = "//*[@id='filterButton']";
	private static final String LABEL_HEADER_FONTE_DE_DADOS = "//*[@id='consolidacoes']/thead/tr[2]/th[2]";
	private static final String LABEL_HEADER_DATA_HORA = "//*[@id='consolidacoes']/thead/tr[2]/th[3]";
	private static final String LABEL_HEADER_CP_KWHDEL = "//*[@id='consolidacoes']/thead/tr[2]/th[4]";
	private static final String LABEL_HEADER_CP_KWHREC = "//*[@id='consolidacoes']/thead/tr[2]/th[5]";
	private static final String LABEL_HEADER_CP_KVARHDEL = "//*[@id='consolidacoes']/thead/tr[2]/th[6]";
	private static final String LABEL_HEADER_CP_KVARHREC = "//*[@id='consolidacoes']/thead/tr[2]/th[7]";
	private static final String LABEL_HEADER_SCP_KWHDEL = "//*[@id='consolidacoes']/thead/tr[2]/th[8]";
	private static final String LABEL_HEADER_SCP_KWHREC = "//*[@id='consolidacoes']/thead/tr[2]/th[9]";
	private static final String LABEL_HEADER_SCP_KVARHDEL = "//*[@id='consolidacoes']/thead/tr[2]/th[10]";
	private static final String LABEL_HEADER_SCP_KVARHREC = "//*[@id='consolidacoes']/thead/tr[2]/th[11]";
	
	
	public void clickButtonProcessar(){
		waitForPageToLoadUntil10s();		
		clickOnElement(BUTTON_PROCESSAR);
		waitForPageToLoadUntil10s();		
	}

	public void getLabelHeaderFonteDeDados(String value){
		getLabel(LABEL_HEADER_FONTE_DE_DADOS, value);
	}

	public void getLabelHeaderDataHora(String value){
		getLabel(LABEL_HEADER_DATA_HORA, value);
	}
	
	public void getLabelHeaderCPKwhDel(String value){
		getLabel(LABEL_HEADER_CP_KWHDEL, value);
	}
	
	public void getLabelHeaderCPKwhRec(String value){
		getLabel(LABEL_HEADER_CP_KWHREC, value);
	}
	
	public void getLabelHeaderCPKvarDel(String value){
		getLabel(LABEL_HEADER_CP_KVARHDEL, value);
	}

	public void getLabelHeaderCPKvarRec(String value){
		getLabel(LABEL_HEADER_CP_KVARHREC, value);
	}
	
	public void getLabelHeaderSCPKwhDel(String value){
		getLabel(LABEL_HEADER_SCP_KWHDEL, value);
	}
	
	public void getLabelHeaderSCPKwhRec(String value){
		getLabel(LABEL_HEADER_SCP_KWHREC, value);
	}
	
	public void getLabelHeaderSCPKvarDel(String value){
		getLabel(LABEL_HEADER_SCP_KVARHDEL, value);
	}
	
	public void getLabelHeaderSCPKvarRec(String value){
		getLabel(LABEL_HEADER_SCP_KVARHREC, value);
	}

}