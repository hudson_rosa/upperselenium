package br.com.upperselenium.test.stage.configuracoes.implantacao.tipos_de_ponto_ccee.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroTiposDePontoCCEEPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = ".//div[2]/div/div[2]/form/fieldset/div[3]/input";   
	private static final String TEXT_NOME = "//*[@id='TipoPontoCcee_Nome']"; 
	private static final String TEXT_DESCRICAO = "//*[@id='TipoPontoCcee_Descricao']";
	
	public void typeTextNomeDefault(String value){
		typeText(TEXT_NOME, value);
	}	
	
	public void typeTextNomeRandom(String value){
		typeText(TEXT_NOME, value);
	}	

	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}	
		
	public void keyPageDown(){
		useKey(TEXT_NOME, Keys.PAGE_DOWN);
	}	

	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
	public void getLabelButtonSalvar(String value){
		getLabel(BUTTON_SALVAR, value);
	}
	
}