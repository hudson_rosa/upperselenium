package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroPontosPage extends BaseHelperPage{
	
	private static final String BUTTON_SALVAR = "//*[@id='salvar']";   
	private static final String TEXT_NOME = "//*[@id='Nome']";
	private static final String SELECT_PASTA = "//*[@id='IdDaPasta']";
	private static final String SELECT_TIPO = "//*[@id='IdDoTipo']";
	private static final String SELECT_SUBESTACAO = "//*[@id='IdDaSubestacao']";	
	private static final String TEXT_UC = "//*[@id='UnidadeConsumidora_NomeENumero']";
	private static final String TEXT_TRAFO_BT = "//*[@id='TrafoBt']";
	private static final String TEXT_DESCRICAO = "//*[@id='Descricao']";	
	private static final String TEXT_LATITUDE = "//*[@id='Latitude']";
	private static final String TEXT_LONGITUDE = "//*[@id='Longitude']";
	
	public void typeTextNome(String value){
		typeText(TEXT_NOME, value);
	}	

	public void typeSelectPasta(String value){
		typeSelectComboOption(SELECT_PASTA, value);
	}
	
	public void typeSelectTipo(String value){
		typeSelectComboOption(SELECT_TIPO, value);
	}
	
	public void typeSelectSubestacao(String value){
		typeSelectComboOption(SELECT_SUBESTACAO, value);
	}
	
	public void typeTextUC(String value){
		typeTextAutoCompleteSelect(TEXT_UC, value);
	}	

	public void typeTextTrafoBT(String value){
		typeTextAutoCompleteSelect(TEXT_TRAFO_BT, value);
	}	
	
	public void typeTextDescricao(String value){
		typeText(TEXT_DESCRICAO, value);
	}	
	
	public void typeTextLatitude(String value){
		typeText(TEXT_LATITUDE, value);
	}	
	
	public void typeTextLongitude(String value){
		typeText(TEXT_LONGITUDE, value);
	}	
	
	public void keyPageDown(){
		useKey(TEXT_NOME, Keys.PAGE_DOWN);
	}	

	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
	
}