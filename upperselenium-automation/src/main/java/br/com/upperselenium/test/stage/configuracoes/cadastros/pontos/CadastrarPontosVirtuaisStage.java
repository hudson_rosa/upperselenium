package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.dpo.CadastrarPontosVirtuaisDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.CadastroPontosVirtuaisPage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page.PontosPage;

public class CadastrarPontosVirtuaisStage extends BaseStage {
	private CadastrarPontosVirtuaisDPO cadastrarPontosVirtuaisDPO;
	private PontosPage pontosPage;
	private CadastroPontosVirtuaisPage cadastroPontosVirtuaisPage;
	
	public CadastrarPontosVirtuaisStage(String dp) {
		cadastrarPontosVirtuaisDPO = loadDataProviderFile(CadastrarPontosVirtuaisDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		pontosPage = initElementsFromPage(PontosPage.class);
		cadastroPontosVirtuaisPage = initElementsFromPage(CadastroPontosVirtuaisPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarPontosVirtuaisDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		pontosPage.clickNovoVirtual();
		cadastroPontosVirtuaisPage.typeTextNome(cadastrarPontosVirtuaisDPO.getNome()+getRandomStringSpaced());		
		cadastroPontosVirtuaisPage.typeSelectPasta(cadastrarPontosVirtuaisDPO.getPasta());		
		cadastroPontosVirtuaisPage.typeSelectTipo(cadastrarPontosVirtuaisDPO.getTipo());		
		cadastroPontosVirtuaisPage.typeSelectSubestacao(cadastrarPontosVirtuaisDPO.getSubestacao());		
		cadastroPontosVirtuaisPage.typeTextUC(cadastrarPontosVirtuaisDPO.getUc());		
		cadastroPontosVirtuaisPage.keyPageDown();		
		cadastroPontosVirtuaisPage.typeTextTrafoBT(cadastrarPontosVirtuaisDPO.getTrafoBt());		
		cadastroPontosVirtuaisPage.typeTextDescricao(cadastrarPontosVirtuaisDPO.getDescricao()+getRandomStringSpaced());		
		cadastroPontosVirtuaisPage.typeTextLatitude(cadastrarPontosVirtuaisDPO.getLatitude());		
		cadastroPontosVirtuaisPage.typeTextLongitude(cadastrarPontosVirtuaisDPO.getLongitude());		
		cadastroPontosVirtuaisPage.typeTextRgParaExportacaoUE(cadastrarPontosVirtuaisDPO.getRgParaExportacaoUE()+getRandomStringNonSpaced());		
		cadastroPontosVirtuaisPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroPontosVirtuaisPage.validateMessageDefault(cadastrarPontosVirtuaisDPO.getOperationMessage());
	}
	
}
