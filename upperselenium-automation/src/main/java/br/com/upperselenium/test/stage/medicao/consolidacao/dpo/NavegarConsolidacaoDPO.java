package br.com.upperselenium.test.stage.medicao.consolidacao.dpo;

import br.com.upperselenium.test.stage.wrapper.BaseHelperDPO;

public class NavegarConsolidacaoDPO extends BaseHelperDPO {

	private String labelHeaderFonteDeDados;
	private String labelHeaderDataHora;
	private String labelHeaderCpKWhDel;
	private String labelHeaderCpKWhRec;
	private String labelHeaderCpKVARhDel;
	private String labelHeaderCpKVARhRec;
	private String labelHeaderScpKWhDel;
	private String labelHeaderScpKWhRec;
	private String labelHeaderScpKVARhDel;
	private String labelHeaderScpKVARhRec;

	public String getLabelHeaderFonteDeDados() {
		return labelHeaderFonteDeDados;
	}

	public void setLabelHeaderFonteDeDados(String labelHeaderFonteDeDados) {
		this.labelHeaderFonteDeDados = labelHeaderFonteDeDados;
	}

	public String getLabelHeaderDataHora() {
		return labelHeaderDataHora;
	}

	public void setLabelHeaderDataHora(String labelHeaderDataHora) {
		this.labelHeaderDataHora = labelHeaderDataHora;
	}

	public String getLabelHeaderCpKWhDel() {
		return labelHeaderCpKWhDel;
	}

	public void setLabelHeaderCpKWhDel(String labelHeaderCpKWhDel) {
		this.labelHeaderCpKWhDel = labelHeaderCpKWhDel;
	}

	public String getLabelHeaderCpKWhRec() {
		return labelHeaderCpKWhRec;
	}

	public void setLabelHeaderCpKWhRec(String labelHeaderCpKWhRec) {
		this.labelHeaderCpKWhRec = labelHeaderCpKWhRec;
	}

	public String getLabelHeaderCpKVARhDel() {
		return labelHeaderCpKVARhDel;
	}

	public void setLabelHeaderCpKVARhDel(String labelHeaderCpKVARhDel) {
		this.labelHeaderCpKVARhDel = labelHeaderCpKVARhDel;
	}

	public String getLabelHeaderCpKVARhRec() {
		return labelHeaderCpKVARhRec;
	}

	public void setLabelHeaderCpKVARhRec(String labelHeaderCpKVARhRec) {
		this.labelHeaderCpKVARhRec = labelHeaderCpKVARhRec;
	}

	public String getLabelHeaderScpKWhDel() {
		return labelHeaderScpKWhDel;
	}

	public void setLabelHeaderScpKWhDel(String labelHeaderScpKWhDel) {
		this.labelHeaderScpKWhDel = labelHeaderScpKWhDel;
	}

	public String getLabelHeaderScpKWhRec() {
		return labelHeaderScpKWhRec;
	}

	public void setLabelHeaderScpKWhRec(String labelHeaderScpKWhRec) {
		this.labelHeaderScpKWhRec = labelHeaderScpKWhRec;
	}

	public String getLabelHeaderScpKVARhDel() {
		return labelHeaderScpKVARhDel;
	}

	public void setLabelHeaderScpKVARhDel(String labelHeaderScpKVARhDel) {
		this.labelHeaderScpKVARhDel = labelHeaderScpKVARhDel;
	}

	public String getLabelHeaderScpKVARhRec() {
		return labelHeaderScpKVARhRec;
	}

	public void setLabelHeaderScpKVARhRec(String labelHeaderScpKVARhRec) {
		this.labelHeaderScpKVARhRec = labelHeaderScpKVARhRec;
	}

}