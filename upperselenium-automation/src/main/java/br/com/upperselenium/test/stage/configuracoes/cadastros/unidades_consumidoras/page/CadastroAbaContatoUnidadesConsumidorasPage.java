package br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.base.parameter.TimePRM;
import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaContatoUnidadesConsumidorasPage extends BaseHelperPage{
	
	private static final String LINK_ABA_CONTATO = ".//div[2]/div/div[2]/ul[2]/li[3]/a";  
	private static final String MODAL_BUTTON_SALVAR = "//*[@id='modal-salvar-contato']/form/div[3]/button[1]"; 	
	private static final String MODAL_BUTTON_CANCELAR = "//*[@id='modal-salvar-contato']/form/div[3]/button[2]"; 	
	private static final String MODAL_TEXT_NOME = "//*[@id='input-nome']";
	private static final String MODAL_TEXT_EMAIL = "//*[@id='input-email']";	
	private static final String MODAL_TEXT_TELEFONE = "//*[@id='input-telefone']";
	private static final String BUTTON_ADICIONAR = "//*[@id='unidade-consumidora']/aba-contatos/div/button";   
	private static final String TEXT_FILTER_NOME = "//*[@id='tabela-contatos']/table/thead/tr[1]/th[1]/list-filter/div/input";	
	private static final String TEXT_FILTER_EMAIL= "//*[@id='tabela-contatos']/table/thead/tr[1]/th[2]/list-filter/div/input";	
	private static final String GRID_VALUE_NOME = "//*[@id='tabela-contatos']/table/tbody/tr/td[1]";	
	private static final String GRID_VALUE_EMAIL= "//*[@id='tabela-contatos']/table/tbody/tr/td[2]";	
	private static final String GRID_VALUE_TELEFONE = "//*[@id='tabela-contatos']/table/tbody/tr/td[3]";	
	
	public void typeModalTextNome(String value){
		typeText(MODAL_TEXT_NOME, value);
	}	
	
	public void typeModalTextEmail(String value){
		typeText(MODAL_TEXT_EMAIL, value);
	}	

	public void typeModalTextTelefone(String value){
		typeText(MODAL_TEXT_TELEFONE, value);
	}	
	
	public void clickModalSalvar(){
		clickOnElement(MODAL_BUTTON_SALVAR);
	}
		
	public void clickModalCancelar(){
		clickOnElement(MODAL_BUTTON_CANCELAR);
	}	
	
	public void typeTextFilterNome(String value){
		typeText(TEXT_FILTER_NOME, value);
	}	
	
	public void typeTextFilterEmail(String value){
		typeText(TEXT_FILTER_EMAIL, value);
	}	
	
	public String getGridTextNome (){
		return getGridFirstLineLabel(GRID_VALUE_NOME);
	}
	
	public String getGridTextEmail (){
		return getGridFirstLineLabel(GRID_VALUE_EMAIL);
	}
	
	public String getGridTextTelefone (){
		return getGridFirstLineLabel(GRID_VALUE_TELEFONE);
	}
	
	public void clickAdicionar(){
		clickOnElement(BUTTON_ADICIONAR);
	}
	
	public void clickAbaContato(){
		clickOnElement(LINK_ABA_CONTATO);
	}
	
	public void keyPageDown(){
		waitForATime(TimePRM._2_SECS);
		useKey(MODAL_TEXT_EMAIL, Keys.PAGE_DOWN);
	}	
	
}