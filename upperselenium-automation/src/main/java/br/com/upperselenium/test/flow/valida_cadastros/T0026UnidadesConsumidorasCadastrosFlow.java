package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.CadastrarAbaContatoUnidadesConsumidorasStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.CadastrarAbaEnderecoUnidadesConsumidorasStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.CadastrarUnidadesConsumidorasStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.unidades_consumidoras.GoToUnidadesConsumidorasStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0026UnidadesConsumidorasCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0026-UnidadesConsumidoras", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Unidades Consumidoras realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0026UnidadesConsumidorasCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToUnidadesConsumidorasStage());
		addStage(new CadastrarUnidadesConsumidorasStage(getDP("CadastrarUnidadesConsumidorasDP.json")));
		addStage(new CadastrarAbaEnderecoUnidadesConsumidorasStage(getDP("CadastrarAbaEnderecoUnidadesConsumidorasDP.json")));
		addStage(new CadastrarAbaContatoUnidadesConsumidorasStage(getDP("CadastrarAbaContatoUnidadesConsumidorasDP.json")));
	}	
}
