package br.com.upperselenium.test.stage.configuracoes.cadastros.medidores;

import br.com.upperselenium.base.BaseStage;
import br.com.upperselenium.base.util.FindElementUtil;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.dpo.CadastrarAbaSincronismoMedidoresDPO;
import br.com.upperselenium.test.stage.configuracoes.cadastros.medidores.page.CadastroAbaSincronismoMedidoresPage;

public class CadastrarAbaSincronismoMedidoresStage extends BaseStage {
	private CadastrarAbaSincronismoMedidoresDPO cadastrarAbaSincronismoMedidoresDPO;
	private CadastroAbaSincronismoMedidoresPage cadastroAbaSincronismoMedidoresPage;
	
	public CadastrarAbaSincronismoMedidoresStage(String dp) {
		cadastrarAbaSincronismoMedidoresDPO = loadDataProviderFile(CadastrarAbaSincronismoMedidoresDPO.class, dp);
	}
	
	@Override
	public void initMappedPages() {
		cadastroAbaSincronismoMedidoresPage = initElementsFromPage(CadastroAbaSincronismoMedidoresPage.class);
	}

	@Override
	public void runStage() {
		getIssue(cadastrarAbaSincronismoMedidoresDPO.getIssue());	
		execute();		
	}
	
	private void execute() {
		cadastroAbaSincronismoMedidoresPage.clickAbaSincronismo();
		FindElementUtil.acceptAlert();
		cadastroAbaSincronismoMedidoresPage.typeCheckAcertoDeRelogio(cadastrarAbaSincronismoMedidoresDPO.getAcertoDeRelogio());
		cadastroAbaSincronismoMedidoresPage.typeSelectSincronismoHora(cadastrarAbaSincronismoMedidoresDPO.getSincronismoHora());
		cadastroAbaSincronismoMedidoresPage.typeSelectSincronismoMinuto(cadastrarAbaSincronismoMedidoresDPO.getSincronismoMinuto());
		cadastroAbaSincronismoMedidoresPage.clickSalvar();
	}

	@Override
	public void runValidations() {
		cadastroAbaSincronismoMedidoresPage.validateMessageDefault(cadastrarAbaSincronismoMedidoresDPO.getOperationMessage());
	}
	
}
