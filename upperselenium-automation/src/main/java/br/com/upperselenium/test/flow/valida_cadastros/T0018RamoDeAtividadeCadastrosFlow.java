package br.com.upperselenium.test.flow.valida_cadastros;

import org.junit.runners.Suite.SuiteClasses;

import br.com.upperselenium.base.BaseFlow;
import br.com.upperselenium.base.annotation.FlowParams;
import br.com.upperselenium.test.S001PIMAreaCadastrosSuite;
import br.com.upperselenium.test.stage.configuracoes.cadastros.ramo_de_atividade.CadastrarRamoDeAtividadeStage;
import br.com.upperselenium.test.stage.configuracoes.cadastros.ramo_de_atividade.GoToRamoDeAtividadeStage;
import br.com.upperselenium.test.stage.wrapper.login.LoginStage;

@SuiteClasses(T0018RamoDeAtividadeCadastrosFlow.class)
@FlowParams(idTest = "PIM-Cadastros-T0018-RamoDeAtividade", testDirPath = "", loginDirPath = "",
	goal = "Valida se a tela de cadastro de Ramo de Atividade realiza a operação de cadastro corretamente.",
	suiteClass = S001PIMAreaCadastrosSuite.class)
public class T0018RamoDeAtividadeCadastrosFlow extends BaseFlow {
	
	@Override
	protected void addFlowStages() {
		addStage(new LoginStage(getDP("LoginDP.json")));
		addStage(new GoToRamoDeAtividadeStage());
		addStage(new CadastrarRamoDeAtividadeStage(getDP("CadastrarRamoDeAtividadeDP.json")));
	}	
}
