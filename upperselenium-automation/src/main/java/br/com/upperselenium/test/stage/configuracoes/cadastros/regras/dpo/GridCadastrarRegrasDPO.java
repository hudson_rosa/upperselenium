package br.com.upperselenium.test.stage.configuracoes.cadastros.regras.dpo;

public class GridCadastrarRegrasDPO {

	private String termo;
	private String se;
	private String operador;
	private String valor;
	private String buttonAdd;
	private String buttonExclude;

	public String getTermo() {
		return termo;
	}

	public void setTermo(String termo) {
		this.termo = termo;
	}

	public String getSe() {
		return se;
	}

	public void setSe(String se) {
		this.se = se;
	}

	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getButtonAdd() {
		return buttonAdd;
	}

	public void setButtonAdd(String buttonAdd) {
		this.buttonAdd = buttonAdd;
	}

	public String getButtonExclude() {
		return buttonExclude;
	}

	public void setButtonExclude(String buttonExclude) {
		this.buttonExclude = buttonExclude;
	}

}
