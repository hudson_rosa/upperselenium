package br.com.upperselenium.test.stage.configuracoes.cadastros.pontos.page;

import org.openqa.selenium.Keys;

import br.com.upperselenium.test.stage.wrapper.BaseHelperPage;

public class CadastroAbaHidrologiaPontosPage extends BaseHelperPage{
	
	private static final String LINK_ABA_HIDROLOGIA = "//*[@id='tabs']/ul/li[6]/a";
	private static final String BUTTON_SALVAR = "//*[@id='salvar']"; 	
	private static final String TEXT_CODIGO_DA_ESTACAO_HIDROMETEOROLOGICA = "//*[@id='CodigoDaEstacao']"; 
	private static final String TEXT_SIGLA_DA_ESTACAO = "//*[@id='SiglaDaEstacao']";
	private static final String SELECT_SITUACAO = "//*[@id='AtivaNumber']";
	private static final String SELECT_TIPO_DE_ESTACAO = "//*[@id='TipoDeEstacaoNumber']";
	private static final String TEXT_LOGIN = "//*[@id='Login']";
	private static final String TEXT_SENHA = "//*[@id='Senha']";
	private static final String TEXT_CODIGO_FLUVIOMETRICO = "//*[@id='CodigoFlu']";
	private static final String TEXT_CODIGO_PLUVIOMETRICO = "//*[@id='CodigoPlu']";
	private static final String TEXT_PARAMETRO_A = "//*[@id='ParametroA']";
	private static final String TEXT_PARAMETRO_B = "//*[@id='ParametroB']";
	private static final String TEXT_PARAMETRO_C = "//*[@id='ParametroC']";
	private static final String CHECK_UTILIZAR_LOGIN_E_SENHA_ANA = "//*[@id='UtilizarLoginSenhaIntegracaoAna']";
	private static final String TOOLTIP_UTILIZAR_LOGIN_E_SENHA_ANA = "//*[@id='popoverUtilizaLoginAna']";
	
	public void typeTextCodigoDaEstacaoHidrometeorologica(String value){
		typeText(TEXT_CODIGO_DA_ESTACAO_HIDROMETEOROLOGICA, value);
	}	
	
	public void typeTextSiglaDaEstacao(String value){
		typeText(TEXT_SIGLA_DA_ESTACAO, value);
	}	
	
	public void typeSelectSituacao(String value){		
		typeSelectComboOption(SELECT_SITUACAO, value);
	}
	
	public void typeSelectTipoDeEstacao(String value){		
		typeSelectComboOption(SELECT_TIPO_DE_ESTACAO, value);
	}
	
	public void typeTextLogin(String value){
		typeText(TEXT_LOGIN, value);
	}	
	
	public void typeTextSenha(String value){
		typeText(TEXT_SENHA, value);
	}	
	
	public void typeTextCodigoFluviometrico(String value){
		typeText(TEXT_CODIGO_FLUVIOMETRICO, value);
	}	
	
	public void typeTextCodigoPluviometrico(String value){
		typeText(TEXT_CODIGO_PLUVIOMETRICO, value);
	}	
	
	public void typeTextParametroA(String value){
		typeText(TEXT_PARAMETRO_A, value);
	}	
	
	public void typeTextParametroB(String value){
		typeText(TEXT_PARAMETRO_B, value);
	}	
	
	public void typeTextParametroC(String value){
		typeText(TEXT_PARAMETRO_C, value);
	}	
	
	public void typeCheckUtilizarLoginESenhaAna(String value){		
		typeCheckOption(CHECK_UTILIZAR_LOGIN_E_SENHA_ANA, value);
	}
		
	public void getTooltipUtilizarLoginESenhaAna(String value){
		getTooltip(TOOLTIP_UTILIZAR_LOGIN_E_SENHA_ANA, value);
	}
	
	public void keyPageDown(){
		useKey(TEXT_CODIGO_DA_ESTACAO_HIDROMETEOROLOGICA, Keys.PAGE_DOWN);
	}	
	
	public void clickSalvar(){
		clickOnElement(BUTTON_SALVAR);
	}
				
	public void clickAbaHidrologia(){
		clickOnElement(LINK_ABA_HIDROLOGIA);
	}
	
}