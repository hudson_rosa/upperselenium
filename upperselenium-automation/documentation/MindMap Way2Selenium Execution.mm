<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node BACKGROUND_COLOR="#000033" COLOR="#ffffff" CREATED="1466951861041" ID="ID_737695689" MODIFIED="1506274322143" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <img src="Way2SeleniumLogo.png" />
  </body>
</html>
</richcontent>
<font BOLD="true" NAME="SansSerif" SIZE="13"/>
<node BACKGROUND_COLOR="#ffffff" CREATED="1466958674034" ID="ID_330227869" MODIFIED="1506273047975" POSITION="right" STYLE="bubble" TEXT="SuiteExecutor">
<font BOLD="true" NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="forward"/>
<node BACKGROUND_COLOR="#ccffff" CREATED="1466954342116" ID="ID_1897391928" MODIFIED="1506273047975" STYLE="bubble" TEXT="1. BaseSuite">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="flag-black"/>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1466957470319" ID="ID_1803572700" MODIFIED="1506273047975" STYLE="bubble" TEXT="1.1 @BerofeClass - setUpClass()">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="flag-green"/>
<node CREATED="1466955290823" ID="ID_1213142914" MODIFIED="1506273047976" STYLE="bubble" TEXT="1.1.1 Remove Reports e logs antigos">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="closed"/>
<node CREATED="1466955375041" ID="ID_1791776574" MODIFIED="1506273047976" STYLE="bubble" TEXT="1.1.1.1 Carrega a flag &quot;deleteAllReports&quot; definida no arquivo &quot;testconfig.properties&quot;">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="hourglass"/>
</node>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1466954196927" ID="ID_1177451618" MODIFIED="1506273047976" STYLE="bubble" TEXT="1.1.2 WebDriverMaster: Prepara WebDriver">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="bell"/>
<node CREATED="1466954352040" ID="ID_460775702" MODIFIED="1506273047976" STYLE="bubble" TEXT="1.1.2.1 Carrega vari&#xe1;veis de ambiente (Grid Selenium)">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="hourglass"/>
</node>
<node CREATED="1466954487916" ID="ID_36134042" MODIFIED="1506273047976" STYLE="bubble" TEXT="1.1.2.2 Inicializa as capabilities de navegador na inst&#xe2;ncia do webDriver">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="forward"/>
</node>
<node CREATED="1466954612108" ID="ID_680008696" MODIFIED="1506273047976" STYLE="bubble" TEXT="1.1.2.3 Carrega a flag &quot;browserType&quot; para obter o tipo de navegador a ser executado">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="hourglass"/>
</node>
<node CREATED="1466954798409" ID="ID_345120095" MODIFIED="1506273047976" STYLE="bubble" TEXT="1.1.2.4 Carrega as capabilities definidas no arquivo .capabilities, caso exista">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="hourglass"/>
</node>
<node CREATED="1466954975282" ID="ID_1549029814" MODIFIED="1506273047976" STYLE="bubble" TEXT="1.1.2.5 Abre o navegador configurando as Desired Capabilities essenciais">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="wizard"/>
<node CREATED="1466955070606" ID="ID_1465739831" MODIFIED="1506273047976" STYLE="bubble" TEXT="1.1.2.5.1 Define os Profile/Options do navegador">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="edit"/>
<node CREATED="1466955141261" ID="ID_1466891467" MODIFIED="1506273047977" STYLE="bubble" TEXT="1.1.2.5.1.1 Instancia o novo WebDriver">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="wizard"/>
</node>
</node>
</node>
<node CREATED="1466955209382" ID="ID_780463666" MODIFIED="1506273047977" STYLE="bubble" TEXT="1.1.2.6 Inicializa a execu&#xe7;&#xe3;o do navegador pr&#xe9;-definido">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1466955676804" ID="ID_117491131" MODIFIED="1506273047977" STYLE="bubble" TEXT="1.1.3 Verifica se a inst&#xe2;ncia do WebDriver est&#xe1; nula">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="xmag"/>
<node CREATED="1466955734339" ID="ID_1680475428" MODIFIED="1506273047977" STYLE="bubble" TEXT="1.1.3.1 Instancia novo WebDriver caso esteja nulo">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="wizard"/>
</node>
</node>
<node CREATED="1466972248297" ID="ID_274944109" MODIFIED="1506273047977" STYLE="bubble" TEXT="1.1.4 Inicializa WebDriver disparando captura de eventos no contexto">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="forward"/>
</node>
<node CREATED="1466955853458" ID="ID_1816908546" MODIFIED="1506273047977" STYLE="bubble" TEXT="1.1.5 LOG: Inicializa&#xe7;&#xe3;o do WebDriver">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="list"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ccffff" CREATED="1466954226865" ID="ID_908707079" MODIFIED="1506273047977" STYLE="bubble" TEXT="2. BaseFlow">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="flag-black"/>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1466971772989" ID="ID_572551162" MODIFIED="1506273047977" STYLE="bubble" TEXT="2.1 RunnerTest">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="flag-green"/>
<node CREATED="1466971288930" ID="ID_1373587100" MODIFIED="1506273047977" STYLE="bubble" TEXT="2.1.1 Invoca ExecutionListener durante a execu&#xe7;&#xe3;o do Flow: checa resultados e plota gr&#xe1;fico de status da Suite">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="down"/>
<node CREATED="1466971502095" ID="ID_1046168288" MODIFIED="1506273047981" STYLE="bubble" TEXT="2.1.1.1 LOG e REPORT: Executa paralelamente ao runFlow() e escreve arquivos com os resultados e c&#xe1;lculos do gr&#xe1;fico de status da Suite executada">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="hourglass"/>
<icon BUILTIN="clock"/>
<icon BUILTIN="edit"/>
<icon BUILTIN="list"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1466957583918" ID="ID_1566048750" MODIFIED="1506273047984" STYLE="bubble" TEXT="2.1 @Before - setUp()">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="flag-green"/>
<node CREATED="1466957701869" ID="ID_1204669716" MODIFIED="1506273047984" STYLE="bubble" TEXT="2.1.1 Maximiza a janela do navegador">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="launch"/>
</node>
<node CREATED="1466958112982" ID="ID_1251407629" MODIFIED="1506273047985" STYLE="bubble" TEXT="2.1.2 REPORT: Cria a estrutrura do Head e Body do HTML">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="list"/>
<node CREATED="1466958220304" ID="ID_420585952" MODIFIED="1506273047985" STYLE="bubble" TEXT="2.1.2.1 Escreve informa&#xe7;&#xf5;es no arquivo HTML: Import do CSS, Nome do WebDriver, data e hora da execu&#xe7;&#xe3;o">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="edit"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1466957698975" ID="ID_1959562694" MODIFIED="1506273047988" STYLE="bubble" TEXT="2.2 @Test - runFlow()">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="flag-green"/>
<icon BUILTIN="launch"/>
<node CREATED="1466958589814" ID="ID_1189832797" MODIFIED="1506273047988" STYLE="bubble" TEXT="2.2.1 Inicia contagem de tempo da execu&#xe7;&#xe3;o do Flow">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="clock"/>
</node>
<node COLOR="#006633" CREATED="1466958748253" ID="ID_1711463778" MODIFIED="1506273047988" STYLE="bubble" TEXT="2.2.2 buildFlowStages() - Instancia lista vazia de Stages do Flow">
<font BOLD="true" NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="wizard"/>
<node CREATED="1466958799254" ID="ID_152000897" MODIFIED="1506273047988" STYLE="bubble" TEXT="2.2.2.1 Grava contagem do Flow em leitura">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="edit"/>
</node>
<node CREATED="1466958871064" ID="ID_229864530" MODIFIED="1506273048000" STYLE="bubble" TEXT="2.2.2.2 LOG e REPORT: Escreve arquivos .log e .html com cabe&#xe7;alho da Suite e Flow">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="list"/>
<node CREATED="1466958888033" ID="ID_345373034" MODIFIED="1506273048005" STYLE="bubble" TEXT="2.2.2.2.1 Escreve informa&#xe7;&#xf5;es no arquivo HTML: Descri&#xe7;&#xe3;o da Suite, Fluxo e Objetivo">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="edit"/>
</node>
</node>
</node>
<node COLOR="#006633" CREATED="1466959213700" ID="ID_960364890" MODIFIED="1506273048005" STYLE="bubble" TEXT="2.2.3 buildFlowStages() - Armazena na lista de Stages todas as Stages lidas no Flow da Suite">
<font BOLD="true" NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="edit"/>
<node CREATED="1466959530637" ID="ID_1761337620" MODIFIED="1506273048007" STYLE="bubble" TEXT="2.2.3.1 Grava a contagem de cada Stage em leitura">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="edit"/>
</node>
<node CREATED="1466959601272" ID="ID_1585224080" MODIFIED="1506273048007" STYLE="bubble" TEXT="2.2.3.2 LOG e REPORT: Escreve arquivo .log e .html com as Stages enumeradas">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="list"/>
</node>
<node CREATED="1466959699704" ID="ID_1676342638" MODIFIED="1506273048008" STYLE="bubble" TEXT="2.2.3.3 Reinicia a contagem das Stages e do Flow">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="clock"/>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1466963611315" ID="ID_1601701715" MODIFIED="1506273048008" STYLE="bubble" TEXT="2.2.3.4 BaseStage: Executa cada Stage da lista em mem&#xf3;ria">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="flag-green"/>
<icon BUILTIN="launch"/>
<node CREATED="1466961079133" ID="ID_381962477" MODIFIED="1506273048008" STYLE="bubble" TEXT="2.2.3.4.1 Orquestra as classes DPO (Data Provider Object) e os DP.json (massa de dados da Stage)">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="hourglass"/>
<node CREATED="1466961321243" ID="ID_1789148718" MODIFIED="1506273048010" STYLE="bubble" TEXT="2.2.3.4.1.1 Carrega os arquivos DPO com os dados dos arquivos DP.json passados no construtor da Stage">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="hourglass"/>
<icon BUILTIN="edit"/>
</node>
</node>
<node CREATED="1466961379449" ID="ID_1937034945" MODIFIED="1506273048012" STYLE="bubble" TEXT="2.2.3.4.2 Instancia as Page Objects referenciadas na Stage">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="wizard"/>
<node CREATED="1466961973085" ID="ID_1501619115" MODIFIED="1506273048012" STYLE="bubble" TEXT="2.2.3.4.2.1 Inicializa elementos mapeados na Page correspondente a partir da PageFactory">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="forward"/>
</node>
</node>
<node CREATED="1466960007756" ID="ID_1446373691" MODIFIED="1506273048012" STYLE="bubble" TEXT="2.2.3.4.3 LOG e REPORT: Escreve arquivos .log e .html com o nome da classe da Stage enumerada em execu&#xe7;&#xe3;o">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="list"/>
</node>
</node>
</node>
<node CREATED="1466962998686" ID="ID_306162163" MODIFIED="1506273048014" STYLE="bubble" TEXT="2.2.4 Itera sobre as Stages da lista em mem&#xf3;ria para obter o resultado/status final de cada Stage">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="go"/>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1466959857588" ID="ID_1447956490" MODIFIED="1506273048014" STYLE="bubble" TEXT="2.2.4.1 BaseStage: Executa a Stage em mem&#xf3;ria">
<edge WIDTH="1"/>
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="flag-green"/>
<icon BUILTIN="launch"/>
<node COLOR="#006633" CREATED="1466960744640" ID="ID_622889278" MODIFIED="1506273048014" STYLE="bubble" TEXT="2.2.4.1.1 Stage Atual: Executa o m&#xe9;todo runStage() para realizar as a&#xe7;&#xf5;es do teste">
<font BOLD="true" NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="flag-green"/>
<icon BUILTIN="launch"/>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1466962541471" ID="ID_1955783985" MODIFIED="1506273048014" STYLE="bubble" TEXT="3.2.4.1.1.1 Busca pelos m&#xe9;todos e elementos da Page Object instanciada (click, preenchimento, etc.)">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="flag-green"/>
<icon BUILTIN="launch"/>
</node>
<node CREATED="1466964061692" ID="ID_1808725349" MODIFIED="1506273128446" STYLE="bubble" TEXT="3.2.4.1.1.2 LOG e REPORT: Escreve os eventos sobre os elementos interagidos na tela do navegador com o WebDriver">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="edit"/>
<icon BUILTIN="list"/>
</node>
<node CREATED="1466964212871" ID="ID_470927660" MODIFIED="1506273048018" STYLE="bubble" TEXT="3.2.4.1.1.3 LOG e REPORT: Escreve o resultado final da Stage executada no Flow">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="edit"/>
<icon BUILTIN="list"/>
<node CREATED="1466964291099" ID="ID_737072321" MODIFIED="1506273048018" STYLE="bubble" TEXT="3.2.4.1.1.3-A) Caso ocorra alguma exce&#xe7;&#xe3;o ou falha de teste, o escopo da Stage &#xe9; interrompido">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="xmag"/>
<node CREATED="1466964387495" ID="ID_737926112" MODIFIED="1506273048018" STYLE="bubble" TEXT="A.1) A exce&#xe7;&#xe3;o correspondente &#xe9; lan&#xe7;ada">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="edit"/>
</node>
<node CREATED="1466964674331" ID="ID_500117677" MODIFIED="1506273048018" STYLE="bubble" TEXT="A.2) O Screenshot da falha &#xe9; armazenado em arquivo &quot;.png&quot;">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="edit"/>
</node>
<node CREATED="1466964750858" ID="ID_1072354855" MODIFIED="1506273048019" STYLE="bubble" TEXT="A.3) LOG e REPORT: Escreve arquivos .log e .html com exce&#xe7;&#xe3;o, screenshot e indicadores de falha">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="list"/>
</node>
</node>
<node CREATED="1466964999955" ID="ID_1807328262" MODIFIED="1506273048021" STYLE="bubble" TEXT="3.2.4.1.1.3-B) Caso ocorra sucesso na execu&#xe7;&#xe3;o da Stage, o escopo do Stage &#xe9; transferido para a pr&#xf3;xima Stage da itera&#xe7;&#xe3;o">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="xmag"/>
<node CREATED="1466965112093" ID="ID_1396465169" MODIFIED="1506273048022" STYLE="bubble" TEXT="B.1) LOG e REPORT: Escreve arquivos .log e .html com indicadores de sucesso">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="list"/>
</node>
</node>
</node>
</node>
</node>
</node>
<node CREATED="1466965325429" ID="ID_350734587" MODIFIED="1506273048022" STYLE="bubble" TEXT="2.2.5 LOG e REPORT: Escreve arquivos com o tempo total de execu&#xe7;&#xe3;o do Flow">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="list"/>
</node>
</node>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1466972981436" ID="ID_108374740" MODIFIED="1506273048023" STYLE="bubble" TEXT="2.3 @After - tearDown()">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="flag-green"/>
<icon BUILTIN="launch"/>
<node CREATED="1466973268622" ID="ID_1252271075" MODIFIED="1506273048023" STYLE="bubble" TEXT="2.3.1 Remove todos os cookies do navegador antes de finalizar o WebDriver">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="closed"/>
<node CREATED="1466973401102" ID="ID_482108520" MODIFIED="1506273048023" STYLE="bubble" TEXT="2.3.1.1 REPORT: Escreve arquivo com a finaliza&#xe7;&#xe3;o das tags de execu&#xe7;&#xe3;o do Flow no HTML">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="list"/>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ccffff" CREATED="1466973653542" ID="ID_535957656" MODIFIED="1506273048023" STYLE="bubble" TEXT="3. BaseSuite">
<font NAME="SansSerif" SIZE="13"/>
<node BACKGROUND_COLOR="#ccffcc" CREATED="1466973683907" ID="ID_1648995884" MODIFIED="1506273048023" STYLE="bubble" TEXT="3.1 AfterClass - tearDownClass()">
<font NAME="SansSerif" SIZE="13"/>
<node CREATED="1466974266438" ID="ID_1924508879" MODIFIED="1506273048023" STYLE="bubble" TEXT="3.1.1 Finaliza a execu&#xe7;&#xe3;o da Suite, fechando a sess&#xe3;o do webDriver">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="stop-sign"/>
</node>
<node CREATED="1466973723369" ID="ID_1542395082" MODIFIED="1506273048023" STYLE="bubble" TEXT="3.1.2 LOG: Escreve arquivo .log com a indica&#xe7;&#xe3;o de finaliza&#xe7;&#xe3;o da execu&#xe7;&#xe3;o da Suite">
<font NAME="SansSerif" SIZE="13"/>
<icon BUILTIN="list"/>
</node>
</node>
</node>
</node>
</node>
</map>
