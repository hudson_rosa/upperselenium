@tag
Feature: Validation of Postman sign in user when is attempting wrong values

  @tag1
  Scenario: Verification of Postman Website Sign In
    Given Open the browser and go to SignIn page on Postman Website
    When Enter enter the wrong username and password
    Then Appears an alert information
